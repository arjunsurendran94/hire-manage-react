import * as designationActions from '../../constants/actionConstants';
import { performRequest } from '../../config/index';


export default function addNewDesignationFromModal(values) {
    const headers ={"Content-Type": "application/json","Authorization" : "Token "+localStorage.getItem("token")||null}
    const org_slug = localStorage.getItem("organization_slug")
    let data={}
    data.organization=org_slug

    data.designation_name=values.designation_name
    if (values.user_permissions===undefined){
        data.permission_groups=[]
    }
    else{
        data.permission_groups=values.user_permissions
    }
    
    return dispatch => {

        dispatch({
            type: designationActions.ADD_NEW_DESIGANTION_FROM_MODAL_REQUEST
        })


        return performRequest('post', '/departments/designations/',headers, data)
        
            .then((response) => {
                if (response.status === 201) {
                    dispatch({
                        type: designationActions.ADD_NEW_DESIGANTION_FROM_MODAL_SUCCESS,
                        payload: response
                    })
                    // dispatch(reset('addDesignationform'))
                    // dispatch({
                    //     type:designationActions.UPDATING_DESIGNATIONS_REDUCER
                    // })
                   
                }
                
            })
            .catch((error) => {
                dispatch({
                    type: designationActions.ADD_NEW_DESIGANTION_FROM_MODAL_FAILURE,
                    payload: error
                })
            })
    }
}