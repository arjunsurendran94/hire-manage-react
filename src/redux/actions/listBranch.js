import * as branchType from '../../constants/actionConstants';
import { performRequest } from '../../config/index';
// import axios from 'axios'

export default function fetchUserBranch(org_slug,token){
    const headers ={"Authorization":"Token "+token}
    let params = {}
    params.organization = org_slug

    return function (dispatch) {
        //return  axios.post("http://8c5389eb.ngrok.io/users",postReqObj)
        dispatch({
            type: branchType.FETCH_BRANCH_REQUEST
        })
        return performRequest('post', '/organizations/branches/organization-branches/',headers,params)
        // return axios.get('https://hm-rest.herokuapp.com/accounts/email-confirmation/'+urlData.id+'/'+urlData.token+'/')    
            .then((response) => {
                if(response.data.length>0){
                    dispatch({
                        type: branchType.FETCH_BRANCH_SUCCESS,
                        payload: response
                    })
                }
            })
            .catch((error) => {
                dispatch({
                    type: branchType.FETCH_BRANCH_FAILURE,
                    payload: error
                })
            })
    }
}