import * as clientType from '../../constants/actionConstants';
import { performRequest } from '../../config/index';
import {reset} from 'redux-form';

export default function addClient(clientData){
        
    const headers ={"Authorization" : "Token "+localStorage.getItem("token")||null,"Content-Type": "application/json"}

    return dispatch => {
        dispatch({
            type: clientType.ADD_CLIENT_REQUEST
        })

        return performRequest('post', `/clients/clients/?portal_password=${clientData.portal_password}`,headers, clientData)
            .then((response) => {
                dispatch({
                    type: clientType.ADD_CLIENT_SUCCESS,
                    payload: response
                })
                dispatch(reset('addClientform'))
            })
            .catch((error) => {
                dispatch({
                    type: clientType.ADD_CLIENT_FAILURE,
                    payload: error
                })
            })
    }
}