import * as departmentType from '../../constants/actionConstants';
import { performRequest } from '../../config/index';

export default function editDepartment(values,department_slug){
    values.organization = localStorage.getItem("organization_slug")
    const headers ={"Content-Type": "application/json","Authorization" : "Token "+localStorage.getItem("token")||null}
    let data={}
    data.organization =localStorage.getItem("organization_slug")
    data.branch= values.branch
    data.department_name=values.department_name
    data.description=values.description
    return dispatch => {

        dispatch({
            type: departmentType.EDIT_DEPARTMENT_REQUEST
        })

        return performRequest('put', `departments/departments/${department_slug}/`,headers, data)

            .then((response) => {
                if (response.status === 200) {
                    dispatch({
                        type: departmentType.EDIT_DEPARTMENT_SUCCESS,
                        payload: response
                    })
                }
            })
            .catch((error) => {
                dispatch({
                    type: departmentType.EDIT_DEPARTMENT_FAILURE,
                    payload: error
                })
            })
    }
}