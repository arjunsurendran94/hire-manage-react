import { performRequest, } from '../../config/index';
import * as employeeType from '../../constants/actionConstants';

export default function searchFilterEmployees(obj_list) {

    const headers = { "Content-Type":"application/json","Authorization": "Token " + localStorage.getItem("token") }
    let organization_slug = localStorage.getItem("organization_slug") || null
    let params = new FormData()
    for (let key in obj_list) {
        params.append(key,obj_list[key])

      }
    params.append('organization',organization_slug)
    
    
    
    return dispatch => {
        dispatch({
            type: employeeType.SEARCH_FILTER_EMPLOYEE_REQUEST
        })

        return performRequest('post', '/employees/search-employees/organization-employees/', headers, params)
            
        .then((response) => {
                // debugger
                if (response.data) {
                    dispatch({
                        type: employeeType.SEARCH_FILTER_EMPLOYEE_SUCCESS,
                        payload: response
                    })

                }
            })
            .catch((error) => {
                // debugger
                dispatch({
                    type: employeeType.SEARCH_BRANCH_DEPARTMENTS_FAILURE,
                    payload: error
                })
            })
    }
}