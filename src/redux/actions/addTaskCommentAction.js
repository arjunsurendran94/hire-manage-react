
import * as taskType from '../../constants/actionConstants';
import { performRequest } from '../../config/index';
import {reset} from 'redux-form';

export default function addTaskComment(data){
        
    const headers ={"Authorization" : "Token "+localStorage.getItem("token")||null,"Content-Type": "application/json"}

    return dispatch => {
        dispatch({
            type: taskType.ADD_TASK_COMMENT_REQUEST
        })

        return performRequest('post', '/projects/comments/',headers, data)
            .then((response) => {
                
                dispatch({
                    type: taskType.ADD_TASK_COMMENT_SUCCESS,
                    payload: response
                })
                // dispatch(reset('addProjectform'))
            })
            .catch((error) => {
                
                dispatch({
                    type: taskType.ADD_TASK_COMMENT_FAILURE,
                    payload: error
                })
            })
    }
}