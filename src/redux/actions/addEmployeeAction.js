import * as employeeType from '../../constants/actionConstants';
import { performRequest } from '../../config/index';
import {reset} from 'redux-form';

export default function addEmployee(employeeData){

    
    const headers ={"Authorization" : "Token "+localStorage.getItem("token")||null,"Content-Type": "application/json"}

    return dispatch => {
        dispatch({
            type: employeeType.ADD_EMPLOYEE_REQUEST
        })

        return performRequest('post', '/employees/employees/',headers, employeeData)
            .then((response) => {
                dispatch({
                    type: employeeType.ADD_EMPLOYEE_SUCCESS,
                    payload: response
                })
                dispatch(reset('addEmployeeForm'))
            })
            .catch((error) => {
                dispatch({
                    type: employeeType.ADD_EMPLOYEE_FAILURE,
                    payload: error
                })
            })
    }
}