
import * as taskType from '../../constants/actionConstants';
import { performRequest } from '../../config/index';
import {reset} from 'redux-form';

export default function listTasks(data){
    // debugger
    if(data==undefined){
        data={}
        data.organization=localStorage.getItem('organization_slug')
    }
    else{
        data.organization=localStorage.getItem('organization_slug')
    }
    
    const headers ={"Authorization" : "Token "+localStorage.getItem("token")||null,"Content-Type": "application/json"}

    return dispatch => {
        dispatch({
            type: taskType.LISTING_TASK_REQUEST
        })

        return performRequest('post', 'projects/task/project-tasks/',headers,data)
            .then((response) => {
                // debugger
                dispatch({
                    type: taskType.LISTING_TASK_SUCCESS,
                    payload: response
                })
                // dispatch(reset('addProjectform'))
            })
            .catch((error) => {
                // debugger
                dispatch({
                    type: taskType.LISTING_TASK_FAILURE,
                    payload: error
                })
            })
    }
}