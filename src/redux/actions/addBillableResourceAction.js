import * as projectType from '../../constants/actionConstants';
import { performRequest } from '../../config/index';
import {reset} from 'redux-form';
import createContract from './createContractAction';

export default function addBillableResource(data){
    
    

    const form_data = new FormData();
      for (var key in data) {
         form_data.append(key, data[key]);
      }
    const headers ={"Authorization" : "Token "+localStorage.getItem("token")||null,"Content-Type": "application/json"}
    return dispatch => {
        dispatch({
            type: projectType.ADD_BILLABLE_RESOURCE_REQUEST
        })
       
        return performRequest('post', `/projects/project-resource-hourly/`,headers, form_data)
            .then((response) => {

                dispatch({
                    type: projectType.ADD_BILLABLE_RESOURCE_SUCCESS,
                    payload: response
                })

                // dispatch(createContract(dataContract))
                
            })
            .catch((error) => {
                dispatch({
                    type: projectType.ADD_BILLABLE_RESOURCE_FAILURE,
                    payload: error
                })
            })
    }
}