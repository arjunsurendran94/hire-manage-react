import * as projectType from '../../constants/actionConstants';
import { performRequest } from '../../config/index';
import {reset} from 'redux-form';

export default function addProjectAttachment(data){
        
    const headers ={"Authorization" : "Token "+localStorage.getItem("token")||null,"Content-Type": "application/json"}
    
    return dispatch => {
        dispatch({
            type: projectType.ADD_PROJECT_ATTACHMENT_REQUEST
        })

        return performRequest('post', '/projects/project-attachment-create/',headers, data)
            .then((response) => {
                

                dispatch({
                    type: projectType.ADD_PROJECT_ATTACHMENT_SUCCESS,
                    payload: response
                })
                // dispatch(reset('addProjectform'))
            })
            .catch((error) => {
                
                dispatch({
                    type: projectType.ADD_PROJECT_ATTACHMENT_FAILURE,
                    payload: error
                })
            })
    }
}