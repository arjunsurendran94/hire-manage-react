import * as designationActions from '../../constants/actionConstants';
import { performRequest } from '../../config/index';


export default function DesignationsListing() {
    const headers ={"Authorization" : "Token "+localStorage.getItem("token")||null}
    const org_slug = localStorage.getItem("organization_slug")

    let data ={}
    data.organization=org_slug
    
    return dispatch => {

        dispatch({
            type: designationActions.RETRIEVE_DESIGNATION_REQUEST
        })


        return performRequest('post', '/departments/designations/organization-designations/',headers, data)
        
            .then((response) => {
                if (response.status === 200) {
                    dispatch({
                        type: designationActions.RETRIEVE_DESIGNATION_SUCCESS,
                        payload: response
                    })
                   
                }
                
            })
            .catch((error) => {
                dispatch({
                    type: designationActions.RETRIEVE_DESIGNATION_FAILURE,
                    payload: error
                })
            })
    }
}