import { performRequest } from '../../config/index';
import * as departmentType from '../../constants/actionConstants';


export default function listDepartments(branch_slug,branch_name){

    const headers ={"Authorization":"Token "+localStorage.getItem("token")}
    let organization_slug = localStorage.getItem("organization_slug")
    let params = {}
    params.organization = organization_slug
    params.branch = branch_slug
    
    
    return dispatch =>{
        dispatch({
            type: departmentType.RETRIEVE_DEPARTMENTS_REQUEST
        })
        
    return performRequest('post','/departments/departments/organization-departments/',headers,{branch:branch_slug,organization:organization_slug})
        .then((response) => {
            if(response.data){
                dispatch({
                    type:departmentType.RETRIEVE_DEPARTMENTS_SUCCESS,
                    payload: response
                })
                if(branch_slug){
                    dispatch({
                        type: departmentType.RETRIEVE_BRANCH_DEPARTMENTS_SUCCESS,
                        payload: response
                    })
                }else{
                    dispatch({
                        type: departmentType.RETRIEVE_ORGANIZATION_DEPARTMENTS_SUCCESS,
                        payload: response
                    })
                }
            }
        })
        .catch((error) => {
            dispatch({
                type: departmentType.RETRIEVE_DEPARTMENTS_FAILURE,
                payload: error
            })
        })
    }
}