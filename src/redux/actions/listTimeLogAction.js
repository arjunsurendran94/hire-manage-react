import * as timeType from '../../constants/actionConstants';
import { performRequest } from '../../config/index';


export default function listTimeLog(data){
    // debugger
    console.log('inside timelog',data);
    
data.organization=localStorage.getItem('organization_slug')
const headers ={"Authorization" : "Token "+localStorage.getItem("token")||null,"Content-Type": "application/json"}


    return dispatch => {
        dispatch({
            type: timeType.LIST_TIME_LOG_REQUEST
        })

        return performRequest('post', '/projects/worksheet/employee-task-worksheet/',headers, data)
            .then((response) => {
                // debugger
                dispatch({
                    type: timeType.LIST_TIME_LOG_SUCCESS,
                    payload: response
                })
                
                
            })
            .catch((error) => {
                
                dispatch({
                    type: timeType.LIST_TIME_LOG_FAILURE,
                    payload: error
                })
            })
    }
}