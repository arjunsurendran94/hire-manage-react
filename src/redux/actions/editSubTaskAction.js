
import * as taskType from '../../constants/actionConstants';
import { performRequest } from '../../config/index';
import {reset} from 'redux-form';
import subTaskLog from './subTaskLogAction'

export default function editSubTask(data,slug){
        
    const headers ={"Authorization" : "Token "+localStorage.getItem("token")||null,"Content-Type": "application/json"}

    return dispatch => {
        dispatch({
            type: taskType.EDIT_SUBTASK_REQUEST
        })

        return performRequest('patch', `/projects/subtask/${slug}/`,headers, data)
            .then((response) => {
                
                dispatch({
                    type: taskType.EDIT_SUBTASK_SUCCESS,
                    payload: response
                })
                dispatch(subTaskLog(response?.data?.slug))
            })
            .catch((error) => {
                
                dispatch({
                    type: taskType.EDIT_SUBTASK_FAILURE,
                    payload: error
                })
            })
    }
}