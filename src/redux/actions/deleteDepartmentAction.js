import * as departmentType from '../../constants/actionConstants';
import { performRequest } from '../../config/index';

export default function deleteDepartment(slug,name){
    const headers ={"Authorization" : "Token "+localStorage.getItem("token")||null}
    return function (dispatch) {
        dispatch({
            type: departmentType.DELETE_DEPARTMENT_REQUEST
        })

        return performRequest('delete', '/departments/departments/'+slug+'/',headers)
            .then((response) => {
                if (response.statusText === "No Content") {
                    dispatch({
                        type: departmentType.DELETE_DEPARTMENT_SUCCESS,
                        payload: response,
                        department_name:name
                    })
                    dispatch({
                        type: departmentType.CLEAR_DEPARTMENTS_VALUES,
                        payload:slug
                    })
                }
            })
            .catch((error) => {
                dispatch({
                    type: departmentType.DELETE_DEPARTMENT_FAILURE,
                    payload: error
                })
            })
    }
}