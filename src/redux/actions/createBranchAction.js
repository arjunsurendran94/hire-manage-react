import { performRequest } from '../../config/index';
import * as branchActions from '../../constants/actionConstants';
import {reset} from 'redux-form';
import fetchBranchList from './listBranch';


export default function  CreateBranchAction(values) {

    const headers ={"Authorization" : "Token "+localStorage.getItem("token")||null}
    const org_slug = values.organization

    return function (dispatch) {
        dispatch({
            type: branchActions.CREATE_BRANCH_REQUEST
        })
        return performRequest('post', '/organizations/branches/',headers, values)
        
            .then((response) => {
                if (response.statusText === "Created") {
                    dispatch({
                        type: branchActions.CREATE_BRANCH_SUCCESS,
                        payload: response
                    })
                    dispatch(reset('branchform'))
                    dispatch(reset('addBranchContentform'))
                    dispatch(fetchBranchList(org_slug,localStorage.getItem("token")||null))
                }
                
            })
            .catch((error) => {
                dispatch({
                    type: branchActions.CREATE_BRANCH_FAILURE,
                    payload: error
                })
            })
    }
}
