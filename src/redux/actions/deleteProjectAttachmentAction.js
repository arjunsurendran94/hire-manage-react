import * as projectType from '../../constants/actionConstants';
import { performRequest } from '../../config/index';
import {reset} from 'redux-form';

export default function deleteProjectAttachment(id){
        
    const headers ={"Authorization" : "Token "+localStorage.getItem("token")||null,"Content-Type": "application/json"}
    
    return dispatch => {
        dispatch({
            type: projectType.DELETE_PROJECT_ATTACHMENT_REQUEST
        })

        return performRequest('delete', `/projects/project-attachment-create/${id}`,headers)
            .then((response) => {
                

                dispatch({
                    type: projectType.DELETE_PROJECT_ATTACHMENT_SUCCESS,
                    payload: response
                })
                // dispatch(reset('addProjectform'))
            })
            .catch((error) => {
                
                dispatch({
                    type: projectType.DELETE_PROJECT_ATTACHMENT_FAILURE,
                    payload: error
                })
            })
    }
}