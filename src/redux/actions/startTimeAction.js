import * as timeType from '../../constants/actionConstants';
import { performRequest } from '../../config/index';
import listTimeLog from './listTimeLogAction'

export default function startTime(data){

    const headers ={"Authorization" : "Token "+localStorage.getItem("token")||null,"Content-Type": "application/json"}


    return dispatch => {
        dispatch({
            type: timeType.START_TIME_REQUEST
        })

        return performRequest('post', '/projects/worksheet/',headers, data)
            .then((response) => {
                
                dispatch({
                    type: timeType.START_TIME_SUCCESS,
                    payload: response
                })
                dispatch(listTimeLog({employee:response?.data?.response?.employee,
                    
                    project:response?.data?.response?.project}))
                
            })
            .catch((error) => {
                
                dispatch({
                    type: timeType.START_TIME_FAILURE,
                    payload: error
                })
            })
    }
}