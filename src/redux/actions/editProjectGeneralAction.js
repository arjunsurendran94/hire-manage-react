import * as projectType from '../../constants/actionConstants';
import { performRequest } from '../../config/index';
import {reset} from 'redux-form';

export default function editProjectGeneralInfo(data,slug){
        
    const headers ={"Authorization" : "Token "+localStorage.getItem("token")||null,"Content-Type": "application/json"}
    return dispatch => {
        dispatch({
            type: projectType.EDIT_PROJECT_GENERAL_REQUEST
        })

        return performRequest('patch', `/projects/projects/${slug}/`,headers, data)
            .then((response) => {

                dispatch({
                    type: projectType.EDIT_PROJECT_GENERAL_SUCCESS,
                    payload: response
                })
                
            })
            .catch((error) => {
                dispatch({
                    type: projectType.EDIT_PROJECT_GENERAL_FAILURE,
                    payload: error
                })
            })
    }
}