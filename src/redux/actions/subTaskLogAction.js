
import * as taskType from '../../constants/actionConstants';
import { performRequest } from '../../config/index';
import {reset} from 'redux-form';

export default function subTaskLog(slug){

    const headers ={"Authorization" : "Token "+localStorage.getItem("token")||null,"Content-Type": "application/json"}

    return dispatch => {
        dispatch({
            type: taskType.LISTING_SUBTASK_LOG_REQUEST
        })

        return performRequest('post', '/projects/sub-task-logs/sub-task-log-by-change-reason/',headers, {sub_task:slug})
            .then((response) => {
                dispatch({
                    type: taskType.LISTING_SUBTASK_LOG_SUCCESS,
                    payload: response
                })
                // dispatch(reset('addProjectform'))
            })
            .catch((error) => {
                dispatch({
                    type: taskType.LISTING_SUBTASK_LOG_FAILURE,
                    payload: error
                })
            })
    }
}