import * as projectType from '../../constants/actionConstants';
import { performRequest } from '../../config/index';
import {reset} from 'redux-form';

export default function addResource(data){
        
    

    const headers ={"Authorization" : "Token "+localStorage.getItem("token")||null,"Content-Type": "application/json"}
    return dispatch => {
        dispatch({
            type: projectType.ADD_RESOURCE_REQUEST
        })
       
        return performRequest('post', `/projects/project-resource/`,headers, data)
            .then((response) => {

                dispatch({
                    type: projectType.ADD_RESOURCE_SUCCESS,
                    payload: response
                })
                
            })
            .catch((error) => {
                dispatch({
                    type: projectType.ADD_RESOURCE_FAILURE,
                    payload: error
                })
            })
    }
}