import * as citiesType from '../../constants/actionConstants';
import { performRequest } from '../../config/index';


export default function fetchCities(state_id){

    return function (dispatch) {
        
        // dispatch({
        //     type: citiesType.FETCH_CITIES_REQUEST
        // })
        return performRequest('get', '/general/city-list/'+state_id+'/')    
            .then((response) => {
                dispatch({
                    type: citiesType.FETCH_CITIES_SUCCESS,
                    payload: response
                })
            })
            .catch((error) => {
                dispatch({
                    type: citiesType.FETCH_CITIES_FAILURE,
                    payload: error
                })
            })
    }
}