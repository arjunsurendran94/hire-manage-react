import * as statesType from '../../constants/actionConstants';
import { performRequest } from '../../config/index';
// import axios from 'axios'

export default function fetchStates(country_id){

    return function (dispatch) {
        //return  axios.post("http://8c5389eb.ngrok.io/users",postReqObj)

        return performRequest('get', '/general/state-list/'+country_id+'/')
        // return axios.get('https://hm-rest.herokuapp.com/accounts/email-confirmation/'+urlData.id+'/'+urlData.token+'/')    
            .then((response) => {
                
                dispatch({
                    type: statesType.FETCH_STATES_SUCCESS,
                    payload: response
                })
            })
            .catch((error) => {
                dispatch({
                    type: statesType.FETCH_STATES_FAILURE,
                    payload: error
                })
            })
    }
}