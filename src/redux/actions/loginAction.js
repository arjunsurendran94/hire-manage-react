import * as loginType from '../../constants/actionConstants';
import { performRequest } from '../../config/index';
import fetchUserOrganization from './listOrganization';
import getCurrentEmployeePermission from './getCurrentEmployeePermissionAction'

export default function userLogin(loginData){

    let postReqObj = {};
    postReqObj.email=loginData.email;
    postReqObj.password=loginData.password;
    const headers ={}

    return function (dispatch) {
        //return  axios.post("http://8c5389eb.ngrok.io/users",postReqObj)
        dispatch({
            type: loginType.LOGIN_USER_REQUEST
        })
        return performRequest('post', '/accounts/login/',headers, postReqObj)
            .then((response) => {
                // if (response.data.message == "success") {
                //     dispatch(getAccessToken(postReqObj, response.data.id));
                // }
                dispatch({
                    
                    type: loginType.LOGIN_USER_SUCCESS,
                    payload: response
                })
                if(response.data.token){
                    localStorage.clear()
                    localStorage.setItem('token',response.data.token)
                    dispatch(fetchUserOrganization())
                }
                if(response.data.profile_slug){
                    localStorage.setItem('profile_slug',response.data.profile_slug)
                }
                if(response.data.user_type){
                    localStorage.setItem('user_type',response.data.user_type)
                }
                if(response.data.admin_emp_slug){
                    localStorage.setItem('admin_employee_slug',response.data.admin_emp_slug)
                }
                // localStorage.setItem("userId", response.data.data[0].userId);
                if(localStorage.getItem('user_type')==='employee'){
                dispatch(getCurrentEmployeePermission())
                }
            })
            .catch((error) => {
                dispatch({
                    type: loginType.LOGIN_USER_FAILURE,
                    payload: error
                })
            })
    }
}