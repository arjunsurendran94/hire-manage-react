import * as designationActions from '../../constants/actionConstants';
import { performRequest } from '../../config/index';


export default function RetrieveDesignation(slug) {
    const headers ={"Authorization" : "Token "+localStorage.getItem("token")||null}
    // const org_slug = localStorage.getItem("organization_slug")

    
    return dispatch => {

        dispatch({
            type: designationActions.DESIGNATION_DETAIL_REQUEST
        })


        return performRequest('get', `/departments/designations/${slug}`,headers)
        
            .then((response) => {
                if (response.status === 200) {
                    dispatch({
                        type: designationActions.DESIGNATION_DETAIL_SUCCESS,
                        payload: response
                    })
                   
                }
                
            })
            .catch((error) => {
                dispatch({
                    type: designationActions.DESIGNATION_DETAIL_FAILURE,
                    payload: error
                })
            })
    }
}