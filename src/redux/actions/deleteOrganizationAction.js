import * as organizationType from '../../constants/actionConstants';
import { performRequest } from '../../config/index';
import fetchUserOrganization from './listOrganization';

export default function deleteOrganization(slug){
    const headers ={"Authorization" : "Token "+localStorage.getItem("token")||null}
    return function (dispatch) {
        dispatch({
            type: organizationType.DELETE_ORGANIZATION_REQUEST
        })

        return performRequest('delete', '/organizations/organizations/'+slug+'/',headers)
            .then((response) => {
                if (response.statusText === "No Content") {
                    dispatch({
                        type: organizationType.DELETE_ORGANIZATION_SUCCESS,
                        payload: response
                    })
                    dispatch(fetchUserOrganization())
                }
            })
            .catch((error) => {
                dispatch({
                    type: organizationType.DELETE_ORGANIZATION_FAILURE,
                    payload: error
                })
            })
    }
}