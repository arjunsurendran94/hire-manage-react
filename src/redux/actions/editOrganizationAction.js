import * as organizationType from '../../constants/actionConstants';
import { performRequest } from '../../config/index';

export default function editOrganization(organizationValues){

    const organization_slug = localStorage.getItem("organization_slug")
    const headers ={"Content-Type": "application/json","Authorization" : "Token "+localStorage.getItem("token")||null}
    
    return dispatch => {

        dispatch({
            type: organizationType.EDIT_ORGANIZATION_REQUEST
        })

        return performRequest('put', `/organizations/organizations/${organization_slug}/`,headers, organizationValues)
            .then((response) => {
                if (response.status === 200) {
                    dispatch({
                        type: organizationType.EDIT_ORGANIZATION_SUCCESS,
                        payload: response
                    })
                }
            })
            .catch((error) => {
                dispatch({
                    type: organizationType.EDIT_ORGANIZATION_FAILURE,
                    payload: error
                })
            })
    }
}