import * as employeeType from '../../constants/actionConstants';
import { performRequest } from '../../config/index';

export default function employeeStatusChange(employee_slug){
    const headers ={"Authorization" : "Token "+localStorage.getItem("token")||null}

    return function (dispatch) {
        //return  axios.post("http://8c5389eb.ngrok.io/users",postReqObj)
        dispatch({
            type: employeeType.EMPLOYEE_STATUS_VERIFY_REQUEST
        })
        return performRequest('post', `/employees/employees/${employee_slug}/employee-status-change/`,headers)
        // return axios.get('https://hm-rest.herokuapp.com/accounts/email-confirmation/'+urlData.id+'/'+urlData.token+'/')    
            .then((response) => {
                dispatch({
                    type: employeeType.EMPLOYEE_STATUS_VERIFY_SUCCESS,
                    payload: response
                })
                // localStorage.setItem("token", response.data.token)
                // localStorage.setItem("profile_slug", response.data.profile_slug)
            })
            .catch((error) => {
                dispatch({
                    type: employeeType.EMPLOYEE_STATUS_VERIFY_FAILURE,
                    payload: error
                })
            })
    }
}