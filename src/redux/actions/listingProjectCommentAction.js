import * as projectType from '../../constants/actionConstants';
import { performRequest } from '../../config/index';
import {reset} from 'redux-form';

export default function listProjectComment(slug){
        
    const headers ={"Authorization" : "Token "+localStorage.getItem("token")||null,"Content-Type": "application/json"}
    
    let values={}
    values.project=slug
    const form_data = new FormData();
        for (var key in values) {
            form_data.append(key, values[key]);
        }
    return dispatch => {
        dispatch({
            type: projectType.LISTING_PROJECT_COMMENT_REQUEST
        })

        return performRequest('post', '/projects/client-comments/list-client-communication-comments/',headers, form_data)
            .then((response) => {

                dispatch({
                    type: projectType.LISTING_PROJECT_COMMENT_SUCCESS,
                    payload: response
                })
                // dispatch(reset('addProjectform'))
            })
            .catch((error) => {
                dispatch({
                    type: projectType.LISTING_PROJECT_COMMENT_FAILURE,
                    payload: error
                })
            })
    }
}