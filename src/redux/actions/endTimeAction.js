import * as timeType from '../../constants/actionConstants';
import { performRequest } from '../../config/index';
import listTimeLog from './listTimeLogAction'

export default function endTime(data,slug){
// debugger
console.log('INSIDEEEEE');

const headers ={"Authorization" : "Token "+localStorage.getItem("token")||null,"Content-Type": "application/json"}


    return dispatch => {
        dispatch({
            type: timeType.END_TIME_REQUEST
        })

        return performRequest('patch', `/projects/worksheet/${slug}/`,headers, data)
            .then((response) => {
                // debugger
                dispatch({
                    type: timeType.END_TIME_SUCCESS,
                    payload: response
                })
                dispatch(listTimeLog({employee:response?.data?.response?.employee,
                    
                    project:response?.data?.response?.project}))
                
            })
            .catch((error) => {
                // debugger
                dispatch({
                    type: timeType.END_TIME_FAILURE,
                    payload: error
                })
            })
    }
}