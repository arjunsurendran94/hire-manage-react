import * as designationActions from '../../constants/actionConstants';
import { performRequest } from '../../config/index';


export default function UpdateDesignation(values,slug) {
    const headers ={"Authorization" : "Token "+localStorage.getItem("token")||null}
    const org_slug = localStorage.getItem("organization_slug")

    let data ={
        designation_name:values.designation_name,
        organization:org_slug
    }
    if (values.user_permissions===undefined){
        data.permission_groups=[]
    }
    else{
        data.permission_groups=values.user_permissions
    }
    
    return dispatch => {

        dispatch({
            type: designationActions.DESIGNATION_UPDATE_REQUEST
        })


        return performRequest('put', `departments/designations/${slug}/`,headers,data)
        
            .then((response) => {
                if (response.status === 200) {
                    dispatch({
                        type: designationActions.DESIGNATION_UPDATE_SUCCESS,
                        payload: response
                    })
                   
                }
                
            })
            .catch((error) => {
                dispatch({
                    type: designationActions.DESIGNATION_UPDATE_FAILURE,
                    payload: error
                })
            })
    }
}