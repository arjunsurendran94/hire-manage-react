
import * as taskType from '../../constants/actionConstants';
import { performRequest } from '../../config/index';
import {reset} from 'redux-form';

export default function listingTaskComments(slug){
    
    let data={task:slug}
    const headers ={"Authorization" : "Token "+localStorage.getItem("token")||null,"Content-Type": "application/json"}

    return dispatch => {
        dispatch({
            type: taskType.LISTING_TASK_COMMENTS_REQUEST
        })

        return performRequest('post', '/projects/comments/task_comments/',headers, data)
            .then((response) => {
                
                dispatch({
                    type: taskType.LISTING_TASK_COMMENTS_SUCCESS,
                    payload: response
                })
                // dispatch(reset('addProjectform'))
            })
            .catch((error) => {
                
                dispatch({
                    type: taskType.LISTING_TASK_COMMENTS_FAILURE,
                    payload: error
                })
            })
    }
}