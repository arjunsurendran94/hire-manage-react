import * as changePasswordType from '../../constants/actionConstants';
import { performRequest } from '../../config/index';

export default function changePassword(urlData,password){
    const headers ={}
    return function (dispatch) {
        //return  axios.post("http://8c5389eb.ngrok.io/users",postReqObj)
        dispatch({
            type: changePasswordType.CHANGE_PASSWORD_REQUEST
        })
        return performRequest('post', '/accounts/reset-password/complete/'+urlData.id+'/'+urlData.token+'/',headers, password)
        // return axios.get('https://hm-rest.herokuapp.com/accounts/email-confirmation/'+urlData.id+'/'+urlData.token+'/')    
            .then((response) => {
                // if (response.data.message == "success") {
                //     dispatch(getAccessToken(postReqObj, response.data.id));
                // }
                if(response.data.status === "success"){
                    dispatch({
                        type: changePasswordType.CHANGE_PASSWORD_SUCCESS,
                        payload: response
                    })
                }
            })
            .catch((error) => {
                dispatch({
                    type: changePasswordType.CHANGE_PASSWORD_FAILURE,
                    payload: error
                })
            })
    }
}