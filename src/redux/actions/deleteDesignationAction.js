import * as designationActions from '../../constants/actionConstants';
import { performRequest } from '../../config/index';


export default function DeleteDesignation(slug) {
    const headers ={"Authorization" : "Token "+localStorage.getItem("token")||null}
    
    return dispatch => {

        dispatch({
            type: designationActions.DESIGNATION_DELETE_REQUEST
        })


        return performRequest('delete', `/departments/designations/${slug}/`,headers)
        
            .then((response) => {
                if (response.status === 204) {
                    dispatch({
                        type: designationActions.DESIGNATION_DELETE_SUCCESS,
                        payload: response
                    })
                   
                }
                
            })
            .catch((error) => {
                dispatch({
                    type: designationActions.DESIGNATION_DELETE_FAILURE,
                    payload: error
                })
            })
    }
}