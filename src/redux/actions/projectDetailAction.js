import * as projectActions from '../../constants/actionConstants';
import { performRequest } from '../../config/index';


export default function retrieveProject(slug) {
    const headers ={"Authorization" : "Token "+localStorage.getItem("token")||null,"Content-Type": "application/json"}
    // const org_slug = localStorage.getItem("organization_slug")
    
    
    return dispatch => {

        dispatch({
            type: projectActions.RETRIVE_PROJECT_REQUEST
        })


        return performRequest('get', `/projects/projects/${slug}`,headers)
        
            .then((response) => {
                
                if (response.status === 200) {
                    
                    dispatch({
                        type: projectActions.RETRIVE_PROJECT_SUCCESS,
                        payload: response
                    })
                   
                }
                
            })
            .catch((error) => {
               
                dispatch({
                    type: projectActions.RETRIVE_PROJECT_FAILURE,
                    payload: error
                })
            })
    }
}