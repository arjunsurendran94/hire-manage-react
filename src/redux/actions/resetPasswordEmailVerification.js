import * as emailType from '../../constants/actionConstants';
import { performRequest } from '../../config/index';

export default function resetEmailVerification(urlData){

    return function (dispatch) {
        return performRequest('get', '/accounts/reset-password/verify/'+urlData.id+'/'+urlData.token+'/')   
            .then((response) => {
                dispatch({
                    type: emailType.CONFIRM_PASSWORD_EMAIL_VERIFY_SUCCESS,
                    payload: response
                })
            })
            .catch((error) => {
                dispatch({
                    type: emailType.CONFIRM_PASSWORD_EMAIL_VERIFY_FAILURE,
                    payload: error
                })
            })
    }
}