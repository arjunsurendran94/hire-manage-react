import * as projectType from '../../constants/actionConstants';
import { performRequest } from '../../config/index';
import { reset } from 'redux-form';

export default function downloadContract(id,name) {
    // id = 184
    var str = name;
     var res = str.split("/");
     console.log("name",res,`${res[3]}`)
    const headers = { "Authorization": "Token " + localStorage.getItem("token") || null, "Content-Type": "application/json" }

    return dispatch => {
        dispatch({
            type: projectType.DOWNLOAD_CONTRACT_REQUEST
        })

        return performRequest('get', `/projects/contract/${id}/download`, headers)

            .then((response) => {
                let datafile = new Blob([response.data], { type: 'application/pdf' })
                let url = window.URL.createObjectURL(datafile);
                let a = document.createElement('a');
                a.href = `${response.config.baseURL}/projects/contract/${id}/download/`;
                a.download=`${res[3]}`
                a.click();
                dispatch({
                    type: projectType.DOWNLOAD_CONTRACT_SUCCESS,
                    payload: response
                })
            })
            .catch((error) => {
                dispatch({
                    type: projectType.DOWNLOAD_CONTRACT_FAILURE,
                    payload: error
                })
            })
    }
}