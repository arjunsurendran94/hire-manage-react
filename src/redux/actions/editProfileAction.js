import * as profileActionType from '../../constants/actionConstants';
import { performRequest } from '../../config/index';

export default function editProfile(profileValues){
    const headers ={"Content-Type": "application/json","Authorization" : "Token "+localStorage.getItem("token")||null}
    return function (dispatch) {
        dispatch({
            type: profileActionType.EDIT_PROFILE_REQUEST
        })
        return performRequest('put', '/accounts/update-user-profile/',headers, profileValues)
            .then((response) => {
                if (response.statusText === "OK") {
                    dispatch({
                        type: profileActionType.EDIT_PROFILE_SUCCESS,
                        payload: response.data
                    })
                }
                // dispatch(reset('signupform'))
                // localStorage.setItem("organization_slug",response.data.slug);
            })
            .catch((error) => {
                dispatch({
                    type: profileActionType.EDIT_PROFILE_FAILURE,
                    payload: error
                })
            })
    }
}