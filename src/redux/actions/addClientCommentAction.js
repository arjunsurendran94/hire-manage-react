import * as clientType from '../../constants/actionConstants';
import { performRequest } from '../../config/index';
import {reset} from 'redux-form';

export default function addClientComment(commentData){
    
    
    const headers ={"Authorization" : "Token "+localStorage.getItem("token")||null,"Content-Type": "application/json"}

    return dispatch => {
        dispatch({
            type: clientType.ADD_CLIENT_COMMENT_REQUEST
        })

        return performRequest('post', '/clients/client-comments/',headers,commentData)
            .then((response) => {
                
                dispatch({
                    type: clientType.ADD_CLIENT_COMMENT_SUCCESS,
                    payload: response
                })
                dispatch(reset('addClientCommentform'))    
            })
            .catch((error) => {
                dispatch({
                    type: clientType.ADD_CLIENT_COMMENT_FAILURE,
                    payload: error
                })
            })
    }
}