import * as resetPasswordType from '../../constants/actionConstants';
import { performRequest } from '../../config/index';
import {reset} from 'redux-form';
import {API_URL} from '../../config/hostSetting'

export default function resetPassword(email){

    let postReqObj = {};
    postReqObj.email=email.email;
    postReqObj.password_reset_url =API_URL+"/accounts/complete-password/";
    postReqObj.site_name = "Hire and Mange"
    const headers ={}

    return dispatch => {
        dispatch({
            type: resetPasswordType.RESET_PASSWORD_REQUEST
        })

        return performRequest('post', '/accounts/reset-password/',headers, postReqObj)
            .then((response) => {
                dispatch({
                    type: resetPasswordType.RESET_PASSWORD_SUCCESS,
                    payload: response
                })
                dispatch(reset('forgotpasswordform'))
            })
            .catch((error) => {
                dispatch({
                    type: resetPasswordType.RESET_PASSWORD_FAILURE,
                    payload: error
                })
            })
    }
}