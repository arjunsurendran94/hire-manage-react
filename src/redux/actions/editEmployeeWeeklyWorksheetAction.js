import { performRequest } from '../../config/index';
import * as employeeType from '../../constants/actionConstants';
import listEmployeeWeeklyWorksheet from './listEmployeeWeeklyWorksheetAction'
import listFixedWeeklyWorksheet from './listingFixedWeeklyWorksheetAction'

export default function editEmployeeWeeklyWorksheet(id,data,employee_slug,project_slug,type){
    
    const headers ={"Authorization":"Token "+localStorage.getItem("token")||null}
    
    return dispatch =>{
        dispatch({
            type: employeeType.EDIT_EMPLOYEE_WEEKLY_WORKSHEET_REQUEST
        })
        
    return performRequest('patch',`/projects/weekly-worksheet/${id}/`,headers,data)
        .then((response) => {
            
            if(response.data){

                dispatch({
                    type:employeeType.EDIT_EMPLOYEE_WEEKLY_WORKSHEET_SUCCESS,
                    payload: response
                })
                if(type==='hourly'){
                    dispatch(listEmployeeWeeklyWorksheet(employee_slug,project_slug))
                }else{
                    dispatch(listFixedWeeklyWorksheet(project_slug))
                }
                
            }
        })
        .catch((error) => {
            
            dispatch({
                type: employeeType.EDIT_EMPLOYEE_WEEKLY_WORKSHEET_FAILURE,
                payload: error
            })
        })
    }
}