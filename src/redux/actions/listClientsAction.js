import * as clientType from '../../constants/actionConstants';
import { performRequest } from '../../config/index';
import {reset} from 'redux-form';

export default function listClients(){
    
    
    const headers ={"Authorization" : "Token "+localStorage.getItem("token")||null,"Content-Type": "application/json"}

    const clientData={
        organization:localStorage.getItem("organization_slug"),
        branch:null
    }

    return dispatch => {
        dispatch({
            type: clientType.LIST_CLIENTS_REQUEST
        })

        return performRequest('post', '/clients/clients/organization-clients/',headers,clientData)
            .then((response) => {
                dispatch({
                    type: clientType.LIST_CLIENTS_SUCCESS,
                    payload: response
                })
                // dispatch(reset('addClientform'))
            })
            .catch((error) => {
                dispatch({
                    type: clientType.LIST_CLIENTS_FAILURE,
                    payload: error
                })
            })
    }
}