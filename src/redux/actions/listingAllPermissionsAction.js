import { performRequest } from '../../config/index';
import * as employeeType from '../../constants/actionConstants';


export default function listingAllPermissions(){
    const headers ={"Authorization":"Token "+localStorage.getItem("token")||null}
    
    
    return dispatch =>{
        dispatch({
            type: employeeType.LISTING_ALL_PERMISSIONS_REQUEST
        })
        
    return performRequest('get','/general/permission-list/',headers)
        .then((response) => {
            
            if(response.data){
                dispatch({
                    type:employeeType.LISTING_ALL_PERMISSIONS_SUCCESS,
                    payload: response
                })
            }
        })
        .catch((error) => {
            dispatch({
                type: employeeType.LISTING_ALL_PERMISSIONS_FAILURE,
                payload: error
            })
        })
    }
}