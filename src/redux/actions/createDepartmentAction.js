import * as departmentActions from '../../constants/actionConstants';
import { performRequest } from '../../config/index';
import {reset} from 'redux-form';

export default function  CreateDepartment(values,branch_slug) {
    
    const headers ={"Authorization" : "Token "+localStorage.getItem("token")||null}
    const org_slug = localStorage.getItem("organization_slug")
    let data={}
    data.organization=org_slug
    if(values.owner_type==="branch"){
        data.branch=values.branch_id
    }else{
        data.branch = ''
    }
    data.department_name=values.department_name
    if(values.description){
        data.description=values.description
    }
    

    return dispatch => {

        dispatch({
            type: departmentActions.CREATE_DEPARTMENT_REQUEST
        })


        return performRequest('post', '/departments/departments/',headers, data)
        
            .then((response) => {
                if (response.status === 201) {
                    // debugger
                    dispatch({
                        type: departmentActions.CREATE_DEPARTMENT_SUCCESS,
                        payload: response
                    })
                    dispatch(reset('addDepartmentform'))
                    if (response.data.branch===null){
                        dispatch({
                            type:departmentActions.UPDATE_NEW_ADDED_DEPT_IN_DEPTLIST,
                            payload:response.data
                        })
                    }
                    else{
                        dispatch({
                            type:departmentActions.UPDATE_NEW_ADDED_BRANCHDEPT_IN_DEPTLIST,
                            payload:response.data
                        })
                    }
                    
                    
                    // dispatch(reset('branchform'))
                }
                
            })
            .catch((error) => {
                dispatch({
                    type: departmentActions.CREATE_DEPARTMENT_FAILURE,
                    payload: error
                })
            })
    }
}