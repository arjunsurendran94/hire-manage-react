import * as employeeType from '../../constants/actionConstants';
import { performRequest } from '../../config/index';
// import axios from 'axios'

export default function partialUpdateEmployee(employee_slug,values){
    const headers ={"Authorization" : "Token "+localStorage.getItem("token")||null}
    // let params = {}
    // params.organization = org_slug

    return function (dispatch) {
        //return  axios.post("http://8c5389eb.ngrok.io/users",postReqObj)
        dispatch({
            type: employeeType.UPDATE_EMPLOYEE_REQUEST
        })
        return performRequest('patch', '/employees/employees/'+employee_slug+'/',headers,values)
        // return axios.get('https://hm-rest.herokuapp.com/accounts/email-confirmation/'+urlData.id+'/'+urlData.token+'/')    
            .then((response) => {
                if(response.status === 200){
                    dispatch({
                        type: employeeType.UPDATE_EMPLOYEE_SUCCESS,
                        payload: response
                    })
                }
            })
            .catch((error) => {
                dispatch({
                    type: employeeType.UPDATE_EMPLOYEE_FAILURE,
                    payload: error
                })
            })
    }
}