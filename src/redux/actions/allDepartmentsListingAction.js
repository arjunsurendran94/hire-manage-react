import { performRequest } from '../../config/index';
import * as departmentType from '../../constants/actionConstants';


export default function allDepartmentsListing(branch_slug){

    const headers ={"Authorization":"Token "+localStorage.getItem("token")}
    let organization_slug = localStorage.getItem("organization_slug")
    let params = {}
    params.organization = organization_slug
    params.branch = branch_slug
    
    
    return dispatch =>{
        dispatch({
            type: departmentType.RETRIEVE_ALL_DEPARTMENTS_REQUEST
        })
        
    return performRequest('post','/departments/departments/all-departments/',headers,params)
        .then((response) => {
            if(response.data){
                dispatch({
                    type:departmentType.RETRIEVE_ALL_DEPARTMENTS_SUCCESS,
                    payload: response
                })
            }
        })
        .catch((error) => {
            dispatch({
                type: departmentType.RETRIEVE_ALL_DEPARTMENTS_FAILURE,
                payload: error
            })
        })
    }
}