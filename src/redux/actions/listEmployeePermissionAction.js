import { performRequest } from '../../config/index';
import * as employeeType from '../../constants/actionConstants';


export default function listEmployeePermissions(employee_slug){
    const headers ={"Authorization":"Token "+localStorage.getItem("token")||null}
    
    
    return dispatch =>{
        dispatch({
            type: employeeType.LIST_EMPLOYEE_PERMISSION_REQUEST
        })
        
    return performRequest('get','/employees/employees/'+employee_slug+'/list-employee-permissions/',headers)
        .then((response) => {
            if(response.data){
                dispatch({
                    type:employeeType.LIST_EMPLOYEE_PERMISSION_SUCCESS,
                    payload: response
                })
            }
        })
        .catch((error) => {
            dispatch({
                type: employeeType.LIST_EMPLOYEE_PERMISSION_FAILURE,
                payload: error
            })
        })
    }
}