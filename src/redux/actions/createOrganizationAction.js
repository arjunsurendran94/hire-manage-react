import * as organizationType from '../../constants/actionConstants';
import { performRequest } from '../../config/index';

export default function createOrganization(organizationValues){
    const headers ={"Content-Type": "application/json","Authorization" : "Token "+localStorage.getItem("token")||null}
    return function (dispatch) {
        dispatch({
            type: organizationType.CREATE_ORGANIZATION_REQUEST
        })
        return performRequest('post', '/organizations/organizations/',headers, organizationValues)
            .then((response) => {
                if (response.statusText === "Created") {                    
                    dispatch({
                        type: organizationType.CREATE_ORGANIZATION_SUCCESS,
                        payload: response
                    })
                }
                
                // dispatch(reset('signupform'))
                
            })
            .catch((error) => {

                dispatch({
                    type: organizationType.CREATE_ORGANIZATION_FAILURE,
                    payload: error
                })

            })
    }
}