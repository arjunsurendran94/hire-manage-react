import * as employeeType from '../../constants/actionConstants';
import { performRequest } from '../../config/index';
// import axios from 'axios'

export default function retreiveEmployee(employee_slug){
    let token = localStorage.getItem("token")||null
    const headers ={"Authorization":"Token "+token}
    return function (dispatch) {
        dispatch({
            type: employeeType.RETRIEVE_EMPLOYEE_REQUEST
        })

        return performRequest('get', '/employees/employees/'+employee_slug+'/',headers)
            .then((response) => {
                if(response.status === 200 && response.data){
                    dispatch({
                        type: employeeType.RETRIEVE_EMPLOYEE_SUCCESS,
                        payload: response
                    })
                }
            })
            .catch((error) => {
                dispatch({
                    type: employeeType.RETRIEVE_EMPLOYEE_FAILURE,
                    payload: error
                })
            })
    }
}