import * as emailType from '../../constants/actionConstants';
import { performRequest } from '../../config/index';

export default function emailVerification(urlData){

    return function (dispatch) {
        //return  axios.post("http://8c5389eb.ngrok.io/users",postReqObj)
        dispatch({
            type: emailType.EMAIL_VERIFY_REQUEST
        })
        return performRequest('get', '/accounts/email-confirmation/'+urlData.id+'/'+urlData.token+'/')
        // return axios.get('https://hm-rest.herokuapp.com/accounts/email-confirmation/'+urlData.id+'/'+urlData.token+'/')    
            .then((response) => {
                // if (response.data.message == "success") {
                //     dispatch(getAccessToken(postReqObj, response.data.id));
                // }
                dispatch({
                    type: emailType.EMAIL_VERIFY_SUCCESS,
                    payload: response
                })
                localStorage.clear(); 
                localStorage.setItem("token", response.data.token)
                localStorage.setItem("profile_slug", response.data.profile_slug)
                localStorage.setItem('user_type','admin')
            })
            .catch((error) => {
                dispatch({
                    type: emailType.EMAIL_VERIFY_FAILURE,
                    payload: error
                })
            })
    }
}