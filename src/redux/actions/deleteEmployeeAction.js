import * as employeeType from '../../constants/actionConstants';
import { performRequest } from '../../config/index';


export default function DeleteEmployee(slug,name) {
    const headers ={"Authorization" : "Token "+localStorage.getItem("token")||null}
    
    return dispatch => {

        dispatch({
            type: employeeType.DELETE_EMPLOYEE_REQUEST
        })


        return performRequest('delete', `/employees/employees/${slug}/`,headers)
        
            .then((response) => {
                if (response.status === 204) {
                    dispatch({
                        type: employeeType.DELETE_EMPLOYEE_SUCCESS,
                        payload: response,
                        department_name:name
                    })
                   
                }
                
            })
            .catch((error) => {
                dispatch({
                    type: employeeType.DELETE_EMPLOYEE_FAILURE,
                    payload: error
                })
            })
    }
}