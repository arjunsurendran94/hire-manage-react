import * as rolesType from '../../constants/actionConstants';
import { performRequest } from '../../config/index';
// import axios from 'axios'

export default function retreivePermissionGroups(role_slug){
    let token = localStorage.getItem("token")||null
    const headers ={"Authorization":"Token "+token}
    return function (dispatch) {
        dispatch({
            type: rolesType.RETRIEVE_ROLES_REQUEST
        })

        return performRequest('get', '/general/permission-groups/'+role_slug+'/',headers)
            .then((response) => {
                if(response.status === 200 && response.data){
                    dispatch({
                        type: rolesType.RETRIEVE_ROLES_SUCCESS,
                        payload: response
                    })
                }
            })
            .catch((error) => {
                dispatch({
                    type: rolesType.RETRIEVE_ROLES_FAILURE,
                    payload: error
                })
            })
    }
}