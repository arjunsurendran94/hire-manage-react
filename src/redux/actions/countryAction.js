import * as countryType from '../../constants/actionConstants';
import { performRequest } from '../../config/index';
// import axios from 'axios'

export default function fetchCountry(){

    return function (dispatch) {
        // dispatch({
        //     type: countryType.FETCH_COUNTRY_REQUEST
        // })
        return performRequest('get', '/general/country-list/')    
            .then((response) => {
                dispatch({
                    type: countryType.FETCH_COUNTRY_SUCCESS,
                    payload: response
                })
            })
            .catch((error) => {
                dispatch({
                    type: countryType.FETCH_COUNTRY_FAILURE,
                    payload: error
                })
            })
    }
}