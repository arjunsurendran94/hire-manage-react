import { performRequest } from '../../config/index';
import * as organizatonType from '../../constants/actionConstants';


export default function retrieveOrganization(){
    const headers ={"Authorization":"Token "+localStorage.getItem("token")||null}
    const organization_slug = localStorage.getItem("organization_slug")
    return dispatch =>{
        dispatch({
            type: organizatonType.RETRIEVE_ORGANIZATION_REQUEST
        })
        
    return performRequest('get',`/organizations/organizations/${organization_slug}`,headers)
        .then((response) => {
            if(response.data){
                dispatch({
                    type: organizatonType.RETRIEVE_ORGANIZATION_SUCCESS,
                    payload: response
                })
            }
        })
        .catch((error) => {
            dispatch({
                type: organizatonType.RETRIEVE_ORGANIZATION_FAILURE,
                payload: error
            })
        })
    }
}