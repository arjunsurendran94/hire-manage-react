import * as roleType from '../../constants/actionConstants';
import { performRequest } from '../../config/index';
import retreivePermissionGroups from '../actions/retreiveRoleAction'

export default function editRole(values,role_slug){
    
    const headers ={"Content-Type": "application/json","Authorization" : "Token "+localStorage.getItem("token")||null}
    
    return dispatch => {

        dispatch({
            type: roleType.EDIT_ROLE_REQUEST
        })

        return performRequest('put', `/general/permission-groups/${role_slug}/`,headers, values)

            .then((response) => {
                if (response.status === 200) {
                    dispatch({
                        type: roleType.EDIT_ROLE_SUCCESS,
                        payload: response
                    })
                }
                dispatch(retreivePermissionGroups(role_slug))
            })
            .catch((error) => {
                dispatch({
                    type: roleType.EDIT_ROLE_FAILURE,
                    payload: error
                })
            })
    }
}