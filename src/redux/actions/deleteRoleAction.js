import * as roleType from '../../constants/actionConstants';
import { performRequest } from '../../config/index';
import fetchUserOrganization from './listOrganization';

export default function deleteRole(slug){
    const headers ={"Authorization" : "Token "+localStorage.getItem("token")||null}
    return function (dispatch) {
        dispatch({
            type: roleType.DELETE_ROLE_REQUEST
        })

        return performRequest('delete', '/general/permission-groups/'+slug+'/',headers)
            .then((response) => {
                if (response.statusText === "No Content") {
                    dispatch({
                        type: roleType.DELETE_ROLE_SUCCESS,
                        payload: response
                    })
                    dispatch(fetchUserOrganization())
                }
            })
            .catch((error) => {
                dispatch({
                    type: roleType.DELETE_ROLE_FAILURE,
                    payload: error
                })
            })
    }
}