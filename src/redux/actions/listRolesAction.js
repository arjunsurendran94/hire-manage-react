import * as rolesType from '../../constants/actionConstants';
import { performRequest } from '../../config/index';
// import axios from 'axios'

export default function fetchPermissionGroups(){
    let token = localStorage.getItem("token")
    const headers ={"Authorization":"Token "+token}
    let postReqObj = {};
    postReqObj.organization=localStorage.getItem("organization_slug")||null;
    return function (dispatch) {
        //return  axios.post("http://8c5389eb.ngrok.io/users",postReqObj)
        dispatch({
            type: rolesType.FETCH_ROLES_REQUEST
        })

        return performRequest('post', '/general/permission-groups/permission-groups/',headers,postReqObj)
        // return axios.get('https://hm-rest.herokuapp.com/accounts/email-confirmation/'+urlData.id+'/'+urlData.token+'/')    
            .then((response) => {
                if(response.status === 200 && response.data){
                    // localStorage.setItem("organization_slug",response.data[0].slug)
                    dispatch({
                        type: rolesType.FETCH_ROLES_SUCCESS,
                        payload: response
                    })
                }
            })
            .catch((error) => {
                dispatch({
                    type: rolesType.FETCH_ROLES_FAILURE,
                    payload: error
                })
            })
    }
}