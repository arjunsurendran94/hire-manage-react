import * as organizatonType from '../../constants/actionConstants';
import { performRequest } from '../../config/index';
// import axios from 'axios'

export default function fetchUserOrganization(){
    let token = localStorage.getItem("token")
    const headers ={"Authorization":"Token "+token}

    return function (dispatch) {
        //return  axios.post("http://8c5389eb.ngrok.io/users",postReqObj)
        dispatch({
            type: organizatonType.FETCH_ORGANIZATION_REQUEST
        })

        return performRequest('get', '/organizations/organizations/user-organizations/',headers)
        // return axios.get('https://hm-rest.herokuapp.com/accounts/email-confirmation/'+urlData.id+'/'+urlData.token+'/')    
            .then((response) => {
                if(response.data){
                    if(localStorage.getItem('organization_slug')===null){
                        localStorage.setItem("organization_slug",response.data[0].slug)
                        
                    }
                    dispatch({
                        type: organizatonType.FETCH_ORGANIZATION_SUCCESS,
                        payload: response
                    })
                }
            })
            .catch((error) => {
                dispatch({
                    type: organizatonType.FETCH_ORGANIZATION_FAILURE,
                    payload: error
                })
            })
    }
}