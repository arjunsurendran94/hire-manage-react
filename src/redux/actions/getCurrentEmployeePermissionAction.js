import * as employeeType from '../../constants/actionConstants';
import { performRequest } from '../../config/index';
// import axios from 'axios'

export default function getCurrentEmployeePermission(){
    let profileSlug = localStorage.getItem("profile_slug")||null
    const headers ={"Authorization":"Token "+localStorage.getItem("token")}
    return function (dispatch) {
        dispatch({
            type: employeeType.LOGGED_IN_USER_PERMISSIONS_REQUEST
        })

        return performRequest('get', '/employees/employees/'+profileSlug+'/list-employee-permissions/',headers)
            .then((response) => {
                    if(response&&response.data&&response.data.all_permissions){
                        localStorage.removeItem('permissions')
                        localStorage.removeItem('designation')
                        localStorage.setItem('permissions',response.data.all_permissions)
                        localStorage.setItem('designation',response.data.designationName)
                    }
                    dispatch({
                        type: employeeType.LOGGED_IN_USER_PERMISSIONS_SUCCESS,
                        payload: response
                    })
                
            })
            .catch((error) => {
                dispatch({
                    type: employeeType.LOGGED_IN_USER_PERMISSIONS_REQUEST,
                    payload: error
                })
            })
    }
}