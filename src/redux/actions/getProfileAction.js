import * as profile from '../../constants/actionConstants';
import { performRequest } from '../../config/index';
// import axios from 'axios'



export default function getProfile(){
    const token = localStorage.getItem("token")||null

    const headers ={"Authorization":"Token "+token}

    return function (dispatch) {
        dispatch({
            type: profile.GET_PROFILE_REQUEST
        })
        return performRequest('get', "/accounts/user-profile/",headers)
            .then((response) => {
                if(response.data){
                    dispatch({
                        type: profile.GET_PROFILE_SUCCESS,
                        payload: response
                    })

                }
                if(response.data&&response.data.emp_slug){
                    localStorage.setItem('admin_employee_slug',response.data.emp_slug)
                }
            })
            .catch((error) => {
                dispatch({
                    type: profile.GET_PROFILE_FAILURE,
                    payload: error
                })
            })
    }
}