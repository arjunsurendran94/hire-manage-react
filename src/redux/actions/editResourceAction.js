import * as projectType from '../../constants/actionConstants';
import { performRequest } from '../../config/index';
import {reset} from 'redux-form';

export default function editResource(data,id){
    const headers ={"Authorization" : "Token "+localStorage.getItem("token")||null,"Content-Type": "application/json"}
    return dispatch => {
        dispatch({
            type: projectType.EDIT_RESOURCE_REQUEST
        })
       
        return performRequest('patch', `/projects/project-resource/${id}/`,headers, data)
            .then((response) => {
                dispatch({
                    type: projectType.EDIT_RESOURCE_SUCCESS,
                    payload: response
                })
                
            })
            .catch((error) => {
                dispatch({
                    type: projectType.EDIT_RESOURCE_FAILURE,
                    payload: error
                })
            })
    }
}