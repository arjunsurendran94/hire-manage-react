import * as rolesType from '../../constants/actionConstants';
import { performRequest } from '../../config/index';
import {reset} from 'redux-form';
// import axios from 'axios'

export default function createPermissionGroups(values){
    let token = localStorage.getItem("token")||null
    const headers ={"Authorization":"Token "+token,"Content-Type":"application/json"}
    return function (dispatch) {
        //return  axios.post("http://8c5389eb.ngrok.io/users",postReqObj)
        dispatch({
            type: rolesType.CREATE_ROLES_REQUEST
        })

        return performRequest('post', '/general/permission-groups/',headers,values)
        // return axios.get('https://hm-rest.herokuapp.com/accounts/email-confirmation/'+urlData.id+'/'+urlData.token+'/')    
            .then((response) => {
                if(response.status === 201 && response.data){
                    // localStorage.setItem("organization_slug",response.data[0].slug)
                    dispatch({
                        type: rolesType.CREATE_ROLES_SUCCESS,
                        payload: response
                    })
                    dispatch(reset('addrolesform'))
                }
            })
            .catch((error) => {
                dispatch({
                    type: rolesType.CREATE_ROLES_FAILURE,
                    payload: error
                })
            })
    }
}