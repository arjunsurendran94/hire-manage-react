import * as timeType from '../../constants/actionConstants';
import { performRequest } from '../../config/index';
import listTimeLog from './listTimeLogAction'

export default function editTime(data,slug,type,dashboardEdit){


const headers ={"Authorization" : "Token "+localStorage.getItem("token")||null,"Content-Type": "application/json"}


    return dispatch => {
        dispatch({
            type: timeType.EDIT_TIME_REQUEST
        })

        return performRequest('patch', `/projects/worksheet/${slug}/`,headers, data)
            .then((response) => {
                // debugger
                dispatch({
                    type: timeType.EDIT_TIME_SUCCESS,
                    payload: response
                })
                
            
            })
            .catch((error) => {
                // debugger
                dispatch({
                    type: timeType.EDIT_TIME_FAILURE,
                    payload: error
                })
            })
    }
}