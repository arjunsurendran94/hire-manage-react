import * as clientType from '../../constants/actionConstants';
import { performRequest } from '../../config/index';
import {reset} from 'redux-form';

export default function editRemarkDetails(clientData,slug){
    clientData.client=slug
    const headers ={"Authorization" : "Token "+localStorage.getItem("token")||null}

    return dispatch => {
        dispatch({
            type: clientType.EDIT_CLIENT_REMARK_DETAILS_REQUEST
        })

        return performRequest('patch', `/clients/remarks/${slug}/`,headers, clientData)
            .then((response) => {
                dispatch({
                    type: clientType.EDIT_CLIENT_REMARK_DETAILS_SUCCESS,
                    payload: response
                })
                // dispatch(reset('addClientform'))
            })
            .catch((error) => {
                dispatch({
                    type: clientType.EDIT_CLIENT_REMARK_DETAILS_FAILURE,
                    payload: error
                })
            })
    }
}