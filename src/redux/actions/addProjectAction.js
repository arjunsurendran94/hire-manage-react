import * as projectType from '../../constants/actionConstants';
import { performRequest } from '../../config/index';
import {reset} from 'redux-form';

export default function addProject(data){
        
    const headers ={"Authorization" : "Token "+localStorage.getItem("token")||null,"Content-Type": "application/json"}
    // debugger
    return dispatch => {
        dispatch({
            type: projectType.ADD_PROJECT_REQUEST
        })

        return performRequest('post', '/projects/projects/',headers, data)
            .then((response) => {
                // debugger

                dispatch({
                    type: projectType.ADD_PROJECT_SUCCESS,
                    payload: response
                })
                // dispatch(reset('addProjectform'))
            })
            .catch((error) => {
                // debugger
                dispatch({
                    type: projectType.ADD_PROJECT_FAILURE,
                    payload: error
                })
            })
    }
}