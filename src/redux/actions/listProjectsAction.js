import * as projectType from '../../constants/actionConstants';
import { performRequest } from '../../config/index';
import {reset} from 'redux-form';

export default function listProjects(filterType){


    const headers ={"Authorization" : "Token "+localStorage.getItem("token")||null,"Content-Type": "application/json"}
    let data={}
    data.organization=localStorage.getItem('organization_slug')
    data.branch=''
    return dispatch => {
        dispatch({
            type: projectType.LISTING_PROJECT_REQUEST
        })

        return performRequest('post', `/projects/projects/organization-projects/?type=${filterType}`,headers,data)
            .then((response) => {
                
                dispatch({
                    type: projectType.LISTING_PROJECT_SUCCESS,
                    payload: response
                })
                // dispatch(reset('addProjectform'))
            })
            .catch((error) => {
                dispatch({
                    type: projectType.LISTING_PROJECT_FAILURE,
                    payload: error
                })
            })
    }
}