import * as designationActions from '../../constants/actionConstants';
import { performRequest } from '../../config/index';


export default function SortDesignations(sortlist) {
    const headers ={"Content-Type": "application/json","Authorization" : "Token "+localStorage.getItem("token")||null}
    const org_slug = localStorage.getItem("organization_slug")

    let data ={}
    data.organization=org_slug
    data.sort=sortlist
    
    return dispatch => {

        dispatch({
            type: designationActions.DESIGNATION_SORT_REQUEST
        })


        return performRequest('post', '/departments/designations/organization-designations/',headers, data)
        
            .then((response) => {
                if (response.status === 200) {
                    dispatch({
                        type: designationActions.DESIGNATION_SORT_SUCCESS,
                        payload: response
                    })
                }
                
            })
            .catch((error) => {
                dispatch({
                    type: designationActions.DESIGNATION_SORT_FAILURE,
                    payload: error
                })
            })
    }
}