import * as clientType from '../../constants/actionConstants';
import { performRequest } from '../../config/index';
import {reset} from 'redux-form';

export default function listClientComments(clientSlug){
    
    
    const headers ={"Authorization" : "Token "+localStorage.getItem("token")||null,"Content-Type": "application/json"}

    const data={
        client:clientSlug
        
    }

    return dispatch => {
        dispatch({
            type: clientType.LIST_CLIENT_COMMENTS_REQUEST
        })

        return performRequest('post', '/clients/client-comments/listing-clients-comments/',headers,data)
            .then((response) => {
                
                dispatch({
                    type: clientType.LIST_CLIENT_COMMENTS_SUCCESS,
                    payload: response
                })
                // dispatch(reset('addClientform'))
            })
            .catch((error) => {
                dispatch({
                    type: clientType.LIST_CLIENT_COMMENTS_FAILURE,
                    payload: error
                })
            })
    }
}