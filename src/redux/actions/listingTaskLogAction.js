
import * as taskType from '../../constants/actionConstants';
import { performRequest } from '../../config/index';
import {reset} from 'redux-form';

export default function listingTaskLog(data){

    // const form_data = new FormData();
    //     for (var key in data) {
    //         form_data.append(key, values[key]);
    //     }

    const headers ={"Authorization" : "Token "+localStorage.getItem("token")||null,"Content-Type": "application/json"}

    return dispatch => {
        dispatch({
            type: taskType.LISTING_TASK_LOG_REQUEST
        })

        return performRequest('post', '/projects/task-logs/task-log-by-change-reason/',headers,{task:data})
            .then((response) => {
                
                dispatch({
                    type: taskType.LISTING_TASK_LOG_SUCCESS,
                    payload: response
                })
                // dispatch(reset('addProjectform'))
            })
            .catch((error) => {
                
                dispatch({
                    type: taskType.LISTING_TASK_LOG_FAILURE,
                    payload: error
                })
            })
    }
}