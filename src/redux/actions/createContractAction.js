import * as projectType from '../../constants/actionConstants';
import { performRequest } from '../../config/index';
import {reset} from 'redux-form';

export default function createContract(data){
        
    const headers ={"Authorization" : "Token "+localStorage.getItem("token")||null,"Content-Type": "application/json"}
    return dispatch => {
        dispatch({
            type: projectType.CREATE_CONTRACT_REQUEST
        })
       
        return performRequest('post', `projects/contract-create/`,headers, data)
            .then((response) => {
                
                dispatch({
                    type: projectType.CREATE_CONTRACT_SUCCESS,
                    payload: response
                })
                
            })
            .catch((error) => {
                
                dispatch({
                    type: projectType.CREATE_CONTRACT_FAILURE,
                    payload: error
                })
            })
    }
}