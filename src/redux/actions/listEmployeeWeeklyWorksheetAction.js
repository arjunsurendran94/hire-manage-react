import { performRequest } from '../../config/index';
import * as employeeType from '../../constants/actionConstants';


export default function listEmployeeWeeklyWorksheet(employee_slug,project_slug){
    
    const headers ={"Authorization":"Token "+localStorage.getItem("token")||null}
    
    return dispatch =>{
        dispatch({
            type: employeeType.LISTING_EMPLOYEE_WEEKLY_WORKSHEET_REQUEST
        })
        
    return performRequest('post','/projects/weekly-worksheet/employee-weekly-worksheet/',headers,{employee:employee_slug,project:project_slug})
        .then((response) => {
            
            if(response.data){
                dispatch({
                    type:employeeType.LISTING_EMPLOYEE_WEEKLY_WORKSHEET_SUCCESS,
                    payload: response
                })
            }
        })
        .catch((error) => {
            
            dispatch({
                type: employeeType.LISTING_EMPLOYEE_WEEKLY_WORKSHEET_FAILURE,
                payload: error
            })
        })
    }
}