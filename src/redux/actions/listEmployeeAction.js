import { performRequest } from '../../config/index';
import * as employeeType from '../../constants/actionConstants';


export default function listEmployees(branch_slug){
    
    const headers ={"Authorization":"Token "+localStorage.getItem("token")}
    let organization_slug = localStorage.getItem("organization_slug")||null
    let params = {}
    params.organization = organization_slug
    params.branch = branch_slug
    
    
    return dispatch =>{
        dispatch({
            type: employeeType.LIST_EMPLOYEE_REQUEST
        })
        
    return performRequest('post','/employees/employees/organization-employees/',headers,{branch:branch_slug,organization:organization_slug})
        .then((response) => {
            
            if(response.data){
                dispatch({
                    type:employeeType.LIST_EMPLOYEE_SUCCESS,
                    payload: response
                })
                
            }
        })
        .catch((error) => {
            // debugger
            dispatch({
                type: employeeType.LIST_EMPLOYEE_FAILURE,
                payload: error
            })
        })
    }
}