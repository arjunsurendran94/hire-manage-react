
import * as taskType from '../../constants/actionConstants';
import { performRequest } from '../../config/index';
import {reset} from 'redux-form';

export default function addTask(data){
        
    const headers ={"Authorization" : "Token "+localStorage.getItem("token")||null,"Content-Type": "application/json"}

    return dispatch => {
        dispatch({
            type: taskType.ADD_TASK_REQUEST
        })

        return performRequest('post', '/projects/task/',headers, data)
            .then((response) => {
                // debugger
                dispatch({
                    type: taskType.ADD_TASK_SUCCESS,
                    payload: response
                })
                // dispatch(reset('addProjectform'))
            })
            .catch((error) => {
                // debugger
                dispatch({
                    type: taskType.ADD_TASK_FAILURE,
                    payload: error
                })
            })
    }
}