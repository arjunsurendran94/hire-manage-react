import * as timeZoneType from '../../constants/actionConstants';
import { performRequest } from '../../config/index';
// import axios from 'axios'

export default function fetchTimeZone(){
    return function (dispatch) {
        return performRequest('get', '/general/timezone-list/')
        .then((response) => {
                
                dispatch({
                    type: timeZoneType.FETCH_TIMEZONE_SUCCESS,
                    payload: response
                })
            })
            .catch((error) => {
                dispatch({
                    type: timeZoneType.FETCH_TIMEZONE_FAILURE,
                    payload: error
                })
            })
    }
}