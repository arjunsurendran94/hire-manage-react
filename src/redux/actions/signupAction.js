import * as signupType from '../../constants/actionConstants';
import { performRequest } from '../../config/index';
import {reset} from 'redux-form';
import {API_URL} from '../../config/hostSetting'

export default function userSignUp(signUpData){

    let postReqObj = {};
    postReqObj.user = {};
    postReqObj.user.email=signUpData.email;
    postReqObj.first_name=signUpData.firstname;
    postReqObj.last_name=signUpData.lastname;
    postReqObj.activation_url = API_URL+"/confirm-signup/"
    const headers ={"Content-Type": "application/json"}

    return dispatch => {
        dispatch({
            type: signupType.ADD_USER_REQUEST
        })

        return performRequest('post', '/accounts/signup/',headers, postReqObj)
            .then((response) => {
                dispatch({
                    type: signupType.ADD_USER_SUCCESS,
                    payload: response
                })
                dispatch(reset('signupform'))
            })
            .catch((error) => {
                dispatch({
                    type: signupType.ADD_USER_FAILURE,
                    payload: error
                })
            })
    }
}