import * as timeType from '../../constants/actionConstants';
import { performRequest } from '../../config/index';
import listTimeLog from './listTimeLogAction'

export default function addTime(data,type){
// debugger
console.log('INSIDEEEEE');

const headers ={"Authorization" : "Token "+localStorage.getItem("token")||null,"Content-Type": "application/json"}


    return dispatch => {
        dispatch({
            type: timeType.ADD_TIME_REQUEST
        })

        return performRequest('post', '/projects/worksheet/',headers, data)
            .then((response) => {
                
                dispatch({
                    type: timeType.ADD_TIME_SUCCESS,
                    payload: response
                })
                
                dispatch(listTimeLog({
                                        ...((type==='myTask')&&
                                        {employee:response?.data?.response?.employee}),
                                        project:response?.data?.response?.project}
                                        )
                        )
                
                
            })
            .catch((error) => {
                
                dispatch({
                    type: timeType.ADD_TIME_FAILURE,
                    payload: error
                })
            })
    }
}