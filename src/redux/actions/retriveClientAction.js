import * as clientType from '../../constants/actionConstants';
import { performRequest } from '../../config/index';
import {reset} from 'redux-form';

export default function retrieveClient(clientSlug){
    
    // debugger
    const headers ={"Authorization" : "Token "+localStorage.getItem("token")||null,"Content-Type": "application/json"}

    return dispatch => {
        dispatch({
            type: clientType.RETRIEVE_CLIENT_REQUEST
        })

        return performRequest('get', '/clients/clients/'+clientSlug+'/',headers)
            .then((response) => {
                dispatch({
                    type: clientType.RETRIEVE_CLIENTS_SUCCESS,
                    payload: response
                })
                // dispatch(reset('addClientform'))
            })
            .catch((error) => {
                dispatch({
                    type: clientType.RETRIEVE_CLIENTS_FAILURE,
                    payload: error
                })
            })
    }
}