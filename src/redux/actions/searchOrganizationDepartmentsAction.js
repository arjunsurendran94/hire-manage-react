import * as departmentType from '../../constants/actionConstants';
import { performRequest } from '../../config/index';

export default function searchOrganizationDepartments(item,filtering,branch_slug){

    const headers ={"Authorization":"Token "+localStorage.getItem("token")||null}
    let params = {}
    params.organization = localStorage.getItem("organization_slug")
    if (branch_slug === 'branches_only'){
        params.branch=''
        filtering='branches_only'
    }
    else if (filtering === 'all_departments' && branch_slug!=null){
        params.branch=''
    }
    else{
        params.branch=branch_slug
    }

    return function (dispatch) {
        dispatch({
            type: departmentType.SEARCH_ORGANIZATION_DEPARTMENTS_REQUEST
        })
        return performRequest('post', `/departments/search-departments/organization-departments/?q=${item}&branch_only=${filtering}`,headers,params)
            .then((response) => {
                if(response.status===200){
                    // response.data.map((i)=>i.branch===null)
                    // console.log('responseeee',response.data)
                    let org_objs= response.data.filter(i=>i.branch===null)
                    let bra_objs= response.data.filter(i=>i.branch!=null)  
                    dispatch({
                        type: departmentType.SEARCH_ORGANIZATION_DEPARTMENTS_SUCCESS,
                        organization_objs: org_objs,
                        branch_objs :bra_objs
                    })
                }
            })
            .catch((error) => {
                dispatch({
                    type: departmentType.SEARCH_ORGANIZATION_DEPARTMENTS_FAILURE,
                    payload: error
                })
            })
    }
}