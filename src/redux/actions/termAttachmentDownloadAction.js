import * as projectType from '../../constants/actionConstants';
import { performRequest } from '../../config/index';
import { reset } from 'redux-form';

export default function downloadTermAttachment(id,name) {
    // id = 184
    var str = name;
     var res = str.split("/");
     console.log("name",name,`${res[3]}`)
    const headers = { "Authorization": "Token " + localStorage.getItem("token") || null, "Content-Type": "application/json" }

    return dispatch => {
        dispatch({
            type: projectType.DOWNLOAD_TERM_ATTACHMENT_REQUEST
        })

        return performRequest('get', `projects/term/${id}/download/`, headers)

            .then((response) => {
                
                // let datafile = new Blob([response.data], { type: 'application/pdf' })
                // let url = window.URL.createObjectURL(datafile);
                let a = document.createElement('a');
                a.href = `${response.config.baseURL}/projects/term/${id}/download/`;
                a.download=`${res[4]}`
                a.click();
                dispatch({
                    type: projectType.DOWNLOAD_TERM_ATTACHMENT_SUCCESS,
                    payload: response
                })
            })
            .catch((error) => {
                
                dispatch({
                    type: projectType.DOWNLOAD_TERM_ATTACHMENT_FAILURE,
                    payload: error
                })
            })
    }
}