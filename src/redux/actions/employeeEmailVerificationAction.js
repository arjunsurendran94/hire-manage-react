import * as employeeType from '../../constants/actionConstants';
import { performRequest } from '../../config/index';

export default function employeeEmailVerification(urlData){

    return function (dispatch) {
        //return  axios.post("http://8c5389eb.ngrok.io/users",postReqObj)
        dispatch({
            type: employeeType.EMPLOYEE_EMAIL_VERIFY_REQUEST
        })
        return performRequest('get', '/employees/employee-email-verification/'+urlData.id+'/'+urlData.token+'/')
        // return axios.get('https://hm-rest.herokuapp.com/accounts/email-confirmation/'+urlData.id+'/'+urlData.token+'/')    
            .then((response) => {
                dispatch({
                    type: employeeType.EMPLOYEE_EMAIL_VERIFY_SUCCESS,
                    payload: response
                })
                // localStorage.setItem("token", response.data.token)
                // localStorage.setItem("profile_slug", response.data.profile_slug)
            })
            .catch((error) => {
                dispatch({
                    type: employeeType.EMPLOYEE_EMAIL_VERIFY_FAILURE,
                    payload: error
                })
            })
    }
}