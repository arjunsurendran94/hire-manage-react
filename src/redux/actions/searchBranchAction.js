import * as branchType from '../../constants/actionConstants';
import { performRequest } from '../../config/index';

export default function searchBranch(item){
    const headers ={"Authorization":"Token "+localStorage.getItem("token")||null}
    let params = {}
    params.organization = localStorage.getItem("organization_slug")

    return function (dispatch) {
        dispatch({
            type: branchType.SEARCH_BRANCH_REQUEST
        })
        return performRequest('post', `/organizations/branches/organization-branches/?q=${item}`,headers,params)
            .then((response) => {
                if(response.status===200){
                    dispatch({
                        type: branchType.SEARCH_BRANCH_SUCCESS,
                        payload: response
                    })
                }
            })
            .catch((error) => {
                dispatch({
                    type: branchType.SEARCH_BRANCH_FAILURE,
                    payload: error
                })
            })
    }
}