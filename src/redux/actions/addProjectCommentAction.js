import * as projectType from '../../constants/actionConstants';
import { performRequest } from '../../config/index';
import {reset} from 'redux-form';

export default function addProjectComment(data,slug){
        
    const headers ={"Authorization" : "Token "+localStorage.getItem("token")||null,"Content-Type": "application/json"}
    return dispatch => {
        dispatch({
            type: projectType.ADD_PROJECT_COMMENT_REQUEST
        })

        return performRequest('post', `/projects/client-comments/?project=${slug}`,headers, data)
            .then((response) => {

                dispatch({
                    type: projectType.ADD_PROJECT_COMMENT_SUCCESS,
                    payload: response
                })
                // dispatch(reset('addProjectform'))
            })
            .catch((error) => {
                dispatch({
                    type: projectType.ADD_PROJECT_COMMENT_FAILURE,
                    payload: error
                })
            })
    }
}