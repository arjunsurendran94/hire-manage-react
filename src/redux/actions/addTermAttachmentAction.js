import * as projectType from '../../constants/actionConstants';
import { performRequest } from '../../config/index';
import {reset} from 'redux-form';

export default function addTermAttachment(data){
    
    const form_data = new FormData();
        for (var key in data) {
            form_data.append(key, data[key]);
        }    

    const headers ={"Authorization" : "Token "+localStorage.getItem("token")||null,"Content-Type": "application/json"}
    
    return dispatch => {
        dispatch({
            type: projectType.ADD_TERM_ATTACHMENT_REQUEST
        })

        return performRequest('post', `/projects/term-attachement-update/`,headers, form_data)
            .then((response) => {
                
                dispatch({
                    type: projectType.ADD_TERM_ATTACHMENT_SUCCESS,
                    payload: response
                })
                
            })
            .catch((error) => {
                dispatch({
                    type: projectType.ADD_TERM_ATTACHMENT_FAILURE,
                    payload: error
                })
            })
    }
}