import * as logOutType from '../../constants/actionConstants';
import { performRequest } from '../../config/index';

export default function userLogout(){
    let token = localStorage.getItem("token")
    const headers ={"Content-Type": "application/json","Authorization":"Token "+token}

    return function (dispatch) {
        return performRequest('get', '/accounts/logout/',headers)
            .then((response) => {
                if(response.statusText === "OK"){
                    localStorage.clear()
                    dispatch({
                        type: logOutType.LOGOUT_SUCCESS,
                        payload: response
                    })
                    dispatch({
                        type: logOutType.USER_LOGOUT
                    })
                    
                }
            })
            .catch((error) => {
                dispatch({
                    type: logOutType.LOGOUT_FAILURE,
                    payload: error
                })
            })
    }
}