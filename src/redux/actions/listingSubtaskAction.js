
import * as taskType from '../../constants/actionConstants';
import { performRequest } from '../../config/index';
import {reset} from 'redux-form';

export default function listingSubTask(task){
    // debugger

    const headers ={"Authorization" : "Token "+localStorage.getItem("token")||null,"Content-Type": "application/json"}

    return dispatch => {
        dispatch({
            type: taskType.LISTING_SUBTASK_REQUEST
        })

        return performRequest('post', '/projects/subtask/subtask-listing/',headers, {task:task})
            .then((response) => {
                // debugger
                dispatch({
                    type: taskType.LISTING_SUBTASK_SUCCESS,
                    payload: response
                })
                // dispatch(reset('addProjectform'))
            })
            .catch((error) => {
                // debugger
                dispatch({
                    type: taskType.LISTING_SUBTASK_FAILURE,
                    payload: error
                })
            })
    }
}