import * as projectType from '../../constants/actionConstants';
import { performRequest } from '../../config/index';
import {reset} from 'redux-form';

export default function deleteResource(id){
        
    const headers ={"Authorization" : "Token "+localStorage.getItem("token")||null,"Content-Type": "application/json"}
    return dispatch => {
        dispatch({
            type: projectType.DELETE_RESOURCE_REQUEST
        })
       
        return performRequest('delete', `/projects/project-resource/${id}/`,headers)
            .then((response) => {

                dispatch({
                    type: projectType.DELETE_RESOURCE_SUCCESS,
                    payload: response
                })
                
            })
            .catch((error) => {
                dispatch({
                    type: projectType.DELETE_RESOURCE_FAILURE,
                    payload: error
                })
            })
    }
}