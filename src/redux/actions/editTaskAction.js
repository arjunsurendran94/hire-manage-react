
import * as taskType from '../../constants/actionConstants';
import { performRequest } from '../../config/index';
import listMyTasks from './listMyTaskAction'


import {reset} from 'redux-form';


export default function editTask(data,slug){
        // debugger
    const headers ={"Authorization" : "Token "+localStorage.getItem("token")||null,"Content-Type": "application/json"}

    return dispatch => {
        dispatch({
            type: taskType.EDIT_TASK_REQUEST
        })

        return performRequest('patch', `/projects/task/${slug}/`,headers, data)
            .then((response) => {
                // debugger
                dispatch({
                    type: taskType.EDIT_TASK_SUCCESS,
                    payload: response
                })
                dispatch(listMyTasks({project:response?.data?.project,assigned:localStorage.getItem('admin_employee_slug')}))
                
            })
            .catch((error) => {
                // debugger
                dispatch({
                    type: taskType.EDIT_TASK_FAILURE,
                    payload: error
                })
            })
    }
}