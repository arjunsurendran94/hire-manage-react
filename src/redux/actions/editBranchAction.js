import * as branchType from '../../constants/actionConstants';
import { performRequest } from '../../config/index';

export default function editBranch(values,branch_slug){
    values.organization = localStorage.getItem("organization_slug")
    const headers ={"Content-Type": "application/json","Authorization" : "Token "+localStorage.getItem("token")||null}
    
    return dispatch => {

        dispatch({
            type: branchType.EDIT_BRANCH_REQUEST
        })

        return performRequest('put', `organizations/branches/${branch_slug}/`,headers, values)

            .then((response) => {
                if (response.status === 200) {
                    dispatch({
                        type: branchType.EDIT_BRANCH_SUCCESS,
                        payload: response
                    })
                }
            })
            .catch((error) => {
                dispatch({
                    type: branchType.EDIT_BRANCH_FAILURE,
                    payload: error
                })
            })
    }
}