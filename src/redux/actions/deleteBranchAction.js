import * as branchType from '../../constants/actionConstants';
import { performRequest } from '../../config/index';
import fetchUserBranch from './listBranch'

export default function deleteBranch(slug,name){
    const headers ={"Authorization" : "Token "+localStorage.getItem("token")||null}
    return function (dispatch) {
        dispatch({
            type: branchType.DELETE_BRANCH_REQUEST
        })

        return performRequest('delete', '/organizations/branches/'+slug+'/',headers)
            .then((response) => {
                if (response.statusText === "No Content") {
                    dispatch({
                        type: branchType.DELETE_BRANCH_SUCCESS,
                        payload: response,
                        branch_name:name
                    })
                    dispatch(fetchUserBranch(localStorage.getItem('organization_slug'),localStorage.getItem('token')))
                }
            })
            .catch((error) => {
                dispatch({
                    type: branchType.DELETE_BRANCH_FAILURE,
                    payload: error
                })
            })
    }
}