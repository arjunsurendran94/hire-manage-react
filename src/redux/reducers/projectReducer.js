import * as projectType from '../../constants/actionConstants';

export function projectReducer(state = null, action) {
    switch (action.type) {
        case projectType.ADD_PROJECT_REQUEST:
            return { ...state, isLoading: true };

        case projectType.ADD_PROJECT_SUCCESS:
            // debugger
            return {
                ...state,
                project_obj: action.payload.data,
                // projects:state.projects.concat(action.payload.data),
                isLoading: false,
                status: 'createsuccess',
            }

        case projectType.ADD_PROJECT_FAILURE:
            // debugger
                return {
                    ...state,
                    project_obj: action.payload.response,
                    isLoading: false,
                    status: 'error',
                }
        
        case projectType.LISTING_PROJECT_REQUEST:
            return { ...state, status:'',isLoading: true };
        
        case projectType.LISTING_PROJECT_SUCCESS:
            return {
                ...state,
                projects: action.payload.data,
                isLoading: false,
                status: 'success',
            }

        case projectType.LISTING_PROJECT_FAILURE:
                    return {
                        ...state,
                        projects: action.payload.response,
                        isLoading: false,
                        status: 'error',
                    }
        
        case projectType.RETRIVE_PROJECT_REQUEST:
            return { ...state, status:'',isLoading: true };

        case projectType.RETRIVE_PROJECT_SUCCESS:

            console.log('state.projectObj',state.projectObj)
            

            return {
                ...state,
                projectObj: action.payload.data,
                isLoading: false,
                status: 'retrive_success',
            }

        case projectType.RETRIVE_PROJECT_FAILURE:
                    return {
                        ...state,
                        projectObj: action.payload.response,
                        isLoading: false,
                        status: 'error',
                    }

        case projectType.ADD_PROJECT_COMMENT_REQUEST:
            return { ...state, status:'',isLoading: true };

        case projectType.ADD_PROJECT_COMMENT_SUCCESS:
            return {
                ...state,
                projectCommentObj: action.payload.data,
                isLoading: false,
                status: 'success',
                projectComments:state.projectComments.concat(action.payload.data),
            }

        case projectType.ADD_PROJECT_COMMENT_FAILURE:
            return {
                ...state,
                projectComments: action.payload.response,
                isLoading: false,
                status: 'error',
            }

        case projectType.LISTING_PROJECT_COMMENT_REQUEST:
            return { ...state, status:'',isLoading: true };

        case projectType.LISTING_PROJECT_COMMENT_SUCCESS:
            return {
                ...state,
                projectComments: action.payload.data,
                isLoading: false,
                status: 'success',
            }

        case projectType.LISTING_PROJECT_COMMENT_FAILURE:
            return {
                ...state,
                projectComments: action.payload.response,
                isLoading: false,
                status: 'error',
            }
        
        case projectType.DOWNLOAD_CONTRACT_REQUEST:
            return { ...state, status:'',isLoading: true };

        case projectType.DOWNLOAD_CONTRACT_SUCCESS:
            return {
                ...state,
                contract: action.payload.data,
                isLoading: false,
                status: 'success',
            }

        case projectType.DOWNLOAD_CONTRACT_FAILURE:
            return {
                ...state,
                contract: action.payload.response,
                isLoading: false,
                status: 'error',
            }
        
        case projectType.EDIT_PROJECT_GENERAL_REQUEST:
            return { ...state, status:'',isLoading: true };
        case projectType.EDIT_PROJECT_GENERAL_SUCCESS:
            return {
                ...state,
                projectObj: action.payload.data,
                isLoading: false,
                status: 'editsuccess',
            }

        case projectType.EDIT_PROJECT_GENERAL_FAILURE:
            return {
                ...state,
                projectObj: action.payload.response,
                isLoading: false,
                status: 'error',
            }


        case projectType.EDIT_RESOURCE_REQUEST:
            return { ...state, status:'',isLoading: true };
        case projectType.EDIT_RESOURCE_SUCCESS:
            return {
                ...state,
                projectObj: action.payload.data,
                isLoading: false,
                status: 'editsuccess',
            }

        case projectType.EDIT_RESOURCE_FAILURE:
            return {
                ...state,
                projectObj: action.payload.response,
                isLoading: false,
                status: 'error',
            }
        case projectType.ADD_RESOURCE_REQUEST:
            return { ...state, status:'',isLoading: true };
        case projectType.ADD_RESOURCE_SUCCESS:
            return {
                ...state,
                projectObj: action.payload.data,
                isLoading: false,
                status: 'editsuccess',
            }

        case projectType.ADD_RESOURCE_FAILURE:
            return {
                ...state,
                projectObj: action.payload.response,
                isLoading: false,
                status: 'error',
            }
        case projectType.DELETE_RESOURCE_REQUEST:
            return { ...state, status:'',isLoading: true };
        case projectType.DELETE_RESOURCE_SUCCESS:
            return {
                ...state,
                projectObj: action.payload.data,
                isLoading: false,
                status: 'delete_success',
            }

        case projectType.DELETE_RESOURCE_FAILURE:
            return {
                ...state,
                projectObj: action.payload.response,
                isLoading: false,
                status: 'error',
            }

        case projectType.CREATE_CONTRACT_REQUEST:
            return { ...state, status:'',isLoading: true };
        case projectType.CREATE_CONTRACT_SUCCESS:
            return {
                ...state,
                projectObj: action.payload.data,
                isLoading: false,
                status: 'editsuccess',
            }

        case projectType.CREATE_CONTRACT_FAILURE:
            return {
                ...state,
                projectObj: action.payload.response,
                isLoading: false,
                status: 'error',
            }
    
        case projectType.ADD_BILLABLE_RESOURCE_REQUEST:
            return { ...state, status:'',isLoading: true };
        case projectType.ADD_BILLABLE_RESOURCE_SUCCESS:
            return {
                ...state,
                projectObj: action.payload.data,
                isLoading: false,
                status: 'editsuccess',
            }

        case projectType.ADD_BILLABLE_RESOURCE_FAILURE:
            return {
                ...state,
                projectObj: action.payload.response,
                isLoading: false,
                status: 'error',
            }
        
        case projectType.ADD_PROJECT_ATTACHMENT_REQUEST:
            return { ...state, status:'',isLoading: true };
        case projectType.ADD_PROJECT_ATTACHMENT_SUCCESS:
            return {
                ...state,
                projectObj: action.payload.data,
                isLoading: false,
                status: 'editsuccess',
            }

        case projectType.ADD_PROJECT_ATTACHMENT_FAILURE:
            return {
                ...state,
                projectObj: action.payload.response,
                isLoading: false,
                status: 'error',
                
            }
        case projectType.DELETE_PROJECT_ATTACHMENT_REQUEST:
            return { ...state, status:'',isLoading: true };
        case projectType.DELETE_PROJECT_ATTACHMENT_SUCCESS:
            return {
                ...state,
                projectObj: action.payload.data,
                isLoading: false,
                status: 'delete_success',
            }

        case projectType.DELETE_PROJECT_ATTACHMENT_FAILURE:
            return {
                ...state,
                projectObj: action.payload.response,
                isLoading: false,
                status: 'error',
                
            }

        case projectType.ADD_CONTRACT_ATTACHMENT_REQUEST:
            return { ...state, status:'',isLoading: true };
        case projectType.ADD_CONTRACT_ATTACHMENT_SUCCESS:
            return {
                ...state,
                projectObj: action.payload.data,
                isLoading: false,
                status: 'editsuccess',
            }

        case projectType.ADD_CONTRACT_ATTACHMENT_FAILURE:
            return {
                ...state,
                projectObj: action.payload.response,
                isLoading: false,
                status: 'error',
                
            }
    
    case projectType.ADD_TERM_ATTACHMENT_REQUEST:
        return { ...state, status:'',isLoading: true };
    case projectType.ADD_TERM_ATTACHMENT_SUCCESS:
        return {
            ...state,
            projectObj: action.payload.data,
            isLoading: false,
            status: 'editsuccess',
        }

    case projectType.ADD_TERM_ATTACHMENT_FAILURE:
        return {
            ...state,
            projectObj: action.payload.response,
            isLoading: false,
            status: 'error',
            
        }

    case projectType.DOWNLOAD_TERM_ATTACHMENT_REQUEST:
        return { ...state, status:'',isLoading: true };
    case projectType.DOWNLOAD_TERM_ATTACHMENT_SUCCESS:
        return {
            ...state,
            term: action.payload.data,
            isLoading: false,
            status: 'success',
        }

    case projectType.DOWNLOAD_TERM_ATTACHMENT_FAILURE:
        return {
            ...state,
            term: action.payload.response,
            isLoading: false,
            status: 'error',
        }
    
    case projectType.LISTING_EMPLOYEE_WEEKLY_WORKSHEET_REQUEST:
        return { ...state, status:'',isLoading: true };
    case projectType.LISTING_EMPLOYEE_WEEKLY_WORKSHEET_SUCCESS:
        return {
            ...state,
            weeklyworksheets: action.payload.data,
            isLoading: false,
            status: 'success',
        }

    case projectType.LISTING_EMPLOYEE_WEEKLY_WORKSHEET_FAILURE:
        return {
            ...state,
            weeklyworksheets: action.payload.response,
            isLoading: false,
            status: 'error',
        }

    case projectType.EDIT_EMPLOYEE_WEEKLY_WORKSHEET_REQUEST:
        return { ...state, status:'',isLoading: true };
    case projectType.EDIT_EMPLOYEE_WEEKLY_WORKSHEET_SUCCESS:
        return {
            ...state,
            weeklyworksheet_obj: action.payload.data,
            isLoading: false,
            status: 'edit_employee_worksheet_success',
        }

    case projectType.EDIT_EMPLOYEE_WEEKLY_WORKSHEET_FAILURE:
        return {
            ...state,
            weeklyworksheet_obj: action.payload.response,
            isLoading: false,
            status: 'error',
        }
    
    case projectType.LISTING_FIXED_WEEKLY_WORKSHEET_REQUEST:
        return { ...state, status:'',isLoading: true };
    case projectType.LISTING_FIXED_WEEKLY_WORKSHEET_SUCCESS:
        return {
            ...state,
            weeklyworksheets: action.payload.data,
            isLoading: false,
            status: 'success',
        }

    case projectType.LISTING_FIXED_WEEKLY_WORKSHEET_FAILURE:
        return {
            ...state,
            weeklyworksheets: action.payload.response,
            isLoading: false,
            status: 'error',
        }

    case projectType.PROJECT_SEARCH:
        return{
            ...state,
            isLoading: false,
            search:action.payload
        }
    case projectType.CLEAR_PROJECT_SEARCH:
        return{
            ...state,
            isLoading: false,
            search:''
        }

    default:
            return state;
    }

}