import * as BranchType from '../../constants/actionConstants';

export function createBranchReducer(state = null, action) {
    switch (action.type) { 
        case BranchType.CREATE_BRANCH_REQUEST:
            return{
                ...state,isLoading:true,
                branchName:'',
                branchCreateError:'',
                status:''
            }
        case BranchType.CREATE_BRANCH_SUCCESS:
            return {
                ...state,branchName:action.payload.data.branch_name,
                status:'success',
                isLoading:false,
                branchCreateError:''
            }

        case BranchType.CREATE_BRANCH_FAILURE:
            return {
                ...state,
                branchCreateError: action.payload.response,status:'failure',
                isLoading:false,
                branchName:'',
            };
        default:
            return state;
    }
    
}