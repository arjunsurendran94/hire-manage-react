import * as resetPasswordType from '../../constants/actionConstants';

export function resetPasswordReducer(state = null, action) {
    switch (action.type) {
        case resetPasswordType.RESET_PASSWORD_REQUEST:
            return { ...state,isLoading:true,message:'',error:'' };

        case resetPasswordType.RESET_PASSWORD_SUCCESS:

            return {
                ...state, message: action.payload.data,
                isLoading:false
            }

        case resetPasswordType.RESET_PASSWORD_FAILURE:
            return {
                ...state,
                error: action.payload.response,
                isLoading:false
            };
        default:
            return state;
    }
}