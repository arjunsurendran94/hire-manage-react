import * as departmentType from '../../constants/actionConstants';

export function deleteDepartmentReducer(state = null, action) {
    switch (action.type) {
        case departmentType.DELETE_DEPARTMENT_REQUEST:
            return{
                ...state,
                isLoading:true
            }
        case departmentType.DELETE_DEPARTMENT_SUCCESS:
            return {
                ...state,
                department_delete:true,
                isLoading:false,
                department_name:action.department_name
            }

        case departmentType.DELETE_DEPARTMENT_FAILURE:
            return {
                ...state,
                error: action.payload.response,
                isLoading:false
            };
        default:
            return state;
    }
}