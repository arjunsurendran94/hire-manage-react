import * as DepartmentType from '../../constants/actionConstants';
const initialState = {
    organization_department:[] ,
    branch_department:[] ,
    branch_department_length:[],
    isLoading:'',
    error:''
}

export default function DepartmentListReducer(state=initialState,action){
    switch(action.type){
        case DepartmentType.RETRIEVE_DEPARTMENTS_REQUEST:
            return { ...state, isLoading:true };

        case DepartmentType.RETRIEVE_DEPARTMENTS_SUCCESS:
            return {
                ...state,
                isLoading:false,
                departments:action.payload.data
            }

        case DepartmentType.RETRIEVE_ORGANIZATION_DEPARTMENTS_SUCCESS:
            return {
                ...state,
                organization_department:  state.organization_department.concat(action.payload.data),
                isLoading:false,
                org_department_length:state.organization_department.concat(action.payload.data).length,
            }
        case DepartmentType.RETRIEVE_BRANCH_DEPARTMENTS_SUCCESS:
            return {
                ...state,
                branch_department:  state.branch_department.concat(action.payload.data),
                isLoading:false,
                connections: {
                    ...state.connections,
                    [action.payload.data[0].branch]: action.payload.data.length
                  },
                branch_department_length: state.branch_department_length.concat({[action.payload.data[0].slug]:action.payload.data.length})
            }
    

        case DepartmentType.RETRIEVE_DEPARTMENTS_FAILURE:
            return {
                ...state,
                error: action.payload.response,
                isLoading:false
            };
        
        case DepartmentType.EDIT_DEPARTMENT_SUCCESS:
            return {
                ...state,
                data: action.payload.data,
                isLoading:false,
                status:'success'
            }
    
        case DepartmentType.CLEAR_DEPARTMENTS_VALUES:
            state.organization_department.forEach((value,index)=> 
            {(value.slug === action.payload)&& delete state.organization_department[index]}
            )
            state.branch_department.forEach((value,index)=> 
            {(value.slug === action.payload)&& delete state.branch_department[index]}
            )
            return {
                ...state,
                organization_department:state.organization_department ,
                branch_department:state.branch_department ,
            }

        case DepartmentType.EDIT_DEPARTMENT_FAILURE:
            return {
                ...state,
                error: action.payload.response,
                isLoading:false,
                status:'failure'
            };
            
        case DepartmentType.SEARCH_ORGANIZATION_DEPARTMENTS_SUCCESS:
            return {
                ...state,
                search_organization_department:action.organization_objs,
                isLoading:false,
                search_branch_department:action.branch_objs
            }

        case DepartmentType.SEARCH_ORGANIZATION_DEPARTMENTS_FAILURE:
            return {
                ...state,
                error:action.payload.data,
                isLoading:false,
            }
        
        case DepartmentType.RETRIEVE_ALL_DEPARTMENTS_REQUEST:
            return{
                ...state,
                isLoading:true
            }
        case DepartmentType.RETRIEVE_ALL_DEPARTMENTS_SUCCESS:
            return{
                ...state,
                isLoading:false,
                alldepartments:action.payload.data
            }
        case DepartmentType.REGISTRATION_COMPLETION_FAILURE:
            return{
                ...state,
                isLoading:false,
                error:action.payload.data
            }
        case DepartmentType.UPDATE_NEW_ADDED_DEPT_IN_DEPTLIST:
            // debugger
            return{
                ...state,
                isLoading:false,
                organization_department:state.organization_department.concat(action.payload)
            }
        case DepartmentType.UPDATE_NEW_ADDED_BRANCHDEPT_IN_DEPTLIST:
            return{
                ...state,
                isLoading:false,
                branch_department:state.branch_department.concat(action.payload)
            }
        
        default:
            return state;
    }
    
}