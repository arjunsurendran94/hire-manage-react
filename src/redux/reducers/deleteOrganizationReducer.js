import * as organizationType from '../../constants/actionConstants';

export function deleteOrganizationReducer(state = null, action) {
    switch (action.type) {
        case organizationType.DELETE_ORGANIZATION_REQUEST:
            return {
                ...state,
                isLoading:true
            }

        case organizationType.DELETE_ORGANIZATION_SUCCESS:
            return {
                ...state,
                organization_delete:true,
                isLoading:false
            }

        case organizationType.DELETE_ORGANIZATION_FAILURE:
            return {
                ...state,
                error: action.payload.response,
                org_delete_error: true,
                isLoading:false
            };
        default:
            return state;
    }
}