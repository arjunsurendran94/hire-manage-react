import * as rolesType from '../../constants/actionConstants';

export function createPermissionGroupsReducer(state = null, action) {
    switch (action.type) {
        case rolesType.CREATE_ROLES_REQUEST:
            return { ...state, isLoading:true,createdRoles:'',createdRolesSucessStatus:'',createdRolesErrorStatus:'',createdRolesError:'' };

        case rolesType.CREATE_ROLES_SUCCESS:
            return {
                ...state, 
                createdRoles: action.payload.data ,
                createdRolesSucessStatus: action.payload.status,
                createdRolesErrorStatus:'',
                isLoading:false,
                createdRolesError:''
            }

        case rolesType.CREATE_ROLES_FAILURE:
            return {
                ...state,
                createdRolesError: action.payload.response,
                createdRolesErrorStatus: action.payload.response.status,
                createdRolesSucessStatus:'',
                isLoading:false,
                createdRoles:''

            };
        default:
            return state;
    }
}