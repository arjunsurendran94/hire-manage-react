import * as organizatonType from '../../constants/actionConstants';

export default function retrieveOrganizationReducer(state=null,action){
    switch(action.type){
        case organizatonType.RETRIEVE_ORGANIZATION_REQUEST:
            return { ...state, isLoading:true };

        case organizatonType.RETRIEVE_ORGANIZATION_SUCCESS:

            return {
                ...state, organization_data: action.payload.data,
                isLoading:false
            }

        case organizatonType.RETRIEVE_ORGANIZATION_FAILURE:
            return {
                ...state,
                error: action.payload.response,
                existedMail : "isTrue",
                isLoading:false
            };
        case organizatonType.EDIT_ORGANIZATION_SUCCESS:
            return {
                ...state,organization_data: action.payload.data,
                isLoading:false,
                status:'success'
            }
        case organizatonType.EDIT_ORGANIZATION_FAILURE:
            return {
                ...state,organizationCreateError: action.payload.response,
                isLoading:false,
                status:'failure'
            }
        case organizatonType.EDIT_ORGANIZATION_CLEAR_SUCCESS:

            return {
                ...state,status:''
            };
        default:
            return state;
    }
    
}