import * as employeeType from '../../constants/actionConstants';

export function retreiveEmployeeReducer(state = null, action) {
    switch (action.type) {
        case employeeType.RETRIEVE_EMPLOYEE_REQUEST:
            return { ...state, isLoading:true };

        case employeeType.RETRIEVE_EMPLOYEE_SUCCESS:
            return {
                ...state, employeeData: action.payload.data ,
                employeeDataSucessStatus: action.payload.status,
                isLoading:false,
            }

        case employeeType.RETRIEVE_EMPLOYEE_FAILURE:
            return {
                ...state,
                employeeDataError: action.payload.response,
                employeeDataErrorStatus: action.payload.response.status
            };
        default:
            return state;
    }
}