import * as timeZone from '../../constants/actionConstants';


export function timeZoneDetails(state = null, action) {
    switch (action.type) {
        case timeZone.FETCH_TIMEZONE_REQUEST:
            return { ...state };

        case timeZone.FETCH_TIMEZONE_SUCCESS:
                // localStorage.setItem("token", action.payload.data.token);

            return {
                ...state, timeZones: action.payload.data
            }

        case timeZone.FETCH_TIMEZONE_FAILURE:
            return {
                ...state,
                error: action.payload.response
            };
        default:
            return state;
    }
}