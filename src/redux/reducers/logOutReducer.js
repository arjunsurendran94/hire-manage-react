import * as logOutType from '../../constants/actionConstants';

export function logOut(state = null, action) {
    switch (action.type) {
        case logOutType.LOGOUT_REQUEST:
            return { ...state ,isLoading:true};

        case logOutType.LOGOUT_SUCCESS:
            return {
                ...state, logoutFlag : true,isLoading:false
            }

        case logOutType.LOGOUT_FAILURE:
            return {
                ...state,
                error: action.payload.response,
                isLoading:false
            };
        default:
            return state;
    }
}