import * as organizationType from '../../constants/actionConstants';

export function createOrganizationReducer(state = null, action) {
    switch (action.type) {
        case organizationType.CREATE_ORGANIZATION_REQUEST:
            return{
                ...state,
                isLoading:true
            }
        case organizationType.CREATE_ORGANIZATION_SUCCESS:
            localStorage.setItem("organization_slug",action.payload.data.slug);
            return {
                ...state,
                organizationName:action.payload.data.organization_name,organizationSlug:action.payload.data.slug,
                status:'success',
                isLoading:false
            }

        case organizationType.CREATE_ORGANIZATION_FAILURE:
        
            return {
                ...state,
                organizationCreateError: action.payload.response,
                status:'failure',
                isLoading:false
                
            };
            
        default:
            return state;
    }
}