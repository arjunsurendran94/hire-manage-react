import * as resetPasswordType from '../../constants/actionConstants';

export function resetPasswordEmailConfirmReducer(state = null, action) {
    switch (action.type) {
        case resetPasswordType.CONFIRM_PASSWORD_EMAIL_VERIFY_REQUEST:
            return { ...state,isLoading:true };

        case resetPasswordType.CONFIRM_PASSWORD_EMAIL_VERIFY_SUCCESS:

            return {
                ...state, email: action.payload.data.email,
                isLoading:false,
                status:'success'
            }

        case resetPasswordType.CONFIRM_PASSWORD_EMAIL_VERIFY_FAILURE:
            return {
                ...state,
                error: action.payload.response,
                isLoading:false,
                status:'error'
            };
        default:
            return state;
    }
}