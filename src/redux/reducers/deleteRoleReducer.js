import * as roleType from '../../constants/actionConstants';

export function deleteRoleReducer(state = null, action) {
    switch (action.type) {
        case roleType.DELETE_ROLE_REQUEST:
            return {
                ...state,
                isLoading:true
            }

        case roleType.DELETE_ROLE_SUCCESS:
            return {
                ...state,
                role_delete:true,
                isLoading:false
            }

        case roleType.DELETE_ROLE_FAILURE:
            return {
                ...state,
                error: action.payload.response,
                role_delete_error: true,
                isLoading:false
            };
        default:
            return state;
    }
}