import * as citiesType from '../../constants/actionConstants';

export function cityReducer(state = null, action) {
    switch (action.type) {
        // case citiesType.FETCH_CITIES_REQUEST:
        //     return { ...state,isLoading:true };

        case citiesType.FETCH_CITIES_SUCCESS:
                // localStorage.setItem("token", action.payload.data.token);

            return {
                ...state, cityList: action.payload.data
            }

        case citiesType.FETCH_CITIES_FAILURE:
            return {
                ...state,cityList:[],
                error: action.payload.response
            };
        default:
            return state;
    }
}