import * as signupType from '../../constants/actionConstants';

export function signupReducer(state = null, action) {
    switch (action.type) {
        case signupType.ADD_USER_REQUEST:
            return { ...state, isLoading:true };

        case signupType.ADD_USER_SUCCESS:

            return {
                ...state, message: action.payload.data,
                isLoading:false
            }

        case signupType.ADD_USER_FAILURE:
            return {
                ...state,
                error: action.payload.response,
                existedMail : "isTrue",
                isLoading:false
            };
        default:
            return state;
    }
}