import * as clientType from '../../constants/actionConstants';

export function clientReducer(state = null, action) {
    switch (action.type) {
        case clientType.ADD_CLIENT_REQUEST:
            return { ...state, client_obj: '', status:'',isLoading: true };

        case clientType.ADD_CLIENT_SUCCESS:
            return {
                ...state,
                client_obj: action.payload.data,
                clients:state.clients.concat(action.payload.data),
                isLoading: false,
                status: 'success',
            }

        case clientType.ADD_CLIENT_FAILURE:
            return {
                ...state,
                client_obj: action.payload.response,
                isLoading: false,
                status: 'error'
            };
        case clientType.LIST_CLIENTS_REQUEST:
            return { ...state, isLoading: true };

        case clientType.LIST_CLIENTS_SUCCESS:
            return {
                ...state,
                clients: action.payload.data,
                isLoading: false,
                clients_listing_status: 'success',
                status: ''
            }

        case clientType.LIST_CLIENTS_FAILURE:
            return {
                ...state,
                clients: action.payload.response,
                isLoading: false,
                clients_listing_status: 'error',
                status: ''

            };
        case clientType.CLIENT_SEARCH:
            return{
                ...state,
                isLoading: false,
                search:action.payload
            }
        case clientType.CLEAR_CLIENT_SEARCH:
            return{
                ...state,
                isLoading: false,
                search:''
            }

        case clientType.RETRIEVE_CLIENT_REQUEST:
            return { ...state, isLoading: true };

        case clientType.RETRIEVE_CLIENTS_SUCCESS:
            return {
                ...state,
                client_obj: action.payload.data,
                isLoading: false,
                clients_listing_status: '',
                retrivestatus: 'success'
            }

        case clientType.RETRIEVE_CLIENTS_FAILURE:
            return {
                ...state,
                clients: action.payload.response,
                isLoading: false,
                clients_listing_status: '',
                retrivestatus: 'error'

            };
        case clientType.SET_CLIENT_PORTAL_PASSWORD:

            return {
                ...state,
                portal_password: action.payload,
                isLoading: false,
                portal_password_status: 'success',
                client_obj: ''
            }
        case clientType.CLEAR_PORTAL_PASSWORD_STATUS:
            return {
                ...state,
                isLoading: false,
                portal_password_status: '',
                client_obj: ''
            }
        case clientType.LIST_CLIENT_COMMENTS_REQUEST:
            return { ...state, isLoading: true };
        case clientType.LIST_CLIENT_COMMENTS_SUCCESS:
            return {
                ...state,
                isLoading: false,
                comment_list: action.payload.data,
                comment_list_status: action.payload.status
            }
        case clientType.LIST_CLIENT_COMMENTS_FAILURE:
            return {
                ...state,
                isLoading: false,
                comment_list: action.payload.response,
                comment_list_status: action.payload.status
            }
        case clientType.ADD_CLIENT_COMMENT_REQUEST:
            return { ...state, isLoading: true };
        case clientType.ADD_CLIENT_COMMENT_SUCCESS:
            let comment_objs = [action.payload.data, ...state.comment_list]
            return {
                ...state,
                isLoading: false,
                comment: action.payload.data,
                comment_status: action.payload.status,
                comment_list: comment_objs
            }
        case clientType.ADD_CLIENT_COMMENT_FAILURE:
            return {
                ...state,
                isLoading: false,
                comment: action.payload.response,
                comment_status: action.payload.status,
            }
        case clientType.EDIT_CLIENT_BASIC_DETAILS_REQUEST:
            return { ...state, client_edit: '', client_editerror_obj: '', isLoading: true };
        case clientType.EDIT_CLIENT_BASIC_DETAILS_SUCCESS:
            return {
                ...state,
                isLoading: false,
                client_obj: action.payload.data,
                client_edit: 'success',
                clients: state.clients.map((obj) => obj.client.slug === action.payload.data.client.slug ? action.payload.data : obj)
            }
        case clientType.EDIT_CLIENT_BASIC_DETAILS_FAILURE:

            return {
                ...state,
                isLoading: false,
                client_edit: 'error',
                client_editerror_obj: action.payload.response
            }
        case clientType.EDIT_CLIENT_OTHER_DETAILS_REQUEST:
            return { ...state, client_edit: '', client_editerror_obj: '', isLoading: true };
        case clientType.EDIT_CLIENT_OTHER_DETAILS_SUCCESS:

            let a = { ...state.client_obj }
            a['otherdetails'] = action.payload.data
            return {
                ...state,
                isLoading: false,
                client_obj: a,
                client_edit: 'success',
            }
        case clientType.EDIT_CLIENT_OTHER_DETAILS_FAILURE:

            return {
                ...state,
                isLoading: false,
                client_edit: 'error',
                client_editerror_obj: action.payload.response
            }
        case clientType.EDIT_CLIENT_ADDRESS_DETAILS_REQUEST:
            return { ...state, client_edit: '', client_editerror_obj: '', isLoading: true };
        case clientType.EDIT_CLIENT_ADDRESS_DETAILS_SUCCESS:
            let address = { ...state.client_obj }
            address['billing_address'] = action.payload.data
            return {
                ...state,
                isLoading: false,
                client_obj: address,
                client_edit: 'success',
            }
        case clientType.EDIT_CLIENT_ADDRESS_DETAILS_FAILURE:

            return {
                ...state,
                isLoading: false,
                client_edit: 'error',
                client_editerror_obj: action.payload.response
            }
        case clientType.EDIT_CLIENT_REMARK_DETAILS_REQUEST:
            return { ...state, client_edit: '', client_editerror_obj: '', isLoading: true };
        case clientType.EDIT_CLIENT_REMARK_DETAILS_SUCCESS:
            let remark = { ...state.client_obj }
            remark['remark'] = action.payload.data
            return {
                ...state,
                isLoading: false,
                client_obj: remark,
                client_edit: 'success',
            }
        case clientType.EDIT_CLIENT_REMARK_DETAILS_FAILURE:

            return {
                ...state,
                isLoading: false,
                client_edit: 'error',
                client_editerror_obj: action.payload.response
            }
        case clientType.EDIT_CLIENT_CONTACT_PERSON_DETAILS_REQUEST:
            return { ...state, client_edit: '', client_editerror_obj: '', isLoading: true };
        case clientType.EDIT_CLIENT_CONTACT_PERSON_DETAILS_SUCCESS:
            let fullobj = { ...state.client_obj }
            fullobj['contact_persons'] = action.payload.data
            return {
                ...state,
                isLoading: false,
                client_obj: fullobj,
                client_edit: 'success',
            }
        case clientType.EDIT_CLIENT_CONTACT_PERSON_DETAILS_FAILURE:

            return {
                ...state,
                isLoading: false,
                client_edit: 'error',
                client_editerror_obj: action.payload.response
            }
        
        default:
            return state;
    }

}