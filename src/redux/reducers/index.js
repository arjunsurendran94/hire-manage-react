import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form'
import {emailVerificationReducer} from './emailVerificationReducer'
import {signupReducer} from './signupReducer'
import {loginReducer} from './loginReducer'
import {countryReducer} from './countryReducer'
import {stateReducer} from './stateReducer'
import {cityReducer} from './cityReducer'
import {organzationList} from './listOrganizationReducer'
import {resetPasswordReducer} from './resetPasswordReducer'
import {changePasswordReducer} from './changePasswordReducer'
import {createOrganizationReducer} from './createOrganizationReducer'
import {registraionCompleteReducer} from './registrationCompleteReducer'
import {createBranchReducer} from './createBranchReducer'
import {branchList} from './listBranchReducer'
import {resetPasswordEmailConfirmReducer} from './resetPasswordEmailConfirmReducer'
import retrieveOrganizationReducer from './retrieveOrganizationReducer'
import DepartmentListReducer from './listDepartmentsReducer'
import {profileDetail} from './getProfileReducer'
import {timeZoneDetails} from './getTimeZoneReducer'
import {editProfileDetails} from './editProfileReducer'
import {logOut} from './logOutReducer'
import {createDepartmentReducer} from './createDepartmentReducer'
import {deleteDepartmentReducer} from './deleteDepartmentReducer'
import {deleteBranchReducer} from './deleteBranchReducer'
import {deleteOrganizationReducer} from './deleteOrganizationReducer'
import {fetchPermissionGroupsReducer} from './listRolesReducer'
import {createPermissionGroupsReducer} from './createRolesReducer'
import {retreivePermissionGroupsReducer} from './retreiveRoleReducer'
import {createDesignationReducer} from './createDesignationReducer'
import {deleteRoleReducer} from './deleteRoleReducer'
import {editPermissionGroupsReducer} from './editRoleReducer'
import {addEmployeeReducer} from './addEmployeeReducer'
import {retreiveEmployeeReducer} from './retreiveEmployeeReducer'
import {employeeEmailVerificationReducer} from './employeeEmailVerificationReducer'
import {employeePermissionReducer} from './listEmployeePermissionReducer'
import {clientReducer} from './clientReducer'
import {projectReducer} from './projectReducer'
import {taskReducer} from './taskReducer'

const appReducer = combineReducers({
    form: formReducer,
    userSignup:signupReducer,
    userLogin:loginReducer,
    emailVerificationData:emailVerificationReducer,
    countryData:countryReducer,
    stateData:stateReducer,
    cityData:cityReducer,
    organzationList:organzationList,
    resetPassword:resetPasswordReducer,
    changePassword:changePasswordReducer,
    registrationCompletion:registraionCompleteReducer,
    createOrganization:createOrganizationReducer,
    createBranch:createBranchReducer,
    branchList:branchList,
    resetEmailVerification:resetPasswordEmailConfirmReducer,
    retrieveOrganization:retrieveOrganizationReducer,
    departmentList:DepartmentListReducer,
    profileDetail:profileDetail,
    timeZoneDetails:timeZoneDetails,
    editProfileDetails:editProfileDetails,
    logOut:logOut,
    createDepartment:createDepartmentReducer,
    deleteDepartment:deleteDepartmentReducer,
    deleteBranch:deleteBranchReducer,
    deleteOrganization:deleteOrganizationReducer,
    rolesData:fetchPermissionGroupsReducer,
    rolesCreated:createPermissionGroupsReducer,
    role:retreivePermissionGroupsReducer,
    DesignationData:createDesignationReducer,
    roleDelete:deleteRoleReducer,
    roleEdit:editPermissionGroupsReducer,
    employees:addEmployeeReducer,
    employeeData:retreiveEmployeeReducer,
    employeeVerification:employeeEmailVerificationReducer,
    employeeRoles:employeePermissionReducer,
    clientReducer:clientReducer,
    projectReducer:projectReducer,
    taskReducer:taskReducer

})

const rootReducer = (state, action) => {
    if (action.type === 'USER_LOGOUT') {
      state = undefined
    }
    return appReducer(state, action)
  }
export default rootReducer