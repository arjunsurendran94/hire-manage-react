import * as rolesType from '../../constants/actionConstants';

export function retreivePermissionGroupsReducer(state = null, action) {
    switch (action.type) {
        case rolesType.RETRIEVE_ROLES_REQUEST:
            return { ...state, isLoading:true };

        case rolesType.RETRIEVE_ROLES_SUCCESS:
            return {
                ...state, roleData: action.payload.data ,
                roleDataSucessStatus: action.payload.status,
                isLoading:false,
            }

        case rolesType.RETRIEVE_ROLES_FAILURE:
            return {
                ...state,
                roleDataError: action.payload.response,
                roleDataErrorStatus: action.payload.response.status
            };
        default:
            return state;
    }
}