import * as regType from '../../constants/actionConstants';

export function registraionCompleteReducer(state = null, action) {
    switch (action.type) {
        case regType.REGISTRATION_COMPLETION_REQUEST:
            return { ...state ,isLoading:true};

        case regType.REGISTRATION_COMPLETION_SUCCESS:

            return {
                ...state, registrationCompleted: action.payload.data.profile_slug,
                isLoading:false
            }

        case regType.REGISTRATION_COMPLETION_FAILURE:
            return {
                ...state,
                error: action.payload.response,
                isLoading:false
            };
        default:
            return state;
    }
}