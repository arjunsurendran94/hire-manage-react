import * as employeeType from '../../constants/actionConstants';


export function employeePermissionReducer(state = null, action) {
    switch (action.type) {
        case employeeType.LIST_EMPLOYEE_PERMISSION_REQUEST:
            return { ...state,isLoading:true };

        case employeeType.LIST_EMPLOYEE_PERMISSION_SUCCESS:
                // localStorage.setItem("token", action.payload.data.token);

            return {
                ...state, employeePermissions: action.payload.data,isLoading:false
            }

        case employeeType.LIST_EMPLOYEE_PERMISSION_FAILURE:
            return {
                ...state,
                error: action.payload.response,
                isLoading:false
            };

        case employeeType.UPDATE_EMPLOYEE_PERMISSION_REQUEST:
            return { ...state,isLoading:true };

        case employeeType.UPDATE_EMPLOYEE_PERMISSION_SUCCESS:
                // localStorage.setItem("token", action.payload.data.token);

            return {
                ...state, employeePermissionStatus:action.payload.statusText,employeePermissions: action.payload.data,isLoading:false
            }

        case employeeType.UPDATE_EMPLOYEE_PERMISSION_FAILURE:
            return {
                ...state,
                error: action.payload.response,
                isLoading:false
            };
        default:
            return state;
    }
}