import * as countryType from '../../constants/actionConstants';

export function countryReducer(state = null, action) {
    switch (action.type) {
        // case countryType.FETCH_COUNTRY_REQUEST:
        //     return { ...state,isLoading:true };

        case countryType.FETCH_COUNTRY_SUCCESS:
            return {
                ...state, 
                countryList: action.payload.data,
            }

        case countryType.FETCH_COUNTRY_FAILURE:
            return {
                ...state,
                error: action.payload.response
            };
        default:
            return state;
    }
}