import * as statesType from '../../constants/actionConstants';

export function stateReducer(state = null, action) {
    switch (action.type) {
        case statesType.FETCH_STATES_REQUEST:
            return { ...state };

        case statesType.FETCH_STATES_SUCCESS:
                // localStorage.setItem("token", action.payload.data.token);

            return {
                ...state, stateList: action.payload.data
            }

        case statesType.FETCH_STATES_FAILURE:
            return {
                ...state,stateList:[],
                error: action.payload.response
            };
        default:
            return state;
    }
}