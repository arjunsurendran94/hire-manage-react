import * as rolesType from '../../constants/actionConstants';

export function fetchPermissionGroupsReducer(state = null, action) {
    switch (action.type) {
        case rolesType.FETCH_ROLES_REQUEST:
            return { ...state, isLoading:true };

        case rolesType.FETCH_ROLES_SUCCESS:
            return {
                ...state, roles: action.payload.data ,
                rolesSucessStatus: action.payload.status,
                isLoading:false,
            }

        case rolesType.FETCH_ROLES_FAILURE:
            return {
                ...state,
                rolesError: action.payload.response,
                rolesErrorStatus: action.payload.response.status
            };
        
        case rolesType.ROLES_SEARCH:
            return {
                ...state, 
                roles: state.roles,
                search: action.payload,
                isLoading:false,
            }
        
        case rolesType.CLEAR_SEARCH:
            return{
                
                roles:state.roles,
                isLoading:false
            }
        
        case rolesType.LISTING_ALL_PERMISSIONS_REQUEST:
            return { ...state, status:'',isLoading:true };

        case rolesType.LISTING_ALL_PERMISSIONS_SUCCESS:
            
            return {
                ...state, 
                allPermissions: action.payload.data,                
                isLoading:false,
                allPermissionsStatus:action.payload.status
            }

        case rolesType.FETCH_ROLES_FAILURE:
            return {
                ...state,
                allPermissions: action.payload.response,                
                isLoading:false,
                allPermissionsStatus:action.payload.status
            };


        default:
            return state;
    }
}