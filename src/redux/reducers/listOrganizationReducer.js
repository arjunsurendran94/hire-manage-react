import * as organizatonType from '../../constants/actionConstants';

export function organzationList(state = null, action) {
    switch (action.type) {
        case organizatonType.FETCH_ORGANIZATION_REQUEST:
            return { ...state, isLoading:true };

        case organizatonType.FETCH_ORGANIZATION_SUCCESS:
            return {
                ...state, data: action.payload.data ,
                statusCode: action.payload.status,
                isLoading:false,
            }

        case organizatonType.FETCH_ORGANIZATION_FAILURE:
            return {
                ...state,
                error: action.payload.response,
                statusCode: action.payload.response.status
            };
        default:
            return state;
    }
}