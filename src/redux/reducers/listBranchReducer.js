import * as branchType from '../../constants/actionConstants';

export function branchList(state = null, action) {
    
    switch (action.type) {
        case branchType.FETCH_BRANCH_REQUEST:
            return { ...state, isLoading:true };

        case branchType.FETCH_BRANCH_SUCCESS:

            return {
                ...state, data: action.payload.data,isLoading:false,
            }

        case branchType.FETCH_BRANCH_FAILURE:
            return {
                ...state,
                error: action.payload.response,
                isLoading:false
            };
        
        case branchType.EDIT_BRANCH_REQUEST:
                return {
                    ...state,
                    isLoading:true
                }
        
        case branchType.EDIT_BRANCH_SUCCESS:
            return {
                ...state,
                data: action.payload.data,
                status:'success',
                isLoading:false
            }
        
        case branchType.EDIT_BRANCH_FAILURE:
            return {
                ...state,
                error: action.payload.response,
                status:'failure',
                isLoading:false
            };
        
        case branchType.SEARCH_BRANCH_REQUEST:
            return {
                ...state,
                isLoading:true
            }
        
        case branchType.SEARCH_BRANCH_SUCCESS:
            return {
                ...state,
                data: action.payload.data,
                status:'success',
                isLoading:false
            }
        
        case branchType.SEARCH_BRANCH_FAILURE:
            return {
                ...state,
                error: action.payload.response,
                status:'failure',
                isLoading:false
            };
        
        default:
            return state;
    }
}