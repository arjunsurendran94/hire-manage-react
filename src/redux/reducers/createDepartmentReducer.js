import * as DepartmentType from '../../constants/actionConstants';

export  function createDepartmentReducer(state = null, action) {
    switch (action.type) {
        case DepartmentType.CREATE_DEPARTMENT_REQUEST:
            return { ...state, isLoading:true };

        case DepartmentType.CREATE_DEPARTMENT_SUCCESS:
            return {
                ...state,
                department:action.payload.data,
                isLoading:false,
                status:'success'
            }

        case DepartmentType.CREATE_DEPARTMENT_FAILURE:
            return {
                ...state,
                departmentCreateError: action.payload.response,
                isLoading:false,
                status:'error'
            };
        case DepartmentType.DEPARTMENTS_CLEAR_SUCCESS_STATUS:
            return{
                ...state,
                status:''
            }
        default:
            return state;
    }
    
}