import * as profile from '../../constants/actionConstants';

export function profileDetail(state = null, action) {
    switch (action.type) {
        case profile.GET_PROFILE_REQUEST:
            return { ...state,isLoading:true };

        case profile.GET_PROFILE_SUCCESS:

            return {
                ...state, 
                profileDetail: action.payload.data,
                isLoading:false
            }

        case profile.GET_PROFILE_FAILURE:
            return {
                ...state,
                error: action.payload.response,
                isLoading:false
            };
        default:
            return state;
    }
}