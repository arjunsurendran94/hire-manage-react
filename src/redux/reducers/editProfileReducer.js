import * as editProfileType from '../../constants/actionConstants';

export function editProfileDetails(state = null, action) {
    switch (action.type) {
        case editProfileType.EDIT_PROFILE_REQUEST:
            return { ...state,isLoading:true };

        case editProfileType.EDIT_PROFILE_SUCCESS:
            return {
                ...state, profileEditFlag: true, data:action.payload,isLoading:false
            }

        case editProfileType.EDIT_PROFILE_FAILURE:
            
            return {
                ...state,
                error: action.payload,profileEditFlag: false,
                isLoading:false
            };
        
        case editProfileType.CLEAR_PROFILE_ERRORS:
            return{
                ...state,error:''
            }
        default:
            return state;
    }
}