import * as DesignationType from '../../constants/actionConstants';

export  function createDesignationReducer(state = null, action) {
    switch (action.type) {
        case DesignationType.CREATE_DESIGNATION_REQUEST:
            return { ...state, isLoading:true };

        case DesignationType.CREATE_DESIGNATION_SUCCESS:
            return {
                ...state,
                designation_obj:action.payload.data,
                isLoading:false,
                createstatus:'success',
                designation:state.designation.concat(action.payload.data)
            }

        case DesignationType.CREATE_DESIGNATION_FAILURE:
            return {
                ...state,
                designationError: action.payload.response,
                isLoading:false,
                createstatus:'error'
            };
        
        case DesignationType.RETRIEVE_DESIGNATION_REQUEST:
            return { ...state, isLoading:true };

        case DesignationType.RETRIEVE_DESIGNATION_SUCCESS:
            return {
                ...state,
                designation:action.payload.data,
                isLoading:false,
                status:'success'
            }

        case DesignationType.RETRIEVE_DESIGNATION_FAILURE:
            return {
                ...state,
                designationError: action.payload.response,
                isLoading:false,
                status:'error'
            };
        
        case DesignationType.DESIGNATION_DETAIL_REQUEST:
            return { ...state, isLoading:true };

        case DesignationType.DESIGNATION_DETAIL_SUCCESS:
            return {
                ...state,
                desgn:action.payload.data,
                isLoading:false,
                status:'success'
            }

        case DesignationType.DESIGNATION_DETAIL_FAILURE:
            return {
                ...state,
                desgnError: action.payload.response,
                isLoading:false,
                status:'error'
            };
        
        case DesignationType.DESIGNATION_UPDATE_REQUEST:
            return { ...state, isLoading:true };

        case DesignationType.DESIGNATION_UPDATE_SUCCESS:
            return {
                ...state,
                desgn:action.payload.data,
                isLoading:false,
                updatestatus:'success'
            }

        case DesignationType.DESIGNATION_UPDATE_FAILURE:
            
            return {
                ...state,
                designationError: action.payload.response,
                isLoading:false,
                updatestatus:'error'
            };

        case DesignationType.DESIGNATION_DELETE_REQUEST:
            return { ...state, isLoading:true };

        case DesignationType.DESIGNATION_DELETE_SUCCESS:
            return {
                ...state,
                designationDelete:action.payload.data,
                isLoading:false,
                deletestatus:'success'
            }

        case DesignationType.DESIGNATION_DELETE_FAILURE:
            
            return {
                ...state,
                designationDelete: action.payload.response,
                isLoading:false,
                deletestatus:'failure'
            };

        case DesignationType.DESIGNATION_SORT_REQUEST:
            return { ...state, isLoading:true };
        
        case DesignationType.DESIGNATION_SORT_SUCCESS:
            
            return {
                ...state,
                designation:action.payload.data,
                isLoading:false,
                status:'success'
            }
        
        case DesignationType.DESIGNATION_SORT_FAILURE:
            
            return {
                ...state,
                designationError: action.payload.response,
                isLoading:false,
                status:'error'
            };
        
        case DesignationType.DESIGNATION_SEARCH:
            
            return {
                ...state,
                designation:state.designation,
                search:action.payload,
                isLoading:false,
            }
        
        case DesignationType.DESIGNATION_CLEAR_SEARCH:
            return {
                designation:state.designation,
                isLoading:false,    
            }

        case DesignationType.DESIGNATION_CLEAR_MESSAGES:
            return{
                isLoading:false,
                createstatus:'',
                updatestatus:''

            }
        
        case DesignationType.ADD_NEW_DESIGANTION_FROM_MODAL_REQUEST:
            return { ...state, isLoading:true };
        
        case DesignationType.ADD_NEW_DESIGANTION_FROM_MODAL_SUCCESS:
            
            return{
                ...state,
                design_obj:action.payload.data,
                isLoading:false,
                modalstatus:'success',
                designation:state.designation.concat(action.payload.data)
            }
        case DesignationType.ADD_NEW_DESIGANTION_FROM_MODAL_FAILURE:
            return {
                ...state,
                design_obj_Error: action.payload.response,
                isLoading:false,
                modalstatus:'error'
            }
        case DesignationType.CLEAR_MODAL_SUCCESS:
            return{
                ...state,
                isLoading:false,
                modalstatus:''
            }
            
        default:
            return state;
    }
    
}