import * as changePasswordType from '../../constants/actionConstants';

export function changePasswordReducer(state = null, action) {
    switch (action.type) {
        case changePasswordType.CHANGE_PASSWORD_REQUEST:
            return { ...state ,isLoading:true};

        case changePasswordType.CHANGE_PASSWORD_SUCCESS:

            return {
                ...state, passwordChange: action.payload.data.status,isLoading:false
            }

        case changePasswordType.CHANGE_PASSWORD_FAILURE:
            return {
                ...state,
                error: action.payload.response,
                isLoading:false
            };
        default:
            return state;
    }
}