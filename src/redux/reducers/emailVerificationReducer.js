import * as emailType from '../../constants/actionConstants';

export function emailVerificationReducer(state = null, action) {
    switch (action.type) {
        case emailType.EMAIL_VERIFY_REQUEST:
            return { ...state,isLoading:true };

        case emailType.EMAIL_VERIFY_SUCCESS:

            return {
                ...state, message: action.payload.data,
                isLoading:false
            }

        case emailType.EMAIL_VERIFY_FAILURE:
            return {
                ...state,
                error: action.payload,
                isLoading:false
            };
        default:
            return state;
    }
}