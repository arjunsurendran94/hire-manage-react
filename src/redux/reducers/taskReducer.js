import * as taskType from '../../constants/actionConstants';

export function taskReducer(state = null, action) {
    switch (action.type) {
        case taskType.ADD_TASK_REQUEST:
            return { ...state, status: '', isLoading: true };

        case taskType.ADD_TASK_SUCCESS:
            // debugger
            return {
                ...state,
                task_obj: action.payload.data,
                tasks: state.tasks.concat(action.payload.data),
                mytasks:(action.payload.data?.assigned?.slug===localStorage.getItem('admin_employee_slug')?
                state.mytasks.concat(action.payload.data):[...state.mytasks]
                ),
                isLoading: false,
                status: 'create_task_success',
            }

        case taskType.ADD_TASK_FAILURE:
            // debugger
            return {
                ...state,
                error_obj: action.payload.response,
                isLoading: false,
                status: action.payload.response.status,
            }

        case taskType.LISTING_TASK_REQUEST:
            return { ...state, status: '', isLoading: true };

        case taskType.LISTING_TASK_SUCCESS:
            return {
                ...state,
                tasks: action.payload.data,
                // tasks:state.tasks.concat(action.payload.data),
                isLoading: false,
                status: 'list_task_success',
            }

        case taskType.LISTING_TASK_FAILURE:
            return {
                ...state,
                error_obj: action.payload.response,
                isLoading: false,
                status: action?.payload?.response?.status,
            }

        case taskType.LISTING_MYTASK_REQUEST:
            return { ...state, status: '', isLoading: true };

        case taskType.LISTING_MYTASK_SUCCESS:
            return {
                ...state,
                mytasks: action.payload.data,
                isLoading: false,
                status: 'list_task_success',
            }

        case taskType.LISTING_MYTASK_FAILURE:

            return {
                ...state,
                error_obj: action.payload.response,
                isLoading: false,
                status: action.payload.response.status,
            }

        case taskType.ADD_SUBTASK_REQUEST:
            return { ...state, status: '', isLoading: true };

        case taskType.ADD_SUBTASK_SUCCESS:

            return {
                ...state,
                subtask_obj: action.payload.data,
                // tasks:state.tasks.concat(action.payload.data),
                isLoading: false,
                status: 'create_subtask_success',
            }

        case taskType.ADD_SUBTASK_FAILURE:
            return {
                ...state,
                error_obj: action.payload.response,
                isLoading: false,
                status: action.payload.response.status,
            }

        case taskType.LISTING_SUBTASK_REQUEST:
            return { ...state, status: '', isLoading: true };

        case taskType.LISTING_SUBTASK_SUCCESS:
            return {
                ...state,
                subtasks: action.payload.data,
                // tasks:state.tasks.concat(action.payload.data),
                isLoading: false,
                status: 'list_subtask_success',
            }

        case taskType.LISTING_SUBTASK_FAILURE:
            return {
                ...state,
                error_obj: action.payload.response,
                isLoading: false,
                status: action.payload.response.status,
            }


        case taskType.EDIT_TASK_REQUEST:
            return { ...state, status: '', isLoading: true };

        case taskType.EDIT_TASK_SUCCESS:
            // debugger
            let index = state.tasks.findIndex(i => i.slug === action.payload.data.slug)

            return {
                ...state,
                task_obj: action.payload.data,
                tasks: state.tasks.map(obj => (obj.slug === action.payload.data.slug) ? action.payload.data : obj),
                // mytasks:(action.payload.data?.assigned?.slug===localStorage.getItem('admin_employee_slug')?
                // state.mytasks.concat(action.payload.data):[...state.mytasks]
                // ),
                isLoading: false,
                status: 'edit_task_success',
            }


        case taskType.EDIT_TASK_FAILURE:
            return {
                ...state,
                error_obj: action.payload.response,
                isLoading: false,
                status: action.payload.response.status,
            }


        case taskType.EDIT_SUBTASK_REQUEST:
            return { ...state, status: '', isLoading: true };

        case taskType.EDIT_SUBTASK_SUCCESS:
            // debugger

            return {
                ...state,
                subtask_obj: action.payload.data,
                subtasks: state.subtasks.map(obj => (obj.slug === action.payload.data.slug) ? action.payload.data : obj),
                isLoading: false,
                status: 'edit_subtask_success',
            }


        case taskType.EDIT_SUBTASK_FAILURE:
            return {
                ...state,
                error_obj: action.payload.response,
                isLoading: false,
                status: action.payload.response.status,
            }


        case taskType.LISTING_TASK_LOG_REQUEST:
            return { ...state, status: '', isLoading: true };

        case taskType.LISTING_TASK_LOG_SUCCESS:
            // debugger

            return {
                ...state,
                tasklog: action.payload.data,
                isLoading: false,
                status: 'listing_tasklog_success',
            }

        case taskType.LISTING_TASK_LOG_FAILURE:
            return {
                ...state,
                error_obj: action.payload.response,
                isLoading: false,
                status: action.payload.response.status,
            }

        case taskType.START_TIME_REQUEST:
            return { ...state, status: '', isLoading: true };

        case taskType.START_TIME_SUCCESS:

            return {
                ...state,
                timeObj: action.payload.data,
                isLoading: false,
                status: 'start_time_success',
            }

        case taskType.START_TIME_FAILURE:
            return {
                ...state,
                error_obj: action.payload.response,
                isLoading: false,
                status: action.payload.response.status,
            }


        case taskType.LIST_TIME_LOG_REQUEST:
            return { ...state, status: '', isLoading: true };

        case taskType.LIST_TIME_LOG_SUCCESS:

            return {
                ...state,
                timelog: action.payload.data,
                isLoading: false,
                status: 'list_timelog_success',
            }

        case taskType.LIST_TIME_LOG_FAILURE:
            return {
                ...state,
                error_obj: action.payload.response,
                isLoading: false,
                status: action.payload.response.status,
            }

        case taskType.END_TIME_REQUEST:
            return { ...state, status: '', isLoading: true };

        case taskType.END_TIME_SUCCESS:

            return {
                ...state,
                timeObj: action.payload.data,
                isLoading: false,
                status: 'end_time_success',
            }

        case taskType.END_TIME_FAILURE:
            return {
                ...state,
                error_obj: action.payload.response,
                isLoading: false,
                status: action.payload.response.status,
            }
        
            case taskType.ADD_TIME_REQUEST:
                return { ...state, status: '', isLoading: true };
    
            case taskType.ADD_TIME_SUCCESS:
                
                return {
                    ...state,
                    timeObj: action.payload.data,  
                    timelog:state.timelog.concat(action.payload?.data?.response),                  
                    isLoading: false,
                    status: 'add_time_success',
                }
    
            case taskType.ADD_TIME_FAILURE:
                return {
                    ...state,
                    error_obj: action.payload.response,
                    isLoading: false,
                    status: action.payload.response.status,
                }
        
        
            case taskType.EDIT_TIME_REQUEST:
                return { ...state, status: '', isLoading: true };
    
            case taskType.EDIT_TIME_SUCCESS:
                
                return {
                    ...state,
                    timeObj: action.payload.data,
                    timelog:state.timelog.map(obj=>(obj.slug===action.payload.data.response.slug)?
                                                action.payload.data.response:obj),
                    isLoading: false,
                    status: 'edit_time_success',
                }
    
            case taskType.EDIT_TIME_FAILURE:
                
                return {
                    ...state,
                    error_obj: action.payload.response,
                    isLoading: false,
                    status: action?.payload?.response?.status,
                }


            case taskType.LISTING_SUBTASK_LOG_REQUEST:
                return { ...state, status: '', isLoading: true };
    
            case taskType.LISTING_SUBTASK_LOG_SUCCESS:
    
                return {
                    ...state,
                    subTaskLog: action.payload.data,
                    isLoading: false,
                    status: 'listing_subtasklog_success',
                }
    
            case taskType.LISTING_SUBTASK_LOG_FAILURE:
                return {
                    ...state,
                    error_obj: action.payload.response,
                    isLoading: false,
                    status: action.payload.response.status,
                }

            case taskType.ADD_TASK_COMMENT_REQUEST:
                return { ...state, status: '', isLoading: true };
    
            case taskType.ADD_TASK_COMMENT_SUCCESS:
    
                return {
                    ...state,
                    taskCommentObj: action.payload.data,
                    taskComments:state.taskComments.concat(action.payload.data),
                    isLoading: false,
                    status: 'add_task_comment_success',
                }
    
            case taskType.ADD_TASK_COMMENT_FAILURE:
                return {
                    ...state,
                    error_obj: action.payload.response,
                    isLoading: false,
                    status: action.payload.response.status,
                }
            
            case taskType.LISTING_TASK_COMMENTS_REQUEST:
                return { ...state, status: '', isLoading: true };
    
            case taskType.LISTING_TASK_COMMENTS_SUCCESS:
    
                return {
                    ...state,
                    taskComments: action.payload.data,
                    isLoading: false,
                    status: 'listing_task_comment_success',
                }
    
            case taskType.LISTING_TASK_COMMENTS_FAILURE:
                return {
                    ...state,
                    error_obj: action.payload.response,
                    isLoading: false,
                    status: action.payload.response.status,
                }
        default:
            return state;
    }

}