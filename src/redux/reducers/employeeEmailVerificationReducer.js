import * as employeeType from '../../constants/actionConstants';

export function employeeEmailVerificationReducer(state = null, action) {
    switch (action.type) {
        case employeeType.EMPLOYEE_EMAIL_VERIFY_REQUEST:
            return { ...state,isLoading:true };

        case employeeType.EMPLOYEE_EMAIL_VERIFY_SUCCESS:

            return {
                ...state, employeeVerify: action.payload.data,
                isLoading:false
            }

        case employeeType.EMPLOYEE_EMAIL_VERIFY_FAILURE:
            return {
                ...state,
                employeeVerifyError: action.payload,
                isLoading:false
            };
        default:
            return state;
    }
}