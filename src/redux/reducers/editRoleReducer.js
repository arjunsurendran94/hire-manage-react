import * as rolesType from '../../constants/actionConstants';

export function editPermissionGroupsReducer(state = null, action) {
    switch (action.type) {
        case rolesType.EDIT_ROLE_REQUEST:
            return { ...state, isLoading:true };

        case rolesType.EDIT_ROLE_SUCCESS:
            return {
                ...state, editRole: action.payload.data ,
                editRolesSucessStatus: action.payload.status,
                isLoading:false,
            }

        case rolesType.EDIT_ROLE_FAILURE:
            return {
                ...state,
                editRoleError: action.payload.response,
                editRoleErrorStatus: action.payload.response.status
            };
        default:
            return state;
    }
}