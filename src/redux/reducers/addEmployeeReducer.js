import * as employeeType from '../../constants/actionConstants';

const initialState = {
    allEmployees:[] ,
    createdEmployee:'' ,
    isLoading:'',
    error:'',
    employee_delete:false,
    employee_name:'',
    employeeStatusChange:'',
    employeeStatus:'',
    statusChangeError:'',
    createdError:'',
    createdErrorStatus:'',
    employeeUpdate:'',
    employeeUpdateStatus:'',
    employeeUpdateError:''

}

export function addEmployeeReducer(state=initialState, action) {
    switch (action.type) {
        case employeeType.ADD_EMPLOYEE_REQUEST:
            return { ...state ,isLoading:true,createdEmployee:'',createdError: '',createdEmployeeStatus:'',
            createdErrorStatus:''
        };

        case employeeType.ADD_EMPLOYEE_SUCCESS:

            return {
                ...state, 
                createdEmployee: action.payload.data,
                createdEmployeeStatus:action.payload.status,
                isLoading:false
            }

        case employeeType.ADD_EMPLOYEE_FAILURE:
            return {
                ...state,
                createdError: action.payload.response.data,
                isLoading:false,
                createdErrorStatus: action.payload.response.status,
            };

        case employeeType.LIST_EMPLOYEE_REQUEST:
            return {
                ...state,
                isLoading:true
            }
        case employeeType.LIST_EMPLOYEE_SUCCESS:
            return {
                ...state,
                isLoading:false,
                allEmployees:  action.payload.data,
            }
        case employeeType.LIST_EMPLOYEE_FAILURE:
                return {
                    ...state,
                    error: action.payload.response,
                    isLoading:false
                };
        
        case employeeType.SEARCH_FILTER_EMPLOYEE_REQUEST:
            return {
                ...state,
                isLoading:true
            }
        case employeeType.SEARCH_FILTER_EMPLOYEE_SUCCESS:
            // debugger
            return {
                ...state,
                allEmployees:  action.payload.data,
                isLoading:false
            }
        case employeeType.SEARCH_FILTER_EMPLOYEE_FAILURE:
            return {
                ...state,
                error: action.payload.response,
                isLoading:false
            }

        case employeeType.DELETE_EMPLOYEE_REQUEST:
            return{
                ...state,
                isLoading:true
            }
        case employeeType.DELETE_EMPLOYEE_SUCCESS:
            return {
                ...state,
                employee_delete:true,
                isLoading:false,
                employee_name:action.department_name
            }

        case employeeType.DELETE_EMPLOYEE_FAILURE:
            return {
                ...state,
                error: action.payload.response,
                isLoading:false
            };

        case employeeType.EMPLOYEE_STATUS_VERIFY_REQUEST:
                return{
                    ...state,
                    isLoading:true
                }
        case employeeType.EMPLOYEE_STATUS_VERIFY_SUCCESS:
            return {
                ...state,
                employeeStatusChange:true,
                isLoading:false,
                employeeStatus:action.payload.data
            }
    
        case employeeType.EMPLOYEE_STATUS_VERIFY_FAILURE:
            return {
                ...state,
                statusChangeError: action.payload.response,
                isLoading:false
            };
            
        case employeeType.UPDATE_EMPLOYEE_REQUEST:
            return{
                ...state,
                isLoading:true
            }
        case employeeType.UPDATE_EMPLOYEE_SUCCESS:
            return {
                ...state,
                employeeUpdate:action.payload.data,
                isLoading:false,
                employeeUpdateStatus:action.payload.status
            }
    
        case employeeType.UPDATE_EMPLOYEE_FAILURE:
            return {
                ...state,
                employeeUpdate: false,
                isLoading:false,
                employeeUpdateError:action.payload.response
            };

            case employeeType.LOGGED_IN_USER_PERMISSIONS_REQUEST:
                return{
                    ...state,
                    isLoading:true,
                    status:''
                }
            case employeeType.LOGGED_IN_USER_PERMISSIONS_SUCCESS:
                return {
                    ...state,
                    employeePermissions:action.payload.data,
                    isLoading:false,
                    status:'permission_listing_success'
                }
        
            case employeeType.LOGGED_IN_USER_PERMISSIONS_FAILURE:
                return {
                    ...state,
                    employeePermissions: action.payload.response,
                    isLoading:false,
                    status:'permission_listing_error'
                };
        default:
            return state;
    }
}