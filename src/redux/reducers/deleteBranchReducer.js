import * as branchType from '../../constants/actionConstants';

export function deleteBranchReducer(state = null, action) {
    switch (action.type) {
        case branchType.DELETE_BRANCH_REQUEST:
            return{
                ...state,
                isLoading:true
            }
        case branchType.DELETE_BRANCH_SUCCESS:
            return {
                ...state,
                branch_delete:true,
                isLoading:false,
                branch_name:action.branch_name
            }

        case branchType.DELETE_BRANCH_FAILURE:
            return {
                ...state,
                error: action.payload.response,
                branch_delete_error: true,
                isLoading:false
            };
        default:
            return state;
    }
}