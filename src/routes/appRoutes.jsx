import React, {Component} from 'react';
import {BrowserRouter as Router,Switch, Route,Redirect} from 'react-router-dom';
import HomeContainer from '../containers/home/HomeContainer'
import LoginContainer from '../containers/registration/loginContainer'
import SignupContainer from '../containers/registration/signupContainer'
import ConfirmSignupContainer from '../containers/registration/confirmSignupContainer'
import ForgotPasswordContainer from '../containers/registration/forgotPasswordContainer'
import CreateOrganizationContainer from '../containers/organization/createOrganizationContainer';
import CompleteResetPassword from '../containers/registration/changePasswordContainer';
import SideBar from '../components/dashboard/common/sideBar'
import DashBoardHeader from '../components/dashboard/common/dashboardHeader'
import ProfleContainer from '../containers/profile/profileContainer'

import DashBoardContainer from '../containers/dashboard/dashboardContainer'
import OrganizationContainer from '../containers/dashboard/organizationContainer'
import OrganizationSettingsContainer from '../containers/dashboard/organizationSettingContainer'
import OrganizationDepartmentContainer from '../containers/dashboard/organizationDepartmentConatiner'
import OrganizationBranchContainer from '../containers/dashboard/organizationBranchContainer'
import BranchDetailComponent from '../components/dashboard/common/branchDetailComponent'
import OrganizationAddNewDepartmentContainer from '../containers/dashboard/organizationNewDepartmentContainer'
import OrganizationAddNewBranchContainer from '../containers/dashboard/organizationNewBranchContainer'
import ClientAddNewClientContainer from '../components/dashboard/addClient'
import TermsConditions from '../components/registration/termsConditions'
import OrganizationEmployeesContainer from '../containers/dashboard/organizationEmployeesContainer'
import OrganizationRolesContainer from '../containers/dashboard/organizationRolesContainers'
import OrganizationDesignationContainer from '../containers/dashboard/organizationDesignationContainer'
import OrganizationAddDesignationContainer from '../containers/dashboard/organizationAddDesignationContainer'
import OrganizationAddRolesContainer from '../containers/dashboard/organizationAddRolesContainer'
import OrganizationRoleDetailContainer from '../containers/dashboard/organizationRoleDetailContainer'
import OrganizationDesignationDetailContainer from '../containers/dashboard/organizationDesignationDetailComponent'
import OrganizationRoleUpdateContainer from '../containers/dashboard/organizationRoleUpdateContainer'
import AddEmployeeContainer from '../containers/dashboard/addEmployeeContainer'

import AddClientContainer from '../containers/dashboard/addClientContainer'
import AddProjectContainer from '../containers/dashboard/addProjectContainer'
import OrganizationEmployeeDetailContainer from '../containers/dashboard/organizationEmployeeDetailContainer'
import HistoryContainer from '../containers/dashboard/historyContainer'
import ErrorBoundry from '../errorHandlerComponent';
import ClientContainer from '../containers/dashboard/clientsListingContainer'
import ProjectContainer from '../containers/dashboard/projectListingContainer'
import ClientDetailContainer from '../containers/dashboard/clientDetailContainer'
import ProjectDetailContainer from '../containers/dashboard/projectDetailContainer'
import TaskDetailContainer from'../containers/dashboard/taskDetailContainer'
import PageNotFound from '../components/dashboard/common/pageNotFound.jsx'

class AppRoute extends Component {

    is_admin=()=>{
      // console.log('user_type',localStorage.getItem('user_type'));
      return localStorage.getItem('user_type') === 'admin'
    }

    render() {
      let permissions=[]
      permissions=localStorage.getItem("permissions")?.split(',')
      let is_admin=localStorage.getItem('user_type') === 'admin'
      // console.log('user_type',localStorage.getItem('user_type'));
      // console.log('perms',permissions?.includes('view_project'))
      
      
      return (
          <Router>
            <Switch>
            <ErrorBoundry >

              <Route exact path="/" component={HomeContainer}/>
              <Route path="/login" component={LoginContainer} />
              <Route path='/signup' component={SignupContainer}/>
              {/* <Route path="/create-organization" component={CreateOrganizationContainer} /> */}
              <Route path='/confirm-signup/:id/:token' component={ConfirmSignupContainer}/>
              <Route path='/accounts/password-reset/' component={ForgotPasswordContainer}/>
              <Route path='/accounts/complete-password/:id/:token' component={CompleteResetPassword}/>
              <Route path='/accounts/profile/' component={ProfleContainer}/>
              <Route exact path="/create-organization" render={() => (!localStorage.token? (<Redirect to="/"/>) : (<CreateOrganizationContainer />))}/>

              <Route path='/settings' component={BranchDetailComponent} />

              <Route path='/sidebar' component={SideBar}/>
              <Route path='/dashheader' component={DashBoardHeader}/>

              <Route path='/dashboard' component={DashBoardContainer}/>


              {/* <Route path='/dashboard' render={() => ((this.t())?  (<DashBoardContainer />): (<Redirect to="/pagenotfound"/>) ) }/> */}



              <Route path='/organization/organization/' render={() => ((this.is_admin())?  (<OrganizationContainer />): (<Redirect to="/pagenotfound"/>) ) } />
              <Route path='/organization/settings/:id/' render={(props) => ((this.is_admin())?  (<OrganizationSettingsContainer {...props}/>): (<Redirect to="/pagenotfound"/>) ) }  />
              <Route path='/departments/departments/' render={() => ((this.is_admin())?  (<OrganizationDepartmentContainer />): (<Redirect to="/pagenotfound"/>) ) } />
              <Route path='/organization/branches/' render={() => ((this.is_admin())?  (<OrganizationBranchContainer />): (<Redirect to="/pagenotfound"/>) ) } />
              <Route path='/departments/add-new-department/' render={() => ((this.is_admin())?  (<OrganizationAddNewDepartmentContainer />): (<Redirect to="/pagenotfound"/>) ) } />
              <Route path='/organization/add-new-branch/' render={() => ((this.is_admin())?  (<OrganizationAddNewBranchContainer />): (<Redirect to="/pagenotfound"/>) ) } />

              <Route path='/terms-of-service/' render={(props) => <TermsConditions {...props} tos={true} />}/>
              <Route path='/user-aggrement/' render={(props) => <TermsConditions {...props} ua={true} />}/>
              <Route path='/privacy-policy/' render={(props) => <TermsConditions {...props} pp={true} />}/>

              <Route path='/employees/employees/' render={() => ((this.is_admin())?  (<OrganizationEmployeesContainer />): (<Redirect to="/pagenotfound"/>) ) } />

              <Route path='/permission-groups/' render={() => ((this.is_admin())?  (<OrganizationRolesContainer />): (<Redirect to="/pagenotfound"/>) ) } />
              <Route path='/departments/designations/' render={(props) => ((this.is_admin())?  (<OrganizationDesignationContainer {...props} />): (<Redirect to="/pagenotfound"/>) ) } />
             
              <Route path='/departments/add-designation/' render={() => ((this.is_admin())?  (<OrganizationAddDesignationContainer />): (<Redirect to="/pagenotfound"/>) ) } />
              <Route path='/departments/designation/:designation_slug' render={(props) => ((this.is_admin())?  (<OrganizationDesignationDetailContainer {...props} />): (<Redirect to="/pagenotfound"/>) ) }/>
              <Route path='/departments/update-designation/:designation_slug' render={(props) => ((this.is_admin())?<OrganizationAddDesignationContainer {...props} update={true}/>:(<Redirect to="/pagenotfound"/>))}/>


              <Route path='/add-permission-group/' render={() => ((this.is_admin())?  (<OrganizationAddRolesContainer />): (<Redirect to="/pagenotfound"/>) )} />
              <Route path='/permission-detail/:role_slug/' render={(props) => ((this.is_admin())?  (<OrganizationRoleDetailContainer {...props}/>): (<Redirect to="/pagenotfound"/>) )} />
              <Route path='/permission-update/:role_slug/' render={(props) => ((this.is_admin())?  (<OrganizationRoleUpdateContainer {...props} />): (<Redirect to="/pagenotfound"/>) )} />
              <Route path='/employees/create-employee/' render={() => ((this.is_admin())?  (<AddEmployeeContainer />): (<Redirect to="/pagenotfound"/>) )} />

              <Route path='/employees/set-employee-password/:id/:token' render={(props) => <CompleteResetPassword {...props} employeePassword={true}/>}/>
              
              <Route path='/employees/employee/:employee_slug' render={(props) => ((this.is_admin())?  (<OrganizationEmployeeDetailContainer  {...props}/>): (<Redirect to="/pagenotfound"/>) )} />
              <Route path='/history/' component={HistoryContainer}/>
              <Route path='/clients/clients/' render={(props) => ((permissions?.includes('view_client')||this.is_admin())?  (<ClientContainer {...props}/>): (<Redirect to="/pagenotfound"/>) )} />
              <Route path='/clients/client/:client_slug' render={(props) => ((permissions?.includes('view_client')||this.is_admin())?  (<ClientDetailContainer {...props}/>): (<Redirect to="/pagenotfound"/>) )} />
              {/* <Route path='/clients/add-new-client/' component={ClientAddNewClientContainer}/> */}
              <Route path='/clients/add-new-client/' render={(props) => ((permissions?.includes('add_client')||this.is_admin())?  (<AddClientContainer {...props}/>): (<Redirect to="pagenotfound/"/>) )} />
              <Route path='/groups/' component={HistoryContainer}/>     
              
              <Route path='/projects/projects/' component={ProjectContainer}/>
              <Route path='/projects/add-new-project/' component={AddProjectContainer}/>
              <Route path='/projects/project/:project_slug' component={ProjectDetailContainer}/>

              {/* <Route path='/projects/projects/' render={(props) => ((permissions?.includes('view_project')||(this.is_admin()))?  (<ProjectContainer {...props}/>): (<Redirect to="/pagenotfound"/>) )} />
              <Route path='/projects/add-new-project/' render={(props) => ((permissions?.includes('add_project')||this.is_admin())?  (<AddProjectContainer {...props}/>): (<Redirect to="/pagenotfound"/>) )} />
              <Route path='/projects/project/:project_slug' render={(props) => ((permissions?.includes('view_project')||this.is_admin())?  (<ProjectDetailContainer {...props}/>): (<Redirect to="/pagenotfound"/>) )} />
               */}
              <Route path='/projects/task/:task_slug' component={TaskDetailContainer}/>
              <Route path='/reports/' component={HistoryContainer}/>
              <Route path='/pagenotfound' component={PageNotFound}/>

              </ErrorBoundry >

            </Switch>
          </Router>
      );
    }
}
export default AppRoute;