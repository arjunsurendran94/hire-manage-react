import React ,{ Component } from 'react';
import { Link } from 'react-router-dom';


export default class Header extends Component{
    

    render(){
        return(
        <div className="pull-left fullwidth">
            <div className="header-wrapper fullwidth pull-left pt-4 pb-4 bg-white">
                <div className="container">
                    <div className="logo pull-left">
                        <Link to= '/'>
                        <img className="img-fluid" src={require("../../static/assets/image/logo.png") } alt="logo"/>
                        </Link>
                    </div>
                    
                    { this.props.login ? 
                    <div className="pull-right hdr-rght">
                        <Link to='/login'
                         href="" className="text-uppercase mar-right color log-fnt-16">
                             log in
                         </Link>                        
                        <Link to= "/signup" 
                        className="text-uppercase color log-fnt-16">sign up
                        </Link>
                    </div> 
                    : null
                    }
                
                    

                </div>
            </div>
            {
                this.props.showsubheader ? 
                <div className="menu-wrapper fullwidth pull-left">
                    <div className="container">
                        <ul className="fullwidth text-center m-0">
                            <li className="d-inlin li-list"><Link className="log-fnt-16">Home</Link></li>
                            <li className="d-inlin li-list"><Link className="log-fnt-16">About Us</Link></li>
                            <li className="d-inlin li-list"><Link className="log-fnt-16">Services</Link></li>
                            <li className="d-inlin li-list"><Link className="log-fnt-16">Contact Us</Link></li>
                            <li className="li-list dis-none"><Link className="log-fnt-16">LOG IN</Link></li>
                            <li className="li-list dis-none"><Link className="log-fnt-16">SIGN UP</Link></li>
                        </ul>
                    </div>
                </div>
                : null
            }
            

        </div>
        )
    }
}


