import React ,{ Component, Fragment } from 'react';


export default class ErrorMessage extends Component{

    render(){
        return(
            <Fragment>
                <div className="alert alert-dismissible fade show alert-danger mt-3">
                         <i className="icon-warning-triangle alert-icon mr-2"></i> 
                         {this.props.msg}
                        <button  onClick ={this.props.msg!=='No projects found'? this.props.closeMessage:null} type="button" className="close" data-dismiss="alert" aria-label="Close">
                        {this.props.msg!=='No projects found'? <span aria-hidden="true">×</span>:null}
                        </button>
                </div>
            </Fragment>
        )
    }
}



