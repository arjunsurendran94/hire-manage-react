import React ,{ Component, Fragment } from 'react';


export default class SuccessMessage extends Component{

    render(){
        return(
            <Fragment>
                <div className="alert alert-dismissible fade show alert-warning-green mt-3">
                         <i className="icon-checked b-6 alert-icon mr-2"></i> 
                          {this.props.msg} 
                        <button type="button" className="close" onClick={this.props.closemsg} data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">×</span>
                        </button>
                </div>
            </Fragment>
        )
    }
}



