import React ,{ Component, Fragment } from 'react';
import LoaderImage from '../../static/assets/image/loader.gif';


export default class Loader extends Component{

    render(){
        return(
            <Fragment>
            <div className="loader-img"> <img src={LoaderImage} className="mx-auto d-block center loader-w-mt" alt="Leap" /></div>
            </Fragment>
        )
    }
}