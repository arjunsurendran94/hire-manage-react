import React, { Component, Fragment } from 'react';
import { Field, reduxForm } from 'redux-form';
import { Link } from 'react-router-dom';
import {countryReorder} from '../../countryReorder'

// const  { DOM: { input, select, textarea } } = React

const required = value => {
  return (value || typeof value === 'number' ? undefined : ' is Required')
}

var letters = /^[-a-zA-Z0-9-()]+(\s+[-a-zA-Z0-9-()]+)*$/;
const alphabetOnly =  value => {
    return(value && value.match(letters) ? undefined : ' must be characters only')
}

const maxLength = max => value =>{
  return  value && value.length > max ? `Must be ${max} characters or less` : undefined}
const maxLength200 = maxLength(200)

const renderField = ({
  input,
  label,
  type,
  existed,
  meta: { touched, error, warning,pristine }
}) => {

  return (
    

    <Fragment>
      <input {...input} placeholder={label} type={type} className={(touched && error) || (existed === 'The fields organization, branch_name must make a unique set.') ? 'input-control input-control border border-danger' : 'input-control input-control'} />
      
      {touched &&
        ((error && <small className="text-danger">{label}{error}</small>) ||
          (warning && <span>{warning}</span>))}
          
      {/* frommmmmmm herreeeeeeeeee */}
      { (touched) && (existed === 'The fields organization, branch_name must make a unique set.'
        ) && <small class="text-danger">
        Branch Name already exists
      </small>

    }

    </Fragment>

  )
}

const renderFieldText = ({ input, label, name, type,rows,cols, textarea, meta: { touched, error, warning, invalid } }) => {
  const textareaType = <textarea {...input} rows={rows} cols={cols} placeholder={label}  type={type} className={`form-control ${touched && invalid ? 'has-danger' : ''}`}/>;
  // const inputType = <input {...input} placeholder={label}  type={type} className={`form-control ${touched && invalid ? 'has-danger' : ''}`}/>;
  return (
      <div>
          <div>
              { textareaType}
              {touched && ((error && <small className="text-danger">{label} {error}</small>) || (warning && <span>{warning}</span>))}
          </div>
      </div>
  );
};

const renderFieldSelect = ({
  input,
  label,
  country,
  countryState,
  city,
  className,
  meta: { touched, error, warning, }
}) => {

  return (


    <Fragment>
      <select {...input} className={className}>
        <option value="">{label}</option>
        {(country && label === "Country") &&
          <Fragment>
            {country.countryList.map((plan, index) => (
              <option key={index} value={plan.id}>{plan.name_ascii}</option>
            ))}</Fragment>
        }
        {(countryState && label === "State") &&
          <Fragment>
            {countryState.stateList.map((plan, index) => (
              <option key={index} value={plan.id}>{plan.name_ascii}</option>
            ))}</Fragment>
        }
        {(city && label === "City") &&
          <Fragment>
            {city.cityList.map((plan, index) => (
              <option key={index} value={plan.id}>{plan.name_ascii}</option>
            ))}</Fragment>
        }
      </select>
      <div>
        {touched &&
          ((error && <small className="text-danger">{label}{error}</small>) ||
            (warning && <span>{warning}</span>))}
      </div>
      {/* {((existed)&&(existed.error.status == 500)) && <small class="text-danger">
          {existed.errors.error}
      </small>} */}

    </Fragment>

  )
}


class CreateBranch extends Component {
  state = {
    existed_error:'',
    show_branch_form:false
  };

  componentDidMount() {
    
    this.props.retrieveOrganization()
    
    this.props.fetchCountryList();
    this.props.fetchBranchList(localStorage.getItem('organization_slug'),localStorage.getItem('token'))
  }
  
  handleCountryChange = (e) => {

    // making fields null
    this.props.change('state','')  
    this.props.change('city','')

    this.props.fetchStatesList(e.target.value);
    this.props.fetchCityList("");
    
}
handleStateChange = (e) => {

    // making fields null
    this.props.change('city','')
    
    this.props.fetchCityList(e.target.value);
}
  showHideBranchForm = (val) => {
    
    
    if(val ==='submit'|| val ==='add branch') {
    // document.getElementById('branch-form').classList.toggle("d-block")
    // document.getElementById('add-branch-btn').classList.toggle('d-none')
    // document.getElementById('footer-button').classList.toggle('d-none')
    this.setState({show_branch_form:!this.state.show_branch_form})
    }    // save btn click
    
    if (val === 'cancel'){
      this.setState({show_branch_form:false,existed_error:''},()=> this.props.reset())
         
    }
  }

  submit = (values) => {
    values.organization = this.props.organizationCreatedData&&this.props.organizationCreatedData.organization_data && this.props.organizationCreatedData.organization_data.slug
    // this.showHideBranchForm("submit")
    this.props.createBranch(values)
   

  }
  // finish = () =>  {
  //   return <Redirect to='/dashboard'/>
  // }

  Capitalize(str){
    return str.charAt(0).toUpperCase() + str.slice(1);
    }
  
  componentDidUpdate(prevProps){

    

    if((prevProps.branchCreatedData!=this.props.branchCreatedData)){
      
      this.props.branchCreatedData.status==='failure'&&
      this.props.branchCreatedData&&this.props.branchCreatedData.branchCreateError&&
      this.props.branchCreatedData.branchCreateError.data&&this.props.branchCreatedData.branchCreateError.data.non_field_errors&&
      this.props.branchCreatedData.branchCreateError.data.non_field_errors[0]==='The fields organization, branch_name must make a unique set.'&&
      this.setState({
        existed_error:this.props.branchCreatedData.branchCreateError.data.non_field_errors[0]
      },()=>this.showHideBranchForm('error')
      )
      if(this.props.branchCreatedData.status==='success'){
        this.showHideBranchForm('submit')

      }

    }
  }

  branchExistedError =() =>{
    this.setState({existed_error:''})
  }

  render() {

    console.log('org',this.props.organizationCreatedData)


    const { handleSubmit} = this.props
    return (

      <div className="container-fluid create-wrapper">

        <div className="row full-height justify-content-center">
          <div className="col-sm-11 col-md-6 mt-5 mb-4">
            <img src={require("../../static/assets/image/logo.png")} alt="logo"className="brand-img" />
            <div className="col-sm-12">
              <div className="bx-content row shadow mt-4">
                <div className="tab-content col-sm-12" id="nav-tabContent">
                  <div className="tab-pane fade show active" id="nav-registration" role="tabpanel" aria-labelledby="nav-createOrganisation-tab">

                    <div className="row">
                      <div className="col-sm-12 col-md-8 mx-auto pt-5 pb-5 pr-4 pl-4">

                        <h5>{this.props.organizationCreatedData&&this.props.organizationCreatedData.organization_data && this.props.organizationCreatedData.organization_data.organization_name && this.Capitalize(this.props.organizationCreatedData.organization_data.organization_name)}</h5>
                        
                        {(this.props.branchList&&this.props.branchList.data&&this.props.branchList.data.length>0)&&
                        this.props.branchList.data.map((branch,index) => (<h6 key={index}>- {this.Capitalize(branch.branch_name)}</h6>))}
                        
                        {!this.state.show_branch_form&&<button id="add-branch-btn" className="btn btn-white mb-2 add-branch-button" onClick={()=>this.showHideBranchForm("add branch")} ><i className="icon-plus" style={{ fontSize: "10px" }}></i> Add Branch</button>}
                        
                        {this.state.show_branch_form &&
                        <form onSubmit={handleSubmit(this.submit)} method="post" id="branch-form" action="" noValidate>

                          <h5 className="border-left border-dark border-bold pl-3 mb-4">Add Branch</h5>

                          <div className="form-group">
                            <label htmlFor="">Branch Name <span className="text-danger">*</span></label>
                            <Field type="text" 
                            name="branch_name" 
                            component={renderField}
                             validate={[required,alphabetOnly]} 
                             existed={this.state.existed_error}
                             onChange={this.branchExistedError}
                             label="Branch Name" 
                             id="id_branch_name" />
                          </div>

                          <div className="row">
                            <div className="col-sm-12 col-md-4 responsive-grid-4">
                              <div className="form-group">

                                <label htmlFor="id_country">Country <span className="text-danger">*</span></label>
                                <Field
                                  name="country"
                                  type="select"
                                  country={countryReorder(this.props.countryDetails)}
                                  component={renderFieldSelect}
                                  className="input-control select-country"
                                  label="Country"
                                  validate={required}
                                  onChange={this.handleCountryChange} />



                              </div>
                            </div>

                            <div className="col-sm-12 col-md-4 responsive-grid-4">
                              <div className="form-group">
                                <label htmlFor="id_state">State <span className="text-danger">*</span></label>
                                <Field
                                  name="state"
                                  type="select"
                                  countryState={this.props.stateDetails}
                                  component={renderFieldSelect}
                                  className="input-control select-state"
                                  label="State"
                                  validate={required}
                                  onChange={this.handleStateChange}
                                />


                              </div>
                            </div>

                            <div className="col-sm-12 col-md-4 responsive-grid-4">
                              <div className="form-group">
                                <label htmlFor="id_city">City <span className="text-danger">*</span></label>
                                <Field
                                  name="city"
                                  type="select"
                                  city={this.props.cityDetails}
                                  component={renderFieldSelect}
                                  className="input-control select-city"
                                  label="City"
                                  validate={required} />


                              </div>
                            </div>
                          </div>

                          <div className="form-group">
                            <label htmlFor="id_address">Address</label>
                            <Field name="address" validate={[maxLength200]} type="text" className="input-control input-control" cols="40" rows="10" component={renderFieldText} label=" Address" id="id_address" />
                          </div>

                          <div className="form-group">
                            <button id="cancel-btn" onClick={()=>this.showHideBranchForm("cancel")} className="btn btn-default btn-light border" type="button">
                              Cancel</button>

                            <button className="btn btn-primary float-right" type="submit">
                              Save
                              </button>
                          </div>

                        </form>
                        }


                      </div>
                    </div>
                    {/*  */}
                    
                    {!this.state.show_branch_form&&
                    <div id="footer-button" className="row mt-3 mb-4" >
                      <div className="col-sm-6 "></div>
                      <div className="col-sm-6 text-right">
                          <input className="d-none" type="checkbox" name="submit" value="checked" checked="" />
                          <Link className="text-white" to = '/dashboard'>
                          <button  className="btn btn-primary mb-2" style={{ minWidth: "150px" }} value="Finish" >Finish
                          </button>
                          </Link>
                      </div>
                    </div>}


                    {/*  */}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div >

      </div >

    )
  }
}
CreateBranch = reduxForm({
  form: 'branchform'
})(CreateBranch)
export default CreateBranch