import React, { Component, Fragment } from 'react';
import { Field, reduxForm } from 'redux-form'
import CreateBranch from '../../containers/organization/createBranchContainer'
import Loader from '../common/Loader'
import {countryReorder} from '../../countryReorder'



const required = value => {
    return (value || typeof value === 'number' ? undefined : ' is Required')
}

const maxLength = max => value =>{
   return  value && value.length > max ? `Must be ${max} characters or less` : undefined}
const maxLength15 = maxLength(15)
const maxLength200 = maxLength(200)

var letters = /^[-a-zA-Z0-9-()]+(\s+[-a-zA-Z0-9-()]+)*$/;
const alphabetOnly =  value => {
    return(value && value.match(letters) ? undefined : ' must be characters only')
}

const websiteValiadtion = value => {
    if (value) {
        var pattern = new RegExp('^(https?:\\/\\/)' + // protocol
            '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
            '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
            '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
            '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
            '(\\#[-a-z\\d_]*)?$', 'i'); // fragment locator
        return !!pattern.test(value) ? undefined : 'Enter a valid url ( eg:https://websitebuilders.com )'
    }
}
const renderField = ({
    input,
    label,
    type,
    existed,
    backend_error,
    id,
    className,
    accept,
    meta: { touched, error, warning, }
}) => {

    return (


        <Fragment>
            {console.log('err',existed)}
            <input {...input} placeholder={label} type={type} className={className} id={id} accept={accept} />
            {touched &&
                ((error && <small className="text-danger">{label}{error}</small>) ||
                    (warning && <span>{warning}</span>))}
            {/* {((existed) && (existed.existedMail)) && <small class="text-danger">
                {existed.error.data.user.email[0]} */}
            {/* </small>} */}
            {((existed)&&(existed.organizationCreateError)&&(existed.organizationCreateError.data)&&(existed.organizationCreateError.data.non_field_errors)) && <small class="text-danger">
            {existed.organizationCreateError.data.non_field_errors[0]}
        </small>}
        {backend_error&&<small className="text-danger">{backend_error} (eg:https://websitebuilders.com) </small>}
        
        </Fragment>

    )
}

const renderFieldText = ({ input, label, name, type,rows,cols, textarea, meta: { touched, error, warning, invalid } }) => {
    const textareaType = <textarea {...input} rows={rows} cols={cols} placeholder={label}  type={type} className={`form-control ${touched && invalid ? 'has-danger' : ''}`}/>;
    // const inputType = <input {...input} placeholder={label}  type={type} className={`form-control ${touched && invalid ? 'has-danger' : ''}`}/>;
    return (
        <div>
            <div>
                { textareaType}
                {touched && ((error && <small className="text-danger">{label} {error}</small>) || (warning && <span>{warning}</span>))}
            </div>
        </div>
    );
};

const renderFieldSelect = ({
    input,
    label,
    country,
    countryState,
    city,
    className,
    meta: { touched, error, warning, }
}) => {
    return (

        

        <Fragment>
            <select {...input} className={className}>

                <option value="">{label}</option>
                {(country && label === "Country") &&
                    <Fragment>
                        {country.countryList.map((plan, index) => (
                            <option key={index} value={plan.id}>{plan.name_ascii}</option>
                        ))}</Fragment>
                }
                {(countryState && label === "State") &&
                    <Fragment>
                        {countryState&&countryState.stateList&&countryState.stateList.map((plan, index) => (
                            <option key={index} value={plan.id}>{plan.name_ascii}</option>
                        ))}</Fragment>
                }
                {(city && label === "City") &&
                    <Fragment>
                        {city&&city.cityList.map((plan, index) => (
                            <option key={index} value={plan.id}>{plan.name_ascii}</option>
                        ))}</Fragment>
                }
            </select>
            <div>
                {touched &&
                    ((error && <small className="text-danger">{label}{error}</small>) ||
                        (warning && <span>{warning}</span>))}
            </div>
            {/* {((existed)&&(existed.error.status == 500)) && <small class="text-danger">
            {existed.errors.error}
        </small>} */}

        </Fragment>

    )
}

class CreateOrganization extends Component {
    state={
        image:'',
        image_validation_error:false,
        cityData:''
    }
    componentDidMount() {
            this.props.fetchCountryList();

    }

    // Error handling
    
    // componentDidUpdate(prevProps){
    //     if((prevProps.organizationCreatedData &&this.props.organizationCreatedData&&
    //         prevProps.organizationCreatedData.status !== this.props.organizationCreatedData.status)&&
    //     this.props.organizationCreatedData.status === "failure"
    //     ){
    //     throw 'Something went wrong ...';
    // }
    // }


    handleCountryChange = (e) => {
        
        // making fields null
        this.props.change('state','')  
        this.props.change('city','')

        this.props.fetchStatesList(e.target.value);
        this.props.fetchCityList(" ");
    }

    handleStateChange = (e) => {
        // making fields null
        this.props.change('city','')

        this.props.fetchCityList(e.target.value)
    }

    submit = (values) => {
        values.logo = this.state.image
        values.owner = localStorage.getItem("profile_slug")
        const form_data = new FormData();
        for ( var key in values ) {
            form_data.append(key, values[key]);
        }
        this.props.createOrganization(form_data)
    }
    upLoadFile =(e)=>{
        let image = e.target.files[0]
        var imageTypes = ['image/png','image/jpeg','image/gif']
        var image_index = imageTypes.indexOf(image.type)
        if (image_index >= 0){
            this.setState({image:image,image_validation_error:false},() =>{console.log(this.state.image,'uploaded image')})
        }
        else{
            this.setState({image:"",image_validation_error:true})
        }
    }
    render() {
        console.log('pp',this.props.organizationCreatedData)
        console.log('local storage',localStorage.getItem('organization_slug'))
        const { handleSubmit} = this.props
        if(this.props.organizationCreatedData && (this.props.organizationCreatedData.isLoading)){
            return <Loader/>;    
        }
        return (
            ((this.props.organizationCreatedData&&this.props.organizationCreatedData.status==='success')||
            localStorage.getItem('organization_slug')!==null) ?
            <CreateBranch />:
            <div className="container-fluid create-wrapper">
                <div className="row full-height justify-content-center">
                    <div className="col-sm-11 col-md-6 mt-5 mb-4">
                        <img alt="logo" className="brand-img" src={require("../../static/assets/image/logo.png")} />
                        <div className="col-sm-12">
                            <div className="bx-content row shadow mt-4">
                                
                                <div className="tab-content col-sm-12" id="nav-tabContent">
                                    <div className="tab-pane fade show active" id="nav-registration" role="tabpanel" aria-labelledby="nav-createOrganisation-tab">

                                        <form onSubmit={handleSubmit(this.submit)} noValidate>

                                            <div className="col-sm-12 col-md-8 mx-auto pt-5 pb-5 pr-4 pl-4">
                                                <h5 className="border-left border-dark border-bold pl-3 mb-4">
                                                    Add Organization
                                    </h5>

                                                <div className="form-group">

                                                    <label htmlFor="id_organization_name">Organization Name <span className="text-danger">*</span></label>


                                                    <Field type="text" name="organization_name" component={renderField}
                                                        label="Organization Name" validate={[required,alphabetOnly]} existed={this.props.organizationCreatedData} className="input-control input-control" />

                                                </div>

                                                <div className="form-group">
                                                    <label htmlFor="id_description">Description</label>
                                                    <Field type="text" cols="40" rows="10" name="description" component={renderFieldText} className="input-control input-control"
                                                        label="Description" validate={[maxLength200]}/>
                                                </div>

                                                <div className="row">
                                                    <div className="col-sm-12 col-md-4 responsive-grid-4">
                                                        <div className="form-group">

                                                            <label htmlFor="id_country">Country <span className="text-danger">*</span></label>
                                                            <Field
                                                                name="country"
                                                                type="select"
                                                                country={countryReorder(this.props.countryDetails)}
                                                                component={renderFieldSelect}
                                                                className="input-control select-country"
                                                                label="Country"
                                                                validate={required}
                                                                onChange={this.handleCountryChange} />



                                                        </div>
                                                    </div>

                                                    <div className="col-sm-12 col-md-4 responsive-grid-4">
                                                        <div className="form-group">
                                                            <label htmlFor="id_state">State <span className="text-danger">*</span></label>
                                                            
                                                            
                                                            <Field
                                                                name="state"
                                                                type="select"
                                                                countryState={this.props.stateDetails}
                                                                component={renderFieldSelect}
                                                                className="input-control select-state"
                                                                label="State"
                                                                validate={required}
                                                                onChange={this.handleStateChange}
                                                            />
                                                            


                                                        </div>
                                                    </div>

                                                    <div className="col-sm-12 col-md-4 responsive-grid-4">
                                                        <div className="form-group">
                                                            <label htmlFor="id_city">City <span className="text-danger">*</span></label>
                                                            <Field
                                                                name="city"
                                                                type="select"
                                                                city={this.props.cityDetails}
                                                                component={renderFieldSelect}
                                                                className="input-control select-city"
                                                                label="City"
                                                                validate={required} />


                                                        </div>
                                                    </div>
                                                </div>

                                                <div className="form-group">
                                                    <label htmlFor="id_address">Address</label>
                                                    <Field type="text" name="address" cols="40" rows="10" component={renderFieldText} className="input-control input-control"
                                                        label="Address" validate={[maxLength200]}/>

                                                </div>

                                                <div className="form-group">
                                                    <label htmlFor="id_website">Website</label>
                                                    <Field 
                                                        type="url" 
                                                        name="website" 
                                                        component={renderField} 
                                                        className="input-control input-control"
                                                        placeholder="Enter Website" 
                                                        validate={websiteValiadtion}
                                                        backend_error={this.props.organizationCreatedData&&
                                                            this.props.organizationCreatedData.organizationCreateError&&
                                                            this.props.organizationCreatedData.organizationCreateError.data&&
                                                            this.props.organizationCreatedData.organizationCreateError.data.website
                                                    }
                                                        />

                                            
                                                </div>

                                                <div className="form-group">
                                                    <label htmlFor="">Logo</label>
                                                    <input className="d-none inputfile" type="file" onChange={this.upLoadFile} id="id_logo" accept="image/*"/>    
                                                    <label htmlFor="id_logo" className="btn btn-white btn-block ">
                                                        <i className="icon-paper-clip"></i>{this.state.image !==""?<span>{this.state.image.name}</span>:<span>Upload Logo</span>}
                                                    </label>
                                                    {this.state.image_validation_error  && <small class="text-danger">Upload a valid image. The file you uploaded was either not an image or a corrupted image.</small>}
                                                </div>
                                                <div className="form-group">


                                                    <button className="btn btn-primary float-right" type="submit">
                                                        Save
                                    </button>
                                                    <div className="float-none"></div>
                                                </div>



                                            </div></form>

                                    </div>


                                    <div id="footer-button" className="row mt-3 mb-4">

                                        <div className="col-sm-6">
                                        </div>

                                        <div className="col-sm-6 text-right">

                                        </div>
                                    </div>

                                </div>
                            
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
CreateOrganization = reduxForm({
    form: 'CreateOrganization'
})(CreateOrganization)
export default CreateOrganization