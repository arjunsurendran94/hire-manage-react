import React, { Component, Fragment } from 'react';
import { Link ,Redirect} from 'react-router-dom';
import { Field, reduxForm } from 'redux-form'
import Loader from '../common/Loader'
import Header from '../../components/header/Header';
import Footer from '../../components/footer/Footer';
import {countryReorder} from '../../countryReorder'

const passwordsMustMatch = (value, allValues) => {
  return(value !== allValues.password1 ? 
    ' do not match' :
     undefined)
}

const radioCheckbox = ({ input,options,type, checked,meta: { touched, error, warning } }) => (
    
    <Fragment>
        <div className="sgn-md-btn fullwidth pull-left">
            {options.map((plan, index) => (
                // <label key={index} className="radio-button radio-button-css">
                //     <input type="radio" {...input} key={index} value={plan.value} /> 
                //     <span class="label-visible subscribe_radio">
                //         <span class="fake-radiobutton"></span>
                //         <strong>
                //             <b>{plan.text}</b>
                //             <small>${plan.value}/year</small>
                //         </strong>
                //     </span>
                // </label>
                <div className="col-md-6 pull-left" key={index}>
                    <div className="row">
                        <input type="radio" {...input}  checked ={(plan.value === checked)&&true } key={index} disabled={(plan.value ==="employer")&&true} name={plan.name} value={plan.value} id={plan.id}  />
                        {/* <Field name="registration_type" component="input" type="radio" value={plan.value} id="id_registration_type_0"/> */}
                        
            <label htmlFor={plan.id} className="fullwidth pull-left">{plan.text==='Service Company'&&<i class="icon-checked  mr-2"></i>}{plan.text}</label>
                    </div>
                </div>
                ))}
        </div>
        <label>{touched &&
            ((error && <small className="text-danger">{error}</small>) ||
                (warning && <span>{warning}</span>))}</label>
    </Fragment>
)

const Checkbox = ({ input,className, meta: { touched, error, warning } }) => (
    <Fragment>
        <div>
            <input type="checkbox" className={className} {...input} />Yes, I understand and agree to the <Link to="/terms-of-service/" target="_blank" className="clr fnt-600">Terms of Service</Link>, including the <Link to="/user-aggrement/" target="_blank" className="clr fnt-600">User Agreement</Link> and <Link to="/privacy-policy/" target="_blank" className="clr fnt-600">Privacy Policy</Link>.
        </div>
        {touched &&
            ((error && <small className="text-danger" style={{fontSize:"11px"}}>{error}</small>) ||
                (warning && <span>{warning}</span>))}
    </Fragment>
)

const required =  value => {
    return(value || typeof value === 'number' ? undefined : ' is Required')
}

const requiredRadio =  value => {
    return(value || typeof value === 'number' ? undefined : ' Select a  Service')
}

const requiredCheck =  value => {
    return(value || typeof value === 'number' ? undefined : ' Please agree to the terms and Conditions')
}


const minLength = min => value =>
    value && value.length < min ? ` Must be ${min} characters or more` : undefined
const minLength2 = minLength(8)
var paswd=  /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{7,15}$/;
const mixPassword =  value => {
    return(value.match(paswd) ? undefined : ' between 7 to 15 characters which contain at least one numeric digit and a special character')
}

const renderField = ({
    input,
    label,
    type,
    existed,
    password_error,
    meta: { touched, error, warning,  }
  }) => {
      
    return(
    

      <Fragment>
        <input {...input} placeholder={label} type={type} className={(touched && error)||(existed&&existed.error&&password_error) ? 'fullwidth border border-danger': 'fullwidth'}/>
        {touched &&
          ((error && <small className="text-danger">{label}{error}</small>) ||
            (warning && <span>{warning}</span>))}
        {((existed)&&(existed.existedMail)) && <small className="text-danger">
            {existed.error.data.user.email[0]}
        </small>}
        {((password_error) &&(existed)&&(existed.error.status === 400)&&(existed.error.data)&&(existed.error.data.password_data)&&(existed.error.data.password_data.confirm_password)) && <small class="text-danger">
            {existed.error.data.password_data.confirm_password[0]}
        </small>}
        
      </Fragment>
    
  )}

  const renderFieldSelect = ({
    input,
    label,
    type,
    existed,
    options,
    meta: { touched, error, warning,  }
  }) => {
      
    return(
    

      <Fragment>
        {(options)? 
            <select {...input} className="fullwidth">
                <option value="">Please Select a Country</option>
            {options.countryList.map((plan, index) => (
                <option key={index} value={plan.id}>{plan.name_ascii}</option>
            ))}
            </select>:<p>wrong</p>
        }
        <div>
        {touched &&
          ((error && <small className="text-danger">{label}{error}</small>) ||
            (warning && <span>{warning}</span>))}
        </div>
        {/* {((existed)&&(existed.error.status == 500)) && <small class="text-danger">
            {existed.errors.error}
        </small>} */}
        
      </Fragment>
    
  )}





class ConfirmSignup extends Component {
    constructor(props){
        super(props);
        this.state ={
            radio_value:"service_company",
            password_error:false
        }

    }

    componentDidMount() {
        
        if (this.props.match.params) {
            this.props.emailVerification(this.props.match.params);
            this.props.fetchCountryList();
        }
    }

    submit = (values) => {
        values.token = this.props.isEmailVerified.message.token
        values.profile_slug = this.props.isEmailVerified.message.profile_slug

        // this.props.resetForm()
        this.props.registrationComplete(values).then(()=>{
            if(this.props.registrationCompletion && this.props.registrationCompletion.error){
                this.setState({
                    password_error:true
                })
            }
        })
    }
    handleInputChange = (e) => {
        if((e.target.name === "password1") && this.props.registrationCompletion && this.props.registrationCompletion.error){
            this.setState({password_error:false})
        }
       
    }

    

    handleRadioButtonChange = (e) => {
        // const name = e.target.name;
        const value = e.target.value;
        this.setState({radio_value:value})
    }

    render() {
        const { handleSubmit} = this.props
        const serviceType = [
            { text: 'Employer', value: 'employer',name:"registration_type" , id:"id_registration_type_0",disabled:"true"},
            { text: 'Service Company', value: 'service_company',name:"registration_type" , id:"id_registration_type_1" }
        ]
        if(this.props.registrationCompletion && (this.props.registrationCompletion.isLoading)){
            return <Loader/>;    
        }
        if(this.props.registrationCompletion && this.props.registrationCompletion.registrationCompleted && (!this.props.registrationCompletion.isLoading)){
            return <Redirect to='/create-organization' />;    
        }
        return (
            <div>
            {(this.props.isEmailVerified) ?
                <Fragment>
                    <Header/>
                <section className={"login-wrapper signup-wrapper fullwidth pull-left position-relative" + 
                ( this.props.isEmailVerified&&this.props.isEmailVerified.error&&this.props.isEmailVerified.error.response&&
                    this.props.isEmailVerified.error.response.data&&
                    this.props.isEmailVerified.error.response.data.status==='error' ? ' signup-invalid ' : '')}>
                    <div className="overlay2"></div>
                    <div className="login m-auto fullwidth bg-white d-table position-relative">
                        <div className="pad-48">
                            
                            {this.props.isEmailVerified&&this.props.isEmailVerified.error&&this.props.isEmailVerified.error.response&&
                            this.props.isEmailVerified.error.response.data&&this.props.isEmailVerified.error.response.data.status==='error'&&
                            this.props.isEmailVerified.error.response.data.message ?
                            
                            <div className="alert alert-danger alert-dismissible fade show" role="alert">
                   {this.props.isEmailVerified.error.response.data.message}  
                  <br/>
              </div>:

                            
                            
                            
                            <Fragment>
                            <h5 className="text-uppercase text-center mb-2 font-18">Complete your free account setup</h5>
                            <p className="fullwidth text-center fnt-14">{(this.props.isEmailVerified.message)&&this.props.isEmailVerified.message.email}<i className="icon-checked b-6 text-success"></i></p>
                            <div className="alert fade show alert-warning-green mt-3">
                                <i className="icon-checked b-6 alert-icon mr-2"></i> Your email address is verified successfully.
                            </div>
                            <form onSubmit={handleSubmit(this.submit)} noValidate>
                                <div className="col-md-12 pull-left log-icon signup">
                                    {/* <select class="fullwidth">
                                        <option value="Select your country">Select your country</option>
                                        <option value="India">India</option>
                                        <option value="Australia">Australia</option>
                                        <option value="USA">USA</option>
                                    </select> */}
                                    {/* <Field name="country" component="select" validate={required} className="fullwidth">
                                        <option value="Select your country">Select your country</option>
                                        <option value="India">India</option>
                                        <option value="Australia">Australia</option>
                                        <option value="USA">USA</option>
                                    </Field> */}
                                    <Field 
                                        name="country" 
                                        type ="select"
                                        options = {countryReorder(this.props.countryDetails)} 
                                        component={renderFieldSelect} 
                                        className="fullwidth" 
                                        label="Country"
                                        validate={required}/>
                                </div>

                                <div className="col-md-12 pull-left log-icon signup">
                                    {/* <input type="password" name="password1" placeholder="Password" class="input-control fullwidth" required="" id="id_password1" /> */}
                                    <Field
                                        name="password1"
                                        type="password"
                                        label="Password"
                                        placeholder="Password"
                                        onChange={this.handleInputChange}
                                        className="input-control fullwidth"
                                        component={renderField}
                                        validate={[required,minLength2,mixPassword]}
                                        password_error = {this.state.password_error}
                                        existed={this.props.registrationCompletion}
                                    />
                                </div>

                                <div className="col-md-12 pull-left log-icon signup">
                                    {/* <input type="password" name="password2" placeholder="Confirm Password" class="input-control fullwidth" required="" id="id_password2" /> */}
                                    <Field
                                        name="password2"
                                        type="password"
                                        label="Confirm Password"
                                        placeholder="Confirm Password"
                                        onChange={this.handleInputChange}
                                        className="input-control fullwidth"
                                        component={renderField}
                                        validate={[required,passwordsMustMatch]}
                                    />
                                </div>


                                <div className="sign-mid fullwidth text-center">
                                    <h6 className="fnt-600 mb-3">I want to register as:</h6>
                                    {/* <div class="sgn-md-btn fullwidth pull-left"> */}
                                        {/* <div class="col-md-6 pull-left">
                                            <div class="row">
                                                <input type="radio" name="registration_type" value="employer" id="id_registration_type_0" required />
                                                <Field name="registration_type" component="input" type="radio" value="employer" id="id_registration_type_0"/>
                                                <label for="id_registration_type_0" class="fullwidth pull-left">Employer</label>
                                            </div>
                                        </div>
                        
                                        <div class="col-md-6 pull-left">
                                            <div class="row">
                                                <input type="radio" name="registration_type" value="service_company" id="id_registration_type_1" required checked />
                                                <Field name="registration_type" component="input" type="radio" value="service_company" id="id_registration_type_1"/>
                                                <label for="id_registration_type_1" class="fullwidth pull-left">Service Company</label>
                                            </div>
                                        </div> */}
                                        <Field
                                        name="registration_type"
                                        component={radioCheckbox}
                                        checked={this.state.radio_value}
                                        options = {serviceType}
                                        validate={requiredRadio}
                                        onChange={this.handleRadioButtonChange}/>
                        
                                    {/* </div> */}
                                </div>

                                <div className="sgn-chk-bx fullwidth pull-left fnt-12 mb-4 mt-4">
                                    <Field name="policyagree" id="employed" component={Checkbox} validate={requiredCheck} type="checkbox" className="mr-2" />
                                    {/* <input type="checkbox" class="mr-2" />Yes, I understand and agree to the <a href="" class="clr fnt-600">Terms of Service</a>, including the <a href="" class="clr fnt-600">User Agreement</a> and <a href="" class="clr fnt-600">Privacy Policy</a>. */}
                                </div>
                                <button className="bg fullwidth text-center text-capitalize lg-btn sgn-btn d-table m-auto text-white pt-1 pb-1">Create my Account</button>
                                <p className="fullwidth text-center signup-p">Already have an account? <Link to='/login' className="clr fnt-600">Log In</Link></p>
                            </form>
                            </Fragment>
                            }
                        
                        </div>
                    </div>
                </section>
                <Footer/>
                </Fragment>
                :<Loader/>
            }
            </div>
        )}
}

ConfirmSignup = reduxForm({
    form:'confirmSignup',
    initialValues: { registration_type: 'service_company' },
})(ConfirmSignup)
export default ConfirmSignup