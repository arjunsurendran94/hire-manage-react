import React ,{ Component ,Fragment} from 'react';
import { Link } from 'react-router-dom';
import { Field, reduxForm } from 'redux-form'
import Footer from '../footer/Footer';
import Header from '../header/Header';
import Loader from '../common/Loader'

const required =  value => {
    return(value || typeof value === 'number' ? undefined : ' is Required')
}
var letters = /^[-a-zA-Z0-9-()]+(\s+[-a-zA-Z0-9-()]+)*$/;
const alphabetOnly =  value => {
    return(value && value.match(letters) ? undefined : ' must be characters only')
}

const maxLength = max => value =>{
  return(value && value.length > max ? `Must be ${max} characters or less` : undefined)
}
const maxLength15 = maxLength(15)

const email = value =>
  value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
    ? ' is Invalid'
    : undefined


const renderField = ({
    input,
    label,
    type,
    existed,
    meta: { touched, error, warning,  }
  }) => {
      
    return(
    

      <Fragment>
        <input {...input} placeholder={label} type={type} className={(touched && error)||(existed&&existed.existedMail&&existed.error&&existed.error.data&&existed.error.data.user&&existed.error.data.user.email) ? 'fullwidth border border-danger': 'fullwidth'}/>
        {touched &&
          ((error && <small className="text-danger">{label}{error}</small>) ||
            (warning && <span>{warning}</span>))}
        {((existed)&&(existed.existedMail)&&existed.error&&existed.error.data&&existed.error.data.user&&existed.error.data.user.email) && <small className="text-danger">
            {existed.error.data.user.email[0]}
        </small>}
        {/* {((existed)&&(existed.error.status == 500)) && <small class="text-danger">
            {existed.errors.error}
        </small>} */}
        
      </Fragment>
    
  )}


class Signup extends Component{


    submit = (values) => {
        this.props.userSignUp(values)
        // this.props.resetForm()
    }
    
    render(){
        const { handleSubmit} = this.props
        return(
            <Fragment>
            {(this.props.userSignupDetails && this.props.userSignupDetails.isLoading)?
                <Loader/>:
            <Fragment>
            <Header />
            <div className={" signup-wrapper fullwidth pull-left position-relative " + ((this.props.userSignupDetails&&this.props.userSignupDetails.message)?'login-wrapper1':'login-wrapper') }>
                <div className="overlay2"></div>
                <div className="login m-auto fullwidth bg-white d-table position-relative">
                    <div className="pad-48 pull-left">
                        <h5 className="text-uppercase text-center mb-5">create your account</h5>
                        {(this.props.userSignupDetails&&this.props.userSignupDetails.message)&&
                            <div className="alert fade show alert-warning-green mt-3">
                                <i className="icon-checked b-6 alert-icon mr-2"></i> We have sent an email to your email address. Please verify the email. Thank you
                            </div>
                        }

                        {
                            (this.props.userSignupDetails&&this.props.userSignupDetails.error&&this.props.userSignupDetails.error.data&&
                                this.props.userSignupDetails.error.data.error&&
                                <div className="alert alert-danger alert-dismissible fade show">
                                 {this.props.userSignupDetails.error.data.error}
                            </div>)
                        }


                        {!(this.props.userSignupDetails&&(this.props.userSignupDetails.message||(this.props.userSignupDetails.error&&this.props.userSignupDetails.error.status===500)))&&
                        <form onSubmit={handleSubmit(this.submit)} noValidate>
                            <div className="col-md-6 pull-left log-icon signup">
                                <Field type="text" name="firstname" component={renderField}
                                label="First Name" validate={[required,alphabetOnly]}/>
                            </div>
                            <div className="col-md-6 pull-left log-icon signup">
                                <Field type="text" name="lastname" component={renderField}
                                label="Last Name" validate={[required,alphabetOnly]} />
                            </div>
                            <div className="col-md-12 pull-left log-icon signup mt-4">
                                <Field type="email" name="email" component={renderField} 
                                label="Email Address" existed ={this.props.userSignupDetails} validate={[required,email]} />
                            </div>
                            <div className="mt-4 pull-left w-100">
                                <button className="bg fullwidth text-center text-capitalize lg-btn d-table m-auto text-white pt-1 pb-1" >Sign Up</button>
                            </div>
                            <p className="fullwidth text-center signup-p pull-left">Already have an account? 
                            
                            <Link to='/login' href="" className="clr fnt-600"> Log In</Link></p>
                        </form>
                        }


                    
                    </div>
                </div>
            </div>
            <Footer/>
            </Fragment>
                    }
        </Fragment>)
    
    }
}
Signup = reduxForm({
    form:'signupform'
})(Signup)
export default Signup