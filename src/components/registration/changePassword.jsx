import React ,{ Component ,Fragment} from 'react';
import { Redirect } from 'react-router-dom';
import { Field, reduxForm } from 'redux-form'

const required =  value => {
    return(value || typeof value === 'number' ? undefined : ' is Required')
}

const minLength = min => value =>
    value && value.length < min ? ` Must be ${min} characters or more` : undefined
const minLength2 = minLength(8)


    const passwordsMustMatch = (value, allValues) => {
        return(value !== allValues.password ? 
          ' do not match' :
           undefined)
      }


const renderField = ({
    input,
    label,
    type,
    existed,
    meta: { touched, error, warning,  }
  }) => {
      
    return(
    

      <Fragment>
        <input {...input} placeholder={label} type={type} className={(touched && error)||(existed&&existed.existedMail) ? 'input-control border border-danger': 'input-control'}/>
        {touched &&
          ((error && <small className="text-danger">{label}{error}</small>) ||
            (warning && <span>{warning}</span>))}
        
      </Fragment>
    
  )}

class CompleteResetPassword extends Component{
    componentDidMount() {
        
        if (this.props.match.params) {
            if(this.props.employeePassword){
                this.props.employeeEmailVerification(this.props.match.params);
            }else{
                this.props.resetEmailVerification(this.props.match.params);
            }
            
        }
    }

    submit = (values) => {
        this.props.changePassword(this.props.match.params,values)
    }


    render(){
        
        const { handleSubmit} = this.props
        if(this.props.changePasswordStatus){
            
            if(this.props.employeePassword){
                if(this.props.changePasswordStatus.error){
                    return <Redirect to='/employees/employees/'/>;
    
                }
                if(this.props.changePasswordStatus.passwordChange){
                    return <Redirect to='/login'/>;
                }
            }else{
                if(this.props.changePasswordStatus.error){
                    return <Redirect to='/login'/>;
                }
                if(this.props.changePasswordStatus.passwordChange){
                    return <Redirect to='/login'/>;
                }
            }            
        }
        return(
            <Fragment>
                <div className="container-fluid">
                    <div className="row full-height justify-content-center">
                        <div className="col-sm-6 col-md-3 align-self-center">
                            <img alt="logo" src={require("../../static/assets/image/logo.png")} className="brand-img"/>
                            
                            {((this.props.employeeVerification&&this.props.employeeVerification.employeeVerify&&this.props.employeeVerification.employeeVerify.status==='success')||
                            (this.props?.email_response?.status==='success')) ?
                            <Fragment>
                            <div className="alert-bx alert-warning-yellow mt-3">
                                <i className="icon-warning-triangle alert-icon mr-2"></i> Create a password to login, this password will be used for future login
                            </div>
                            

                            <ul className="color-danger list-unstyled">
                            </ul>
                            
                            <div className="bx-content shadow mt-3 pt-5 pb-5">
                                {this.props.employeePassword ?
                                    <div className="col-sm-12 pl-4 pr-4 pt-4 pb-2"><h5>Create a Password</h5></div> :
                                    <div className="col-sm-12 pl-4 pr-4 pt-1 text-center"><h5>RESET PASSWORD</h5></div>
                                }
                                <p className="pl-4 pr-4 text-center"><small> {(this.props.email_response&&this.props.email_response.email) &&this.props.email_response.email} </small></p>
                                <div className="col-sm-12 pl-4 pr-4">

                                <form onSubmit={handleSubmit(this.submit)} noValidate>
                                        {/* <input type="hidden" name="csrfmiddlewaretoken" value="cF7c9FRLskBHMXROiNcjDl0kJp3Bsou4CDiU5xbioXApujng8ASKTAZldmHxSi2U"/> */}

                                    <div className="row">
                                        
                                            <div className="col-sm-12 mb-2">
                                            <label htmlFor="id_new_password1" className="text-light-grey">Password</label>
                                            {/* <input type="password" name="new_password1" class="input-control  " id="id_new_password1" placeholder="Enter New Password"/> */}
                                            <Field
                                            name="password"
                                            type="password"
                                            label="Password"
                                            placeholder="Password"
                                            onChange={this.handleInputChange}
                                            component={renderField}
                                            validate={[required,minLength2]}
                                        />

                                            
                                            </div>

                                            <div className="col-sm-12 mb-2">
                                            <label htmlFor="id_new_password2" className="text-light-grey">Confirm password</label>
                                            {/* <input type="password" name="new_password2" class="input-control  " id="id_new_password2" placeholder="Confirm New Password"/> */}
                                            <Field
                                            name="confirm_password"
                                            type="password"
                                            label="Confirm Password"
                                            placeholder="Confirm Password"
                                            onChange={this.handleInputChange}
                                            component={renderField}
                                            validate={[required,passwordsMustMatch]}
                                        />
                                            

                                            </div>

                                            <div className="col-sm-12 text-right">
                                                <button type="submit" className="btn btn-primary">Change Password</button>
                                            </div>
                                        
                                    </div>

                                </form>



                                </div>
                            </div>
                            </Fragment>
                            :
                            <Fragment>
                                <div className="alert alert-dismissible fade show alert-danger mt-3">
                                <i className="icon-warning-triangle alert-icon mr-2"></i> Invalid Verification Link
                            </div>
                            </Fragment>
                            }
                        
                        
                        </div>
                    </div>
                </div>
                                
            </Fragment>
        )
    }
}

CompleteResetPassword = reduxForm({
    form:'completepasswordform'
})(CompleteResetPassword)
export default CompleteResetPassword