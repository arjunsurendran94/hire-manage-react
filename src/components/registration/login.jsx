import React ,{ Component ,Fragment} from 'react';
import { Link ,Redirect} from 'react-router-dom';
import { Field, reduxForm } from 'redux-form'
import Footer from '../footer/Footer';
import Header from '../header/Header';
import Loader from '../common/Loader'


const required =  value => {
    return(value || typeof value === 'number' ? undefined : ' is Required')
}

const minLength = min => value =>
    value && value.length < min ? ` Must be ${min} characters or more` : undefined
const minLength2 = minLength(8)

const email = value =>
  value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
    ? ' is Invalid'
    : undefined




const renderField = ({
    input,
    label,
    type,
    existed,
    meta: { touched, error, warning,  }
  }) => {
      
    return(
    

      <Fragment>
        <input {...input} placeholder={label} type={type} className={(touched && error)||(existed&&existed.existedMail) ? 'fullwidth border border-danger': 'fullwidth'}/>
        {touched &&
          ((error && <small className="text-danger">{label}{error}</small>) ||
            (warning && <span>{warning}</span>))}
        {((existed)&&(existed.existedMail)) && <small class="text-danger">
            {existed.error.data.user.email[0]}
        </small>}
        {/* {((existed)&&(existed.error.status == 500)) && <small class="text-danger">
            {existed.errors.error}
        </small>} */}
        
      </Fragment>
    
  )}

class Login extends Component{
    submit = (values) => {
        this.props.userLogin(values)
        // this.props.history.push('/signup')
    }

    render(){
        const { handleSubmit} = this.props
        if(this.props.organzationList){
            if(this.props.organzationList.statusCode === 200){
                if(localStorage.getItem("token")){
                    return(
                        this.props.organzationList.data.length >0?
                        <Redirect to='/dashboard' />:<Redirect to='/create-organization'/>)
                }
            }
        }
        return(
            <Fragment>
                {(this.props.loginDetails && this.props.loginDetails.isLoading) ?
                <Loader/>:
                    <Fragment>
            <Header />
            <div className="login-wrapper fullwidth pull-left create-wrapper">
                <div className="login m-auto fullwidth bg-white d-table">
                    <div className="pad-48">
                        <h5 className="text-uppercase text-center mb-5">Log in and get to work</h5>
                        {(this.props.loginDetails&&this.props.loginDetails.error)&&
                        <div class="alert-bx error-alert mt-3">
                        <i class="icon-warning-triangle alert-icon mr-2"></i> Please enter a correct Email Address and password. Note that both fields may be case-sensitive.
                        </div>}
                        {(this.props.changePasswordStatus&&this.props.changePasswordStatus.passwordChange)&&
                        <div class="alert fade show alert-warning-green mt-3">
                            <i class="icon-checked b-6 alert-icon mr-2"></i> Your password has been set. You may go ahead and log in now. 
                        </div>}
                        {(this.props.changePasswordStatus&&this.props.changePasswordStatus.error)&&
                        <div class="alert fade show alert-danger mt-3">
                        <i class="icon-warning-triangle alert-icon mr-2"></i> The password reset link was invalid, possibly because it has already been used. Please request a new password reset.
                        </div>}
                        <form onSubmit={handleSubmit(this.submit)} noValidate>
                            <div className="position-relative log-icon mb-4">
                                <span className="position-absolute clr"><i className="icon-avatar"></i></span>
                                {/* <input type="email" placeholder="User Name or Email" className="fullwidth" /> */}
                                <Field type="email" name="email" component={renderField} 
                                label="Email Address" existed ={this.props.userSignupDetails} validate={[required,email]} />
                            </div>
                            <div className="position-relative log-icon mb-2">
                                <span className="position-absolute clr"><i className="fa fa-unlock-alt"></i></span>
                                {/* <input type="password" placeholder="Password"  className="fullwidth" /> */}
                                <Field
                                    name="password"
                                    type="password"
                                    label="Password"
                                    placeholder="Password"
                                    onChange={this.handleInputChange}
                                    className="input-control fullwidth"
                                    component={renderField}
                                    validate={[required,minLength2]}
                                />
                            </div>
                            <div className="log-lnk fullwidth pull-left mb-4">
                                {/* <div className="pull-left"><input className="mr-2" type="checkbox" />Keep me logged in</div> */}
                                <Link to="/accounts/password-reset/" className="pull-right">Forgot Password</Link>
                            </div>
                            <button className="bg fullwidth text-center text-capitalize lg-btn d-table m-auto text-white pt-1 pb-1">log in</button>
                        </form>
                    </div>
                    <div className="log-sign-up fullwidth pull-left position-relative">
                        <div className="overlay2"></div>
                        <div className="position-relative">
                            <h6 className="text-white text-center mb-3">New to Hire & Manage</h6>
                            <Link to='signup/'>
                            <button className="bg-white fullwidth text-center text-capitalize lg-btn d-table m-auto pt-1 pb-1 clr border-white">sign up</button>
                            </Link>
                        </div>
                    </div>
                </div>
            </div>
            <Footer/>
            </Fragment>
                        }
            </Fragment>
        )
    }
}

Login = reduxForm({
    form:'loginform'
})(Login)
export default Login