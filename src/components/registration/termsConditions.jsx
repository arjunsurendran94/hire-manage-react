import React, { Component, Fragment } from 'react';
import { Link } from 'react-router-dom';
import Header from '../header/Header';
import Footer from '../footer/Footer'

export default class TermsConditions extends Component {
    render() {
        return (
            <Fragment>
                <Header />
                <div className="login-wrapper fullwidth pull-left">
                    <div className="login m-auto fullwidth ">
                        
                        <div className=" fullwidth pull-left position-relative">
                        {this.props.tos&&
                        <div>
                        <h3 className="text-center">Terms of Service</h3>
                        <div>
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.Why do we use it?
                            It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. 
                            The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', 
                            making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' 
                            will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).    
                        </div>
                        </div>}

                        {this.props.ua&&
                        <div>
                        <h3 className="text-center">User Aggreement</h3>
                        <div>
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.Why do we use it?
                            It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. 
                            The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', 
                            making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' 
                            will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).    
                        </div>
                        </div>}
                        
                        {this.props.pp&&
                        <div>
                        <h3 className="text-center">Privacy Policy</h3>
                        <div>
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.Why do we use it?
                            It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. 
                            The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', 
                            making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' 
                            will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).    
                        </div>
                        </div>}

                        
                        </div>
                    </div>
                </div>

            {/* <footer class="pull-left fullwidth" style={{position: "fixed",bottom: 0,left: 0,right: 0}}>
            <div class="footer-wrapper fullwidth pull-left" >
                <div class="container">
                    <div class="un-line fullwidth pull-left pt-3 pb-3">
                        <ul class="pull-left text-white">
                            <Link class="text-uppercase ftr-blk ft-link">about us</Link>
                            <Link class="text-uppercase ftr-blk ft-link" to="/login">log in</Link>
                            <Link class="text-uppercase ftr-blk ft-link" to="/signup">sign up</Link>
                            <Link class="text-uppercase ftr-blk ft-link">contact us</Link>
                        </ul>
                        <ul class="pull-right m-0">
                            <div class="soc-mid">
                                <li class="d-inline social-list mr-3 pull-left"><Link class="pull-left"><i class="fa fa-facebook-f"></i></Link></li>
                                <li class="d-inline social-list mr-3 pull-left"><Link class="pull-left"><i class="fa fa-linkedin"></i></Link></li>
                                <li class="d-inline social-list pull-left"><Link class="pull-left"><i class="fa fa-twitter"></i></Link></li>
                            </div>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="copyright fullwidth text-center pt-3 pb-3 pull-left text-white"> &copy;2019 HIRE & MANAGE.</div>
        </footer> */}

        <Footer/>

            </Fragment>

        )
    }
}