import React ,{ Component ,Fragment} from 'react';
import { Field, reduxForm } from 'redux-form'
// import LoaderImage from '../../static/assets/image/loader.gif';
import LoaderImage from '../../static/assets/image/loader.gif';
var loaderStyle = {
    width: '3%',
    position: 'fixed',
    top: '40%',
    left: '50%',
    
 }
const required =  value => {
    return(value || typeof value === 'number' ? undefined : ' is Required')
}

// const minLength = min => value =>
//     value && value.length < min ? ` Must be ${min} characters or more` : undefined
// const minLength2 = minLength(8)

const email = value =>
  value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
    ? ' is Invalid'
    : undefined




const renderField = ({
    input,
    label,
    type,
    existed,
    meta: { touched, error, warning,  }
  }) => {
      
    return(
    

      <Fragment>
        <input {...input} placeholder={label} type={type} className={(touched && error)||(existed&&existed.existedMail) ? 'input-control border border-danger': 'input-control'}/>
        {touched &&
          ((error && <small className="text-danger">{label}{error}</small>) ||
            (warning && <span>{warning}</span>))}
        
      </Fragment>
    
  )}

class ForgotPassword extends Component{
    constructor(props){
        super(props)
        this.state = {
            isLoading : true,
            clicked:false
            
        }
    }
    
    componentDidMount() {
        this.setState({isLoading: false})
    }
    submit = (values) => {
        this.props.resetPassword(values)
        this.setState({clicked:true})
    }


    render(){
        const { handleSubmit} = this.props
        console.log("reset",this.props.resetPasswordStatus)
        return(
            <Fragment>
                <div className="container-fluid create-wrapper">
                    <div className="row full-height justify-content-center">
                        <div className="col-sm-6 col-md-3 align-self-center">

                        {(this.props.resetPasswordStatus&&this.props.resetPasswordStatus.message)&&
                        <div class="alert fade show alert-warning-green mt-3">
                            <i class="icon-checked b-6 alert-icon mr-2"></i> We've emailed you instructions for setting your password,    if an account exists with the email you entered. You should receive them shortly.
                        </div>
                        }

                        {this.props.resetPasswordStatus?.isLoading?
                           <Fragment>
                              <div className="loader-img"> <img src={LoaderImage} style={loaderStyle} /></div>
                           </Fragment>:

                        
                            

                        <div className="bx-content shadow mt-3 pt-3 pb-3">
                            <img alt="" src={require("../../static/assets/image/logo.png") }
                            className="brand-img ml-4"/>
                            <div className="col-sm-12 pl-4 pr-4 pt-4 pb-2 text-center"><h5>FORGOT PASSWORD</h5></div>
                            <div className="col-sm-12 pl-4 pr-4">

                            {

                                <form onSubmit={handleSubmit(this.submit)} noValidate>
                                    <input type="hidden" name="csrfmiddlewaretoken" value="2mIw8sWK2jUMXociRGgomtSq1Cx59IhoFB3tZUEowztKtJo1hLReQd6R2cvwSVco"/>
                                <div className="row">

                                    <div className="col-sm-12 mb-2">
                                        <label className="text-light-grey">Email Address</label>

                                        {/* <input type="email" name="email" maxLength="254" id="id_email" className="input-control
                                        " placeholder="Enter Email Address"/> */}
                                        <Field 
                                            type="email" 
                                            name="email" 
                                            component={renderField} 
                                            label="Email Address" 
                                            validate={[required,email]} />

                                        

                                    </div>
                                    
                                    <div className="col-sm-12 text-right">
                                        <button type="submit" className="btn btn-primary">Submit</button>
                                    </div>

                                </div>
                            </form>

                            }
                            </div>
                        </div>}
                        
                        </div>
                    </div>
                </div>
                                
            </Fragment>
                                   
        )
    }
}

ForgotPassword = reduxForm({
    form:'forgotpasswordform'
})(ForgotPassword)
export default ForgotPassword