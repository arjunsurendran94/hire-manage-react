import React, {Component,Fragment} from 'react'
import { Link } from 'react-router-dom'

class History extends Component{
    constructor(props){
        super(props)
        this.state = {
            routePath : '',
        }
    }
    componentDidMount () {
        if(this.props.location && this.props.location.pathname === "/history/"){
            this.setState({routePath:"History"})
        }
        if(this.props.location && this.props.location.pathname === "/clients/clients/"){
            this.setState({routePath:"Client"})
        }
        if(this.props.location && this.props.location.pathname === "/groups/"){
            this.setState({routePath:"Groups"})
        }
        if(this.props.location && this.props.location.pathname === "/projects/projects/"){
            this.setState({routePath:"Project"})
        }
        if(this.props.location && this.props.location.pathname === "/reports/"){
            this.setState({routePath:"Reports"})
        }
    }
    render(){
        return( 
            // <Fragment>
            //     <div className="container-fluid">
            //         <div className="row">
            //             <h3>This is the "{this.state.routePath}" page and its under maintenance<br/>Please go back to the previous page</h3>
            //         </div>
            //     </div>
                
            <Fragment>
            {/* <Header /> */}
            {/* <div className="login-wrapper fullwidth pull-left create-wrapper">
                <div className="login m-auto fullwidth bg-white d-table">
                    <div className="pad-48">
                        <h5 className=" text-center mb-5">This is the "{this.state.routePath}" page and its under maintenance<br/>Please go back to the previous page</h5>
                    </div>                      
                </div>
            </div> */}
            <div className="container-fluid create-wrapper">
                    <div className="row full-height justify-content-center">
                        <div className="col-sm-6 col-md-3 align-self-center">
                        <div className="bx-content shadow mt-3 pt-3 pb-3">
                            <div className="text-center">
                            <img alt="" src={require("../../static/assets/image/logo.png") }
                            className="brand-img ml-4"/>
                            </div>
                            <div className="col-sm-12 pl-4 pr-4 pt-4 pb-2 text-center"><h5>Ooops! <br/>The "{this.state.routePath}" page is under maintenance</h5></div>
                            <div className="col-sm-12 pl-4 pr-4 text-center">
                                <Link to= "/dashboard" ><span>Go to Dashboard</span></Link>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            {/* <Footer/> */}
            </Fragment>
            // </Fragment>
        )
    }
}

export default History