import React, {Component,Fragment} from 'react'
import SideBar from './common/sideBar'
import DashBoardHeaderContainer from '../../containers/common/dashBoardHeaderContainer'
import DashBoardSubheader from './common/dashboardSubHeader'
import AddEmployeeContent from './content/addEmployeeContent'

class AddEmployee extends Component{
    constructor(props){
        super(props)
        this.state = {
            show : true,
            hide :  true
        }
    }
    render(){
        return( 
            <Fragment>
                <div className="container-fluid">
                    <div className="row">
                        {<SideBar a={true} {...this.props}/>}
                        <div className="col-sm-12 container-body ">
                            <DashBoardHeaderContainer/>
                            <div className="row">
                                <DashBoardSubheader subheader = "createEmployee" {...this.props}/>
                                <AddEmployeeContent {...this.props}/>
                            </div>
                        </div>
                    </div>
                </div>
            </Fragment>
        )
    }
}

export default AddEmployee