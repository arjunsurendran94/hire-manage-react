import React, {Component,Fragment} from 'react'
// import OrganizationContent from './content/organizationContent'
import SideBar from './common/sideBar'
import DashBoardHeaderContainer from '../../containers/common/dashBoardHeaderContainer'
import DashBoardSubheader from './common/dashboardSubHeader'
import DesignationDetailContent from '../dashboard/content/designationDetailContent'


export default class DesignationDetail extends Component{
    constructor(props) {
        super(props);
        this.state = {show_message: false};
    }

    setShowMessage = () => {
        this.setState({show_message:!this.state.show_message})
    }

    render(){
        return( 
            <Fragment>
                <div className="container-fluid">
                    <div className="row">
                        {<SideBar a={true} {...this.props}/>}
                        <div className="col-sm-12 container-body ">
                            <DashBoardHeaderContainer/>
                            <div className="row">
                                <DashBoardSubheader setShowMessage = {this.setShowMessage} subheader = "DesignationDetail" {...this.props}/>
                                <DesignationDetailContent  show_message={this.state.show_message} setShowMessage = {this.setShowMessage} {...this.props}/>
                            </div>
                        </div>
                    </div>
                </div>
            </Fragment>
        )
    }
}