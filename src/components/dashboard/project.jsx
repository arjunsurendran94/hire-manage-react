import React, { Component, Fragment } from 'react'
import ProjectContent from './content/projectContent'
import ClientList from './content/clientList'
import SideBar from './common/sideBar'
import DashBoardHeaderContainer from '../../containers/common/dashBoardHeaderContainer'
import DashBoardSubheader from './common/dashboardSubHeader'
import Loader from '../common/Loader'
import LoaderImage from '../../static/assets/image/loader.gif';
import { Link } from 'react-router-dom'
import SuccessMessage from '../common/SuccessMessage'
import ErrorMessage from '../common/ErrorMessage'


var loaderStyle = {
   width: '3%',
   position: 'fixed',
   top: '50%',
   left: '57%'
}
class Project extends Component {
   closeMessage =()=>{
      console.log("err not working");
      // this.props.listProjects() 
       this.props.clearProjectsSearch()
      
       
   }
   componentDidMount() {
      this.props.clearProjectsSearch()
      this.props.listProjects()
      
   }
   
   render() {
      console.log('pppppp',this.props.projectData?.isLoading);
      let projects=[]
      if(this.props.projectData && this.props.projectData.status === 'success' && this.props.projectData.projects &&
                                       this.props.projectData.projects.length > 0 ){
       projects=this.props?.projectData?.projects
       if(this.props?.projectData?.search){
         //  debugger
          let lowercasedFilter = this.props?.projectData?.search?.toLowerCase();
            let parameters = ["project_name"]
            projects = this.props?.projectData?.projects?.filter(item => {
               return parameters.some(key =>
                  item.project[key].toLowerCase().includes(lowercasedFilter)
               );
            });

       }
      }
      else{
         projects=[]
      }
      
      return (
         <Fragment>
            <div className="container-fluid">
               <div className="row">
                  <SideBar />
                  <div className="col-sm-12 container-body ">
                     <DashBoardHeaderContainer />
                     <div className="row">
                        <DashBoardSubheader {...this.props} subheader="Projects" />
                         
                        {console.log('ssssss',this.props.projectData&&this.props.projectData.isLoading)
                        }
                        {this.props.projectData&&!this.props.projectData.isLoading?
                        <Fragment>
                           <div className='col-sm-12 mt-3 sub-header'>
                              {this.props.location && this.props.location.state && this.props.location.state.status === 'createsuccess'
                                 && <SuccessMessage closemsg={this.CloseMsg}
                                    msg='Project Created Sucessfully' />}

                              {this.props.location && this.props.location.state && this.props.location.state.status === 'error'
                                 && <ErrorMessage closemsg={this.CloseMsg} 
                                    msg='Project creation Failed' />}
                                    {/* {projects.length > 0&&<Fragment>
                                       <ErrorMessage closemsg={this.CloseMsg}
                                    msg='No projects found' />
                                       </Fragment>} */}

                              {this.props.projectData && this.props.projectData.status === 'success' && this.props.projectData.projects &&
                                       this.props.projectData.projects.length > 0&&projects.length > 0&&
                              <table class="table table-cs text-grey table-text-sm table-striped table-borderless">
                                 <thead>
                                    <tr>
                                       <th>PROJECT NAME</th>
                                       <th>CLIENT NAME</th>
                                       <th>START DATE (yyy/mm/dd)</th>
                                       <th>END DATE (yyy/mm/dd)</th>
                                       {/* <th>CONTACT NUMBER</th> */}
                                    </tr>
                                 </thead>
                                 <tbody>
                                 
                                    

                                    {
                                       projects.map((obj) =>

                                          <Fragment>
                                             <tr>
                                                
                                             <td data-title="PROJECT NAME">
                                                {/* to={{pathname:`/clients/client/${clientObj.client.slug}`}} */}
                                                <Link to={{ pathname: `/projects/project/${obj.project.slug}` }}>
                                                   {obj.project && obj.project.project_name}
                                                </Link>
                                             </td>
                                             <td data-title="CLIENT NAME">{obj.project && obj.project.client}</td>
                                             <td data-title="START DATE">
                                                {obj.project && obj.project.start_date}
                                             </td>
                                             <td data-title="END DATE">
                                                {obj.project && obj.project.end_date}
                                             </td>
                                             </tr>
                                             </Fragment>

                                       )}
                                    
                                   

                                 </tbody>
                              </table>}
                              {this.props.projectData && this.props.projectData.status === 'success' && this.props.projectData.projects &&
                                       this.props.projectData.projects.length > 0&&projects.length === 0&&
                              <Fragment>
                                       <ErrorMessage closeMessage={this.closeMessage}
                                    msg='No projects found' />
                                       </Fragment>}
                           </div>
                           
                           {this.props.projectData && this.props.projectData.projects && this.props.projectData.projects.length == 0 &&
                              <ProjectContent />
                           }


                        </Fragment>:
                         <Fragment>
                         {console.log('zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz')
                         }
                         <div className="loader-img"> <img src={LoaderImage} style={loaderStyle} /></div>
                      </Fragment>
                         }
                     </div>
                  </div>
               </div>
            </div>
         </Fragment>
      )
   }
}
export default Project