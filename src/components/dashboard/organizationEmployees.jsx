import React, {Component,Fragment} from 'react'
import SideBar from './common/sideBar'
import DashBoardHeaderContainer from '../../containers/common/dashBoardHeaderContainer'
import DashBoardSubheader from './common/dashboardSubHeader'
import OrganizationEmployeeContent from './content/organizationEmployeesContent'

class OrganizationEmployees extends Component{
    constructor(props){
        super(props)
        this.state = {
            show : true,
            hide :  true,
            status:"Active",
            employeeTotal:this.props.employees && this.props.employees.allEmployees&&this.props.employees.allEmployees.length
        }
    }
    componentDidMount () {
        // {if(this.state.employeeTotal<=0){
        //     if(this.props.branches && this.props.branches.data&&this.props.branches.data.length > 0){
        //         this.props.listEmployees('')
        //         this.props.branches.data.map((branch,index)=>{
        //             this.props.listEmployees(branch.slug)
        //             return true
        //         })
        //     }else{
        //         this.props.retrieveBranches(localStorage.getItem('organization_slug'),localStorage.getItem('token'))
        //         .then(()=>{
        //             this.props.listEmployees('')
        //             this.props.branches&&this.props.branches.data && this.props.branches.data.length>0&&this.props.branches.data.map((branch,index)=>{
        //                 this.props.listEmployees(branch.slug)
        //                 return true
        //             })
        //         })
        //     }
        // }
        this.props.listEmployees('')
        
        // this.props.retrieveDepartments('')
    }

    employeeStatusChange = (status) => {
        this.setState({
            status : status
        })

    }

    render(){
        return( 
            <Fragment>
                <div className="container-fluid">
                    <div className="row">
                        {<SideBar a={true} {...this.props}/>}
                        <div className="col-sm-12 container-body ">
                            <DashBoardHeaderContainer/>
                            <div className="row">
                                <DashBoardSubheader employeeStatusValue = {this.employeeStatusChange} subheader = "Employees" {...this.props}/>
                                <OrganizationEmployeeContent  employeeStatus = {this.state.status} {...this.props}/>
                            </div>
                        </div>
                    </div>
                </div>
            </Fragment>
        )
    }
}

export default OrganizationEmployees