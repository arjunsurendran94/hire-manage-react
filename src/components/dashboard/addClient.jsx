import React, {Component,Fragment} from 'react'
import SideBar from './common/sideBar'
import DashBoardHeaderContainer from '../../containers/common/dashBoardHeaderContainer'
import DashBoardSubheader from './common/dashboardSubHeader'
import AddClientContent from './content/addClientContent'

class AddClient extends Component{
    constructor(props) {
        super(props);
        this.child = React.createRef();
      }
    
    submitclientform= (values)=> 
    {   
        this.child.current.ref.current.wrapped.current.simulateClick()
    }
    
    render(){
        return( 
            <Fragment>
                <div className="container-fluid">
                    <div className="row">
                        <SideBar a={true} {...this.props}/>
                        <div className="col-sm-12 container-body ">
                            <DashBoardHeaderContainer/>
                            <div className="row">
                                <DashBoardSubheader submitclientform={this.submitclientform} {...this.props} subheader = "Add Client"/>
                                 <AddClientContent ref={this.child} {...this.props} /> 
                            </div>
                        </div>
                    </div>

                    </div>
            </Fragment>
        )
    }
}

export default AddClient