import React, {Component,Fragment} from 'react'
import SideBar from './common/sideBar'
import DashBoardHeaderContainer from '../../containers/common/dashBoardHeaderContainer'
import DashBoardSubheader from './common/dashboardSubHeader'
import OrganizationBranchesContent from './content/organizationBranchesContent'

class OrganizationBranches extends Component{
    componentDidMount(){
        window.scrollTo(0, 0)
    }
    render(){
        return( 
            <Fragment>
                <div className="container-fluid">
                    <div className="row">
                        <SideBar a={true} {...this.props}/>
                        <div className="col-sm-12 container-body ">
                            <DashBoardHeaderContainer/>
                            <div className="row">
                                <DashBoardSubheader {...this.props} subheader = "Branches"/>
                                <OrganizationBranchesContent subheader = "Branches" editable = {false} departmentFlag={false} {...this.props}/>
                            </div>
                        </div>
                    </div>

                    </div>
            </Fragment>
        )
    }
}

export default OrganizationBranches