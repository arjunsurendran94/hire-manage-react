
import React, {Component,Fragment} from 'react'
import DashBoardHeaderContainer from '../../containers/common/dashBoardHeaderContainer'
import DashBoardSubheader from './common/dashboardSubHeader'
import SideBar from './common/sideBar'
import ProfileContent from './content/profileContent'


class Profile extends Component{
    
    render(){
        return( 
            <Fragment>
                <div className="container-fluid">
                    <div className="row">
                        <SideBar/>
                        <div className="col-sm-12 container-body ">
                            <DashBoardHeaderContainer/>
                            <div className="row">
                                <DashBoardSubheader {...this.props} subheader = "My Profile"/>
                                <ProfileContent {...this.props}/>
                            </div>
                        </div>
                    </div>

                    </div>
            </Fragment>
        )
    }
}
export default Profile