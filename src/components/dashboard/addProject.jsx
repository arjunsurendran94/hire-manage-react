import React, {Component,Fragment} from 'react'
import SideBar from './common/sideBar'
import DashBoardHeaderContainer from '../../containers/common/dashBoardHeaderContainer'
import DashBoardSubheader from './common/dashboardSubHeader'
import AddProjectContent from './content/addProjectContent'

class AddClient extends Component{
    constructor(props) {
        super(props);
        this.child = React.createRef();
      }
    
    submitprojectform= (values)=> 
    {   
        this.child.current.ref.current.wrapped.current.simulateClick()
    }
    
    render(){
        return( 
            <Fragment>
                <div className="container-fluid">
                    <div className="row">
                        <SideBar a={true} {...this.props}/>
                        <div className="col-sm-12 container-body ">
                            <DashBoardHeaderContainer/>
                            <div className="row">
                                <DashBoardSubheader submitprojectform={this.submitprojectform} {...this.props} subheader = "Add Project"/>
                                 <AddProjectContent ref={this.child} {...this.props} /> 
                            </div>
                        </div>
                    </div>

                    </div>
            </Fragment>
        )
    }
}

export default AddClient