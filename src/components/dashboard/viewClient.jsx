import React, {Component,Fragment} from 'react'
import SideBar from './common/sideBar'
import DashBoardHeaderContainer from '../../containers/common/dashBoardHeaderContainer'
import DashBoardSubheader from './common/dashboardSubHeader'
import ViewClientContent from './content/viewClientContent'

class ViewClient extends Component{
    render(){
        return( 
            <Fragment>
                <div className="container-fluid">
                    <div className="row">
                        <SideBar a={true} {...this.props}/>
                        <div className="col-sm-12 container-body ">
                            <DashBoardHeaderContainer {...this.props}/>
                            <div className="row">
                                <DashBoardSubheader {...this.props} subheader = "View Client"/>
                                 <ViewClientContent {...this.props} /> 
                            </div>
                        </div>
                    </div>

                    </div>
            </Fragment>
        )
    }
}

export default ViewClient