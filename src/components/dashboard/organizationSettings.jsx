import React, {Component,Fragment} from 'react'
import SideBar from './common/sideBar'
import DashBoardHeaderContainer from '../../containers/common/dashBoardHeaderContainer'
import DashBoardSubheader from './common/dashboardSubHeader'
import OrganizationSettingsContent from './content/organizationSettingsContent'

class OrganizationSettings extends Component{
    
    componentDidMount(){
        window.scrollTo(0, 0)
    }
    render(){
        
        return( 
            <Fragment>
                <div className="container-fluid">
                    <div className="row">
                        <SideBar a={true} {...this.props}/>
                        <div className="col-sm-12 container-body ">
                            <DashBoardHeaderContainer/>
                            <div className="row">
                                <DashBoardSubheader {...this.props} subheader = "Settings"/>
                                <OrganizationSettingsContent subheader = "Settings" editable = {true} departmentFlag = {true} {...this.props}/>
                            </div>
                        </div>
                    </div>
                </div>
            </Fragment>
        )
    }
}

export default OrganizationSettings