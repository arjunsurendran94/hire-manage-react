import React, {Component,Fragment} from 'react'
import OrganizationContent from './content/organizationContent'
import SideBar from './common/sideBar'
import DashBoardHeaderContainer from '../../containers/common/dashBoardHeaderContainer'
import DashBoardSubheader from './common/dashboardSubHeader'



class Organization extends Component{
    render(){
        return( 
            <Fragment>
                <div className="container-fluid">
                    <div className="row">
                        <SideBar/>
                        <div className="col-sm-12 container-body ">
                            <DashBoardHeaderContainer/>
                            <div className="row">
                                <DashBoardSubheader {...this.props} subheader = "organization"/>
                                <OrganizationContent subheader = "organization" {...this.props}/>
                            </div>
                        </div>
                    </div>
                    

                    {/* <form id="change-organization-form" action="/organization/change-organization/" method="post" class="d-none">
                            <input type="hidden" name="csrfmiddlewaretoken" value="Cnm3RJmrZGc6ncEZoy929AWcHEwKqPCOGUPJZ1uRDUuelf6W1bOOOZJQjY0Pb92b">
                        <select name="organization" class="input-control">
                            
                            <option value="ef4b34bb-85ea-4824-93cb-50203b4f77f7" selected="">
                            GreenVistas
                            </option>
                            
                        </select>
                        <input type="text" class="d-none" name="request-page-url" value="/dashboard/">
                        </form> */}

                    </div>
            </Fragment>
        )
    }
    
}

export default Organization


