import React, {Component,Fragment} from 'react'
import SideBar from './common/sideBar'
import DashBoardHeaderContainer from '../../containers/common/dashBoardHeaderContainer'
import DashBoardSubheader from './common/dashboardSubHeader'
import OrganizationDepartmentsContent from './content/organizationDepartmentsContent'

class OrganizationDepartments extends Component{
    constructor(props) {
        super(props);
        this.state = {searchstatus: false};
      }

    componentDidMount(){
        window.scrollTo(0, 0)
    }

    changeSearchStatus=()=>{
        this.setState({searchstatus: true})
    }

    render(){
        return( 
            <Fragment>
                <div className="container-fluid">
                    <div className="row">
                        <SideBar a={true} {...this.props}/>
                        <div className="col-sm-12 container-body ">
                            <DashBoardHeaderContainer/>
                            <div className="row">
                                <DashBoardSubheader {...this.props} changeSearchStatus={this.changeSearchStatus} subheader = "Departments" {...this.props}/>
                                <OrganizationDepartmentsContent searchstatus ={this.state.searchstatus} subheader = "Departments" editable = {false}  departmentFlag = {true} {...this.props}/>
                            </div>
                        </div>
                    </div>

                    </div>
            </Fragment>
        )
    }
}

export default OrganizationDepartments