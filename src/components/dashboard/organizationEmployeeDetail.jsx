import React, {Component,Fragment} from 'react'
import SideBar from './common/sideBar'
import DashBoardHeaderContainer from '../../containers/common/dashBoardHeaderContainer'
import DashBoardSubheader from './common/dashboardSubHeader'
import OrganizationEmployeeDetailContent from './content/organizationEmployeeDetailContent'

class OrganizationEmployeeDetail extends Component{
    componentDidMount(){
        if (this.props.match.params) {
                this.props.retreiveEmployee(this.props.match.params.employee_slug) 
                this.props.listEmployeePermissions(this.props.match.params.employee_slug)      
        }
        this.props.fetchPermissionGroups()
    }
    render(){
        return( 
            <Fragment>
                <div className="container-fluid">
                    <div className="row">
                        {<SideBar a={true} {...this.props}/>}
                        <div className="col-sm-12 container-body ">
                            <DashBoardHeaderContainer/>
                            <div className="row">
                                <DashBoardSubheader subheader = "employeeDetail" {...this.props}/>
                                <OrganizationEmployeeDetailContent  {...this.props}/>
                            </div>
                        </div>
                    </div>
                </div>
            </Fragment>
        )
    }
}

export default OrganizationEmployeeDetail