import React, {Component,Fragment} from 'react'
import { Field, reduxForm } from 'redux-form'
import {countryReorder} from '../../../countryReorder'
import {alphabetOnly} from '../../../constants/validations'

// var letters = /^[-a-zA-Z0-9-()]+(\s+[-a-zA-Z0-9-()]+)*$/;
// const alphabetOnly =  value => {
//     console
//     return(value && value.match(letters) ? undefined : ' must be characters only')
// }

const MODAL_OPEN_CLASS = "modal-open";
const required =  value => {
    return(value || typeof value === 'number' ? undefined : 'is Required')
}

const requiredPlace =  value => {
    return(value || typeof value === 'number' ? undefined : 'Please select your ')
}


const maxLength = max => value =>
  value && value.length > max ? `Must be ${max} characters or less` : undefined
const maxLength15 = maxLength(128)
const maxLength300 = maxLength(300)

const websiteValiadtion = value => {
    if (value) {
        var pattern = new RegExp('^(https?:\\/\\/)' + // protocol
            '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
            '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
            '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
            '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
            '(\\#[-a-z\\d_]*)?$', 'i'); // fragment locator
        return !!pattern.test(value) ? undefined : 'Enter a valid url ( eg:https://websitebuilders.com )'
    }
}

const renderFieldText = ({ input, label, type,rows,cols, textarea, meta: { touched, error, warning, invalid } }) => {
    const textareaType = <textarea {...input} rows={rows} cols={cols} placeholder={label}  type={type} className={`form-control ${touched && invalid ? 'has-danger' : ''}`}/>;
    // const inputType = <input {...input} placeholder={label}  type={type} className={`form-control ${touched && invalid ? 'has-danger' : ''}`}/>;

    return (
        <div>
            <div>
                { textareaType}
                {touched && ((error && <small className="text-danger">Description {error}</small>) || (warning && <span>{warning}</span>))}
            </div>
        </div>
    );
};

const renderField = ({
    input,
    cols,
    rows,
    backend_error,
    existed,
    type,
    label,
    meta: { touched, error, warning,  }
  }) => {
      
    return(
        <Fragment>
            <input {...input} cols={cols} rows={rows} placeholder={label} type={type} className={(touched && error)|| (existed)? 'input-control border border-danger': 'input-control'}/>
            {touched &&
            ((error && <small className="text-danger">{label} {error}</small>) ||
                (warning && <span>{warning}</span>))}

                {/* edit website validation error */}
            {backend_error&&backend_error.organization&&
            backend_error.organization.organizationCreateError&&
            backend_error.organization.organizationCreateError.data&&
            backend_error.organization.organizationCreateError.data.website&&
            <small className="text-danger"> {backend_error.organization.organizationCreateError.data.website[0]} (eg:https://websitebuilders.com) </small>}
            
            {/* create website validation error */}
            {backend_error&&backend_error.organizationCreatedData&&
            backend_error.organizationCreatedData.organizationCreateError&&
            backend_error.organizationCreatedData.organizationCreateError.data&&
            backend_error.organizationCreatedData.organizationCreateError.data.website&&
            <small className="text-danger"> {backend_error.organizationCreatedData.organizationCreateError.data.website[0]} (eg:https://websitebuilders.com) </small>}
            
            {/* organization name exited error*/}
            {existed&&<small className="text-danger">{existed}</small>}

        </Fragment>
    
  )}

  const renderFieldSelect = ({
    input,
    label,
    country,
    countryState,
    city,
    className,
    meta: { touched, error, warning, }
}) => {

    return (


        <Fragment>
            <select {...input} className={(touched && error)? className+' border border-danger': className}>
                <option value="">{label}</option>
                {(country && label === "Country") &&
                    <Fragment>
                        {country.countryList.map((plan, index) => (
                            <option key={index} value={plan.id}>{plan.name_ascii}</option>
                        ))}</Fragment>
                }
                {(countryState && label === "State") &&
                    <Fragment>
                        {countryState.stateList.map((plan, index) => (
                            <option key={index} value={plan.id}>{plan.name_ascii}</option>
                        ))}</Fragment>
                }
                {(city && label === "City") &&
                    <Fragment>
                        {city.cityList.map((plan, index) => (
                            <option key={index} value={plan.id}>{plan.name_ascii}</option>
                        ))}</Fragment>
                }
            </select>
            <div>
                {touched &&
                    ((error && <small className="text-danger">{error}{label}</small>) ||
                        (warning && <span>{warning}</span>))}
            </div>
        </Fragment>

    )
}

class OrganizationEditModal extends Component{
    state={
        image:'',
        image_validation_error:false,
        existed_error:''
    }
    componentDidMount() {
        
        if (this.props.subheader==="Settings"){
            this.props.fetchCountryList();
            this.props.fetchStatesList(this.props.organization.organization_data.country.id);
            this.props.fetchCityList(this.props.organization.organization_data.state.id);

            let data={}
            data.organization_name=this.props.organization.organization_data.organization_name
            data.description=this.props.organization.organization_data.description
            data.country=this.props.organization.organization_data.country.id
            data.state=this.props.organization.organization_data.state.id
            data.city=this.props.organization.organization_data.city.id
            data.address=this.props.organization.organization_data.address
            if(this.props.organization.organization_data.website){
            data.website=this.props.organization.organization_data.website
            }
            this.props.initialize(data)

        }
        // this.props.fetchStatesList(this.props.organization.organization_data.state.id);
        document.body.classList.add(MODAL_OPEN_CLASS);
        
      }
    
    componentWillUnmount() {
        document.body.classList.remove(MODAL_OPEN_CLASS);
      }
    handleCountryChange = (e) => {
        // making fields null
        this.props.change('state','')  
        this.props.change('city','')

        this.props.fetchStatesList(e.target.value);
        this.props.fetchCityList(" ");
    }
    handleStateChange = (e) => {
        // making fields null
        this.props.change('city','')
        
        this.props.fetchCityList(e.target.value);
    }

      upLoadFile =(e)=>{
        let image = e.target.files[0]
        var imageTypes = ['image/png','image/jpg','image/gif','image/jpeg',]
        var image_index = imageTypes.indexOf(image.type)
        if (image_index >= 0){
            this.setState({image:image,image_validation_error:false},() =>{console.log(this.state.image,'uploaded image')})
        }
        else{
            this.setState({image:"",image_validation_error:true})
        }
    }
    submit = (values) => {
        values.logo = this.state.image;
        values.owner = localStorage.getItem("profile_slug")
        const form_data = new FormData();
        for ( var key in values ) {
            form_data.append(key, values[key]);
        }
        (this.props.subheader === "My Profile")?
        (this.props.createOrganization(form_data)):(this.props.editOrganization(form_data))
        
    }
    organisationExistedError = () =>{
        this.setState({existed_error:''})
    }
    componentDidUpdate(prevProps){
        if(this.props.organizationCreatedData!=prevProps.organizationCreatedData){
            this.props.organizationCreatedData.organizationCreateError&&this.props.organizationCreatedData.organizationCreateError&&this.props.organizationCreatedData.organizationCreateError.data&&
            this.props.organizationCreatedData.organizationCreateError.data.non_field_errors&&this.props.organizationCreatedData.organizationCreateError.data.non_field_errors[0]==='The fields owner, organization_name must make a unique set.'&&
            this.setState({existed_error:'Organization Name Already exists'})
        }
        if(this.props.organization!=prevProps.organization){
            this.props.organization&&this.props.organization.organizationCreateError&&this.props.organization.organizationCreateError.data&&
                this.props.organization.organizationCreateError.data.non_field_errors&&
                this.props.organization.organizationCreateError.data.non_field_errors[0]==='The fields owner, organization_name must make a unique set.'&&
                this.setState({existed_error:'Organization Name Already exists'})
        }
    }
    render(){

        const { handleSubmit} = this.props
        if(this.props.organization &&this.props.organization.status==='success'){
            window.location.reload()
        }

        return (
            <Fragment>
                <div id="modal-block">
                    <div className="modal form-modal show" id="editOrganizationModal" role="dialog" style={{paddingRight: "15px", display: "block"}}>
                    <div className="modal-dialog modal-lg">
                        <div className="modal-content">

                        {/* <!-- Modal Header --> */}
                        <div className="modal-header">
                            <h4 className="modal-title">{(this.props.subheader ==="My Profile")?"Add Organization":"Edit Organization"}</h4>
                            <button type="button" className="btn btn-close btn-black" data-dismiss="modal" onClick = {this.props.modalClose}><span aria-hidden="true">×</span></button>
                        </div>

                        {/* <!-- Modal body --> */}
                        <div className="modal-body">
                            <form onSubmit={handleSubmit(this.submit)} id="add-organization-form" className="popup-form" method="post" noValidate>

                            <div className="form-group row">
                                <div className="col-md-4">
                                <label className="form-control-label" htmlFor="id_organization_name">
                                <label htmlFor="id_organization_name">Organization name</label></label> 
                                <span className="text-danger">*</span>
                                </div>
                                <div className="col-md-8">
                                    {/* <input type="text" name="organization_name" value="Azure Interior" placeholder="Enter Organization Name" className="input-control" maxlength="128" required="" id="id_organization_name"/>
                                    <div className="error-message text-danger">                                        
                                    </div> */}
                                    <Field 
                                        type="text"
                                        name="organization_name" 
                                        component={renderField}
                                        existed={this.state.existed_error}
                                        onChange={this.organisationExistedError}
                                        label="Organization Name"
                                        validate={[required,alphabetOnly]}/>
                                </div>
                            </div>

                            <div className="form-group row">
                                <div className="col-md-4">
                                <label className="form-control-label" htmlFor="id_description">
                                <label htmlFor="id_description">Description</label></label>
                                </div>
                                <div className="col-md-8">
                                {/* <textarea name="description" cols="40" rows="10" placeholder="Enter Description" className="input-control" id="id_description"></textarea> */}
                                {/* <Field 
                                    type="textarea" 
                                    cols="40" 
                                    rows="10" 
                                    name="description" 
                                    component={renderField}
                                    placeholder="Enter Description"/> */}
                                    <Field 
                                        type="text" 
                                        cols="40" 
                                        rows="6" 
                                        name="description" 
                                        component={renderFieldText} 
                                        className="input-control"
                                        validate={[maxLength300]}
                                        placeholder="Enter Description" 
                                        label="Enter Description"/>
                                </div>
                            </div>

                            <div className="form-group row">
                                <div className="col-md-4">
                                <label className="form-control-label" htmlFor="id_logo">
                                <label htmlFor="id_logo">Logo</label></label>
                                </div>
                                <div className="col-md-8 ">
                                {/* <input type="file" name="logo" accept="image/*" id="id_logo"/> */}
                                <input type="file" onChange={this.upLoadFile} id="id_logo" accept="image/*"/> 
                                
                                {this.state.image_validation_error  && <small class="text-danger">Upload a valid image. The file you uploaded was either not an image or a corrupted image.</small>}
                                </div>
                            </div>

                            <div className="form-group row">
                                <div className="col-md-4">
                                <label className="form-control-label" htmlFor="id_country">
                                <label htmlFor="id_country">Country</label> 
                                <span className="text-danger">*</span>
                                </label>
                                </div>
                                <div className="col-md-8 ">
                                {/* <select name="country" className="input-control country" required="" id="id_country">
                                    <option value="">Country</option>

                                    <option value="105" selected="">India</option>

                                </select>
                                <div className="error-message text-danger">

                                </div> */}
                                <Field
                                    name="country"
                                    type="select"
                                    country={countryReorder(this.props.countryDetails)}
                                    component={renderFieldSelect}
                                    className="input-control select-country"
                                    label="Country"
                                    validate={requiredPlace}
                                    onChange={this.handleCountryChange} />
                                </div>
                            </div>

                            <div className="form-group row">
                                <div className="col-md-4">
                                <label className="form-control-label" htmlFor="id_state">
                                <label htmlFor="id_state">State</label> 
                                <span className="text-danger">*</span>
                                </label>
                                </div>
                                <div className="col-md-8 ">
                                {/* <select name="state" className="input-control state" required="" id="id_state">
                                    <option value="">State</option>s
                                    <option value="1350">Andaman and Nicobar</option>
                                </select>
                                <div className="error-message text-danger">
                                    
                                </div> */}
                                <Field
                                    name="state"
                                    type="select"
                                    countryState={this.props.stateDetails}
                                    component={renderFieldSelect}
                                    className="input-control select-state"
                                    label="State"
                                    validate={requiredPlace}
                                    onChange={this.handleStateChange}
                                />
                                </div>
                            </div>

                            <div className="form-group row">
                                <div className="col-md-4">
                                <label className="form-control-label" htmlFor="id_city">
                                <label htmlFor="id_city">City</label> 
                                <span className="text-danger">*</span>
                                </label>
                                </div>
                                <div className="col-md-8 ">
                                <Field
                                    name="city"
                                    type="select"
                                    city={this.props.cityDetails}
                                    component={renderFieldSelect}
                                    className="input-control select-city"
                                    label="City"
                                    validate={requiredPlace} />
                                </div>
                            </div>

                            <div className="form-group row">
                                <div className="col-md-4">
                                <label className="form-control-label" htmlFor="id_address">
                                <label htmlFor="id_address">Address</label></label>
                                </div>
                                <div className="col-md-8 ">
                                {/* <textarea name="address" cols="40" rows="10" placeholder="Enter Address" className="input-control" id="id_address">3404  Edgewood Road</textarea> */}
                                <Field 
                                    type="text" 
                                    name="address" 
                                    cols="40" 
                                    rows="5" 
                                    component={renderFieldText} 
                                    className="input-control input-control"
                                    placeholder="Enter Address"
                                    label="Enter Address"
                                    validate={[maxLength300]} />
                                </div>
                            </div>

                            <div className="form-group row">
                                <div className="col-md-4">
                                <label className="form-control-label" htmlFor="id_website">
                                <label htmlFor="id_website">Website</label></label>
                                </div>
                                <div className="col-md-8">
                                {/* <input type="url" name="website" placeholder="Enter Website" className="input-control" maxlength="200" id="id_website"/>
                                <div className="error-message text-danger"></div> */}
                                <Field 
                                    type="url" 
                                    name="website" 
                                    component={renderField} 
                                    className="input-control input-control"
                                    placeholder="Enter Website" 
                                    validate={websiteValiadtion}
                                    backend_error={this.props}
                                    />
                                </div>
                            </div>

                            </form>
                        </div>

                        {/* <!-- Modal footer --> */}
                        <div className="modal-footer">
                            <input type="submit" form="add-organization-form" className="btn width btn-success" value="SAVE"/>
                            <button type="button" className="btn btn-secondary" data-dismiss="modal" onClick = {this.props.modalClose}>Close</button>
                        </div>

                        </div>
                    </div>
                    </div></div>
                    <div className="modal-backdrop show"></div>
            </Fragment>
        )
    }
}

OrganizationEditModal = reduxForm({
    form: 'OrganizationEditModal'
})(OrganizationEditModal)
export default OrganizationEditModal
// const mapStateToProps = state => {
//     if(state.retrieveOrganization&&state.retrieveOrganization.organization_data){
//     initial = state.retrieveOrganization.organization_data
//     }
    
//     return{
//     initialValues:initial
//     }
//     }
    
// export default connect(
// mapStateToProps
// )(reduxForm({ form: "OrganizationEditModal", enableReinitialize: true })(OrganizationEditModal));