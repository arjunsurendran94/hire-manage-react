import React, { Component, Fragment } from 'react'
import { Field, FieldArray, reduxForm } from 'redux-form'
import { Redirect } from 'react-router-dom';
import { Link } from 'react-router-dom'
import { alphabetOnly } from '../../../constants/validations'
import { countryReorder } from '../../../countryReorder'
import Select from 'react-select';
import * as Helper from '../common/helper/helper';
import PortalPasswordModal from '../modal/addClientModal';
import LoaderImage from '../../../static/assets/image/loader.gif';
import 'react-quill/dist/quill.snow.css';
import ReactQuill, { Quill } from 'react-quill';
import AddSubTaskModal from './addSubTaskModal'
import ErrorMessage from '../../common/ErrorMessage'
import TimeField from 'react-simple-timefield';
const re = /^[0-9\b]+$/;

// var loaderStyle={
//   width: '3%',
//   position: 'fixed',
//   top: '50%',
//   left: '57%'
// }

// const required = value => {
//   return (value || typeof value === 'number' ? undefined : ' is Required')
// }
// const mobileValid = value => {
//   var mobilenoregex = /^(?!\b(0)\1+\b)(\+?\d{1,3}[. -]?)?\(?\d{3}\)?([. -]?)\d{3}\3\d{4}$/
//   if (value && value.length >= 1) {
//     return (mobilenoregex.test(value) ? undefined : ' is Invalid')
//   }

//   return undefined
// }
let Inline = Quill.import('blots/inline');
class BoldBlot extends Inline { }
BoldBlot.blotName = 'bold';
BoldBlot.tagName = 'strong';
Quill.register('formats/bold', BoldBlot);

const formats = ["bold"]
const email = value =>
  value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
    ? ' is Invalid'
    : undefined
const websiteValiadtion = value => {
  if (value) {
    var pattern = new RegExp('^(https?:\\/\\/)' + // protocol
      '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
      '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
      '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
      '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
      '(\\#[-a-z\\d_]*)?$', 'i'); // fragment locator
    return !!pattern.test(value) ? undefined : 'Enter a valid url ( eg:https://websitebuilders.com )'
  }
}
const salutationType = [
  { value: 'mr', text: 'Mr' },
  { value: 'ms', text: 'Ms' },
  { value: 'mrs', text: 'Mrs' }
]
const currency = [{ value: "abc", label: "asdf" }, { value: "wer", label: "uji" }]
const renderField = ({
  input,
  label,
  type,
  existed,
  work_phone_number_error,
  mobile_number_error,
  website_error,
  facebook_error,
  pin_code_error,
  meta: { touched, error, warning, }
}) => {

  return (


    <Fragment>
      <input {...input} placeholder={label} type={type} className={(touched && error) ||
        (existed && (label === 'Contact Email')) || (work_phone_number_error) || (mobile_number_error) || (website_error) || (facebook_error) || (pin_code_error)
        ? 'fullwidth input-control border border-danger' : 'fullwidth input-control'} />
      {touched &&
        ((error && <small className="text-danger">{label}{error}</small>) ||
          (warning && <span>{warning}</span>))}

      {(label === 'Contact Email') && (existed) && <small class="text-danger">
        {existed}
      </small>}
      {(work_phone_number_error) && <small class="text-danger">
        {work_phone_number_error}
      </small>}
      {(mobile_number_error) && <small class="text-danger">
        {mobile_number_error}
      </small>}
      {(website_error) && <small class="text-danger">
        {website_error}
      </small>}
      {(facebook_error) && <small class="text-danger">
        {facebook_error}
      </small>}
      {(pin_code_error) && <small class="text-danger">
        {pin_code_error}
      </small>}
    </Fragment>

  )
}



const renderFieldSelect = ({
  input,
  label,
  type,
  id,
  existed,
  options,
  country,
  countryState,
  city,
  meta: { touched, error, warning, }
}) => {

  return (
    <Fragment>
      {(options && label === "Salutation") &&
        <select {...input} className={(touched && error) ? 'fullwidth input-control border border-danger' : 'fullwidth input-control'}>
          <option id='salutation_id' value="">Select Salutation</option>
          {options.map((salutation) =>
            <option value={salutation.value} >{salutation.text}</option>
          )}
        </select>
      }
      {(options && label === "Branch") &&
        <Fragment>
          <div className="col-sm-6 form-group">
            <label className="text-light-grey" htmlFor="id_branch">
              Branch
          </label>
            <select {...input} className="fullwidth input-control" >
              <option id='no_designation' value="">Select Branch</option>
              {options.map((branch, index) =>
                <option key={index} value={branch.slug} >{branch.branch_name}  </option>
              )}
            </select>
          </div>
        </Fragment>
      }
      {(options && label === "Department") &&
        <select {...input} className="fullwidth input-control">
          <option id='no_designation' value="">Select Department</option>
          {options.map((department, index) =>
            <option key={index} value={department.slug} >{department.department_name}  </option>
          )}
        </select>
      }

      {(label === "Country" || label === "State" || label === "City") &&
        <select {...input} className={(touched && error) ? 'input-control border border-danger' : 'input-control'}>
          <option value="">{label}</option>
          {(country && label === "Country") &&
            <Fragment>
              {country.countryList.map((plan, index) => (
                <option key={index} value={plan.id}>{plan.name_ascii}</option>
              ))}</Fragment>
          }
          {(countryState && label === "State") &&
            <Fragment>
              {countryState.stateList.map((plan, index) => (
                <option key={index} value={plan.id}>{plan.name_ascii}</option>
              ))}</Fragment>
          }
          {(city && label === "City") &&
            <Fragment>
              {city.cityList.map((plan, index) => (
                <option key={index} value={plan.id}>{plan.name_ascii}</option>
              ))}</Fragment>
          }
        </select>
      }

      {(label === "Currency") &&
        <Fragment>
          <select {...input} name="currency" className="input-control" >
            {options.map((option) => (
              <option key={option.value} id={option} value={option.value}>{option.label}</option>
            ))}
          </select>
        </Fragment>
      }

      {
        (label === "Portal Language") &&
        <Fragment>
          <select {...input} className="input-control " >
            <option value="">Select Portal Language</option>
            <option value="English">English</option>
            {/* {Helper.languages.map(option=>{
                                  return <option key={option.value} id={option} value={option.value}>{option.label}</option>
                                })} */}
          </select>
        </Fragment>
      }

      {
        (label === "Payment Terms") &&
        <Fragment>
          <select {...input} className="input-control ">
            <option value="">Select Payment Terms</option>
            {options.map(option =>
              <option key={option.value} value={option.value}>{option.text}</option>
            )}
          </select>
        </Fragment>
      }
      {
        (label === "Enable Portal") &&
        <Fragment>

        </Fragment>
      }

      <div>
        {touched &&
          ((error && <small className="text-danger">{label}{error}</small>) ||
            (warning && <span>{warning}</span>))}
      </div>

    </Fragment>
  )
}


const radioCheckbox = ({ input, label, options, type, checked, meta: { touched, error, warning } }) => (
  <Fragment>

    {options.map((plan, index) => (
      <div className="col-md-3" key={index}>
        <input type="radio" {...input} checked={checked} key={index} name={plan.name} value={plan.value} id={plan.id} /> {plan.text}
      </div>
    ))
    }

    {touched &&
      ((error && <small className="text-danger">{label}{error}</small>) ||
        (warning && <span>{warning}</span>))}


  </Fragment>
)

const renderCheckBoxField = ({ input, label, options, type, checked, meta: { touched, error, warning } }) => (
  <Fragment>
    <input type="checkbox" {...input} />

  </Fragment>
)

const renderContactPerson = ({ fields, meta: { error, submitFailed } }) => (
  <ul>
    <li>
      <a className="add-row" href="javascript:void(0)">
        <div className="row">
          <div className="col-sm-12 text-center">
            <button type="button" className="btn btn-outline-primary add-row" onClick={() => fields.push({})}>
              <i className="icon-plus"></i> Add Contact Persons
                              </button>
          </div>
        </div>
      </a>

      {/* {submitFailed && error && <span>{error}</span>} */}
    </li>
    {fields.map((member, index) => (
      <li key={index}>
        {/* <button
          type="button"
          title="Remove Member"
          onClick={() => fields.remove(index)}
        /> */}


        <div className="row mb-3 formset_row dynamic-form">
          <div className="col-sm-6 form-group">
            <label className="text-light-grey" for="id_contactperson_set-0-salutation">Salutation<span class="text-danger">*</span></label>

            <Field
              component={renderFieldSelect}
              name={`${member}.contactperson_salutation`}
              id="salutation_id"
              label="Salutation"
              validate={required}
              options={salutationType}
            />

          </div>
          <div className="col-sm-6 form-group">
            <label className="text-light-grey" htmlFor="id_contactperson_set-0-first_name">First name<span className="text-danger">*</span></label>
            <Field type="text"
              name={`${member}.contactperson_first_name`}
              component={renderField}
              label="First Name"


              validate={[required, alphabetOnly]}
            />
          </div>
          <div className="col-sm-6 form-group">
            <label className="text-light-grey" htmlFor="id_contactperson_set-0-last_name">Last name </label>
            <Field type="text"
              name={`${member}.contactperson_last_name`}
              component={renderField}
              label="Last Name"


              validate={[required, alphabetOnly]}
            />
          </div>
          <div className="col-sm-6 form-group">
            <label className="text-light-grey" htmlFor="id_contactperson_set-0-email">Email </label>
            <Field
              name={`${member}.contactperson_email`}
              component={renderField}
              label="Contact Email"


              validate={[required, email]}
            />
          </div>
          <div class="col-sm-6 form-group">
            <label class="text-light-grey" for="id_contactperson_set-0-phone_number">Phone number </label>
            <Field type="text"
              name={`${member}.contactperson_mobile_number`}
              component={renderField}
              label="Mobile Number"


              validate={mobileValid}
            />
          </div>
          {/* <div class="col-sm-6 form-group">
                              <label class="text-light-grey" for="id_contactperson_set-0-designation">Designation </label>
                              <input type="text" name="contactperson_set-0-designation" placeholder="Designation" maxlength="128" className="input-control" id="id_contactperson_set-0-designation"/> 
                            </div> */}
          <div className="col-sm-6 form-group">
            <label className="text-light-grey">Skype Name/Number
                              </label>
            <Field type="text"
              name={`${member}.contactperson_skype_name`}
              component={renderField}
              label="Skype Name/Number"



            />
          </div>
          <div className="col-sm-12 text-right">
            <a className="delete-row btn btn-outline-primary"
              href="javascript:void(0)"
              onClick={() => fields.remove(index)}
            ><i class=" icon-delete"></i> remove</a>



          </div>
          <div className="col-sm-12"><hr></hr></div>


        </div>


      </li>
    ))}
  </ul>
)


const MODAL_OPEN_CLASS = "modal-open";

const required = value => {
  return (value || typeof value === 'number' ? undefined : ' is Required')
}

const mobileValid = value => {
  var mobilenoregex = /^(?!\b(0)\1+\b)(\+?\d{1,3}[. -]?)?\(?\d{3}\)?([. -]?)\d{3}\3\d{4}$/
  if (value && value.length >= 1) {
    return (mobilenoregex.test(value) ? undefined : 'Enter a Valid Mobile Number')
  }

  return undefined
}

const maxLength = max => value =>
  value && value.length > max ? `Must be ${max} characters or less` : undefined

const maxLength128 = maxLength(128)

// const renderField = ({
//     input,
//     label,
//     type,
//     existed,
//     backenderror,
//     meta: { touched, error, warning,  }
//   }) => {

//     return(
//       <Fragment>
//         <input {...input} placeholder={label} type={type} className={(touched && error)||(existed&&existed.existedMail) ||(backenderror) ? 'input-control border border-danger': 'input-control'}/>
//         {touched &&
//           ((error && <small className="text-danger">{label}{error}</small>) ||
//             (warning && <span>{warning}</span>))}
//         {((existed)&&(existed.existedMail)) && <small className="text-danger">
//             {existed.error.data.user.email[0]}
//         </small>}
//         {backenderror&&
//             <small className="text-danger">{backenderror}</small>
//             }
//         {/* {((existed)&&(existed.error.status == 500)) && <small class="text-danger">
//             {existed.errors.error}
//         </small>} */}

//       </Fragment>   
// )}


const rendermobileField = ({ input, label, type, backenderror, id, className, accept, initialValue, name, meta: { touched, error, warning, } }) => {
  return (
    <Fragment>
      <input {...input} placeholder={label} type={type} className={className} id={id} accept={accept} defaultValue={initialValue} />
      {touched &&
        ((error && <small className="text-danger">{error} {label == "Phone number" && 'with country code'}</small>) ||
          (warning && <span>{warning}</span>))}
      {backenderror &&
        <small className="text-danger">{backenderror} with country code</small>
      }
    </Fragment>
  )
}


const renderFieldLocationSelect = ({
  input,
  label,
  country,
  countryState,
  city,
  noCity,
  className,
  meta: { touched, error, warning, }
}) => {

  return (
    <Fragment>
      <select {...input} className={(touched && error) ? 'input-control border border-danger' : 'input-control'}>
        <option value="">{label}</option>
        {(country && label === "Country") &&
          <Fragment>
            {country.countryList.map((plan, index) => (
              <option key={index} value={plan.id}>{plan.name_ascii}</option>
            ))}</Fragment>
        }
        {(countryState && label === "State") &&
          <Fragment>
            {countryState.stateList.map((plan, index) => (
              <option key={index} value={plan.id}>{plan.name_ascii}</option>
            ))}</Fragment>
        }
        {(city && label === "City") &&
          <Fragment>
            {city.cityList.map((plan, index) => (
              <option key={index} value={plan.id}>{plan.name_ascii}</option>
            ))}</Fragment>
        }
      </select>
      <div>
        {touched &&
          ((error && <small className="text-danger">{label}{error}</small>) ||
            (warning && <span>{warning}</span>))}
      </div>
      {(noCity && noCity.employeeUpdateError && noCity.employeeUpdateError.data && noCity.employeeUpdateError.data.city && noCity.employeeUpdateError.status === 400) && <small class="text-danger">
        Please select the city
        </small>}




    </Fragment>


  )
}


const renderRadioField = ({ input, label, type, checked }) => (
  <Fragment>

    <label>{label}</label>
    <input {...input} type={type} checked={checked} />

  </Fragment>
);

const checkboxField = ({ input, options, className, makeCheck, edit, type, checked, meta: { touched, error, warning } }) => (
  <Fragment>

    {options.map((option, index) => (
      <div className="col-lg-4 col-md-6 mt-3" key={index}>
        <label htmlFor="162">
          <span className="checkbox">
            <input type="checkbox" id={option.slug} className={className} name={`${input.name}[${index}]`} value={option.slug} checked={makeCheck.length > 0 ? (makeCheck.indexOf(option.slug) !== -1) : (input.value.indexOf(option.slug) !== -1)}
              onChange={(event) => {

                if (makeCheck.length > 0) {
                  const newValue = [...makeCheck]
                  if (event.target.checked) {
                    newValue.push(option.slug);
                    makeCheck.push(option.slug);
                  } else {

                    newValue.splice(newValue.indexOf(option.slug), 1);
                    makeCheck.splice(makeCheck.indexOf(option.slug), 1);
                  }
                  return input.onChange(newValue);
                } else {
                  const newValue = [...input.value];
                  if (event.target.checked) {
                    newValue.push(option.slug);
                  } else {
                    newValue.splice(newValue.indexOf(option.slug), 1);
                  }
                  return input.onChange(newValue);

                }
              }} />
          </span>
          {option.group_name}
        </label>
      </div>
    ))}

    <label>{touched &&
      ((error && <small className="text-danger">{error}</small>) ||
        (warning && <span>{warning}</span>))}</label>
  </Fragment>
)
var role_List = [], orginal_list = []
const roleListData = (role, index) => {

  // role_List=[]
  role_List.push(role.slug)
  orginal_list = role_List
}
const renderTextField = ({
  input,
  label,
  type,

  meta: { touched, error, warning, }
}) => {
  return (
    <Fragment>
      <textarea {...input} placeholder={label} type={type} className={(touched && error

        ? 'fullwidth input-control border border-danger' : 'fullwidth input-control')} />
      {touched &&
        ((error && <small className="text-danger">{label}{error}</small>) ||
          (warning && <span>{warning}</span>))}

    </Fragment>
  )
}
const renderDateField = ({ input, label, options, type, startingDate, checked, meta: { touched, error, warning } }) => (
  <Fragment>

    <input {...input} type="date"
      name="start_date"
      className="fullwidth input-control"
      label="Start Date"
      id="myDate"
      value={startingDate}

    />

    {touched &&
      ((error && <small className="text-danger">{label}{error}</small>) ||
        (warning && <span>{warning}</span>))}
  </Fragment>
)
class UpdateEmployee extends Component {
  constructor(props) {
    super(props)
    this.state = {
      show_branch: false,
      contactNoError: '',
      pincodeError: '',
      other_details: true,
      address_active: false,
      contactPerson_active: false,
      remarks_active: false,
      editModal: false,

      editPart: '',
      index: '',

      contractAttachment: '',
      termAttachment: '',
      errors: {
        contractAttachment: '',
        termAttachment: '',
      },
      billed_hours:(this.props.projectData?.weeklyworksheets?.find(i=>i.id===this.props.index)?.billed)?
      this.props.projectData?.weeklyworksheets?.find(i=>i.id===this.props.index)?.billed:'0',
      payment_status:this.props.projectData?.weeklyworksheets?.find(i=>i.id===this.props.index)?.payment_status,
      weekly_pm_approved:this.props?.calculateBillablehours(this.props.projectData?.weeklyworksheets?.find(i=>i.id===this.props.index)?.worksheets)
    }
  }

  closeSuccessMessage = () => {
    this.setState({
      success_message: false
    })
  }
  addTaskModal = (editPart, index) => {
    console.log("editpart", editPart)
    this.setState({ editModal: true, editPart: editPart, index: index })
  }
  closeEditModal = () => {
    this.setState({ editModal: false, editPart: '', index: '' })
  }

  validateContract = (e) => {
    e.preventDefault()
    if (this.props.editPart === 'contractAttachment') {
      if (!this.state.contractAttachment) {
        let errors = this.state.errors
        errors.contractAttachment = 'Contract Attachment is Empty'

        this.setState({ errors: errors })
      }
      else {
        let errors = this.state.errors
        errors.contractAttachment = ''

        this.setState({ errors: errors })

        let data = {}
        data.contract = this.state.contractAttachment
        this.props.addContractAttachment(data, this.props.index)

      }
    }
    if (this.props.editPart === 'termAttachment') {
      if (!this.state.termAttachment) {
        let errors = this.state.errors
        errors.termAttachment = 'Term Attachment is Empty'

        this.setState({ errors: errors })
      }
      else {
        let errors = this.state.errors
        errors.termAttachment = ''

        this.setState({ errors: errors })

        let data = {}
        data.termAttachment = this.state.termAttachment
        if(this.props && this.props.index && this.props.index.resource===null){
          data.resource=''
        }
        else{
        data.resource = this.props && this.props.index && this.props.index.resource
        }
        data.financial_info=this.props.projectObj&&this.props.projectObj.financial_info&&this.props.projectObj.financial_info.id

        console.log('aaaaaaaaaaaaaaaaaaaa', data);
        this.props.addTermAttachment(data)
      }

    }

  }

  handleWeeklyEstimated = (e) => {
    if (e.target.value === "" || re.test(e.target.value)) {
      
      this.setState({ [e.target.name]: e.target.value });
    } else {
      e.target.value = null;
      this.setState({ [e.target.name]: "" });
    }
  };

  editWeeklyWorksheet=(e)=>{
    
    e.preventDefault();
    let data={
      payment_status:(this.state.billed_hours==='0' ||this.state.billed_hours==='')?'NA':this.state.payment_status,
      billed:this.state.billed_hours===''?'0':this.state.billed_hours
    }
    
    this.props.editEmployeeWeeklyWorksheet(this.props.index,data,this.props.weeklyWorksheetEmp,this.props.projectObj?.project?.slug,this.props.projectObj?.project?.billing_type)
  }

  componentDidMount() {
    document.body.classList.add(MODAL_OPEN_CLASS);
  }

  componentDidUpdate(prevProps) {
    if (this.props.employees != prevProps.employees && this.props.employees && this.props.employees.employeeUpdateError && this.props.employees.employeeUpdateError.data) {
      if (this.props.employees.employeeUpdateError.data.phone) {
        this.setState({ contactNoError: this.props.employees.employeeUpdateError.data.phone[0] })
      }
      if (this.props.employees.employeeUpdateError.data.non_field_errors && this.props.employees.employeeUpdateError.data.non_field_errors[0] === 'Invalid pincode') {
        this.setState({ pincodeError: this.props.employees.employeeUpdateError.data.non_field_errors[0] })
      }
    }

    console.log('wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww');

    if (prevProps.contractAttachmentError != this.props.contractAttachmentError) {

      let Errors = { ...this.state.errors }
      Errors.contractAttachment = this.props.contractAttachmentError
      this.setState({ errors: Errors, contractAttachment: this.props.contractAttachment })
    }

    if (prevProps.termAttachmentError != this.props.termAttachmentError) {

      let Errors = { ...this.state.errors }
      Errors.termAttachment = this.props.termAttachmentError
      this.setState({ errors: Errors })
    }

    if (this.props.contractAttachment != prevProps.contractAttachment) {
      this.setState({ contractAttachment: this.props.contractAttachment })
    }
    if (this.props.termAttachment != prevProps.termAttachment) {
      this.setState({ termAttachment: this.props.termAttachment })
    }
    // if(this.props.Error!=prevProps.Error){
    //   if(this.props.Error&&this.props.Error.contractAttachment!=''){
    //     this.setState({contractAttachmentError:this.props.Error.contractAttachment})
    //   }
    //   if(this.props.Error&&this.props.Error.termAttachment!=''){
    //     this.setState({termAttachmentError:this.props.Error.termAttachment})
    //   }
    // }
    
  }

  componentWillUnmount() {
    role_List = []
    this.props.employeePermissions && this.props.employeePermissions.employeePermissions && this.props.employeePermissions.employeePermissions.permission_groups &&
      this.props.employeePermissions.employeePermissions.permission_groups.map(role => role_List.push(role.slug))

    document.body.classList.remove(MODAL_OPEN_CLASS);
  }

  validateMethod = () => {
    if (this.state.show_branch) {
      return required
    } else {
      return null
    }
  }

  submit = (values) => {
    if (Object.keys(values).length !== 0) {


      if ("permission_groups" in values) {
        this.props.updateEmployeePermissions(this.props.employeeObject.employeeData.slug, values)
      } else {

        if ("owner_type" in values) {
          if (values.owner_type === "organization") {
            if ("branch" in values) {
              values.branch = '';
            }
          }
        }
        this.props.partialUpdateEmployee(this.props.employeeObject.employeeData.slug, values)
      }
    } else {
      console.log('...')
    }

  }
  clearBackendError = (params) => {

    if (params === 'contact-no') {
      this.setState({ contactNoError: '' })
    }
    if (params === 'pincode') {
      this.setState({ pincodeError: '' })
    }
  }

  render() {
    
    // console.log('stateeeeeee',this.state.editPart)
    console.log('this.props.index',this.props.index);
    console.log('this.props.projectData?.weeklyworksheets',this.props.projectData?.weeklyworksheets);
    
    

    const Currency = Helper.Currency

    console.log("state", this.props)
    console.log("state Error", this.state.errors)
    // console.log("stateeeeeee", this.state.contractAttachmentError,this.state.termAttachmentError)

    const userList = [
      { label: "user 1", value: 1, name: "Abc" },
      { label: "user 2", value: 2, name: "Mathu" },
      { label: "user 3", value: 3, name: "John" },
    ];
    let payment_list = [
      { label: "Paid", value: "paid", name: "Paid" },
      { label: "NA", value: "NA", name: "NA" },
      { label: "Not yet paid", value: "not_yet_paid", name: "Not yet paid" },
    ];
    const status = [
      { label: "Created", value: 1, name: "Created" },
      { label: "Started", value: 2, name: "Started" },
      { label: "Oh Hold", value: 3, name: "Oh Hold" },
      { label: "Completed", value: 4, name: "Completed" },
      { label: "Testing", value: 5, name: "Testing" },
      { label: "Re opened", value: 6, name: "Re opened" },
      { label: "Cancelled", value: 7, name: "Cancelled" },
      { label: "Approved", value: 8, name: "Approved" },
      { label: "Billed", value: 9, name: "Billed" },
    ];
    const { handleSubmit } = this.props;
    return (
      <Fragment>
        <div id="modal-block">
          <div className="modal form-modal show" id="editBasicDetails" tabIndex="-1" role="dialog" aria-labelledby="TermsLabel" style={{ paddingRight: "15px", display: "block" }}>
            <div className="modal-dialog modal-dialog-centered" role="document">
              <div className="modal-content">
                <div className="modal-header">
                  {this.props.editPart === "contractAttachment" ?
                    <h5 className="modal-title" id="TermsLabel">Upload Contract Attachment</h5> : null}
                  {this.props.editPart === "termAttachment" ?
                    <h5 className="modal-title" id="TermsLabel">Upload Term Attachment</h5> : null}

                  {this.props.editPart === "subtask" ?
                    <h5 className="modal-title" id="TermsLabel">Add Sub Task Details</h5> :
                    // <h5 className="modal-title" id="TermsLabel"># Task 1</h5>
                    null}
                    {this.props.editPart === "editprojectdetails" ?
                    <h5 className="modal-title" id="TermsLabel">Edit Project Details</h5> :
                    // <h5 className="modal-title" id="TermsLabel"># Task 1</h5>
                    null}
                  {this.props.editPart === "edittask" ?
                    <h5 className="modal-title" id="TermsLabel">Edit Task Details</h5> :
                    // <h5 className="modal-title" id="TermsLabel"># Task 1</h5>
                    null}

                  <button onClick={this.props.closeModal} type="button" className="btn btn-black close-button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                </div>
                {this.props.editPart === "subtask" ?
                  <div className="modal-body" >

                    <form className="row" id="create-project-form" onSubmit={handleSubmit(this.submit)} enctype="multipart/form-data">
                      <div className="col-sm-12 form-group">
                        <label for="id_project_name" >
                          Parent Task name &nbsp;:&nbsp; <span className="text-light-grey"># Task 1</span>
                        </label>

                      </div>
                      <div className="col-sm-12 form-group">
                        <label for="id_project_name" className="text-light-grey">
                          Sub Task name<span className="text-danger">*</span>
                        </label>

                        <Field type="text"
                          name="project_name"
                          component={renderField}
                          label=" Sub Task Name"
                          validate={required}
                          onChange={this.clearExistedError}
                          existed={this.state.existed_error}
                        />

                      </div>


                      <div className="modal-footer">
                        <input type="submit" className="btn btn-success" value="Save" />
                      </div>
                    </form>

                  </div>

                  : null}
                {(this.props.editPart === "contractAttachment" ||this.props.editPart === "termAttachment") ?
                  <div className="modal-body">

                    {this.props.projectData && this.props.projectData.status === 'error'
                      && <ErrorMessage closemsg={this.CloseMsg}
                        msg='An Error Occured please refresh the page' />}

                    <form className="row" id="create-project-form" enctype="multipart/form-data">
                      <div className="col-sm-12 form-group">
                        <label className="text-light-grey" for="id_estimated_time">{this.props.editPart === "contractAttachment" && 'Contract Attachment'}
                          {this.props.editPart === "termAttachment" && 'Term Attachment'}
                        </label>
                        <input type="file" className="fullwidth input-control"
                          name={this.props.editPart}
                          onChange={(event) => this.props.upLoadAttachment(event)}
                          className="form-control"
                          id="id_contract_formset_set-0-contract" />
                        {/* {this.props.contractAttachmentError} */}
                        <span className="text-danger">{this.props.editPart === 'contractAttachment' && this.state.errors.contractAttachment}
                          {this.props.editPart === 'termAttachment' && this.state.errors.termAttachment}
                        </span>
                      </div>
                      <div className="modal-footer">
                        <button onClick={(e) => this.validateContract(e)} className="btn btn-success">Save</button>
                      </div>
                    </form>
                  </div>
                  : null}
                  {(this.props.editPart === "editprojectdetails") ?
                  <div className="modal-body">

                    

                    <form className="row" id="create-project-form" enctype="multipart/form-data">
                    <div className="col-sm-12 form-group" style={{textAlign:"initial"}}>
                        <label
                          className="text-light-grey"
                          for="id_estimated_time"
                        >
                          PM Approved&nbsp;:&nbsp;{this.state.weekly_pm_approved}
                        </label>
                        
                      </div>
                    <div className="col-sm-6 form-group">
                        <label for="id_project_name" className="text-light-grey">
                          Billed Hours<span className="text-danger">*</span>
                        </label>

                        <input
                          type="text"
                          name="billed_hours"
                          label="Billed Hours"
                          className="fullwidth input-control"
                          // validate={required}
                          placeholder="Billed Hours"
                          value={this.state.billed_hours}
                          onChange={(e) => this.handleWeeklyEstimated(e)}
                        // existed={this.state.existed_error}
                        />

                      </div>
                      <div className="col-sm-6 form-group">
                        <label for="id_client" className="text-light-grey">
                          Payment Status
                        </label>

                        <Select
                          options={payment_list}
                          value={payment_list.filter(
                            (i) => i.value === this.state.payment_status
                          )}
                          onChange={(e) =>
                            this.setState({ payment_status: e.value })
                          }
                        />
                       
                      </div>

                      <div className="modal-footer" style={{marginLeft:"4px"}}>
                        <button
                          onClick={(e) =>this.editWeeklyWorksheet(e)}
                          className="btn btn-success"
                        >
                          Save
                        </button>
                      </div>
                    </form>
                  </div>
                  : null}

                {this.props.editPart === "edittask" ?
                  <div className="modal-body" >

                    <form className="row" id="create-project-form" onSubmit={handleSubmit(this.submit)} enctype="multipart/form-data">
                      <div className="col-sm-6 form-group">
                        <label for="id_project_name" >
                          Task Name
                     </label>

                        <Field type="text"
                          name="project_name"
                          component={renderField}
                          label="Task Name"
                          validate={required}
                          onChange={this.clearExistedError}
                          existed={this.state.existed_error}
                        />

                      </div>

                      <div className="col-sm-6 form-group">
                        <label for="id_estimated_time">
                          Estimated Hour(hr)
                     </label>
                        <Field type="text"
                          name="estimated_time"
                          component={renderField}
                          label="Estimated Hours"

                        />
                      </div>

                      <div className="col-sm-6 form-group">
                        <label for="id_client">
                          Created By

                     </label>
                        <Field type="text"
                          name="project_name"
                          component={renderField}
                          label="Name"
                          validate={required}
                          onChange={this.clearExistedError}
                          existed={this.state.existed_error}
                        />
                      </div>
                      <div className="col-sm-6 form-group">
                        <label for="id_client">
                          Assigned To
                     </label>
                        <Select options={userList} />
                      </div>
                      <div className="col-sm-6 form-group">
                        <label for="id_client">
                          Status

                     </label>
                        <Select options={status} />
                      </div>
                      <div className="col-sm-6 form-group">
                        <label for="id_client">
                          Due Date
                     </label>
                        <Field
                          type='text'
                          name="start_date"
                          component={renderDateField}
                          label="Start Date"

                          validate={required}
                          onChange={this.dateChange}
                        />
                      </div>
                      <div className="col-sm-6 form-group">
                        <label>Sub Task</label>
                        <p className="text-light-grey">No sub tasks available</p>
                      </div>


                      <div className="col-sm-12 form-group">
                        <label for="id_project_description" className="text-light-grey">
                          Comments<span className="text-danger">*</span>
                        </label>
                        <ReactQuill>
                          <div className="my-editing-area" />
                        </ReactQuill>
                        {/* <Field type="text"
                        name="project_description"
                        cols="40" rows="10"
                        component={renderTextField}
                        validate={required}
                        className="input-control input-control"
                        label="Task Description"
                        placeholder="Project Description" /> */}
                      </div>



                      <div className="modal-footer">
                        <input type="submit" className="btn btn-success" value="Save" />
                      </div>
                    </form>


                  </div>


                  : null}

              </div>
            </div>
          </div>
        </div>
        <div className="modal-backdrop show"></div>

      </Fragment>
    )
  }
}
UpdateEmployee = reduxForm({
  form: 'updateEmployeeForm'
})(UpdateEmployee)
export default UpdateEmployee