import React, { Component, Fragment } from "react";
import { Field, FieldArray, reduxForm } from 'redux-form'
import { alphabetOnly } from '../../../constants/validations'
import { countryReorder } from '../../../countryReorder'

import * as Helper from '../common/helper/helper';
import Loader from '../../../components/common/Loader'
const Currency = Helper.Currency
const PaymentTerms = [
    { text: 'Fixed', value: 'fixed' },
    { text: 'Hourly', value: 'hourly' }
]
const MODAL_OPEN_CLASS = "modal-open";

const required = value => {
    return (value || typeof value === 'number' ? undefined : ' is Required')
}
const mobileValid = value => {
    var mobilenoregex = /^(?!\b(0)\1+\b)(\+?\d{1,3}[. -]?)?\(?\d{3}\)?([. -]?)\d{3}\3\d{4}$/
    if (value && value.length >= 1) {
        return (mobilenoregex.test(value) ? undefined : ' is Invalid')
    }

    return undefined
}
const email = value =>
    value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
        ? ' is Invalid'
        : undefined
const websiteValiadtion = value => {
    if (value) {
        var pattern = new RegExp('^(https?:\\/\\/)' + // protocol
            '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
            '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
            '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
            '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
            '(\\#[-a-z\\d_]*)?$', 'i'); // fragment locator
        return !!pattern.test(value) ? undefined : 'Enter a valid url ( eg:https://websitebuilders.com )'
    }
}
const salutationType = [
    { value: 'mr', text: 'Mr' },
    { value: 'ms', text: 'Ms' },
    { value: 'mrs', text: 'Mrs' }
]
const clientType = [
    { text: 'Business', value: 'business', name: "client_type", id: "id_client_type_0" },
    { text: 'Individual', value: 'individual', name: "client_type", id: "id_client_type_1" }
]
const renderField = ({
    input,
    label,
    type,
    existed,
    work_phone_number_error,
    mobile_number_error,
    website_error,
    facebook_error,
    pin_code_error,
    meta: { touched, error, warning, }
}) => {

    return (


        <Fragment>
            <input {...input} placeholder={label} type={type} className={(touched && error) ||
                (existed && (label === 'Contact Email')) || (work_phone_number_error) || (mobile_number_error) || (website_error) || (facebook_error) || (pin_code_error)
                ? 'fullwidth input-control border border-danger' : 'fullwidth input-control'} />
            {touched &&
                ((error && <small className="text-danger">{label}{error}</small>) ||
                    (warning && <span>{warning}</span>))}

            {(label === 'Contact Email') && (existed) && <small class="text-danger">
                {existed}
            </small>}
            {(work_phone_number_error) && <small class="text-danger">
                {work_phone_number_error}
            </small>}
            {(mobile_number_error) && <small class="text-danger">
                {mobile_number_error}
            </small>}
            {(website_error) && <small class="text-danger">
                {website_error}
            </small>}
            {(facebook_error) && <small class="text-danger">
                {facebook_error}
            </small>}
            {(pin_code_error) && <small class="text-danger">
                {pin_code_error}
            </small>}
        </Fragment>

    )
}

const renderFieldSelect = ({
    input,
    label,
    type,
    id,
    existed,
    options,
    country,
    countryState,
    city,
    meta: { touched, error, warning, }
}) => {

    return (
        <Fragment>
            {(options && label === "Salutation") &&
                <select {...input} className={(touched && error) ? 'fullwidth input-control border border-danger' : 'fullwidth input-control'}>
                    <option id='salutation_id' value="">Select Salutation</option>
                    {options.map((salutation) =>
                        <option value={salutation.value} >{salutation.text}</option>
                    )}
                </select>
            }
            {(options && label === "Branch") &&
                <Fragment>
                    <div className="col-sm-6 form-group">
                        <label className="text-light-grey" htmlFor="id_branch">
                            Branch
            </label>
                        <select {...input} className="fullwidth input-control" >
                            <option id='no_designation' value="">Select Branch</option>
                            {options.map((branch, index) =>
                                <option key={index} value={branch.slug} >{branch.branch_name}  </option>
                            )}
                        </select>
                    </div>
                </Fragment>
            }
            {(options && label === "Department") &&
                <select {...input} className="fullwidth input-control">
                    <option id='no_designation' value="">Select Department</option>
                    {options.map((department, index) =>
                        <option key={index} value={department.slug} >{department.department_name}  </option>
                    )}
                </select>
            }

            {(label === "Country" || label === "State" || label === "City") &&
                <select {...input} className={(touched && error) ? 'input-control border border-danger' : 'input-control'}>
                    <option value="">{label}</option>
                    {(country && label === "Country") &&
                        <Fragment>
                            {country.countryList.map((plan, index) => (
                                <option key={index} value={plan.id}>{plan.name_ascii}</option>
                            ))}</Fragment>
                    }
                    {(countryState && label === "State") &&
                        <Fragment>
                            {countryState.stateList.map((plan, index) => (
                                <option key={index} value={plan.id}>{plan.name_ascii}</option>
                            ))}</Fragment>
                    }
                    {(city && label === "City") &&
                        <Fragment>
                            {city.cityList.map((plan, index) => (
                                <option key={index} value={plan.id}>{plan.name_ascii}</option>
                            ))}</Fragment>
                    }
                </select>
            }

            {(label === "Currency") &&
                <Fragment>
                    <select {...input} name="currency" className="input-control" >
                        {options.map((option) => (
                            <option key={option.value} id={option} value={option.value}>{option.label}</option>
                        ))}
                    </select>
                </Fragment>
            }

            {
                (label === "Portal Language") &&
                <Fragment>
                    <select {...input} className={(touched && error) ? 'input-control border border-danger' : 'input-control'} >
                        <option value="">Select Portal Language</option>
                        <option value="English">English</option>
                        {/* {Helper.languages.map(option=>{
                                    return <option key={option.value} id={option} value={option.value}>{option.label}</option>
                                  })} */}
                    </select>
                </Fragment>
            }

            {
                (label === "Payment Terms") &&
                <Fragment>
                    <select {...input} className="input-control ">
                        <option value="">Select Payment Terms</option>
                        {options.map(option =>
                            <option key={option.value} value={option.value}>{option.text}</option>
                        )}
                    </select>
                </Fragment>
            }
            {
                (label === "Enable Portal") &&
                <Fragment>

                </Fragment>
            }

            <div>
                {touched &&
                    ((error && <small className="text-danger">{label}{error}</small>) ||
                        (warning && <span>{warning}</span>))}
            </div>

        </Fragment>
    )
}

const radioCheckbox = ({ input, label, options, type, checked, meta: { touched, error, warning } }) => (
    <Fragment>

        {options.map((plan, index) => (
            <div className="col-md-3" key={index}>
                <input type="radio" {...input} checked={checked} key={index} name={plan.name} value={plan.value} id={plan.id} /> {plan.text}
            </div>
        ))
        }

        {touched &&
            ((error && <small className="text-danger">{label}{error}</small>) ||
                (warning && <span>{warning}</span>))}


    </Fragment>
)

const renderContactPerson = ({ fields, meta: { error, submitFailed } }) => (
    <div>
        <li>
            <a className="add-row" href="javascript:void(0)">
                <div className="row">
                    <div className="col-sm-12 text-center">
                        <button type="button" className="btn btn-outline-primary add-row" onClick={() => fields.push({})}>
                            <i className="icon-plus"></i> Add Contact Persons
                                </button>
                    </div>
                </div>
            </a>

            {/* {submitFailed && error && <span>{error}</span>} */}
        </li>
        {fields.map((member, index) => (
            <li key={index}>
                {/* <button
            type="button"
            title="Remove Member"
            onClick={() => fields.remove(index)}
          /> */}


                <div className="row mb-3 formset_row dynamic-form">
                    <div className="col-sm-6 form-group">
                        <label className="text-light-grey" for="id_contactperson_set-0-salutation">Salutation<span class="text-danger">*</span></label>

                        <Field
                            component={renderFieldSelect}
                            name={`${member}.contactperson_salutation`}
                            id="salutation_id"
                            label="Salutation"
                            validate={required}
                            options={salutationType}
                        />

                    </div>
                    <div className="col-sm-6 form-group">
                        <label className="text-light-grey" htmlFor="id_contactperson_set-0-first_name">First name<span className="text-danger">*</span></label>
                        <Field type="text"
                            name={`${member}.contactperson_first_name`}
                            component={renderField}
                            label="First Name"


                            validate={[required, alphabetOnly]}
                        />
                    </div>
                    <div className="col-sm-6 form-group">
                        <label className="text-light-grey" htmlFor="id_contactperson_set-0-last_name">Last name </label>
                        <Field type="text"
                            name={`${member}.contactperson_last_name`}
                            component={renderField}
                            label="Last Name"


                            validate={[required, alphabetOnly]}
                        />
                    </div>
                    <div className="col-sm-6 form-group">
                        <label className="text-light-grey" htmlFor="id_contactperson_set-0-email">Email </label>
                        <Field
                            name={`${member}.contactperson_email`}
                            component={renderField}
                            label="Contact Email"


                            validate={[required, email]}
                        />
                    </div>
                    <div class="col-sm-6 form-group">
                        <label class="text-light-grey" for="id_contactperson_set-0-phone_number">Phone number </label>
                        <Field type="text"
                            name={`${member}.contactperson_mobile_number`}
                            component={renderField}
                            label="Mobile Number"


                            validate={mobileValid}
                        />
                    </div>
                    {/* <div class="col-sm-6 form-group">
                                <label class="text-light-grey" for="id_contactperson_set-0-designation">Designation </label>
                                <input type="text" name="contactperson_set-0-designation" placeholder="Designation" maxlength="128" className="input-control" id="id_contactperson_set-0-designation"/> 
                              </div> */}
                    <div className="col-sm-6 form-group">
                        <label className="text-light-grey">Skype Name/Number
                                </label>
                        <Field type="text"
                            name={`${member}.contactperson_skype_name`}
                            component={renderField}
                            label="Skype Name/Number"



                        />
                    </div>
                    <div className="col-sm-12 text-right">
                        <a className="delete-row btn btn-outline-primary"
                            href="javascript:void(0)"
                            onClick={() => fields.remove(index)}
                        ><i class=" icon-delete"></i> remove</a>



                    </div>
                    <div className="col-sm-12"><hr></hr></div>


                </div>


            </li>
        ))}
    </div>
)

class UpdateClientModal extends Component {
    constructor(props) {
        super(props)
        this.state = {
            pincodeError: '',
            work_phone_number_error: '',
            mobile_number_error: '',
            website_error: '',
            facebook_error: '',
            existed_error: ''
        }
    }

    componentDidMount() {
        document.body.classList.add(MODAL_OPEN_CLASS);

        let data = {}
        if (this.props.editpart === 'basic') {
            if (this.props.clientData && this.props.clientData.client_obj && this.props.clientData.client_obj.client) {
                data.first_name = this.props.clientData.client_obj.client.first_name ? this.props.clientData.client_obj.client.first_name : ''
                data.last_name = this.props.clientData.client_obj.client.last_name ? this.props.clientData.client_obj.client.last_name : ''
                data.email = this.props.clientData.client_obj.client.email ? this.props.clientData.client_obj.client.email : ''
                data.client_type = this.props.clientData.client_obj.client.client_type ? this.props.clientData.client_obj.client.client_type : ''
                data.company_name = this.props.clientData.client_obj.client.company_name ? this.props.clientData.client_obj.client.company_name : ''
                data.mobile = this.props.clientData.client_obj.client.mobile ? this.props.clientData.client_obj.client.mobile : ''
                data.website = this.props.clientData.client_obj.client.website ? this.props.clientData.client_obj.client.website : ''
                data.salutation = this.props.clientData.client_obj.client.salutation ? this.props.clientData.client_obj.client.salutation : ''
                data.work_phone = this.props.clientData.client_obj.client.work_phone ? this.props.clientData.client_obj.client.work_phone : ''
                data.client_display_name = this.props.clientData.client_obj.client.client_display_name ? this.props.clientData.client_obj.client.client_display_name : ''
            }
        }
        if (this.props.editpart === 'otherdetails') {
            if (this.props.clientData && this.props.clientData.client_obj && this.props.clientData.client_obj.otherdetails) {
                data.currency = this.props.clientData.client_obj.otherdetails.currency ? this.props.clientData.client_obj.otherdetails.currency : ''
                data.enable_portal = this.props.clientData.client_obj.otherdetails.enable_portal ? this.props.clientData.client_obj.otherdetails.enable_portal : false
                data.facebook = this.props.clientData.client_obj.otherdetails.facebook ? this.props.clientData.client_obj.otherdetails.facebook : ''
                data.payment_terms = this.props.clientData.client_obj.otherdetails.payment_terms ? this.props.clientData.client_obj.otherdetails.payment_terms : ''
                data.portal_language = this.props.clientData.client_obj.otherdetails.portal_language ? this.props.clientData.client_obj.otherdetails.portal_language : ''
                data.skype_name = this.props.clientData.client_obj.otherdetails.skype_name ? this.props.clientData.client_obj.otherdetails.skype_name : ''
            }
        }

        if (this.props.editpart === 'address') {

            if (this.props.clientData && this.props.clientData.client_obj && this.props.clientData.client_obj.billing_address) {

                this.props.fetchCountryList()
                this.props.fetchStatesList(this.props.clientData.client_obj.billing_address.country.id);
                this.props.fetchCityList(this.props.clientData.client_obj.billing_address.state.id);

                data.address1 = this.props.clientData.client_obj.billing_address.address1 ? this.props.clientData.client_obj.billing_address.address1 : ''
                data.address2 = this.props.clientData.client_obj.billing_address.address2 ? this.props.clientData.client_obj.billing_address.address2 : ''
                data.attention = this.props.clientData.client_obj.billing_address.attention ? this.props.clientData.client_obj.billing_address.attention : ''
                data.country = this.props.clientData.client_obj.billing_address.country.id ? this.props.clientData.client_obj.billing_address.country.id : ''
                data.state = this.props.clientData.client_obj.billing_address.state.id ? this.props.clientData.client_obj.billing_address.state.id : ''
                data.city = this.props.clientData.client_obj.billing_address.city.id ? this.props.clientData.client_obj.billing_address.city.id : ''
                data.fax = this.props.clientData.client_obj.billing_address.fax ? this.props.clientData.client_obj.billing_address.fax : ''
                data.phone = this.props.clientData.client_obj.billing_address.phone ? this.props.clientData.client_obj.billing_address.phone : ''
                data.pin_code = this.props.clientData.client_obj.billing_address.pin_code ? this.props.clientData.client_obj.billing_address.pin_code : ''
            }

        }
        if (this.props.editpart === 'remarks') {
            data.remarks = this.props.clientData && this.props.clientData.client_obj && this.props.clientData.client_obj.remark && this.props.clientData.client_obj.remark.remarks
        }
        if (this.props.editpart === 'contactperson_single') {

            if (this.props.clientData && this.props.clientData.client_obj && this.props.clientData.client_obj.contact_persons&&this.props.clientData.client_obj.contact_persons.length>0) {
                // debugger
                console.log('index',this.props.index);
                
                data.contactperson_salutation=this.props.clientData.client_obj.contact_persons[this.props.index].contactperson_salutation
                data.contactperson_first_name=this.props.clientData.client_obj.contact_persons[this.props.index].contactperson_first_name
                data.contactperson_last_name=this.props.clientData.client_obj.contact_persons[this.props.index].contactperson_last_name
                data.contactperson_email=this.props.clientData.client_obj.contact_persons[this.props.index].contactperson_email
                data.contactperson_mobile_number=this.props.clientData.client_obj.contact_persons[this.props.index].contactperson_mobile_number
                data.contactperson_skype_name=this.props.clientData.client_obj.contact_persons[this.props.index].contactperson_skype_name

            }
        }

        console.log('contacttt', data);

        this.props.initialize(data);

    }

    handleCountryChange = (e) => {
        // making fields null
        this.props.change('state', '')
        this.props.change('city', '')

        this.props.fetchStatesList(e.target.value);

        // city error resolves after u uncomment it 
        this.props.fetchCityList(" ");
    }

    handleStateChange = (e) => {
        // making fields null
        this.props.change('city', '')

        this.props.fetchCityList(e.target.value);
    }

    submit = (values) => {
        console.log('values,slug', values, this.props.clientSlug)
        

        this.props.editpart === 'basic' && this.props.editClientBasicDetails(values, this.props.clientSlug)
        this.props.editpart === 'otherdetails' && this.props.editOtherDetails(values, this.props.clientSlug)
        this.props.editpart === 'address' && this.props.editAddressDetails(values, this.props.clientSlug)
        this.props.editpart === 'remarks' && this.props.editRemarkDetails(values, this.props.clientSlug)
        this.props.editpart === 'contactperson' && this.props.editContactPersonDetails(values, this.props.clientSlug)

        if(this.props.editpart === 'contactperson_single'){
            if(this.props.clientData&&this.props.clientData.client_obj&&this.props.clientData.client_obj.contact_persons){
            let fullcontactperson=this.props.clientData.client_obj.contact_persons
            
            if('contact_person' in values){
            // debugger
            
            let {contact_person,...c}=values
            let a=values['contact_person']
            a.push(c)
            
            // delete values.contact_person
            
            
            // formsetvalues.push(values)
            fullcontactperson.splice(this.props.index,1)
            console.log('after splice',fullcontactperson);
            
            let final=a.concat(fullcontactperson)  
            console.log('a',final)
            let data={}
            data.contact_person=final
            this.props.editContactPersonDetails(data, this.props.clientSlug)
            
            
        }

            else{
                fullcontactperson.splice(this.props.index,1,values)          
                
                // debugger
                let contactdata={}
                contactdata.contact_person=fullcontactperson
                console.log('contactdata',contactdata)
                this.props.editContactPersonDetails(contactdata, this.props.clientSlug)
            }
        }
        }
    }

    componentWillUnmount() {
        document.body.classList.remove(MODAL_OPEN_CLASS);


    }

    clearExistedError = (params) => {

        params === 'email' && this.setState({ existed_error: '' })
        params === 'work_phone' && this.setState({ work_phone_number_error: '' })
        params === 'mobile_number' && this.setState({ mobile_number_error: '' })
        params === 'website' && this.setState({ website_error: '' })
        params === 'facebook' && this.setState({ facebook_error: '' })
        params === 'pin_code' && this.setState({ pincodeError: '' })
    }

    componentDidUpdate(prevProps) {
        if (this.props.clientData != prevProps.clientData && this.props.clientData && this.props.clientData.client_edit === 'success') {
            this.props.closeModal()
        }
        if (this.props.clientData != prevProps.clientData && this.props.clientData && this.props.clientData.client_edit === 'error') {

            this.props.clientData && this.props.clientData.client_editerror_obj && this.props.clientData.client_editerror_obj.data && this.props.clientData.client_editerror_obj.data.email &&
                this.props.clientData.client_editerror_obj.data.email[0] && this.setState({ existed_error: 'Contact Email already exists' })

            this.props.clientData && this.props.clientData.client_editerror_obj && this.props.clientData.client_editerror_obj.data && this.props.clientData.client_editerror_obj.data.work_phone &&
                this.props.clientData.client_editerror_obj.data.work_phone[0] && this.setState({ work_phone_number_error: 'Enter Work Phone Number with Country Code' })

            this.props.clientData && this.props.clientData.client_editerror_obj && this.props.clientData.client_editerror_obj.data && this.props.clientData.client_editerror_obj.data.mobile &&
                this.props.clientData.client_editerror_obj.data.mobile[0] && this.setState({ mobile_number_error: 'Enter Mobile Number with Country Code' })

            this.props.clientData && this.props.clientData.client_editerror_obj && this.props.clientData.client_editerror_obj.data && this.props.clientData.client_editerror_obj.data.website &&
                this.props.clientData.client_editerror_obj.data.website[0] && this.setState({ website_error: 'Invalid Website' })

            // this.props.clientData && this.props.clientData.client_editerror_obj && this.props.clientData.client_editerror_obj.data && this.props.clientData.client_editerror_obj.data.website &&
            //     this.props.clientData.client_editerror_obj.data.website[0] && this.setState({ website_error: 'Invalid Website' })

            this.props.clientData && this.props.clientData.client_editerror_obj && this.props.clientData.client_editerror_obj.data &&
                this.props.clientData.client_editerror_obj.data.non_field_errors && this.props.clientData.client_editerror_obj.data.non_field_errors[0] && this.setState({ pincodeError: 'Invalid Zip Code' })

        }
    }

    render() {
        console.log('props', this.props)
        // console.log('state', this.state)
        const { handleSubmit } = this.props;
        return (
            <Fragment>
                <div id="modal-block">
                    <div className="modal form-modal show" id="editBasicDetails" tabIndex="-1" role="dialog" aria-labelledby="TermsLabel" style={{ paddingRight: "15px", display: "block" }}>
                        <div className="modal-dialog modal-dialog-centered" role="document">
                            <div className="modal-content">

                                <div className="modal-header">
                                    <h5 className="modal-title" id="TermsLabel">Edit {this.props.editpart} Details</h5>
                                    <button onClick={this.props.closeModal} type="button" className="btn btn-black close-button" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>

                                <form onSubmit={handleSubmit(this.submit)}>
                                    <div className="modal-body">
                                        {this.props.editpart === 'basic' &&
                                            <Fragment>
                                                <div className="row popup-form">
                                                    <div className="col-sm-6 form-group ">
                                                        <label htmlFor="id_first_name">Customer Type
                                                        <span className="text-danger">*</span></label>
                                                        <div>
                                                            <label className="individual">
                                                                <Field type="radio"
                                                                    name="client_type"
                                                                    className="individual-button"
                                                                    validate={required}
                                                                    component="input"
                                                                    value="individual"
                                                                    label="Customer Type"
                                                                />Individual</label>
                                                            <label className="individual">                                                            <Field type="radio"
                                                                name="client_type"
                                                                className="individual-button"
                                                                validate={required}
                                                                component="input"
                                                                value="business"
                                                                label="Customer Type"
                                                            />Business</label>
                                                        </div>
                                                    </div>

                                                    <div className="col-sm-6 form-group ">
                                                        <label htmlFor="id_first_name">Salutation</label>
                                                        <span className="text-danger">*</span>
                                                        <Field
                                                            type="text"
                                                            name="salutation"
                                                            component={renderFieldSelect}
                                                            options={salutationType}
                                                            label="Salutation"
                                                            validate={[required]} />
                                                        <div className="text-danger error-message"></div>
                                                    </div>

                                                    <div className="col-sm-6 form-group ">
                                                        <label htmlFor="id_first_name">First name</label>
                                                        <span className="text-danger">*</span>
                                                        <Field
                                                            type="text"
                                                            name="first_name"
                                                            component={renderField}
                                                            label="First Name"
                                                            validate={[required, alphabetOnly]} />
                                                        <div className="text-danger error-message"></div>
                                                    </div>
                                                    <div className="col-sm-6 form-group ">
                                                        <label htmlFor="id_first_name">Last name</label>
                                                        <span className="text-danger">*</span>
                                                        <Field
                                                            type="text"
                                                            name="last_name"
                                                            component={renderField}
                                                            label="Last Name"
                                                            validate={[required, alphabetOnly]} />
                                                        <div className="text-danger error-message"></div>
                                                    </div>
                                                    <div className="col-sm-6 form-group ">
                                                        <label htmlFor="id_first_name">Contact Email</label>
                                                        <span className="text-danger">*</span>
                                                        <Field
                                                            type="text"
                                                            name="email"
                                                            component={renderField}
                                                            label="Contact Email"
                                                            onChange={() => this.clearExistedError('email')}
                                                            existed={this.state.existed_error}
                                                            validate={[required, email]} />
                                                        <div className="text-danger error-message"></div>
                                                    </div>
                                                    <div className="col-sm-6 form-group ">
                                                        <label htmlFor="id_first_name">Client Display Name</label>

                                                        <Field
                                                            type="text"
                                                            name="client_display_name"
                                                            component={renderField}
                                                            label="Client Display Name"
                                                        />
                                                        <div className="text-danger error-message"></div>
                                                    </div>




                                                    <div className="col-sm-6 form-group">
                                                        <label className="text-light-grey" htmlFor="id_first_name">
                                                            Company name
                    </label>
                                                        <Field type="text"
                                                            name="company_name"
                                                            component={renderField}
                                                            label="Company Name"
                                                            onChange={this.clearExistedError}
                                                        />
                                                    </div>
                                                    <div className="col-sm-6 form-group">
                                                        <label className="text-light-grey" htmlFor="id_work_phone_number">
                                                            Work Phone Number
                    </label>
                                                        <Field type="text"
                                                            name="work_phone"
                                                            component={renderField}
                                                            label="Work Phone Number"
                                                            onChange={() => this.clearExistedError('work_phone')}
                                                            work_phone_number_error={this.state.work_phone_number_error}
                                                            validate={mobileValid}
                                                        />
                                                    </div>
                                                    <div className="col-sm-6 form-group">
                                                        <label className="text-light-grey" htmlFor="id_skype">
                                                            Mobile Number
                    </label>
                                                        <Field type="text"
                                                            name="mobile"
                                                            component={renderField}
                                                            label="Mobile Number"
                                                            onChange={() => this.clearExistedError('mobile_number')}
                                                            mobile_number_error={this.state.mobile_number_error}
                                                            validate={mobileValid}
                                                        />

                                                    </div>
                                                    <div className="col-sm-6 form-group">
                                                        <label className="text-light-grey" htmlFor="id_website">
                                                            Website
                    </label>
                                                        <Field type="text"
                                                            name="website"
                                                            component={renderField}
                                                            onChange={() => this.clearExistedError('website')}
                                                            website_error={this.state.website_error}
                                                            validate={websiteValiadtion}
                                                        />
                                                    </div>


                                                </div>
                                            </Fragment>
                                        }
                                        {this.props.editpart === 'otherdetails' &&
                                            <Fragment>
                                                <div className="row">
                                                    <div className="col-md-12 mb-2">
                                                        <label className="text-light-grey" htmlFor="id_currency">Currency</label>
                                                        <Field type="text"
                                                            name="currency"
                                                            component={renderFieldSelect}
                                                            options={Currency}
                                                            label="Currency"
                                                        />
                                                    </div>
                                                    <div className="col-md-7  mb-2  ">
                                                        <label className="text-light-grey" htmlFor="id_payment_terms">Payment terms</label>

                                                        <Field type="text"
                                                            name="payment_terms"
                                                            component={renderFieldSelect}
                                                            options={PaymentTerms}
                                                            label="Payment Terms"
                                                        />
                                                    </div>


                                                    <div className="col-sm-4 form-group ">
                                                        <label htmlFor="id_first_name">Portal Access</label>
                                                        <span className="text-danger"></span>
                                                        <span>
                                                            <label>
                                                                <Field type="checkbox"
                                                                    name="enable_portal"

                                                                    component="input"
                                                                /></label>
                                                        </span>
                                                    </div>

                                                    <div className="col-md-12 mb-2 mt-1">
                                                        <label className="text-light-grey" htmlFor="id_portal_language">Portal language
                              <button className="btn btn-link-grey p-0" role="button" data-toggle="popover" data-placement="bottom" data-trigger="focus" title="Portal Language" data-content="And here's some amazing content. It's very engaging. Right?">
                                                                <i className="icon-warning-circle" style={{ verticalAlign: "middle" }}></i>
                                                            </button>
                                                        </label>
                                                        <Field
                                                            name="portal_language"
                                                            component={renderFieldSelect}
                                                            validate={required}
                                                            label="Portal Language"
                                                        />

                                                    </div>
                                                    <div className="col-sm-6 form-group">
                                                        <label className="text-light-grey" htmlFor="id_facebook">Facebook </label>


                                                        <Field type="text"
                                                            name="facebook"
                                                            component={renderField}
                                                            onChange={() => this.clearExistedError('facebook')}
                                                            facebook_error={this.state.facebook_error}
                                                            validate={websiteValiadtion}
                                                        />

                                                    </div>
                                                    {/* <div className="col-sm-6 form-group">
                              <label className="text-light-grey" htmlFor="id_twitter">Twitter</label>
                              <div className="input-group">
                                 <div className="input-group-prepend soc-icon-lt">
                                    <span className="input-group-text" id="addon-wrapping"><i className="icon-twitter text-primary"></i></span>
                                 </div>
                                 <input type="text" name="twitter" placeholder="http://www.twitter.com/" maxLength="200" className="input-control inp-padding" id="id_twitter"/>
                              </div>
                            </div> */}
                                                    <div className="col-sm-6 form-group">
                                                        <label className="text-light-grey" htmlFor="id_skype">
                                                            Skype Name/Number
                            </label>


                                                        <Field type="text"
                                                            name="skype_name"
                                                            component={renderField}
                                                            label="Skype Name/Number"
                                                            onChange={this.clearExistedError}
                                                            existed={this.state.existed_error}

                                                        />

                                                    </div>
                                                </div>
                                            </Fragment>
                                        }
                                        {this.props.editpart === 'address' &&
                                            <Fragment>
                                                <div className="row mb-3">
                                                    <div className="col-sm-6 form-group">
                                                        <label className="text-light-grey" for="id_attention">Attention </label>
                                                        <Field
                                                            name="attention"
                                                            component={renderField}
                                                            label="Attention"
                                                        />
                                                    </div>
                                                    <div className="col-sm-6 form-group">
                                                        <label className="text-light-grey" for="id_country">Country <span class="text-danger">*</span></label>
                                                        <Field
                                                            name="country"
                                                            type="select"
                                                            country={countryReorder(this.props.countryDetails)}
                                                            component={renderFieldSelect}
                                                            className="input-control select-country"
                                                            label="Country"
                                                            validate={required}
                                                            onChange={this.handleCountryChange} />
                                                    </div>
                                                    <div className="col-md-4 form-group">
                                                        <label className="text-light-grey" for="id_state">State <span class="text-danger">*</span></label>
                                                        <Field
                                                            name="state"
                                                            type="select"
                                                            countryState={this.props.stateDetails}
                                                            component={renderFieldSelect}
                                                            className="input-control select-state"
                                                            label="State"
                                                            validate={required}
                                                            onChange={this.handleStateChange}
                                                        />
                                                    </div>
                                                    <div className="col-md-4 form-group">
                                                        <label className="text-light-grey" for="id_city">City <span class="text-danger">*</span></label>
                                                        <Field
                                                            name="city"
                                                            type="select"
                                                            city={this.props.cityDetails}
                                                            component={renderFieldSelect}
                                                            className="input-control select-city"
                                                            label="City"
                                                            validate={required} />
                                                    </div>
                                                    <div class="col-md-4 form-group">
                                                        <label className="text-light-grey" for="id_pin_code">Zip Code </label>
                                                        <Field
                                                            type="text"
                                                            name="pin_code"
                                                            component={renderField}
                                                            label="Zip Code"
                                                            onChange={() => this.clearExistedError('pin_code')}
                                                            pin_code_error={this.state.pincodeError} />
                                                    </div>
                                                    <div className="col-sm-6 form-group">
                                                        <label className="text-light-grey" for="id_address1">Address1 </label>
                                                        <Field type="text"
                                                            name="address1"
                                                            cols="40" rows="10"
                                                            component="textarea"
                                                            className="input-control input-control"
                                                            placeholder="Address1" />
                                                    </div>
                                                    <div className="col-sm-6 form-group">
                                                        <label className="text-light-grey" for="id_address2">Address2 </label>
                                                        <Field type="text"
                                                            name="address2"
                                                            cols="40" rows="10"
                                                            component="textarea"
                                                            className="input-control input-control"
                                                            placeholder="Address2" />
                                                    </div>
                                                    <div className="col-sm-6 form-group">
                                                        <label className="text-light-grey" for="id_fax">Fax </label>
                                                        <Field
                                                            name="fax"
                                                            component={renderField}
                                                            label="Fax"
                                                        />
                                                    </div>
                                                </div>
                                            </Fragment>
                                        }
                                        {this.props.editpart === 'remarks' &&
                                            <Fragment>
                                                <Field type="text"
                                                    name="remarks"
                                                    cols="40" rows="10"
                                                    component="textarea"
                                                    className="input-control input-control"
                                                    placeholder="Remarks" />
                                            </Fragment>
                                        }
                                        {this.props.editpart === 'contactperson' &&
                                            <Fragment>
                                                <FieldArray name="contact_person" component={renderContactPerson} />
                                            </Fragment>
                                        }
                                        {this.props.editpart === 'contactperson_single' &&
                                            <Fragment>

                                                <div className="row mb-3 formset_row dynamic-form">
                                                    <div className="col-sm-6 form-group">
                                                        <label className="text-light-grey" for="id_contactperson_set-0-salutation">Salutation<span class="text-danger">*</span></label>

                                                        <Field
                                                            component={renderFieldSelect}
                                                            name="contactperson_salutation"
                                                            id="salutation_id"
                                                            label="Salutation"
                                                            validate={required}
                                                            options={salutationType}
                                                        />

                                                    </div>
                                                    <div className="col-sm-6 form-group">
                                                        <label className="text-light-grey" htmlFor="id_contactperson_set-0-first_name">First name<span className="text-danger">*</span></label>
                                                        <Field type="text"
                                                            name="contactperson_first_name"
                                                            component={renderField}
                                                            label="First Name"


                                                            validate={[required, alphabetOnly]}
                                                        />
                                                    </div>
                                                    <div className="col-sm-6 form-group">
                                                        <label className="text-light-grey" htmlFor="id_contactperson_set-0-last_name">Last name </label>
                                                        <Field type="text"
                                                            name="contactperson_last_name"
                                                            component={renderField}
                                                            label="Last Name"


                                                            validate={[required, alphabetOnly]}
                                                        />
                                                    </div>
                                                    <div className="col-sm-6 form-group">
                                                        <label className="text-light-grey" htmlFor="id_contactperson_set-0-email">Email </label>
                                                        <Field
                                                            name="contactperson_email"
                                                            component={renderField}
                                                            label="Contact Email"


                                                            validate={[required, email]}
                                                        />
                                                    </div>
                                                    <div class="col-sm-6 form-group">
                                                        <label class="text-light-grey" for="id_contactperson_set-0-phone_number">Phone number </label>
                                                        <Field type="text"
                                                            name="contactperson_mobile_number"
                                                            component={renderField}
                                                            label="Mobile Number"


                                                            validate={mobileValid}
                                                        />
                                                    </div>
                                                    {/* <div class="col-sm-6 form-group">
                                <label class="text-light-grey" for="id_contactperson_set-0-designation">Designation </label>
                                <input type="text" name="contactperson_set-0-designation" placeholder="Designation" maxlength="128" className="input-control" id="id_contactperson_set-0-designation"/> 
                              </div> */}
                                                    <div className="col-sm-6 form-group">
                                                        <label className="text-light-grey">Skype Name/Number
                                </label>
                                                        <Field type="text"
                                                            name="contactperson_skype_name"
                                                            component={renderField}
                                                            label="Skype Name/Number"



                                                        />
                                                    </div>
                                                    

                                                </div>


                                                <FieldArray name="contact_person" component={renderContactPerson} />
                                            </Fragment>
                                        }
                                    </div>
                                    <div className="modal-footer">
                                        <input type="submit" className="btn btn-success" value="Save" />
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
                <div className="modal-backdrop show"></div>
            </Fragment>
        )
    }

}
UpdateClientModal = reduxForm({
    form: 'updateClientForm'
})(UpdateClientModal)
export default UpdateClientModal