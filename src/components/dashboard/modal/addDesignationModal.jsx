import React, { Component, Fragment } from 'react'
import {alphabetOnly} from '../../../constants/validations'


const MODAL_OPEN_CLASS = "modal-open";

var selectedRoleList=[]
var letters = /^[-a-zA-Z0-9-()]+(\s+[-a-zA-Z0-9-()]+)*$/;
class AddDesignationNew extends Component {
    constructor(props) {
        super(props);
        this.state = {
            designationVal: "",
            designationError:"",
            // valid:true
            
        };
    }
    validate =()=>{
        if(this.state.designationVal === ""){
            this.setState({designationError:'Designation Name is Required'})
        }
        // if(!((event.target.value).match(letters))){
        //     this.setState({designationVal:event.target.value,designationError:'Designation Name must be characters only'})
        // }
    }

    submit1 = (event) => {
        event.preventDefault()
        this.validate()

        // let default_roles = []
        //     default_roles = this.props.roles.roles.filter(role => role.default === true)
        //     if(!("user_permissions" in values)){
        //         values.user_permissions =[]
        //     }
        //     if(values.user_permissions!== undefined){
        //         default_roles.map(i => values.user_permissions.push(i.slug))
        //     }
        
        let values={}
        if(this.state.designationVal){

        }
        values.designation_name=this.state.designationVal
        let default_roles =[]
        this.props.roles.roles.map((role) => {
            if(role.default === true){
                default_roles.push(role.slug)
            }
            return true
        })
        values.user_permissions = selectedRoleList.concat(default_roles)
        
        
        // if(this.state.designationVal !== ""){
        //     this.setState({designationError:''},()=>{
        //         this.props.addNewDesignationFromModal(values)

        //     })
        // }

        if(this.state.designationError===""&&this.state.designationVal!==''){
            this.props.addNewDesignationFromModal(values)
        }

    }

    componentDidMount(){
      
        document.body.classList.add(MODAL_OPEN_CLASS);
    }

    componentWillUnmount() {
        document.body.classList.remove(MODAL_OPEN_CLASS);
        this.props.clearModalStatus()
      }
    
    
    HandleCheckbox = event =>{
       event.target.checked?
       selectedRoleList.push(event.target.getAttribute('data-tag')):
       selectedRoleList.pop(event.target.getAttribute('data-tag'))

    }  

    handleInputChange = event =>{
        if(event.target.value===''){
            this.setState({designationVal:event.target.value,designationError:'Designation Name is Required'})
            }
        else if(!((event.target.value).match(letters))){
            this.setState({designationVal:event.target.value,designationError:'Designation Name must be characters only'})
        }
        else{
                this.setState({designationVal:event.target.value,designationError:''})
            }
        }    
    
    componentDidUpdate(prevProps){
                
                if(prevProps.desigantions!==this.props.desigantions){

                (this.props.desigantions&&
                this.props.desigantions.design_obj_Error&&
                (this.props.desigantions.design_obj_Error.data.non_field_errors[0] === "The fields organization, designation_name must make a unique set."))&&
                this.setState({designationError:'This designation name already exists'})
            
            }
        
    }

    render() {
        console.log('state',this.state)
        if(this.props.desigantions&&this.props.desigantions.modalstatus==='success'){
            this.props.onCloseModal()
            // window.location.reload()
        }
        
        return (
            <Fragment>
                <div id="modal-block">
                    <div className="modal form-modal show"  role="dialog" style={{ display: 'block', paddingLeft: '15px' }}>
                        <div className="modal-dialog modal-lg">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <h4 className="modal-title">Add Designation</h4>
                                    <button type="button" onClick={this.props.onCloseModal} className="btn btn-close btn-black" data-dismiss="modal"><span aria-hidden="true">×</span></button>
                                </div>

                                <div className="modal-body">
                                    <form id="add-designation-form" onSubmit={this.submit1} noValidate>

                                        <div className="form-group row">
                                            <div className="col-md-3">
                                                <label className="form-control-label" htmlFor="id_designation_name">
                                                    <label htmlFor="id_designation_name">Designation name</label></label>
                                                <span className="text-danger">*</span>

                                            </div>
                                            <div className="col-md-9">
                                            <input name="designation_name" 
                                            placeholder="Designation Name" 
                                            type="text" 
                                            className={"fullwidth input-control" + (this.state.designationError && " border border-danger ") }
                                            onChange={this.handleInputChange}/>
                                            <small className="text-danger">{this.state.designationError}</small>
                                            
                                            
                                            
                                            </div>
                                            
                                        </div>

                                        <div className="form-group row ml-15">

                                            <div className="col-md-3">
                                                <label className="form-control-label" htmlFor="">
                                                    <label>Roles</label></label>

                                            </div>

                                            <ul id="id_selection">
                                                {this.props.roles && this.props.roles.roles&&this.props.roles.roles.filter((role)=>role.default===false).map((role,index)=>
                                                <ul key={index}>
                                                    <li>
                                                <input type="checkbox" data-tag={role.slug} onClick={this.HandleCheckbox} name={role.group_name} />
                                                {role.group_name}
                                                </li>
                                                </ul>
                                                )}
                                                 
                                            </ul>
                                        </div>
                                        

                                        {/* <div className="m-5">
                                            <input type="submit" className="btn width btn-primary float-right" value="SAVE" />
                                        </div> */}
                                    </form>

                                    <div className="modal-footer">
                                            <input type="submit" form="add-designation-form" className="btn width btn-success" defaultValue="SAVE" />
                                            <button type="button" onClick={this.props.onCloseModal} className="btn btn-secondary" data-dismiss="modal">Close</button>
                                    </div>

                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
                <div className="modal-backdrop show"></div>
            </Fragment>

        )
    }
}
// AddDesignationNew = reduxForm({
//     form:'addDesignationModalform'
//   })(AddDesignationNew)
export default AddDesignationNew