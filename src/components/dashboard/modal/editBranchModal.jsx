import React, {Component,Fragment} from 'react'
import { Field, reduxForm } from 'redux-form'
import {countryReorder} from '../../../countryReorder'
import {alphabetOnly} from '../../../constants/validations'



const MODAL_OPEN_CLASS = "modal-open";
const required =  value => {
    return(value || typeof value === 'number' ? undefined : 'Please enter your branch name')
}

const requiredPlace =  value => {
    return(value || typeof value === 'number' ? undefined : 'Please select your ')
}

const renderFieldText = ({ input, label, name, type,rows,cols, textarea, meta: { touched, error, warning, invalid } }) => {
    const textareaType = <textarea {...input} rows={rows} cols={cols} placeholder={label}  type={type} className={`form-control ${touched && invalid ? 'has-danger' : ''}`}/>;
    // const inputType = <input {...input} placeholder={label}  type={type} className={`form-control ${touched && invalid ? 'has-danger' : ''}`}/>;
    return (
        <div>
            <div>
                { textareaType}
                {touched && ((error && <small className="text-danger">{label} {error}</small>) || (warning && <span>{warning}</span>))}
            </div>
        </div>
    );
};

const maxLength = max => value =>
  value && value.length > max ? `Must be ${max} characters or less` : undefined
const maxLength128 = maxLength(128)  
const maxLength200 = maxLength(200)

const renderField = ({
    input,
    cols,
    label,
    rows,
    type,
    placeholder,
    meta: { touched, error, warning,  }
  }) => {
      
    return(
        <Fragment>
            <input {...input} cols={cols} rows={rows} placeholder={placeholder} type={type} className={(touched && error)? 'input-control border border-danger': 'input-control'}/>
            {touched &&
            ((error && <small className="text-danger">{label} {error}</small>) ||
                (warning && <span>{warning}</span>))}
        </Fragment>
    
  )}

  const renderFieldSelect = ({
    input,
    label,
    country,
    countryState,
    city,
    className,
    meta: { touched, error, warning, }
}) => {

    return (


        <Fragment>
            <select {...input} className={(touched && error)? className+' border border-danger': className}>
                <option value="">{label}</option>
                {(country && label === "Country") &&
                    <Fragment>
                        {country.countryList.map((plan, index) => (
                            <option key={index} value={plan.id}>{plan.name_ascii}</option>
                        ))}</Fragment>
                }
                {(countryState && label === "State") &&
                    <Fragment>
                        {countryState.stateList.map((plan, index) => (
                            <option key={index} value={plan.id}>{plan.name_ascii}</option>
                        ))}</Fragment>
                }
                {(city && label === "City") &&
                    <Fragment>
                        {city.cityList.map((plan, index) => (
                            <option key={index} value={plan.id}>{plan.name_ascii}</option>
                        ))}</Fragment>
                }
            </select>
            <div>
                {touched &&
                    ((error && <small className="text-danger">{error}{label}</small>) ||
                        (warning && <span>{warning}</span>))}
            </div>
        </Fragment>

    )
}

class BranchEditModal extends Component{
    state={
        image:'',
        image_validation_error:false,    
        show_error_message: false

    }
    componentDidMount() {
        document.body.classList.add(MODAL_OPEN_CLASS);
        this.props.fetchCountryList();
        this.props.fetchStatesList(this.props.branch.country.id);
        this.props.fetchCityList(this.props.branch.state.id);


        let data={}
        data.branch_name=this.props.branch.branch_name
        data.country=this.props.branch.country.id
        data.state=this.props.branch.state.id
        data.city=this.props.branch.city.id
        data.address=this.props.branch.address
    

        this.props.initialize(data)
      }
    
    componentWillUnmount() {
        document.body.classList.remove(MODAL_OPEN_CLASS);
      }
      handleCountryChange = (e) => {
         // making fields null
        this.props.change('state','')  
        this.props.change('city','')

        this.props.fetchStatesList(e.target.value);
        this.props.fetchCityList(" ");
    }
    handleStateChange = (e) => {
        // making fields null
        this.props.change('city','')
        
        this.props.fetchCityList(e.target.value);
    }

    submit = (values) => {
        this.setState({show_error_message : true})

        this.props.editBranch(values,this.props.branch.slug)

    }

    closeError =() =>{
        this.setState({show_error_message : false})
    }

    render(){
        const { handleSubmit} = this.props
        return (
            <Fragment>
                <div id="modal-block">

                    <div className="modal form-modal show" id="editBranchModal" role="dialog" style={{paddingRight: "15px", display: "block"}}>
                    <div className="modal-dialog modal-lg">
                        <div className="modal-content">

                        {/* <!-- Modal Header --> */}
                        <div className="modal-header">
                            <h4 className="modal-title">Edit Branch</h4>
                            <button type="button" className="btn btn-close btn-black" data-dismiss="modal" onClick = {this.props.modalClose}><span aria-hidden="true">×</span></button>
                        </div>

                        {/* <!-- Modal body --> */}
                        <div className="modal-body">
                            <form onSubmit={handleSubmit(this.submit)} id="add-branch-form" className="popup-form" action="/organization/ajax-update-branch/24de1acb-4258-48eb-83d4-3e923006d7ff/" method="post" noValidate="">

                                { (this.state.show_error_message && this.props.branches&&this.props.branches.error&&this.props.branches.error.data&&this.props.branches.error.data.non_field_errors)&&
              <div className="alert alert-danger alert-dismissible fade show" role="alert">
                  <button type="button" onClick={this.closeError} className="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
                   {this.props.branches.error.data.non_field_errors[0]}  
                  <br/>
              </div>}                      
                            
                                <div className="form-group row">
                                    <div className="col-md-4">
                                        <label className="form-control-label" htmlFor="id_branch_name">
                                            <label htmlFor="id_branch_name">Branch name</label>
                                        </label>
                                        <span className="text-danger">*</span>
                                    </div>
                                    <div className="col-md-8">
                                        {/* <input type="text" name="branch_name" value="dfdf" placeholder="Enter Branch Name" class="input-control" maxlength="128" required="" id="id_branch_name"/>
                                        <div class="error-message text-danger"></div> */}
                                        <Field 
                                        type="text"
                                        name="branch_name" 
                                        label="Branch Name"
                                        component={renderField}
                                        placeholder="Enter Branch Name"
                                        validate={[required, alphabetOnly, maxLength128]}/>
                                    </div>
                                </div>

                                <div className="form-group row">
                                    <div className="col-md-4">
                                        <label className="form-control-label" htmlFor="id_country">
                                            <label htmlFor="id_country">Country</label>
                                            <span className="text-danger">*</span>
                                        </label>
                                    </div>
                                    <div className="col-md-8 ">
                                        {/* <select name="country" class="input-control country" required="" id="id_country">
                                            <option value="">Country</option>
                                            <option value="105" selected="">India</option>
                                        </select>
                                        <div class="error-message text-danger"></div> */}
                                        <Field
                                            name="country"
                                            type="select"
                                            country={countryReorder(this.props.countryDetails)}
                                            component={renderFieldSelect}
                                            className="input-control select-country"
                                            label="Country"
                                            validate={requiredPlace}
                                            onChange={this.handleCountryChange} />
                                    </div>
                                </div>

                                <div className="form-group row">
                                    <div className="col-md-4">
                                        <label className="form-control-label" htmlFor="id_state">
                                            <label htmlFor="id_state">State</label>
                                            <span className="text-danger">*</span>
                                        </label>
                                    </div>
                                    <div className="col-md-8 ">
                                        {/* <select name="state" class="input-control state" required="" id="id_state">
                                            <option value="">State</option>
                                            <option value="1350">Andaman and Nicobar</option>
                                        </select>
                                        <div class="error-message text-danger"></div> */}
                                        <Field
                                            name="state"
                                            type="select"
                                            countryState={this.props.stateDetails}
                                            component={renderFieldSelect}
                                            className="input-control select-state"
                                            label="State"
                                            validate={requiredPlace}
                                            onChange={this.handleStateChange}
                                        />
                                    </div>
                                </div>

                                <div className="form-group row">
                                    <div className="col-md-4">
                                        <label className="form-control-label" htmlFor="id_city">
                                            <label htmlFor="id_city">City</label>
                                            <span className="text-danger">*</span>
                                        </label>
                                    </div>
                                    <div className="col-md-8 ">
                                        {/* <select name="city" class="input-control city" required="" id="id_city">
                                            <option value="">City</option>

                                            <option value="10980">Along</option>

                                        </select>
                                        <div class="error-message text-danger"></div> */}
                                        <Field
                                            name="city"
                                            type="select"
                                            city={this.props.cityDetails}
                                            component={renderFieldSelect}
                                            className="input-control select-city"
                                            label="City"
                                            validate={requiredPlace} />
                                    </div>
                                </div>

                                <div className="form-group row">
                                    <div className="col-md-4">
                                        <label className="form-control-label" htmlFor="id_address">
                                            <label htmlFor="id_address">Address</label>
                                        </label>
                                </div>
                                <div className="col-md-8 ">
                                    {/* <textarea name="address" cols="40" rows="10" placeholder="Enter Address" class="input-control" id="id_address">fddfd</textarea> */}
                                    <Field 
                                    type="text" 
                                    name="address" 
                                    cols="40" 
                                    rows="10" 
                                    component={renderFieldText}   
                                    validate={[maxLength200]}
                                    className="input-control input-control"
                                    placeholder="Enter Address" />
                                </div>
                                </div>
                            </form>
                        </div>
                        {/* <!-- Modal footer --> */}
                        <div className="modal-footer">
                            <input type="submit" form="add-branch-form" className="btn width btn-success" value="SAVE"/>
                            <button type="button" className="btn btn-secondary" onClick = {this.props.modalClose} data-dismiss="modal">Close</button>
                        </div>

                        </div>
                    </div>
                    </div>
                    </div>
                    <div className="modal-backdrop show"></div>
            </Fragment>
        )
    }
}

BranchEditModal = reduxForm({
    form: 'ModalForm'
})(BranchEditModal)
export default BranchEditModal