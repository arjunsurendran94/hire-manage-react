import React, {Component,Fragment} from 'react'

const MODAL_OPEN_CLASS = "modal-open";

class EmployeeStatusModal extends Component {

    componentDidMount() {
        document.body.classList.add(MODAL_OPEN_CLASS);
        document.body.style.paddingRight = "15px";
        (this.props.subheader !== "employeeDetail" && this.props.subheader !== "roleDetails" && this.props.subheader !== "DesignationDetail")&&this.props.fetchCountryList();
      }
    
    componentWillUnmount() {
        document.body.classList.remove(MODAL_OPEN_CLASS);
        document.body.style.paddingRight = "";
    }

    inActivateEmployee=()=>{
        this.props.onCloseModal()
        this.props.employeeStatusChange(this.props.match.params.employee_slug)
    }

    activateEmployee=()=>{
        this.props.onCloseModal()
        this.props.employeeStatusChange(this.props.match.params.employee_slug)
    }


    render() {
        return (
            <Fragment>
                <div className="modal fade show" id="confirmModal" tabIndex="-1" role="dialog" style={{display: "block"}}>
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-body">
                                <button onClick={this.props.onCloseModal} type="button" className="btn btn-close btn-black" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                                { (this.props.toActive === "active_to_inactive")&&
                                    <form >
                                        <input type="hidden" name="csrfmiddlewaretoken" value="fPJU6JQh4lNscJMP0SwoirMRTiU9dIJ4XKeoIuvqdFz0KY7WnWJjGAcghxFjyNcr"/>
                                        <h4 className="text-center">Are you sure you want to  In-Activate  <br/>"{this.props.name}"?</h4>
                                    </form>
                                }
                                {(this.props.toActive === "inactive_to_active")&&
                                    <form >
                                        <input type="hidden" name="csrfmiddlewaretoken" value="evcVvriGK4IsfzqSVsdEtVIJxQi5kQQCZ2Lt5k3SlpxuaL8aHjrufqBMxDgbyD9h"/>
                                        <h4 className="text-center">Are you sure you want to  Activate  <br/>"Arjun Surendran"?</h4>
                                    </form>
                                }
                                <div className="text-center">
                                    <button onClick={this.props.onCloseModal} type="button" className="btn btn-secondary" style={{marginRight:"5px"}} data-dismiss="modal">Close</button>                         
                                    {(this.props.toActive === "active_to_inactive")&&<button onClick = {this.inActivateEmployee} type="submit" form="modalForm"  className="btn btn-primary  btn-danger  "> In-Activate </button>}
                                    {(this.props.toActive === "inactive_to_active")&&<button onClick = {this.activateEmployee} type="submit" form="modalForm" className="btn btn-primary btn-success  "> Activate </button>}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="modal-backdrop show"></div>
            </Fragment>    
        )
    }
}

export default EmployeeStatusModal