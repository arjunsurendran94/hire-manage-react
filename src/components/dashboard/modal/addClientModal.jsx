import React, {Component,Fragment} from 'react'
import { Field, reduxForm } from 'redux-form'
import { Redirect } from 'react-router-dom'
import {alphabetOnly} from '../../../constants/validations'


const MODAL_OPEN_CLASS = "modal-open";


class PortalPasswordModal extends Component {

    constructor(props) {
        super(props);
        this.state = {
          passwordVal:'',
          passwordError:''
        };
      }

    

    

    myfunct = (event) => {
      
      
      this.props.setClientPortalPassword(this.state.passwordVal)
      
    }

    componentDidMount(){
      
      document.body.classList.add(MODAL_OPEN_CLASS);

      
    }

    componentWillUnmount() {
      document.body.classList.remove(MODAL_OPEN_CLASS);
    }

    validatePassword=(e)=>{
      
      let password=e.target.value
      console.log('pass',password)
      var paswdRegex=  /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{7,15}$/;
      (password.match(paswdRegex))||(password==='')? this.setState({passwordError:'',passwordVal:password}):this.setState({passwordVal:password,passwordError:'Password must be between 7 to 15 characters which contain at least one numeric digit and a special character'})
      
      
    }

    render() {
        console.log('props',this.props.email)
        console.log('state',this.state)

        
        
        return (
            <Fragment>
            <div id="modal-block">
            <div className="modal form-modal show" id="addDepartmentModal" role="dialog" style={{display: 'block', paddingLeft: '15px'}}>
              <div className="modal-dialog modal-lg">
                <div className="modal-content">
                  <div className="modal-header">
                    <h4 className="modal-title">Enable Portal Access</h4>
                    <button type="button" onClick={this.props.onCloseModal} className="btn btn-close btn-black" data-dismiss="modal"><span aria-hidden="true">×</span></button>
                  </div>
                  <div className="modal-body">
                    <form id="add-department-form" className="popup-form" noValidate>
                      
                      

                      <div className="form-group row">
                        <div className="col-md-4">
                          <label className="form-control-label" htmlFor="id_description">
                            <label htmlFor="id_description">Email Address</label></label>
                        </div>
                        <div className="col-md-8 ">
                          {this.props.email?this.props.email:<span className='text-danger'>Please Fill Contact Email</span>}
                        </div>
                      </div>
                      <div className="form-group row">
                        <div className="col-md-4">
                          <label className="form-control-label" htmlFor="id_description">
                            <label htmlFor="id_description">Portal Password</label></label>
                        </div>
                        <div className="col-md-8 ">
                        <input name="password" type="password" onChange={(e)=>this.validatePassword(e)} 
                        className={this.state.passwordError? "fullwidth input-control border border-danger":"fullwidth input-control"} />
                        <span className="text-danger">{this.state.passwordError}</span>
                        </div>
                      </div>
                    </form>
                  </div>
                  <div className="modal-footer">
                    <input type="submit" form="add-department-form" onClick={()=>this.myfunct()} disabled={(this.state.passwordError!="")||(!this.props.email)} className="btn width btn-success" defaultValue="SAVE" />
                    <button type="button" onClick={this.props.onCloseModal} className="btn btn-secondary" data-dismiss="modal">Close</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="modal-backdrop show"></div>
            </Fragment>)
            }
}

export default PortalPasswordModal