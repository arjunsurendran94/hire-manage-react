import React, {Component,Fragment} from 'react'
import { Field, reduxForm } from 'redux-form'
import { Redirect } from 'react-router-dom'
import {alphabetOnly} from '../../../constants/validations'

const required =  value => {
  return(value || typeof value === 'number' ? undefined : ' is Required')
}

const renderRadioField = ({ input, label, type, checked }) => (
  <Fragment>
    
    <label>{label}</label>
    <input {...input} type={type} /> 
    
  </Fragment>
);

const renderField = ({
  input,
  label,
  type,
  existed,
  meta: { touched, error, warning,  }
}) => {
    
  return(
  

    <Fragment>
      <input {...input} placeholder={label} type={type} className={(touched && error)||(existed) ? 'fullwidth input-control border border-danger': 'fullwidth input-control'}/>
      {touched &&
        ((error && <small className="text-danger">{label}{error}</small>) ||
          (warning && <span>{warning}</span>))}
      
      {(existed)&& <small class="text-danger">
          {existed}
      </small>}
      
    </Fragment>
  
)}

const renderFieldSelect = ({
  input,
  label,
  type,
  existed,
  options,
  meta: { touched, error, warning,  }
}) => {
    
  return(
    <Fragment>
      {(options)? 
          <select {...input} className={(touched && error) || (existed) ?
            'fullwidth input-control border border-danger' : 'fullwidth input-control'}>
              <option id='no-branch' value="">-----</option>
              {options.data.map((branch,index)=>
                            <option key={index} value={branch.slug} >{branch.branch_name}  </option>
                              )}
          </select>:<p className="text-danger">Please create a branch first.</p>
      }
      <div>
      {touched &&
        ((error && <small className="text-danger">{label}{error}</small>) ||
          (warning && <span>{warning}</span>))}
      </div>
      {/* {((existed)&&(existed.error.status == 500)) && <small class="text-danger">
          {existed.errors.error}
      </small>} */}
      
    </Fragment>
  
)}

const MODAL_OPEN_CLASS = "modal-open";


class AddDepartment extends Component {

    constructor(props) {
        super(props);
        this.state = {show_branch: false,existed_error:''};
      }

    show_branch=()=>{ 
        this.setState({show_branch:true})
    }
    hide_branch=()=>{ 
      this.setState({show_branch:false})
    }

    componentDidUpdate(prevProps){
      if(this.props.edit&&(this.props.departmentlist!=prevProps.departmentlist)){
        this.props.departmentlist&&this.props.departmentlist.error&&this.props.departmentlist.error.data&&this.props.departmentlist.error.data.non_field_errors&&
        this.props.departmentlist.error.data.non_field_errors[0]==='The fields department_name, organization, branch must make a unique set.'&&
        this.setState({existed_error:'Department Name already exists'})
      }
      else{
        if((this.props.department!=prevProps.department)){
        this.props.department&&this.props.department.departmentCreateError&&this.props.department.departmentCreateError.data&&this.props.department.departmentCreateError.data.non_field_errors&&
        this.props.department.departmentCreateError.data.non_field_errors[0]==='The fields department_name, organization, branch must make a unique set.'&&
        this.setState({existed_error:'Department Name already exists'})
      }
      }
    }

    submit = (values) => {
      if(this.props.edit){
        if(values.owner_type ==="organization"){
          values.branch=''
        }else{
          values.branch=values.branch_id
        }
        // let hasbranch = 'branch_id' in values
        // if (!hasbranch){
        //   values.branch=''
        // }
        this.props.editDepartment(values,this.props.department_obj.slug)
      }
      else{
        this.props.createDepartments(values)
      }
    }

    componentDidMount(){
      
      document.body.classList.add(MODAL_OPEN_CLASS);

      if(this.props.edit){
        let data={};
        data.department_name=this.props.department_obj.department_name;
        data.description=this.props.department_obj.description;
        if(this.props.department_obj.branch===null){
            data.owner_type="organization";
        }
        else{
          data.owner_type="branch";
          data.branch_id=this.props.department_obj.branch;
          this.setState({show_branch:true});
        }
        this.props.edit && this.props.initialize(data);
    }
    else{
      this.props.initialize({owner_type:'organization'})
    }
    }

    componentWillUnmount() {
      document.body.classList.remove(MODAL_OPEN_CLASS);
    }

    clearExistedError =() =>{
      this.setState({existed_error:''})
    }

    render() {
        if(this.props.department && this.props.department.status==='success'){
          return <Redirect to='/organization/organization/' />

        } 
        const { handleSubmit } = this.props
        return (
            <Fragment>
            <div id="modal-block">
            <div className="modal form-modal show" id="addDepartmentModal" role="dialog" style={{display: 'block', paddingLeft: '15px'}}>
              <div className="modal-dialog modal-lg">
                <div className="modal-content">
                  <div className="modal-header">
                    <h4 className="modal-title">{this.props.edit ? 'Edit' : 'Add'} Department</h4>
                    <button type="button" onClick={this.props.onCloseModal} className="btn btn-close btn-black" data-dismiss="modal"><span aria-hidden="true">×</span></button>
                  </div>
                  <div className="modal-body">
                    <form id="add-department-form" className="popup-form" onSubmit={handleSubmit(this.submit)} noValidate>
                      {(this.props.branches&&this.props.branches.data&&this.props.branches.data.length>0)?
                      <div className="form-group row">
                        <div className="col-md-4" />
                        <div className="col-md-8">
                          <label>
                          <Field name="owner_type" component={renderRadioField} type="radio" value="organization" onChange={this.hide_branch} />{' '}
                          Add for {this.props.organization.organization_data.organization_name}
                        </label><br/>
                        <label>
                          <Field name="owner_type" component={renderRadioField} type="radio" value="branch" onChange={this.show_branch}/>{' '}
                          Add for Branch
                        </label>
                          {/* <input type="radio" onChange={()=>this.hide_branch()} id="radio_org" name="selection" defaultChecked /> Add for {this.props.organization.organization_data.organization_name}<br />
                          <input type="radio" onChange={()=>this.show_branch()} id="radio_branch" name="selection" /> Add for Branch */}
                        </div>
                      </div> :
                      <div className="form-group row" id="buttondisable">
                        <div className="col-md-4"></div>
                        <div className="col-md-8 text-info branch">
                          Note: This organization has no branches. add new branch to create departments under branch
                        </div>
                      </div>
                      }

                       { this.state.show_branch && 
                      <div className="form-group row branch" >
                        <div className="col-md-4">
                          <label className="form-control-label" htmlFor="id_branch">
                            <label htmlFor="id_branch">Branch</label></label>
                            <span className="text-danger">*</span>
                        </div>
                        <div className="col-md-8">
                          
                          <Field 
                                        name="branch_id" 
                                        type ="select"
                                        options = {this.props.branches} 
                                        component={renderFieldSelect} 
                                        className="fullwidth" 
                                        label="Branch"
                                        validate={required}/>
                                        
                          <div className="error-message text-danger">
                          </div>
                        </div>
                      </div>
                       }

                      <div className="form-group row">
                        <div className="col-md-4">
                          <label className="form-control-label" htmlFor="id_department_name">
                            <label htmlFor="id_department_name">Department Name</label></label>
                          <span className="text-danger">*</span>
                        </div>
                        <div className="col-md-8">
                        <Field type="text"
                        name="department_name"
                        component={renderField}
                        onChange={this.clearExistedError}
                        existed={this.state.existed_error}
                        label="Department Name"
                        validate={[required,alphabetOnly]}/>
                          <div className="error-message text-danger">
                          </div>
                        </div>
                      </div>
                      <div className="form-group row">
                        <div className="col-md-4">
                          <label className="form-control-label" htmlFor="id_description">
                            <label htmlFor="id_description">Description</label></label>
                        </div>
                        <div className="col-md-8 ">
                          <Field type="text"
                          name="description"
                          cols="40" rows="10"
                          component="textarea" 
                          className="input-control input-control"
                          placeholder="Description" />
                        </div>
                      </div>
                    </form>
                  </div>
                  <div className="modal-footer">
                    <input type="submit" form="add-department-form" className="btn width btn-success" defaultValue="SAVE" />
                    <button type="button" onClick={this.props.onCloseModal} className="btn btn-secondary" data-dismiss="modal">Close</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="modal-backdrop show"></div>
            </Fragment>)
            }
}
AddDepartment = reduxForm({
  form:'addDepartmentModalform'
})(AddDepartment)
export default AddDepartment