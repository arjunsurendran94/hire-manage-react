import React, { Component, Fragment } from 'react'
import { Field, FieldArray, reduxForm } from 'redux-form'
import { Redirect } from 'react-router-dom';
import { Link } from 'react-router-dom'
import { alphabetOnly } from '../../../constants/validations'
import { countryReorder } from '../../../countryReorder'
import Select from 'react-select';
import * as Helper from '../common/helper/helper';
import PortalPasswordModal from '../modal/addClientModal';
import LoaderImage from '../../../static/assets/image/loader.gif';
import 'react-quill/dist/quill.snow.css';
import ReactQuill, {Quill} from 'react-quill';
import AddSubTaskModal from './addSubTaskModal'


// var loaderStyle={
//   width: '3%',
//   position: 'fixed',
//   top: '50%',
//   left: '57%'
// }

// const required = value => {
//   return (value || typeof value === 'number' ? undefined : ' is Required')
// }
// const mobileValid = value => {
//   var mobilenoregex = /^(?!\b(0)\1+\b)(\+?\d{1,3}[. -]?)?\(?\d{3}\)?([. -]?)\d{3}\3\d{4}$/
//   if (value && value.length >= 1) {
//     return (mobilenoregex.test(value) ? undefined : ' is Invalid')
//   }

//   return undefined
// }

const positiveInteger = value =>{
  var positiveregex = /^\s*\d*\s*$/
  return value && positiveregex.test(value) ? undefined : ' is Invalid'
  }

let Inline = Quill.import('blots/inline');
class BoldBlot extends Inline { }
BoldBlot.blotName = 'bold';
BoldBlot.tagName = 'strong';
Quill.register('formats/bold', BoldBlot);

const formats = ["bold"]
const email = value =>
  value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
    ? ' is Invalid'
    : undefined
const websiteValiadtion = value => {
  if (value) {
    var pattern = new RegExp('^(https?:\\/\\/)' + // protocol
      '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
      '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
      '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
      '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
      '(\\#[-a-z\\d_]*)?$', 'i'); // fragment locator
    return !!pattern.test(value) ? undefined : 'Enter a valid url ( eg:https://websitebuilders.com )'
  }
}
const salutationType = [
  { value: 'mr', text: 'Mr' },
  { value: 'ms', text: 'Ms' },
  { value: 'mrs', text: 'Mrs' }
]
const currency = [{ value: "abc", label: "asdf" }, { value: "wer", label: "uji" }]
const renderField = ({
  input,
  label,
  type,
  existed,
  work_phone_number_error,
  mobile_number_error,
  website_error,
  facebook_error,
  pin_code_error,
  meta: { touched, error, warning, }
}) => {

  return (


    <Fragment>
      <input {...input} placeholder={label} type={type} className={(touched && error) ||
       (existed && (label==='Contact Email')) || (work_phone_number_error)  || (mobile_number_error) || (website_error) || (facebook_error) || (pin_code_error)
      ? 'fullwidth input-control border border-danger' : 'fullwidth input-control'} />
      {touched &&
        ((error && <small className="text-danger">{label}{error}</small>) ||
          (warning && <span>{warning}</span>))}

      { (label==='Contact Email') && (existed) && <small class="text-danger">
        {existed}
      </small>}
      { (work_phone_number_error) && <small class="text-danger">
        {work_phone_number_error}
      </small>}
      { (mobile_number_error) && <small class="text-danger">
        {mobile_number_error}
      </small>}
      { (website_error) && <small class="text-danger">
        {website_error}
      </small>}
      { (facebook_error) && <small class="text-danger">
        {facebook_error}
      </small>}
      { (pin_code_error) && <small class="text-danger">
        {pin_code_error}
      </small>}
    </Fragment>

  )
}



const renderFieldSelect = ({
  input,
  label,
  type,
  id,
  existed,
  options,
  country,
  countryState,
  city,
  meta: { touched, error, warning, }
}) => {

  return (
    <Fragment>
      {(options && label === "Salutation") &&
        <select {...input} className={(touched && error) ? 'fullwidth input-control border border-danger' : 'fullwidth input-control'}>
          <option id='salutation_id' value="">Select Salutation</option>
          {options.map((salutation) =>
            <option value={salutation.value} >{salutation.text}</option>
          )}
        </select>
      }
      {(options && label === "Branch") &&
      <Fragment>
        <div className="col-sm-6 form-group">
        <label className="text-light-grey" htmlFor="id_branch">
        Branch
          </label>
        <select {...input} className="fullwidth input-control" >
          <option id='no_designation' value="">Select Branch</option>
          {options.map((branch, index) =>
            <option key={index} value={branch.slug} >{branch.branch_name}  </option>
          )}
        </select>
        </div>
      </Fragment>
      }
      {(options && label === "Department") &&
        <select {...input} className="fullwidth input-control">
          <option id='no_designation' value="">Select Department</option>
          {options.map((department, index) =>
            <option key={index} value={department.slug} >{department.department_name}  </option>
          )}
        </select>
      }

      {(label === "Country" || label === "State" || label === "City") &&
        <select {...input} className={(touched && error) ? 'input-control border border-danger' : 'input-control'}>
          <option value="">{label}</option>
          {(country && label === "Country") &&
            <Fragment>
              {country.countryList.map((plan, index) => (
                <option key={index} value={plan.id}>{plan.name_ascii}</option>
              ))}</Fragment>
          }
          {(countryState && label === "State") &&
            <Fragment>
              {countryState.stateList.map((plan, index) => (
                <option key={index} value={plan.id}>{plan.name_ascii}</option>
              ))}</Fragment>
          }
          {(city && label === "City") &&
            <Fragment>
              {city.cityList.map((plan, index) => (
                <option key={index} value={plan.id}>{plan.name_ascii}</option>
              ))}</Fragment>
          }
        </select>
      }

      {(label === "Currency") &&
        <Fragment>
          <select {...input} name="currency" className="input-control" >
            {options.map((option) => (
              <option key={option.value} id={option} value={option.value}>{option.label}</option>
            ))}
          </select>
        </Fragment>
      }

      {
        (label === "Portal Language") &&
        <Fragment>
          <select {...input} className="input-control " >
            <option value="">Select Portal Language</option>
            <option value="English">English</option>
            {/* {Helper.languages.map(option=>{
                                  return <option key={option.value} id={option} value={option.value}>{option.label}</option>
                                })} */}
          </select>
        </Fragment>
      }

      {
        (label === "Payment Terms")&&
        <Fragment>
          <select {...input} className="input-control ">
            <option value="">Select Payment Terms</option>
            {options.map(option=>
              <option key={option.value} value={option.value}>{option.text}</option>
              )}
          </select>
        </Fragment>
      }
      {
        (label==="Enable Portal")&&
        <Fragment>

        </Fragment>
      }

      <div>
        {touched &&
          ((error && <small className="text-danger">{label}{error}</small>) ||
            (warning && <span>{warning}</span>))}
      </div>

    </Fragment>
  )
}




const MODAL_OPEN_CLASS = "modal-open";

const required =  value => {
    
    return(value || typeof value === 'number' ? undefined : ' is Required')
}

const mobileValid = value => {
    var mobilenoregex = /^(?!\b(0)\1+\b)(\+?\d{1,3}[. -]?)?\(?\d{3}\)?([. -]?)\d{3}\3\d{4}$/
    if (value && value.length >= 1) {
        return (mobilenoregex.test(value) ? undefined : 'Enter a Valid Mobile Number')
    }
    
    return undefined
}

const maxLength = max => value =>
  value && value.length > max ? `Must be ${max} characters or less` : undefined

const maxLength128 = maxLength(128)



const renderDateField = ({ input, label, options, type, startingDate,checked, meta: { touched, error, warning } }) => (
    <Fragment>
 
       <input {...input} type="date"
                               name="start_date"
                               className="fullwidth input-control"
                               label="Start Date"
                               id="myDate" 
                               value={startingDate}
                               
                            />
 
       {touched &&
       ((error && <small className="text-danger">{label}{error}</small>) ||
       (warning && <span>{warning}</span>))}
    </Fragment>
    )

  const RenderSelectInput = ({input,label, userList,options, name, id,meta: { touched, error, warning }}) =>{ 
    const handleBlur = e => e.preventDefault();
    
    return(
      <React.Fragment>
            <Select {...input} options={userList} onChange={input.onChange}
         onBlur={handleBlur} />
         <small className="text-danger">{error}</small>
         {touched &&
       ((error && <small className="text-danger">{label}{error}</small>) ||
       (warning && <span>{warning}</span>))}
         </React.Fragment>
  )}


var employeeList=[];
var userList=[];


class AddTask extends Component{
    constructor(props){
        super(props)
        this.state = {
            show_branch: false,
            contactNoError:'',
            pincodeError: '',
            other_details: true,
            assigned_error:'',
            assigned_val:'',
            taskdescriptionVal:'',
            userList:[],
            address_active: false,
            contactPerson_active: false,
            remarks_active: false,
            editModal:false,
              editPart:'',
              index:''

        }
    }
  
  closeSuccessMessage = () => {
    this.setState({
      success_message: false
    })
  }
  addTaskModal=(editPart,index)=>{
    console.log("editpart",editPart)
    
    this.setState({editModal:true,editPart:editPart,index:index})
 }
 closeEditModal = () =>{
    this.setState({editModal:false,editPart:'',index:''})
 }

    componentDidMount(){
        document.body.classList.add(MODAL_OPEN_CLASS);
        this.props.listEmployees('')
    }

    componentDidUpdate(prevProps){
      if (this.props.employees!=prevProps.employees&&this.props.employees&&this.props.employees.allEmployees&&this.props.employees.allEmployees.response){
              employeeList=this.props.employees.allEmployees.response.filter((obj)=>
            obj.is_owner===false)
            
            if(userList.length==0&&employeeList.length>0){
             
              employeeList&&employeeList.map((emp)=>userList.push(
                {name:"user",value:emp.slug,label:emp.first_name+' '+emp.last_name}
               ))
              
              }

              this.setState({userList:userList})
            }

            if(prevProps.tasks!=this.props.tasks&&this.props.tasks&&this.props.tasks.status==='createsuccess'){
              this.setState({editModal:false,editPart:'',index:''})
              
            }
              
    }

    componentWillUnmount() {
        document.body.classList.remove(MODAL_OPEN_CLASS);
    }

    validateMethod = () => {
        if(this.state.show_branch){
            return required
        }else{
            return null
        }
    }

    taskdescription=(e)=>{
      console.log('lll',e);
      this.setState({taskdescriptionVal:e})
    }
    
    submit = (values) => {    
      this.state.assigned_val===''?this.setState({assigned_error:'Assigned is required'}):
      values.project=this.props.match.params.project_slug
      values.assigned=[]
      values.assigned.push(this.state.assigned_val)
      values.task_description=this.state.taskdescriptionVal
      
      console.log('values',values)
      const form_data = new FormData();
        for (var key in values) {
            form_data.append(key, values[key]);
        }
      this.props.addTask(form_data)
    }

    clearBackendError =(params)=>{
        
        if(params==='contact-no'){
            this.setState({contactNoError:''})
        }
        if(params==='pincode'){
            this.setState({pincodeError:''})
        }
    }

    assignedValidation=(e)=>{
      
      this.setState({assigned_error:'',assigned_val:e.value})
    
    }
    render(){

        
      
        
        console.log('propssssssssssssssssssss',this.props);
        
        
        const Currency = Helper.Currency
        
          

        const { handleSubmit} = this.props;
        return(
            <Fragment>
                <div id="modal-block">
                    <div className="modal form-modal show" id="editBasicDetails" tabIndex="-1" role="dialog" aria-labelledby="TermsLabel" style={{paddingRight: "15px", display: "block"}}>
                        <div className="modal-dialog modal-dialog-centered" role="document">
                            <div className="modal-content">
                                <div className="modal-header" style={{padding: "2rem 2rem"}}>
                                   
                                    <div>
                                    <h5 className="modal-title" id="TermsLabel">?????????</h5>
                                    
                                    
                                    
                                  </div>
                                    
                                   

                                    
                                </div>
                                {/* {this.props.editPart==="project"? */}
                                <div className="modal-body">
                                    
                                <form className="row" id="create-project-form" onSubmit={handleSubmit(this.submit)}  enctype="multipart/form-data">
                              <div className="col-sm-6 form-group">
                     <label for="id_project_name" className="text-light-grey">
                     Project name<span className="text-danger">*</span>
                     </label>

                     <Field type="text"
                        name="project_name"
                        component={renderField}
                        label="Project Name"
                        //  validate={required}
                        // onChange={this.clearExistedError}
                        // existed={this.state.existed_error}
                        />

                  </div>
                  
                  <div className="col-sm-6 form-group">
                     <label className="text-light-grey" for="id_estimated_time">Estimated Hour</label><span className="text-danger">*</span>
                     <Field type="text"
                        name="estimated_time"
                        component={renderField}
                        label="Estimated Hours"

                    />
                  </div>    
                  
                  <div className="col-sm-6 form-group">
                     <label for="id_client" className="text-light-grey">
                     Start Date
                     </label><span className="text-danger">*</span>
                     <Field type="date"
                        name="start_date"
                        component={renderField}
                        label="Start Date"
                        //  validate={required}
                        // onChange={this.clearExistedError}
                        // existed={this.state.existed_error}
                        />
                  </div>
                  <div className="col-sm-6 form-group">
                     <label for="id_client" className="text-light-grey">
                     End Date
                     </label><span className="text-danger">*</span>
                     <Field type="date"
                        name="end_date"
                        component={renderField}
                        label="End Date"
                         validate={required}
                         onChange={this.clearExistedError}
                         existed={this.state.existed_error}
                        />
                  </div>
                  <div className="col-sm-12 form-group">
                     <label for="id_project_description" className="text-light-grey">
                     Project description<span className="text-danger">*</span>
                     </label>
                     <ReactQuill>
        <div className="my-editing-area"/>
      </ReactQuill>
                     {/* <Field type="text"
                        name="project_description"
                        cols="40" rows="10"
                        component={renderTextField}
                        validate={required}
                        className="input-control input-control"
                        label="Task Description"
                        placeholder="Project Description" /> */}
                  </div>
                                          
                                   
                    {/* <div className="col-sm-6 form-group">
                     <label className="text-light-grey" for="id_estimated_time">Hours Spent</label><span className="text-danger">*</span>
                           
                     <Field type="text"
                        name="estimated_time"
                        component={renderField}
                        label="Hours Spent"

                    /> 
                           <span className="text-danger">{this.state.startingDateError}</span>
                           
                  </div> */}
                                    <div className="modal-footer">
                                        <input type="submit"  className="btn btn-success" value="Save"/>
                                    </div>
                                    </form>
   
                                </div>
                             
                             {/* :null} */}
                           
                             

                            </div>
                        </div>
                    </div>
                </div>
                <div className="modal-backdrop show"></div>
                
             
             
            </Fragment>
        )
    }
}
AddTask = reduxForm({
    form:'addTaskForm'
})(AddTask)
export default AddTask