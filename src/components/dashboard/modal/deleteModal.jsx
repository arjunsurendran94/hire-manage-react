import React, {Component,Fragment} from 'react'

const MODAL_OPEN_CLASS = "modal-open";

class DeleteModal extends Component {

    componentDidMount() {
        document.body.classList.add(MODAL_OPEN_CLASS);
        document.body.style.paddingRight = "15px";
        (this.props.subheader !== "employeeDetail" && this.props.subheader !== "roleDetails" && this.props.subheader !== "DesignationDetail")&&this.props.fetchCountryList();
      }
    
    componentWillUnmount() {
        document.body.classList.remove(MODAL_OPEN_CLASS);
        document.body.style.paddingRight = "";
    }
    deleteDepartment=()=>{
        this.props.onCloseModal()
        this.props.deleteDepartment(this.props.slug,this.props.name)
        
    }
    deleteBranch=()=>{
        this.props.onCloseModal()
        this.props.deleteBranch(this.props.branch.slug,this.props.branch.branch_name)
    }
    deleteOrganization=()=>{
        this.props.onCloseModal()
        this.props.deleteOrganization(localStorage.getItem('organization_slug')||null)
        
    }
    deleteRole=()=>{
        this.props.onCloseModal()
        this.props.deleteRole(this.props.match.params.role_slug)
        
    }
    deleteDesignation=()=>{
        this.props.onCloseModal()
        this.props.setShowMessage()
        this.props.DeleteDesignation(this.props.match.params.designation_slug)
    }
    deleteEmployee=()=>{
        this.props.onCloseModal()
        this.props.cannotDelete()
        // this.props.DeleteEmployee(this.props.match.params.employee_slug,this.props.name)
    }

    render() {
        return (
            <Fragment>
                <div className="modal fade show" id="deletemodal" tabIndex="-1" role="dialog" style={{paddingRight: "15px", display: "block"}}>
                    <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-body">
                            <button type="button" onClick={this.props.onCloseModal} className="btn btn-close btn-black" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            <form id="deleteform" method="post" action="/departments/delete-department/d839b15f-c24d-4427-9ade-46e86e3c904a/">
                                <input type="hidden" name="csrfmiddlewaretoken" value="6LWV5ZrEgc4TSYzIK0yqeSD1AjLxKwpQp2Zt498LWwIysojIewIatOy492zVxREW"/>
                                <h4 className="text-center">Are you sure you want to Delete ?</h4>
                            </form>
                            <div className="text-center">
                                <button type="button"  onClick={this.props.onCloseModal} style={{marginRight:"5px"}} className="btn btn-secondary" data-dismiss="modal">Close</button>
                                {(this.props.deleteType ==="department") &&<button type="button" onClick={this.deleteDepartment} form="deleteform" className="btn btn-primary btn-danger "> Delete </button>}
                                {(this.props.deleteType ==="branch") &&<button type="button" onClick={this.deleteBranch} form="deleteform" className="btn btn-primary btn-danger "> Delete </button>}
                                {(this.props.deleteType ==="organization") &&<button type="button" onClick={this.deleteOrganization} form="deleteform" className="btn btn-primary btn-danger "> Delete </button>}
                                {(this.props.deleteType ==="role") &&<button type="button" onClick={this.deleteRole} form="deleteform" className="btn btn-primary btn-danger "> Confirm </button>}
                                {(this.props.deleteType ==="designation") &&<button type="button" onClick={this.deleteDesignation} form="deleteform" className="btn btn-primary btn-danger "> Confirm </button>}
                                {(this.props.deleteType ==="employee") &&<button type="button" onClick={this.deleteEmployee} form="deleteform" className="btn btn-primary btn-danger "> Delete </button>}


                            </div>
                        </div>
                    </div>
                    </div>
                </div>
                <div className="modal-backdrop show"></div>
            </Fragment>    
        )
    }
}

export default DeleteModal