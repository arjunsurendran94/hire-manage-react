import React,{Component,Fragment} from 'react'
import { Field,reduxForm} from 'redux-form'
import {countryReorder} from '../../../countryReorder'

const MODAL_OPEN_CLASS = "modal-open";

const required =  value => {
    return(value || typeof value === 'number' ? undefined : ' is Required')
}

const mobileValid = value => {
    var mobilenoregex = /^(?!\b(0)\1+\b)(\+?\d{1,3}[. -]?)?\(?\d{3}\)?([. -]?)\d{3}\3\d{4}$/
    if (value && value.length >= 1) {
        return (mobilenoregex.test(value) ? undefined : 'Enter a Valid Mobile Number')
    }
    
    return undefined
}

const maxLength = max => value =>
  value && value.length > max ? `Must be ${max} characters or less` : undefined

const maxLength128 = maxLength(128)

const renderField = ({
    input,
    label,
    type,
    existed,
    backenderror,
    meta: { touched, error, warning,  }
  }) => {
      
    return(
      <Fragment>
        <input {...input} placeholder={label} type={type} className={(touched && error)||(existed&&existed.existedMail) ||(backenderror) ? 'input-control border border-danger': 'input-control'}/>
        {touched &&
          ((error && <small className="text-danger">{label}{error}</small>) ||
            (warning && <span>{warning}</span>))}
        {((existed)&&(existed.existedMail)) && <small className="text-danger">
            {existed.error.data.user.email[0]}
        </small>}
        {backenderror&&
            <small className="text-danger">{backenderror}</small>
            }
        {/* {((existed)&&(existed.error.status == 500)) && <small class="text-danger">
            {existed.errors.error}
        </small>} */}
        
      </Fragment>   
)}


const rendermobileField = ({input,label,type,backenderror,id,className,accept,initialValue,name,meta: { touched, error, warning, }}) => {
    return (          
        <Fragment>
            <input {...input} placeholder = {label} type={type} className={className} id={id} accept={accept} defaultValue={initialValue} />
            {touched && 
                ((error && <small className="text-danger">{error} {label =="Phone number" &&'with country code'}</small>) ||
                    (warning && <span>{warning}</span>))}
            {backenderror&&
            <small className="text-danger">{backenderror} with country code</small>
            }
        </Fragment>
)}


const renderFieldLocationSelect = ({
    input,
    label,
    country,
    countryState,
    city,
    noCity,
    className,
    meta: { touched, error, warning, }
}) => {

    return (
        <Fragment>
        <select {...input} className={(touched && error) ? 'input-control border border-danger' : 'input-control'}>
            <option value="">{label}</option>
            {(country && label === "Country") &&
                <Fragment>
                    {country.countryList.map((plan, index) => (
                        <option key={index} value={plan.id}>{plan.name_ascii}</option>
                    ))}</Fragment>
            }
            {(countryState && label === "State") &&
                <Fragment>
                    {countryState.stateList.map((plan, index) => (
                        <option key={index} value={plan.id}>{plan.name_ascii}</option>
                    ))}</Fragment>
            }
            {(city && label === "City") &&
                <Fragment>
                    {city.cityList.map((plan, index) => (
                        <option key={index} value={plan.id}>{plan.name_ascii}</option>
                    ))}</Fragment>
            }
        </select>
        <div>
            {touched &&
                ((error && <small className="text-danger">{label}{error}</small>) ||
                    (warning && <span>{warning}</span>))}
        </div>
        {(noCity&&noCity.employeeUpdateError&&noCity.employeeUpdateError.data&&noCity.employeeUpdateError.data.city&&noCity.employeeUpdateError.status===400) && <small class="text-danger">
            Please select the city
        </small>}

        
        {/* {((existed)&&(existed.error.status == 500)) && <small class="text-danger">
            {existed.errors.error}
        </small>} */}

    </Fragment>

        // <Fragment>   
        //         {(country && label === "Country") &&
        //             <Fragment>
        //                 <select {...input} className={(touched && error) ? "input-control border border-danger":"input-control"}>
        //                 <option value="">{label}</option>
        //                 {country.countryList.map((plan, index) => (
        //                     <option key={index} value={plan.id}>{plan.name_ascii}</option>
        //                 ))}
        //                 </select>
        //             </Fragment>
        //         }
        //         {(countryState && label === "State") &&
        //             <Fragment>
        //                 <select {...input} className={(touched && error) ? "input-control border border-danger":"input-control"}>
        //                 <option value="">{label}</option>
        //                 {countryState.stateList.map((plan, index) => (
        //                     <option key={index} value={plan.id}>{plan.name_ascii}</option>
        //                 ))}
        //                 </select>
        //             </Fragment>
        //         }
        //         {(label === "City") &&
        //             <Fragment>
        //                 <select {...input} className={(touched && error)||(noCity&&noCity.employeeUpdateError&&noCity.employeeUpdateError.data&&noCity.employeeUpdateError.data.city&&noCity.employeeUpdateError.status===400) ? "input-control border border-danger":"input-control"}>
        //                 <option value="">{label}</option>
        //                 {city.cityList.map((plan, index) => (
        //                     <option key={index} value={plan.id}>{plan.name_ascii}</option>
        //                 ))}
        //                 </select>
        //             </Fragment>
        //         }
            
        //     <div>
        //         {touched &&
        //             ((error && <small className="text-danger">{label}{error}</small>) ||
        //                 (warning && <span>{warning}</span>))}
        //     </div>
        //     {(noCity&&noCity.employeeUpdateError&&noCity.employeeUpdateError.data&&noCity.employeeUpdateError.data.city&&noCity.employeeUpdateError.status===400) && <small class="text-danger">
        //     Please select the city
        // </small>}

        // </Fragment>

    )
}
const renderFieldSelect = ({
    input,
    label,
    disable,
    type,
    existed,
    options,
    meta: { touched, error, warning,  }
    }) => {
        
    return(
        <Fragment>
        {(options && label === "Designation")&&
            <select {...input} className={(touched && error) ? "input-control border border-danger":"input-control"}>
                <option id='no_designation' value="">Select Designation</option>
                {options.map((designation,index)=>
                    <option key={index} value={designation.slug} >{designation.designation_name}  </option>
                )}
            </select>
            // :<p className="text-danger">Please create a branch first.</p>
        }
        {(options && label === "Branch")&&
            <select {...input} className={(touched && error) ? "input-control border border-danger":"input-control"} disabled={disable}>
                <option id='no_designation' value="">Select Branch</option>
                {options.map((branch,index)=>
                    <option key={index} value={branch.slug} >{branch.branch_name}  </option>
                )}
            </select>
            // :<p className="text-danger">Please create a branch first.</p>
        }
        {(options && label === "Department")&&
            <select {...input} className="fullwidth input-control">
                <option id='no_designation' value="">Select Department</option>
                {options.map((department,index)=>
                    <option key={index} value={department.slug} >{department.department_name}  </option>
                )}
            </select>
            // :<p className="text-danger">Please create a branch first.</p>
        }
        <div>
        {touched &&
            ((error && <small className="text-danger">{label}{error}</small>) ||
            (warning && <span>{warning}</span>))}
        </div>
        {/* {((existed)&&(existed.error.status == 500)) && <small class="text-danger">
            {existed.errors.error}
        </small>} */}
        
        </Fragment>
    
)}

const renderRadioField = ({ input, label, type, checked }) => (
    <Fragment>
        
        <label>{label}</label>
        <input {...input} type={type} checked={checked} /> 
        
    </Fragment>
);

const checkboxField = ({ input,options,className,makeCheck,edit,type, checked,meta: { touched, error, warning } }) => (
    <Fragment>
        
        {options.map((option, index) => (
            <div className="col-lg-4 col-md-6 mt-3" key={index}>
                <label htmlFor="162">
                    <span className="checkbox">
                        <input type="checkbox" id={option.slug} className={className} name={`${input.name}[${index}]`} value={option.slug} checked={makeCheck.length>0?(makeCheck.indexOf(option.slug) !== -1):(input.value.indexOf(option.slug) !== -1)}
                            onChange={(event) => {
                                
                                if(makeCheck.length>0){
                                    const newValue = [...makeCheck]
                                    if (event.target.checked) {
                                        newValue.push(option.slug);
                                        makeCheck.push(option.slug);
                                    } else {
                                        
                                        newValue.splice(newValue.indexOf(option.slug), 1);
                                        makeCheck.splice(makeCheck.indexOf(option.slug), 1);
                                    }
                                    return input.onChange(newValue);
                                }else{
                                    const newValue = [...input.value];
                                    if (event.target.checked) {
                                        newValue.push(option.slug);
                                    } else {
                                        newValue.splice(newValue.indexOf(option.slug), 1);
                                    }
                                    return input.onChange(newValue);

                                }
                            }}/>
                    </span>
                    {option.group_name}
                </label>
            </div>
            ))}
        
        <label>{touched &&
            ((error && <small className="text-danger">{error}</small>) ||
                (warning && <span>{warning}</span>))}</label>
    </Fragment>
)
var role_List = [], orginal_list=[]
const roleListData = (role,index) => {
    
    // role_List=[]
    role_List.push(role.slug)
    orginal_list=role_List
}


class UpdateEmployee extends Component{
    constructor(props){
        super(props)
        this.state = {
            show_branch: false,
            contactNoError:'',
            pincodeError: ''
        }
    }

    componentDidMount(){
        document.body.classList.add(MODAL_OPEN_CLASS);
        
        if(this.props.employeeObject&&this.props.employeeObject.employeeData){
            this.props.fetchStatesList(this.props.employeeObject.employeeData.nationality.id);

            this.props.employeeObject.employeeData.state&&this.props.fetchCityList(this.props.employeeObject.employeeData.state.id);
            let data={};
            if(this.props.editPart === "basic"){
                data.first_name=this.props.employeeObject.employeeData.first_name;
                data.last_name=this.props.employeeObject.employeeData.last_name;
                data.phone=this.props.employeeObject.employeeData.phone;
            }

            if(this.props.editPart === "primaryInfo"){
                if(this.props.employeeObject&&this.props.employeeObject.employeeData&&
                    this.props.employeeObject.employeeData.branch){
                        this.setState({show_branch:true})
                        data.branch = this.props.employeeObject.employeeData.branch.slug
                        this.props.retrieveDepartments(this.props.employeeObject.employeeData.branch.slug);
                }else{
                    this.setState({show_branch:false})
                }
                data.department = this.props.employeeObject.employeeData.department&&this.props.employeeObject.employeeData.department.slug
                data.designation = this.props.employeeObject.employeeData.designation&&this.props.employeeObject.employeeData.designation.slug
                
            }

            if(this.props.editPart ==="other"){
                data.nationality=this.props.employeeObject.employeeData.nationality.id
                if(this.props.employeeObject.employeeData.state){
                    data.state=this.props.employeeObject.employeeData.state.id
                }
                if(this.props.employeeObject.employeeData.city){
                    data.city=this.props.employeeObject.employeeData.city.id
                }

                data.street_name=this.props.employeeObject.employeeData.street_name;
                data.locality_name=this.props.employeeObject.employeeData.locality_name;
                data.pin_code=this.props.employeeObject.employeeData.pin_code;
                data.email=this.props.employeeObject.employeeData.email;
                data.house_name=this.props.employeeObject.employeeData.house_name;
            }
            if(this.props.editPart ==="roles"){
                if(this.props.employeePermissions&&this.props.employeePermissions.employeePermissions&&this.props.employeePermissions.employeePermissions.permission_groups&&
                    this.props.employeePermissions.employeePermissions.permission_groups.length>0){
                        this.props.employeePermissions.employeePermissions.permission_groups.map((role,index)=>{
                            // role_List.push(role.slug)
                            
                            roleListData(role, index)
                            return true
                        })
                        return role_List
                }
            }
            this.props.initialize(data);
        }
        
    }

    componentDidUpdate(prevProps){
        if(this.props.employees!=prevProps.employees&&this.props.employees&&this.props.employees.employeeUpdateError&&this.props.employees.employeeUpdateError.data){
            if(this.props.employees.employeeUpdateError.data.phone){
                this.setState({contactNoError:this.props.employees.employeeUpdateError.data.phone[0]})
            }
            if(this.props.employees.employeeUpdateError.data.non_field_errors&&this.props.employees.employeeUpdateError.data.non_field_errors[0]==='Invalid pincode'){
                this.setState({pincodeError:this.props.employees.employeeUpdateError.data.non_field_errors[0]})
            }
        }
    }

    componentWillUnmount() {
        
        
        role_List=[]
        this.props.employeePermissions&&this.props.employeePermissions.employeePermissions&&this.props.employeePermissions.employeePermissions.permission_groups&&
                    this.props.employeePermissions.employeePermissions.permission_groups.map(role=>role_List.push(role.slug))


        document.body.classList.remove(MODAL_OPEN_CLASS);
        
        
    }

    select_org=()=>{ 
        this.props.retrieveDepartments('');
        this.setState({show_branch:false})
    }
    
    select_branch=()=>{ 
        this.setState({show_branch:true})   
    }

    handleBranchChange = (e) => {
        this.props.retrieveDepartments(e.target.value);
    }

    handleCountryChange = (e) => {
        //  making fields null
        this.props.change('state','')  
        this.props.change('city','')

        this.props.fetchStatesList(e.target.value);
        this.props.fetchCityList(" ");
    }

    handleStateChange = (e) => {
        // making fields null
        this.props.change('city','')
        
        this.props.fetchCityList(e.target.value);
    }

    // employeeRoles = () => {
    //     let role_List = []
    //     if(this.props.employeeObject&&this.props.employeeObject.employeeData&&this.props.employeeObject.employeeData.designation&&
    //         this.props.employeeObject.employeeData.designation.permission_groups){
    //             this.props.employeeObject.employeeData.designation.permission_groups.map((role,index)=>{
    //                 role_List.push(role.slug)
    //                 return true
    //             })
    //             return role_List
    //     }else{
    //         return role_List
    //     }
        
    // }
    validateMethod = () => {
        if(this.state.show_branch){
            return required
        }else{
            return null
        }
    }

    submit = (values) => {
        if(Object.keys(values).length !== 0){
            
            
            if("permission_groups" in values){
                this.props.updateEmployeePermissions(this.props.employeeObject.employeeData.slug,values)
            }else{
                
                if("owner_type" in values){
                    if(values.owner_type === "organization"){
                        if("branch" in values){
                            values.branch='';
                        }
                    }
                }
                this.props.partialUpdateEmployee(this.props.employeeObject.employeeData.slug,values)
            }
        }else{
            console.log('...')
        }
        
    }
    clearBackendError =(params)=>{
        
        if(params==='contact-no'){
            this.setState({contactNoError:''})
        }
        if(params==='pincode'){
            this.setState({pincodeError:''})
        }
    }
    render(){
    
        const { handleSubmit} = this.props;
        return(
            <Fragment>
                <div id="modal-block">
                    <div className="modal form-modal show" id="editBasicDetails" tabIndex="-1" role="dialog" aria-labelledby="TermsLabel" style={{paddingRight: "15px", display: "block"}}>
                        <div className="modal-dialog modal-dialog-centered" role="document">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <h5 className="modal-title" id="TermsLabel">Edit Basic Details</h5>
                                    <button onClick = {this.props.closeModal} type="button" className="btn btn-black close-button" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>

                                <div className="modal-body">
                                    <form  onSubmit={handleSubmit(this.submit)}>
                                    {(this.props.editPart ==="basic")&&
                                        <div className="row popup-form">
                                            <input type="hidden" name="csrfmiddlewaretoken" value="LXZcavhPpfvZZsgfHMUsbqySuUGozcDMPVPEBXtpDJRMS5FSDy3BPmTiH820KOMD"/>
                                            <div className="col-sm-6 form-group ">
                                                <label htmlFor="id_first_name">First name</label>
                                                <span className="text-danger">*</span>
                                                {/* <input type="text" name="first_name" value="Jisha" maxlength="128" class="input-control" required="" id="id_first_name"/> */}
                                                <Field 
                                                    type="text" 
                                                    name="first_name" 
                                                    component={renderField}
                                                    label="First Name" 
                                                    validate={[required,maxLength128]}/>
                                                <div className="text-danger error-message"></div>
                                            </div>
                                            <div className="col-sm-6 form-group  ">
                                                <label htmlFor="id_last_name">Last name</label>
                                                <span className="text-danger">*</span>
                                                {/* <input type="text" name="last_name" value="Jacks" maxlength="128" class="input-control" required="" id="id_last_name"/> */}
                                                <Field 
                                                    type="text" 
                                                    name="last_name" 
                                                    component={renderField}
                                                    label="Last Name" 
                                                    validate={[required,maxLength128]} />
                                                <div className="text-danger error-message"></div>
                                            </div>
                                            <div className="col-sm-6 form-group  ">
                                                <label htmlFor="id_phone">Phone</label>
                                                {/* <input type="tel" name="phone" value="+919526126445" class="input-control" id="id_phone"/> */}
                                                <Field 
                                                    type="text" 
                                                    name="phone" 
                                                    component={rendermobileField}
                                                    label="Phone number" 
                                                    onChange={()=>this.clearBackendError('contact-no')}
                                                    validate={[mobileValid]} 
                                                    backenderror={this.state.contactNoError}
                                                    className="input-control input-control" />
                                                <div className="text-danger error-message"></div>
                                            </div>
                                        </div>
                                    }
                                    {(this.props.editPart ==="primaryInfo")&&
                                        <div  id="employee-primary-info-form"  className="row popup-form" >
                                            <input type="hidden" name="csrfmiddlewaretoken" value="gzPscFcoLfWxm4IWPCktKoIO8cRF8aaokxFUD7oYZJikfH7zLotCok3elqdhjMjf"/>                                       
                                            <div className="col-sm-6 form-group">
                                                <label htmlFor="id_selection_0">
                                                    <Field name="owner_type" checked={(!this.state.show_branch)?true:false} component={renderRadioField} type="radio" value="organization" onChange={this.select_org} />{' '}Add to Organization
                                                </label>   

                                                {this.props.branches&&this.props.branches.data&&<label htmlFor="id_selection_1">
                                                <Field name="owner_type" checked={this.state.show_branch}  component={renderRadioField} type="radio" value="branch" onChange={this.select_branch}/>{' '}Add to Branch
                                                </label>}

                                            </div>

                                            {this.props.branches&&this.props.branches.data&&<div className="col-sm-6 form-group branch">
                                                <label className="text-light-grey" htmlFor="id_branch">
                                                    <label htmlFor="id_branch">Branch:</label>
                                                </label>
                                                {/* <select name="branch" className="input-control" id="id_branch" disabled="disabled">
                                                    <option value="" selected="">Select Branch</option>
                                                    <option value="70">sdadaf</option>
                                                </select> */}
                                                <Field 
                                                    name="branch" 
                                                    type ="select"
                                                    options = {(this.props.branches&&this.props.branches.data&&this.props.branches.data&&this.props.branches.data.length>0)&&this.props.branches.data} 
                                                    component={renderFieldSelect} 
                                                    className="fullwidth" 
                                                    label="Branch"
                                                    disable = {!this.state.show_branch}
                                                    onChange = {this.handleBranchChange}
                                                    validate={this.validateMethod()}/>
                                                <div className="text-danger error-message"></div>
                                            </div>}
                                            

                                            <div className="col-sm-6 form-group">
                                                <label htmlFor="id_department">Department</label>
                                                {/* <select name="department" className="input-control" id="id_department">
                                                    <option value="" selected="">Select Department</option>
                                                    <option value="30">a</option>
                                                </select> */}
                                                <Field 
                                                    name="department" 
                                                    type ="select"
                                                    options = {(this.props.departmentlist&&this.props.departmentlist.departments)&&this.props.departmentlist.departments} 
                                                    component={renderFieldSelect} 
                                                    className="fullwidth" 
                                                    label="Department"
                                                    // validate={required}
                                                    />
                                                <div className="text-danger error-message"></div>
                                            </div>

                                            <div className="col-sm-6 form-group">
                                                <label htmlFor="">Designation</label>
                                                <span className="text-danger">*</span>
                                                <Field 
                                                    name="designation" 
                                                    type ="select"
                                                    options = {(this.props.designationList&&this.props.designationList.designation)&&this.props.designationList.designation} 
                                                    component={renderFieldSelect} 
                                                    className="fullwidth" 
                                                    label="Designation"
                                                    validate={required}/>
                                                <div className="text-danger error-message"></div>
                                            </div>
                                        </div>
                                    }
                                    {(this.props.editPart === "other")&&
                                        <div id="employee-other-details-form"  className="row popup-form " >
                                            <input type="hidden" name="csrfmiddlewaretoken" value="MwiBw9nAw0AttbHoXtuDgdxvS4bhYOQxQu83XBzaKuWgmO61TfDMU9SV5ixT9qZo"/>
                                        
                                            <div className="col-sm-6 form-group">
                                                <label htmlFor="">Nationality
                                                    <span className="text-danger">*</span>
                                                </label>
                                                <Field
                                                    name="nationality"
                                                    type="select"
                                                    country={countryReorder(this.props.countryDetails)}
                                                    component={renderFieldLocationSelect}
                                                    className="input-control select-country"
                                                    label="Country"
                                                    validate={required}
                                                    onChange={this.handleCountryChange} />  
                                                <div className="text-danger error-message"></div>
                                            </div>
                                            <div className="col-sm-6 form-group">
                                                <label htmlFor="">State
                                                  <span className="text-danger">*</span>
                                                </label>
                                                <Field
                                                    name="state"
                                                    type="select"
                                                    countryState={this.props.stateDetails}
                                                    component={renderFieldLocationSelect}
                                                    className="input-control select-state"
                                                    label="State"
                                                    validate={required}
                                                    onChange={this.handleStateChange}/>  
                                              <div className="text-danger error-message"></div>
                                            </div>
                                            <div className="col-sm-6 form-group">
                                                <label htmlFor="">City
                                                    <span className="text-danger">*</span>
                                                </label>
                                                <Field
                                                    name="city"
                                                    type="select"
                                                    city={this.props.cityDetails}
                                                    component={renderFieldLocationSelect}
                                                    className="input-control select-city"
                                                    noCity = {this.props.employees}
                                                    label="City"
                                                    validate={required} />
                                                <div className="text-danger error-message"></div>
                                            </div>
                                            <div className="col-sm-6 form-group">
                                                <label htmlFor="">House name / flat No</label>
                                                {/* <input type="text" name="house_name" maxlength="128" class="input-control" id="id_house_name"/> */}
                                                <Field 
                                                    type="text" 
                                                    name="house_name" 
                                                    component={renderField}
                                                    label="House name / flat No"/>
                                                <div className="text-danger error-message"></div>
                                            </div>
                                            <div className="col-sm-6 form-group">
                                                <label htmlFor="">Street Name / No</label>
                                                {/* <input type="text" name="street_name" maxlength="128" class="input-control" id="id_street_name"/> */}
                                                <Field 
                                                    type="text" 
                                                    name="street_name" 
                                                    component={renderField}
                                                    label="Street Name / No"/>
                                                <div className="text-danger error-message"></div>
                                            </div>
                                            <div className="col-sm-6 form-group">
                                                <label htmlFor="">Locality Name / No</label>
                                                {/* <input type="text" name="locality_name" maxlength="128" class="input-control" id="id_locality_name"/> */}
                                                <Field 
                                                    type="text" 
                                                    name="locality_name" 
                                                    component={renderField}
                                                    label="Locality Name / No"/>
                                                <div className="text-danger error-message"></div>
                                            </div>
                                            <div className="col-sm-6 form-group">
                                                <label htmlFor="id_pin_code">Pin code</label>
                                                {/* <input type="text" name="pin_code" class="input-control" id="id_pin_code"/> */}
                                                <Field 
                                                    type="text" 
                                                    name="pin_code" 
                                                    component={renderField}
                                                    onChange={()=>this.clearBackendError('pincode')}
                                                    backenderror={this.state.pincodeError}
                                                    label="Zip Code"/>
                                                <div className="text-danger error-message"></div>
                                            </div>
                                        </div>
                                    }
                                    {(this.props.editPart === "roles")&&
                                        <div className="row popup-form " >
                                            <input type="hidden" name="csrfmiddlewaretoken" value="9h1TsnQaaooFewoElAnk4dylZ8e8tvQVdfRlTP2KoSKs79NhhmwtI9TLcmAKE7ZM"/>
                                            {(this.props.roles&&this.props.roles.roles)&&
                                                // this.props.roles.roles.map((role,index) => (
                                                //     <div className="col-lg-4 col-md-6 mt-3" key={index}>
                                                //         <label htmlFor="162">
                                                //             <span className="checkbox">
                                                //                 <input name="permission_groups" type="checkbox" value="162"/>
                                                //             </span>
                                                //             {role.group_name}
                                                //         </label>
                                                //     </div>
                                                // ))
                                                <Field
                                                type="checkbox"
                                                name="permission_groups"
                                                component={checkboxField}
                                                options = {this.props.roles.roles}
                                                makeCheck = {role_List.length>0?role_List:[]}
                                                />
                                            }
                                        </div>
                                    }
                                    <div className="modal-footer">
                                        <input type="submit"  className="btn btn-success" value="Save"/>
                                    </div>
                                    </form>
                                </div>

                                
                            </div>
                        </div>
                    </div>
                </div>
                <div className="modal-backdrop show"></div>
            </Fragment>
        )
    }
}
UpdateEmployee = reduxForm({
    form:'updateEmployeeForm'
})(UpdateEmployee)
export default UpdateEmployee