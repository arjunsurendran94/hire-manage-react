import React, { Component, Fragment } from "react";
import { Field, FieldArray, reduxForm } from "redux-form";
import { Redirect } from "react-router-dom";
import { Link } from "react-router-dom";
import { alphabetOnly } from "../../../constants/validations";
import { countryReorder } from "../../../countryReorder";
import Select from "react-select";
import * as Helper from "../common/helper/helper";
import PortalPasswordModal from "../modal/addClientModal";
import LoaderImage from "../../../static/assets/image/loader.gif";
import "react-quill/dist/quill.snow.css";
import ReactQuill, { Quill } from "react-quill";
import AddSubTaskModal from "./addSubTaskModal";
import ErrorMessage from "../../common/ErrorMessage";
import Moment from "moment";
import TimeField from 'react-simple-timefield';

var loaderStyle={
  width: '3%',
  position: 'fixed',
  top: '50%',
  left: '57%'
}

// const required = value => {
//   return (value || typeof value === 'number' ? undefined : ' is Required')
// }
// const mobileValid = value => {
//   var mobilenoregex = /^(?!\b(0)\1+\b)(\+?\d{1,3}[. -]?)?\(?\d{3}\)?([. -]?)\d{3}\3\d{4}$/
//   if (value && value.length >= 1) {
//     return (mobilenoregex.test(value) ? undefined : ' is Invalid')
//   }

//   return undefined
// }

const positiveInteger = (value) => {
  var positiveregex = /^\s*\d*\s*$/;
  return value && positiveregex.test(value) ? undefined : " is Invalid";
};

const re = /^[0-9\b]+$/;

let Inline = Quill.import("blots/inline");
class BoldBlot extends Inline { }
BoldBlot.blotName = "bold";
BoldBlot.tagName = "strong";
Quill.register("formats/bold", BoldBlot);

const formats = ["bold"];
const email = (value) =>
  value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
    ? " is Invalid"
    : undefined;
const websiteValiadtion = (value) => {
  if (value) {
    var pattern = new RegExp(
      "^(https?:\\/\\/)" + // protocol
      "((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|" + // domain name
      "((\\d{1,3}\\.){3}\\d{1,3}))" + // OR ip (v4) address
      "(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*" + // port and path
      "(\\?[;&a-z\\d%_.~+=-]*)?" + // query string
      "(\\#[-a-z\\d_]*)?$",
      "i"
    ); // fragment locator
    return !!pattern.test(value)
      ? undefined
      : "Enter a valid url ( eg:https://websitebuilders.com )";
  }
};
const projectStatus = [
  { value: "active", label: "Active" },
  { value: "inactive", label: "In Active" },
];
const currency = [
  { value: "abc", label: "asdf" },
  { value: "wer", label: "uji" },
];
const renderField = ({
  input,
  label,
  type,
  existed,
  work_phone_number_error,
  mobile_number_error,
  website_error,
  facebook_error,
  pin_code_error,
  meta: { touched, error, warning },
}) => {
  return (
    <Fragment>
      <input
        {...input}
        placeholder={label}
        type={type}
        className={
          (touched && error) ||
            (existed && label === "Contact Email") ||
            work_phone_number_error ||
            mobile_number_error ||
            website_error ||
            facebook_error ||
            pin_code_error
            ? "fullwidth input-control border border-danger"
            : "fullwidth input-control"
        }
      />
      {touched &&
        ((error && (
          <small className="text-danger">
            {label}
            {error}
          </small>
        )) ||
          (warning && <span>{warning}</span>))}

      {label === "Contact Email" && existed && (
        <small class="text-danger">{existed}</small>
      )}
      {work_phone_number_error && (
        <small class="text-danger">{work_phone_number_error}</small>
      )}
      {mobile_number_error && (
        <small class="text-danger">{mobile_number_error}</small>
      )}
      {website_error && <small class="text-danger">{website_error}</small>}
      {facebook_error && <small class="text-danger">{facebook_error}</small>}
      {pin_code_error && <small class="text-danger">{pin_code_error}</small>}
    </Fragment>
  );
};

const renderFieldSelect = ({
  input,
  label,
  type,
  id,
  existed,
  options,
  country,
  countryState,
  city,
  meta: { touched, error, warning },
}) => {
  return (
    <Fragment>
      {options && label === "Salutation" && (
        <select
          {...input}
          className={
            touched && error
              ? "fullwidth input-control border border-danger"
              : "fullwidth input-control"
          }
        >
          <option id="salutation_id" value="">
            Select Salutation
          </option>
          {options.map((salutation) => (
            <option value={salutation.value}>{salutation.text}</option>
          ))}
        </select>
      )}
      {options && label === "Branch" && (
        <Fragment>
          <div className="col-sm-6 form-group">
            <label className="text-light-grey" htmlFor="id_branch">
              Branch
            </label>
            <select {...input} className="fullwidth input-control">
              <option id="no_designation" value="">
                Select Branch
              </option>
              {options.map((branch, index) => (
                <option key={index} value={branch.slug}>
                  {branch.branch_name}{" "}
                </option>
              ))}
            </select>
          </div>
        </Fragment>
      )}
      {options && label === "Department" && (
        <select {...input} className="fullwidth input-control">
          <option id="no_designation" value="">
            Select Department
          </option>
          {options.map((department, index) => (
            <option key={index} value={department.slug}>
              {department.department_name}{" "}
            </option>
          ))}
        </select>
      )}

      {(label === "Country" || label === "State" || label === "City") && (
        <select
          {...input}
          className={
            touched && error
              ? "input-control border border-danger"
              : "input-control"
          }
        >
          <option value="">{label}</option>
          {country && label === "Country" && (
            <Fragment>
              {country.countryList.map((plan, index) => (
                <option key={index} value={plan.id}>
                  {plan.name_ascii}
                </option>
              ))}
            </Fragment>
          )}
          {countryState && label === "State" && (
            <Fragment>
              {countryState.stateList.map((plan, index) => (
                <option key={index} value={plan.id}>
                  {plan.name_ascii}
                </option>
              ))}
            </Fragment>
          )}
          {city && label === "City" && (
            <Fragment>
              {city.cityList.map((plan, index) => (
                <option key={index} value={plan.id}>
                  {plan.name_ascii}
                </option>
              ))}
            </Fragment>
          )}
        </select>
      )}

      {label === "Currency" && (
        <Fragment>
          <select {...input} name="currency" className="input-control">
            {options.map((option) => (
              <option key={option.value} id={option} value={option.value}>
                {option.label}
              </option>
            ))}
          </select>
        </Fragment>
      )}

      {label === "Portal Language" && (
        <Fragment>
          <select {...input} className="input-control ">
            <option value="">Select Portal Language</option>
            <option value="English">English</option>
            {/* {Helper.languages.map(option=>{
                                  return <option key={option.value} id={option} value={option.value}>{option.label}</option>
                                })} */}
          </select>
        </Fragment>
      )}

      {label === "Payment Terms" && (
        <Fragment>
          <select {...input} className="input-control ">
            <option value="">Select Payment Terms</option>
            {options.map((option) => (
              <option key={option.value} value={option.value}>
                {option.text}
              </option>
            ))}
          </select>
        </Fragment>
      )}
      {label === "Enable Portal" && <Fragment></Fragment>}

      <div>
        {touched &&
          ((error && (
            <small className="text-danger">
              {label}
              {error}
            </small>
          )) ||
            (warning && <span>{warning}</span>))}
      </div>
    </Fragment>
  );
};

const MODAL_OPEN_CLASS = "modal-open";

const required = (value) => {
  return value || typeof value === "number" ? undefined : " is Required";
};

const mobileValid = (value) => {
  var mobilenoregex = /^(?!\b(0)\1+\b)(\+?\d{1,3}[. -]?)?\(?\d{3}\)?([. -]?)\d{3}\3\d{4}$/;
  if (value && value.length >= 1) {
    return mobilenoregex.test(value)
      ? undefined
      : "Enter a Valid Mobile Number";
  }

  return undefined;
};

const maxLength = (max) => (value) =>
  value && value.length > max ? `Must be ${max} characters or less` : undefined;

const maxLength128 = maxLength(128);

const renderDateField = ({
  input,
  label,
  options,
  type,
  startingDate,
  checked,
  meta: { touched, error, warning },
}) => (
    <Fragment>
      <input
        {...input}
        type="date"
        name="start_date"
        className="fullwidth input-control"
        label="Start Date"
        id="myDate"
        value={startingDate}
      />

      {touched &&
        ((error && (
          <small className="text-danger">
            {label}
            {error}
          </small>
        )) ||
          (warning && <span>{warning}</span>))}
    </Fragment>
  );

const RenderSelectInput = ({
  input,
  label,
  userList,
  options,
  name,
  id,
  meta: { touched, error, warning },
}) => {
  const handleBlur = (e) => e.preventDefault();

  return (
    <React.Fragment>
      <Select
        {...input}
        options={userList}
        onChange={input.onChange}
        onBlur={handleBlur}
      />
      <small className="text-danger">{error}</small>
      {touched &&
        ((error && (
          <small className="text-danger">
            {label}
            {error}
          </small>
        )) ||
          (warning && <span>{warning}</span>))}
    </React.Fragment>
  );
};

var employeeList = [];

const colourStyles = {
  control: (styles) => ({ ...styles, backgroundColor: "white" }),
  option: (styles, { data, isDisabled, isFocused, isSelected }) => {
    return {
      ...styles,
      backgroundColor: "white",
      color: "black",
    };
  },
};

let totalLoggedHours=0
let totalBillableHours=0
let totalBilledHours=0

class AddTask extends Component {
  constructor(props) {
    super(props);
    var date = new Date();

    

    var formatedDate = `${date.getFullYear()}-${(
      "0" +
      (date.getMonth() + 1)
    ).slice(-2)}-${("0" + date.getDate()).slice(-2)}`;

    this.state = {
      log_array:[],
      pm_approved: '00:00:00',
      todays_date: formatedDate,
      show_branch: false,
      contactNoError: "",
      pincodeError: "",
      other_details: true,
      assigned_error: "",
      assigned_val: "",
      taskdescriptionVal: "",
      userList: [],
      address_active: false,
      contactPerson_active: false,
      remarks_active: false,
      editModal: false,
      editPart: "",
      index: "",

      project_name:
        this.props.projectData &&
        this.props.projectData.projectObj &&
        this.props.projectData.projectObj.project &&
        this.props.projectData.projectObj.project.project_name,
      start_date:
        this.props.projectData &&
        this.props.projectData.projectObj &&
        this.props.projectData.projectObj.project &&
        this.props.projectData.projectObj.project.start_date,
      end_date:
        this.props.projectData &&
        this.props.projectData.projectObj &&
        this.props.projectData.projectObj.project &&
        this.props.projectData.projectObj.project.end_date,
      client:
        this.props.projectData &&
        this.props.projectData.projectObj &&
        this.props.projectData.projectObj.project &&
        this.props.projectData.projectObj.project.client_slug,
      project_cost:
        this.props.projectData &&
        this.props.projectData.projectObj &&
        this.props.projectData.projectObj.project &&
        this.props.projectData.projectObj.project.project_cost,
      total_hours: "",
      estimated_hours:
        this.props.projectData &&
        this.props.projectData.projectObj &&
        this.props.projectData.projectObj.project &&
        this.props.projectData.projectObj.project.estimated_hours,
      project_description:
        this.props.projectData &&
        this.props.projectData.projectObj &&
        this.props.projectData.projectObj.project &&
        this.props.projectData.projectObj.project.project_description,
      billing_type:
        this.props.projectData &&
        this.props.projectData.projectObj &&
        this.props.projectData.projectObj.project &&
        this.props.projectData.projectObj.project.billing_type,
      project_type:
        this.props.projectData &&
        this.props.projectData.projectObj &&
        this.props.projectData.projectObj.project &&
        this.props.projectData.projectObj.project.project_type,
      projectStatus:
        (this.props.projectData &&
          this.props.projectData.projectObj &&
          this.props.projectData.projectObj.project &&
          this.props.projectData.projectObj.project.project_status ===
          "active") ||
          this.props.projectData?.projectObj?.project?.project_status === "created"
          ? "active"
          : this.props.projectData?.projectObj?.project?.project_status,

      attachments: [],
      termAttachments: [],
      contractAttachments: [],
      showRestDetails: false,
      detail_text: "",

      resource_assigned: "",
      billable: "",
      non_billable: "",
      tracker: "",
      hourly_rate: "",
      weekly_limit: "",

      contract_title: "",
      contractAttachment: "",
      termAttachment: "",
      projectAttachment: "",
      contractStatus: "",

      timeAssigned: "",
      task_start_date: Moment().subtract(1, 'days').format("YYYY-MM-DD"),
      add_start_time: "00:00:00",
      add_end_time: "",
      hours: "",
      minutes: "",
      timeBillable: false,
      timeTask: "",
      timeDescription: "",

      errors: {
        project_name: "",

        start_date: "",
        end_date: "",
        project_cost: "",
        client: "",
        estimated_hours: "",
        project_description: "",
        selectedType: "",
        resourceError: "",
        hourly_rate: "",
        weekly_limit: "",
        billing_type_error: "",

        contract_title: "",
        contractAttachment: "",
        termAttachment: "",
        projectAttachment: "",

        availbleResources: [],
        timeAssigned: "",
        task_start_date: "",
        add_start_time: "",
        add_end_time: "",
        timeTask: "",
        duration: ""
      },
      errorMessages: "",
      weeklyworksheets:[],
      totalBilled:0,
      availbleResources: [],
      availbleResourcesSet:false,
      contractDoesNotExists:false
    };
    this.onTimeChange = this.onTimeChange.bind(this);
  }
  onTimeChange=(event, value)=>{
    
    const newTime = value.replace(/-/g, ':');
    // const time = newTime.substr(0, 5);
    const finalValue = newTime.padEnd(8, this.state[event.target.name]?.substr(5, 3));
    // const timeSecondsCustomColon = timeSeconds.replace(/:/g, '-');

    this.setState({ [event.target.name]:finalValue });
  }
  selectBillable = () => {
    this.setState({
      billable: true,
      non_billable: false,
    });
  };
  selectNonBillable = () => {
    this.setState({
      non_billable: true,
      billable: false,
    });
  };
  closeSuccessMessage = () => {
    this.setState({
      success_message: false,
    });
  };
  addTaskModal = (editPart, index) => {
    

    this.setState({ editModal: true, editPart: editPart, index: index });
  };
  closeEditModal = () => {
    this.setState({ editModal: false, editPart: "", index: "" });
  };

  componentDidMount() {
    document.body.classList.add(MODAL_OPEN_CLASS);

    this.props.listEmployees("");

    // let projectObj, contracts = [], billableResources = [], combined = []

    // if (this.props.projectData && this.props.projectData.status === 'success') {
    //   projectObj = this.props.projectData.projectObj

    

    //   billableResources = projectObj && projectObj.financial_info && projectObj.financial_info.resources &&
    //     projectObj.financial_info.resources.filter(i => i.resource_billing_type === 'billable')

    //   contracts = projectObj && projectObj.financial_info && projectObj.financial_info.contract_attachments

    //   if (Array.isArray(billableResources) && billableResources.length > 0 && Array.isArray(contracts) && contracts.length > 0) {
    //     combined = billableResources.map((i, index) => Object.assign({}, i, contracts[index]))
    //   }
    //   // a.map((i,index)=>Object.assign({},i,arr2[index]))
    

    // }
    // {
    // combined.length > 0 && combined.map((obj, index) =>
    //   this.setState({
    //     hourly_rate: obj && obj.hourly_rate,
    //     weekly_limit: obj && obj.weekly_limit
    //   }))
    // }

    // TEAMMMM

    if (this.props.editPart === "team") {
      
      let data =
        this.props.projectObj &&
        this.props.projectObj.financial_info &&
        this.props.projectObj.financial_info.resources &&
        this.props.projectObj.financial_info.resources.filter(
          (res) => res.resource_id === this.props.id
        );

      let billable, non_billable;

      
      if (data[0].resource_billing_type === "billable") {
        billable = true;
        non_billable = false;
      } else {
        billable = false;
        non_billable = true;
      }

      this.setState({
        resource_assigned: data[0].resource_assigned,
        designation_name:
          data[0].resource_assigned.designation.designation_name,
        billable: billable,
        non_billable: non_billable,
        tracker: data[0].tracker,
      });
    }
    if (this.props.editPart === "attachment-edit") {
      let data = this.props.index;
      
      this.setState({ attachment: data });
    }
    if (this.props.editPart === "financial") {
      let data =
        this.props.projectObj &&
        this.props.projectObj.financial_info &&
        this.props.projectObj.financial_info.resources &&
        this.props.projectObj.financial_info.resources.filter(
          (res) => res.resource_id === this.props.id
        );
      

      this.setState({
        hourly_rate: data[0].hourly_rate,
        weekly_limit: data[0].weekly_limit,
        tracker: data[0].tracker,
        contract: data[0].contract,
      });
    }
    if (this.props.editPart === "status") {
      let contractStatus =
        this.props.projectObj &&
        this.props.projectObj.financial_info &&
        this.props.projectObj.financial_info.contract_attachments &&
        this.props.projectObj.financial_info.contract_attachments.filter(
          (res) => res.contract_id === this.props.id
        );

      this.setState({ contractStatus: contractStatus[0].contract_status });
    }

    if (this.props.editPart === 'edit_log') {
      
      console.log('this.props.timeLogObj',this.props.timeLogObj)
      

      let timeAssigned = { value: this.props.timeLogObj?.employee }
      let timeTask = { value: this.props.timeLogObj?.task }

      let start_date_format = new Date(this.props.timeLogObj?.start_time_format);
      start_date_format = Moment(start_date_format).format("YYYY-MM-DD");

      let start_time_format = new Date(this.props.timeLogObj?.start_time_format);
      // start_time_format.setHours(start_time_format.getHours() + 5);
      // start_time_format.setMinutes(start_time_format.getMinutes() + 30);
      start_time_format = Moment(start_time_format).format("HH:mm:ss");

      let end_time_format = new Date(this.props.timeLogObj?.end_time_format);
      // end_time_format.setHours(end_time_format.getHours() + 5);
      // end_time_format.setMinutes(end_time_format.getMinutes() + 30);
      end_time_format = Moment(end_time_format).format("HH:mm:ss");
      let timeDescription = this.props.timeLogObj?.description
      let pm_approved=(!this.props.timeLogObj?.pm_approved)?"00:00:00":this.props.timeLogObj?.pm_approved

      this.setState({
        timeAssigned: timeAssigned,
        task_start_date: start_date_format,
        add_start_time: start_time_format,
        add_end_time: end_time_format,
        hours: this.props.timeLogObj?.worked_hours?.split(":")[0],
        minutes: this.props.timeLogObj?.worked_hours?.split(":")[1],
        timeBillable: this.props.timeLogObj?.tracking,
        timeTask: timeTask,
        timeDescription: timeDescription,
        pm_approved:pm_approved
      })
    }

    if(this.props.editPart === 'edit_pm_approved'){
      this.setState({pm_approved:(this.props.obj?.pm_approved)?this.props.obj?.pm_approved:'00:00:00'})
    }

    if(this.props.editPart==='view project'&&this.props.projectObj?.project?.billing_type==='hourly'){
      

      this.props.weeklyWorksheetEmp&&this.props.listEmployeeWeeklyWorksheet(this.props.weeklyWorksheetEmp,this.props?.projectObj?.project?.slug)
    }


    if(this.props.editPart==='view project'&&this.props.projectObj?.project?.billing_type==='fixed'){
      
      this.props.listFixedWeeklyWorksheet(this.props?.projectObj?.project?.slug)
    }

  }

  componentDidUpdate(prevProps) {
    if (
      this.props.employees != prevProps.employees &&
      this.props.employees &&
      this.props.employees.allEmployees &&
      this.props.employees.allEmployees.response
    ) {
      let existingResources = [],
        userList = [];

      this.props.projectObj &&
        this.props.projectObj.financial_info &&
        this.props.projectObj.financial_info.resources &&
        this.props.projectObj.financial_info.resources.map((res) =>
          existingResources.push(res.resource_assigned)
        );

      

      // employeeList = this.props.employees.allEmployees.response.filter((obj) =>
      //   (obj.is_owner === false&&(existingResources))
      //   )

      // employeeList = this.props.employees.allEmployees.response.filter(
      // (i) => existingResources.some(x => x.slug != (i.slug&&i.is_owner === false))&&i)

      employeeList = this.props.employees.allEmployees.response.filter(
        (elem) =>
          !existingResources.find(({ slug }) => elem.slug === slug) &&
          elem.is_owner === false
      );

      if (this.props.editPart === "team") {
        employeeList.push(this.state.resource_assigned);
      }

      

      if (userList.length == 0 && employeeList.length > 0) {
        employeeList &&
          employeeList.map((emp) =>
            userList.push({
              name: "user",
              slug: emp.slug,
              designation_name: emp.designation.designation_name,
              label: emp.first_name + " " + emp.last_name,
            })
          );
      }

      this.setState({ userList: userList });
    }

    if (
      prevProps.tasks != this.props.tasks &&
      this.props.tasks &&
      this.props.tasks.status === "createsuccess"
    ) {
      this.setState({ editModal: false, editPart: "", index: "" });
    }

    
    console.log('prevProps.availbleResources',prevProps.availbleResources)

    console.log('this.props.availbleResources',this.props.availbleResources)
    
    
    if (!this.state.availbleResourcesSet) {
      
      this.setState({ availbleResources: this.props.availbleResources ,availbleResourcesSet:true})
    }

    if (this.props.tasks != prevProps.tasks && this.props.tasks && this.props.tasks.status === 'add_time_success') {
      // this.setState({taskResponse:false})
      this.props.closeModal()
    }
    

    if (
      prevProps.tasks != this.props.tasks &&
      this.props.tasks &&
      this.props.tasks.status === 400
    ) {

      let Errors = [],
        ErrorObj = {};
      let errors =
        this.props.tasks &&
        this.props.tasks.error_obj &&
        this.props.tasks.error_obj.data;
      Object.keys(errors).map(
        (item) => (
          Errors.push(errors[item])
        )
      );
      this.setState({ errorMessages: Errors });
    }
    
    if(this.props.projectData!=prevProps.projectData){
      // debugger
      this.setState({weeklyworksheets:this.props.projectData?.weeklyworksheets})
    }

    // if(this.props.projectData!=prevProps.projectData&&this.props.projectData?.weeklyworksheets?.length>0
      
    //   ){
    //     debugger
    //     let hours=0
    //     let a=this.props.projectData?.weeklyworksheets?.worksheets.map((num)=>{
    //     hours=hours+Moment.duration(num.productive_hours, 'HH:mm:ss').asHours()
    //     return hours.toFixed(1)
    // })
    // this.setState({loggedHours:a})
    // }

    if(this.props.projectData!=prevProps.projectData&&this.props.projectData?.weeklyworksheets?.length>0){
      let totalBilled

      if(this.props.projectData?.weeklyworksheets?.length>1){

      totalBilled = this.props.projectData?.weeklyworksheets.reduce( ( sum, { billed } ) => sum + parseInt(billed) , 0)

      // totalBilled=this.props.projectData?.weeklyworksheets.reduce((a,b)=>parseInt(a.billed)+parseInt(b.billed))
      }
      else{
        totalBilled=this.props.projectData?.weeklyworksheets[0].billed
      }
      
      this.setState({totalBilled:totalBilled})
      
      
    }

    if(this.props.projectData!=prevProps.projectData&&this.props.projectData?.status==='edit_employee_worksheet_success'){
      this.closeEditModal()
    }

    if(this.props.projectData!=prevProps.projectData&&this.props.projectData?.projectObj?.project?.billing_type==='fixed'&&
    this.props.projectData?.weeklyworksheets?.length>0
    ){
      let weeklyworksheets=[]
      // let a=this.props.projectData?.weeklyworksheets?.forEach(element => {
        // debugger
      // })
    }

  }

  calculateLoggedhours=(data,index)=>{
    
    
    let hours=0
    let a=data.map((num)=>{
      hours=hours+Moment.duration(num.productive_hours, 'HH:mm:ss').asHours()
    })
    
    
    totalLoggedHours=totalLoggedHours+hours
    
    return hours.toFixed(1)

  }

  calculateBillablehours=(data)=>{
    // debugger
    let hours=0
    if(data){
    let a=data?.map((num)=>{
      if(num.pm_approved!=null){
        hours=hours+Moment.duration(num.pm_approved,'HH:mm:ss').asHours()
      }
    })
  }

    return hours.toFixed(1)
  }

  componentWillUnmount() {
    document.body.classList.remove(MODAL_OPEN_CLASS);
  }

  validateMethod = () => {
    if (this.state.show_branch) {
      return required;
    } else {
      return null;
    }
  };

  taskdescription = (e) => {
    
    this.setState({ taskdescriptionVal: e });
  };

  submit = (values) => {
    this.state.assigned_val === ""
      ? this.setState({ assigned_error: "Assigned is required" })
      : (values.project = this.props.match.params.project_slug);
    values.assigned = [];
    values.assigned.push(this.state.assigned_val);
    values.task_description = this.state.taskdescriptionVal;

    
    const form_data = new FormData();
    for (var key in values) {
      form_data.append(key, values[key]);
    }
    this.props.addTask(form_data);
  };

  clearBackendError = (params) => {
    if (params === "contact-no") {
      this.setState({ contactNoError: "" });
    }
    if (params === "pincode") {
      this.setState({ pincodeError: "" });
    }
  };

  assignedValidation = (e) => {
    this.setState({ assigned_error: "", assigned_val: e.value });
  };



  validate = () => {
    let project_name_error,
      client_error,
      project_cost_error,
      hourly_rate_error,
      start_date_error,
      end_date_error,
      project_description_error,
      estimated_hours_error,
      contract_title_error,
      contract_error,
      selectedType_error = "";
    
    if (!this.state.project_name) {
      project_name_error = "Project Name is required";
    }
    if (this.state.project_type === "client_project" && !this.state.client) {
      client_error = "Client is required";
    }
    if (
      this.state.billing_type === "fixed" &&
      this.state.project_type === "client_project" &&
      !this.state.project_cost
    ) {
      project_cost_error = "Project cost is required";
    }
    // if (this.state.billing_type==="fixed" && !this.state.hourly_rate) {
    //    hourly_rate_error = 'Hourly rate is required'
    // }
    if (!this.state.start_date) {
      start_date_error = "Start Date is required";
    }
    if (
      this.state.billing_type === "fixed" &&
      this.state.project_type === "client_project" &&
      !this.state.end_date
    ) {
      end_date_error = "End Date is required";
    }
    if (!this.state.project_description) {
      project_description_error = "Project Description is required";
    }
    // if (this.state.select_hourly&&!this.state.estimated_hours) {
    //    estimated_hours_error = 'Estimated hours is required'
    // }

    if (
      project_name_error ||
      client_error ||
      project_cost_error ||
      hourly_rate_error ||
      end_date_error ||
      project_description_error ||
      start_date_error ||
      contract_title_error ||
      contract_error ||
      estimated_hours_error ||
      selectedType_error
    ) {
      const errors = {
        ...this.state.errors,
        project_name: project_name_error,
        client: client_error,
        project_cost: project_cost_error,

        start_date: start_date_error,
        end_date: end_date_error,
        project_description: project_description_error,
        estimated_hours: estimated_hours_error,
      };
      this.setState({ errors: errors });

      return false;
    } else {
      return true;
    }
  };

  editSubmit = (params, e) => {
    e.preventDefault();
    let valid = this.validate();

    if (valid) {
      let slug =
        this.props.match &&
        this.props.match.params &&
        this.props.match.params.project_slug;
      let data = {};
      data.project_name = this.state.project_name;
      data.billing_type = this.state.billing_type;
      data.project_type = this.state.project_type;
      data.start_date = this.state.start_date;
      data.end_date = this.state.end_date;
      data.estimated_hours = !this.state.estimated_hours?0:this.state.estimated_hours;
      data.project_cost = this.state.project_cost;
      data.project_description = this.state.project_description;
      data.client = this.state.client;
      data.project_status = this.state.projectStatus;

      

      this.props.editProjectGeneralInfo(data, slug);
    }
  };

  changeBillingType = () => {
    if (this.state.billing_type === "fixed") {
      this.setState({ billing_type: "hourly", estimated_hours: "" });
    } else {
      this.setState({ billing_type: "fixed", project_cost: "" });
    }
  };
  handleClientChange = (input) => {
    // debugger
    let errors = { ...this.state.errors };
    errors["client"] = "";
    this.setState({ client: input.value, errors: errors });
  };

  handleChange = (e) => {
    let errors = { ...this.state.errors };
    errors[e.target.name] = "";
    this.setState({ [e.target.name]: e.target.value, errors: errors });
  };

  handleWeeklyEstimated = (e) => {
    if (e.target.value === "" || re.test(e.target.value)) {
      this.setState({ [e.target.name]: e.target.value });
    } else {
      e.target.value = null;
      this.setState({ [e.target.name]: "" });
    }
  };

  handleresourceChange = (e) => {
    let errors = { ...this.state.errors };
    errors.resourceError = "";
    this.setState({
      resource_assigned: e,
      designation_name: e.designation_name,
      errors: errors,
      billable:"",
      non_billable:""
    });
  };

  handlebillable = (e) => {
    let errors = { ...this.state.errors };
    
    errors.billing_type_error = "";
    let contractDoesNotExists=this.state.contractDoesNotExists

    if(this.props.editPart==='team'){
      contractDoesNotExists=this.contractDoesNotExists()
    }
    this.setState({
      billable: true,
      tracker: true,
      non_billable: false,
      errors: errors,
      contractDoesNotExists:contractDoesNotExists
    });

  };

  contractDoesNotExists=()=>{
    
    let a=this.props.projectObj.financial_info.contract_attachments?.some(i=>i.resource===this.state?.resource_assigned?.slug)
    console.log('a',a);
  
    return !a
  }

  handlenonbillable = (e) => {
    let errors = { ...this.state.errors };
    errors.hourly_rate = "";
    errors.weekly_limit = "";
    errors.billing_type_error = "";
    this.setState({
      billable: false,
      tracker: false,
      non_billable: true,
      errors: errors,

      hourly_rate: "",
      weekly_limit: "",

      contract_title: "",
      contractAttachment: "",
      termAttachment: "",
    });
  };

  trackerOnChange = (e) => {
    this.setState({ tracker: e.target.checked });
  };

  editResource = (e) => {
    
    e.preventDefault();

    let valid = this.validateEditresource(this.state.contractDoesNotExists);

    let data = {};
    if (this.state.billable) {
      data.resource_billing_type = "billable";
    } else {
      data.resource_billing_type = "non_billable";
    }
    data.tracker = this.state.tracker;
    data.resource_assigned = this.state.resource_assigned.slug;

    if(valid){

    if(this.state.contractDoesNotExists&&this.props.projectObj?.project?.billing_type!='fixed'&&this.state.billable){
      

      data.hourly_rate = this.state.hourly_rate;
      data.weekly_limit = this.state.weekly_limit;
      // data.tracker=this.state.tracker
      let contractData={}
      contractData.contract_title=this.state.contract_title
      if(this.state.contractAttachment)
      {
        contractData.contract=this.state.contractAttachment
      }  
      if(this.state.termAttachment)    
      {
        contractData.termAttachment = this.state.termAttachment;
      }      
      contractData.resource_assigned=this.state.resource_assigned.slug
      contractData.financial_info=this.props.projectObj?.financial_info?.id
      
      console.log('contractData',contractData)
      
      let form_data = new FormData();
      
      for (var key in contractData) {
        form_data.append(key, contractData[key]);
      }

      this.props.createContract(form_data)

      this.props.editResource(data, this.props.id);

      
    }
    else{
    this.props.editResource(data, this.props.id);
    }

    }
  };

  createContractNonBillableToBillable=()=>{

    
  }

  weeklyHourlyonChange = (e) => {
    if (e.target.value === "" || re.test(e.target.value)) {
      this.setState({ [e.target.name]: e.target.value });
    } else {
      e.target.value = "";
      this.setState({ [e.target.name]: "" });
    }
  };

  validateAddresource = () => {
    let resourceError,
      hourly_rate_error,
      weekly_limit_error,
      billing_type_error,
      contract_title_error;
    if (this.state.resource_assigned === "") {
      resourceError = "Resource is Required";
    }
    if ((this.state.billable || this.state.non_billable) === "") {
      billing_type_error = "Billing type is required";
    }
    if (
      this.props.projectObj &&
      this.props.projectObj.project &&
      this.props.projectObj.project.billing_type === "hourly" &&
      this.props.projectObj.project &&
      this.props.projectObj.project.project_type === "client_project" &&
      this.state.billable
    ) {
      if (this.state.hourly_rate === "") {
        hourly_rate_error = "Hourly rate is required";
      }
      if (this.state.weekly_limit === "") {
        weekly_limit_error = "Weekly limit is required";
      }
      if (this.state.contract_title === "") {
        contract_title_error = "Contract title is required";
      }
    }
    if (
      resourceError ||
      hourly_rate_error ||
      weekly_limit_error ||
      billing_type_error ||
      contract_title_error
    ) {
      let errors = {
        ...this.state.errors,
        resourceError: resourceError,
        hourly_rate: hourly_rate_error,
        weekly_limit: weekly_limit_error,
        billing_type_error: billing_type_error,
        contract_title: contract_title_error,
      };

      this.setState({ errors: errors });
      return false;
    }
    return true;
  };

  validateEditresource = (contractExists) => {
    let resourceError,
      hourly_rate_error,
      weekly_limit_error,
      billing_type_error,
      contract_title_error;
    if (this.state.resource_assigned === "") {
      resourceError = "Resource is Required";
    }
    if ((this.state.billable || this.state.non_billable) === "") {
      billing_type_error = "Billing type is required";
    }
    if (
      this.props.projectObj &&
      this.props.projectObj.project &&
      this.props.projectObj.project.billing_type === "hourly" &&
      this.props.projectObj.project &&
      this.props.projectObj.project.project_type === "client_project" &&
      this.state.billable && contractExists
    ) {
      if (this.state.hourly_rate === "") {
        hourly_rate_error = "Hourly rate is required";
      }
      if (this.state.weekly_limit === "") {
        weekly_limit_error = "Weekly limit is required";
      }
      if (this.state.contract_title === "") {
        contract_title_error = "Contract title is required";
      }
    }
    if (
      resourceError ||
      hourly_rate_error ||
      weekly_limit_error ||
      billing_type_error ||
      contract_title_error
    ) {
      let errors = {
        ...this.state.errors,
        resourceError: resourceError,
        hourly_rate: hourly_rate_error,
        weekly_limit: weekly_limit_error,
        billing_type_error: billing_type_error,
        contract_title: contract_title_error,
      };

      this.setState({ errors: errors });
      return false;
    }
    return true;
  };

  addResource = (e) => {
    // e?.preventDefault();
    let valid = this.validateAddresource();

    if (valid) {
      let data = {},
        dataContract = {};
      if (this.state.billable) {
        data.resource_billing_type = "billable";
      } else {
        data.resource_billing_type = "non_billable";
      }
      data.hourly_rate = this.state.hourly_rate;
      data.weekly_limit = this.state.weekly_limit;
      data.tracker = this.state.tracker;
      data.resource_assigned = this.state.resource_assigned.slug;
      data.financial_info =
        this.props.projectObj &&
        this.props.projectObj.financial_info &&
        this.props.projectObj.financial_info.id;

      if (
        this.state.billable &&
        this.props.projectObj &&
        this.props.projectObj.project &&
        this.props.projectObj.project.billing_type === "hourly"
      ) {
        data.contract_title = this.state.contract_title;
        if (this.state.contractAttachment != "") {
          data.contract = this.state.contractAttachment;
        }
        if (this.state.termAttachment != "") {
          data.termAttachment = this.state.termAttachment;
        }
        data.resource_assigned_contract = [this.state.resource_assigned.slug];

        this.props.addBillableResource(data);
      } else {
        this.props.addResource(data);
      }
    }
  };

  upLoadAttachment = (e) => {
    e.preventDefault();
    var fileTypes = [
      "image/png",
      "image/jpg",
      "image/gif",
      "image/jpeg",
      "application/pdf",
      "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
      "application/zip",
    ];

    let name = e.target.name;
    let file = e.target.files[0];
    if (file) {
      let file_index = fileTypes.indexOf(file.type);
      if (file_index >= 0) {
        let errors = { ...this.state.errors };
        errors[name] = "";
        this.setState({ [name]: file, errors: errors });
      } else {
        e.target.value = null;
        let errors = { ...this.state.errors };
        errors[name] = "Invalid file format";
        this.setState({ [name]: "", errors: errors });
      }
    }
  };

  validateFinancial = (e) => {
    //  debugger
    e.preventDefault();
    let errors = this.state.errors;
    let validate = true;
    let data = {};
    if (this.state.hourly_rate === "") {
      errors.hourly_rate = "hourly rate is required";
      validate = false;
    }
    if (this.state.weekly_limit === "") {
      errors.weekly_limit = "weekly limit is required";
      validate = false;
    }

    this.setState({
      errors: errors,
    });
    if (validate) {
      let form_data = new FormData();
      data.hourly_rate = this.state.hourly_rate;
      data.weekly_limit = this.state.weekly_limit;
      // data.tracker=this.state.tracker
      for (var key in data) {
        form_data.append(key, data[key]);
      }
      this.props.editResource(form_data, this.props.id);
    }
  };

  validateProjectAttachment = (e) => {
    e.preventDefault();
    if (
      this.state.projectAttachment != "" &&
      this.state.errors &&
      this.state.errors.projectAttachment === ""
    ) {
      let data = {};
      data.project =
        this.props.projectObj &&
        this.props.projectObj.project &&
        this.props.projectObj.project.slug;
      data.attachment = this.state.projectAttachment;
      let form_data = new FormData();
      for (var key in data) {
        form_data.append(key, data[key]);
      }

      this.props.addProjectAttachment(form_data);
    } else {
      let errors = this.state.errors;
      errors.projectAttachment = "Project Attachment is empty";
      this.setState({ errors: errors });
    }
  };

  handleProjectStatus = (input) => {
    // debugger
    this.setState({ projectStatus: input.value });
  };

  validateContract = (e) => {
    e.preventDefault();
    if (!this.state.contractAttachment) {
      let errors = this.state.errors;
      errors.contractAttachment = "Contract Attachment is Empty";

      this.setState({ errors: errors });
    } else {
      let data = {};
      data.contract = this.state.contractAttachment;
      this.props.addContractAttachment(data, this.props.id);
    }
  };
  dateChange = (e) => {
    this.setState({ task_start_date: e.target.value });
  };
  validateTermAttachment = (e) => {
    e.preventDefault();
    if (!this.state.termAttachment) {
      let errors = this.state.errors;
      errors.termAttachment = "Term Attachment is Empty";

      this.setState({ errors: errors });
    } else {
      let data = {};
      data.contract = this.state.termAttachment;
      //  this.props.addContractAttachment(data,this.props.id)
    }
  };

  handleAvailbleResourcesChange = (e) => {
    this.setState(prevstate => ({
      errors: {
        ...prevstate.errors,
        timeAssigned: ''
      },
      timeAssigned: e, timeTask: ""
    }))
  }

  calculateDuration = () => {
    let endTime = Moment(this.state.add_end_time, "HH:mm:ss");
    let startTime = Moment(this.state.add_start_time, "HH:mm:ss")
    let dif = endTime.diff(startTime);
    let dur = Moment.duration(dif);

    let minutes = dur.minutes();
    let hours = dur.hours();

    if (hours < 0 || minutes < 0) {
      this.setState(prevstate => ({
        errors: {
          ...prevstate.errors,
          duration: 'Start Time is greater than End Time'
        },
        hours: "", minutes: ""
      }))
    }
    else {
      this.setState(prevstate => ({
        errors: {
          ...prevstate.errors,
          duration: ""
        },
        hours: hours, minutes: minutes
      }))
    }

  }

  handleChangeStartEndTime = (e) => {
    // debugger
    let name = e.target.name
    let value = e.target.value

    // this.setState(prevstate=>
    // ({errors:{...prevstate.errors,[name]:''},
    //   [name]:value},this.calculateDuration)
    // )
    let errors = { ...this.state.errors }
    errors[name] = ""
    this.setState({ errors: errors, [name]: value }, this.calculateDuration)

  }

  logTime = (e) => {

    e.preventDefault()
    let { timeAssigned, task_start_date, add_start_time, add_end_time, timeTask, duration } = this.state.errors
    // let errors={...this.state.errors}
    // debugger
    let valid = true

    if (!this.state.timeAssigned) {
      valid = false
      timeAssigned = 'Assignee is Required'
    }
    if (!this.state.task_start_date) {
      valid = false
      task_start_date = 'Date is Required'
    }
    if (!this.state.add_start_time) {
      valid = false
      add_start_time = 'Start Time is Required'
    }
    if (!this.state.add_end_time) {
      valid = false
      add_end_time = 'End Time is Required'
    }
    if (!this.state.timeTask?.value) {
      valid = false
      timeTask = 'Task is Required'
    }
    if (this.state.errors.duration) {
      valid = false
    }
    if (valid) {

      let data = {
        employee: this.state.timeAssigned?.value,
        task: (this.state.timeTask?.value) ? this.state.timeTask?.value : null,
        project: this.props.match?.params?.project_slug,
        start_time: this.convertToLFormat(this.state.task_start_date + " " + this.state.add_start_time),
        end_time: this.convertToLFormat(this.state.task_start_date + " " + this.state.add_end_time),
        worked_hours: this.state.hours + ":" + this.state.minutes + ":00",
        productive_hours: this.state.hours + ":" + this.state.minutes + ":00",
        billable_hours: this.state.hours + ":" + this.state.minutes + ":00",
        hourly: this.state.timeBillable,
        description: this.state.timeDescription,
        ...(this.props.editPart === 'edit_log'&&{pm_approved: this.state.pm_approved})
      }
      
      
      this.props.editPart === 'edit_log' ? this.props.editTime(data, this.props.timeLogObj?.slug, this.props.taskType,this.props.dashBoardEdit) :
      
      
      this.props.addTime(data, this.props.taskType)
    }

    else {
      this.setState(prevstate => ({
        errors: {
          ...prevstate.errors,
          timeAssigned: timeAssigned,
          task_start_date: task_start_date,
          add_start_time: add_start_time,
          add_end_time: add_end_time,
          timeTask: timeTask
        }
      }))
    }


  }

  convertToLFormat = (obj) => {
    let dateTime = new Date(obj);

    // dateTime.setHours(dateTime.getHours() + 5);
    // dateTime.setMinutes(dateTime.getMinutes() + 30);

    return Moment(dateTime).format("YYYY-MM-DD HH:mm:ss");
  };


  capitalizeFirstLetter=(str)=> {
  return str.charAt(0).toUpperCase() + str.slice(1);
  }

  editPmApproved=()=>{
    // debugger
    let data={
      pm_approved:this.state.pm_approved,
      start_time:this.props.obj?.start_time_format,
      end_time:this.props.obj?.end_time_format,
    }
    this.props.editTime(data, this.props.obj?.slug,true)
  }

  render() {
 
    console.log('errors',this.state.errors)
    
    
    let hours=0;
    let weeklyworksheets,total_hours, total_billablehours;
    if(this.state?.weeklyworksheets?.length>0){
      
      weeklyworksheets= this.state?.weeklyworksheets?.map((i,index)=>
      total_hours=((i.worksheets)?.map((num)=>{
      hours=hours+Moment.duration(num.productive_hours, 'HH:mm:ss').asHours()
      return hours
    }))
    
    )}


    let billable_hours=0
    if(this.state.weeklyworksheets?.length>0){
      weeklyworksheets= this.state.weeklyworksheets?.map((i,index)=>
      total_billablehours=(i.worksheets).map((num)=>{
        if(num.pm_approved!=null){
          billable_hours=billable_hours+Moment.duration(num.pm_approved,'HH:mm:ss').asHours()
        }
        return billable_hours;
      })
      )
    }

    
   
    let taskLog = []
    if (this.props.taskType === 'myTask') {

      taskLog = this.props.tasks?.tasks.filter(i => i?.assigned?.slug === localStorage.getItem('profile_slug'))
        .map(task => ({
          label: task.task_name, assigned: task.assigned, value: task.slug
        }))
    }
    else {
      taskLog = this.props.tasks?.tasks?.filter(task => task?.assigned?.slug === this.state.timeAssigned?.value).map(task => ({
        label: task.task_name, assigned: task.assigned, value: task.slug
      }))
    }


    let is_admin = localStorage.getItem("user_type") === "admin";
    let permissions = [];
    permissions = localStorage.getItem("permissions");
    if (permissions && permissions.length > 0) {
      permissions = permissions.split(",");
    }

    let comments =
      this.props.projectData && this.props.projectData.projectComments;
    if (
      this.props.projectData &&
      this.props.projectData.status === "editsuccess"
    ) {
      // this.props.closeModal()
      window.location.reload();
    }
    const Currency = Helper.Currency;
    const status = [
      { label: "Created", value: 1, name: "Created" },
      { label: "Started", value: 2, name: "Started" },
      { label: "Oh Hold", value: 3, name: "Oh Hold" },
      { label: "Completed", value: 4, name: "Completed" },
      { label: "Testing", value: 5, name: "Testing" },
      { label: "Re opened", value: 6, name: "Re opened" },
      { label: "Cancelled", value: 7, name: "Cancelled" },
      { label: "Approved", value: 8, name: "Approved" },
      { label: "Billed", value: 9, name: "Billed" },
    ];
    let contract_list = [
      { label: "Active", value: "active", name: "Active" },
      { label: "Paused", value: "paused", name: "Paused" },
    ];
    let task_list = [
      { label: "Task 1", value: "task1", name: "Task 1" },
      { label: "Task 2", value: "task2", name: "Task 2" },
      { label: "Task 3", value: "task3", name: "Task 3" },
      { label: "Task 4", value: "task4", name: "Task 4" },
      { label: "Task 5", value: "task5", name: "Task 5" },
    ];

    let fix_contractAttachments = [],
      fix_terAttachments = [];
    let projectObj =
      this.props.projectData && this.props.projectData.projectObj;
    fix_contractAttachments =
      projectObj &&
      projectObj.financial_info &&
      projectObj.financial_info.contract_attachments;
    fix_terAttachments =
      projectObj &&
      projectObj.financial_info &&
      projectObj.financial_info.term_attachments;

    let fixedFinancialInfo;

    if (fix_contractAttachments && fix_contractAttachments.length > 0) {
      // debugger
      fixedFinancialInfo = Object.assign(
        fix_contractAttachments && fix_contractAttachments[0]
      );
    }

    if (fix_terAttachments && fix_terAttachments.length > 0) {
      
      fixedFinancialInfo = Object.assign(
        fixedFinancialInfo && fix_terAttachments[0]
      );
    }
    if (
      fix_contractAttachments &&
      fix_contractAttachments.length > 0 &&
      fix_terAttachments &&
      fix_terAttachments.length > 0
    ) {
      // debugger
      fixedFinancialInfo = Object.assign(
        fix_contractAttachments[0],
        fix_terAttachments[0]
      );
    }

    

    let combined = this.props?.combined?.filter(
      (i) => i.resource_id === this.props.id
    );



    const { handleSubmit } = this.props;




    return (
      <Fragment>
        
        {this.props.tasks.isLoading?
        <Fragment>
        <div className="loader-img">
          {" "}
          <img src={LoaderImage} style={loaderStyle} />
        </div>
      </Fragment>:
        <Fragment>

        
        <div id="modal-block">
          <div
            className={
              this.state.editPart !== "contractAttachment" &&
                this.state.editPart !== "termAttachment" &&
                this.state.editPart !== "subtask" &&
                this.state.editPart !== "editprojectdetails" &&
                this.state.editPart !== "edittask"
                ? "modal form-modal show"
                : null
            }
            id="editBasicDetails"
            tabIndex="-1"
            role="dialog"
            aria-labelledby="TermsLabel"
            style={{ paddingRight: "15px", display: "block" }}
          >
            <div
              className="modal-dialog modal-dialog-centered"
              role="document"
              style={{
                overflowY: this.props.editPart === "view project" && "initial",
              }}
            >
              <div
                className="modal-content"
                style={{
                  width:
                    this.props.editPart === "view project" && "max-content",
                  padding: "3px",
                }}
              >
                <div className="modal-header" style={{ padding: "2rem 2rem" }}>
                  {this.props.editPart === "log_time" ? (
                    <h5 className="modal-title" id="TermsLabel">
                      Log Time
                    </h5>
                  ) : null}
                   {this.props.editPart ==="edit_pm_approved" ? (
                    <h5 className="modal-title" id="TermsLabel">
                      Edit PM Approved
                    </h5>
                  ) : null}
                  {this.props.editPart === "edit_log" ? (
                    <h5 className="modal-title" id="TermsLabel">
                      Edit Logged Time
                    </h5>
                  ) : null}
                  {this.props.editPart === "task" ? (
                    <h5 className="modal-title" id="TermsLabel">
                      Add Task Details
                    </h5>
                  ) : null}
                  {this.props.editPart === "financial" ? (
                    <h5 className="modal-title" id="TermsLabel">
                      Edit Financial info
                    </h5>
                  ) : null}
                  {this.props.editPart === "team" ? (
                    <h5 className="modal-title" id="TermsLabel">
                      Edit Team Details
                    </h5>
                  ) : null}
                  {this.props.editPart === "team-add" ? (
                    <h5 className="modal-title" id="TermsLabel">
                      Add Team Details
                    </h5>
                  ) : null}

                  

                  {/* {this.state.editPart === "upload" ?
                    <h5 className="modal-title" id="TermsLabel">Upload Contract</h5> : null} */}
                  {this.props.editPart === "status" ? (
                    <h5 className="modal-title" id="TermsLabel">
                      Edit Contract Status
                    </h5>
                  ) : null}
                  {this.props.editPart === "view" &&
                    this.state.editPart !== "edittask" &&
                    this.state.editPart !== "subtask" ? (
                      <div>
                        <h5 className="modal-title" id="TermsLabel">
                          # Task 1
                      </h5>
                        <div className="project-options">
                          <button
                            onClick={() => this.addTaskModal("edittask")}
                            className="btn btn-link-secondary "
                            id="edit-task"
                          >
                            <i
                              className="icon-pencil"
                              style={{ fontSize: "16px" }}
                            ></i>
                          </button>
                          <button
                            className="btn btn-link-secondary "
                            id="delete-task"
                          >
                            <i
                              className="icon-trash"
                              style={{ fontSize: "16px" }}
                            ></i>
                          </button>
                        </div>
                      </div>
                    ) : null}

                  {this.props.editPart === "project" ? (
                    <h5 className="modal-title" id="TermsLabel">
                      Edit Project Details
                    </h5>
                  ) : null}

                  {this.props.editPart === "view project" &&
                    this.state.editPart !== "contractAttachment" &&
                    this.state.editPart !== "termAttachment" ? (
                      <h5 className="modal-title" id="TermsLabel">
                        View Project Details
                      </h5>
                    ) : null}
                  {this.props.editPart === "attachment-edit" ? (
                    <h5 className="modal-title" id="TermsLabel">
                      Edit Attachment
                    </h5>
                  ) : null}
                  {this.props.editPart === "StartEndTimer" ? (
                    <h5 className="modal-title" id="TermsLabel">
                      Confirm {this.props.formatString(this.props.index)} Timer
                    </h5>
                  ) : null}
                  <button
                    onClick={this.props.closeModal}
                    type="button"
                    className="btn btn-black close-button"
                    data-dismiss="modal"
                    aria-label="Close"
                  >
                    <span aria-hidden="true">×</span>
                  </button>

                  {this.props.editPart === "attachment-add" ? (
                    <h5 className="modal-title" id="TermsLabel">
                      Add Attachment
                    </h5>
                  ) : null}
                  <button
                    onClick={this.props.closeModal}
                    type="button"
                    className="btn btn-black close-button"
                    data-dismiss="modal"
                    aria-label="Close"
                  >
                    <span aria-hidden="true">×</span>
                  </button>
                </div>

                {this.props.projectData &&
                  this.props.projectData.status === "error" && (
                    <ErrorMessage
                      closemsg={this.CloseMsg}
                      msg="An Error Occured please refresh the page"
                    />
                  )}

                {this.props.editPart === "attachment-edit" ? (
                  <div className="modal-body">
                    <form
                      className="row"
                      id="create-project-form"
                      enctype="multipart/form-data"
                    >
                      <div className="col-sm-6 form-group">
                        <input
                          type="file"
                          name="contract_formset_set-0-contract"
                          className="form-control"
                          id="id_contract_formset_set-0-contract"
                          defaultValue={this.state.attachment}
                        />
                        <div className="form-control-error-list"></div>
                      </div>
                      <div className="col-sm-6 form-group">
                        <a
                          className="btn btn-outline-primary delete-row"
                          href="javascript:void(0)"
                        >
                          <i class=" icon-delete"></i> Remove
                        </a>
                      </div>
                      <div className="modal-footer">
                        <input
                          className="btn btn-success"
                          style={{ width: "100px" }}
                          value="Save"
                        />
                      </div>
                    </form>
                  </div>
                ) : null}

                {this.props.editPart === "attachment-add" ? (
                  <div className="modal-body">
                    <form
                      className="row"
                      id="create-project-form"
                      enctype="multipart/form-data"
                    >
                      <div className="col-sm-6 form-group">
                        <input
                          type="file"
                          name="projectAttachment"
                          className="form-control"
                          onChange={(e) => this.upLoadAttachment(e)}
                          id="id_contract_formset_set-0-contract"
                        />
                        <div className="form-control-error-list">
                          <span className="text-danger">
                            {this.state.errors.projectAttachment}
                          </span>
                        </div>
                      </div>
                      <div className="col-sm-6 form-group"></div>
                      <div className="modal-footer">
                        <button
                          onClick={(e) => this.validateProjectAttachment(e)}
                          className="btn btn-success"
                          style={{ width: "100px" }}
                        >
                          {" "}
                          Save
                        </button>
                      </div>
                    </form>
                  </div>
                ) : null}

                {this.props.editPart === "project" ? (
                  <div className="modal-body">
                    <form
                      className="row"
                      id="create-project-form"
                      enctype="multipart/form-data"
                    >
                      <div className="col-sm-6 form-group">
                        <label
                          for="id_project_name"
                          className="text-light-grey"
                        >
                          Project name<span className="text-danger">*</span>
                        </label>
                        <input
                          type="text"
                          name="project_name"
                          label="Project Name"
                          className="fullwidth input-control"
                          // validate={required}
                          placeholder="Project Name"
                          defaultValue={this.state.project_name}
                          readOnly
                        // onChange={(e) => this.handleChange(e)}
                        // existed={this.state.existed_error}
                        />
                        <span className="text-danger">
                          {this.state.errors.project_name}
                        </span>
                      </div>
                      {this.state.project_type === "client_project" ? (
                        <div className="col-sm-4 form-group">
                          <label for="id_client" className="text-light-grey">
                            Select Client<span className="text-danger">*</span>
                          </label>
                          <Select
                            value={
                              this.props.clientList &&
                              this.props.clientList.filter(
                                (option) => option.value === this.state.client
                              )
                            }
                            options={this.props.clientList}
                            readOnly
                          // onChange={(e) => this.handleClientChange(e)}
                          />
                          <span className="text-danger">
                            {this.state.errors && this.state.errors.client}
                          </span>
                        </div>
                      ) : null}
                      {/* {this.state.project_type==='client_project'?
                      <div className="col-sm-6 form-group">
                        <label for="id_client" className="text-light-grey">
                          Select Project Type<span className="text-danger">*</span>
                        </label>
                        
                        <Select
                          options={this.props.typeList}
                          value={this.props.typeList&&this.props.typeList.filter(option => option.value === this.state.billing_type)}
                          onChange={()=>this.changeBillingType()}
                        />
                        <span className="text-danger"></span>
                      </div>:null} */}

                      <div className="col-sm-6 form-group">
                        <label
                          className="text-light-grey"
                          for="id_estimated_time"
                        >
                          Start Date
                        </label>
                        <span className="text-danger">*</span>
                        <input
                          type="date"
                          name="start_date"
                          label="Start Date"
                          className="fullwidth input-control"
                          defaultValue={this.state.start_date}
                          // onChange={(e) => this.handleChange(e)}
                          readOnly
                        />
                      </div>
                      <div className="col-sm-6 form-group">
                        <label
                          className="text-light-grey"
                          for="id_estimated_time"
                        >
                          End Date
                        </label>
                        {this.state.billing_type === "fixed" ? (
                          <span className="text-danger">*</span>
                        ) : null}
                        <input
                          type="date"
                          name="end_date"
                          label="End Date"
                          className="fullwidth input-control"
                          defaultValue={this.state.end_date}
                          onChange={(e) => this.handleChange(e)}
                          readOnly={
                            this.props.projectData &&
                            this.props.projectData.projectObj &&
                            this.props.projectData.projectObj.project &&
                            this.props.projectData.projectObj.project
                              .end_date != null
                          }
                        />
                      </div>
                      {this.state.project_type === "client_project" ? (
                        <Fragment>
                          {this.state.billing_type === "fixed" ? (
                            <div className="col-sm-6 form-group">
                              <label
                                className="text-light-grey"
                                for="id_estimated_time"
                              >
                                Project Cost
                                <span className="text-danger">*</span>
                              </label>
                              <input
                                type="text"
                                name="project_cost"
                                className="fullwidth input-control"
                                label="Project Cost"
                                value={this.state.project_cost}
                                placeholder="Project Cost"
                                onChange={(e) => this.handleWeeklyEstimated(e)}
                              />
                              <span className="text-danger">
                                {this.state.errors.project_cost}
                              </span>
                            </div>
                          ) : (
                              <div className="col-sm-6 form-group">
                                <label
                                  className="text-light-grey"
                                  for="id_estimated_time"
                                >
                                  Estimated hours{" "}
                                </label>
                                <input
                                  type="text"
                                  name="estimated_hours"
                                  className="fullwidth input-control"
                                  label="Estimated Hours"
                                  defaultValue={this.state.estimated_hours}
                                  onChange={(e) => this.handleWeeklyEstimated(e)}
                                />
                                <span className="text-danger"></span>
                              </div>
                            )}
                        </Fragment>
                      ) : null}

                      {(is_admin ||
                        (permissions &&
                          permissions.includes("change_project_status"))) && (
                          <div className="col-sm-6 form-group">
                            <label
                              for="id_project_description"
                              className="text-light-grey"
                            >
                              Project status<span className="text-danger">*</span>
                            </label>
                            <Select
                              value={projectStatus.filter(
                                (i) => i.value === this.state.projectStatus
                              )}
                              options={projectStatus}
                              onChange={(e) => this.handleProjectStatus(e)}
                            />
                          </div>
                        )}

                      <div className="col-sm-6 form-group">
                        <label
                          for="id_project_description"
                          className="text-light-grey"
                        >
                          Project description
                          <span className="text-danger">*</span>
                        </label>
                        <textarea
                          name="project_description"
                          cols="40"
                          rows="10"
                          className="input-control input-control"
                          label="Project Description"
                          value={this.state.project_description}
                          onChange={(e) => this.handleChange(e)}
                          placeholder="Project Description"
                        />
                        <span className="text-danger">
                          {this.state.errors &&
                            this.state.errors.project_description}
                        </span>
                      </div>
                      <div className="modal-footer">
                        <button
                          onClick={(e) => this.editSubmit("general", e)}
                          className="btn btn-success"
                        >
                          Save
                        </button>
                      </div>
                    </form>
                  </div>
                ) : null}
                {this.props.editPart === "financial" ? (
                  <div className="modal-body">
                    <form
                      className="row"
                      id="create-project-form"
                      enctype="multipart/form-data"
                    >
                      <div className="col-sm-6 form-group">
                        <label
                          for="id_project_name"
                          className="text-light-grey"
                        >
                          Hourly Rate<span className="text-danger">*</span>
                        </label>
                        <input
                          type="text"
                          name="hourly_rate"
                          label="Hourly Rate"
                          className="fullwidth input-control"
                          // validate={required}
                          placeholder="Hourly Rate"
                          value={this.state.hourly_rate}
                          onChange={(e) => this.handleWeeklyEstimated(e)}
                        // existed={this.state.existed_error}
                        />
                        <span className="text-danger">
                          {this.state.errors.hourly_rate}
                        </span>
                      </div>
                      <div className="col-sm-6 form-group">
                        <label
                          className="text-light-grey"
                          for="id_estimated_time"
                        >
                          Weekly Limit
                        </label>
                        <span className="text-danger">*</span>
                        <input
                          type="text"
                          name="weekly_limit"
                          label="Weekly Limit"
                          className="fullwidth input-control"
                          // validate={required}
                          placeholder="Weekly Limit"
                          value={this.state.weekly_limit}
                          onChange={(e) => this.handleWeeklyEstimated(e)}
                        // existed={this.state.existed_error}
                        />
                        <span className="text-danger">
                          {this.state.errors.weekly_limit}
                        </span>
                      </div>
                      <div className="modal-footer">
                        <button
                          onClick={(e) => this.validateFinancial(e)}
                          className="btn btn-success"
                        >
                          Save
                        </button>
                      </div>
                    </form>
                  </div>
                ) : null}

                {this.props.editPart === "edit_pm_approved" ? (
                  <div className="modal-body">
                   
                      <div className="col-sm-6 form-group" style={{textAlign:"initial"}}>
                        <label
                          className="text-light-grey"
                          for="id_estimated_time"
                          
                        >
                          PM Approved
                        </label><span className="text-danger">*</span>
                       
                        <section>
                          <TimeField
                            showSeconds
                            name="pm_approved"
                            value={this.state.pm_approved}
                            onChange={this.onTimeChange}
                            className="fullwidth input-control "
                            style={{
                              //   border: '2px solid #666',
                              //   fontSize: 42,
                              width: "100%",
                              //   padding: '5px 8px',
                              //   color: '#333',
                              //   borderRadius: 3
                            }}
                          />
                        </section>
                        
                      </div>
                      
                      <div className="modal-footer">
                        <button
                          onClick={(e) => this.editPmApproved()}
                          className="btn btn-success"
                        >
                          Save
                        </button>
                      </div>
                    
                  </div>
                ) : null}
                          
                {this.props.editPart === "status" ? (
                  <div className="modal-body">
                    <form
                      className="row"
                      id="create-project-form"
                      enctype="multipart/form-data"
                    >
                      <div className="col-sm-6 form-group">
                        <label for="id_client" className="text-light-grey">
                          Contract Status
                        </label>

                        <Select
                          options={contract_list}
                          value={contract_list.filter(
                            (i) => i.value === this.state.contractStatus
                          )}
                          onChange={(e) =>
                            this.setState({ contractStatus: e.value })
                          }
                        />
                        <span className="text-danger">
                          {this.props.errors && this.props.errors.client}
                        </span>
                      </div>

                      <div className="modal-footer">
                        <button
                          onClick={(e) =>
                            this.props.editContractStatus(
                              { contract_status: this.state.contractStatus },
                              this.props.id
                            )
                          }
                          className="btn btn-success"
                        >
                          Save
                        </button>
                      </div>
                    </form>
                  </div>
                ) : null}
                
                {(this.props.editPart === "log_time" || this.props.editPart === "edit_log") ? (
                  <div className="modal-body">

                    {this.state.errorMessages &&
                      this.state.errorMessages.length > 0 &&
                      this.state.errorMessages.map((i) => (
                        <ErrorMessage msg={i} />
                      ))}

                    <form
                      className="row"
                      id="create-project-form"
                      enctype="multipart/form-data"
                    >
                      <div className="col-sm-6 form-group" style={{textAlign:"initial"}}>
                        <label for="id_client" className="text-light-grey">
                          Asignee  <span className="text-danger">*</span>
                        </label>

                        {console.log('this.state.availbleResources' ,this.state.availbleResources)}
                        {console.log('this.state.timeAssigned' ,this.state.timeAssigned)}

                        <Select
                          options={this.props.taskType === 'myTask' ?
                            this.state.availbleResources?.filter(obj => obj.value === localStorage.getItem('admin_employee_slug'))
                            : this.state.availbleResources}
                          onChange={(e) =>
                            this.handleAvailbleResourcesChange(e)
                          }
                          value={this.state.availbleResources?.filter(
                            (emp) =>
                              emp.value === this.state.timeAssigned?.value

                          )}
                        />
                        <span className="text-danger">
                          {this.state.errors?.timeAssigned}
                        </span>
                      </div>
                      <div className="col-sm-6 form-group" style={{textAlign:"initial"}}>
                        <label
                          className="text-light-grey"
                          for="id_estimated_time"
                        >
                          Date
                        </label>
                        <span className="text-danger">*</span>

                        <input
                          type="date"
                          name="Date"
                          label="Date"
                          className="fullwidth input-control"
                          // startingDate={this.state.startingDate}
                          //   validate={required}
                          value={this.state.task_start_date}
                          onChange={this.dateChange}
                          max={Moment().subtract(1, 'days').format("YYYY-MM-DD")}
                        />
                        <span className="text-danger">
                          {this.state.errors?.start_date}
                        </span>
                        {/* <span className="text-danger">{this.state.startingDateError}</span> */}
                      </div>
                      <div className="col-sm-4 form-group" style={{textAlign:"initial"}}>
                        <label
                          className="text-light-grey"
                          for="id_estimated_time"
                        >
                          Start Time
                        </label>
                        <span className="text-danger">*</span>

                        <input
                          type="time"
                          name="add_start_time"
                          label="Date"
                          className="fullwidth input-control"
                          // startingDate={this.state.startingDate}
                          //   validate={required}
                          value={this.state.add_start_time}
                          onChange={(e) => this.handleChangeStartEndTime(e)}
                          //  min={this.state.todays_date}
                          step="1"
                        />
                        <span className="text-danger">
                          {this.state.errors?.add_start_time}
                        </span>
                        {/* <span className="text-danger">{this.state.startingDateError}</span> */}
                      </div>
                      <div className="col-sm-4 form-group" style={{textAlign:"initial"}}>
                        <label
                          className="text-light-grey"
                          for="id_estimated_time"
                        >
                          End Time
                        </label>
                        <span className="text-danger">*</span>

                        <input
                          type="time"
                          name="add_end_time"
                          label="Date"
                          className="fullwidth input-control"
                          // startingDate={this.state.startingDate}
                          //   validate={required}
                          value={this.state.add_end_time}
                          onChange={(e) => this.handleChangeStartEndTime(e)}
                          min={this.state.add_start_time}
                          step="1"
                        />
                        <span className="text-danger">
                          {this.state.errors?.add_end_time}
                        </span>
                        {/* <span className="text-danger">{this.state.startingDateError}</span> */}
                      </div>
                      <div className="col-sm-3 form-group" style={{textAlign:"initial"}}>
                        <label className="text-light-grey" title="Time Spent">
                          Time Spent
                        </label>
                        <div>
                          <div className="time-spent">
                            <input class="form-control" type="text" value={this.state.hours} readOnly={true} />
                            <small className="text-light-grey">hours</small>
                          </div>
                          <div className="time-spent">
                            <input class="form-control" type="text" value={this.state.minutes} readOnly={true} />
                            <small className="text-light-grey">minutes</small>
                          </div>
                        </div>
                        <span className="text-danger">{this.state.errors?.duration}</span>
                      </div>


                      {/* <div className="tab-content col-sm-" id="myTaskContent"> */}


                      <div className="col-sm-6 form-group" style={{textAlign:"initial"}}>
                        <label
                          className="text-light-grey"
                          for="id_estimated_time"
                        >
                          Select Task
                        </label> <span className="text-danger">*</span>
                        <Select
                          value={taskLog.filter(i => i.value === this.state.timeTask?.value)}
                          onChange={(e) => this.setState(prevstate => ({ errors: { ...prevstate.errors, timeTask: '' }, timeTask: e }))}
                          options={taskLog} />
                        <span className="text-danger">{this.state.errors?.timeTask}</span>
                      </div>
                      
                      {(is_admin ||
                          (permissions &&
                            permissions.includes("change_pm_approved")))&&<div className="col-sm-6 form-group" style={{textAlign:"initial"}}>
                        <label
                          className="text-light-grey"
                          for="id_estimated_time"
                        >
                          PM Approved
                        </label>
                       
                        <section>
                          <TimeField
                            showSeconds
                            name="pm_approved"
                            value={this.state.pm_approved}
                            onChange={this.onTimeChange}
                            className="fullwidth input-control "
                            style={{
                              //   border: '2px solid #666',
                              //   fontSize: 42,
                              width: "100%",
                              //   padding: '5px 8px',
                              //   color: '#333',
                              //   borderRadius: 3
                            }}
                          />
                        </section>
                        
                      </div>}

                      <div
                        className="col-sm-3 form-group " style={{textAlign:"initial"}}
                      // style={{ marginTop: "20px",display:"flex",justifyContent:"center" }}

                      >
                        {/* <span style={{ marginRight: "10px" }}>{obj.label} {index}</span> */}
                        <input
                          type="checkbox"
                          className="individual-button"
                          checked={this.state.timeBillable}
                          onChange={(e) => this.setState({ timeBillable: e.target.checked })}
                          name="timeBillable"
                        />
                        <label for="internal" className="individual">
                          Billable&nbsp;&nbsp;?
                        </label>
                      </div>



                      {/* <div className="col-sm-12 form-group" style={{textAlign:"initial"}}>
                        <label
                          for="id_project_description"
                          className="text-light-grey"
                        >
                          Task description
                        </label>

                        <textarea
                          name="time_description"
                          cols="40"
                          rows="10"
                          className="input-control input-control"
                          label=" Description"
                          value={this.state.timeDescription}
                          onChange={(e) => this.setState({ timeDescription: e.target.value })}
                          placeholder=" Description"
                        />
                      </div> */}

                      <div className="modal-footer" style={{ width: "100%" }}>
                        <div className="row" style={{ width: "100%" }}>
                          <div className="col-sm-6" style={{textAlign:"initial"}}>
                            <button onClick={this.props.closeModal} className="btn btn-outline-primary add-row">Close</button>
                          </div>
                          <div className="col-sm-6" style={{ textAlign: "end" }}>
                            <button onClick={(e) => this.logTime(e)} className="btn btn-success">Log This Time</button>
                          </div>
                        </div>


                      </div>
                    </form>
                  </div>
                ) : null}

                {/* {this.state.editPart === "upload" ?
                  <div className="modal-body">
                    <form className="row" id="create-project-form" enctype="multipart/form-data"> 
                      <div className="col-sm-12 form-group">
                        <label className="text-light-grey" for="id_estimated_time">Contract</label><span className="text-danger">*</span>
                        <input type="file" className="fullwidth input-control" 
                        name="contractAttachment"
                         onChange={(event) => this.upLoadAttachment(event)} 
                         className="form-control"
                         id="id_contract_formset_set-0-contract" />
                        <span className="text-danger">{this.state.errors.contractAttachment}</span>
                      </div>
                      <div className="modal-footer">
                      <button onClick={(e)=>this.validateContract(e)} className="btn btn-success">Save</button>
                      </div>
                    </form>
                  </div>
                  : null} */}

                

                {this.props.editPart === "team" ? (
                  <div className="modal-body">
                    <form
                      className="row"
                      id="create-project-form"
                      enctype="multipart/form-data"
                    >
                      <Fragment>
                        <div
                          className="sub-header"
                          style={{
                            borderBottom: "none",
                            backgroundColor: "none",
                          }}
                        >
                          {/* {this.state.userList.map((obj, index) => {
                        return ( */}
                          <Fragment>
                          {console.log('{this.state.billable}',this.state.billable,this.state.non_billable)
                            }
                            <div
                              className="row "
                            // style={{ margin: "auto", width: "520px", marginBottom: "20px" }}
                            >
                              <div
                                className=" col-sm-6 "
                                style={{ textAlign: "justify" }}
                              >
                                <label
                                  for="id_client"
                                  className="text-light-grey"
                                >
                                  Select Resource
                                </label>
                                <Select
                                  options={this.state.userList}
                                  value={
                                    this.state.userList &&
                                    this.state.userList.filter(
                                      (i) =>
                                        (this.state.resource_assigned &&
                                          this.state.resource_assigned.slug) ===
                                        i.slug
                                    )
                                  }
                                  onChange={(event) =>
                                    this.handleresourceChange(event)
                                  }
                                />
                                {/* <span className="text-danger">{this.props.resourceError[index]}</span> */}
                              </div>
                              <div
                                className="col-sm-6 "
                                style={{ margin: "auto", paddingTop: "25px" }}
                              >
                                {this.state.designation_name}
                              </div>
                            </div>

                            

                            <div className="row" style={{ marginTop: "20px" }}>
                              <div className="col-sm-9 form-group ">
                                {/* <span style={{ marginRight: "10px" }}>{obj.label} {index}</span> */}
                                <input
                                  type="radio"
                                  className="individual-button"
                                  checked={this.state.billable}
                                  // name={`projectinfo_${index}`}
                                  onClick={this.handlebillable}
                                  value="Internal"
                                />
                                <label for="internal" className="individual">
                                  Billable
                                </label>
                                <input
                                  type="radio"
                                  // name={`projectinfo_${index}`}
                                  checked={this.state.non_billable}
                                  className="individual-button"
                                  onClick={this.handlenonbillable}
                                  value="Client"
                                />
                                <label for="client" className="individual">
                                  Non Billable
                                </label>
                                <span className="text-danger">
                                            {this.state.errors &&
                                              this.state.errors.billing_type_error}
                                </span>
                              </div>

                              {/* <div
                                className="row"
                                style={{
                                  width: "283px",
                                  paddingLeft: "15px",
                                  width: "200px",
                                  marginTop: "20px",
                                }}
                              >
                                <div
                                  className="col-sm-6 form-group"
                                  style={{ marginTop: "auto" }}
                                >
                                  <input
                                    type="checkbox"
                                    onChange={(e) => this.trackerOnChange(e)}
                                    checked={this.state.tracker}
                                  />
                                  <label
                                    for="id_client"
                                    className="text-light-grey"
                                  >
                                    Tracker
                                  </label>
                                </div>
                              </div>
                          */}
                               {this.state.non_billable && (
                                <div
                                  className="row"
                                  style={{
                                    width: "283px",
                                    paddingLeft: "15px",
                                    width: "200px",
                                    marginTop: "20px",
                                  }}
                                >
                                  <div
                                    className="col-sm-6 form-group"
                                    style={{ marginTop: "auto" }}
                                  >
                                    <input
                                      type="checkbox"
                                      onChange={(e) => this.trackerOnChange(e)}
                                      checked={this.state.tracker}
                                    />
                                    <label
                                      for="id_client"
                                      className="text-light-grey"
                                    >
                                      Tracker
                                    </label>
                                  </div>
                                </div>
                              )}

                            <div className="row" style={{ margin: "auto" }}>
                                {this.state.contractDoesNotExists&&this.state.billable && (
                                  <Fragment>
                                    {this.props.projectObj &&
                                      this.props.projectObj.project &&
                                      this.props.projectObj.project
                                        .billing_type === "hourly" &&
                                      this.props.projectObj.project &&
                                      this.props.projectObj.project
                                        .project_type === "client_project" ? (
                                        <div className="col-sm-4 form-group">
                                          <label
                                            className="text-light-grey"
                                            for="id_end_date"
                                          >
                                            Hourly Rate
                                        </label>
                                          <input
                                            type="text"
                                            name="hourly_rate"
                                            className="fullwidth input-control"
                                            label="Hourly Rate"
                                            placeholder="Hourly Rate"
                                            onChange={(e) =>
                                              this.weeklyHourlyonChange(e)
                                            }
                                            value={this.state.hourly_rate}
                                          />
                                          <span className="text-danger">
                                            {this.state.errors &&
                                              this.state.errors.hourly_rate}
                                          </span>
                                        </div>
                                      ) : null}
                                    {this.props.projectObj &&
                                      this.props.projectObj.project &&
                                      this.props.projectObj.project
                                        .billing_type === "hourly" &&
                                      this.props.projectObj.project &&
                                      this.props.projectObj.project
                                        .project_type === "client_project" ? (
                                        <div className="col-sm-4 form-group">
                                          <label
                                            className="text-light-grey"
                                            for="id_end_date"
                                          >
                                            Weekly Limit
                                        </label>
                                          <input
                                            type="text"
                                            name="weekly_limit"
                                            className="fullwidth input-control"
                                            label="Weekly Limit"
                                            placeholder="Weekly Limit"
                                            onChange={(e) =>
                                              this.weeklyHourlyonChange(e)
                                            }
                                            value={this.state.weekly_limit}
                                          />
                                          <span className="text-danger">
                                            {this.state.errors &&
                                              this.state.errors.weekly_limit}
                                          </span>
                                        </div>
                                      ) : null}

                                    {this.props.projectObj &&
                                      this.props.projectObj.project &&
                                      this.props.projectObj.project
                                        .billing_type === "hourly" &&
                                      this.props.projectObj.project &&
                                      this.props.projectObj.project
                                        .project_type === "client_project" ? (
                                        <Fragment>
                                          <div className="col-sm-4 form-group">
                                            <label className="text-light-grey">
                                              Contract Title
                                          </label>
                                            <input
                                              type="text"
                                              name="contract_title"
                                              className="fullwidth input-control"
                                              label="Contract Title"
                                              value={this.state.contract_title}
                                              placeholder="Contract Title"
                                              onChange={(e) =>
                                                this.handleChange(e)
                                              }
                                            />

                                            <span className="text-danger">
                                              {this.state.errors.contract_title}
                                            </span>
                                          </div>
                                          <div className="col-sm-6 form-group">
                                            <label className="text-light-grey">
                                              Contract Attachment
                                          </label>
                                            <input
                                              type="file"
                                              className="fullwidth input-control"
                                              name="contractAttachment"
                                              onChange={(event) =>
                                                this.upLoadAttachment(event)
                                              }
                                              className="form-control"
                                              id="id_contract_formset_set-0-contract"
                                            />
                                            <span className="text-danger">
                                              {
                                                this.state.errors
                                                  .contractAttachment
                                              }
                                            </span>
                                          </div>

                                          <div className="col-sm-6 form-group">
                                            <label className="text-light-grey">
                                              Term Attachment
                                          </label>

                                            <input
                                              type="file"
                                              className="fullwidth input-control"
                                              name="termAttachment"
                                              onChange={(event) =>
                                                this.upLoadAttachment(event)
                                              }
                                              className="form-control"
                                              id="id_contract_formset_set-0-contract"
                                            />
                                            <span className="text-danger">
                                              {this.state.errors.termAttachment}
                                            </span>
                                          </div>
                                        </Fragment>
                                      ) : null}
                                  </Fragment>
                                )}
                              </div>
                            </div>
                          </Fragment>
                        </div>
                      </Fragment>
                      <div className="modal-footer" style={{ border: "none" }}>
                        {/* <input onClick={this.editResource} className="btn btn-success" value="Save" /> */}
                        <button
                          onClick={(e) => this.editResource(e)}
                          className="btn btn-success"
                        >
                          Save
                        </button>
                      </div>
                    </form>
                  </div>
                ) : null}

                {this.props.editPart === "team-add" ? (
                  <div className="modal-body">
                    
                      <Fragment>
                        <div
                          className="sub-header"
                          style={{
                            borderBottom: "none",
                            backgroundColor: "none",
                          }}
                        >
                          {/* {this.state.userList.map((obj, index) => {
                        return ( */}
                          <Fragment>
                            <div
                              className="row "
                            // style={{ margin: "auto", width: "520px", marginBottom: "20px" }}
                            >
                              <div
                                className=" col-sm-6 "
                                style={{ textAlign: "justify" }}
                              >
                                <label
                                  for="id_client"
                                  className="text-light-grey"
                                >
                                  Select Add Resource
                                </label>
                                <Select
                                  // name={`resource_${index}`}
                                  style={{ backgroundColor: "white" }}
                                  options={this.state.userList}
                                  value={
                                    this.state.userList &&
                                    this.state.userList.filter(
                                      (i) =>
                                        (this.state.resource_assigned &&
                                          this.state.resource_assigned.slug) ===
                                        i.slug
                                    )
                                  }
                                  onChange={(event) =>
                                    this.handleresourceChange(event)
                                  }
                                />
                                <span className="text-danger">
                                  {this.state.errors.resourceError}
                                </span>
                              </div>
                              <div
                                className="col-sm-6 "
                                style={{ margin: "auto", paddingTop: "25px",paddingLeft:"40px" }}
                              >
                                {this.state.designation_name}
                                {/* <button className="btn btn-outline-primary delete-row" style={{ width: "130px",marginTop: "2rem"}} 
                                    // onClick={() => this.props.removeResource(index)}
                                     href="javascript:void(0)" ><i class=" icon-delete"></i> Remove</button> */}
                              </div>
                            </div>

                            <div className="row" style={{ marginTop: "20px" }}>
                              <div className="col-sm-9 form-group ">
                                {/* <span style={{ marginRight: "10px" }}>{obj.label} {index}</span> */}
                                <input
                                  type="radio"
                                  className="individual-button"
                                  checked={this.state.billable}
                                  // name={`projectinfo_${index}`}
                                  onClick={this.handlebillable}
                                  value="Internal"
                                />
                                <label for="internal" className="individual">
                                  Billable
                                </label>
                                <input
                                  type="radio"
                                  // name={`projectinfo_${index}`}
                                  checked={this.state.non_billable}
                                  className="individual-button"
                                  onClick={this.handlenonbillable}
                                  value="Client"
                                />
                                <label for="client" className="individual">
                                  Non Billable
                                </label>
                              </div>

                              <span className="text-danger">
                                {this.state.errors.billing_type_error}
                              </span>

                              {this.state.non_billable && (
                                <div
                                  className="row"
                                  style={{
                                    width: "283px",
                                    paddingLeft: "15px",
                                    width: "200px",
                                    marginTop: "20px",
                                  }}
                                >
                                  <div
                                    className="col-sm-6 form-group"
                                    style={{ marginTop: "auto" }}
                                  >
                                    <input
                                      type="checkbox"
                                      onChange={(e) => this.trackerOnChange(e)}
                                      checked={this.state.tracker}
                                    />
                                    <label
                                      for="id_client"
                                      className="text-light-grey"
                                    >
                                      Tracker
                                    </label>
                                  </div>
                                </div>
                              )}

                              {/* neeeeeeeeeeeeeeeeeeeeeeeeeeeeeee */}

                              <div className="row" style={{ margin: "auto" }}>
                                {this.state.billable && (
                                  <Fragment>
                                    {this.props.projectObj &&
                                      this.props.projectObj.project &&
                                      this.props.projectObj.project
                                        .billing_type === "hourly" &&
                                      this.props.projectObj.project &&
                                      this.props.projectObj.project
                                        .project_type === "client_project" ? (
                                        <div className="col-sm-4 form-group">
                                          <label
                                            className="text-light-grey"
                                            for="id_end_date"
                                          >
                                            Hourly Rate
                                        </label>
                                          <input
                                            type="text"
                                            name="hourly_rate"
                                            className="fullwidth input-control"
                                            label="Hourly Rate"
                                            placeholder="Hourly Rate"
                                            onChange={(e) =>
                                              this.weeklyHourlyonChange(e)
                                            }
                                            value={this.state.hourly_rate}
                                          />
                                          <span className="text-danger">
                                            {this.state.errors &&
                                              this.state.errors.hourly_rate}
                                          </span>
                                        </div>
                                      ) : null}
                                    {this.props.projectObj &&
                                      this.props.projectObj.project &&
                                      this.props.projectObj.project
                                        .billing_type === "hourly" &&
                                      this.props.projectObj.project &&
                                      this.props.projectObj.project
                                        .project_type === "client_project" ? (
                                        <div className="col-sm-4 form-group">
                                          <label
                                            className="text-light-grey"
                                            for="id_end_date"
                                          >
                                            Weekly Limit
                                        </label>
                                          <input
                                            type="text"
                                            name="weekly_limit"
                                            className="fullwidth input-control"
                                            label="Weekly Limit"
                                            placeholder="Weekly Limit"
                                            onChange={(e) =>
                                              this.weeklyHourlyonChange(e)
                                            }
                                            value={this.state.weekly_limit}
                                          />
                                          <span className="text-danger">
                                            {this.state.errors &&
                                              this.state.errors.weekly_limit}
                                          </span>
                                        </div>
                                      ) : null}

                                    {this.props.projectObj &&
                                      this.props.projectObj.project &&
                                      this.props.projectObj.project
                                        .billing_type === "hourly" &&
                                      this.props.projectObj.project &&
                                      this.props.projectObj.project
                                        .project_type === "client_project" ? (
                                        <Fragment>
                                          <div className="col-sm-4 form-group">
                                            <label className="text-light-grey">
                                              Contract Title
                                          </label>
                                            <input
                                              type="text"
                                              name="contract_title"
                                              className="fullwidth input-control"
                                              label="Contract Title"
                                              value={this.state.contract_title}
                                              placeholder="Contract Title"
                                              onChange={(e) =>
                                                this.handleChange(e)
                                              }
                                            />

                                            <span className="text-danger">
                                              {this.state.errors.contract_title}
                                            </span>
                                          </div>
                                          <div className="col-sm-6 form-group">
                                            <label className="text-light-grey">
                                              Contract Attachment
                                          </label>
                                            <input
                                              type="file"
                                              className="fullwidth input-control"
                                              name="contractAttachment"
                                              onChange={(event) =>
                                                this.upLoadAttachment(event)
                                              }
                                              className="form-control"
                                              id="id_contract_formset_set-0-contract"
                                            />
                                            <span className="text-danger">
                                              {
                                                this.state.errors
                                                  .contractAttachment
                                              }
                                            </span>
                                          </div>

                                          <div className="col-sm-6 form-group">
                                            <label className="text-light-grey">
                                              Term Attachment
                                          </label>

                                            <input
                                              type="file"
                                              className="fullwidth input-control"
                                              name="termAttachment"
                                              onChange={(event) =>
                                                this.upLoadAttachment(event)
                                              }
                                              className="form-control"
                                              id="id_contract_formset_set-0-contract"
                                            />
                                            <span className="text-danger">
                                              {this.state.errors.termAttachment}
                                            </span>
                                          </div>
                                        </Fragment>
                                      ) : null}
                                  </Fragment>
                                )}

                                {/* {values.select_billable[index].select_nonbillable &&
                                    <div className="row" style={{ margin: "auto", width: "520px", marginTop: "20px" }}>



                                      <div className="col-sm-2  form-group" >

                                        <span style={{ margin: "auto", color: "#9C9C9C" }}>{obj.designation_name} </span>
                                      </div>
                                      <div className="col-sm-4 form-group" style={{ marginTop: "auto" }}>


                                        <input type="checkbox" onChange={() => this.props.trackerOnChange(index)} checked={obj.tracker} />
                                        <label for="id_client" className="text-light-grey">
                                          Tracker
                      </label>
                                      </div>

                                    </div>} */}
                              </div>
                           
                           
                           
                           
                           
                           
                           
                            </div>
                          </Fragment>

                          {/* //     )
                        // })} */}
                        </div>
                      </Fragment>
                      <div className="modal-footer" style={{ border: "none" }}>
                        <button
                          onClick={(e) => this.addResource(e)}
                          className="btn btn-success"
                        >
                          Save
                        </button>
                        {/* <input onClick={this.addResource} className="btn btn-success" value="Save" /> */}
                      </div>
                    
                  </div>
                ) : null}

                {this.props.editPart === "task" ? (
                  <div className="modal-body">
                    <form
                      className="row"
                      id="create-project-form"
                      onSubmit={handleSubmit(this.submit)}
                      enctype="multipart/form-data"
                    >
                      <div className="col-sm-6 form-group">
                        <label
                          for="id_project_name"
                          className="text-light-grey"
                        >
                          Task name<span className="text-danger">*</span>
                        </label>
                        <Field
                          type="text"
                          name="task_name"
                          component={renderField}
                          label="Task Name"
                          validate={required}
                          onChange={this.clearExistedError}
                          existed={this.state.existed_error}
                        />
                      </div>
                      <div className="col-sm-6 form-group">
                        <label
                          className="text-light-grey"
                          for="id_estimated_time"
                        >
                          Estimated Hour
                        </label>
                        <Field
                          type="text"
                          name="estimated_time"
                          component={renderField}
                          validate={positiveInteger}
                          label="Estimated Hours"
                        />
                      </div>
                      <div className="col-sm-6 form-group">
                        <label for="id_client" className="text-light-grey">
                          Due date
                        </label>
                        <span className="text-danger">*</span>
                        <Field
                          type="text"
                          name="due_date"
                          component={renderDateField}
                          label="Due date"
                          validate={required}
                          existed={this.state.existed_error}
                        />
                      </div>
                      <div className="col-sm-6 form-group">
                        <label for="id_client" className="text-light-grey">
                          Assigned To
                        </label>
                        <span className="text-danger">*</span>
                        <Select
                          options={this.state.userList}
                          onChange={this.assignedValidation}
                        />
                        <small className="text-danger">
                          {this.state.assigned_error}
                        </small>
                        {/* <Field userList={userList} value="this.state.accountno" label='Assigned' onChange={this.assigned} 
                     validate={required} component={RenderSelectInput} />
                   */}
                      </div>
                      <div className="col-sm-12 form-group">
                        <label
                          for="id_project_description"
                          className="text-light-grey"
                        >
                          Task description
                        </label>
                        <ReactQuill onChange={(e) => this.taskdescription(e)}>
                          <div className="my-editing-area" />
                        </ReactQuill>

                        {/* <Field type="text"
                        name="project_description"
                        cols="40" rows="10"
                        component={renderTextField}
                        validate={required}
                        className="input-control input-control"
                        label="Task Description"
                        placeholder="Project Description" /> */}
                      </div>

                      {/* <div className="col-sm-6 form-group">
                     <label className="text-light-grey" for="id_estimated_time">Hours Spent</label><span className="text-danger">*</span>
                           
                     <Field type="text"
                        name="estimated_time"
                        component={renderField}
                        label="Hours Spent"

                    /> 
                           <span className="text-danger">{this.state.startingDateError}</span>
                           
                  </div> */}
                      <div className="modal-footer">
                        <input
                          type="submit"
                          className="btn btn-success"
                          value="Save"
                        />
                      </div>
                    </form>
                  </div>
                ) : null}

                {this.props.editPart === "view project" &&
                  this.state.editPart !== "contractAttachment" &&
                  this.state.editPart !== "termAttachment" ? (
                    <div
                      className="modal-body table-scroll"
                      style={{ height: "300px", overflowY: "auto" }}
                    >
                      <div className="row ">
                        <table
                          className="table"
                          style={{ color: "#9C9C9C", textAlign: "center" }}
                        >
                          <thead className="table-head">
                            <tr>
                              <th style={{ width: "100px" }}>Contract Id</th>
                              {projectObj &&
                                projectObj.project &&
                                projectObj.project.billing_type != "fixed" && (
                                  <Fragment>
                                    <th style={{ width: "150px" }}>Name</th>
                                    <th style={{ width: "150px" }}>Role</th>
                                    <th style={{ width: "121px" }}>
                                      Hourly Rate
                                  </th>
                                    <th style={{ width: "150px" }}>
                                      Weekly Limit
                                  </th>
                                  </Fragment>
                                )}
                              <th>Contract</th>
                              <th>Terms</th>
                              <th>Status</th>
                              {/* <th style={{color:"#9C9C9C"}}>Details</th> */}
                            </tr>
                          </thead>
                          <tbody>
                            {projectObj &&
                              projectObj.project &&
                              projectObj.project.billing_type === "fixed" ? (
                                <Fragment>
                                  {/* {fixedFinancialInfo} */}
                                  <tr>
                                    <td>
                                      {fixedFinancialInfo &&
                                        fixedFinancialInfo.contract_number}
                                    </td>
                                    {fixedFinancialInfo &&
                                      fixedFinancialInfo.contract ? (
                                        <td>
                                          {" "}
                                          <button
                                            type="button"
                                            onClick={() =>
                                              this.props.downloadContract(
                                                fixedFinancialInfo.contract_id,
                                                fixedFinancialInfo.contract
                                              )
                                            }
                                            style={{
                                              width: "75px",
                                              fontSize: "12px",
                                            }}
                                            className="btn btn-outline-primary add-row"
                                          >
                                            Download{" "}
                                          </button>
                                        </td>
                                      ) : (
                                        <td>
                                          <button
                                            type="button"
                                            onClick={() =>
                                              this.addTaskModal(
                                                "contractAttachment",
                                                fixedFinancialInfo.contract_id
                                              )
                                            }
                                            style={{
                                              width: "75px",
                                              fontSize: "12px",
                                            }}
                                            className="btn btn-outline-primary add-row"
                                          >
                                            Upload
                                    </button>
                                        </td>
                                      )}

                                    {fixedFinancialInfo &&
                                      fixedFinancialInfo.termAttachment ? (
                                        <td>
                                          {" "}
                                          <button
                                            type="button"
                                            onClick={() =>
                                              this.props.downloadTermAttachment(
                                                fixedFinancialInfo.term_attachment_id,
                                                fixedFinancialInfo.termAttachment
                                              )
                                            }
                                            style={{
                                              width: "75px",
                                              fontSize: "12px",
                                            }}
                                            className="btn btn-outline-primary add-row"
                                          >
                                            Download
                                    </button>
                                        </td>
                                      ) : (
                                        <td>
                                          <button
                                            type="button"
                                            onClick={() =>
                                              this.addTaskModal(
                                                "termAttachment",
                                                fixedFinancialInfo
                                              )
                                            }
                                            style={{
                                              width: "75px",
                                              fontSize: "12px",
                                            }}
                                            className="btn btn-outline-primary add-row"
                                          >
                                            Upload{" "}
                                          </button>
                                        </td>
                                      )}

                                    <td>
                                      {fixedFinancialInfo &&
                                        fixedFinancialInfo.contract_status &&
                                        fixedFinancialInfo.contract_status
                                          .charAt(0)
                                          .toUpperCase() +
                                        fixedFinancialInfo.contract_status.slice(
                                          1
                                        )}
                                    </td>
                                  </tr>
                                </Fragment>
                              ) : (
                                <Fragment>
                                  {combined &&
                                    combined.length > 0 &&
                                    combined.map((obj) => (
                                      <tr>
                                        <td>{obj && obj.contract_number}</td>
                                        {projectObj &&
                                          projectObj.project &&
                                          projectObj.project.billing_type !=
                                          "fixed" && (
                                            <Fragment>
                                              <td style={{ width: "150px" }}>
                                                {obj &&
                                                  obj.resource_assigned &&
                                                  obj.resource_assigned
                                                    .first_name}{" "}
                                                {obj &&
                                                  obj.resource_assigned &&
                                                  obj.resource_assigned.last_name}
                                              </td>
                                              <td style={{ width: "183px" }}>
                                                {obj &&
                                                  obj.resource_assigned &&
                                                  obj.resource_assigned
                                                    .designation &&
                                                  obj.resource_assigned.designation
                                                    .designation_name}
                                              </td>
                                              <td style={{ width: "121px" }}>
                                                {obj && obj.hourly_rate}
                                              </td>
                                              <td style={{ width: "150px" }}>
                                                {obj && obj.weekly_limit}
                                              </td>
                                            </Fragment>
                                          )}
                                        {/* <td> <button type="button" style={{width:"75px",fontSize:"12px"}} className="btn btn-outline-primary add-row">Download </button></td> */}
                                        {obj.contract ? (
                                          <td>
                                            {" "}
                                            <button
                                              type="button"
                                              onClick={() =>
                                                this.props.downloadContract(
                                                  obj.contract_id,
                                                  obj.contract
                                                )
                                              }
                                              style={{
                                                width: "75px",
                                                fontSize: "12px",
                                              }}
                                              className="btn btn-outline-primary add-row"
                                            >
                                              Download{" "}
                                            </button>
                                          </td>
                                        ) : (
                                            <td>
                                              <button
                                                type="button"
                                                onClick={() =>
                                                  this.addTaskModal(
                                                    "contractAttachment",
                                                    obj.contract_id
                                                  )
                                                }
                                                style={{
                                                  width: "75px",
                                                  fontSize: "12px",
                                                }}
                                                className="btn btn-outline-primary add-row"
                                              >
                                                Upload{" "}
                                              </button>
                                            </td>
                                          )}
                                        {/* <td> <button type="button" style={{width:"75px",fontSize:"12px"}} className="btn btn-outline-primary add-row">Download </button></td> */}
                                        {/* terms */}
                                        {obj && obj.termAttachment ? (
                                          <td>
                                            {" "}
                                            <button
                                              type="button"
                                              onClick={() =>
                                                this.props.downloadTermAttachment(
                                                  obj.term_attachment_id,
                                                  obj.termAttachment
                                                )
                                              }
                                              style={{
                                                width: "75px",
                                                fontSize: "12px",
                                              }}
                                              className="btn btn-outline-primary add-row"
                                            >
                                              Download
                                        </button>
                                          </td>
                                        ) : (
                                            <td>
                                              <button
                                                type="button"
                                                onClick={() =>
                                                  this.addTaskModal(
                                                    "termAttachment",
                                                    obj
                                                  )
                                                }
                                                style={{
                                                  width: "75px",
                                                  fontSize: "12px",
                                                }}
                                                className="btn btn-outline-primary add-row"
                                              >
                                                Upload{" "}
                                              </button>
                                            </td>
                                          )}
                                        <td>
                                          {obj &&
                                            obj.contract_status &&
                                            obj.contract_status
                                              .charAt(0)
                                              .toUpperCase() +
                                            obj.contract_status.slice(1)}
                                        </td>
                                      </tr>
                                    ))}
                                </Fragment>
                              )}
                          </tbody>
                        </table>
                        <br></br>
                        <span
                          style={{
                            textAlign: "center",
                            margin: "auto",
                            width: "800px",
                            borderBottom: "1px solid #e9ecef",
                            borderTop: "1px solid #e9ecef",
                            padding: "8px",
                          }}
                        >
                          {projectObj &&
                            projectObj.project &&
                            projectObj.project.project_name}
                        </span>
                        <div
                          className="row"
                          style={{
                            textAlign: "center",
                            margin: "auto",
                            width: "800px",
                            borderBottom: "1px solid #e9ecef",
                            color: "#9C9C9C",
                            display: "inline-flex",
                            padding: "8px",
                          }}
                        >
                          <h6 className="col-sm-6">
                            Client&nbsp;:
                          <span style={{ fontWeight: "100" }}>
                              {projectObj &&
                                projectObj.project &&
                                projectObj.project.client}
                            </span>
                          </h6>
                          {/* <h6 className="col-sm-6">
                            BD&nbsp;:<span style={{ fontWeight: "100" }}> </span>
                          </h6> */}
                        </div>
                      </div>
                      <div
                        className="row"
                        style={{
                          borderBottom: "1px solid #e9ecef",
                          color: "#9C9C9C",
                          padding: "22px 1px",
                        }}
                      >
                        <div className="col-sm-5">
                          <li className="li-content">
                            Start Date&nbsp;:&nbsp;
                          <span style={{ fontWeight: "100" }}>
                              {projectObj &&
                                projectObj.project &&
                                projectObj.project.start_date}
                            </span>
                          </li>
                          {/* <li className="li-content">
                            Total Allowed&nbsp;:&nbsp;
                          <span style={{ fontWeight: "100" }}>120</span>
                          </li> */}
                          <li className="li-content">
                            Total Billed&nbsp;:&nbsp;
                              <span style={{ fontWeight: "100" }}>{this.state.totalBilled}</span>
                          </li>

                          <li className="li-content">
                            Contract Active Date&nbsp;:&nbsp;
                          <span style={{ fontWeight: "100" }}>
                              {projectObj &&
                                projectObj.project &&
                                projectObj.project.billing_type === "hourly" &&
                                combined &&
                                combined.length > 0 &&
                                combined[0].created_at}
                              {projectObj &&
                                projectObj.project &&
                                projectObj.project.billing_type === "fixed" &&
                                fixedFinancialInfo &&
                                fixedFinancialInfo.created_at}
                            </span>
                          </li>
                          <li className="li-content">
                            Contract Paused Date&nbsp;:&nbsp;
                          <span style={{ fontWeight: "100" }}>
                              {projectObj &&
                                projectObj.project &&
                                projectObj.project.billing_type === "hourly" &&
                                combined &&
                                combined.length > 0 &&
                                combined[0].updated_at}
                              {projectObj &&
                                projectObj.project &&
                                projectObj.project.billing_type === "fixed" &&
                                fixedFinancialInfo &&
                                fixedFinancialInfo.updated_at}
                            </span>
                          </li>
                        </div>

                        <div className="col-sm-4">
                          <li className="li-content">
                            Estimated End Date&nbsp;:&nbsp;
                          <span style={{ fontWeight: "100" }}>
                              {projectObj &&
                                projectObj.project &&
                                projectObj.project.end_date}
                            </span>
                          </li>
                          <li className="li-content">
                            Total Logged&nbsp;:&nbsp; 
                          <span style={{ fontWeight: "100" }}>{total_hours?total_hours[total_hours.length-1].toFixed(2):null}</span>
                          </li>
                          {projectObj &&
                                projectObj.project &&
                                projectObj.project.billing_type === "fixed" &&
                          <li className="li-content">
                            Estimated Hours&nbsp;:&nbsp; {projectObj.project.hourly_rate_fixed}
                          <span style={{ fontWeight: "100" }}></span>
                          </li>}
                          {/* <li className="li-content">
                            Total Paid&nbsp;:&nbsp;
                          <span style={{ fontWeight: "100" }}>96</span>
                          </li> */}
                        </div>
                        <div className="col-sm-3">
                          <li className="li-content">
                            End Date&nbsp;:&nbsp;
                          <span style={{ fontWeight: "100" }}>NA</span>
                          </li>
                          <li className="li-content">
                            Total Billable&nbsp;:&nbsp;
                          <span style={{ fontWeight: "100" }}>{total_billablehours?total_billablehours[total_billablehours.length-1].toFixed(2):null}</span>
                          </li>
                          
                          {/* <li className="li-content">
                            Pending&nbsp;:&nbsp;
                          <span style={{ fontWeight: "100" }}>30</span>
                          </li> */}
                        </div>
                      </div>

                      {this.props.projectData?.projectObj?.project?.billing_type==='hourly'
                      &&<div className="row ">
                        <table
                          className="table"
                          style={{ color: "#9C9C9C", textAlign: "center" }}
                        >{this.state.weeklyworksheets?.length>0?
                          <thead className="table-head">
                            <tr>
                              
                              <th>Duration</th>
                              <th>Weekly Limit</th>
                              <th>Logged</th>
                              <th>Billable</th>
                              <th>Billed</th>
                              <th>Payment Status</th>
                              <th></th>
                            </tr>
                          </thead>:<div className="text-danger">No weekly worksheets found </div>}
                          <tbody>
                                
                            {this.state.weeklyworksheets?.length>0&&
                            this.state.weeklyworksheets?.map((i,index)=>
                            <tr>
                              
                              <td>{Moment(i.week_start).format("ll")} - {Moment(i.week_end).format("ll")}</td>
                              
                            <td>{this.props.projectData?.projectObj?.financial_info?.
                            resources?.filter(con=>con?.resource_assigned?.slug===i.employee?.slug)[0]?.weekly_limit }</td>
                              <td>{i.worksheets?.length>0&&this.calculateLoggedhours(i.worksheets,index)} </td>
                              <td>{i.worksheets?.length>0&&this.calculateBillablehours(i.worksheets)}</td>
                            <td>{i.billed?i.billed:'0'}</td>
                            <td>{this.capitalizeFirstLetter( i.payment_status.replace(/\_/g, " "))}</td>
                              <td><i
                              className="btn btn-link-secondary"
                              onClick={() =>
                                this.addTaskModal(
                                  "editprojectdetails",i.id
                                )
                              }
                              className="icon-pencil"
                              style={{ fontSize: "16px" }}
                            ></i></td>
                            </tr>)}
                            
                          </tbody>
                        </table>
                      </div>}
                      {this.props.projectData?.projectObj?.project?.billing_type==="fixed"
                      &&<div className="row ">
                      <table
                          className="table"
                          style={{ color: "#9C9C9C", textAlign: "center" }}
                        >
                          {this.props.projectData?.weeklyworksheets?.length>0?
                    <thead className="table-head">
                      <tr>
                        <th>Duration</th>
                      
                        <th>Employee Name</th>
                        <th class="middle">Logged</th>
                        <th class="middle">Billable</th>
                        <th class="middle">Billed</th>
                        <th class="middle">Payment Status</th>
                        <th></th>
                      </tr>
                    </thead>:<div className="text-danger">No weekly worksheets found </div>}

                    <tbody>
                      {this.props.projectData?.weeklyworksheets?.length>0&&
                      this.props.projectData?.weeklyworksheets?.map((i,index)=>
                      <tr>
                        <td>{Moment(i.week_start).format("ll")} - {Moment(i.week_end).format("ll")}</td>
                        
                      <td> {i?.employee?.first_name} {i?.employee?.last_name}</td>
                        <td>{i.worksheets?.length>0&&this.calculateLoggedhours(i.worksheets,index)}</td>
                        <td>{i.worksheets?.length>0&&this.calculateBillablehours(i.worksheets)}</td>
                        <td>{i.billed?i.billed:'0'}</td>
                        <td>{this.capitalizeFirstLetter( i.payment_status.replace(/\_/g, " "))}</td>
                        <td><i
                              className="btn btn-link-secondary"
                              onClick={() =>
                                this.addTaskModal(
                                  "editprojectdetails",i.id
                                )
                              }
                              className="icon-pencil"
                              style={{ fontSize: "16px" }}
                            ></i></td>
                        
                      </tr>)}
                      
                      
                    </tbody>
                  </table>
                      </div>}

                      {(this.props.editPart!='view project')&&<Fragment>
                        <div className="row">
                          <h6 className="table-head" style={{ padding: "3px" }}>
                            Comments 
                        </h6>
                          {comments != null && comments.length > 0 ? (
                            <Fragment>
                              {comments.map((com) => (
                                <Fragment>
                                  <div
                                    className="table-head"
                                    style={{
                                      border: "1px solid #e9ecef",
                                      padding: "3px",
                                      margin: "6px",
                                    }}
                                  >
                                    <span style={{ fontWeight: "600" }}>
                                      {com.commented_by}&nbsp;:&nbsp;
                                  </span>
                                    {com.detail_text}{" "}
                                  </div>
                                </Fragment>
                              ))}
                            </Fragment>
                          ) : (
                              "No comments"
                            )}
                        </div>
                      </Fragment>}

                    </div>
                  ) : null}

                {this.props.editPart === "view" &&
                  this.state.editPart !== "edittask" &&
                  this.state.editPart !== "subtask" ? (
                    <div className="modal-body">
                      <form
                        className="row"
                        id="create-project-form"
                        onSubmit={handleSubmit(this.submit)}
                        enctype="multipart/form-data"
                      >
                        <div className="col-sm-6 form-group">
                          <label for="id_project_name">
                            Task name &nbsp;:&nbsp;{" "}
                            <span className="text-light-grey">task 1</span>
                          </label>
                          {/* <Field type="text"
                        name="project_name"
                        component={renderField}
                        label="Task Name"
                        validate={required}
                        onChange={this.clearExistedError}
                        existed={this.state.existed_error}
                        /> */}
                        </div>
                        <div className="col-sm-6 form-group">
                          <label for="id_estimated_time">
                            Estimated Hour(hr)&nbsp;:&nbsp;
                          <span className="text-light-grey">200</span>
                          </label>
                          {/* <Field type="text"
                        name="estimated_time"
                        component={renderField}
                        label="Estimated Hours"

                    /> */}
                        </div>

                        <div className="col-sm-6 form-group">
                          <label for="id_client">
                            Created By&nbsp;:&nbsp;
                          <span className="text-light-grey">Name</span>
                          </label>
                          {/* <Field type="text"
                        name="project_name"
                        component={renderField}
                        label="Name"
                        validate={required}
                        onChange={this.clearExistedError}
                        existed={this.state.existed_error}
                        /> */}
                        </div>
                        <div className="col-sm-6 form-group">
                          <label for="id_client">
                            Assigned To&nbsp;:&nbsp;
                          <span className="text-light-grey">Name</span>
                          </label>
                          {/* <Select options={userList}/> */}
                        </div>
                        <div className="col-sm-6 form-group">
                          <label for="id_client">
                            Status&nbsp;:&nbsp;
                          <span className="text-light-grey">Created</span>
                          </label>
                          {/* <Select options={userList}/> */}
                        </div>
                        <div className="col-sm-6 form-group">
                          <label for="id_client">
                            Due Date&nbsp;:&nbsp;
                          <span className="text-light-grey">20/06/2020</span>
                          </label>
                          {/* <Select options={userList}/> */}
                        </div>
                        <div className="col-sm-6 form-group">
                          <label>Add Sub Task</label>
                          <p className="text-light-grey">
                            No sub tasks available
                        </p>
                        </div>
                        <div className="col-sm-6 form-group">
                          <button
                            type="button"
                            onClick={() => this.addTaskModal("subtask")}
                            className="btn btn-outline-primary add-row"
                          >
                            <i className="icon-plus"></i> Add sub Task
                        </button>
                        </div>
                        <div className="col-sm-12 form-group">
                          <label
                            for="id_project_description"
                            className="text-light-grey"
                          >
                            Comments<span className="text-danger">*</span>
                          </label>
                          <ReactQuill>
                            <div className="my-editing-area" />
                          </ReactQuill>
                          {/* <Field type="text"
                        name="project_description"
                        cols="40" rows="10"
                        component={renderTextField}
                        validate={required}
                        className="input-control input-control"
                        label="Task Description"
                        placeholder="Project Description" /> */}
                        </div>
                        <div className="modal-footer">
                          <input
                            type="submit"
                            className="btn btn-success"
                            value="Save"
                          />
                        </div>
                      </form>
                    </div>
                  ) : null}

                {this.props.editPart === "StartEndTimer" ?
                  <div className="modal-body">
                    <form
                      className="row"
                      id="create-project-form"
                      enctype="multipart/form-data"
                    >
                      <div className="tab-content col-sm-9" id="myTaskContent">
                        {this.props.index === 'start' ? <div className="col-sm-6 form-group">
                          <label
                            className="text-light-grey"
                            for="id_estimated_time"
                          >
                            Select Task
                    </label> <span className="text-danger">*</span>
                          <Select
                            value={taskLog.filter(i => i.value === this.state.timeTask?.value)}
                            onChange={(e) => this.setState(prevstate => ({ errors: { ...prevstate.errors, timeTask: '' }, timeTask: e }))}
                            options={taskLog} />
                          <span className="text-danger">{this.state.errors?.timeTask}</span>
                        </div> :
                          <h6 className="text-center">Are you sure you want to End Timer</h6>
                        }
                      </div>
                      <div className="modal-footer" style={{ border: "none" }}>
                        <button
                          onClick={(e) => this.props.startTimer(e, this.props.index, this.state.timeTask?.value)}
                          disabled={this.props.index === 'start' && !this.state.timeTask?.value}
                          className={this.props.index === 'start' ? "btn btn-success" : "btn btn-danger"}
                        >{this.props.formatString(this.props.index)}
                        </button>
                      </div>
                    </form>
                  </div>
                  : null}

              </div>
            </div>
          </div>
        </div>

        <div className="modal-backdrop show"></div>
        {this.state.editModal && (
          <AddSubTaskModal
            {...this.props}
            validateContract={this.validateContract}
            upLoadAttachment={this.upLoadAttachment}
            contractAttachmentError={this.state.errors.contractAttachment}
            termAttachmentError={this.state.errors.termAttachment}
            contractAttachment={this.state.contractAttachment}
            termAttachment={this.state.termAttachment}
            closeModal={this.closeEditModal}
            index={this.state.index}
            editPart={this.state.editPart}
            calculateBillablehours={this.calculateBillablehours}
          />
        )}
        </Fragment>}
      </Fragment>
    );
  }
}
AddTask = reduxForm({
  form: "addTaskForm",
})(AddTask);
export default AddTask;
