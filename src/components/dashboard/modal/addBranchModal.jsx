import React, {Component,Fragment} from 'react'
import { Field, reduxForm } from 'redux-form'
import { Redirect } from 'react-router-dom'
import {countryReorder} from '../../../countryReorder'
import {alphabetOnly} from '../../../constants/validations'


const MODAL_OPEN_CLASS = "modal-open";

const required = value => {
  return (value || typeof value === 'number' ? undefined : ' is Required')
}

const renderField = ({
  input,
  label,
  type,
  existed,
  meta: { touched, error, warning, }
}) => {
  return (
    

    <Fragment>
      <input {...input} placeholder={label} type={type} className={(touched && error) || (existed && existed.existedMail) ? 'input-control input-control border border-danger' : 'input-control input-control'} />
      {touched &&
        ((error && <small className="text-danger">{label}{error}</small>) ||
          (warning && <span>{warning}</span>))}
      {((existed) && (existed.existedMail)) && <small class="text-danger">
        {existed.error.data.user.email[0]}
      </small>}

    </Fragment>

  )
}

const renderFieldSelect = ({
  input,
  label,
  country,
  countryState,
  city,
  className,
  meta: { touched, error, warning, }
}) => {
  return (
    <Fragment>
    <select {...input} className={(touched && error)? className+' border border-danger': className}>

        <option value="">{label}</option>
        {(country && label === "Country") &&
          <Fragment>
            {country.countryList.map((plan, index) => (
              <option key={index} value={plan.id}>{plan.name_ascii}</option>
            ))}</Fragment>
        }
        {(countryState && label === "State") &&
          <Fragment>
            {countryState.stateList.map((plan, index) => (
              <option key={index} value={plan.id}>{plan.name_ascii}</option>
            ))}</Fragment>
        }
        {(city && label === "City") &&
          <Fragment>
            {city.cityList.map((plan, index) => (
              <option key={index} value={plan.id}>{plan.name_ascii}</option>
            ))}</Fragment>
        }
      </select>
      <div>
        {touched &&
          ((error && <small className="text-danger">{label}{error}</small>) ||
            (warning && <span>{warning}</span>))}
      </div>
      {/* {((existed)&&(existed.error.status == 500)) && <small class="text-danger">
          {existed.errors.error}
      </small>} */}

    </Fragment>

  )
}

class AddBranch extends Component{
  
  state = {
    show_error_message: false
  };
  componentDidMount() {
    document.body.classList.add(MODAL_OPEN_CLASS);
    this.props.fetchCountryList();
  }


  componentWillUnmount() {
    document.body.classList.remove(MODAL_OPEN_CLASS);
  }
  
  handleCountryChange = (e) => {
    // making fields null
    this.props.change('state','')  
    this.props.change('city','')

    this.props.fetchStatesList(e.target.value);
    this.props.fetchCityList("");
    
}
handleStateChange = (e) => {
    // making fields null
    this.props.change('city','')

    this.props.fetchCityList(e.target.value);
}

  submit = (values) => {
    this.setState({show_error_message : true})
    values.organization = localStorage.getItem("organization_slug")
    this.props.createBranch(values)
 
  }


  closeError =() =>{
    this.setState({show_error_message : false})
  }

    render(){
          const { handleSubmit} = this.props;

          if(this.props.branchCreatedData && this.props.branchCreatedData.branchName){
          return <Redirect to='/organization/organization/' />
        }
        


        return(
          <Fragment>
      <div id="modal-block">

      

        <div className="modal form-modal show" id="addBranchModal" role="dialog" style={{display: 'block', paddingLeft: '15px'}}>
          <div className="modal-dialog modal-lg">
            <div className="modal-content">
              {/* Modal Header */}
              <div className="modal-header">
                <h4 className="modal-title">Add Branch</h4>
                <button type="button" className="btn btn-close btn-black" onClick={this.props.onCloseModal} data-dismiss="modal"><span aria-hidden="true">×</span></button>
              </div>
              {/* Modal body */}
              <div className="modal-body">
              <form onSubmit={handleSubmit(this.submit)} id="add-branch-form" className="popup-form" noValidate>
              
              { (this.state.show_error_message && this.props.branchCreatedData &&  this.props.branchCreatedData.branchCreateError)&&
              <div className="alert alert-danger alert-dismissible fade show" role="alert">
                  <button type="button" onClick={this.closeError} className="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
                   {this.props.branchCreatedData.branchCreateError.data.non_field_errors[0]}  
                  <br/>
              </div>}
                  
                  <div className="form-group row">
                    <div className="col-md-4">
                      <label className="form-control-label" htmlFor="id_branch_name">
                        <label htmlFor="id_branch_name">Branch Name</label></label>
                      <span className="text-danger">*</span>
                    </div>
                    <div className="col-md-8">
                    <Field type="text" name="branch_name" component={renderField} validate={[required,alphabetOnly]} label="Branch Name" id="id_branch_name" />

                      <div className="error-message text-danger">
                      </div>
                    </div>
                  </div>
                  <div className="form-group row">
                    <div className="col-md-4">
                      <label className="form-control-label" htmlFor="id_country">
                        <label htmlFor="id_country">Country</label> 
                        <span className="text-danger">*</span>
                      </label>
                    </div>
                    <div className="col-md-8 ">
                    <Field
                                  name="country"
                                  type="select"
                                  country={countryReorder(this.props.countryDetails)}
                                  component={renderFieldSelect}
                                  className="input-control select-country"
                                  label="Country"
                                  validate={required}
                                  onChange={this.handleCountryChange} />
                      <div className="error-message text-danger">
                      </div>
                    </div>
                  </div>
                  <div className="form-group row">
                    <div className="col-md-4">
                      <label className="form-control-label" htmlFor="id_state">
                        <label htmlFor="id_state">State</label> 
                        <span className="text-danger">*</span>
                      </label>
                    </div>
                    <div className="col-md-8 ">
                    <Field
                                  name="state"
                                  type="select"
                                  countryState={this.props.stateDetails}
                                  component={renderFieldSelect}
                                  className="input-control select-state"
                                  label="State"
                                  validate={required}
                                  onChange={this.handleStateChange}
                                />
                      <div className="error-message text-danger">
                      </div>
                    </div>
                  </div>
                  <div className="form-group row">
                    <div className="col-md-4">
                      <label className="form-control-label" htmlFor="id_city">
                        <label htmlFor="id_city">City</label> 
                        <span className="text-danger">*</span>
                      </label>
                    </div>
                    <div className="col-md-8 ">
                    <Field
                                  name="city"
                                  type="select"
                                  city={this.props.cityDetails}
                                  component={renderFieldSelect}
                                  className="input-control select-city"
                                  label="City"
                                  validate={required} />
                      <div className="error-message text-danger">
                      </div>
                    </div>
                  </div>
                  <div className="form-group row">
                    <div className="col-md-4">
                      <label className="form-control-label" htmlFor="id_address">
                        <label htmlFor="id_address">Address</label></label>
                    </div>
                    <div className="col-md-8 ">
                      <Field name="address" type="text" placeholder="Address" className="input-control input-control" component="textarea" label=" Address" id="id_address" />

                      <div className="error-message text-danger">
                      </div>
                    </div>
                  </div>
                </form>
              </div>
              {/* Modal footer */}
              <div className="modal-footer">
                <input type="submit" form="add-branch-form" className="btn width btn-success" defaultValue="SAVE" />
                <button type="button" className="btn btn-secondary" onClick={this.props.onCloseModal} data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div></div>
        <div className="modal-backdrop show"></div>
      </Fragment>  )
    }
}
AddBranch = reduxForm({
  form:'addBranchModalform'
})(AddBranch)
export default AddBranch