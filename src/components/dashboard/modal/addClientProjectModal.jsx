import React, { Component, Fragment } from 'react'
import { Field, FieldArray, reduxForm } from 'redux-form'
import { Redirect } from 'react-router-dom';
import { Link } from 'react-router-dom'
import { alphabetOnly } from '../../../constants/validations'
import { countryReorder } from '../../../countryReorder'

import * as Helper from '../common/helper/helper';
import PortalPasswordModal from '../modal/addClientModal';
import LoaderImage from '../../../static/assets/image/loader.gif';


// var loaderStyle={
//   width: '3%',
//   position: 'fixed',
//   top: '50%',
//   left: '57%'
// }

// const required = value => {
//   return (value || typeof value === 'number' ? undefined : ' is Required')
// }
// const mobileValid = value => {
//   var mobilenoregex = /^(?!\b(0)\1+\b)(\+?\d{1,3}[. -]?)?\(?\d{3}\)?([. -]?)\d{3}\3\d{4}$/
//   if (value && value.length >= 1) {
//     return (mobilenoregex.test(value) ? undefined : ' is Invalid')
//   }

//   return undefined
// }
const email = value =>
  value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
    ? ' is Invalid'
    : undefined
const websiteValiadtion = value => {
  if (value) {
    var pattern = new RegExp('^(https?:\\/\\/)' + // protocol
      '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
      '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
      '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
      '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
      '(\\#[-a-z\\d_]*)?$', 'i'); // fragment locator
    return !!pattern.test(value) ? undefined : 'Enter a valid url ( eg:https://websitebuilders.com )'
  }
}
const salutationType = [
  { value: 'mr', text: 'Mr' },
  { value: 'ms', text: 'Ms' },
  { value: 'mrs', text: 'Mrs' }
]
const currency = [{ value: "abc", label: "asdf" }, { value: "wer", label: "uji" }]
const renderField = ({
  input,
  label,
  type,
  existed,
  work_phone_number_error,
  mobile_number_error,
  website_error,
  facebook_error,
  pin_code_error,
  meta: { touched, error, warning, }
}) => {

  return (


    <Fragment>
      <input {...input} placeholder={label} type={type} className={(touched && error) ||
       (existed && (label==='Contact Email')) || (work_phone_number_error)  || (mobile_number_error) || (website_error) || (facebook_error) || (pin_code_error)
      ? 'fullwidth input-control border border-danger' : 'fullwidth input-control'} />
      {touched &&
        ((error && <small className="text-danger">{label}{error}</small>) ||
          (warning && <span>{warning}</span>))}

      { (label==='Contact Email') && (existed) && <small class="text-danger">
        {existed}
      </small>}
      { (work_phone_number_error) && <small class="text-danger">
        {work_phone_number_error}
      </small>}
      { (mobile_number_error) && <small class="text-danger">
        {mobile_number_error}
      </small>}
      { (website_error) && <small class="text-danger">
        {website_error}
      </small>}
      { (facebook_error) && <small class="text-danger">
        {facebook_error}
      </small>}
      { (pin_code_error) && <small class="text-danger">
        {pin_code_error}
      </small>}
    </Fragment>

  )
}



const renderFieldSelect = ({
  input,
  label,
  type,
  id,
  existed,
  options,
  country,
  countryState,
  city,
  meta: { touched, error, warning, }
}) => {

  return (
    <Fragment>
      {(options && label === "Salutation") &&
        <select {...input} className={(touched && error) ? 'fullwidth input-control border border-danger' : 'fullwidth input-control'}>
          <option id='salutation_id' value="">Select Salutation</option>
          {options.map((salutation) =>
            <option value={salutation.value} >{salutation.text}</option>
          )}
        </select>
      }
      {(options && label === "Branch") &&
      <Fragment>
        <div className="col-sm-6 form-group">
        <label className="text-light-grey" htmlFor="id_branch">
        Branch
          </label>
        <select {...input} className="fullwidth input-control" >
          <option id='no_designation' value="">Select Branch</option>
          {options.map((branch, index) =>
            <option key={index} value={branch.slug} >{branch.branch_name}  </option>
          )}
        </select>
        </div>
      </Fragment>
      }
      {(options && label === "Department") &&
      <div className="col-sm-6 form-group">
      <label className="text-light-grey" htmlFor="id_department">
        Department
          </label>
        <select {...input} className="fullwidth input-control">
          <option id='no_designation' value="">Select Department</option>
          {options.map((department, index) =>
            <option key={index} value={department.slug} >{department.department_name}  </option>
          )}
        </select>
        </div>
      }

      {(label === "Country" || label === "State" || label === "City") &&
        <select {...input} className={(touched && error) ? 'input-control border border-danger' : 'input-control'}>
          <option value="">{label}</option>
          {(country && label === "Country") &&
            <Fragment>
              {country.countryList.map((plan, index) => (
                <option key={index} value={plan.id}>{plan.name_ascii}</option>
              ))}</Fragment>
          }
          {(countryState && label === "State") &&
            <Fragment>
              {countryState.stateList.map((plan, index) => (
                <option key={index} value={plan.id}>{plan.name_ascii}</option>
              ))}</Fragment>
          }
          {(city && label === "City") &&
            <Fragment>
              {city.cityList.map((plan, index) => (
                <option key={index} value={plan.id}>{plan.name_ascii}</option>
              ))}</Fragment>
          }
        </select>
      }

      {(label === "Currency") &&
        <Fragment>
          <select {...input} name="currency" className="input-control" >
            {options.map((option) => (
              <option key={option.value} id={option} value={option.value}>{option.label}</option>
            ))}
          </select>
        </Fragment>
      }

      {
        (label === "Portal Language") &&
        <Fragment>
          <select {...input} className="input-control " >
            <option value="">Select Portal Language</option>
            <option value="English">English</option>
            {/* {Helper.languages.map(option=>{
                                  return <option key={option.value} id={option} value={option.value}>{option.label}</option>
                                })} */}
          </select>
        </Fragment>
      }

      {
        (label === "Payment Terms")&&
        <Fragment>
          <select {...input} className="input-control ">
            <option value="">Select Payment Terms</option>
            {options.map(option=>
              <option key={option.value} value={option.value}>{option.text}</option>
              )}
          </select>
        </Fragment>
      }
      {
        (label==="Enable Portal")&&
        <Fragment>

        </Fragment>
      }

      <div>
        {touched &&
          ((error && <small className="text-danger">{label}{error}</small>) ||
            (warning && <span>{warning}</span>))}
      </div>

    </Fragment>
  )
}


const radioCheckbox = ({ input, label, options, type, checked, meta: { touched, error, warning } }) => (
  <Fragment>

    {options.map((plan, index) => (
      <div className="col-md-3" key={index}>
        <input type="radio" {...input} checked={checked} key={index} name={plan.name} value={plan.value} id={plan.id} /> {plan.text}
      </div>
    ))
    }

    {touched &&
      ((error && <small className="text-danger">{label}{error}</small>) ||
        (warning && <span>{warning}</span>))}


  </Fragment>
)

const renderCheckBoxField =({ input, label, options, type, checked, meta: { touched, error, warning } }) => (
  <Fragment>
        <input type="checkbox" {...input}/> 
     
  </Fragment>
)

const renderContactPerson = ({ fields, meta: { error, submitFailed } }) => (
  <ul>
    <li>
      <a className="add-row" href="javascript:void(0)">
        <div className="row">
          <div className="col-sm-12 text-center">
            <button type="button" className="btn btn-outline-primary add-row" onClick={() => fields.push({})}>
              <i className="icon-plus"></i> Add Contact Persons
                              </button>
          </div>
        </div>
      </a>

      {/* {submitFailed && error && <span>{error}</span>} */}
    </li>
    {fields.map((member, index) => (
      <li key={index}>
        {/* <button
          type="button"
          title="Remove Member"
          onClick={() => fields.remove(index)}
        /> */}
        

        <div className="row mb-3 formset_row dynamic-form">
          <div className="col-sm-6 form-group">
            <label className="text-light-grey" for="id_contactperson_set-0-salutation">Salutation<span class="text-danger">*</span></label>

            <Field
              component={renderFieldSelect}
              name={`${member}.contactperson_salutation`}
              id="salutation_id"
              label="Salutation"
              validate={required}
              options={salutationType}
            />

          </div>
          <div className="col-sm-6 form-group">
            <label className="text-light-grey" htmlFor="id_contactperson_set-0-first_name">First name<span className="text-danger">*</span></label>
            <Field type="text"
              name={`${member}.contactperson_first_name`}
              component={renderField}
              label="First Name"


              validate={[required, alphabetOnly]}
            />
          </div>
          <div className="col-sm-6 form-group">
            <label className="text-light-grey" htmlFor="id_contactperson_set-0-last_name">Last name </label>
            <Field type="text"
              name={`${member}.contactperson_last_name`}
              component={renderField}
              label="Last Name"


              validate={[required, alphabetOnly]}
            />
          </div>
          <div className="col-sm-6 form-group">
            <label className="text-light-grey" htmlFor="id_contactperson_set-0-email">Email </label>
            <Field
              name={`${member}.contactperson_email`}
              component={renderField}
              label="Contact Email"


              validate={[required, email]}
            />
          </div>
          <div class="col-sm-6 form-group">
            <label class="text-light-grey" for="id_contactperson_set-0-phone_number">Phone number </label>
            <Field type="text"
              name={`${member}.contactperson_mobile_number`}
              component={renderField}
              label="Mobile Number"


              validate={mobileValid}
            />
          </div>
          {/* <div class="col-sm-6 form-group">
                              <label class="text-light-grey" for="id_contactperson_set-0-designation">Designation </label>
                              <input type="text" name="contactperson_set-0-designation" placeholder="Designation" maxlength="128" className="input-control" id="id_contactperson_set-0-designation"/> 
                            </div> */}
          <div className="col-sm-6 form-group">
            <label className="text-light-grey">Skype Name/Number
                              </label>
            <Field type="text"
              name={`${member}.contactperson_skype_name`}
              component={renderField}
              label="Skype Name/Number"



            />
          </div>
          <div className="col-sm-12 text-right">
            <a className="delete-row btn btn-outline-primary"
              href="javascript:void(0)"
              onClick={() => fields.remove(index)}
            ><i class=" icon-delete"></i> remove</a>



          </div>
          <div className="col-sm-12"><hr></hr></div>


        </div>


      </li>
    ))}
  </ul>
)








// import { Field,reduxForm} from 'redux-form'
// import {countryReorder} from '../../../countryReorder'

const MODAL_OPEN_CLASS = "modal-open";

const required =  value => {
    return(value || typeof value === 'number' ? undefined : ' is Required')
}

const mobileValid = value => {
    var mobilenoregex = /^(?!\b(0)\1+\b)(\+?\d{1,3}[. -]?)?\(?\d{3}\)?([. -]?)\d{3}\3\d{4}$/
    if (value && value.length >= 1) {
        return (mobilenoregex.test(value) ? undefined : 'Enter a Valid Mobile Number')
    }
    
    return undefined
}

const maxLength = max => value =>
  value && value.length > max ? `Must be ${max} characters or less` : undefined

const maxLength128 = maxLength(128)


const rendermobileField = ({input,label,type,backenderror,id,className,accept,initialValue,name,meta: { touched, error, warning, }}) => {
    return (          
        <Fragment>
            <input {...input} placeholder = {label} type={type} className={className} id={id} accept={accept} defaultValue={initialValue} />
            {touched && 
                ((error && <small className="text-danger">{error} {label =="Phone number" &&'with country code'}</small>) ||
                    (warning && <span>{warning}</span>))}
            {backenderror&&
            <small className="text-danger">{backenderror} with country code</small>
            }
        </Fragment>
)}


const renderFieldLocationSelect = ({
    input,
    label,
    country,
    countryState,
    city,
    noCity,
    className,
    meta: { touched, error, warning, }
}) => {

    return (
        <Fragment>
        <select {...input} className={(touched && error) ? 'input-control border border-danger' : 'input-control'}>
            <option value="">{label}</option>
            {(country && label === "Country") &&
                <Fragment>
                    {country.countryList.map((plan, index) => (
                        <option key={index} value={plan.id}>{plan.name_ascii}</option>
                    ))}</Fragment>
            }
            {(countryState && label === "State") &&
                <Fragment>
                    {countryState.stateList.map((plan, index) => (
                        <option key={index} value={plan.id}>{plan.name_ascii}</option>
                    ))}</Fragment>
            }
            {(city && label === "City") &&
                <Fragment>
                    {city.cityList.map((plan, index) => (
                        <option key={index} value={plan.id}>{plan.name_ascii}</option>
                    ))}</Fragment>
            }
        </select>
        <div>
            {touched &&
                ((error && <small className="text-danger">{label}{error}</small>) ||
                    (warning && <span>{warning}</span>))}
        </div>
        {(noCity&&noCity.employeeUpdateError&&noCity.employeeUpdateError.data&&noCity.employeeUpdateError.data.city&&noCity.employeeUpdateError.status===400) && <small class="text-danger">
            Please select the city
        </small>}

        
       

    </Fragment>

       
    )
}


const renderRadioField = ({ input, label, type, checked }) => (
    <Fragment>
        
        <label>{label}</label>
        <input {...input} type={type} checked={checked} /> 
        
    </Fragment>
);

const checkboxField = ({ input,options,className,makeCheck,edit,type, checked,meta: { touched, error, warning } }) => (
    <Fragment>
        
        {options.map((option, index) => (
            <div className="col-lg-4 col-md-6 mt-3" key={index}>
                <label htmlFor="162">
                    <span className="checkbox">
                        <input type="checkbox" id={option.slug} className={className} name={`${input.name}[${index}]`} value={option.slug} checked={makeCheck.length>0?(makeCheck.indexOf(option.slug) !== -1):(input.value.indexOf(option.slug) !== -1)}
                            onChange={(event) => {
                                
                                if(makeCheck.length>0){
                                    const newValue = [...makeCheck]
                                    if (event.target.checked) {
                                        newValue.push(option.slug);
                                        makeCheck.push(option.slug);
                                    } else {
                                        
                                        newValue.splice(newValue.indexOf(option.slug), 1);
                                        makeCheck.splice(makeCheck.indexOf(option.slug), 1);
                                    }
                                    return input.onChange(newValue);
                                }else{
                                    const newValue = [...input.value];
                                    if (event.target.checked) {
                                        newValue.push(option.slug);
                                    } else {
                                        newValue.splice(newValue.indexOf(option.slug), 1);
                                    }
                                    return input.onChange(newValue);

                                }
                            }}/>
                    </span>
                    {option.group_name}
                </label>
            </div>
            ))}
        
        <label>{touched &&
            ((error && <small className="text-danger">{error}</small>) ||
                (warning && <span>{warning}</span>))}</label>
    </Fragment>
)


class AddClientModal extends Component{
    constructor(props){
        super(props)

        this.state = {
          show_branch: false,
          success_message: false,
          existed_error: '',
          other_details: false,
          address_active: true,
          contactPerson_active: false,
          remarks_active: false,
          image_validation_error: false,
          pincodeError: '',
          work_phone_number_error:'',
          mobile_number_error:'',
          website_error:'',
          facebook_error:'',
          image:null,
          image_validation_error: false,
          checked:false,
          isOpen:false,
          emailValue:'',
          passwordValue:''
        };
    }

  select_business = () => {

    this.setState({ show_individual: false })
  }

  select_individual = () => {
    this.setState({ show_individual: true })
  }

  closeSuccessMessage = () => {
    this.setState({
      success_message: false
    })
  }
  clickedOtherDetails = () => {
    this.setState({
      other_details: true,
      contactPerson: false,
      address_active: false,
      remarks_active: false,
    })
  }
  clickedContactPerson = () => {
    this.setState({
      contactPerson: true,
      other_details: false,
      address_active: false,
      remarks_active: false,
    })
  }
  clickedAddress = () => {
    this.setState({
      address_active: true,
      other_details: false,
      contactPerson: false,
      remarks_active: false,
    })
  }
  clickedRemarksActive = () => {
    this.setState({
      remarks_active: true,
      other_details: false,
      address_active: false,
      contactPerson: false,
    })
  }

    componentDidMount(){
        document.body.classList.add(MODAL_OPEN_CLASS);

        this.props.fetchCountryList()
    }

    componentDidUpdate(prevProps){

      if(prevProps.clientData!=this.props.clientData){
      
        this.props.clientData&&this.props.clientData.client_obj&&this.props.clientData.client_obj.data&&this.props.clientData.client_obj.data.client&&this.props.clientData.client_obj.data.client.email&&
        this.props.clientData.client_obj.data.client.email[0]&&this.setState({existed_error:'Contact Email already exists'})
  
        this.props.clientData&&this.props.clientData.client_obj&&this.props.clientData.client_obj.data&&this.props.clientData.client_obj.data.client&&this.props.clientData.client_obj.data.client.photo&&
        this.props.clientData.client_obj.data.client.photo[0]&&this.setState({image_validation_error:true})
  
        this.props.clientData&&this.props.clientData.client_obj&&this.props.clientData.client_obj.data&&this.props.clientData.client_obj.data.client&&this.props.clientData.client_obj.data.client.work_phone&&
        this.props.clientData.client_obj.data.client.work_phone[0]&&this.setState({work_phone_number_error:'Enter Work Phone Number with Country Code'})
  
  
        this.props.clientData&&this.props.clientData.client_obj&&this.props.clientData.client_obj.data&&this.props.clientData.client_obj.data.client&&this.props.clientData.client_obj.data.client.mobile&&
        this.props.clientData.client_obj.data.client.mobile[0]&&this.setState({mobile_number_error:'Enter Mobile Number with Country Code'})
  
        this.props.clientData&&this.props.clientData.client_obj&&this.props.clientData.client_obj.data&&this.props.clientData.client_obj.data.client&&this.props.clientData.client_obj.data.client.website&&
        this.props.clientData.client_obj.data.client.website[0]&&this.setState({website_error:'Invalid Website'})
  
        this.props.clientData&&this.props.clientData.client_obj&&this.props.clientData.client_obj.data&&this.props.clientData.client_obj.data.other_details&&this.props.clientData.client_obj.data.other_details.facebook&&
        this.props.clientData.client_obj.data.other_details.facebook[0]&&this.setState({facebook_error:'Invalid URL'})
  
        this.props.clientData&&this.props.clientData.client_obj&&this.props.clientData.client_obj.data&&this.props.clientData.client_obj.data.billing_address&&
        this.props.clientData.client_obj.data.billing_address.non_field_errors&&this.props.clientData.client_obj.data.billing_address.non_field_errors[0]&&this.setState({pincodeError:'Invalid Zip Code'})
  
        if(this.props.clientData&&this.props.clientData.portal_password_status==='success'){
          this.setState({passwordValue:this.props.clientData.portal_password})
          this.closePortalModal()
          this.props.clearClientPortalPasswordStatus()
      }
    }
  }

    componentWillUnmount() { 
    document.body.classList.remove(MODAL_OPEN_CLASS);
    }

    select_org=()=>{ 
        this.props.retrieveDepartments('');
        this.setState({show_branch:false})
    }
    
    select_branch=()=>{ 
        this.setState({show_branch:true})   
    }

    handleBranchChange = (e) => {
        this.props.retrieveDepartments(e.target.value);
    }

    handleCountryChange = (e) => {
        //  making fields null
        this.props.change('state','')  
        this.props.change('city','')

        this.props.fetchStatesList(e.target.value);
        this.props.fetchCityList(" ");
    }

    handleStateChange = (e) => {
        // making fields null
        this.props.change('city','')
        
        this.props.fetchCityList(e.target.value);
    }

    upLoadFile = (e) => {
      let image = e.target.files[0]
      var imageTypes = ['image/png', 'image/jpeg', 'image/gif']
      var image_index = imageTypes.indexOf(image.type)
      if (image_index >= 0) {
        this.setState({ image: image, image_validation_error: false }, () => { console.log(this.state.image, 'uploaded image') })
      }
      else {
        this.setState({ image: null, image_validation_error: true })
      }
    }
    

  clearExistedError = (params,e) => {
    
          params==='email'&&this.setState({ existed_error: '' ,emailValue:e})
          params==='work_phone'&&this.setState({work_phone_number_error:''})
          params==='mobile_number'&&this.setState({mobile_number_error:''})
          params==='website'&&this.setState({website_error:''})
          params==='facebook'&&this.setState({facebook_error:''})
          params==='pin_code'&&this.setState({pincodeError:''})
        }


        submit = (values) => {
    
          console.log('before values', values)
          
          if(!values.hasOwnProperty('organization')){
            values.organization=localStorage.getItem("organization_slug")
          }
          if(!values.hasOwnProperty('branch')){
            values.branch=null
          }
          if(!values.hasOwnProperty('department')){
            values.department=null
          }
          if(!values.hasOwnProperty('company_name')){
            values.company_name=""
          }
          if(!values.hasOwnProperty('mobile')){
            values.mobile=""
          }
          if(!values.hasOwnProperty('work_phone')){
            
            values.work_phone=""
          }
          if(!values.hasOwnProperty('client_display_name')){
            values.client_display_name=""
          }
          if(!values.hasOwnProperty('website')){
            values.website=""
          }
          
          
          values.photo=null   
      
          
          // other details
          if(!values.hasOwnProperty('currency')){
            values.currency=""
          }if(!values.hasOwnProperty('payment_terms')){
            values.payment_terms=""
          }
          if(!this.state.checked){
            values.enable_portal=false
          }
          else{
            values.enable_portal=true
            values.portal_password=this.state.passwordValue
          }
          if(!values.hasOwnProperty('portal_language')){
            values.portal_language="English"
          }
          if(!values.hasOwnProperty('facebook')){
            values.facebook=""
          }
          if(!values.hasOwnProperty('skype_name')){
            values.skype_name=""
          }
          // address
          if(!values.hasOwnProperty('attention')){
            values.attention=""
          }if(!values.hasOwnProperty('address1')){
            values.address1=""
          }if(!values.hasOwnProperty('address2')){
            values.address2=""
          }if(!values.hasOwnProperty('pin_code')){
            values.pin_code=""
          }if(!values.hasOwnProperty('fax')){
            values.fax=""
          }
          if(!values.hasOwnProperty('phone')){
            values.phone=""
          }
          // shipping
          if(!values.hasOwnProperty('shipping_attention')){
            values.shipping_attention=""
          }if(!values.hasOwnProperty('shipping_country')){
            values.shipping_country=""
          }if(!values.hasOwnProperty('shipping_state')){
            values.shipping_state=""
          }if(!values.hasOwnProperty('shipping_city')){
            values.shipping_city=""
          }if(!values.hasOwnProperty('shipping_address1')){
            values.shipping_address1=""
          }if(!values.hasOwnProperty('shipping_address2')){
            values.shipping_address2=""
          }if(!values.hasOwnProperty('shipping_pin_code')){
            values.shipping_pin_code=""
          }
          if(!values.hasOwnProperty('shipping_fax')){
            values.shipping_fax=""
          }
          if(!values.hasOwnProperty('shipping_phone')){
            values.shipping_phone=""
          }
          // remarks
          if(!values.hasOwnProperty('remarks')){
            values.remarks=""
          }
          // contact_person
          if(!values.hasOwnProperty('contact_person')){
            values.contact_person=[]
          }
          console.log('after values', values)
          
          this.setState({
            existed_error: '',
            pincodeError: '',
            work_phone_number_error:'',
            mobile_number_error:'',
            website_error:'',
            facebook_error:'',
        },() => {
         (!this.state.image_validation_error)&& this.props.addClient(values)
         })
          
        }

        toggleEnablePortalModal = (e) => {
          e.target.checked?   
          this.setState({ isOpen: !this.state.isOpen ,
            checked: e.target.checked
          }):
          this.setState({checked: e.target.checked,passwordValue:''})
        }
    
    render(){
        const Currency = Helper.Currency
        const clientType = [
          { text: 'Business', value: 'business', name: "client_type", id: "id_client_type_0" },
          { text: 'Individual', value: 'individual', name: "client_type", id: "id_client_type_1" }
        ]
        const PaymentTerms =[
          {text:'Fixed',value:'fixed'},
          {text:'Hourly',value:'hourly'}
        ]
        const { handleSubmit} = this.props;
        return(
            <Fragment>
                <div id="modal-block">
                    <div className="modal form-modal show" id="editBasicDetails" tabIndex="-1" role="dialog" aria-labelledby="TermsLabel" style={{paddingRight: "15px", display: "block"}}>
                        <div className="modal-dialog modal-dialog-centered" role="document">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <h5 className="modal-title" id="TermsLabel">Add Client Details</h5>
                                    <button onClick = {this.props.closeModal} type="button" className="btn btn-black close-button" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>

                                <div className="modal-body">
                                    <form  onSubmit={handleSubmit(this.submit)}>
                                        <div className="row popup-form">
                                        <div className="col-md-12">

<h6 className="mb-4 mt-4">Primary Contact
      <button type="button" className="btn btn-link-grey p-0" role="button" >
    <i className="icon-warning-circle"></i>
  </button>
</h6>
<div className="row mb-3">
  <div className="col-sm-12 form-group">
    <label className="text-light-grey">Customer Type <span className="text-danger">*</span></label>
    <span>
      <Field type="radio"
        name="client_type"
        validate={required}
        component={radioCheckbox}
        options={clientType}
        label="Customer Type"
      />
    </span>
  </div>



  <div className="col-sm-6 form-group">
    <label className="text-light-grey" htmlFor="id_salutation">
      Salutation
          <span className="text-danger">*</span>
    </label>
    <Field
      component={renderFieldSelect}
      name="salutation"
      id="salutation_id"
      label="Salutation"
      validate={required}
      options={salutationType}
    />


  </div>
  <div className="col-sm-6 form-group">
    <label className="text-light-grey" htmlFor="id_first_name">
      First name <span className="text-danger">*</span>
        </label>
    <Field type="text"
      name="first_name"
      component={renderField}
      label="First Name"
      onChange={this.clearExistedError}
      existed={this.state.existed_error}
      validate={[required, alphabetOnly]}
    />
  </div>

  <div className="col-sm-6 form-group">
    <label className="text-light-grey" htmlFor="id_last_name">
      Last name <span className="text-danger">*</span>
        </label>
    <Field type="text"
      name="last_name"
      component={renderField}
      label="Last Name"
      onChange={this.clearExistedError}
      existed={this.state.existed_error}
      validate={[required, alphabetOnly]}
    />
  </div>
  <div className="col-sm-6 form-group">
    <label className="text-light-grey" htmlFor="id_first_name">
      Company name
      </label>
    <Field type="text"
      name="company_name"
      component={renderField}
      label="Company Name"
      onChange={this.clearExistedError}
    />
  </div>
  <div className="col-sm-6 form-group">
    <label className="text-light-grey" htmlFor="id_contact_email">
      Contact email <span className="text-danger">*</span>
      </label>
    <Field type="email"
      name="email"
      component={renderField}
      label="Contact Email"
      onChange={(params,e)=>this.clearExistedError('email',e)}
      existed={this.state.existed_error}
      validate={[required, email]}
    />
  </div>
  <div className="col-sm-6 form-group">
    <label className="text-light-grey" htmlFor="id_customer_display_name">
      Customer Display Name
      </label>
    <Field type="text"
      name="client_display_name"
      component={renderField}
      label="Customer Display Name"
      onChange={this.clearExistedError}


    />
  </div>
  
    
    <Field
      component={renderFieldSelect}
      name="branch"
      id="branch_id"
      label="Branch"
      options={(this.props.branches && this.props.branches.data && this.props.branches.data && this.props.branches.data.length > 0) && this.props.branches.data}
      onChange={this.handleBranchChange}

    />
  
  
    <Field
      component={renderFieldSelect}
      name="department"
      id="department_id"
      label="Department"
      options={(this.props.departmentlist && this.props.departmentlist.departments) && this.props.departmentlist.departments}
    />
  

  <div className="col-sm-6 form-group">
    <label className="text-light-grey" htmlFor="id_work_phone_number">
      Work Phone Number
      </label>
    <Field type="text"
      name="work_phone"
      component={renderField}
      label="Work Phone Number"
      onChange={()=>this.clearExistedError('work_phone')}
      work_phone_number_error={this.state.work_phone_number_error}
      validate={mobileValid}
    />
  </div>
  <div className="col-sm-6 form-group visible-img">
    <label className="text-light-grey" htmlFor="id_photo">
      Photo
      </label>
    <input className="inputfile" type="file" onChange={this.upLoadFile} id="id_logo" accept="image/*" />
    {/* <Field type="file" */}
    {/* name="photo" */}
    {/* component={renderField} */}
    {/* onChange={this.clearExistedError} */}
    {/* existed={this.state.existed_error} */}
    {/* validate={[required]} */}
    {/* /> */}
    {this.state.image_validation_error && <small class="text-danger">Upload a valid image. The file you uploaded was either not an image or a corrupted image.</small>}

  </div>
  <div className="col-sm-6 form-group">
    <label className="text-light-grey" htmlFor="id_skype">
      Mobile Number
      </label>
    <Field type="text"
      name="mobile"
      component={renderField}
      label="Mobile Number"
      onChange={()=>this.clearExistedError('mobile_number')}
      mobile_number_error={this.state.mobile_number_error}
      validate={mobileValid}
    />

  </div>
  <div className="col-sm-6 form-group">
    <label className="text-light-grey" htmlFor="id_website">
      Website
      </label>
    <Field type="text"
      name="website"
      component={renderField}
      onChange={()=>this.clearExistedError('website')}
      website_error={this.state.website_error}
      validate={websiteValiadtion}
    />
  </div>
</div>
</div>
<div className="col-md-12 full-height-8">
<div className="row">
  <div className="userMail-grid-tabbx">
    <ul className="nav nav-tabs" id="myTab" role="tablist">
      <li className="nav-item">
      <a className={this.state.other_details?"nav-link active show":"nav-link"} onClick={this.clickedOtherDetails} id="otherdetail-tab" data-toggle="tab" href="#otherdetail" role="tab" aria-controls="otherdetail" aria-selected="true">Other Details 
      
      </a>
      </li>
      <li className="nav-item"> 
      
      <a className={this.state.address_active?"nav-link active show":"nav-link"} id="address-tab" onClick={this.clickedAddress} data-toggle="tab" href="#address" role="tab" aria-controls="address" aria-selected="false">Address
      {this.state.pincodeError&&<i class="icon-warning-triangle text-danger text-sm"></i>}
      </a>                    
      </li>
      <li class="nav-item"><a onClick={this.clickedContactPerson} className={this.state.contactPerson?"nav-link active show":"nav-link"} data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Contact Persons</a></li>
      <li class="nav-item"><a onClick={this.clickedRemarksActive} className={this.state.remarks_active?"nav-link active show":"nav-link"} data-toggle="tab" href="#remarks" role="tab" aria-controls="remarks" aria-selected="false">Remarks</a></li>
    </ul>
    
    <div className="tab-content" id="myTabContent">
    
    <div className={this.state.other_details?"tab-pane userMail-tab fade show active":"tab-pane userMail-tab fade"} id="otherdetail" role="tabpanel" aria-labelledby="otherdetail-tab">
        
          <div className="row">
            <div className="col-md-12 mb-2">
              <label className="text-light-grey" htmlFor="id_currency">Currency</label>
              <Field type="text"
                name="currency"
                component={renderFieldSelect}
                options={Currency}
                label="Currency"
              />
            </div>
            <div className="col-md-7  mb-2  ">
              <label className="text-light-grey" htmlFor="id_payment_terms">Payment terms</label>
              {/* <select name="payment_terms" className="input-control" id="id_payment_terms">
                <option value="" defaultValue="">Select Payment Terms</option>
              </select> */}
              <Field type="text"
                name="payment_terms"
                component={renderFieldSelect}
                options={PaymentTerms}
                label="Payment Terms"
              />
            </div>
            {/* <div className="col-md-5  mb-2  align-self-end">
                <button type="button" className="btn btn-outline-primary btn-block" data-toggle="modal" data-target="#paymentTermsModal">
                <i className="icon-settings"></i> Configure Terms
                </button>
              </div> */}
            <div className="col-md-12 mb-2 mt-1">
              <label className="text-light-grey" htmlFor="id_enable_portal">Enable Portal ?
                  <button className="btn btn-link-grey p-0" role="button" data-toggle="popover" data-placement="bottom" data-trigger="focus" title="Enable Portal ?" data-content="And here's some amazing content. It's very engaging. Right?">
                  <i className="icon-warning-circle" style={{ verticalAlign: "middle" }}></i>
                </button>
              </label>
              <p>
                <span className="checkbox-cs mr-2">
                  <label> 
                    <input type="checkbox" onChange={this.toggleEnablePortalModal}name="enable_portal" id="id_enable_portal" />
                                                      
                    <span className="check-input mr-2"><i className="icon-checked icon-tick"></i></span>
                        Allow portal access for this contact
                    </label>
                </span>
                <span className="text-muted">( Email address is mandatory )</span>
              </p>
            </div>
            {/* {(this.state.isOpen ) && <PortalPasswordModal email={this.state.emailValue} {...this.props} onCloseModal={this.closePortalModal}/> } */}

            <div className="col-md-12 mb-2 mt-1">
              <label className="text-light-grey" htmlFor="id_portal_language">Portal language 
                <button className="btn btn-link-grey p-0" role="button" data-toggle="popover" data-placement="bottom" data-trigger="focus" title="Portal Language" data-content="And here's some amazing content. It's very engaging. Right?">
                  <i className="icon-warning-circle" style={{ verticalAlign: "middle" }}></i>
                </button>
              </label>
              <Field
                name="portal_language"
                component={renderFieldSelect}
                label="Portal Language"
              />

            </div>
            <div className="col-sm-6 form-group">
              <label className="text-light-grey" htmlFor="id_facebook">Facebook </label>


              <Field type="text"
                name="facebook"
                component={renderField}                              
                onChange={()=>this.clearExistedError('facebook')}
                facebook_error={this.state.facebook_error}
                validate={websiteValiadtion}
              />

            </div>
            {/* <div className="col-sm-6 form-group">
                <label className="text-light-grey" htmlFor="id_twitter">Twitter</label>
                <div className="input-group">
                   <div className="input-group-prepend soc-icon-lt">
                      <span className="input-group-text" id="addon-wrapping"><i className="icon-twitter text-primary"></i></span>
                   </div>
                   <input type="text" name="twitter" placeholder="http://www.twitter.com/" maxLength="200" className="input-control inp-padding" id="id_twitter"/>
                </div>
              </div> */}
            <div className="col-sm-6 form-group">
              <label className="text-light-grey" htmlFor="id_skype">
                Skype Name/Number
              </label>


              <Field type="text"
                name="skype"
                component={renderField}
                label="Skype Name/Number"
                onChange={this.clearExistedError}
                existed={this.state.existed_error}

              />

            </div>
          </div>
          
      </div>

      
      <div className={this.state.address_active?"tab-pane userMail-tab fade show active":"tab-pane userMail-tab fade"} id="address" role="tabpanel" aria-labelledby="address-tab">
          <div className="row mb-3">
            <div className="col-sm-6 form-group">
              <label className="text-light-grey" for="id_attention">Attention </label>
              <Field
                name="attention"
                component={renderField}
                label="Attention"
              />
            </div>
            <div className="col-sm-6 form-group">
              <label className="text-light-grey" for="id_country">Country <span class="text-danger">*</span></label>
              <Field
                name="country"
                type="select"
                country={countryReorder(this.props.countryDetails)}
                component={renderFieldSelect}
                className="input-control select-country"
                label="Country"
                validate={required}
                onChange={this.handleCountryChange} />
            </div>
            <div className="col-md-4 form-group">
              <label className="text-light-grey" for="id_state">State <span class="text-danger">*</span></label>
              <Field
                name="state"
                type="select"
                countryState={this.props.stateDetails}
                component={renderFieldSelect}
                className="input-control select-state"
                label="State"
                validate={required}
                onChange={this.handleStateChange}
              />
            </div>
            <div className="col-md-4 form-group">
              <label className="text-light-grey" for="id_city">City <span class="text-danger">*</span></label>
              <Field
                name="city"
                type="select"
                city={this.props.cityDetails}
                component={renderFieldSelect}
                className="input-control select-city"
                label="City"
                validate={required} />
            </div>
            <div class="col-md-4 form-group">
              <label className="text-light-grey" for="id_pin_code">Zip Code </label>
              <Field
                type="text"
                name="pin_code"
                component={renderField}
                label="Zip Code"
                onChange={()=>this.clearExistedError('pin_code')}
                pin_code_error={this.state.pincodeError} />
            </div>
            <div className="col-sm-6 form-group">
              <label className="text-light-grey" for="id_address1">Address1 </label>
              <Field type="text"
                name="address1"
                cols="40" rows="10"
                component="textarea"
                className="input-control input-control"
                placeholder="Address1" />
            </div>
            <div className="col-sm-6 form-group">
              <label className="text-light-grey" for="id_address2">Address2 </label>
              <Field type="text"
                name="address2"
                cols="40" rows="10"
                component="textarea"
                className="input-control input-control"
                placeholder="Address2" />
            </div>
            <div className="col-sm-6 form-group">
              <label className="text-light-grey" for="id_fax">Fax </label>
              <Field
                name="fax"
                component={renderField}
                label="Fax"
              />
            </div>
          </div>
        </div>
        

      
        <div className={this.state.contactPerson?"tab-pane userMail-tab fade show active":"tab-pane userMail-tab fade"} id="contact" role="tabpanel" aria-labelledby="contact-tab">
          <FieldArray name="contact_person" component={renderContactPerson} />
        </div>
        

      
        <div className={this.state.remarks_active?"tab-pane userMail-tab fade show active":"tab-pane userMail-tab fade"} id="remarks" role="tabpanel" aria-labelledby="remarks-tab">
          <label className="text-light-grey" for="id_remarks">Remarks </label>
          <span class="text-sm"> (For Internal use)</span>
          <Field type="text"
            name="remarks"
            cols="40" rows="10"
            component="textarea"
            className="input-control input-control"
            placeholder="Remarks" />
        </div>
        
       

    </div>
  </div>
</div>
</div>
                                        </div>
                                  
                                   
                                    <div className="modal-footer">
                                        <input type="submit"  className="btn btn-success" value="Save"/>
                                    </div>
                                    </form>

                                    
                                </div>

                                
                            </div>
                        </div>
                    </div>
                </div>
                <div className="modal-backdrop show"></div>
            </Fragment>
        )
    }
}
AddClientModal = reduxForm({
    form:'addClientModalForm'
})(AddClientModal)
export default AddClientModal