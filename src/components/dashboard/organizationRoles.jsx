import React, {Component,Fragment} from 'react'
import SideBar from './common/sideBar'
import DashBoardHeaderContainer from '../../containers/common/dashBoardHeaderContainer'
import DashBoardSubheader from './common/dashboardSubHeader'
import OrganizationRolesContent from './content/organizationRolesContent'

class OrganizationRoles extends Component{
    constructor(props){
        super(props)
        this.state = {
            show : true,
            hide :  true
        }
    }
    componentDidMount(){
        this.props.fetchPermissionGroups()
        
    }
    componentWillUnmount(){
        this.props.clearSearch()
    }

    render(){
       
        return( 
            <Fragment>
                <div className="container-fluid">
                    <div className="row">
                        {<SideBar a={true} {...this.props}/>}
                        <div className="col-sm-12 container-body ">
                            <DashBoardHeaderContainer/>
                            <div className="row">
                                <DashBoardSubheader subheader = "Roles" {...this.props}/>
                                <OrganizationRolesContent {...this.props}/>
                            </div>
                        </div>
                    </div>
                </div>
            </Fragment>
        )
    }
}

export default OrganizationRoles