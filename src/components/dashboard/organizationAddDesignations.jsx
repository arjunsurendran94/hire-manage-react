import React, {Component,Fragment} from 'react'
import SideBar from './common/sideBar'
import DashBoardHeaderContainer from '../../containers/common/dashBoardHeaderContainer'
import DashBoardSubheader from './common/dashboardSubHeader'
import OrganizationAddDesignationContent from './content/organizationAddDesignationContent'

class OrganizationAddDesignations extends Component{
    constructor(props){
        super(props)
        this.state = {
            show : true,
            hide :  true
        }
    }
    componentDidMount(){
        this.props.fetchPermissionGroups()
        if(this.props.update){
            this.props.RetrieveDesignation(this.props.match.params.designation_slug)
        }
    }
    render(){
        return( 
            <Fragment>
                <div className="container-fluid">
                    <div className="row">
                        {<SideBar a={true} {...this.props}/>}
                        <div className="col-sm-12 container-body ">
                            <DashBoardHeaderContainer/>
                            <div className="row">
                                <DashBoardSubheader subheader = "Add Designation" {...this.props}/>
                                <OrganizationAddDesignationContent {...this.props}/>
                            </div>
                        </div>
                    </div>
                </div>
            </Fragment>
        )
    }
}

export default OrganizationAddDesignations