import React, {Component,Fragment} from 'react'
import SideBar from './common/sideBar'
import DashBoardHeaderContainer from '../../containers/common/dashBoardHeaderContainer'
import DashBoardSubheader from './common/dashboardSubHeader'
import AddDepartmentContent from './content/addDepartmentContent'

class OrganizationAddNewDepartments extends Component{
    render(){
        return( 
            <Fragment>
                <div className="container-fluid">
                    <div className="row">
                        <SideBar a={true} {...this.props}/>
                        <div className="col-sm-12 container-body ">
                            <DashBoardHeaderContainer/>
                            <div className="row">
                                <DashBoardSubheader subheader = "Add Department"/>
                                <AddDepartmentContent {...this.props} />
                            </div>
                        </div>
                    </div>

                    </div>
            </Fragment>
        )
    }
}

export default OrganizationAddNewDepartments