import React, {Component, Fragment} from 'react'
import { Link } from 'react-router-dom'

class OrganizationEmployeeContent extends Component{

    renderPagination=()=>{
        let item = [];
        for(let key=1;key<=this.props.employees.allEmployees.count;key++){
            item.push(
                <li class="page-item"><a class="page-link" href="#">{key}</a></li>
            )
        }
        return item;
    }
    render(){
        return (
            <Fragment>
                
                {
                    (this.props.employees && this.props.employees.allEmployees &&this.props.employees.allEmployees.response)?
                    <div className="sub-header col-sm-12 mt-3">
                        <div className="row">
                            <div className="col-sm-12">
                                <div className="bx-content shadow">
                                    <div className="form-group p-4">        
                                        <span><b>{this.props.employees && this.props.employees.allEmployees &&this.props.employees.allEmployees.response.length}</b> Employees Found</span>                               
                                        <span className="float-right"></span>               
                                    </div>

                                
                                    <div className="scroll-wrapper table-responsive-cs" style={{position: "relative"}}>
                                        <div className="table-responsive-cs scroll-content" style={{height: "auto", marginBottom: "-10px", marginRight: "-10px", maxHeight: "373.817px"}}>
                                            <table className="table table-cs text-grey table-text-sm table-striped table-borderless ">
                                                <thead>
                                                    <tr>
                                                    <th>EMPLOYEE NAME </th>
                                                    <th>DESIGNATION</th>
                                                    <th>DEPARTMENT</th>
                                                    <th>BRANCH</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    {
                                                        this.props.employees.allEmployees.response.map((employee,index)=>(
                                                         <tr key={index}>
                                                            <td>
                                                                <span className="user-img-holder">{employee.first_name.charAt(0).toUpperCase()}{employee.last_name.charAt(0).toUpperCase()}</span>
                                                                <span className="project-details">
                                                                <span className="project-details-title"><Link  to={`/employees/employee/${employee.slug}`} className="text-black">
                                                                {employee.first_name} {employee.last_name}</Link></span>
                                                                <div style={{display:"block"}} className="project-details-user">{employee.email}</div>
                                                                </span>
                                                            </td>
                                                            <td>{(employee.designation&&employee.designation.designation_name)&&employee.designation.designation_name}</td>
                                                            <td>{(employee.department&&employee.department.department_name)&&employee.department.department_name}</td>
                                                            <td>{(employee.branch&&employee.branch.branch_name)&&employee.branch.branch_name}</td>
                                                        </tr>

                                                        ))
                                                   
                                                    }                                   
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <th colSpan="6" className="text-center p-4"></th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        
                        </div>
                        
                        
                        <div>
                        {/* <nav aria-label="Page navigation example">
                                <ul class="pagination">
                                    {(this.props.employees&&this.props.employees.allEmployees&&
                                        this.props.employees.allEmployees.previous===null)?
                                        <li  class="page-item disabled"><a class="page-link" href="#">Previous</a></li>:
                                        <li  class="page-item"><a class="page-link" href="#">Previous</a></li>
                                        }
                                    
                                    {this.props.employees.allEmployees.count&&
                                    this.renderPagination()
                                    }
                                    {(this.props.employees&&this.props.employees.allEmployees&&
                                    this.props.employees.allEmployees.next===null)?
                                    <li  class="page-item disabled"><a class="page-link" href="#">Next</a></li>:
                                    <li  class="page-item"><a class="page-link" href="#">Next</a></li>
                                    }
                                    
                                </ul>
                        </nav>    */}
                        
                        </div> 
                    </div>

                :
                <div className="mt-3 col-sm-12">
                    <div className="row">
                        <div className="col-sm-12">
                            <div className="bx-content shadow">
                                <div className="form-group p-4">
                                    <span>No Employees Found</span>               
                                </div>                   
                            </div>
                        </div>
                    </div>
                </div>
            }
            </Fragment>
        )
    }
}

export default OrganizationEmployeeContent