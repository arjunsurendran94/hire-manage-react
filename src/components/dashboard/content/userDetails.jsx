import React, { Component,Fragment } from 'react';
import { Form, Button } from 'semantic-ui-react';

class UserDetails extends Component{

    saveAndContinue = (e) => {
        e.preventDefault()
        this.props.nextStep()
    }

    render(){
        const { values } = this.props;
        return(
            
                <Fragment>
                  <div className="sub-header" style={{textAlign:"center",marginTop:"20px"}}>
                    <div className="col-sm-12 form-group ">
                        {/* <div style={{display:"flex",width:"500px"}}> */}
                <input type="radio" className="individual-button" checked={values.select_internal} name="projectinfo" onClick={this.props.select_internal} value="Internal"/>
                  <label for="internal" className="individual">Internal Project</label>
                  
                 
                  <input type="radio"  name="projectinfo" checked={values.select_client} className="individual-button" onClick={this.props.select_client} value="Client"/>
                  <label for="client" className="individual" >Client Project</label></div>
                  {/* </div> */}
                  <div className="col-sm-12 form-group">
                                    <button type="button" style={{width:"100px"}} className="btn btn-outline-primary add-row" onClick={this.saveAndContinue}>Next </button>
                </div>
                </div>
                </Fragment>
        
        )
    }
}

export default UserDetails;