import React, {Component, Fragment} from 'react'
import ViewDashboardContent from './viewDashboardContent'
class DashBoardContent extends Component{
    render(){
        return (
            <Fragment>
                <div className="col-sm-12 mt-3 sub-header">
                    <div className="col-sm">
                        <div className=" justify-content-center">
                            <div className="align-self-center text-center">
                            {/* <h1>Welcome to Dashboard</h1> */}
                            <ViewDashboardContent {...this.props} /> 
                            </div>
                        </div>
                    </div>
                </div>
            </Fragment>
        )
    }
}

export default DashBoardContent