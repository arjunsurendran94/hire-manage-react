<div
className="tab-pane userMail-tab fade show active text-grey"
id="activemytask"
role="tabpanel"
aria-labelledby="active-mytask"
>
<div className="position-main">
<table style={{textAlign:"center"}} class="table table-cs text-grey table-text-sm table-striped table-borderless">
        <thead>
<tr>
    <th>Task Name</th>
    {!this.state.openNav &&(<th>Status</th>)}
    {!this.state.openNav &&(<th>Assignee</th>)}
    {!this.state.openNav &&(<th>Due Date</th>)}
   
</tr>
        </thead>
        <tbody>
        {this.props.tasks &&
this.props.tasks.mytasks &&
this.props.tasks.mytasks.length > 0 ? (
    this.props.tasks.mytasks.filter(taskObj => taskObj. task_board === "ongoing"||taskObj. task_board ==="todo"). map(
    // this.props.tasks.mytasks.filter(taskObj => taskObj.task_board === ("cancelled"||"done")). map(
(taskObj,index) => (
<tr>
    <td><i
      style={{ margin: "0px 10px" }}
      class="fa fa-check-circle-o"
      aria-hidden="true"
    ></i>{taskObj.task_name}
    </td>
    <td>{this.formatString(
          taskObj && taskObj.task_board
        )}</td>
    <td><div className="user-img-holder  position-middle">
        {taskObj &&
          taskObj.assigned &&
          taskObj.assigned.first_name.charAt(0)}
        {taskObj &&
          taskObj.assigned &&
          taskObj.assigned.last_name.charAt(0)}
      </div>
      {taskObj &&
          taskObj.assigned &&
          taskObj.assigned.first_name}
        {taskObj &&
          taskObj.assigned &&
          taskObj.assigned.last_name}</td>
   <td> {taskObj.due_date}</td>
</tr>

))
) : (
<div className="position-main ">
No Tasks Available
</div>
)}
        </tbody>
    </table>
<div
style={{ position: "absolute", lineHeight: "35px" }}
>
Task Name
</div>
{!this.state.openNav && (
<div className="status-list">Status</div>
)}
{!this.state.openNav && (
<div className="line-sub">Assignee</div>
)}
{!this.state.openNav && (
<div className="position-sub">Due Date</div>
)}
</div>
{/* MY TASKS */}
{this.props.tasks &&
this.props.tasks.mytasks &&
this.props.tasks.mytasks.length > 0 ? (
this.props.tasks.mytasks.filter(taskObj => taskObj. task_board === "ongoing"||taskObj. task_board ==="todo"). map(
(taskObj,index) => (

<div className="position-main ">
  <div
    style={{
      position: "absolute",
      lineHeight: "35px",
    }}
  >
    <i
      style={{ margin: "0px 10px" }}
      class="fa fa-check-circle-o"
      aria-hidden="true"
    ></i>
    <Link
      onClick={() =>
        this.openNav("view_task", taskObj)
      }
    >
      {taskObj.task_name}
    </Link>
  </div>

  {!this.state.openNav && (
    <Fragment>
      <div className="status-list">
        {this.formatString(
          taskObj && taskObj.task_board
        )}
      </div>{" "}
      {console.log("taskobj", taskObj.task_board)}
      <div className="user-img-holder  position-middle">
        {taskObj &&
          taskObj.assigned &&
          taskObj.assigned.first_name.charAt(0)}
        {taskObj &&
          taskObj.assigned &&
          taskObj.assigned.last_name.charAt(0)}
      </div>
      <div className="position-name">
        {taskObj &&
          taskObj.assigned &&
          taskObj.assigned.first_name}
        {taskObj &&
          taskObj.assigned &&
          taskObj.assigned.last_name}
      </div>
      <div className="position-sub">
        {taskObj.due_date}
      </div>
    </Fragment>
  )}
</div>
))
) : (
<div className="position-main ">
No Tasks Available
</div>
)}
</div>