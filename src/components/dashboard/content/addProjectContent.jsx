import React, { Component, Fragment } from 'react'
import UserDetails from './userDetails';
import PersonalDetails from './personalDetails';
import Confirmation from './confirmation';
import Success from './success';
import { connect } from 'react-redux'
import { Field, FieldArray, reduxForm, formValueSelector } from 'redux-form'
import { Redirect } from 'react-router-dom';
import { alphabetOnly } from '../../../constants/validations'
import * as Helper from '../common/helper/helper';
import LoaderImage from '../../../static/assets/image/loader.gif';
import Select from 'react-select';
import DatePicker from "react-datepicker";
import UpdateClientModal from '../modal/addClientProjectModal'
import EmployeeStatusModal from '../modal/employeeStatusModal';
import Terms from './terms';
var loaderStyle = {
   width: '3%',
   position: 'fixed',
   top: '50%',
   left: '57%'
}

var userList = [];
var employeeList = [];
var clientList = []

var setContract=true

const required = value => {
   return (value || typeof value === 'number' ? undefined : ' is Required')
}
const positiveInteger = value => {
   var positiveregex = /^\s*\d*\s*$/
   return value && positiveregex.test(value) ? undefined : ' is Invalid'
}
const mobileValid = value => {
   var mobilenoregex = /^(?!\b(0)\1+\b)(\+?\d{1,3}[. -]?)?\(?\d{3}\)?([. -]?)\d{3}\3\d{4}$/
   if (value && value.length >= 1) {
      return (mobilenoregex.test(value) ? undefined : ' is Invalid')
   }
   return undefined
}
const email = value =>
   value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
      ? ' is Invalid'
      : undefined
const websiteValiadtion = value => {
   if (value) {
      var pattern = new RegExp('^(https?:\\/\\/)' + // protocol
         '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
         '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
         '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
         '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
         '(\\#[-a-z\\d_]*)?$', 'i'); // fragment locator
      return !!pattern.test(value) ? undefined : 'Enter a valid url ( eg:https://websitebuilders.com )'
   }
}

const currency = [{ value: "abc", label: "asdf" }, { value: "wer", label: "uji" }]



const renderTextField = ({
   input,
   label,
   type,

   meta: { touched, error, warning, }
}) => {
   return (
      <Fragment>
         <textarea {...input} placeholder={label} type={type} className={(touched && error

            ? 'fullwidth input-control border border-danger' : 'fullwidth input-control')} />
         {touched &&
            ((error && <small className="text-danger">{label}{error}</small>) ||
               (warning && <span>{warning}</span>))}

      </Fragment>
   )
}



const renderField = ({
   input,
   label,
   type,
   existed,
   work_phone_number_error,
   mobile_number_error,
   website_error,
   facebook_error,
   pin_code_error,
   meta: { touched, error, warning, }
}) => {
   return (
      <Fragment>
         <input {...input} placeholder={label} type={type} className={(touched && error) ||
            (existed && (label === 'Contact Email')) || (work_phone_number_error) || (mobile_number_error) || (website_error) || (facebook_error) || (pin_code_error)
            ? 'fullwidth input-control border border-danger' : 'fullwidth input-control'} />
         {touched &&
            ((error && <small className="text-danger">{label}{error}</small>) ||
               (warning && <span>{warning}</span>))}
         {(label === 'Contact Email') && (existed) && <small class="text-danger">
            {existed}
         </small>}
         {(work_phone_number_error) && <small class="text-danger">
            {work_phone_number_error}
         </small>}
         {(mobile_number_error) && <small class="text-danger">
            {mobile_number_error}
         </small>}
         {(website_error) && <small class="text-danger">
            {website_error}
         </small>}
         {(facebook_error) && <small class="text-danger">
            {facebook_error}
         </small>}
         {(pin_code_error) && <small class="text-danger">
            {pin_code_error}
         </small>}
      </Fragment>
   )
}

const radioCheckbox = ({ input, label, options, type, checked, meta: { touched, error, warning } }) => (
   <Fragment>
      {options.map((plan, index) => (
         <div className="col-md-3" key={index}>
            <input type="radio" {...input} checked={checked} key={index} name={plan.name} value={plan.value} id={plan.id} /> {plan.text}
         </div>
      ))
      }
      {touched &&
         ((error && <small className="text-danger">{label}{error}</small>) ||
            (warning && <span>{warning}</span>))}
   </Fragment>
)
const renderCheckBoxField = ({ input, label, options, type, checked, meta: { touched, error, warning } }) => (
   <Fragment>
      <input type="checkbox" {...input} />
   </Fragment>
)

const renderDateField = ({ input, label, options, type, startingDate, checked, meta: { touched, error, warning } }) => (
   <Fragment>

      <input {...input} type="date"
         name="start_date"
         className="fullwidth input-control"
         label="Start Date"
         id="myDate"
      

      />

      {touched &&
         ((error && <small className="text-danger">{label}{error}</small>) ||
            (warning && <span>{warning}</span>))}
   </Fragment>
)

const renderSelectField = ({
   input,
   label,
   type,
   id,
   existed,
   options,
   country,
   countryState,
   city,
   onBlur,
   meta: { touched, error, warning, }
}) => {
   const handleBlur = e => e.preventDefault();
   return (
      <React.Fragment>
         <Select {...input} options={options} onChange={input.onChange}
            onBlur={handleBlur} />
      </React.Fragment>

   )
}


const renderResourceField = ({ fields, fixed_rate, options, meta: { error, submitFailed } }) => (
   <div>
      <li style={{ marginTop: "16px" }}>

         <tr className="dynamic-form-add">
            <td colspan="2">
               <a className="add-row" href="javascript:void(0)">
                  <div className="row">
                     <div className="col-sm-12 text-center">
                        <button type="button" style={{ width: "100px" }} className="btn btn-outline-primary add-row" onClick={() => fields.push({})}>
                           <i className="icon-plus"></i> Add </button>
                     </div>
                  </div>
               </a>
            </td>
         </tr>

         {/* {submitFailed && error && <span>{error}</span>} */}
      </li>



      {fields.map((member, index) => (
         <li key={index}>
            {/* <button
           type="button"
           title="Remove Member"
           onClick={() => fields.remove(index)}
         /> */}
            {/* <tr className="contract_formset_row dynamic-form">
                        <td className="">
                           <input type="file" name="contract_formset_set-0-contract" className="form-control" id="id_contract_formset_set-0-contract"/>
                           <div className="form-control-error-list">
                           </div>
                        </td>
                        <td>
                           <input type="hidden" name="contract_formset_set-0-DELETE" id="id_contract_formset_set-0-DELETE"/>
                           <a onClick={() => fields.remove(index)} className="btn btn-outline-primary delete-row" href="javascript:void(0)"><i class=" icon-delete"></i> Remove</a>
                        </td>
                     </tr> */}

            <tr >
               <Fragment>
                  <div className="row financial-content">
                     <div className="col-sm-3 form-group">
                        <label className="text-light-grey" for="id_rate">Rate</label>
                        <Field type="text"

                           name={`${member}.hourly_rate`}
                           validate={[required, positiveInteger]}
                           component={renderField}
                           label="Hourly Rate"

                        />

                     </div>

                     {!fixed_rate &&
                        <div className="col-sm-3 form-group">
                           <label className="text-light-grey" for="id_weekly_limit">Weekly limit</label>
                           <Field type="text"
                              name={`${member}.weekly_limit`}
                              component={renderField}
                              label="Weekly limit"

                           />
                        </div>}

                     <div className="col-sm-4">

                        <label for="id_client" className="text-light-grey" style={{ marginBottom: "10px" }}>
                           Add Resource
              </label>

                        <Field type="text"
                           name={`${member}.assign_resource`}
                           component={renderSelectField}
                           label="Assign Resource"
                           options={options}
                           value="this.state.accountno"
                           validate={required}
                        />


                     </div>
                     <div className="col-sm-2 form-group text-center">
                        <a className="btn btn-outline-primary delete-row" style={{ marginTop: "27px" }} onClick={() => fields.remove(index)} href="javascript:void(0)"><i class=" icon-delete"></i> Remove</a>
                     </div>
                  </div>
               </Fragment>
            </tr>




         </li>
      ))}
   </div>
)

const renderContractAttachmentField = ({ fields, meta: { error, submitFailed } }) => (
   <ul>
      <li>
         <tr className="dynamic-form-add">
            <td colspan="2">
               <a className="add-row" href="javascript:void(0)">
                  <div className="row">
                     <div className="col-sm-12 text-center">
                        <button type="button" className="btn btn-outline-primary add-row" onClick={() => fields.push({})}>
                           <i className="icon-plus"></i> Add Attachment</button>
                     </div>
                  </div>
               </a>
            </td>
         </tr>

         {/* {submitFailed && error && <span>{error}</span>} */}
      </li>



      {fields.map((member, index) => (
         <li key={index}>
            {/* <button
           type="button"
           title="Remove Member"
           onClick={() => fields.remove(index)}
         /> */}
            <tr className="contract_formset_row dynamic-form">
               <td className="">
                  <input type="file" name="contract_formset_set-0-contract" className="form-control" id="id_contract_formset_set-0-contract" />
                  <div className="form-control-error-list">
                  </div>
               </td>
               <td>
                  <input type="hidden" name="contract_formset_set-0-DELETE" id="id_contract_formset_set-0-DELETE" />
                  <a onClick={() => fields.remove(index)} className="btn btn-outline-primary delete-row" href="javascript:void(0)"><i class=" icon-delete"></i> Remove</a>
               </td>
            </tr>




         </li>
      ))}
   </ul>
)


// var numberOnly = /^[0-9]*$/

const re = /^[0-9\b]+$/;
let allClients=[]

class AddProjectContent extends Component {
   constructor(props) {
      super(props);
      var date = new Date();


      var formatedDate = `${date.getFullYear()}-${("0" + (date.getMonth() + 1)).slice(-2)}-${("0" + date.getDate()).slice(-2)}`;
      this.state = {
         show_branch: false,
         success_message: false,
         existed_error: '',
         other_details: false,
         checked: false,
         isOpen: false,
         emailValue: '',
         passwordValue: '',
         select_hourly: true,
         select_fixed: false,
         select_client: true,
         select_internal: false,


         select_billable: [],
         


         editModal: false,
         editPart: '',
         index: '',

         project_name: "",
         start_date: formatedDate,
         end_date: '',
         client: '',
         project_cost: '',
         total_hours:'',
         estimated_hours: '',
         project_description: '',
         todays_date:formatedDate,

         attachments: [],
         termAttachments: [],
         contractAttachments: [],
         resourcesAssigned: [],
         showRestDetails:[],
         detail_text:'',
         errors: {
            project_name: '',

            start_date: '',
            end_date: '',
            project_cost: '',
            client: '',
            estimated_hours: '',
            project_description: '',
            selectedType:''
            
         },
         designationSelected: '',
         renderOnce: true,
         selectedType:'',
         hourly_rate:'',
         fixed_contract:null,
         fixed_contract_error:'',

         selectedOption: null,
         step: 1,
         projectType: '',
         firstName: '',
         lastName: '',
         email: '',
         age: '',
         city: '',
         country: '',
         clientList: [],
         employees:[],
         resource:'',
         resourceError:[]
      };
   }

   

   clearExistedError = (params, e) => {
      params === 'email' && this.setState({ existed_error: '', emailValue: e })
      params === 'work_phone' && this.setState({ work_phone_number_error: '' })
      params === 'mobile_number' && this.setState({ mobile_number_error: '' })
      params === 'website' && this.setState({ website_error: '' })
      params === 'facebook' && this.setState({ facebook_error: '' })
      params === 'pin_code' && this.setState({ pincodeError: '' })
   }
   select_client = () => {
      this.setState({ select_client: true, select_internal: false })
   }
   select_internal = () => {
      this.setState({ select_internal: true, select_client: false })
   }
   select_billable = (id) => {
      
      let select_billable=[...this.state.select_billable]
      select_billable[id]={ select_billable: true, select_nonbillable: false }

      let resourcesAssigned=[...this.state.resourcesAssigned]
      resourcesAssigned[id].resource_billing_type='billable'
      resourcesAssigned[id].tracker=true
      
      let contractAttachments=[...this.state.contractAttachments]
      contractAttachments.splice(id,0,{
         contract_title:'',contract_title_error:'',contract:null,contract_name:'',contract_error:''})

      let termAttachments=[...this.state.termAttachments]
      termAttachments.splice(id,0,{termAttachment:null,error:'',name:''})

      this.setState({
         select_billable:select_billable,resourcesAssigned:resourcesAssigned,contractAttachments:contractAttachments,
         termAttachments:termAttachments
      })
   }
   select_nonbillable = (id) => {
      let select_billable=[...this.state.select_billable]
      select_billable[id]={ select_billable: false, select_nonbillable: true }

      let resourcesAssigned=[...this.state.resourcesAssigned]
      resourcesAssigned[id].resource_billing_type='non_billable'
      resourcesAssigned[id].tracker=false
      resourcesAssigned[id].hourly_rate_error=''
      resourcesAssigned[id].weekly_limit_error=''

      let contractAttachments=[...this.state.contractAttachments]
      contractAttachments.splice(id,1)

      let termAttachments=[...this.state.termAttachments]
      termAttachments.splice(id,1)

      this.setState({
         select_billable:select_billable,resourcesAssigned:resourcesAssigned,contractAttachments:contractAttachments,
         termAttachments:termAttachments
      })
   }
   select_hourly = () => {
      let errors={...this.state.errors}
      errors.project_cost=''
      this.setState({ select_hourly: true, select_fixed: false,errors:errors })
   }
   select_fixed = () => {
      let errors={...this.state.errors}
      errors.estimated_hours=''
      this.setState({ select_fixed: true, select_hourly: false,errors:errors })
   }
   closeSuccessMessage = () => {
      this.setState({
         success_message: false
      })
   }
   updateClientModal = () => {
      this.setState({ editModal: true })
   }
   closeEditModal = () => {
      this.setState({ editModal: false })
   }

   handleclientChange = selectedOption => {
      
      let fullErrors=this.state.errors
      fullErrors['client']=''
      this.setState({ selectedOption: selectedOption, client: selectedOption.value ,
         errors:fullErrors});

   };
   handleTypeChange = selectedType => {
      

      let fullErrors=this.state.errors
      if(selectedType.value==='hourly'){fullErrors['end_date']=''}
      fullErrors['selectedType']=''
      
      this.setState({ selectedType: selectedType,typeValue:selectedType.value,
         errors:fullErrors});

   };

   handleresourceChange = (selectedOption,id) => {
      let showRestDetails=[...this.state.showRestDetails]
      showRestDetails=showRestDetails.map((element,index) => index == id ? {...element, showDetail : true} : element);

      let resourceError=[]
      let resourcesAssigned=[...this.state.resourcesAssigned]

      // let resourcesFiltered=resourcesAssigned.filter((i)=>i.value!=selectedOption.value)
      // let employeeList=[...this.state.employees]
      // if(resourcesFiltered.length!=resourcesAssigned.length){
      //    let obj=resourcesAssigned.filter((i)=>i.value==selectedOption.value)
         
      //    employeeList.push(obj)
      // }
      resourcesAssigned.map((obj,index)=>obj.value===selectedOption.value ? resourceError[id]='Already exist':resourceError[id]='')
      let existed = resourceError.some(function (el) {
         return el === 'Already exist';
       });

      if(!existed){
         let res=resourcesAssigned[id]
         resourcesAssigned[id]={...res,...selectedOption}
         this.setState({showRestDetails:showRestDetails,resourcesAssigned:resourcesAssigned,resourceError:resourceError})
      }
      else{
         this.setState({resourceError:resourceError})
      }
   };

   handleBranchChange = (e) => {
      this.props.change('Department', '')
      this.props.retrieveDepartments(e.target.value);
   }
   upLoadFile = (e) => {
      let image = e.target.files[0]
      var imageTypes = ['image/png', 'image/jpeg', 'image/gif']
      var image_index = imageTypes.indexOf(image.type)
      if (image_index >= 0) {
         this.setState({ image: image, image_validation_error: false }, () => { console.log(this.state.image, 'uploaded image') })
      }
      else {
         this.setState({ image: null, image_validation_error: true })
      }
   }
   handleCountryChange = (e) => {
      // making fields null
      this.props.change('state', '')
      this.props.change('city', '')
      this.props.fetchStatesList(e.target.value);
      // city error resolves after u uncomment it 
      this.props.fetchCityList(" ");
   }
   errorOnChange = (param) => {
      if (param === 'pincode') {
         this.setState({ pincodeError: '' })
      }
   }

   toggleEnablePortalModal = (e) => {
      e.target.checked ?
         this.setState({
            isOpen: !this.state.isOpen,
            checked: e.target.checked
         }) :
         this.setState({ checked: e.target.checked, passwordValue: '' })
   }

   closePortalModal = () => {
      this.setState({
         isOpen: false,
      });
   }
   handleStateChange = (e) => {
      // making fields null
      this.props.change('city', '')
      this.props.fetchCityList(e.target.value);
   }

   dateChange = (e) => {
      this.setState({ start_date: e.target.value ,end_date:''})
   }

   addAttachment = (params) => {
      params === 'attachments' && this.setState((prevState) => ({
         attachments: [...prevState.attachments, { attachment: '',error:'' }]
      }))
      // params === 'terms' && this.setState((prevState) => ({
      //    termAttachments: [...prevState.termAttachments, { termAttachment: '',error:'' }]
      // }))
      // params === 'contracts' && this.setState((prevState) => ({
      //    contractAttachments: [...prevState.contractAttachments, { contract: '',error:'' }]
      // }))
   }


   upLoadAttachment = (id, e, params) => {
      
      var fileTypes = ['image/png','image/jpg','image/gif','image/jpeg','application/pdf',
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        'application/zip'
      ]

      if (params === 'attachments') {
         let fullAttachments1=[]
         let file1 = e.target.files[0]
            
        
        
        let file_index = fileTypes.indexOf(file1.type)
        if (file_index >= 0){
         
         fullAttachments1 = this.state.attachments
         fullAttachments1[id].attachment = file1
         fullAttachments1[id].error = ''
      }
        else{
           
         fullAttachments1 = this.state.attachments
         fullAttachments1[id].attachment = ''
         fullAttachments1[id].error = 'Invalid file format'
         e.target.value=null
        }
        this.setState({
         attachments: fullAttachments1
      })

      }

      if (params === 'terms') {
         
         let file2 = e.target.files[0]
         let fullAttachments2 = this.state.termAttachments

         // fullAttachments2[id].attachment = file2

         let file_index = fileTypes.indexOf(file2.type)
        if (file_index >= 0){
         
         // fullAttachments2 = this.state.attachments
         fullAttachments2[id].termAttachment = file2
         fullAttachments2[id].error = ''
         fullAttachments2[id].name = file2.name
      }
        else{
           
         // fullAttachments2 = this.state.attachments
         fullAttachments2[id].termAttachment = null
         fullAttachments2[id].error = 'Invalid file format'
         fullAttachments2[id].name = ''
         e.target.value=null
      
        }

         this.setState({
            termAttachments: fullAttachments2
         })
      }
      if (params === 'contracts'&&this.state.typeValue!='fixed') {
         let file3 = e.target.files[0]
         let fullAttachments3 = [...this.state.contractAttachments]

         // fullAttachments3[id].contract = file3

         let file_index = fileTypes.indexOf(file3.type)
        if (file_index >= 0){
         
         // fullAttachments2 = this.state.attachments
         fullAttachments3[id].contract = file3
         fullAttachments3[id].error = ''
         fullAttachments3[id].contract_name = file3.name
      }
        else{
           
         // fullAttachments2 = this.state.attachments
         fullAttachments3[id].contract = null
         fullAttachments3[id].error = 'Invalid file format'
         fullAttachments3[id].contract_name = ''
         e.target.value=null
        }


         this.setState({
            contractAttachments: fullAttachments3
         })
      }

      if(params === 'contracts'&&this.state.typeValue==='fixed'){
         let file4 = e.target.files[0]

         let file_index = fileTypes.indexOf(file4.type)
        if (file_index >= 0){
         
         this.setState({fixed_contract:file4,fixed_contract_error:''})
      }
        else{
         
         e.target.value=null
         this.setState({fixed_contract:null,fixed_contract_error:'Invalid file format'})
        }
      }
      
   }

   removeAttachment = (id, params,e) => {
      
      if (params === 'attachments') {
         let fullAttachments = this.state.attachments
         fullAttachments.splice(id, 1)
         this.setState({ attachments: fullAttachments })
      }
      if (params === 'terms') {

         let fulltermAttachments = this.state.termAttachments
         fulltermAttachments[id].termAttachment=null
         fulltermAttachments[id].error=''
         fulltermAttachments[id].name=''
         this.setState({ termAttachments: fulltermAttachments })
      }
      if (params === 'contracts') {
         
         let fullcontractAttachments = this.state.contractAttachments
         fullcontractAttachments[id].contract=null
         fullcontractAttachments[id].error=''
         fullcontractAttachments[id].contract_name=''
         this.setState({ contractAttachments: fullcontractAttachments })
      }


   }
   
   setcontractState=(billObjList)=>{
      let contractAttachments=[]
      contractAttachments=[...this.state.contractAttachments]
      let termAttachments=[...this.state.termAttachments]

      // billObjList.map((obj)=>contractAttachments.push({
      //    contract_title:'',contract_title_error:'',contract:null,contract_name:'',contract_error:''
      // }),
      
      // )
      // billObjList.map((obj)=>termAttachments.push({termAttachment:null,error:'',name:''}))
      // console.log('contractAttachmentsssssssss',contractAttachments);
      
      this.setState((prevState)=>({
         contractAttachments:[...prevState.contractAttachments,{
            contract_title:'',contract_title_error:'',contract:null,contract_name:'',contract_error:''
         }],
         termAttachments:[...prevState.termAttachments,{termAttachment:null,error:'',name:''}]
            }))

      setContract&&this.setState({contractAttachments:contractAttachments,termAttachments:termAttachments})
      setContract=false
   }


   // addResource =(e)=>{
   //    this.setState((prevState)=>({
   //       resourcesAssigned:[...prevState.resourcesAssigned,{hourly_rate:'',weekly_limit:'',resource_assigned:''}]
   //    }))
   // }

   // removeResource=(id)=>{
   //    let fullResources=this.state.resourcesAssigned
   //    fullResources.splice(id,1)
   //    this.setState({resourcesAssigned:fullResources})
   // }

   // handleHourlyRate=(e)=>{
   //    console.log('name,value',e.target.name,e.target.value)
   //    let name=e.target.name
   //    let value=e.target.value

   //    this.setState((prevState)=>({
   //       resourcesAssigned:[...prevState.resourcesAssigned,{hourly_rate:e.target.value,weekly_limit:'',resource_assigned:''}]
   //    }))

   //    // this.setState({resourcesAssigned:{'hourly_rate':e.target.value}})

   // }

   // handleWeeklyLimit=(e)=>{
   //    this.setState({resourcesAssigned:{'weekly_limit':e.target.value}})
   // }

   componentDidUpdate(prevProps, prevState) {

      if (this.props.clientData && this.props.clientData.status === 'success' && this.props.clientData != prevProps.clientData &&
         this.props.clientData.client_obj != '') {
         this.setState({ editModal: false })

         if(this.props.clientData && this.props.clientData.client_obj){
            
            this.setState(prevState=>({
               selectedOption: {
                  value: this.props.clientData.client_obj.client.slug,
                  label: this.props.clientData.client_obj.client.first_name + ' ' + this.props.clientData.client_obj.client.last_name
               },
               client:this.props.clientData.client_obj.client.slug,
               errors:{
               ...prevState.errors,
               client:''}
            }))

         }
      }

      if (this.props.clientData != prevProps.clientData ) {

         let ss = this.props.clientData && this.props.clientData.clients && this.props.clientData.clients.map((obj) => { return ({ value: obj.client.slug, label: obj.client.first_name + ' ' + obj.client.last_name }) }
         )
         this.setState({ clientList: ss })

      }
      if((this.props.employees)!=
      (prevProps.employees)  
      &&employeeList&&employeeList.length==0){
         
         this.props.employees&&this.props.employees.allEmployees&&this.props.employees.allEmployees.response&&
         this.props.employees.allEmployees.response.filter(obj=>obj.is_owner==false).map((emp)=>employeeList.push(
                  {name:"user",value:emp.slug,label:emp.first_name+' '+emp.last_name,
                  designation_name:emp.designation.designation_name,designation_slug:emp.designation.slug
                 
                 }
                 ))


         
         this.setState({employees:employeeList})
      }


      // if(this.props&&this.props.resourcesAdded&&this.props.resourcesAdded!=prevProps.resourcesAdded){

      //    let rf=[],aaa=[]
      //    for (var i=0;i<this.props.resourcesAdded.length;i++){
      //       if(Object.keys(this.props.resourcesAdded[i]).length != 0){
      //          rf.push(this.props.resourcesAdded[i].assign_resource)

      //          aaa=userList.filter(e => !rf.includes(e))
      //          userList=aaa

      //       }
      //    }
      //    console.log('rffffffffffffffffffffffffff',rf)


      //    console.log('USERLISTTTTTTTTTTTTTTTTTT',aaa);

      //    this.setState({resourcesAssigned:rf})
      // }

      // if(employeeList&&this.state.renderOnce){
      //    console.log('renderOnce');

      //    employeeList&&employeeList.map((emp)=>userList.push(
      //       {name:"user",value:emp.slug,label:emp.first_name+' '+emp.last_name,
      //       designation_name:emp.designation.designation_name,designation_slug:emp.designation.slug,
      //      billing:true,timetracking:true
      //      }
      //      ))
      //      this.setState({renderOnce:false})
      // }
   }

   SetDesignation = (e) => {

      this.setState({ designationSelected: e.value })
   }

   trackerOnChange=(id)=>{
      let resourcesAssigned=[...this.state.resourcesAssigned]
      // let tracker=resourcesAssigned[id].tracker
      resourcesAssigned[id].tracker=!(resourcesAssigned[id].tracker)

      this.setState({resourcesAssigned:resourcesAssigned})
   }

   addResource = () => {
      
      let arrayLength
      arrayLength=this.state.resourcesAssigned.length
      // (arrayLength==0||(this.state.resourcesAssigned[arrayLength-1].value!=''))&&


      if(arrayLength==0){
         let employeeList=[]
         this.props.employees&&this.props.employees.allEmployees&&this.props.employees.allEmployees.response&&
         this.props.employees.allEmployees.response.filter(obj=>obj.is_owner==false).map((emp)=>employeeList.push(
                  {name:"user",value:emp.slug,label:emp.first_name+' '+emp.last_name,
                  designation_name:emp.designation.designation_name,designation_slug:emp.designation.slug
                 
                 }
                 ))

         this.setState((prevState) => ({
            resourcesAssigned: [...prevState.resourcesAssigned, 
               {name:'',value:'',label:'',
                     designation_name:'',designation_slug:'',hourly_rate:'',weekly_limit:'',tracker:true,
                     resource_billing_type:'billable' , hourly_rate_error:'',weekly_limit_error:''            
               }],
            select_billable:[...prevState.select_billable,
               { select_billable: true, select_nonbillable: false }],
            showRestDetails:[...prevState.showRestDetails,
            {showDetail:false}],
            employees:employeeList,
            contractAttachments:[...prevState.contractAttachments,{
               contract_title:'',contract_title_error:'',contract:null,contract_name:'',contract_error:''
            }],
            termAttachments:[...prevState.termAttachments,{termAttachment:null,error:'',name:''}]
            
         }))
      }
      // length greater than 
      else if(this.state.resourcesAssigned[arrayLength-1].value!=''){
         
         let employeeList=[...this.state.employees]
         let resourcesAssigned=[...this.state.resourcesAssigned]

         let employeesFiltered = employeeList.filter((el) => {
            return resourcesAssigned.every((f) => {
              return f.value != el.value 
            });
          });
         
         this.setState((prevState) => ({
            resourcesAssigned: [...prevState.resourcesAssigned, 
               {name:'',value:'',label:'',
                     designation_name:'',designation_slug:'',hourly_rate:'',weekly_limit:'',tracker:false,
                     resource_billing_type:'billable'     

               }],
            select_billable:[...prevState.select_billable,
               { select_billable: true, select_nonbillable: false }],
            showRestDetails:[...prevState.showRestDetails,
            {showDetail:false}],
            employees:employeesFiltered,
            contractAttachments:[...prevState.contractAttachments,{
               contract_title:'',contract_title_error:'',contract:null,contract_name:'',contract_error:''
            }],
            termAttachments:[...prevState.termAttachments,{termAttachment:null,error:'',name:''}]
         }))
      }
      else{
         let resourceError=[]
         resourceError[arrayLength-1]='Select Resource'
         this.setState({
            resourceError:resourceError
         })
      }

   }

   removeResource =(id)=>{
      
      let fullReources = this.state.resourcesAssigned
      let obj=fullReources[id]
      fullReources.splice(id, 1)
      
      if(obj.value){
         let employeeList=[...this.state.employees]
         employeeList.push(obj)
      }

      let select_billable=[...this.state.select_billable]
      select_billable.splice(id,1)
      let showRestDetails=[...this.state.showRestDetails]
      showRestDetails.splice(id,1)

      let contractAttachments=[...this.state.contractAttachments]
      contractAttachments.splice(id,1)
      let termAttachments=[...this.state.termAttachments]
      termAttachments.splice(id,1)


      // let employeeList=[...this.state.employees]
      // let resourcesAssigned=[...this.state.resourcesAssigned]

      // let employeesFiltered = employeeList.filter((el) => {
      //    return resourcesAssigned.every((f) => {
      //      return f.value != el.value 
      //    });
      //  });
            
      this.setState({ resourcesAssigned: fullReources ,
                     select_billable:select_billable,
                     showRestDetails:showRestDetails,
                     selectedValue:null,
                     employees:employeeList,
                     termAttachments:termAttachments,
                     contractAttachments:contractAttachments
                  })
      
   }



   nextStep = (param) => {
      const { step } = this.state
      if(param==='redirect'){
         this.setState({
            step: step + 2
         })  
      }
      else{this.setState({
         step: step + 1
      })}
   }

   prevStep = () => {
      const { step } = this.state
      this.setState({
         step: step - 1
      })
   }
   prevStepFinish = () => {
      const { step } = this.state
      this.setState({
         step: step - 2
      })
   }
   weeklyHourlyonChange=(e,index)=>{
      
      
      if (e.target.value === '' || re.test(e.target.value)) {
         // debugger
         let name=e.target.name
         let resourcesAssigned=[...this.state.resourcesAssigned]
         resourcesAssigned[index][name]=e.target.value
         resourcesAssigned[index][name+'_error']=''
         this.setState({resourcesAssigned: resourcesAssigned})
      }
   }

   validateHourlyWeekly=()=>{
      // debugger
      let resourcesAssigned=[...this.state.resourcesAssigned]
      
      let valid=true
      resourcesAssigned.forEach(function (i) {
         
         if(i.resource_billing_type==='billable'){
         if(i.hourly_rate===""){
            i.hourly_rate_error='Hourly rate is required'
            valid=false
         }
         if(i.weekly_limit===""){
            i.weekly_limit_error='Weekly limit is required'
            valid=false
         }}
         
       });
       
      this.setState({resourcesAssigned:resourcesAssigned})
      return valid
   }

   handleChange = input => event => {
      if (input === 'estimated_hours'||input === 'project_cost'||input === 'hourly_rate') {
         
         if (event.target.value === '' || re.test(event.target.value)) {
            let fullErrors=this.state.errors
            fullErrors[input]=''
            this.setState({ [input]: event.target.value ,
               errors:fullErrors,
            })
         }
         else {
            
            this.setState(prevState => ({
               errors: {                   // object that we want to update
                   ...prevState.errors,    // keep all other key-value pairs
                   [input]: ''       // update the value of specific key
               }
           }))
         }
        
          
      if (input === 'hourly_rate') {
         if (this.state.project_cost!==""&&event.target.value){
            let total_hours=(this.state.project_cost)/(event.target.value)
            this.setState({ 
               total_hours:total_hours
            })
         }
         else{
            this.setState({ 
               total_hours:''
            })
         }
      }
      if (input === 'project_cost') {
         if (this.state.hourly_rate!==""&&event.target.value){
            let total_hours=(event.target.value)/(this.state.hourly_rate)
            this.setState({ 
               total_hours:total_hours
            })
         }
         else{
            this.setState({ 
               total_hours:''
            })
         }
      }
      }
      else {
         
         
            let fullErrors=this.state.errors
         fullErrors[input]=''
         this.setState({ [input]: event.target.value ,
            errors:fullErrors,
         })
         
         
      }
   }

   handleContracts=(e,index)=>{
console.log("terms in",this.state.contractAttachments,e.target.name)
      let contractAttachments=[...this.state.contractAttachments]
           
      contractAttachments[index][e.target.name]=e.target.value
      contractAttachments[index].contract_title_error=''

      this.setState({contractAttachments:contractAttachments})
      
   }

   validateProjectForm = (params) => {
      // debugger
      let project_name_error, client_error, project_cost_error, hourly_rate_error,start_date_error, end_date_error, project_description_error,
         estimated_hours_error,contract_title_error,contract_error,selectedType_error = ''
console.log("hourly_rate",this.state.hourly_rate)
      if (!this.state.project_name) {
         project_name_error = 'Project Name is required'
      }
      if (this.state.select_client&&!this.state.client) {
         client_error = 'Client is required'
      }
      if (this.state.typeValue==="fixed" && !this.state.project_cost) {
         project_cost_error = 'Project cost is required'
      }
      if (this.state.typeValue==="fixed" && !this.state.hourly_rate) {
         hourly_rate_error = 'Hourly rate is required'
      }
      if (!this.state.start_date) {
         start_date_error = "Start Date is required"
      }
      if (this.state.typeValue==="fixed" && !this.state.end_date) {
         end_date_error = 'End Date is required'
      }
      if (!this.state.project_description) {
         project_description_error = 'Project Description is required'
      }
      // if (this.state.select_hourly&&!this.state.estimated_hours) {
      //    estimated_hours_error = 'Estimated hours is required'
      // }
      if(!this.state.select_internal&&!this.state.selectedType){
         selectedType_error='Project type is required'
      }

      
      if (project_name_error || client_error || project_cost_error ||hourly_rate_error|| end_date_error || project_description_error
         || start_date_error || contract_title_error || contract_error ||estimated_hours_error||selectedType_error
      ) {
         const errors = {
            ...this.state.error,
            project_name: project_name_error,
            client: client_error,
            project_cost: project_cost_error,
            hourly_rate:hourly_rate_error,
            start_date: start_date_error,
            end_date: end_date_error,
            project_description: project_description_error,
            estimated_hours: estimated_hours_error,
            selectedType:selectedType_error
         }
         this.setState({ errors: errors })
                  
         return false
      }
      
      else{
         if(params==='create'){
            this.submit1()
         }
         return true
      }
   }

   

   validateContract=()=>{
      // debugger
      let contractAttachments=[...this.state.contractAttachments]
      let termAttachments=[...this.state.termAttachments]
      let valid=true
      contractAttachments.forEach(function (i) {
         // debugger
         if(i.contract_title===""){
            i.contract_title_error='Contract Title is required'
            valid=false
         }
         // if(i.contract===null){
         //    i.error='Contract is required'
         //    valid=false
         // }
         if(i.contract_error!=""){
            valid=false
         }
       });
       termAttachments.forEach(function(j){
          if(j.error!=''){
             valid=false
          }
       })
      
      this.setState({contractAttachments:contractAttachments})
      // console.log('contractAttachments',contractAttachments);
      return valid
   }

   submit1() {
      
      let data={}
      data.organization = localStorage.getItem('organization_slug')
      data.branch = ''
      data.department = null
      data.project_name=this.state.project_name
      
      data.client=this.state.client
      if(this.state.detail_text!=''){
      data.detail_text=this.state.detail_text}

      if(this.state.select_client){data.project_type='client_project'}
      else{
         data.project_type='internal_project'
      }
      if(this.state.typeValue==="hourly"){data.billing_type='hourly'}
      else{
         data.billing_type='fixed'
      }
      data.start_date=this.state.start_date
      if(this.state.end_date!=''){data.end_date=this.state.end_date}

      data.project_description=this.state.project_description
      
      if(this.state.project_cost!=''){
         data.project_cost=this.state.project_cost
      }
      if(this.state.total_hours!=''){
         data.hourly_rate_fixed=this.state.total_hours
      }
      // if(this.state.estimated_hours!=''){data.estimated_hours=this.state.estimated_hours}
      
      // console.log(values)

      // values.branch = ''
      // values.department = null
      // values.billing_type = 'hourly'
      // values.project_type = 'internal_project'
      // values.client = ''
      // values.organization = localStorage.getItem('organization_slug')
      // if (values.resources) {
      //    delete values.resources;
      // }


      const form_data = new FormData();
      for (var key in data) {
         form_data.append(key, data[key]);
      }
      if(this.state.attachments.length>0){
         this.state.attachments.map(obj=>obj.attachment!=''&&form_data.append('attachment',obj.attachment))
      }

      if(this.state.resourcesAssigned.length>0){
         this.state.resourcesAssigned.map(obj=>obj.value!=''&&
         (form_data.append('resource_billing_type',obj.resource_billing_type),
         form_data.append('hourly_rate',obj.hourly_rate),
         form_data.append('weekly_limit',obj.weekly_limit),
         form_data.append('resource_assigned',obj.value),
         form_data.append('tracker',obj.tracker))
         )
      }

      if(this.state.typeValue!="fixed"&&this.state.termAttachments.length>0){
         this.state.termAttachments.map(obj=>obj.termAttachment!=null&&
         form_data.append('termAttachment',obj.termAttachment
         ))
      }

      if(this.state.typeValue!="fixed"){
      if(this.state.contractAttachments.length>0)
         {this.state.contractAttachments.map(obj=>obj.contract!=null?
         (form_data.append('contract',obj.contract),
         form_data.append('contract_title',obj.contract_title))
         :
         (form_data.append('contract_title',obj.contract_title))
         )}
      
      }

      if(this.state.typeValue==="fixed"){
         if(this.state.fixed_contract!=null){
            form_data.append('contract',this.state.fixed_contract)
            form_data.append('contract_title',this.state.project_name)
         }
         else{
         
         form_data.append('contract_title',this.state.project_name)
         }
      }
      
      this.props.addProject(form_data)

   }

   componentDidMount(){
      
      this.props.listEmployees('')
   }

   render() {

      let permissions=[]
   permissions=localStorage.getItem('permissions')
   if(permissions&&permissions.length>0){
      permissions=permissions.split(',')
   }
   let is_admin = localStorage.getItem('user_type')==='admin' 
   
      if(permissions&&permissions.length>0&&(!permissions.includes('add_project'))){
         return <Redirect
                to={{
                    pathname: '/projects/projects/'
                }}
            />
      }


      const typeList=[{label:"Hourly ",value:"hourly"},{label:"Fixed",value:"fixed"}]
      const { step } = this.state;
      const { project_name, startingDate, select_internal, select_client, editModal, select_fixed, select_hourly, add_resource, select_nonbillable, select_billable, attachments
         , termAttachments, contractAttachments, todays_date,start_date, end_date, estimated_hours,project_cost,resourcesAssigned ,selectedType,typeValue,project_description,hourly_rate,fixed_contract_error,total_hours} = this.state;
     
         const values = {
         project_name, startingDate, select_internal, select_client, editModal, select_fixed, select_hourly, add_resource, select_nonbillable, select_billable, attachments
         , typeValue,termAttachments, contractAttachments,todays_date, start_date, end_date, estimated_hours,project_cost,resourcesAssigned,selectedType,project_description,hourly_rate,
         fixed_contract_error,total_hours
      };

      
      
      if(this.props.projectData&&this.props.projectData.status==='createsuccess'){
         
         return <Redirect
                to={{
                    pathname: '/projects/projects/',
                     state:{status:'createsuccess'}
                }}
            />
         }
      
      if(this.props.projectData&&this.props.projectData.status==='error'){
           
            return <Redirect
                   to={{
                       pathname: '/projects/projects/',
                        state:{status:'error'}
                   }}
               />
            }
      

      switch (step) {
         case 1:
            return <UserDetails
               nextStep={this.nextStep}
               select_internal={this.select_internal}
               select_client={this.select_client}
               handleChange={this.handleChange}
               values={values}
               prevStep={this.prevStep}
            />
         case 2:
            return <PersonalDetails
               {...this.props}
               errors={this.state.errors}
               nextStep={this.nextStep}
               prevStep={this.prevStep}
               select_hourly={this.select_hourly}
               select_fixed={this.select_fixed}
               updateClientModal={this.updateClientModal}
               closeEditModal={this.closeEditModal}
               handleChange={this.handleChange}
               values={values}
               dateChange={this.dateChange}
               addAttachment={this.addAttachment}
               upLoadAttachment={this.upLoadAttachment}
               removeAttachment={this.removeAttachment}
               clientList={this.state.clientList}
               validateProjectForm={this.validateProjectForm}
               handleclientChange={this.handleclientChange}
               selectedOption={this.state.selectedOption}
               handleTypeChange={this.handleTypeChange}
               selectedType={this.state.selectedType}
               typeValue={this.state.typeValue}
               typeList={typeList}
               
            />
         case 3:
            return <Confirmation
               nextStep={this.nextStep}
               prevStep={this.prevStep}
               values={values}
               addResource={this.addResource}
               removeResource={this.removeResource}
               select_billable={this.select_billable}
               select_nonbillable={this.select_nonbillable}
               employees={this.state.employees}
               handleresourceChange={this.handleresourceChange}
               resources={this.state.resourcesAssigned}
               showRestDetails={this.state.showRestDetails}
               selectedValue={this.state.selectedValue}
               resourceError={this.state.resourceError}
               weeklyHourlyonChange={this.weeklyHourlyonChange}
               trackerOnChange={this.trackerOnChange}
               validateProjectForm={this.validateProjectForm}
               validateHourlyWeekly={this.validateHourlyWeekly}
               selectedType={this.state.selectedType}
               typeValue={this.state.typeValue}
            />
         case 4:
            return <Terms
               nextStep={this.nextStep}
               prevStep={this.prevStep}
               values={values}
               addAttachment={this.addAttachment}
               upLoadAttachment={this.upLoadAttachment}
               removeAttachment={this.removeAttachment}
               validateProjectForm={this.validateProjectForm}
               setcontractState={this.setcontractState}
               handleTerms={this.handleContracts}
               validateContract={this.validateContract}
               selectedType={this.state.selectedType}
               typeValue={this.state.typeValue}
            />
         case 5:
            return <Success
               nextStep={this.nextStep}
               prevStep={this.prevStepFinish}
               values={values}
               handleChange={this.handleChange}
               validateProjectForm={this.validateProjectForm}
               projectData={ this.props.projectData}
                
            />
      }
   }

}


export default AddProjectContent