import React, {Component, Fragment} from 'react'
import BranchDetailComponent from '../common/branchDetailComponent'
import { Link } from 'react-router-dom'
import Loader from '../../common/Loader'

class ClientList extends Component{
  
    render(){  
        console.log('ss',this.props)
        
        return ( 
            <div className='col-sm-12 mt-3 sub-header'>
                <table class="table table-cs text-grey table-text-sm table-striped table-borderless">
                    <thead>
                        <tr>
                            <th>CONTACT NAME</th>
                            <th>COMPANY NAME</th>
                            <th>COUNTRY</th>
                            <th>CONTACT NUMBER</th>
                            <th>SKYPE ID</th>
                            <th>WEBSITE</th>
                        </tr>
                    </thead>
                    <tbody>
                    {this.props.clients.map(clientObj=>
                        <tr>
                        <td data-title="CONTACT NAME">
                            <div className="position-relative">
                                <span className="user-img-holder clnt-lst12">
                                    {clientObj&&clientObj.client&&clientObj.client.first_name.charAt(0)}{clientObj&&clientObj.client&&clientObj.client.last_name.charAt(0)}
                                    </span>
                                <span className="project-details clnt-mr-lft">
                                    <span className="project-details-title">
                                        <Link 
                to={{pathname:`/clients/client/${clientObj&&clientObj.client&&clientObj.client.slug}`}} className="text-black">
                                    {clientObj&&clientObj.client&&clientObj.client.first_name} &nbsp;
                                    {clientObj&&clientObj.client&&clientObj.client.last_name}
                                    </Link>
                                    </span>
                                    <span className="project-details-user">{clientObj&&clientObj.client&&clientObj.client.email}</span>
                                </span>
                            </div>
                        </td>
                        <td data-title="COMPANY NAME">{clientObj&&clientObj.client&&clientObj.client.company_name}</td>
                        <td data-title="COUNTRY">
                            <span className="badge badge-pill badge-outline-primary">
                                {clientObj && clientObj.billing_address && clientObj.billing_address.country && clientObj.billing_address.country.name_ascii}</span>
                        </td>
                        <td data-title="CONTACT NUMBER">
                            <div className="position-relative">
                                <span className="text-muted clnt-lst12"><i class="icon-phone"></i></span>
                                <span className="span-column clnt-mr-lft2">
                                    <span className="span-row">{clientObj && clientObj.client && clientObj.client.mobile}</span>
                                    <span className="span-row"></span>
                                </span>
                            </div>
                        </td>
                        <td data-title="SKYPE ID">
                            {clientObj && clientObj.otherdetails && clientObj.otherdetails.skype_name}
                        </td>
                        <td data-title="WEBSITE">
                            <a href="">{clientObj && clientObj.client && clientObj.client.website}</a>
                        </td>
                    </tr>
                        )}
                    </tbody>
                </table>
            </div>
        )
    }
}

export default ClientList