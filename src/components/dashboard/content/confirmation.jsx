// Confirmation.jsx
import React, { Component, Fragment } from 'react';
import { Button, List } from 'semantic-ui-react';
import Select from 'react-select';

let employeesList = []
class Confirmation extends Component {
    saveAndContinue = (params,values) => {
        console.log("fixed",params,values)
        if(params==='save'){
            if(values.typeValue==="fixed"||values.select_internal){
                this.props.nextStep(); 
            }
            else
            {let validate=this.props.validateHourlyWeekly()
            validate&&this.props.nextStep();}
        }
        if(params==='skip'){
            this.props.nextStep();
        }
        if(params==="redirect"){
            console.log("hi in redirect")
            this.props.nextStep('redirect');
        }
        if(params==='create'){
            if(values.typeValue==="fixed"||values.select_internal){
                this.props.validateProjectForm('create') 
            }
            else
            {
                let validate=this.props.validateHourlyWeekly()
            validate&&this.props.validateProjectForm('create')
        }
            
        }
    }

    back = (e) => {
        e.preventDefault();
        this.props.prevStep();
    }

    componentDidUpdate(prevProps) {


    }

    render() {
        const { values } = this.props

        employeesList = this.props.employees

        return (
            <Fragment>
                <div className="sub-header" style={{ marginTop: "20px" }} >





                    <div className="col-sm-12 ">
                        <button style={{ width: "130px", marginTop: "2rem" }} onClick={this.props.addResource} type="button" className="btn btn-outline-primary add-row">

                            <i className="icon-plus"></i> Add Resource
                                       </button>
                    </div>


                    {this.props.resources.map((obj, index) => {
                        return (
                            <Fragment>
                                <div className="row " style={{ margin: "auto", width: "520px", marginBottom: "20px" }}>
                                    <div className=" col-sm-6 " style={{ textAlign: "justify" }} >
                                        <label for="id_client" className="text-light-grey">
                                            Select Resource
                                        </label>

                                        <Select
                                            name={`resource_${index}`}
                                            options={employeesList}
                                            value={obj}

                                            onChange={(event) => this.props.handleresourceChange(event, index)}
                                        />
                                         <span className="text-danger">{this.props.resourceError[index]}</span>
                                    </div>
                                    <div className="col-sm-6 ">
                                    <button className="btn btn-outline-primary delete-row" style={{ width: "130px",marginTop: "2rem"}} onClick={() => this.props.removeResource(index)} href="javascript:void(0)" ><i class=" icon-delete"></i> Remove</button>
                                    </div>
                                </div>


                                {(this.props.showRestDetails && this.props.showRestDetails[index].showDetail) ?

                                    <div className="row" style={{ margin: "auto", width: "520px" }}>
                                        
                                        
                                        <div className="col-sm-9 form-group ">
                                            {/* <div style={{display:"flex",width:"500px"}}> */}<span style={{ marginRight: "10px" }}>{obj.label} {index}</span>
                                            <input type="radio" className="individual-button" checked={values.select_billable[index].select_billable} name={`projectinfo_${index}`} onClick={() => this.props.select_billable(index)} value="Internal" />
                                            <label for="internal" className="individual">Billable</label>


                                            <input type="radio" name={`projectinfo_${index}`} checked={values.select_billable[index].select_nonbillable} className="individual-button" onClick={() => this.props.select_nonbillable(index)} value="Client" />
                                            <label for="client" className="individual" >Non Billable</label></div>
                                        
                                        {values.select_billable[index].select_billable&&
                                        <Fragment>
                                            {values.typeValue!=="fixed"&&!values.select_internal?
                                        <div className="col-sm-4 form-group">
                                            <label className="text-light-grey" for="id_end_date">Hourly Rate</label>
                                            <input type="text"
                                                name="hourly_rate"
                                                className="fullwidth input-control"
                                                label="Hourly Rate"
                                                placeholder="Hourly Rate"
                                                onChange={(e)=>this.props.weeklyHourlyonChange(e,index)}
                                                value={obj.hourly_rate}
                                            />
                                            <span className="text-danger">{obj.hourly_rate_error}</span>
                                        </div>:null}
                                        {values.typeValue!=="fixed"&&!values.select_internal?
                                        <div className="col-sm-4 form-group">
                                            <label className="text-light-grey" for="id_end_date">Weekly Limit</label>
                                            <input type="text"
                                                name="weekly_limit"
                                                className="fullwidth input-control"
                                                label="Weekly Limit"
                                                placeholder="Weekly Limit"
                                                onChange={(e)=>this.props.weeklyHourlyonChange(e,index)}
                                                value={obj.weekly_limit}
                                            />
                                            <span className="text-danger">{obj.weekly_limit_error}</span>
                                        </div>:null}

                                        {values.typeValue!=="fixed"&&!values.select_internal?
                                        <div className="col-sm-4 form-group" style={{ margin: "auto" }}>

                                            <span style={{ margin: "auto" ,color:"#9C9C9C"}}>{obj.designation_name} 

                                            </span>

                                        </div>:
                                        <div className="col-sm-4 form-group">

                                        <span style={{color:"#9C9C9C"}}>{obj.designation_name} 

                                        </span>

                                    </div>}
                                        </Fragment>}

                                        {values.select_billable[index].select_nonbillable&&
                                        <div className="row" style={{ margin: "auto", width: "520px", marginTop: "20px" }}>



                                            <div className="col-sm-2  form-group" >

                                                <span style={{ margin: "auto",color:"#9C9C9C" }}>{obj.designation_name} </span>
                                            </div>
                                            <div className="col-sm-4 form-group" style={{ marginTop: "auto" }}>


                                                <input type="checkbox" onChange={()=>this.props.trackerOnChange(index)} checked={obj.tracker} />
                                                <label for="id_client" className="text-light-grey">
                                                    Tracker
                                            </label>
                                            </div>

                                        </div>}

                                    </div>
                                    : null}
                                {/* {values.select_nonbillable && values.add_resource ? */}

                                {/* : null} */}

                            </Fragment>

                        )
                    })}

                    {((this.props.resources&&this.props.resources[0]&&this.props.resources[0].value==='')||this.props.resources.length==0) ?
                         <div className="row" style={{ margin: "auto", width: "520px", marginTop: "20px" }}>
                        <div className="col-sm-6 form-group" style={{ margin: "auto", width: "520px" }} >
                        <button type="button" style={{ width: "130px" }} onClick={this.back} className="btn btn-outline-primary add-row">Back </button>

                    </div>
                    {values.typeValue==="fixed"?
                        <div className="col-sm-6" style={{ margin: "auto", width: "520px",textAlign: "center" }}>
                            <button type="button" style={{ width: "130px" }} className="btn btn-outline-primary add-row" onClick={()=>this.saveAndContinue('redirect',values)}>skip </button>
                        </div>: <div className="col-sm-6" style={{ margin: "auto", width: "520px",textAlign: "center" }}>
                            <button type="button" style={{ width: "130px" }} className="btn btn-outline-primary add-row" onClick={()=>this.saveAndContinue('skip')}>skip </button>
                        </div>} </div>: 
                        <div className="row" style={{ margin: "auto", width: "520px", marginTop: "20px" }}>
                            <div className="col-sm-4 form-group" style={{ margin: "auto", width: "520px" }} >
                                <button type="button" style={{ width: "130px" }} onClick={this.back} className="btn btn-outline-primary add-row">Back </button>

                            </div>
                            <div className="col-sm-4 form-group" style={{ margin: "auto", width: "520px" }} >
                                <button type="button" style={{ width: "130px" }} onClick={()=>this.saveAndContinue('create',values)} className="btn btn-outline-primary add-row">Create and close </button>

                            </div>
                            {!values.select_internal?
                            <div className="col-sm-4 form-group" style={{ margin: "auto", width: "520px" }}>
                                {values.typeValue==="fixed"?
                                <button type="button" style={{ width: "130px" }} className="btn btn-outline-primary add-row" onClick={()=>this.saveAndContinue('redirect',values)}>Save And Next </button>
                                :<button type="button" style={{ width: "130px" }} className="btn btn-outline-primary add-row" onClick={()=>this.saveAndContinue('save',values)}>Save And Next </button>}
                            </div>:
                            <div className="col-sm-4 form-group" style={{ margin: "auto", width: "520px" }}>
                            <button type="button" style={{ width: "130px" }} className="btn btn-outline-primary add-row" onClick={()=>this.saveAndContinue('redirect',values)}>Save And Next </button>

                        </div>}
                            </div>}
                </div>
            </Fragment>
        )
    }
}

export default Confirmation;