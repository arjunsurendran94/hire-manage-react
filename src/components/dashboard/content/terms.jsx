// Confirmation.jsx
import React, { Component, Fragment } from 'react';
import { Button, List } from 'semantic-ui-react';
import Select from 'react-select';
class Terms extends Component {
   saveAndContinue = (params) => {
      //   debugger
      let validate = this.props.validateContract()
      if (params === 'create') {
         validate && this.props.validateProjectForm('create')
      }
      if (params === 'save') {
         validate && this.props.nextStep();
      }
   }

   back = (e) => {

      e.preventDefault();
      this.props.prevStep();
   }

   render() {
      const { values } = this.props
      



      const clientList = [{ name: "abc", value: "abc", label: "abc" }]
      let attachments = values.attachments;
      let terms = values.terms;
      let resources = values.resourcesAssigned
      

      let found
      let billable_resources = []
      if (values.typeValue === "hourly") {


         billable_resources = resources.filter(el => el.resource_billing_type === 'billable' && el.value != '');
         if (billable_resources.length > 0) {
            found = true
            // this.props.setcontractState(billable_resources)
         }
      }
      else {
         found = false
      }

      // if (values.typeValue === "fixed") {
      //    let fixedArray = [{ fixed: 'fixed' }]
      //    this.props.setcontractState(fixedArray)
      // }

      return (
         <Fragment>
            <div className="sub-header" style={{ marginTop: "20px" }} >

               {/* no billable */}
               {values.typeValue === "hourly" || values.select_internal ?
                  <Fragment>
                     {!found ?
                        <div className="row " style={{ margin: "auto", width: "500px", color: "#9C9C9C", marginBottom: "20px" }}>
                           Atleast one Billable resource needs to be added to assign a contract.Please click Back button  to
                           go to the previous screen and add resources.
                  </div> :
                        <Fragment>
                           <div className="row " style={{ margin: "auto", width: "750px", marginBottom: "20px" }}>


                              <div className="col-sm-12 form-group">
                                 <table className="table" style={{ textAlign: "center" }} >
                                    <thead >
                                       <tr>
                                          <th>Name</th>
                                          <th>Contract Title</th>
                                          <th>Contract</th>
                                          <th>Terms</th>
                                       </tr>
                                    </thead>
                                    <tbody >


                                       <Fragment>
                                          {billable_resources.map((obj, index) =>
                                             <tr>
                                                <td>{obj.label}</td>
                                                <td><div >

                                                   <input type="text"
                                                      name="contract_title"
                                                      className="fullwidth input-control"
                                                      label="Contract Title"
                                                      value={values.contractAttachments[index] && values.contractAttachments[index].contract_title}
                                                      placeholder="Contract Title"
                                                      onChange={(e) => this.props.handleTerms(e, index)}

                                                   />

                                                   <span className="text-danger">{values.contractAttachments[index] && values.contractAttachments[index].contract_title_error}</span>
                                                </div> </td>
                                                <td >
                                                   <table className="table">
                                                      <tbody>
                                                         <tr className="contract_formset_row dynamic-form">
                                                            <td className="">
                                                               {(values.contractAttachments[index] && !values.contractAttachments[index].contract) ?
                                                                  <input type="file" className="fullwidth input-control" name="contract" onChange={(event) => this.props.upLoadAttachment(index, event, 'contracts')} className="form-control" id="id_contract_formset_set-0-contract" />

                                                                  : <Fragment>
                                                                     
                                                                     <span >{values.contractAttachments[index] && values.contractAttachments[index].contract_name}</span>
                                                                     <button onClick={() => this.props.removeAttachment(index, 'contracts')} type="button" className="btn btn-red close-button" data-dismiss="modal" aria-label="Close">
                                                                        <span style={{ color: "red" }} aria-hidden="true">×</span>
                                                                     </button>
                                                                  </Fragment>
                                                               }
                                                               <div className="form-control-error-list">


                                                                  <span className="text-danger">{values.contractAttachments[index] && values.contractAttachments[index].error}</span>
                                                               </div>
                                                            </td>
                                                            <td>
                                                            </td>
                                                         </tr>
                                                      </tbody>
                                                   </table>
                                                </td>
                                                <td >
                                                   <table className="table">

                                                      <tbody>



                                                         <tr className="contract_formset_row dynamic-form">
                                                            <td className="">
                                                               {values.termAttachments[index] && values.termAttachments[index].termAttachment === null
                                                                  ?

                                                                  <input type="file" className="fullwidth input-control" name="attachments" onChange={(event) => this.props.upLoadAttachment(index, event, 'terms')} className="form-control" id="id_contract_formset_set-0-contract" />
                                                                  :
                                                                  <Fragment>
                                                                     {values.termAttachments[index] && values.termAttachments[index].name}
                                                                     <button onClick={() => this.props.removeAttachment(index, 'terms')} type="button" className="btn btn-red close-button" data-dismiss="modal" aria-label="Close">
                                                                        <span style={{ color: "red" }} aria-hidden="true">×</span>
                                                                     </button>
                                                                  </Fragment>
                                                               }

                                                               <div className="form-control-error-list">
                                                                  <span className="text-danger">{values.termAttachments[index] && values.termAttachments[index].error}</span>
                                                               </div>
                                                            </td>
                                                            <td>
                                                            </td>
                                                         </tr>


                                                      </tbody>
                                                   </table>
                                                </td>
                                             </tr>

                                          )}
                                       </Fragment>


                                    </tbody>



                                 </table>
                              </div>
                           </div>
                        </Fragment>
                     }</Fragment> : null}

               {/* fixed */}






               {(values.typeValue === "hourly" && !found) || (values.select_internal && !found) ?
                  <div className="row">
                     <div className="col-sm-12 form-group" style={{ textAlign: "center" }}>
                        <button type="button" style={{ width: "130px", marginRight: "20px" }} onClick={this.back} className="btn btn-outline-primary add-row" >Back</button>
                        <button type="button" style={{ width: "130px" }} onClick={() => this.saveAndContinue('create')} className="btn btn-outline-primary add-row" >Create and Close</button>

                     </div></div> : null}


               {((values.typeValue === "fixed") || (values.typeValue === "hourly" && found)) &&
                  <div className="row">
                     <div className="col-sm-4 form-group" style={{ textAlign: "center" }}>

                        <button type="button" style={{ width: "130px" }} className="btn btn-outline-primary add-row" onClick={this.back}>Back </button>

                     </div>
                     {values.typeValue === "hourly" ?
                        <div className="col-sm-4 form-group" style={{ textAlign: "center" }}>
                           <button type="button" style={{ width: "130px" }} onClick={() => this.saveAndContinue('create')} className="btn btn-outline-primary add-row" >Create Contract</button>

                        </div> : null}

                     <div className="col-sm-4 form-group" style={{ textAlign: "center" }}>
                        <button type="button" style={{ width: "130px" }} className="btn btn-outline-primary add-row" onClick={() => this.saveAndContinue('save')}>Save And Next </button>

                     </div></div>}

            </div>

         </Fragment>
      )
   }
}

export default Terms;