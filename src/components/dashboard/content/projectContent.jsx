import React, {Component, Fragment} from 'react'
import { Link } from 'react-router-dom'


import Loader from '../../common/Loader'



class ProjectContent extends Component{
    state = {
        branch_name:"",
        branch_slug:"",
        index:"org",
        departments_list:[]
    }

    
    render(){
        let permissions = []
        permissions = localStorage.getItem('permissions')
        if (permissions && permissions.length > 0) {
            permissions = permissions.split(',')
        }
        return (
            
            <div className='col-sm-12 mt-3 '>
                <div className="align-self-center text-center">
                    {(permissions && permissions.includes('add_project'))?
                    <Fragment>
                    <div>Create and manage your projects here.</div>
                    <Link to="/projects/add-new-project/" className="btn btn-link-vilot">CREATE NEW PROJECT</Link> 
                    </Fragment>:
                    <Fragment>
                    <h3>No Projects Found</h3>
                    </Fragment>}
                    
                
                </div>
            </div>
        )
    }
}

export default ProjectContent