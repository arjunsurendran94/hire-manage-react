import React, {Component, Fragment} from 'react'
import BranchDetailComponent from '../common/branchDetailComponent'
import { Link } from 'react-router-dom'

// import SideBar from '../common/sideBar'
// import DashBoardHeader from '../common/dashboardHeader'
// import DashBoardSubheader from '../common/dashboardSubHeader'

import Loader from '../../common/Loader'

import ErrorMessage from '../../common/ErrorMessage'

class ClientContent extends Component{
    state = {
        branch_name:"",
        branch_slug:"",
        index:"org",
        departments_list:[]
    }

    
    render(){
        
        
        return (
            
            <div className='col-sm-12 mt-3 sub-header'>
                <ErrorMessage closeMessage={this.props.setShowMessage} msg='Client not found'/>

                <div className="align-self-center text-center">
                    <h3>Business is no fun without people.</h3>
                    <div>create and manage your contacts all in one place</div>
                    <Link to="/clients/add-new-client/" className="btn btn-link-vilot">CREATE NEW CUSTOMER</Link> 
                </div>
            </div>
        )
    }
}

export default ClientContent