import React, {Component, Fragment} from 'react'
import UpdateEmployee from '../modal/updateEmployeeModal'
import LoaderImage from '../../../static/assets/image/loader.gif';
var loaderStyle = {
    width: '3%',
    position: 'fixed',
    top: '50%',
    left: '57%'
 }
class OrganizationEmployeeDetailContent extends Component{
    constructor(props) {
        super(props);
        this.state = {
            firstTab :true,
            secondTab:false,
            thirdTab:false,
            editPart:'',
            editBasic:false,
            image: '',
            sendImage: '',
            image_validation_error: ''
        };
    }

    
    _onChange = (event) => {
        const file = event.target.files[0]
        if(file){
        const reader = new FileReader();

        var imageTypes = ['image/png', 'image/jpg', 'image/gif', 'image/jpeg']
        var image_index = imageTypes.indexOf(file.type)

        if (image_index >= 0){
            
        this.setState({
            sendImage: file
        })

        reader.onloadend = () => {
            this.setState({
                image: reader.result,image_validation_error: ''
            })
        }
        reader.readAsDataURL(file);
            this.setState({
                image: reader.result,image_validation_error: ''
            })
        let values={}
        values.photo=file
        const form_data = new FormData();
        for (var key in values) {
            form_data.append(key, values[key]);
        }
        this.props.partialUpdateEmployee(this.props.employeeObject.employeeData.slug,form_data)
        }

        else {
            this.setState({ image: "", image_validation_error: 'Invalid Image' })
        }

    }
    
    else {
        this.setState({
            image: ""
        })
    }

}


    componentDidMount(){
        this.props.fetchCountryList()
        this.props.designationsListing();
        this.props.retrieveBranches(localStorage.getItem('organization_slug'),localStorage.getItem('token'))
        if(this.props.departmentlist&&this.props.departmentlist.organization_department.length<=0){
            this.props.retrieveDepartments('');
        }
        // this.props.retrieveDepartments('');
    }

    nameIcon = (employee) => {
        const iconName = employee.first_name.charAt(0).toUpperCase()+employee.last_name.charAt(0).toUpperCase()
        return iconName
    }
    employeeName = (employee) => {
        const empName =employee.first_name+' '+employee.last_name
        return empName
    }
    selectFirstSubMenu = () => {
        this.setState({
            firstTab :true,
            secondTab:false,
            thirdTab:false
        })
    }
    selectSecondSubMenu = () => {
        this.setState({
            firstTab :false,
            secondTab:true,
            thirdTab:false
        })
    }
    selectThirdSubMenu = () => {
        this.setState({
            firstTab :false,
            secondTab:false,
            thirdTab:true
        })
    }
    updateEmployee = (part) => {
        this.setState({editBasic:true,editPart:part})
    }

    closeModal = () => {
        this.setState({editBasic:false,editPart:''})
    }

    render(){
        

        var firstTabClass =`nav-link${this.state.firstTab ? " active show" : ""}`;
        var secondTabClass =`nav-link${this.state.secondTab ? " active show" : ""}`;
        var thirdTabClass =`nav-link${this.state.thirdTab ? " active show" : ""}`;
        if(this.props.employeePermissions&&this.props.employeePermissions.employeePermissionStatus === "OK"){
            window.location.reload()
        }
        if(this.props.employees && this.props.employees.employeeUpdate&&this.props.employees.employeeUpdateStatus === 200){
            window.location.reload()
        }
        return (
            <Fragment>
                {this.props.employeeObject&&this.props.employeeObject.isLoading?
                           <Fragment>
                              <div className="loader-img"> <img src={LoaderImage} style={loaderStyle} /></div>
                           </Fragment>
                           
                        :
           
            <Fragment>
                {(this.props.employeeObject&&this.props.employeeObject.employeeData)&&
                <div className="col-sm-12 mt-3 sub-header">
                    <div className="row">
                        <div className="userMail-grid fullwidth">
                            <div className="emp-pg fullwidth float-left">
                                <form  className="image-update-form">
                                    <input type="hidden" name="csrfmiddlewaretoken" value="dewxL3K2z87FzG6aTcfIDTT32DECXOSOKmsDr9Y00OeHms2tL5MQIKnVtquvkeGH"/>
                                    <div className="employee-lft emp-img fullwidth">             
                                        
                                        {(this.props.employeeObject.employeeData&&this.props.employeeObject.employeeData.photo)?
                                        <img alt="employee_image" className="img-fluid" src = {this.props.employeeObject.employeeData.photo}/>                              
                                        :
                                        <img alt="employee_image" className="img-fluid" src = {require("../../../static/assets/image/user.jpg")}/>                              
                                        }
                                        
                                        <div className="p-relative">
                                            <label htmlFor="id_photo" className="id-pic" id="overlay"><i className="icon-edit"></i><br/> Update</label>
                                            <div className="d-none">
                                                <input type="file" name="photo" onChange={this._onChange} accept="image/*" id="id_photo"/>
                                            </div>
                                            <div className="text-danger text-center error-message">{this.state.image_validation_error}</div>
                                        </div>      
                                    </div>
                                </form>

                                <div className="col-lg-12 float-left emp-pad">
                                    <div className="row">
                                        <div className="employee-rgt float-left fullwidth">
                                            <div className="userMail-grid-nav">
                                                <div className="inline-bx mt-1 mb-4">
                                                    <span className="user-img-holder">{this.nameIcon(this.props.employeeObject.employeeData)}</span>
                                                    <span className="project-details">
                                                        <span className="project-details-title">{this.employeeName(this.props.employeeObject.employeeData)}</span>
                                                        <span className="project-details-user text-primary">{(this.props.employeeObject.employeeData.nationality&&this.props.employeeObject.employeeData.nationality.name_ascii)&&this.props.employeeObject.employeeData.nationality.name_ascii}</span>
                                                    </span>
                                                </div><br/>

                                                
                                                <div className="inline-bx mt-1 mb-4">
                                                    <span className="icon-holder"><i className="icon-phone-o b-6"></i></span>
                                                    <span className="project-details">
                                                        <span className="project-details-title">{this.props.employeeObject.employeeData.phone} (Mob)</span>
                                                    </span>
                                                </div><br/>
                                                

                                                <div className="inline-bx mt-1 mb-2">
                                                    <span className="icon-holder"><i className="icon-envelope b-6"></i></span>
                                                    <span className="project-details">
                                                        <span className="project-details-title">{this.props.employeeObject.employeeData.email}</span>
                                                    </span>
                                                </div><br/>

                                                {(this.props.employeeObject.employeeData.status === "Active")&&

                                                    <div className="inline-bx mt-1 mb-2">
                                                        <span className="icon-holder text-success" style={{borderColor: "green"}}><i className="icon-checked b-6"></i></span>
                                                        <span className="project-details text-success">
                                                            <span className="project-details-title">Active</span>
                                                        </span>
                                                    </div>
                                                }
                                                {(this.props.employeeObject.employeeData.status === "Inactive") &&
                                                    <div className="inline-bx mt-1 mb-2">
                                                        <span className="icon-holder text-danger" style={{borderColor: "red"}}><i className="icon-cross b-6"></i></span>
                                                        <span className="project-details text-danger">
                                                        <span className="project-details-title">Inactive</span>
                                                        </span>
                                                    </div>
                                                }
                                                {(this.props.employeeObject.employeeData.status === "Invited") &&
                                                    <div className="inline-bx mt-1 mb-2">
                                                        <span className="icon-holder text-primary" style={{borderColor: "#007bff"}}><i className="icon-warning-circle b-6"></i></span>
                                                        <span className="project-details text-primary">
                                                        <span className="project-details-title">Invited</span>
                                                        </span>
                                                    </div>
                                                }

                                                <div className="project-options1">                             
                                                    <button onClick= {()=>this.updateEmployee("basic")} className="btn btn-link-secondary show-form-modal" >
                                                        <i className="icon-pencil"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="userMail-grid-tab shadow float-left fullwidth">
                                <ul className="nav nav-tabs" id="myTab" role="tablist">
                                    <li className="nav-item">
                                        <button onClick = {this.selectFirstSubMenu}className={firstTabClass} id="billing-tab" data-toggle="tab" href="#billing" role="tab" aria-controls="billing" aria-selected="true">Primary Info</button>
                                    </li>
                                    <li className="nav-item">
                                        <button onClick = {this.selectSecondSubMenu} className={secondTabClass} id="other-tab" data-toggle="tab" href="#other" role="tab" aria-controls="other" aria-selected="false">Other Details</button>
                                    </li>                                
                                    <li className="nav-item">
                                        <button onClick = {this.selectThirdSubMenu} className={thirdTabClass} id="contact-tab" data-toggle="tab" href="#permissions" role="tab" aria-controls="contact" aria-selected="false">Roles</button>
                                    </li>
                                 </ul>
                                <div className="tab-content fullwidth float-left" id="myTabContent">
                                    {
                                        this.state.firstTab &&
                                        <div className={this.state.firstTab ?"tab-pane userMail-tab fade float-left fullwidth active show":"tab-pane userMail-tab fade float-left fullwidth" } id="billing" role="tabpanel" aria-labelledby="billing-tab" style={{position: "relative"}}>                                    
                                            <button onClick= {()=>this.updateEmployee("primaryInfo")} type="button" className="btn btn-link-secondary btn-edit-abs show-form-modal" data-url="/employees/update-employee-primary-info/a9a6c5b6-9510-4c34-924a-739cf43edc29/">
                                                <i className="icon-edit-bold h6 text-blue-grey"></i>
                                            </button>
                                            <ul className="p-0 fullwidth float-left">
                                                <li className="col-lg-2 float-left mb-4">
                                                    <div>
                                                        <span className="b-6">BRANCH</span>
                                                    </div>
                                                    <div className="text-muted text-sm">
                                                        {(this.props.employeeObject.employeeData.branch&&this.props.employeeObject.employeeData.branch.branch_name)?
                                                        this.props.employeeObject.employeeData.branch.branch_name
                                                        :"NILL"}
                                                    </div>
                                                </li>    
                                                    
                                                <li className="col-lg-2 float-left mb-4">
                                                    <div>
                                                        <span className="b-6">DEPARTMENT</span>
                                                    </div>
                                                    <div className="text-muted text-sm">
                                                        {(this.props.employeeObject.employeeData.department&&this.props.employeeObject.employeeData.department.department_name)?
                                                        this.props.employeeObject.employeeData.department.department_name:
                                                        "NILL"}
                                                    </div>
                                                </li>     

                                                <li className="col-lg-2 float-left mb-4">
                                                    <div>
                                                        <span className="b-6">DESIGNATION</span>
                                                    </div>
                                                    <div className="text-muted text-sm">
                                                        {
                                                            (this.props.employeeObject.employeeData.designation&&this.props.employeeObject.employeeData.designation.designation_name)?
                                                            this.props.employeeObject.employeeData.designation.designation_name:
                                                            "NILL"
                                                        }
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    }
                                    {
                                        this.state.secondTab&&
                                        <div className={this.state.secondTab ? "tab-pane userMail-tab fade position-relative float-left fullwidth active show":"tab-pane userMail-tab fade position-relative float-left fullwidth"} id="other" role="tabpanel" aria-labelledby="other-tab">                   
                                            <button onClick= {()=>this.updateEmployee("other")} type="button" className="btn btn-link-secondary btn-edit-abs show-form-modal" data-url="/employees/update-employee-other-details/a9a6c5b6-9510-4c34-924a-739cf43edc29/">
                                                <i className="icon-edit-bold h6 text-blue-grey"></i>
                                            </button>
                                            <ul className="p-0 fullwidth float-left"> 
                                                <li className="col-lg-3 float-left mb-4">
                                                    <div>
                                                        <span className="b-6">NATIONALITY</span>
                                                    </div>
                                                    <div className="text-muted text-sm">
                                                    {(this.props.employeeObject.employeeData.nationality&&this.props.employeeObject.employeeData.nationality.name_ascii)&&this.props.employeeObject.employeeData.nationality.name_ascii}
                                                    </div>
                                                </li>

                                                <li className="col-lg-3 float-left mb-4">
                                                    <div>
                                                        <span className="b-6">STATE</span>
                                                    </div>
                                                    <div className="text-muted text-sm">
                                                    {(this.props.employeeObject.employeeData.state&&this.props.employeeObject.employeeData.state.name_ascii)&&this.props.employeeObject.employeeData.state.name_ascii}
                                                    </div>
                                                </li>

                                                <li className="col-lg-3 float-left mb-4">
                                                    <div>
                                                        <span className="b-6">CITY</span>
                                                    </div>
                                                    <div className="text-muted text-sm">
                                                    {(this.props.employeeObject.employeeData.city&&this.props.employeeObject.employeeData.city.name_ascii)&&this.props.employeeObject.employeeData.city.name_ascii}
                                                    </div>
                                                </li>

                                                <li className="col-lg-3 float-left mb-4">
                                                    <div>
                                                        <span className="b-6">HOUSE NAME / FLAT NO</span>
                                                    </div>
                                                    <div className="text-muted text-sm">{this.props.employeeObject.employeeData.house_name?this.props.employeeObject.employeeData.house_name:"NILL"}</div>
                                                </li>
                                                <li className="col-lg-3 float-left mb-4">
                                                    <div>
                                                        <span className="b-6">STREET NAME / NO</span>
                                                    </div>
                                                    <div className="text-muted text-sm">{this.props.employeeObject.employeeData.street_name?this.props.employeeObject.employeeData.street_name:"NILL"}</div>
                                                </li>
                                                <li className="col-lg-3 float-left mb-4">
                                                    <div>
                                                        <span className="b-6">LOCALITY NAME / NO</span>
                                                    </div>
                                                    <div className="text-muted text-sm">{this.props.employeeObject.employeeData.locality_name?this.props.employeeObject.employeeData.locality_name:"NILL"}</div>
                                                </li>
                                                <li className="col-lg-3 float-left mb-4">
                                                    <div>
                                                        <span className="b-6">ZIP CODE</span>
                                                    </div>
                                                    <div className="text-muted text-sm">{this.props.employeeObject.employeeData.pin_code?this.props.employeeObject.employeeData.pin_code:"NILL"}</div>
                                                </li>
                                            </ul>
                                        </div>
                                    }
                                    {
                                        this.state.thirdTab&&
                                        <div className={this.state.thirdTab ?"tab-pane userMail-tab fade active show":"tab-pane userMail-tab fade" } id="permissions" role="tabpanel" aria-labelledby="contact-tab" style={{position: "relative"}}>
                                            <ul className="p-0 fullwidth float-left">
                                                <li className="col-lg-3 float-left mb-4">
                                                <div>
                                                        <span className="b-6">ROLES</span>
                                                    </div>
                                                    {
                                                        (this.props.employeePermissions&&
                                                            this.props.employeePermissions.employeePermissions&&
                                                            this.props.employeePermissions.employeePermissions.permission_groups&&
                                                            this.props.employeePermissions.employeePermissions.permission_groups.length>0)&&
                                                            (this.props.employeePermissions.employeePermissions.permission_groups.map((role,index)=>(
                                                                <div className="text-muted text-sm" key={index}>
                                                                    {role.group_name}
                                                                </div> 
                                                            )))

                                                        

                                                        // (this.props.employeeObject&&this.props.employeeObject.employeeData
                                                        //  &&this.props.employeeObject.employeeData.designation&&
                                                        //  this.props.employeeObject.employeeData.designation.permission_groups&&
                                                        //  this.props.employeeObject.employeeData.designation.permission_groups.length>0&&
                                                        //  this.props.employeeObject.employeeData.designation.permission_groups.map((role,index)=>(
                                                        //             <div className="text-muted text-sm" key={index}>
                                                        //                 {role.group_name}
                                                        //             </div> 
                                                        //         )
                                                        //  ))

                                                        
                                                    }
                                                </li>
                                            </ul>
                                            <button onClick= {()=>this.updateEmployee("roles")} type="button" className="btn btn-link-secondary btn-edit-abs show-form-modal" data-url="/employees/update-employee-permissions/a9a6c5b6-9510-4c34-924a-739cf43edc29/">
                                                <i className="icon-edit-bold h6 text-blue-grey"></i>
                                            </button>                                      
                                        </div>
                                    }
                                    
                                    
                                    <div className="tab-pane userMail-tab fade" id="remarks" role="tabpanel" aria-labelledby="remarks-tab">
                                        Lorem Ipsum is simply dummy text of the printing and
                                        typesetting industry. Lorem Ipsum has been the
                                        industry's standard dummy text ever since the 1500s,
                                        when an unknown printer took a galley of type and
                                        scrambled it to make a type specimen book. It has
                                        survived not only five centuries, but also the leap
                                        into electronic typesetting, remaining essentially
                                        unchanged.
                                    </div>
                                </div>
                            </div>                   
                        </div>                       
                    </div>
                </div>
                }
                {this.state.editBasic && <UpdateEmployee editPart={this.state.editPart} closeModal = {this.closeModal} {...this.props} />}
            </Fragment>}</Fragment>
        )
    }
}

export default OrganizationEmployeeDetailContent