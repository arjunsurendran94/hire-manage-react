import React, { Component, Fragment } from 'react'
import { Field, reduxForm } from 'redux-form'
import { API_URL } from '../../../config/hostSetting'
import AddDesignationNew from '../modal/addDesignationModal';
import { countryReorder } from '../../../countryReorder'
import LoaderImage from '../../../static/assets/image/loader.gif';
import Addemployeeprimaryinfo from './addEmployeePrimaryInfo'
import Addemployeeotherinfo from './addEmployeeOtherInfo';
import PropTypes from 'prop-types'

var loaderStyle = {
    width: '3%',
    position: 'fixed',
    top: '50%',
    left: '57%'
}

const required = value => {
    return (value || typeof value === 'number' ? undefined : ' is Required')
}

const maxLength = max => value =>
    value && value.length > max ? `Must be ${max} characters or less` : undefined

const maxLength128 = maxLength(128)

const email = value =>
    value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
        ? ' is Invalid'
        : undefined

var letters = /^[a-zA-Z.]+\s*$/;
const alphabetOnly = value => {
    return (value && value.match(letters) ? undefined : ' must be only alphabets without spaces in between')
}

const renderRadioField = ({ input, label, type, checked }) => (
    <Fragment>

        <label>{label}</label>
        <input {...input} type={type} checked={checked} />

    </Fragment>
);

const renderFieldSelect = ({
    input,
    label,
    disable,
    type,
    existed,
    options,
    meta: { touched, error, warning, }
}) => {
    // debugger
    return (
        <Fragment>
            {(options && label === "Designation") &&
                <select {...input} className="fullwidth input-control">
                    <option id='no_designation' value="">Select Designation</option>
                    {options && options.designation && options.designation.map((designation, index) =>

                        <option key={index}

                            value={designation.slug} >{designation.designation_name} </option>
                    )}
                </select>
                // :<p className="text-danger">Please create a branch first.</p>
            }
            {(options && label === "Branch") &&
                <select {...input} className="fullwidth input-control" disabled={disable}>
                    <option id='no_designation' value="">Select Branch</option>
                    {options.map((branch, index) =>
                        <option key={index} value={branch.slug} >{branch.branch_name}  </option>
                    )}
                </select>
                // :<p className="text-danger">Please create a branch first.</p>
            }
            {(options && label === "Department") &&
                <select {...input} className="fullwidth input-control">
                    <option id='no_designation' value="">Select Department</option>
                    {options.map((department, index) =>
                        <option key={index} value={department.slug} >{department.department_name}  </option>
                    )}
                </select>
                // :<p className="text-danger">Please create a branch first.</p>
            }
            <div>
                {touched &&
                    ((error && <small className="text-danger">{label}{error}</small>) ||
                        (warning && <span>{warning}</span>))}
            </div>
            {/* {((existed)&&(existed.error.status == 500)) && <small class="text-danger">
            {existed.errors.error}
        </small>} */}

        </Fragment>

    )
}

const renderFieldLocationSelect = ({
    input,
    label,
    country,
    countryState,
    city,
    className,
    meta: { touched, error, warning, }
}) => {

    return (


        <Fragment>
            <select {...input} className={(touched && error) ? 'input-control border border-danger' : 'input-control'}>
                <option value="">{label}</option>
                {(country && label === "Country") &&
                    <Fragment>
                        {country.countryList.map((plan, index) => (
                            <option key={index} value={plan.id}>{plan.name_ascii}</option>
                        ))}</Fragment>
                }
                {(countryState && label === "State") &&
                    <Fragment>
                        {countryState.stateList.map((plan, index) => (
                            <option key={index} value={plan.id}>{plan.name_ascii}</option>
                        ))}</Fragment>
                }
                {(city && label === "City") &&
                    <Fragment>
                        {city.cityList.map((plan, index) => (
                            <option key={index} value={plan.id}>{plan.name_ascii}</option>
                        ))}</Fragment>
                }
            </select>
            <div>
                {touched &&
                    ((error && <small className="text-danger">{label}{error}</small>) ||
                        (warning && <span>{warning}</span>))}
            </div>
            {/* {((existed)&&(existed.error.status == 500)) && <small class="text-danger">
                {existed.errors.error}
            </small>} */}

        </Fragment>

    )
}

const mobileValid = value => {
    var mobilenoregex = /^(?!\b(0)\1+\b)(\+?\d{1,3}[. -]?)?\(?\d{3}\)?([. -]?)\d{3}\3\d{4}$/
    if (value && value.length >= 1) {
        return (mobilenoregex.test(value) ? undefined : 'Enter a Valid Mobile Number')
    }

    return undefined
}



const rendermobileField = ({ input, label, type, existed, invalid_error, id, className, accept, initialValue, name, meta: { touched, error, warning, } }) => {
    return (
        <Fragment>
            <input {...input} placeholder={label} type={type} className={(touched && error) || (invalid_error && invalid_error.phone) ? 'input-control border border-danger' : 'input-control'} id={id} accept={accept} defaultValue={initialValue} />
            {touched &&
                ((error && <small className="text-danger">{label !== "Phone number" && label}{error} with country code</small>) ||
                    (warning && <span>{warning}</span>))}

            {invalid_error && invalid_error.phone &&
                <small className="text-danger">{invalid_error.phone[0]} with country code</small>
            }
        </Fragment>
    )
}
const renderField = ({
    input,
    label,
    type,
    existed,
    pincodeError,
    meta: { touched, error, warning, }
}) => {

    return (


        <Fragment>
            {console.log('pincodeError', pincodeError)}
            <input {...input} placeholder={label} type={type} className={(touched && error) || ((existed && existed.email) || (pincodeError && pincodeError.non_field_errors)) ? 'input-control border border-danger' : 'input-control'} />
            {touched &&
                ((error && <small className="text-danger">{label}{error}</small>) ||
                    (warning && <span>{warning}</span>))}
            {((existed) && (existed.email)) && <small className="text-danger">
                {existed.email[0]}
            </small>}
            {((pincodeError) && (pincodeError.non_field_errors)) && <small className="text-danger">
                {pincodeError.non_field_errors[0]}
            </small>}
            {/* {((existed)&&(existed.error.status == 500)) && <small class="text-danger">
            {existed.errors.error}
        </small>} */}

        </Fragment>

    )
}


class AddEmployeeContent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            active: true,
            notactive: false,
            show_branch: false,
            image: '',
            sendImage: '',
            image_validation_error: '',
            showEmpCreatedMessage: false,
            empEmail: '',
            emailError: '',
            pincodeError: '',
            formError: true,
            showDesgnModal: false,
            designationOption: "",
            contactNoError: '',
            errorMessages: '',
            page: 1,
            allErrors:[]
        }
    }

    componentDidMount() {
        this.props.fetchCountryList()
        this.props.designationsListing();
        this.props.retrieveBranches(localStorage.getItem('organization_slug'), localStorage.getItem('token'))
        if (this.props.departmentlist && this.props.departmentlist.organization_department.length <= 0) {
            this.props.retrieveDepartments('');
        }
        this.props.fetchPermissionGroups()
        // this.props.retrieveDepartments('');
    }

    select_org = () => {
        this.props.retrieveDepartments('');
        this.setState({ show_branch: false })
    }

    select_branch = () => {
        this.setState({ show_branch: true })
    }

    selectDesignation = (e) => {
        this.setState({ designationOption: e.target.value })
    }

    
    handleBranchChange = (e) => {
        this.props.retrieveDepartments(e.target.value);
    }
    
    onImageChange = (event) => {
        if (event.target.files && event.target.files[0]) {
            let reader = new FileReader();
            reader.onload = (e) => {
                reader.readAsDataURL(event.target.files[0]);
                this.setState({ image: e.target.result });
            };

        }
    }

    errorOnChange = (param) => {
        if (param === 'email') {
            this.setState({ emailError: '' })
        }
        if (param === 'contact-no') {
            this.setState({ contactNoError: '' })
        }
        if (param === 'pincode') {
            this.setState({ pincodeError: '' })
        }
    }

    _onChange = (event) => {
        const file = event.target.files[0]
        const reader = new FileReader();

        var imageTypes = ['image/png', 'image/jpg', 'image/gif', 'image/jpeg']
        var image_index = imageTypes.indexOf(file.type)

        if (image_index >= 0) {

            this.setState({
                sendImage: file
            })

            reader.onloadend = () => {
                this.setState({
                    image: reader.result, image_validation_error: ''
                })
            }
            if (file) {
                reader.readAsDataURL(file);
                this.setState({
                    image: reader.result, image_validation_error: ''
                })
            }
            else {
                this.setState({
                    image: ""
                })
            }
        }
        else {
            this.setState({ image: "", image_validation_error: 'Invalid Image' })
        }

    }

    upLoadFile = (e) => {
        let image = e.target.files[0]
        var imageTypes = ['image/png', 'image/jpg', 'image/gif', 'image/jpeg']
        var image_index = imageTypes.indexOf(image.type)
        if (image_index >= 0) {
            this.setState({ image: image, image_validation_error: '' }, () => { console.log(this.state.image, 'uploaded image') })
        }
        else {
            this.setState({ image: "", image_validation_error: 'Invalid Image' })
        }
    }

    showEmpCreatedMessage = () => {
        this.setState({ showEmpCreatedMessage: false })
    }

    

    closeErrorMessage = () => {
        this.setState({ formError: false, errorMessages: '' })
    }

    submit = (values) => {
        console.log('valuessssssssssssssssssssssssssssssssssssssssss', values)
        if (!("phone" in values)) {
            values.phone = ''
        }
        if (!("department" in values)) {
            values.department = ''
        }
        values.photo = this.state.sendImage
        values.organization = localStorage.getItem("organization_slug") || null;
        values.invite_url = API_URL + "/employees/set-employee-password"
        values.site_name = "Hire and Manage";
        if ("owner_type" in values) {

            if (values.owner_type === "organization") {
                values.branch = ''
            }
            delete values["owner_type"];
        } else {
            values.branch = ''
        }
        const form_data = new FormData();
        for (var key in values) {
            form_data.append(key, values[key]);
        }
        this.props.addEmployee(form_data).then(() => {
            // debugger
            if (this.props.employeeCreated && this.props.employeeCreated.createdEmployee && this.props.employeeCreated.createdEmployeeStatus === 200) {
                this.setState({
                    active: true,
                    notactive: false,
                    showEmpCreatedMessage: true,
                    empEmail: this.props.employeeCreated.createdEmployee.email,
                    sendImage:''
                })
            }
            if (this.props.employeeCreated && this.props.employeeCreated.createdError && this.props.employeeCreated.createdErrorStatus === 400) {
                if (this.props.employeeCreated.createdError.email) {
                    this.setState({ active: true, notactive: false, emailError: this.props.employeeCreated.createdError?.email[0],showEmpCreatedMessage: false })
                }
                if (this.props.employeeCreated.createdError.non_field_errors) {
                    this.setState({ pincodeError: this.props.employeeCreated.createdError?.non_field_errors[0],showEmpCreatedMessage: false })
                }
                if (this.props.employeeCreated.createdError.phone) {
                    this.setState({ contactNoError: this.props.employeeCreated.createdError?.phone[0],showEmpCreatedMessage: false })
                }
            }
        })
    }

    toggleAddDesignationModal = () => {
        this.setState({ showDesgnModal: !this.state.showDesgnModal })
    }

    componentDidUpdate(prevProps) {
        if (prevProps.desigantions !== this.props.desigantions && this.props.desigantions.design_obj && this.props.desigantions.design_obj.slug) {
            console.log("checkkkkkk", this.props.desigantions)
            // this.setState({designationOption:this.props.desigantions.design_obj.slug})
            this.props.change('designation', this.props.desigantions.design_obj.slug)
        }

        if ((this.props.employeeCreated != prevProps.employeeCreated) && 
        (this.props?.employeeCreated?.createdErrorStatus == 500 || 
            this.props?.employeeCreated?.createdErrorStatus == 400 ||
            this.props?.employeeCreated?.createdErrorStatus == 403)
        ) {
            this.setState({ errorMessages: this.props?.employeeCreated?.createdError?.error,
                active: true,notactive: false,page:1 });
        }

        if(this.props.employeeCreated != prevProps.employeeCreated&&this.props.employeeCreated?.createdEmployeeStatus===200
            ){  
                
                this.setState({page:1,active: true,notactive: false,allErrors:[]})
        }

        
        
        if(this.props.employeeCreated!=prevProps.employeeCreated&&this.props.employeeCreated?.createdError){
            
            let Errors=[]
            
            Object.keys(this.props.employeeCreated?.createdError).map(
                (item) => (
                  Errors.push(this.props.employeeCreated?.createdError[item][0])
                )
              );
              this.setState({ allErrors: Errors });
        }

    }

    nextPage=()=> {
        
        this.setState({ page: this.state.page + 1 ,active: false,
            notactive: true})
      }
    
      previousPage=()=>{
        this.setState({ page: this.state.page - 1,active: true,
            notactive: false })
      }

      togglePrimaryClass = () => {
        this.setState({
            active: true,
            notactive: false,
            
        })
    }
    toggleOtherClass = () => {
        this.setState({
            active: false,
            notactive: true
        })
    }

    render() {

        
        
        console.log('allErrors',this.state.allErrors);
        

        const { onSubmit } = this.submit

        var buttonClass1 = `nav-link ${this.state.active ? " active show" : ""}`;
        var buttonClass2 = `nav-link ${this.state.notactive ? " active show" : ""}`;

        return (


            <Fragment>
                {this.props.employeeCreated?.isLoading ?
                    <Fragment>
                        <div className="loader-img"> <img src={LoaderImage} style={loaderStyle} /></div>
                    </Fragment> :

                    <div className="col-sm-12">
                        <div className="row justify-content-center mt-4">
                            <div className="col-sm-10 col-md-7">
                                <div className="bx-content bx-shadow">

                                    <ul className="nav nav-tabs" id="myTab" role="tablist">
                                        <li className="nav-item">
                                            <a className={buttonClass1}  href="#" id="billing-tab" data-toggle="tab" role="tab" aria-controls="billing" >Primary Info
                                            {
                                            this.state.emailError != ''
                                             &&
                                                    <i className="icon-warning-triangle text-danger text-sm"></i>}
                                            </a>
                                        </li>
                                        <li className="nav-item">
                                            <a className={buttonClass2}  href="#" id="other-tab" data-toggle="tab" role="tab" aria-controls="other">Other Details
                                        {console.log('contactNoError comp', this.state.contactNoError)}

                                                {                                        
                                            (this.state.contactNoError != ''||this.state.pincodeError != '')
                                             &&
                                                    <i className="icon-warning-triangle text-danger text-sm"></i>}
                                            </a>
                                        </li>
                                    </ul>

                                   
                                        <div className="tab-content fullwidth float-right" id="myTabContent">
                                            
                                            {/* zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz */}

                                            {this.state.page===1&&<Addemployeeprimaryinfo onSubmit={this.nextPage}
                                            pstate={this.state} 
                                            {...this.props} 
                                            departmentList={this.props?.departmentlist?.departments}
                                            toggleAddDesignationModal={this.toggleAddDesignationModal}
                                            validateMethod={this.validateMethod}
                                            handleBranchChange={this.handleBranchChange}
                                            select_org={this.select_org}
                                            select_branch={this.select_branch}
                                            errorOnChange={this.errorOnChange}
                                            _onChange={this._onChange}
                                            showEmpCreatedMessage={this.showEmpCreatedMessage}
                                            closeErrorMessage={this.closeErrorMessage}
                                            />}

                                            {this.state.page===2&&<Addemployeeotherinfo onSubmit={this.submit}
                                            pstate={this.state} 
                                            {...this.props} 
                                            errorOnChange={this.errorOnChange}
                                            previousPage={this.previousPage}
                                            handleCountryChange={this.handleCountryChange}
                                            showEmpCreatedMessage={this.showEmpCreatedMessage}
                                            closeErrorMessage={this.closeErrorMessage}
                                            />}
                                            

                                        </div>
                                        <span className="float-right m-4">
                                            {/* {this.state.active &&
                                            <button onClick={this.toggleOtherClass} className="btn btn-prmary-blue" type="button">
                                                Next
                                            </button>
                                        }
                                        {this.state.notactive &&
                                            <button className="btn btn-prmary-blue" type="submit" disabled={(this.props.designationList&&this.props.designationList.designation&&this.props.designationList.designation.length<=0)?true:false}>
                                                Invite
                                            </button>
                                        } */}

                                        
                                            {/* <button className="btn btn-prmary-blue" type="submit" >
                                                Invite
                                        </button> */}

                                        </span>
                                    
                                    {/*    DESIGNATION MODAL */
                                        this.state.showDesgnModal && <AddDesignationNew {...this.props} onCloseModal={this.toggleAddDesignationModal} />
                                    }
                                </div>
                            </div>
                        </div>
                    </div>}

            </Fragment>

        )
    }
}


AddEmployeeContent.propTypes = {
    onSubmit: PropTypes.func.isRequired
  }



export default AddEmployeeContent