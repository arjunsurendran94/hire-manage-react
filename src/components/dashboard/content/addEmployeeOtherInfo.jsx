import React, { Component, Fragment } from 'react'
import { Field, reduxForm } from 'redux-form'
import { countryReorder } from '../../../countryReorder'


const required = value => {
    return (value || typeof value === 'number' ? undefined : ' is Required')
}

const maxLength = max => value =>
    value && value.length > max ? `Must be ${max} characters or less` : undefined

const maxLength128 = maxLength(128)

const email = value =>
    value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
        ? ' is Invalid'
        : undefined

var letters = /^[a-zA-Z.]+\s*$/;
const alphabetOnly = value => {
    return (value && value.match(letters) ? undefined : ' must be only alphabets without spaces in between')
}

const renderRadioField = ({ input, label, type, checked }) => (
    <Fragment>

        <label>{label}</label>
        <input {...input} type={type} checked={checked} />

    </Fragment>
);

const renderFieldSelect = ({
    input,
    label,
    disable,
    type,
    existed,
    options,
    meta: { touched, error, warning, }
}) => {
    // debugger
    return (
        <Fragment>
            {(options && label === "Designation") &&
                <select {...input} className="fullwidth input-control">
                    <option id='no_designation' value="">Select Designation</option>
                    {options && options.designation && options.designation.map((designation, index) =>

                        <option key={index}

                            value={designation.slug} >{designation.designation_name} </option>
                    )}
                </select>
                // :<p className="text-danger">Please create a branch first.</p>
            }
            {(options && label === "Branch") &&
                <select {...input} className="fullwidth input-control" disabled={disable}>
                    <option id='no_designation' value="">Select Branch</option>
                    {options.map((branch, index) =>
                        <option key={index} value={branch.slug} >{branch.branch_name}  </option>
                    )}
                </select>
                // :<p className="text-danger">Please create a branch first.</p>
            }
            {(options && label === "Department") &&
                <select {...input} className="fullwidth input-control">
                    {console.log('options', options)
                    }
                    <option id='no_designation' value="">Select Department</option>
                    {options.map((department, index) =>
                        <option key={index} value={department.slug} >{department.department_name}  </option>
                    )}
                </select>
                // :<p className="text-danger">Please create a branch first.</p>
            }
            <div>
                {touched &&
                    ((error && <small className="text-danger">{label}{error}</small>) ||
                        (warning && <span>{warning}</span>))}
            </div>
            {/* {((existed)&&(existed.error.status == 500)) && <small class="text-danger">
            {existed.errors.error}
        </small>} */}

        </Fragment>

    )
}

const renderFieldLocationSelect = ({
    input,
    label,
    country,
    countryState,
    city,
    className,
    meta: { touched, error, warning, }
}) => {

    return (


        <Fragment>
            <select {...input} className={(touched && error) ? 'input-control border border-danger' : 'input-control'}>
                <option value="">{label}</option>
                {(country && label === "Country") &&
                    <Fragment>
                        {country.countryList.map((plan, index) => (
                            <option key={index} value={plan.id}>{plan.name_ascii}</option>
                        ))}</Fragment>
                }
                {(countryState && label === "State") &&
                    <Fragment>
                        {countryState.stateList.map((plan, index) => (
                            <option key={index} value={plan.id}>{plan.name_ascii}</option>
                        ))}</Fragment>
                }
                {(city && label === "City") &&
                    <Fragment>
                        {city.cityList.map((plan, index) => (
                            <option key={index} value={plan.id}>{plan.name_ascii}</option>
                        ))}</Fragment>
                }
            </select>
            <div>
                {touched &&
                    ((error && <small className="text-danger">{label}{error}</small>) ||
                        (warning && <span>{warning}</span>))}
            </div>
            {/* {((existed)&&(existed.error.status == 500)) && <small class="text-danger">
                {existed.errors.error}
            </small>} */}

        </Fragment>

    )
}

const mobileValid = value => {
    var mobilenoregex = /^(?!\b(0)\1+\b)(\+?\d{1,3}[. -]?)?\(?\d{3}\)?([. -]?)\d{3}\3\d{4}$/
    if (value && value.length >= 1) {
        return (mobilenoregex.test(value) ? undefined : 'Enter a Valid Mobile Number')
    }

    return undefined
}



const rendermobileField = ({ input, label, type, existed, invalid_error, id, className, accept, initialValue, name, meta: { touched, error, warning, } }) => {
    return (
        <Fragment>
            <input {...input} placeholder={label} type={type} className={(touched && error) || (invalid_error && invalid_error.phone) ? 'input-control border border-danger' : 'input-control'} id={id} accept={accept} defaultValue={initialValue} />
            {touched &&
                ((error && <small className="text-danger">{label !== "Phone number" && label}{error} with country code</small>) ||
                    (warning && <span>{warning}</span>))}

            {invalid_error && invalid_error.phone &&
                <small className="text-danger">{invalid_error.phone[0]} with country code</small>
            }
        </Fragment>
    )
}
const renderField = ({
    input,
    label,
    type,
    existed,
    pincodeError,
    meta: { touched, error, warning, }
}) => {

    return (


        <Fragment>
            {console.log('pincodeError', pincodeError)}
            <input {...input} placeholder={label} type={type} className={(touched && error) || ((existed && existed.email) || (pincodeError && pincodeError.non_field_errors)) ? 'input-control border border-danger' : 'input-control'} />
            {touched &&
                ((error && <small className="text-danger">{label}{error}</small>) ||
                    (warning && <span>{warning}</span>))}
            {((existed) && (existed.email)) && <small className="text-danger">
                {existed.email[0]}
            </small>}
            {((pincodeError) && (pincodeError.non_field_errors)) && <small className="text-danger">
                {pincodeError.non_field_errors[0]}
            </small>}
            {/* {((existed)&&(existed.error.status == 500)) && <small class="text-danger">
            {existed.errors.error}
        </small>} */}

        </Fragment>

    )
}
class Addemployeeotherinfo extends Component {
    constructor(props) {
        super(props)

        this.state = {

        }

    }

    handleCountryChange = (e) => {
        // making fields null
        
        this.props.change('state', '')
        this.props.change('city', '')

        this.props.fetchStatesList(e.target.value);

        // city error resolves after u uncomment it 
        this.props.fetchCityList(" ");
    }

    handleStateChange = (e) => {
        // making fields null
        this.props.change('city', '')

        this.props.fetchCityList(e.target.value);
    }

    render() {
        const { handleSubmit, previousPage } = this.props
        var buttonClass1 = `nav-link ${this.props.pstate.active ? " active show" : ""}`;
        var buttonClass2 = `nav-link ${this.props.pstate.notactive ? " active show" : ""}`;
        var primaryClass = `tab-pane userMail-tab fade float-left fullwidth ${this.props.pstate.active ? " active show" : ""}`;
        var otherClass = `tab-pane userMail-tab fade position-relative float-left fullwidth${this.props.pstate.notactive ? " active show" : ""}`;

        return (
            <Fragment>
                <form onSubmit={handleSubmit}>
                    <div className={otherClass} id="other" role="tabpanel" aria-labelledby="other-tab">
                        {this.props.pstate.formError && this.props.formStateValues && ("submitFailed" in this.props.formStateValues) &&
                            <div className="alert alert-dismissible fade show alert-danger mt-3">
                                <i className="icon-warning-triangle alert-icon mr-2"></i> Invalid Details
                                                    <button onClick={this.props.closeErrorMessage} type="button" className="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                        }

                        {this.props.pstate.errorMessages &&
                            <div className="alert alert-dismissible fade show alert-danger mt-3">
                                <i className="icon-warning-triangle alert-icon mr-2"></i> {this.props.pstate.errorMessages}
                                <button onClick={this.props.closeErrorMessage} type="button" className="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                        }

                        {this.props.pstate.contactNoError && <div className="alert alert-dismissible fade show alert-danger mt-3">
                            <i className="icon-warning-triangle alert-icon mr-2"></i> {this.props.pstate.contactNoError}
                            <button onClick={this.props.closeErrorMessage} type="button" className="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>}

                        {this.props.pstate.pincodeError && <div className="alert alert-dismissible fade show alert-danger mt-3">
                            <i className="icon-warning-triangle alert-icon mr-2"></i> {this.props.pstate.pincodeError}
                            <button onClick={this.props.closeErrorMessage} type="button" className="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>}

                        <div className="col-sm-12">
                            <div className="row">
                                <div className="col-md-6 pull-left" style={{ backgroundColor: "#fff" }}>
                                    <div className="row mb-3">
                                        <div className="col-sm-12 form-group">
                                            <label className="text-light-grey" htmlFor="id_phone">
                                                <label htmlFor="id_phone">Phone</label>
                                            </label>
                                            {/* <input type="tel" name="phone" placeholder="Phone number" className="input-control" id="id_phone"/>    */}
                                            <Field
                                                type="text"
                                                name="phone"
                                                component={rendermobileField}
                                                invalid_error={this.props.pstate.contactNoError}
                                                label="Phone number"
                                                onChange={() => this.props.errorOnChange('contact-no')}
                                                validate={[mobileValid]}
                                                className="input-control input-control" />
                                        </div>
                                        <div className="col-sm-12 form-group">
                                            <label className="text-light-grey" htmlFor="id_nationality">
                                                <label htmlFor="id_nationality">Nationality</label>
                                                <span className="text-danger">*</span>
                                            </label>
                                            {/* <select name="nationality" className="input-control" required="" id="id_nationality">
                                                                    <option value="" selected="">Select Country</option>
                                                                    <option value="105">India</option>
                                                                </select> */}
                                            <Field
                                                name="nationality"
                                                type="select"
                                                country={countryReorder(this.props.countryDetails)}
                                                component={renderFieldLocationSelect}
                                                className="input-control select-country"
                                                label="Country"
                                                validate={required}
                                                onChange={this.handleCountryChange} />
                                        </div>
                                        <div className="col-sm-12 form-group">
                                            <label className="text-light-grey" htmlFor="id_state">
                                                <label htmlFor="id_state">State</label>
                                                <span className="text-danger">*</span>
                                            </label>
                                            {/* <select name="state" className="input-control" required="" id="id_state">
                                                                    <option value="" selected="">Select State</option>
                                                                </select>    */}
                                            <Field
                                                name="state"
                                                type="select"
                                                countryState={this.props.stateDetails}
                                                component={renderFieldLocationSelect}
                                                className="input-control select-state"
                                                label="State"
                                                validate={required}
                                                onChange={this.handleStateChange}
                                            />
                                        </div>
                                        <div className="col-sm-12 form-group">
                                            <label className="text-light-grey" htmlFor="id_city">
                                                <label htmlFor="id_city">City</label>
                                                <span className="text-danger">*</span>
                                            </label>
                                            {/* <select name="city" className="input-control" required="" id="id_city">
                                                                    <option value="" selected="">Select City</option>
                                                                </select>                  */}
                                            <Field
                                                name="city"
                                                type="select"
                                                city={this.props.cityDetails}
                                                component={renderFieldLocationSelect}
                                                className="input-control select-city"
                                                label="City"
                                                validate={required} />
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-6 pull-left">
                                    <div className="row">
                                        <div className="col-sm-12 form-group">
                                            <label className="text-light-grey" htmlFor="id_house_name">
                                                <label htmlFor="id_house_name">House name / flat No</label>
                                            </label>
                                            <Field
                                                type="text"
                                                name="house_name"
                                                component={renderField}
                                                label="House name / flat No" />
                                        </div>
                                        <div className="col-sm-12 form-group">
                                            <label className="text-light-grey" htmlFor="id_street_name">
                                                <label htmlFor="id_street_name">Street Name / No</label>
                                            </label>
                                            <Field
                                                type="text"
                                                name="street_name"
                                                component={renderField}
                                                label="Street Name / No" />
                                        </div>
                                        <div className="col-sm-12 form-group">
                                            <label className="text-light-grey" htmlFor="id_locality_name">
                                                <label htmlFor="id_locality_name">Locality Name / No</label>
                                            </label>
                                            <Field
                                                type="text"
                                                name="locality_name"
                                                component={renderField}
                                                label="Locality Name / No" />
                                        </div>
                                        <div className="col-sm-12 form-group">
                                            <label className="text-light-grey" htmlFor="id_pin_code">
                                                <label htmlFor="id_pin_code">Pin Code</label>
                                            </label>
                                            <Field
                                                type="text"
                                                name="pin_code"
                                                component={renderField}
                                                label="Zip Code"
                                                onChange={() => this.props.errorOnChange('pincode')}
                                                pincodeError={this.props.pstate.pincodeError} />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button type="button" onClick={previousPage} className="btn btn-prmary-blue float-left">Previous</button>
                        <button type="submit" className="btn btn-prmary-blue float-right">Invite</button>
                    </div>
                </form>
            </Fragment>
        )
    }
}
Addemployeeotherinfo = reduxForm({
    form: 'addEmployeeForm',
    destroyOnUnmount: false, // <------ preserve form data
    forceUnregisterOnUnmount: true, // <------ unregister fields on unmount
})(Addemployeeotherinfo)
export default Addemployeeotherinfo