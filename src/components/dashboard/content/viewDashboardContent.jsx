import React, { Component, Fragment } from "react";
import { Field, FieldArray, reduxForm } from "redux-form";
import { Redirect } from "react-router-dom";
import { alphabetOnly } from "../../../constants/validations";
import * as Helper from "../common/helper/helper";
import LoaderImage from "../../../static/assets/image/loader.gif";
import Select from "react-select";
import DatePicker from "react-datepicker";
import AddTaskModal from "../modal/addTaskModal";
import { Link } from "react-router-dom";
import ErrorMessage from "../../common/ErrorMessage";
import Moment from "moment";
import momentDurationFormatSetup from "moment-duration-format";
import TimeField from "react-simple-timefield";
import DurationInput from "react-duration";
import ReactTooltip from 'react-tooltip';

import timings from '../common/helper/timeframes'

var loaderStyle = {
  width: "3%",
  position: "fixed",
  top: "50%",
  left: "57%",
};
const required = (value) => {
  return value || typeof value === "number" ? undefined : " is Required";
};
const positiveInteger = (value) => {
  var positiveregex = /^[1-9]\d*$/;
  return value && positiveregex.test(value) ? undefined : " is Invalid";
};
const mobileValid = (value) => {
  var mobilenoregex = /^(?!\b(0)\1+\b)(\+?\d{1,3}[. -]?)?\(?\d{3}\)?([. -]?)\d{3}\3\d{4}$/;
  if (value && value.length >= 1) {
    return mobilenoregex.test(value) ? undefined : " is Invalid";
  }
  return undefined;
};
const email = (value) =>
  value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
    ? " is Invalid"
    : undefined;
const websiteValiadtion = (value) => {
  if (value) {
    var pattern = new RegExp(
      "^(https?:\\/\\/)" + // protocol
      "((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|" + // domain name
      "((\\d{1,3}\\.){3}\\d{1,3}))" + // OR ip (v4) address
      "(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*" + // port and path
      "(\\?[;&a-z\\d%_.~+=-]*)?" + // query string
      "(\\#[-a-z\\d_]*)?$",
      "i"
    ); // fragment locator
    return !!pattern.test(value)
      ? undefined
      : "Enter a valid url ( eg:https://websitebuilders.com )";
  }
};

const currency = [
  { value: "abc", label: "asdf" },
  { value: "wer", label: "uji" },
];

const renderTextField = ({
  input,
  label,
  type,

  meta: { touched, error, warning },
}) => {
  return (
    <Fragment>
      <textarea
        {...input}
        placeholder={label}
        type={type}
        className={
          touched && error
            ? "fullwidth input-control border border-danger"
            : "fullwidth input-control"
        }
      />
      {touched &&
        ((error && (
          <small className="text-danger">
            {label}
            {error}
          </small>
        )) ||
          (warning && <span>{warning}</span>))}
    </Fragment>
  );
};



let taskFields = [],
  OldTasks = [],
  NewTasks = [];

var dateObj = new Date();
var Currentdate =
  dateObj.getFullYear() +
  "-" +
  (dateObj.getMonth() + 1) +
  "-" +
  dateObj.getDate();
var time =
  dateObj.getHours() + ":" + dateObj.getMinutes() + ":" + dateObj.getSeconds();
var currentDateTime = Currentdate + " " + time;

var firstDay = new Date(dateObj.getFullYear(), dateObj.getMonth(), 1);
var lastDay = new Date(dateObj.getFullYear(), dateObj.getMonth() + 1, 0);










class ViewProjectContent extends Component {
  constructor(props) {
    super(props);
    var date = new Date();

    var formatedDate = `${date.getFullYear()}-${(
      "0" +
      (date.getMonth() + 1)
    ).slice(-2)}-${("0" + date.getDate()).slice(-2)}`;

    this.state = {
      show_branch: false,
      success_message: false,
      existed_error: "",
      other_details: false,
      checked: false,
      isOpen: false,
      emailValue: "",
      passwordValue: "",
      select_client: false,
      select_internal: false,
      startingDate: formatedDate,
      startingDateError: "",
      editModal: false,
      editPart: "",
      index: "",
      comments: null,
      commentError: null,
      employeeList: "",
      expandNav: false,
      task_name: "",
      assigned: "",
      due_date: "",
      task_board: "",
      task_description: "",
      errors: {
        task_name: "",
        assigned: "",
        due_date: "",
        task_description: "",
        task_board: "",
      },
      taskResponse: true,
      type: "",
      taskObj: "",
      subtaskObj: "",
      subtasks: "",
      errorMessages: [],
      isClicked: false,
      historyLog: false,
      timeLog: false,
      startTime: true,
      individualEdit: "",
      taskIndex: "",

      add_start_date: "",
      add_end_date: Moment(date).format("YYYY-MM-DD"),
      add_start_time: "",
      add_end_time: "",

      duration_error: "",

      add_start_date_error: "",
      add_end_date_error: "",
      add_start_time_error: "",
      add_end_time_error: "",
      started: false,
      seconds: "",
      hours: "",
      minutes: "",

      subTaskLog: [],
      taskComments: [],
      taskComment: "",
      taskCommentError: "",
      listEdit: false,
      taskType: localStorage.getItem("user_type") === "admin" ? 'allTask' : 'myTask',
      timeLogObj: '',
      startTimerTask: '',
      viewTimelog: false,
      calendarView: false,
      weeklyView: false,
      monthlyView: true,
      projectView: false,
      project: "",
      employee: localStorage.getItem("user_type") === "admin" ? "" : localStorage.getItem('admin_employee_slug'),
      allTasks: [],
      allTaskSet: false,
      timings: timings(),
      backTime: [],
      selected: "",
      totalProductiveHours: "",
      totalPmApprovedHours: "",
      editModal: false,
      editPart: "",
      index: "",
      obj:"",
      selectedObj:"",
      availbleResources:[],
      param:false
    };
  }



  select_client = () => {
    this.setState({ select_client: true, select_internal: false });
  };

  closeSuccessMessage = () => {
    this.setState({
      success_message: false,
    });
  };





  handleIndividualChange = (e) => {

    let taskObj = { ...this.state.taskObj }
    // this.setState((prevState=>({taskObj:{...prevState.taskObj,task_name:e.target.value}})))
    taskObj[e.target.name] = e.target.value
    console.log("hi")
    // debugger
    this.setState({ taskObj: taskObj })
  }

  handleIndividualStatusOrAssignedChange = (e) => {
    
    this.props.listTimeLog({
      ...(this.state.project != "" && { project: this.state.project }),
      ...(this.state.employee != "" && { employee: this.state.employee }),
      ...(this.state.add_start_date != "" && { start_time: this.state.add_start_date }),
      ...(this.state.add_end_date != "" && { end_time: this.state.add_end_date }),
      task_grp: e.value
    })
  }

  // addTimeSubmit=()=>{

  //   let add_end_date_error='',add_end_time_error='',add_start_date_error='',add_start_time_error='',valid=true,duration_error=''
  //   let errors={...this.state.errors}

  //   if(!this.state.add_start_date){
  //      add_start_date_error='Required'
  //      valid=false
  //   }
  //   // if(!this.state.add_start_time){
  //   //    add_start_time_error='Required'
  //   //    valid=false
  //   // }
  //   if(!this.state.add_end_date){
  //      add_end_date_error='Required'
  //      valid=false
  //   }
  //   // if(!this.state.add_end_time){
  //   //    add_end_time_error='Required'
  //   //    valid=false
  //   // }
  //   if(this.state.taskObj?.assigned===null){
  //      valid=false
  //      errors.assigned='Please Select a Assignee '
  //   }
  //   if(!this.state.seconds||!this.state.minutes||!this.state.hours){
  //      valid=false
  //      duration_error='Invalid Time format'
  //   }

  //   if(valid){
  //      // debugger
  //      let data={}
  //      data.employee=this.state.taskObj?.assigned?.slug
  //      data.task=this.state.taskObj?.slug
  //      data.project=this.state.taskObj?.project
  //      let start_time=this.state.add_start_date 
  //      let end_time=this.state.add_end_date 


  //      // let startDateTimeMom=Moment(start_time)
  //      // let endTimeMom=Moment(end_time)
  //      // let dif=endTimeMom.diff(startDateTimeMom)
  //      // let dur=Moment.duration(dif)

  //      // let minutes=dur.minutes()
  //      // let seconds=dur.seconds()

  //      // // var s = Math.floor(d.asHours()) + moment.utc(ms).format(":mm:ss");

  //      // let calculatedDuration = Math.floor(dur.asHours()) + ':'+minutes+':'+seconds

  //      // console.log('duration',calculatedDuration);

  //      data.start_time=start_time +" "+"00:00:00" 
  //      data.end_time=end_time +" "+"00:00:00"
  //      data.worked_hours=`${this.state.hours}:${this.state.minutes}:${this.state.seconds}`
  //      data.productive_hours=`${this.state.hours}:${this.state.minutes}:${this.state.seconds}`
  //      data.billable_hours=`${this.state.hours}:${this.state.minutes}:${this.state.seconds}`

  //      // console.log('FFFFFFFFFFFFFF',data);

  //      this.setState({isClicked:!this.state.isClicked},()=>this.props.addTime(data))
  //   }
  //     else{
  //      this.setState({add_start_date_error:add_start_date_error,add_start_time_error:add_start_time_error,
  //      add_end_date_error:add_end_date_error,add_end_time_error:add_end_time_error,errors:errors,duration_error:duration_error})
  //     }

  // }
  formatString = (str) => {
    // debugger
    if (str != '') {
      let str1 = str.replace(/_/g, '  ')
      str1 = str1.charAt(0).toUpperCase() + str1.substr(1).toLowerCase()
      return str1
    }
  }


  simulateClick = (e) => {
    document.getElementById("submit-btn").click();
  };
  handleBranchChange = (e) => {
    this.props.change("Department", "");
    this.props.retrieveDepartments(e.target.value);
  };


  submit = (values) => {

    this.setState(
      {
        existed_error: "",
        pincodeError: "",
        work_phone_number_error: "",
        mobile_number_error: "",
        website_error: "",
        facebook_error: "",
      },
      () => {
        this.props.addClient(values);
      }
    );
  };

  dateChange = (e) => {
    this.setState({ startingDate: e.target.value });
  };

  componentDidMount() {

    console.log('sssssssssssssssssssssssssssssssss', localStorage.getItem("user_type") === "admin" ? "" : localStorage.getItem('admin_employee_slug'));



    this.props.listClients();
    let slug =
      this.props.match &&
      this.props.match.params &&
      this.props.match.params.project_slug;

    // this.props.retrieveProject(slug);

    // this.props.listProjectComment(slug);
    this.props.listEmployees("");
    // debugger



    this.props.listProjects()

    // this.props.listTimeLog()


    if (localStorage.getItem('user_type') === 'employee') {
      this.props.listMyTasks({
        project: slug,
        assigned: localStorage.getItem("profile_slug"),
      });
    }

    this.props.listTasks();

    document.addEventListener("mousedown", this.handleClick, false);
  }

  componentWillUnmount() {
    document.removeEventListener("mousedown", this.handleClick, false);
  }

  componentDidUpdate(prevProps,prevState) {


    if (
      prevProps.tasks != this.props.tasks &&
      this.props.tasks &&
      this.props.tasks.status === 400
    ) {
      let Errors = [],
        ErrorObj = {};
      let errors =
        this.props.tasks &&
        this.props.tasks.error_obj &&
        this.props.tasks.error_obj.data;
      Object.keys(errors).map(
        (item) => (
          Errors.push(errors[item][0]), (ErrorObj[item] = errors[item][0])
        )
      );
      this.setState({ errorMessages: Errors, errors: ErrorObj });
    }
    if (
      prevProps.tasks != this.props.tasks &&
      this.props.tasks &&
      this.props.tasks.status === 403
    ) {
      let Errors = ["Permission Denied"];
      this.setState({ errorMessages: Errors });
    }
    if (
      prevProps.tasks != this.props.tasks &&
      this.props.tasks &&
      this.props.tasks.status === 500
    ) {
      let Errors = ["Internal Server Error"];
      this.setState({ errorMessages: Errors });
    }

    if (
      prevProps.tasks != this.props.tasks &&
      this.props.tasks?.status === "list_timelog_success"
    ) {

    }
    if (
      prevProps.tasks != this.props.tasks &&
      this.props.tasks?.status === "listing_subtasklog_success"
    ) {
      this.setState({ subTaskLog: this.props.tasks?.subTaskLog });
    }
    if (
      prevProps.tasks != this.props.tasks &&
      (this.props.tasks?.status === "add_task_comment_success" ||
        "listing_task_comment_success")
    ) {
      this.setState({
        taskComments: this.props.tasks?.taskComments,
        taskComment: "",
      });
    }



    if (this.props.tasks != prevProps.tasks && this.props.tasks &&
      this.props.tasks.status === ('create_task_success' || 'edit_time_success')) {
      // this.setState({taskResponse:false})

      this.closeNav()
    }

    if (this.props.tasks != prevProps.tasks && this.props.tasks &&
      this.props.tasks.status === 'edit_time_success') {
      // this.setState({taskResponse:false})

      this.closeEditModal()

      
    }

    console.log('this.props.tasks?.tasks',this.props.tasks?.tasks);
    
    if ( prevProps.tasks!=this.props.tasks&&this.props.tasks?.tasks?.length > 0) {
      this.setState({ allTasks: this.props.tasks?.tasks, allTaskSet: true })
    }

    // CALENder



    if (
      this.props.tasks?.timelog != prevProps.tasks?.timelog && this.state.calendarView) {
      console.log('prevProps.tasks?.timelog', prevProps.tasks?.timelog)
      console.log('this.props.tasks?.timelog', this.props.tasks?.timelog)

      console.log('this.state.calendarView', this.state.calendarView);


      let format = 'HH:mm:ss'
      let modifiedTimmings = timings(this.state.allTasks)
      // let ft=this.props.tasks?.timelog?.filter((timelogObj)=>timelogObj.end_time!=null)

      let a = this.props.tasks?.timelog?.filter((timelogObj) => timelogObj.end_time != null)?.forEach((timelogObj) => {
        console.log('inside for lopp');

        let start_time = Moment(timelogObj.start_time_format).format(format)
        let end_time = Moment(timelogObj.end_time_format).format(format)
        let tracking = timelogObj.tracking
        let task = this.state.allTasks?.filter(i => i.slug === timelogObj.task)[0]?.task_name
        console.log("taskkkkk",this.state.allTasks?.filter(i => i.slug === timelogObj.task)[0]?.task_name)

        modifiedTimmings = modifiedTimmings.map((i) => {

          let startPercent = Moment(start_time, format).diff(Moment(i.startHourlyTf, format), 'minutes')
          let endPercent = Moment(end_time, format).diff(Moment(i.startHourlyTf, format), 'minutes')
          let width;

          if ((i.startHourlyTf <= start_time && start_time <= i.endHourlyTf) && (i.startHourlyTf <= end_time && end_time <= i.endHourlyTf)) {
            return {
              ...i, background: tracking ? 'auto-tracked' : 'manual-time',
              start_time: start_time,
              end_time: end_time,
              task: task,
              worked_hours: timelogObj.worked_hours,
              endPercent: endPercent * 10,
              startPercent: startPercent * 10,
              width: (10 - (startPercent)) * 12,
              color: tracking ? 'green' : '#e2e25e'
            }
          }

          if ((i.startHourlyTf <= start_time && start_time <= i.endHourlyTf)) {

            // let startPercent=Moment(start_time,format).diff(i.startHourlyTf,'minutes')

            return {
              ...i, background: tracking ? 'auto-tracked' : 'manual-time',
              start_time: start_time,
              end_time: end_time,
              task: task,
              worked_hours: timelogObj.worked_hours,
              // endPercent:endPercent*10,
              startPercent: startPercent * 10,
              width: (10 - (startPercent)) * 12,
              color: tracking ? 'green' : '#e2e25e'
            }
          }
          if (i.startHourlyTf <= end_time && end_time <= i.endHourlyTf) {
            return {
              ...i, background: tracking ? 'auto-tracked' : 'manual-time',
              start_time: start_time,
              end_time: end_time,
              task: task,
              worked_hours: timelogObj.worked_hours,

              width: endPercent * 12,
              endPercent: endPercent * 10,
              color: tracking ? 'green' : '#e2e25e',

            }
          }
          if (i.startHourlyTf >= start_time && i.endHourlyTf <= end_time) {
            return {
              ...i, background: tracking ? 'auto-tracked' : 'manual-time',
              start_time: start_time,
              end_time: end_time,
              task: task,
              worked_hours: timelogObj.worked_hours,

              color: tracking ? 'green' : '#e2e25e'
            }
          }
          else {
            return i
          }

          // debugger
        });

      })
      this.setState({ timings: modifiedTimmings })

    }

    if (this.props.tasks?.timelog != prevProps.tasks?.timelog) {
      let startTime = "00:00:00"
      let hours = 0

      let pmstartTime = "00:00:00"
      let pmHours = 0

      let a = this.props.tasks?.timelog?.forEach(i => {

        // debugger

        var now = Moment.duration(startTime, 'HH:mm:ss').asHours()
        var b = Moment.duration(i.productive_hours, 'HH:mm:ss').asHours()

        var pmnow = Moment.duration(pmstartTime, 'HH:mm:ss').asHours()
        var c = Moment.duration(i.pm_approved, 'HH:mm:ss').asHours()


        hours = hours + (now + b)
        pmHours = pmHours + (pmnow + c)
      })

      console.log('startTimestartTimestartTimestartTimestartTime', hours.toFixed(1));

      this.setState({ totalProductiveHours: hours?.toFixed(1), totalPmApprovedHours: pmHours?.toFixed(1) })

    }

    if (this.state.project!=prevState.project||this.props.tasks != prevProps.tasks) {
      // debugger

      let tasksList = []

      // let unique = [...new Set(this.props.tasks?.tasks?.map(i => { return { label: i.task_name, value: i.slug }}))];
      
      let tasksListUnique=[...new Set(this.props.tasks?.timelog?.map(i=>i.task))]
      
      tasksList= tasksListUnique?.map((timeLog) => {
        // debugger
        return { 
                  label: this.state.allTasks?.filter(i => i.slug === timeLog)[0]?.task_name,
                  value: timeLog
                }
      })

      
      // if(!this.state.project && !this.state.employee){
      //   tasksList = this.props.tasks?.tasks?.map((i) => { return { label: i.task_name, value: i.slug } })

      // }
      // else if(!this.state.project && this.state.employee!=""){
      //   tasksList = this.props.tasks?.tasks?.filter(i=>(i?.assigned?.slug===this.state.employee))?.map((i) => { return { label: i.task_name, value: i.slug } })
      // }
      // else if(this.state.project!="" && !this.state.employee){
      //   tasksList = this.props.tasks?.tasks?.filter(i=>(i.project===this.state.project))?.map((i) => { return { label: i.task_name, value: i.slug } })

      // }
      // else{
      //   tasksList = this.props.tasks?.tasks?.filter(i=>(i.project===this.state.project)).filter(i=>(i?.assigned?.slug===this.state.employee))?.map((i) => { return { label: i.task_name, value: i.slug } })
      // }

      console.log('tasksListtasksListtasksListtasksListtasksList',tasksList)
      

      this.setState({ tasksList: tasksList })
    
    }

  }

  submit = (values) => {
    console.log(values);
  };


  convertToLocalTime = (obj) => {
    let dateTime = new Date(obj);
    dateTime.setHours(dateTime.getHours() + 5);
    dateTime.setMinutes(dateTime.getMinutes() + 30);

    return Moment(dateTime).format("DD-MM-YYYY HH:mm:ss");
  };

  convertToLocalTimeFormat = (obj) => {

    let dateTime = new Date(obj);
    // dateTime.setHours(dateTime.getHours() + 5);
    // dateTime.setMinutes(dateTime.getMinutes() + 30);

    return Moment(dateTime).format("h:mm:ss A");
  };

  convertToDateFormat = (obj) => {
    let dateTime = new Date(obj);
    // dateTime.setHours(dateTime.getHours() + 5);
    // dateTime.setMinutes(dateTime.getMinutes() + 30);

    return Moment(dateTime).format("DD-MM-YYYY");
  };

  parseLog = (obj, type, employeeList) => {
    let data = JSON.parse(obj.replace(/\'/gi, '"'));
    if (type === "field") {
      taskFields =
        data &&
        data.map((i) =>
          i.field === "task_board" ? "status" : i.field.replace(/\_/g, " ")
        );
    }

    if (type === "old") {
      OldTasks =
        data &&
        data.map(function (i) {
          if (i.field === "assigned" && i.old != "") {
            return employeeList.filter((emp) => emp.id === i.old)[0].label;
          }
          if (i.field === "due_date" && i.old != "") {
            return Moment(i.old).format("DD-MM-YYYY");
          } else {
            return i.old.replace(/\_/g, " ");
          }
        });
    }
    if (type === "new") {
      NewTasks =
        data &&
        data.map(function (i) {
          if (i.field === "assigned" && i.new != "") {
            return employeeList.filter((emp) => emp.id === i.new)[0].label;
          }
          if (i.field === "due_date" && i.new != "") {
            return Moment(i.new).format("DD-MM-YYYY");
          } else {
            return i.new.replace(/\_/g, " ");
          }
        });
    }
  };

  submitTask = () => {
    this.validateAddTask();
  };



  handleAssigneeChange = (e) => {
    let errors = { ...this.state.errors };
    errors.assigned = "";
    this.setState({ assigned: e.value, errors: errors });
  };

  handleStatusChange = (e) => {
    let errors = { ...this.state.errors };
    errors.task_board = "";
    this.setState({ task_board: e.value, errors: errors });
  };

  handleChange = (e) => {
    let errors = { ...this.state.errors };
    errors[e.target.name] = "";
    this.setState({ [e.target.name]: e.target.value, errors: errors });
  };

  validateIndividaual = () => {
    // debugger
    let valid = true,
      task_name_error,
      task_description_error;

    if (!this.state.taskObj.task_name) {
      task_name_error = "Name is required";
      valid = false;
    }
    if (!this.state.taskObj.task_description) {
      task_description_error = "Description is required";
      valid = false;
    }
    this.setState((prevState) => ({
      errors: {
        ...prevState.errors,
        task_name: task_name_error,
        task_description: task_description_error,
      },
    }));
    return valid;
  };
  viewTimelog = () => {
    // debugger
    let timelog = this.state.viewTimelog
    this.setState({
      viewTimelog: !timelog,
      calendarView: false,
      // project:"",
      employee: localStorage.getItem("user_type") === "admin" ? "" : localStorage.getItem('admin_employee_slug'),
      add_start_date: Moment(firstDay).format("YYYY-MM-DD"),
      add_end_date: Moment(lastDay).format("YYYY-MM-DD")
    }, () => {
      (() => this.props.listTimeLog({
        ...(this.state.project != "" && { project: this.state.project }),
        ...(this.state.employee != "" && { employee: this.state.employee }),
        start_time: this.state.add_start_date,
        end_time: this.state.add_end_date
      })
      )();
      (() => this.monthlyView())()
    }
    )
  }
  weeklyView = () => {
    this.setState({
      weeklyView: true, monthlyView: false, projectView: false, selected: 'weeks',
      add_start_date: Moment().startOf('week').format("YYYY-MM-DD"),
      add_end_date: Moment().endOf('week').format("YYYY-MM-DD")
    }, () => this.callListTimeLog(this.state.add_start_date, this.state.add_end_date))
  }

  monthlyView = () => {
    this.setState({
      weeklyView: false, monthlyView: true, projectView: false, selected: 'months',
      add_start_date: Moment(firstDay).format("YYYY-MM-DD"),
      add_end_date: Moment(lastDay).format("YYYY-MM-DD")
    },
      () => this.callListTimeLog(this.state.add_start_date, this.state.add_end_date))
  }

  projectView = (projectList) => {
    this.setState({
      weeklyView: false, monthlyView: false, projectView: true, selected: 'project',
      project: projectList?.find(i => i.value != "")?.value,
    }, () => this.callListTimeLog("", "")
    )
  }


  calendarView = (project, employee) => {
    // debugger
    let calendarView = this.state.calendarView
    this.setState({
      calendarView: !calendarView,
      viewTimelog: false,
      project: project,
      employee: employee,

      add_start_date: Moment(dateObj).format("YYYY-MM-DD")
    }, () => this.props.listTimeLog({
      ...(this.state.project != "" && { project: this.state.project }),
      ...(this.state.employee != "" && { employee: this.state.employee }),
      start_time: this.state.add_start_date,
      end_time: this.state.add_start_date
    }))
  }

  handleClick = (e) => {
    
    if (!(this.node && this.node.contains(e.target))) {
      this.setState({ individualEdit: "" })
    }

  };

  formatString = (str) => {
    // debugger
    if (str != "") {
      let str1 = str.replace(/_/g, "  ");
      str1 = str1.charAt(0).toUpperCase() + str1.substr(1).toLowerCase();
      return str1;
    }
  };

  addTaskModal = (editPart, index,obj) => {
  let availbleResources=[]

  let a=this.props.projectData?.projects.filter(function (e) {
    
      if(e.project.slug === obj.project){
        availbleResources=e.financial_info?.resources?.map(
          function(res){
          
          return{name: "employee",
          value: res?.resource_assigned?.slug,
          label: res?.resource_assigned?.first_name + " " + res?.resource_assigned?.last_name,
          id: res?.resource_assigned?.id}
        
        })
      }
        
      });

      console.log("availbleResourcesavailbleResources",availbleResources)
    this.setState({ editModal: true, editPart: editPart, index: index ,obj:obj,availbleResources:availbleResources});
  }

    // if (this.props.employees?.allEmployees?.response?.length > 0) {
    //   this.props.employees.allEmployees.response.filter(res => !res.is_owner).map((res) =>
    //     employeeList.push({
    //       name: "employee",
    //       value: res?.slug,
    //       label: res?.first_name + " " + res?.last_name,
    //       id: res?.id,
    //     })
    //   );
    //   // this.setState({employeeList:employeeList})
    // }

    


  closeEditModal = () => {
    this.setState({ editModal: false, editPart: "", index: "" },()=>this.props.listTimeLog({
      ...(this.state.project != "" && { project: this.state.project }),
      ...(this.state.employee != "" && { employee: this.state.employee }),
      ...(this.state.add_start_date != "" && { start_time: this.state.add_start_date }),
      ...(this.state.add_end_date != "" && { end_time: this.state.add_end_date }),
      ...(this.state.calendarView && { end_time: this.state.add_start_date }),
    }));
  };

  handleEmployeeProject = (e, type) => {
    //   let data={...(this.state.project!=""&&{project:this.state.project}),
    //   ...(this.state.employee!=""&&{assigned:this.state.employee})
    // }
    let name, value;
    if (type === 'select') {
      name = e.name
      value = e.value
    }
    else {
      name = e.target.name
      value = e.target.value
    }

    if (this.state.taskType === "myTask" && !this.state.viewTimelog) {

      this.setState({ [e.name]: e.value, mytakemp: e.value },
        () => this.props.listMyTasks({
          ...(this.state.project != "" && { project: this.state.project }),
          ...(this.state.employee != "" && { assigned: this.state.employee })

        }))
    }
    if (this.state.taskType === "allTask" && !this.state.viewTimelog) {
      this.setState({ [e.name]: e.value, allTaskemp: e.label },
        () => this.props.listTasks({
          ...(this.state.project != "" && { project: this.state.project }),
          ...(this.state.employee != "" && { assigned: this.state.employee })
        }))
    }
    if (this.state.viewTimelog) {
      this.setState({ [e.name]: e.value, viewTimeemp: e.label },
        () => this.props.listTimeLog({
          ...(this.state.project != "" && { project: this.state.project }),
          ...(this.state.employee != "" && { employee: this.state.employee }),
          ...(this.state.add_start_date != "" && { start_time: this.state.add_start_date }),
          ...(this.state.add_end_date != "" && { end_time: this.state.add_end_date }),
        }))
    }
    if (this.state.calendarView) {
      this.setState(
        {
          [e.name]: e.value, calendarViewemp: e.label
        }, () => this.props.listTimeLog({
          ...(this.state.project != "" && { project: this.state.project }),
          ...(this.state.employee != "" && { employee: this.state.employee }),
          ...(this.state.add_start_date != "" && { start_time: this.state.add_start_date }),
          ...(this.state.add_end_date != "" && { end_time: this.state.add_end_date }),
          ...(this.state.calendarView && { end_time: this.state.add_start_date }),
        }))
    }
    if (type === 'date') {
      this.setState({
        [name]: value
      }, () => this.props.listTimeLog({
        ...(this.state.project != "" && { project: this.state.project }),
        ...(this.state.employee != "" && { employee: this.state.employee }),
        ...(this.state.add_start_date != "" && { start_time: this.state.add_start_date }),
        ...(this.state.add_end_date != "" && { end_time: this.state.add_end_date }),
        ...(this.state.calendarView && { end_time: Moment(value).format("YYYY-MM-DD") }),
      }))
    }

  }

  // handleDateChange=(e)=>{
  //   // debugger
  //   let date=e.target.value
  //   this.setState({
  //     [e.target.name]:date
  //   },()=>this.props.listTimeLog({...(this.state.project!=""&&{project:this.state.project}),
  //   ...(this.state.employee!=""&&{employee:this.state.employee}),
  //   ...(this.state.add_start_date!=""&&{start_time:this.state.add_start_date}),
  //   ...(this.state.add_end_date!=""&&{end_time:this.state.add_end_date}),
  //   ...(this.state.calendarView&&{end_time:Moment(date).format("YYYY-MM-DD")})
  // }))
  // }

  handleCalenderView = (cbTime) => {

    let foundTime = []

    var format = 'HH:mm:ss'



    let cbTime1 = Moment(cbTime, "h:mm A").format(format)
    let cbTime2 = Moment(cbTime1, format).add(9, 'minutes').add(59, 'seconds').format(format)

    // HEREeeeeeeeeeeeeeeeeeeeeeeeeeeee
    let af = this.state.backTime?.find(i => cbTime1 >= i.start_time && cbTime1 <= i.end_time)

    if (af) {


      let backGroundcolor = af.tracking ? 'green' : 'yellow'
      return backGroundcolor
    }
    else {
      return 'white'
    }


  }

  insideTimeFrame = (start) => {
    let inside = [];
    let final = start + 6

    for (let i = start; i < final; i++) {
      inside.push(

        <td class="is-tracked" >
          {/* <p > */}
          <div class={this.state.timings[i]?.background} >
            <div class="popover-banner">

              <div class="is-tracked-info show-desc-popover"
                style={{
                  // background: "linear-gradient(to right,  #9c9e9f 0%,#9c9e9f 50%,#33ccff 50%,#33ccff 100%)",
                  textAlign: this.state.timings[i].startPercent === 0 && this.state.timings[i].endPercent !== 100 && this.state.timings[i].endPercent !== 0 ? "initial" : this.state.timings[i].startPercent === 0 && this.state.timings[i].endPercent == 100 || this.state.timings[i].endPercent == 0 ? "center" : "end"
                }} data-timestamp="1601344800">

                {this.state.timings[i]?.task ? <Fragment>

                  {this.state.timings[i].startPercent === 0 && this.state.timings[i].endPercent === 100 && <Fragment>
                    <div data-placement="top"

                      title={`${this.state.timings[i]?.task}  ${this.state.timings[i]?.worked_hours}  `} >
                      {this.state.timings[i]?.timeframe}



                    </div>
                  </Fragment>}
                  {this.state.timings[i].startPercent === 0 && this.state.timings[i].endPercent === 0 && <Fragment>
                    <div data-placement="top" style={{ backgroundColor: "#e9e9e9", color: "black" }}

                    >

                      {this.state.timings[i]?.timeframe}


                    </div>
                  </Fragment>}
                  {this.state.timings[i].startPercent !== 0 && this.state.timings[i].endPercent === 100 && <Fragment>
                    <div style={{
                      background: `linear-gradient(to right,  #e9e9e9 0%,#e9e9e9 ${this.state.timings[i].startPercent}%,${this.state.timings[i]?.color} ${this.state.timings[i].startPercent}%, ${this.state.timings[i]?.color} 100%)`, textAlign: "center", color: "black"
                    }} title={`${this.state.timings[i]?.task}  ${this.state.timings[i]?.worked_hours}  `} > {this.state.timings[i]?.timeframe} </div>
                    {/* <div   style={{display:"inline-block",backgroundColor:"#e9e9e9",color:"black",width:(120-this.state.timings[i]?.width)}}
                
                > 
                         
                         hiii3
                         
           
                           </div> <div title={`${this.state.timings[i]?.task}  ${this.state.timings[i]?.worked_hours}  `}style={{display:"inline-block",width:this.state.timings[i]?.width,color:"red"}}
                 
                 >  
                          
                           helo 3
                          
            
                            </div> */}
                  </Fragment>}
                  {this.state.timings[i].startPercent === 0 && this.state.timings[i].endPercent !== 100 && this.state.timings[i].endPercent !== 0 && <Fragment>

                    <div style={{
                      color: "black",
                      background: `linear-gradient(to right,  ${this.state.timings[i]?.color} 0%,${this.state.timings[i]?.color} ${this.state.timings[i].endPercent}%,#e9e9e9 ${this.state.timings[i].endPercent}%, #e9e9e9 100%)`, textAlign: "center"
                    }} title={`${this.state.timings[i]?.task}  ${this.state.timings[i]?.worked_hours}  `} > {this.state.timings[i]?.timeframe} </div>
                    {/* <div title={`${this.state.timings[i]?.task}  ${this.state.timings[i]?.worked_hours}  `}style={{display:"inline-block",width:this.state.timings[i]?.width,color:"white"}}
                
                >  
                         
                          hi 1
                         
           
                           </div> 
                           <div   style={{display:"inline-block",width:(120-this.state.timings[i]?.width),backgroundColor:"#e9e9e9",color:"black"}}
                 
                 >  
                           
                         helooo 1
                          
            
                            </div> */}
                  </Fragment>}
                  {this.state.timings[i].startPercent !== 0 && this.state.timings[i].endPercent !== 100 && this.state.timings[i].endPercent !== 0 && <Fragment>
                    <div style={{
                      color: "black",
                      background: `linear-gradient(to right,  #e9e9e9 0%, #e9e9e9 ${this.state.timings[i].startPercent}%,${this.state.timings[i].color} ${this.state.timings[i].startPercent}%, ${this.state.timings[i].color} ${this.state.timings[i].endPercent}%,#e9e9e9 ${this.state.timings[i].endPercent}%, #e9e9e9 100%)`, textAlign: "center"
                    }} title={`${this.state.timings[i]?.task}  ${this.state.timings[i]?.worked_hours}  `} > {this.state.timings[i]?.timeframe} </div>
                    {/* <div title={`${this.state.timings[i]?.task}  ${this.state.timings[i]?.worked_hours}  `}style={{display:"inline-block",width:this.state.timings[i]?.width,color:"white"}}
                
                > 
                         
                          hi 2
                         
           
                           </div> <div   style={{display:"inline-block",width:(120-this.state.timings[i]?.width),backgroundColor:"#e9e9e9",color:"black"}}
                 
                 >  
                           
                         helooo 2
                          
            
                            </div> */}
                  </Fragment>}
                </Fragment>
                  : <div data-placement="top"

                    title={`${this.state.timings[i]?.task}  ${this.state.timings[i]?.worked_hours}  `} >
                    {this.state.timings[i]?.timeframe}



                  </div>}

              </div>

            </div>
          </div>
          {/* </p> */}

        </td>

      )
    }

    console.log('inside', this.state.timings);

    return (inside)

  }

  Product = () => {

    let content = [];
    var timeValue = ["12:00 am", "01:00 am", "02:00 am", "03:00 am", "04:00 am", "05:00 am",
      "06:00 am", "07:00 am", "08:00 am", "09:00 am", "10:00 am", "11:00 am", "12:00 pm",
      "01:00 pm", "02:00 pm", "03:00 pm", "04:00 pm", "05:00 pm", "06:00 pm", "07:00 pm"
      , "08:00 pm", "09:00 pm", "10:00 pm", "11:00 pm"]

    timeValue.forEach((time, i) => {
      content.push(
        <Fragment>
          <tr class="row-projects">
            <td rowspan="2" class="block-round-hour text-center">
              <label class="checkbox">
                <span class="round-hour-item">  {Moment(time, "hh:mm a").format("h a")}</span>
              </label>
            </td>
          </tr>
          <tr class="row-screenshots">
            {this.insideTimeFrame(6 * i).map(i => i)}
          </tr>
        </Fragment>
      )
    })

    return (

      content

    );
  }

  previousButton = (number, type) => {
    // debugger
    console.log('dateee', this.state.add_start_date);
    let add_end_date
    let add_start_date = Moment(this.state.add_start_date).subtract(number, type).format("YYYY-MM-DD")
    if (this.state.calendarView) {
      add_end_date = add_start_date
    }
    else {
      add_end_date = Moment(this.state.add_end_date).subtract(number, type).format("YYYY-MM-DD")
    }
    this.callListTimeLog(add_start_date, add_end_date)
  }

  nextButton = (number, type) => {
    // debugger
    console.log('dateee', this.state.add_start_date);
    let add_end_date

    let add_start_date = Moment(this.state.add_start_date).add(number, type).format("YYYY-MM-DD")
    if (this.state.calendarView) {
      add_end_date = add_start_date
    }
    else {
      add_end_date = Moment(this.state.add_end_date).add(number, type).format("YYYY-MM-DD")
    }
    this.callListTimeLog(add_start_date, add_end_date)
  }

  callListTimeLog = (add_start_date, add_end_date) => {
    this.setState({ add_start_date: add_start_date, add_end_date: add_end_date },
      () => this.props.listTimeLog({
        ...(this.state.project != "" && { project: this.state.project }),
        ...(this.state.employee != "" && { employee: this.state.employee }),
        ...(this.state.add_start_date != "" && { start_time: this.state.add_start_date }),
        ...(this.state.add_end_date != "" && { end_time: this.state.add_end_date }),
        // ...(this.state.calendarView && { end_time: this.state.add_start_date }),
      })
    )
  }

  sort=(param)=>{
    console.log('paramparam',param)
    
    this.props.listTimeLog({
      ...(this.state.project != "" && { project: this.state.project }),
      ...(this.state.employee != "" && { employee: this.state.employee }),
      ...(this.state.add_start_date != "" && { start_time: this.state.add_start_date }),
      ...(this.state.add_end_date != "" && { end_time: this.state.add_end_date }),
      ...(this.state.add_end_date != "" && { end_time: this.state.add_end_date }),
      date_sort_asc:param
    })
 this.setState({param:!param})
  }

  render() {
    
    console.log('state availbleResources',this.state.availbleResources)
    
    
    let permissions = [];
    permissions = localStorage.getItem("permissions");
    if (permissions && permissions.length > 0) {
      permissions = permissions.split(",");
    }
    let is_admin = localStorage.getItem("user_type") === "admin";




    const typeList = [
      { label: "Hourly Type", value: "hourly" },
      { label: "Fixed Type", value: "fixed" },
    ];

    const statusList = [
      { label: "ToDo", value: "todo" },
      { label: "Ongoing", value: "ongoing" },
      { label: "Done", value: "done" },
      { label: "On Hold", value: "on_hold" },
      { label: "Moved To Testing", value: "moved_to_testing" },
      { label: "Cancelled", value: "cancelled" },
    ];

    const resources = [
      { label: "Business Team", value: 1 },
      { label: "Admin", value: 2 },
      { label: "Project Manager", value: 3 },
    ];
    var clientList = [];

    this.props.clientData &&
      this.props.clientData.clients &&
      this.props.clientData.clients.map((obj) =>
        clientList.push({
          name: "Client",
          value: obj.client.slug,
          label: obj.client.first_name + " " + obj.client.last_name,
        })
      );

    const userList = [
      { label: "user 1", value: 1, name: "Abc" },
      { label: "user 2", value: 2, name: "Mathu" },
      { label: "user 3", value: 3, name: "John" },
    ];
    const clientType = [
      {
        text: "Internal Project",
        value: "internal",
        name: "client_type",
        id: "id_client_type_0",
      },
      {
        text: "Client Project",
        value: "client",
        name: "client_type",
        id: "id_client_type_1",
      },
    ];
    const { handleSubmit } = this.props;
    const Currency = Helper.Currency;

    let projectObj,
      contracts = [],
      billableResources = [],
      combined = [],
      termAttachments = [];

    if (this.props.projectData && this.props.projectData.status === "success") {
      projectObj = this.props.projectData.projectObj;



    }

    let employeeList = [(!this.state.calendarView && { name: 'employee', value: "", label: "All" })], projectList = [(!(this.state.calendarView || this.state.projectView) && { name: 'project', value: "", label: "All" })];

    if (this.props.employees?.allEmployees?.response?.length > 0) {
      this.props.employees.allEmployees.response.filter(res => !res.is_owner).map((res) =>
        employeeList.push({
          name: "employee",
          value: res?.slug,
          label: res?.first_name + " " + res?.last_name,
          id: res?.id,
        })
      );

      // this.setState({employeeList:employeeList})

    }

    if (this.props.projectData?.projects?.length > 0) {
      this.props.projectData.projects.map(pro => projectList.push(
        { name: 'project', value: pro?.project?.slug, label: pro?.project?.project_name })
      )
    }
    console.log('this.state.timings.length', this.state.timings.length);
    if (this.state.calendarView && this.state.timings.length < 0) {
      console.log('renderrrrrrrrrrrrr');

      this.setState({ timings: timings() })
    }

    console.log("employeeList", employeeList)
    return (
      <Fragment>

        {this.props.projectData?.isLoading ? (
          <Fragment>
            <div className="loader-img">
              {" "}
              <img src={LoaderImage} style={loaderStyle} />
            </div>
          </Fragment>
        ) : (
            <div className="col-sm-12">
              <form
                className="row"
                id="create-project-form"
                onSubmit={handleSubmit(this.submit)}
                enctype="multipart/form-data"
              >

                <div
                  className="col-sm-12"
                  id="create-project-form"
                  role="tabpanel"
                  aria-labelledby="task-list"
                >
                  {/* {this.state.errorMessages &&
                    this.state.errorMessages.length > 0 &&
                    this.state.errorMessages.map((i) => (
                      <ErrorMessage msg={i} />
                    ))} */}

                  <div
                    className="row team-content"
                    style={{
                      width: "100%",
                      margin: "0px",
                      padding: "0px",
                      // maxHeight: "100vh",
                      // minHeight: "100vh",
                      // overflowY: "scroll",
                    }}
                  >
                    <ul
                      className="nav nav-tabs"
                      id="myTab"
                      role="tablist"
                      style={{ minHeight: "51px", maxHeight: "0px" }}
                    >
                      {!is_admin && <li className="nav-item">
                        <a
                          className="nav-link active"
                          style={{ fontSize: "13px", padding: "13px" }}
                          id="mytask-list"
                          data-toggle="tab"
                          href="#mytasklist"
                          role="tab"
                          aria-controls="mytasklist"
                          aria-selected="true"
                          onClick={() => this.setState({
                            taskType: 'myTask', project: "", employee: localStorage.getItem('admin_employee_slug'), viewTimelog: false,
                            calendarView: false
                          }, () => this.props.listMyTasks({
                            ...(this.state.project != "" && { project: this.state.project }),
                            assigned: localStorage.getItem('admin_employee_slug')
                          }))}
                        >
                          My Task
                        </a>
                      </li>}
                      <li className="nav-item">

                        {(is_admin ||
                          (permissions &&
                            permissions.includes("view_all_tasks"))) && (<a
                              className="nav-link"
                              id="alltask-list"
                              data-toggle="tab"
                              style={{ fontSize: "13px", padding: "13px" }}
                              href="#alltasklist"
                              role="tab"
                              aria-controls="alltasklist"
                              aria-selected="true"
                              onClick={() => this.setState({
                                taskType: 'allTask', project: "", employee: "", viewTimelog: false,
                                calendarView: false
                              }, () => {
                                this.props.listTasks({
                                  ...(this.state.project != "" && { project: this.state.project }),
                                  ...(this.state.employee != "" && { assigned: this.state.employee })
                                })
                              }

                              )}
                            >
                              {" "}
                            All Task
                            </a>)}

                      </li>
                    </ul>
                    <div
                      className="tab-content"
                      id="myTabContent"
                      style={{ width: "100%", minHeight: "100vh" }}
                    >


                      <div
                        className={(is_admin) ? "tab-pane userMail-tab fade show active text-grey" : "tab-pane userMail-tab fade  text-grey"}
                        id="alltasklist"
                        // style={{overflow: "auto",minHeight: "50vh",maxHeight:"50vh"}}
                        // style={{ padding: "0px 15px" }}
                        role="tabpanel"
                        aria-labelledby="alltask-list"
                      >

                        <div className="row" style={{ alignItems: "center" }}>
                          {!this.state.viewTimelog && !this.state.calendarView &&
                            <div className="row team col-sm-4">

                              <ul className="nav nav-tabs" id="myTask" role="tablist">
                                <li className="nav-item">
                                  <a
                                    className="nav-link active "
                                    id="active-tab"
                                    data-toggle="tab"
                                    href="#active"
                                    role="tab"
                                    aria-controls="active"
                                    aria-selected="true"
                                  >
                                    Active
                            </a>
                                </li>
                                <li className="nav-item" style={{ padding: "0px" }}>
                                  <a
                                    className="nav-link "
                                    id="inactive-tab"
                                    data-toggle="tab"
                                    href="#inactive"
                                    role="tab"
                                    aria-controls="inactive"
                                    aria-selected="true"
                                  >
                                    Inactive
                            </a>
                                </li>
                              </ul>
                            </div>}
                          <div className="col-sm-4 time-log "
                            style={{ textAlign: "center" }}>

                            <button
                              style={{ width: "110px" }}
                              onClick={this.viewTimelog}
                              type="button"
                              disabled={this.state.viewTimelog}
                              className={this.state.viewTimelog ? "btn btn-primary" : "btn btn-outline-primary add-row"}
                            >
                              View Timelog
                              </button>

                          </div>

                          <div
                            className="col-sm-4 calendar-view "
                            style={{ textAlign: "center" }}
                          >
                            {(
                              <button
                                style={{ width: "110px" }}
                                onClick={() => this.calendarView(projectList?.find(i => i.value != "")?.value, employeeList[1]?.value)}
                                type="button"
                                disabled={this.state.calendarView}
                                className={this.state.calendarView ? "btn btn-primary" : "btn btn-outline-primary add-row"}
                              >
                                Calendar View
                              </button>
                            )}
                          </div>

                          {this.state.viewTimelog || this.state.calendarView ? <div className="col-sm-4 back-btn" style={{ textAlign: "center" }}><button
                            style={{ width: "110px" }}
                            onClick={this.state.viewTimelog ? this.viewTimelog : this.calendarView}
                            type="button"
                            className="btn btn-outline-primary add-row"
                          >
                            Back
                              </button></div> : null}

                          <div className="row" style={{ width: "100%", margin: "18px" }}>
                            <div className="col-sm-6  " style={{ textAlign: "initial" }}>
                              <label

                                className="text-light-grey"
                              >
                                Select Employee
                                </label>
                              <Select onChange={(e) => this.handleEmployeeProject(e, 'select')}
                                value={employeeList?.filter(i => i.value === this.state.employee)}

                                options={employeeList} />
                            </div>
                            <div className="col-sm-6  " style={{ textAlign: "initial" }}>
                              <label

                                className="text-light-grey"
                              >
                                Select Project
                                </label>
                              <Select onChange={(e) => this.handleEmployeeProject(e, 'select')}
                                value={projectList?.filter(i => i.value === this.state.project)}
                                options={projectList} />
                            </div>
                          </div>

                          {(this.state.viewTimelog || this.state.calendarView) ?
                            <div className="row" style={{ width: "100%", margin: "18px" }}>
                              {!this.state.calendarView && !this.state.projectView ? <Fragment>
                                <div className="col-sm-3  " style={{ textAlign: "initial" }}>
                                  <label

                                    className="text-light-grey"
                                  >
                                    Start Date111
                                 </label>
                                  <input
                                    type='date'
                                    name="add_start_date"
                                    label="Start Date"
                                    className="fullwidth input-control"
                                    value={this.state.add_start_date}
                                    onChange={(e) => this.handleEmployeeProject(e, 'date')}
                                  //  min={values.todays_date}
                                  />
                                </div>
                                <div className="col-sm-3  " style={{ textAlign: "initial" }}>
                                  <label

                                    className="text-light-grey"
                                  >
                                    End Date
                                 </label>
                                  <input
                                    type='date'
                                    name="add_end_date"
                                    label="End Date"
                                    className="fullwidth input-control"
                                    value={this.state.add_end_date}
                                    onChange={(e) => this.handleEmployeeProject(e, 'date')}
                                  //  min={values.todays_date}
                                  />
                                </div></Fragment> : null}
                              {this.state.calendarView ?
                                <div className="col-sm-4  " style={{ textAlign: "initial" }}>
                                  <label

                                    className="text-light-grey"
                                  >
                                    Date
                                 </label>
                                  <input
                                    type='date'
                                    name="add_start_date"
                                    label="Date"
                                    className="fullwidth input-control"
                                    value={this.state.add_start_date}
                                    onChange={(e) => this.handleEmployeeProject(e, 'date')}
                                   max={Moment().format("YYYY-MM-DD")}
                                  />
                                </div> : null
                              }
                              <div className="col-sm-6  time-entry" >
                                <div class="input-color">
                                <div className="color-value">

                                  <div class="color-box" style={{ backgroundColor: "green" }}></div><span style={{ marginLeft: "20px", marginRight: "20px" }}> Auto-tracked</span>

                                  </div>
                                  <div className="color-value">
                                  <div class="color-box" style={{ backgroundColor: "#e2e25e" }}></div><span style={{ marginLeft: "20px", marginRight: "20px" }}>Manual Time</span>
                                  </div>
                                  <div className="color-value">
                                  <div class="color-box" style={{ backgroundColor: "#e9e9e9" }}></div><span style={{ marginLeft: "20px", marginRight: "20px" }}>No Task</span>
                                  </div>
                                </div>



                              </div>
                            </div> : null
                          }





                        </div>

                        <div className="tab-content row" id="myTaskContent">
                          <div
                            className="tab-pane userMail-tab fade show active text-grey"
                            id="active"
                            role="tabpanel"
                            aria-labelledby="active-tab"
                            style={{ width: "100%" }}
                          >
                            <div className="row mt-3" style={{ textAlign: "initial" }}>
                              {this.state.viewTimelog && <Fragment>


                                <div class="col-sm-2 weekly-view " style={{ textAlign: "center" }}>
                                  <button type="button" disabled={this.state.weeklyView} onClick={this.weeklyView}
                                    className={this.state.weeklyView ? "btn btn-primary" : "btn btn-outline-primary add-row"}
                                    style={{ width: "100px" }}>Weekly View</button>
                                </div>
                                <div class="col-sm-2 monthly-view " style={{ textAlign: "center" }}>
                                  <button type="button" disabled={this.state.monthlyView} onClick={this.monthlyView}
                                    className={this.state.monthlyView ? "btn btn-primary" : "btn btn-outline-primary add-row"}
                                    style={{ width: "100px" }}>Monthly View</button>
                                </div>
                                <div className={this.state.projectView ? "col-sm-6" : "col-sm-2 project-view"} >
                                  <button type="button" disabled={this.state.projectView} onClick={() => this.projectView(projectList)}
                                    className={this.state.projectView ? "btn btn-primary" : "btn btn-outline-primary add-row"}
                                    style={{ width: "100px" }}>Project View</button>
                                </div>
                                {!this.state.projectView &&
                                  <div class="col-sm-6 previous" style={{ textAlign: "end", marginBottom: "15px" }}>
                                    <button type="button" onClick={() => this.previousButton(1, this.state.selected)} class="btn btn-outline-primary add-row" style={{ width: "90px", marginRight: "10px" }}>
                                      <i class="fa fa-chevron-left" aria-hidden="true" style={{ marginRight: "7px" }}></i>Previous
                                      </button>

                                    {Moment(this.state.add_end_date, 'YYYY-MM-DD').isBefore(Moment()) && <button type="button" onClick={() => this.nextButton(1, this.state.selected)} class="btn btn-outline-primary add-row" style={{ width: "90px" }}>Next
                                        <i class="fa fa-chevron-right" aria-hidden="true" style={{ marginLeft: "7px" }}></i>
                                    </button>}

                                  </div>}
                                {this.props.tasks && this.props.tasks.timelog && this.props.tasks.timelog.length > 0 &&
                                  <Fragment>


                                    <p style={{ marginTop: "2rem", marginRight: "30px" }}><h6 className="text-grey">Total Productive Hours : {this.state.totalProductiveHours}</h6></p>

                                    <p style={{ marginTop: "2rem" }}><h6 className="text-grey">Total PM Approved Hours : {this.state.totalPmApprovedHours}</h6></p>
                                  </Fragment>}
                                <table style={{ textAlign: "center" }} id="dtHorizontalExample" class=" table-scroll table table-cs text-grey table-text-sm table-striped table-borderless" cellspacing="0"
                                  width="100%">
                                  <thead>
                                    {console.log(this.props.tasks, "this.props.tasks")}
                                    {this.props.tasks && this.props.tasks.timelog && this.props.tasks.timelog.length > 0 ?
                                      <tr>
                                         <th class="text-nowrap">
                                         <div style={{float:"left"}}>Date</div>
                                          {
                                           this.state.param?
                                          
                                           <div  onClick={()=>this.sort(true)}  class= "fa fa-sort-asc fa-size sort-icon "></div>
                                           :
                                           <div onClick={()=>this.sort(false)}  class= "fa fa-sort-desc fa-size sort-icon  "></div>
                                          } 
                                              {/* <i  onClick={()=>this.sort(true)} class="fa fa-sort-up"></i>
                                              <i onClick={()=>this.sort(false)} class="fa fa-sort-down"></i> */}
                                          </th>
                                        <th>Project my</th>  
                                        <th>Assignee</th>

                                        <th onClick={() => this.setState({ individualEdit: 'task' })}>{this.state.individualEdit === 'task' ?
                                          <div ref={(node) => (this.node = node)} className="col-sm-6  " style={{ textAlign: "initial" }}>
                                            <Select onChange={(e) => this.handleIndividualStatusOrAssignedChange(e)}
                                              // value={this.state.tasksList?.filter(i => i.value === this.state.project)}
                                              options={this.state.tasksList} />
                                          </div>
                                           :<span className="hover-change" id="project1">Task</span> }</th>
                                        <th>Start</th>
                                        <th>End</th>
                                        <th>Worked hours</th>
                                        <th>Productive hours</th>
                                        <th>Billable</th>
                                        <th>PM Approved</th>
                                        <th></th>
                                      </tr> : null
                                      // <Fragment><tr><h6 style={{color:"red"}}>Please Select an Employee and Project</h6></tr></Fragment>
                                    }
                                  </thead>
                                  <tbody className="text-grey">
                                    {this.props.tasks && this.props.tasks.timelog && this.props.tasks.timelog.length > 0 ?
                                      <Fragment>
                                        {this.props.tasks?.timelog?.map(obj =>
                                          <tr className={!obj.tracking ? "manual-time" : "auto-tracked"}>
                                            <td>{this.convertToDateFormat(obj.start_time_format)}</td>
                                            <td>{projectList?.filter(i => i.value === obj.project)[0]?.label}</td>
                                            
                                            <td>
                                              {this.props.employees?.allEmployees?.response?.filter(i => i.slug === obj.employee)[0].first_name + " " +
                                                this.props.employees?.allEmployees?.response?.filter(i => i.slug === obj.employee)[0].last_name
                                              }
                                            </td>
                                            <td>
                                              {obj.task_name}

                                            </td>

                                            <td>{(obj.start_time_format) && this.convertToLocalTimeFormat(obj.start_time_format)}</td>
                                            <td>{(obj.end_time_format != null) && this.convertToLocalTimeFormat(obj.end_time_format)}</td>
                                            {/* <td><a href=""><i class="icon-close" style={{ fontSize:"16px", color: "rgb(156, 156, 156)"}}></i></a></td> */}
                                            <td>{obj.worked_hours}</td>
                                            <td>{obj.productive_hours}</td>
                                            <td>{obj.hourly ? 'Yes' : 'No'}</td>
                                            <td onClick={() => this.setState({ individualEdit: 'pm_approved' })}>{obj.pm_approved?obj.pm_approved:'00:00:00'}  
                                            
                                            
                                            
                                            </td>
                                            <td>
                                            {(is_admin ||
                          (permissions &&
                            permissions.includes("change_pm_approved")))&&<Link 
                                            onClick={() => this.addTaskModal("edit_log",obj.slug,obj)}
                                            ><i  className="icon-pencil" style={{ fontSize:"16px", color: "rgb(156, 156, 156)",marginLeft:"4px"}}></i></Link> }
                                            </td>
                                          </tr>
                                        )}</Fragment> : <Fragment>
                                        <div style={{ color: "red", position: "absolute", left: "33%",marginTop:"36px" }} className="position-center ">
                                          No Tasks Available
                          </div></Fragment>}
                                  </tbody>
                                </table></Fragment>}
                              {!this.state.calendarView && !this.state.viewTimelog &&
                                <table style={{tableLayout:"auto",width:"100%"}} class="table table-cs text-grey table-text-sm table-striped table-borderless">
                                  <thead>
                                    {this.props.tasks &&
                                      this.props.tasks.tasks &&
                                      this.props.tasks.tasks.length > 0 && (this.props.tasks.tasks.filter(taskObj => taskObj.task_board === "ongoing" || taskObj.task_board === "todo")).length > 0 ?
                                      <tr>
                                        <th className="thead-padding">Project Name</th>
                                        <th className="thead-padding">Task Name</th>
                                        {console.log("this.props.tasks.tasks.length", (this.props.tasks.tasks.filter(taskObj => taskObj.task_board === "ongoing" || taskObj.task_board === "todo")).length)}
                                        <th className="thead-padding">Status</th>

                                        <th className="thead-padding">Assignee</th>

                                        <th className="thead-padding">Due Date</th>

                                      </tr> : null}
                                  </thead>
                                  <tbody>

                                    {this.props.tasks &&
                                      this.props.tasks.tasks &&
                                      this.props.tasks.tasks.length > 0 && (this.props.tasks.tasks.filter(taskObj => taskObj.task_board === "ongoing" || taskObj.task_board === "todo")).length > 0 ? (
                                        this.props.tasks.tasks.filter(taskObj => taskObj.task_board === "ongoing" || taskObj.task_board === "todo").map(
                                          (taskObj, index) => (
                                            <tr>
                                              <td>{taskObj.project_name}</td>
                                              <td style={{whiteSpace:"break-spaces"}}>
                                                <i
                                                  style={{ marginRight:"2px" }}
                                                  class="fa fa-check-circle-o"
                                                  aria-hidden="true"
                                                ></i>

                                                {taskObj.task_name}
                                              </td>
                                              <td>{this.formatString(
                                                taskObj && taskObj.task_board
                                              )}</td>
                                              <td>
                                                {taskObj && taskObj.assigned
                                                  ?
                                                  <Fragment>
                                                    <div className="user-img-holder  ">
                                                      {taskObj &&
                                                        taskObj.assigned &&
                                                        taskObj.assigned.first_name.charAt(0)}
                                                      {taskObj &&
                                                        taskObj.assigned &&
                                                        taskObj.assigned.last_name.charAt(0)}
                                                    </div>
                                                    {taskObj &&
                                                      taskObj.assigned &&
                                                      taskObj.assigned.first_name} {" "}
                                                      {console.log("length",taskObj &&
                                                      taskObj.assigned &&
                                                      taskObj.assigned.last_name.length)}
                                                     {taskObj &&
                                                      taskObj.assigned &&
                                                      taskObj.assigned.last_name&&taskObj.assigned.last_name.length>2?
                                                
                                                      taskObj &&taskObj.assigned && taskObj.assigned.last_name:<Fragment>{taskObj &&taskObj.assigned && taskObj.assigned.last_name&&taskObj.assigned.last_name[0]}{" "}
                                                      {taskObj &&taskObj.assigned && taskObj.assigned.last_name&&taskObj.assigned.last_name[1]}</Fragment>}
                                                  </Fragment> : "Nill"}
                                              </td>

                                              <td>
                                                <div>

                                                  <label

                                                    className="text-light-grey"
                                                  >
                                                    {taskObj?.due_date
                                                      ? taskObj?.due_date
                                                      : "Nill"}
                                                  </label>

                                                </div>
                                              </td>

                                            </tr>

                                          ))
                                      ) : (<Fragment>
                                        {/* {
                        !this.state.allTaskemp?<div className="position-center ">
                        Please select an employee
                      </div>:null
                      } */}
                                        <div style={{ color: "red", position: "absolute", left: "33%",marginTop:"36px" }} className="position-center ">
                                          No Tasks Available
                          </div></Fragment>
                                      )}

                                  </tbody>
                                </table>}

                              {this.props.tasks?.isLoading ?
                                <Fragment>
                                  <div className="loader-img">

                                    <img src={LoaderImage} style={loaderStyle} />
                                  </div>
                                </Fragment>
                                : <Fragment>

                                  {this.state.calendarView &&
                                    <Fragment>
                                      <div class="col-sm-12 previous" style={{ textAlign: "end", marginBottom: "15px" }}>
                                        <button type="button" onClick={() => this.previousButton(1, 'days')} class="btn btn-outline-primary add-row" style={{ width: "90px", marginRight: "10px" }}>
                                          <i class="fa fa-chevron-left" aria-hidden="true" style={{ marginRight: "7px" }}></i>Previous
                                          </button>

                                        {Moment(this.state.add_end_date).isBefore(Moment().format('YYYY-MM-DD')) && <button type="button" onClick={() => this.nextButton(1, 'days')} class="btn btn-outline-primary add-row" style={{ width: "90px" }}>Next
                                        <i class="fa fa-chevron-right" aria-hidden="true" style={{ marginLeft: "7px" }}></i>
                                        </button>}
                                      </div>

                                      {this.props.tasks && this.props.tasks.timelog && this.props.tasks.timelog.length > 0 ?

                                        <Fragment>



                                          <table class="table table-borderless table-diary table-scroll">
                                            <tbody>


                                              <Fragment>

                                                <table class="table table-borderless table-diary table-scroll">
                                                  <tbody>
                                                    {this.state.calendarView && this.Product().map(i => i)}
                                                  </tbody>
                                                </table>


                                              </Fragment>
                                            </tbody>
                                          </table></Fragment>
                                        :
                                        (<Fragment>


                                          <div style={{ color: "red", position: "absolute", left: "33%" ,marginTop:"36px"}} className="position-center ">
                                            No Tasks Available
                          </div>
                                          {/* } */}
                                        </Fragment>
                                        )}


                                    </Fragment>
                                  }
                                </Fragment>}

                            </div>




                          </div>
                          <div
                            className="tab-pane userMail-tab fade  text-grey"
                            id="inactive"
                            role="tabpanel"
                            aria-labelledby="inactive-tab"
                            style={{ width: "100%" }}
                          >
                            <div className="row mt-3" style={{ textAlign: "initial" }}>

                              {!this.state.calendarView && !this.state.viewTimelog &&

                                <table style={{tableLayout:"auto",width:"100%"}} class="table table-cs text-grey table-text-sm table-striped table-borderless">
                                  <thead>
                                    {this.props.tasks &&
                                      this.props.tasks.tasks &&
                                      this.props.tasks.tasks.length > 0 && (this.props.tasks.tasks.filter(taskObj => taskObj.task_board === "cancelled" || taskObj.task_board === "done")).length > 0 ?
                                      <tr>
                                        <th className="thead-padding">Project Name</th>
                                        <th className="thead-padding">Task Name</th>
                                        {console.log("")}
                                        <th className="thead-padding">Status</th>

                                        <th className="thead-padding">Assignee</th>

                                        <th className="thead-padding">Due Date</th>

                                      </tr> : null}
                                  </thead>
                                  <tbody>




                                    {this.props.tasks &&
                                      this.props.tasks.tasks &&
                                      this.props.tasks.tasks.length > 0 && (this.props.tasks.tasks.filter(taskObj => taskObj.task_board === "cancelled" || taskObj.task_board === "done")).length > 0 ? (
                                        this.props.tasks?.tasks?.filter(taskObj => taskObj.task_board === "cancelled" || taskObj.task_board === "done").map(
                                          (taskObj, index) => (
                                            <tr>
                                              <td>{taskObj.project_name}</td>
                                              <td style={{whiteSpace:"break-spaces"}}>
                                                <i
                                                  style={{ marginRight:"2px" }}
                                                  class="fa fa-check-circle-o"
                                                  aria-hidden="true"
                                                ></i>
                                                {taskObj.task_name}
                                              </td>

                                              <td>{this.formatString(taskObj.task_board)}</td>

                                              <td>
                                                {taskObj && taskObj.assigned
                                                  ?
                                                  <Fragment>
                                                    <div className="user-img-holder  ">
                                                      {taskObj &&
                                                        taskObj.assigned &&
                                                        taskObj.assigned.first_name.charAt(0)}
                                                      {taskObj &&
                                                        taskObj.assigned &&
                                                        taskObj.assigned.last_name.charAt(0)}
                                                    </div>
                                                    {taskObj &&
                                                      taskObj.assigned &&
                                                      taskObj.assigned.first_name} {" "}
                                                      {console.log("length",taskObj &&
                                                      taskObj.assigned &&
                                                      taskObj.assigned.last_name.length)}
                                                     {taskObj &&
                                                      taskObj.assigned &&
                                                      taskObj.assigned.last_name&&taskObj.assigned.last_name.length>2?
                                                
                                                      taskObj &&taskObj.assigned && taskObj.assigned.last_name:<Fragment>{taskObj &&taskObj.assigned && taskObj.assigned.last_name&&taskObj.assigned.last_name[0]}{" "}
                                                      {taskObj &&taskObj.assigned && taskObj.assigned.last_name&&taskObj.assigned.last_name[1]}</Fragment>}
                                                  </Fragment> : "Nill"}
                                              </td>

                                              <td>
                                                <div>

                                                  <label

                                                    className="text-light-grey"
                                                  >
                                                    {taskObj?.due_date
                                                      ? taskObj?.due_date
                                                      : "Nill"}
                                                  </label>

                                                </div>
                                              </td>

                                            </tr>
                                          ))
                                      ) : (<Fragment>
                                        {/* {
                                      !this.state.allTaskemp?<div className="position-center ">
                                      Please select an employee
                                    </div>:null
                                    } */}
                                        <div className="position-center ">
                                          No Tasks Available
                                    </div></Fragment>
                                      )}

                                  </tbody>
                                </table>}
                              {this.state.calendarView &&
                                <Fragment>{this.props.tasks && this.props.tasks.timelog && this.props.tasks.timelog.length > 0 ?
                                  this.props.tasks?.timelog?.map(obj =>
                                    <div class="grid-container">
                                      {/* {this.props.tasks?.timelog?.map(obj=> */}
                                      <div class={!obj.tracking ? "manual-time grid-item" : "auto-tracked grid-item"}><span class="try1">
                                        {this.state.allTasks?.length > 0 && this.state.allTasks?.filter(i => i.slug === obj.task)[0]?.task_name}
                                      </span></div>



                                      <div class="grid-item ">{!obj.tracking ? "Manual Time" : "Auto-tracked"}<span class="try">{(obj.start_time_format) && this.convertToLocalTimeFormat(obj.start_time_format)}
                                        {(obj.end_time_format != null) && this.convertToLocalTimeFormat(obj.end_time_format)}
                                      </span></div>


                                    </div>) :
                                  (<Fragment>
                                    {/* {
                            !this.state.calendarViewemp&&!this.state.project?<div className="position-center ">
                             <h6  style={{color:"red",position:"absolute",left:"33%"}}>Please Select an Employee and Project</h6>
                          </div>:null
                          } */}
                                    {/* { this.state.calendarViewemp&&this.state.project&& */}
                                    <div style={{ color: "red", position: "absolute", left: "33%",marginTop:"36px" }} className="position-center ">
                                      No Tasks Available
                          </div>
                                    {/* } */}
                                  </Fragment>
                                  )}


                                </Fragment>
                              }
                            </div>
                          </div>









                        </div>

                      </div>



                      {!is_admin && <div
                        className="tab-pane userMail-tab fade show active text-grey"
                        id="mytasklist"
                        // style={{overflow: "auto",height: "260px"}}
                        // style={{ paddingLeft: "0px 15px" }}
                        style={{ paddingTop: "2px" }}
                        role="tabpanel"
                        aria-labelledby="mytask-list"
                        style={{ width: "100%" }}
                      >

                        <div className="row" style={{ alignItems: "center" }}>
                          {!this.state.viewTimelog && !this.state.calendarView &&
                            <div className="row team col-sm-4">

                              <ul className="nav nav-tabs" id="myTask" role="tablist">
                                <li className="nav-item">
                                  <a
                                    className="nav-link active "
                                    id="active-mytask"
                                    data-toggle="tab"
                                    href="#activemytask"
                                    role="tab"
                                    aria-controls="activemytask"
                                    aria-selected="true"
                                  >
                                    Active
                            </a>
                                </li>
                                <li className="nav-item" style={{ padding: "0px" }}>
                                  <a
                                    className="nav-link "
                                    id="inactive-mytask"
                                    data-toggle="tab"
                                    href="#inactivemytask"
                                    role="tab"
                                    aria-controls="inactive-mytask"
                                    aria-selected="true"
                                  >
                                    Inactive
                            </a>
                                </li>
                              </ul>
                            </div>}
                          <div className="col-sm-4 time-log "
                            style={{ textAlign: "center" }}>

                            <button
                              style={{ width: "110px" }}
                              onClick={this.viewTimelog}
                              type="button"
                              disabled={this.state.viewTimelog}
                              className={this.state.viewTimelog ? "btn btn-primary" : "btn btn-outline-primary add-row"}
                            >
                              View Timelog
                              </button>

                          </div>
                          <div
                            className="col-sm-4 calendar-view "
                            style={{ textAlign: "center" }}
                          >
                            {(
                              <button
                                style={{ width: "110px" }}
                                onClick={() => this.calendarView(projectList?.find(i => i.value != "")?.value, localStorage.getItem("user_type") === "admin" ? "" : localStorage.getItem('admin_employee_slug'))}
                                type="button"
                                disabled={this.state.calendarView}
                                className={this.state.calendarView ? "btn btn-primary" : "btn btn-outline-primary add-row"}
                              >
                                Calendar View
                              </button>
                            )}
                          </div>
                          {this.state.viewTimelog || this.state.calendarView ? <div className="col-sm-4 back-btn" style={{ textAlign: "center" }}><button
                            style={{ width: "110px" }}
                            onClick={this.state.viewTimelog ? this.viewTimelog : this.calendarView}
                            type="button"
                            className="btn btn-outline-primary add-row"
                          >
                            Back
                              </button></div> : null}
                          <div className="row" style={{ width: "100%", margin: "18px" }}>
                            {/* <div className="col-sm-6  " style={{ textAlign: "initial" }}>
                        <label
                                  
                                  className="text-light-grey"
                                >
                                  Select Employee my
                                </label>
                                {console.log('ssssssssss',employeeList)}
                                <Select 
                                onChange={(e)=>this.handleEmployeeProject(e,'select')}
                                value={employeeList?.filter(i=>i.value===this.state.employee)}
                                  options={employeeList?.filter(i=>i.value===localStorage.getItem('admin_employee_slug'))}/>
                        </div> */}
                            <div className="col-sm-6  " style={{ textAlign: "initial" }}>
                              <label

                                className="text-light-grey"
                              >
                                Select Project
                                </label>

                              <Select onChange={(e) => this.handleEmployeeProject(e, 'select')}
                                value={projectList?.filter(i => i.value === this.state.project)}
                                options={projectList} />
                            </div>
                          </div>
                          {this.state.viewTimelog || this.state.calendarView ?
                            <div className="row" style={{ width: "100%", margin: "18px" }}>
                              {!this.state.calendarView && !this.state.projectView ? <Fragment>
                                <div className="col-sm-3  " style={{ textAlign: "initial" }}>
                                  <label

                                    className="text-light-grey"
                                  >
                                    Start Date my
                                 </label>
                                  <input
                                    type='date'
                                    name="add_start_date"
                                    label="Start Date"
                                    className="fullwidth input-control"
                                    value={this.state.add_start_date}
                                    onChange={(e) => this.handleEmployeeProject(e, 'date')}
                                  //  min={values.todays_date}
                                  />
                                </div>
                                <div className="col-sm-3  " style={{ textAlign: "initial" }}>
                                  <label
                                    className="text-light-grey"
                                  >
                                    End Date
                                 </label>
                                  <input
                                    type='date'
                                    name="add_end_date"
                                    label="End Date"
                                    className="fullwidth input-control"
                                    value={this.state.add_end_date}
                                    onChange={(e) => this.handleEmployeeProject(e, 'date')}
                                  //  min={values.todays_date}
                                  />
                                </div></Fragment> : null}
                              {this.state.calendarView ?
                                <div className="col-sm-4  " style={{ textAlign: "initial" }}>
                                  <label

                                    className="text-light-grey"
                                  >
                                    Date
                                 </label>
                                  <input
                                    type='date'
                                    name="add_start_date"
                                    label="Date"
                                    className="fullwidth input-control"
                                    value={this.state.add_start_date}
                                    onChange={(e) => this.handleEmployeeProject(e, 'date')}
                                    max={Moment().format("YYYY-MM-DD")}
                                  />
                                </div> : null
                              }
                              <div className="col-sm-6 time-entry " >
                                <div class="input-color">
                                <div className="color-value">
                                  <div class="color-box" style={{ backgroundColor: "green" }}></div><span style={{ marginLeft: "20px", marginRight: "20px" }}> Auto-tracked</span>

                                </div>
                                <div className="color-value">
                                  <div class="color-box" style={{ backgroundColor: "#e2e25e" }}></div><span style={{ marginLeft: "20px", marginRight: "20px" }}>Manual Time</span>
                                 </div>
                                 <div className="color-value">
                                  <div class="color-box" style={{ backgroundColor: "#e9e9e9" }}></div><span style={{ marginLeft: "20px", marginRight: "20px" }}>No Task</span>
                                </div>
                                </div>

                              </div>
                            </div> : null
                          }
                        </div>

                        <div className="tab-content row" id="myTaskContent">
                          <div
                            className="tab-pane userMail-tab fade show active text-grey"
                            id="activemytask"
                            role="tabpanel"
                            aria-labelledby="active-mytask"
                            style={{ width: "100%" }}
                          >
                            <div className="row mt-3" style={{ textAlign: "initial" }}>
                              {this.state.viewTimelog && <Fragment>


                                <div class="col-sm-2 weekly-view  " style={{ textAlign: "center" }}>
                                  <button type="button" disabled={this.state.weeklyView} onClick={this.weeklyView}
                                    className={this.state.weeklyView ? "btn btn-primary" : "btn btn-outline-primary add-row"}
                                    style={{ width: "100px" }}>Weekly View</button>
                                </div>
                                <div class="col-sm-2 monthly-view " style={{ textAlign: "center" }}>
                                  <button type="button" disabled={this.state.monthlyView} onClick={this.monthlyView}
                                    className={this.state.monthlyView ? "btn btn-primary" : "btn btn-outline-primary add-row"}
                                    style={{ width: "100px" }}>Monthly View</button>
                                </div>
                                <div className={this.state.projectView ? "col-sm-6" : "col-sm-2 project-view"} >
                                  <button type="button" disabled={this.state.projectView} onClick={() => this.projectView(projectList)}
                                    className={this.state.projectView ? "btn btn-primary" : "btn btn-outline-primary add-row"}
                                    style={{ width: "100px" }}>Project View</button>
                                </div>
                                {!this.state.projectView &&
                                  <div class="col-sm-6 previous" style={{ textAlign: "end", marginBottom: "15px" }}>
                                    <button type="button" onClick={() => this.previousButton(1, this.state.selected)} class="btn btn-outline-primary add-row" style={{ width: "90px", marginRight: "10px" }}>
                                      <i class="fa fa-chevron-left" aria-hidden="true" style={{ marginRight: "7px" }}></i>Previous
                                      </button>
                                    {Moment(this.state.add_end_date, 'YYYY-MM-DD').isBefore(Moment()) && <button type="button" onClick={() => this.nextButton(1, this.state.selected)} class="btn btn-outline-primary add-row" style={{ width: "90px" }}>Next
                                        <i class="fa fa-chevron-right" aria-hidden="true" style={{ marginLeft: "7px" }}></i>
                                    </button>}
                                  </div>}
                                {this.props.tasks && this.props.tasks.timelog && this.props.tasks.timelog.length > 0 &&
                                  <Fragment>


                                    <p style={{ marginTop: "2rem", marginRight: "30px" }}><h6 className="text-grey">Total Productive Hours : {this.state.totalProductiveHours}</h6></p>
                                    <p style={{ marginTop: "2rem" }}><h6 className="text-grey">Total PM Approved Hours : {this.state.totalPmApprovedHours}</h6></p>

                                  </Fragment>}
                                <table style={{ textAlign: "center" }} id="dtHorizontalExample" class="table-scroll table table-cs text-grey table-text-sm table-striped table-borderless" cellspacing="0"
                                  width="100%">
                                  <thead>
                                    {this.props.tasks && this.props.tasks.timelog && this.props.tasks.timelog.length > 0 ?
                                      <tr>
                                        <th class="text-nowrap">
                                         <div style={{float:"left"}}>Date</div>  
                                         {
                                          this.state.param? 
                                          
                                           <div  onClick={()=>this.sort(true)}  class= "fa fa-sort-asc fa-size sort-icon "></div>
                                            : 
                                           <div onClick={()=>this.sort(false)}  class= "fa fa-sort-desc fa-size sort-icon  "></div>
                                         } 
                                              {/* <i  onClick={()=>this.sort(true)} class="fa fa-sort-up"></i>
                                              <i onClick={()=>this.sort(false)} class="fa fa-sort-down"></i> */}
                                          </th>
                                        <th>Project</th>
                                        
                                        <th>Assignee</th>
                                        <th onClick={() => this.setState({ individualEdit: 'task' })}>{this.state.individualEdit === 'task' ?
                                          <div ref={(node) => (this.node = node)} className="col-sm-6  " style={{ textAlign: "initial" }}>
                                            <Select onChange={(e) => this.handleIndividualStatusOrAssignedChange(e)}
                                              // value={this.state.tasksList?.filter(i => i.value === this.state.project)}
                                              options={this.state.tasksList} />
                                          </div>
                                           :<span className="hover-change"  id="project1">Task</span> }</th>
                                        <th>Start</th>
                                        <th>End</th>
                                        <th>Worked hours</th>
                                        <th>Productive hours</th>
                                        <th>Billable</th>
                                        <th>PM Approved</th>
                                        <th></th>
                                      </tr> : <div style={{ color: "red", position: "absolute", left: "33%",marginTop:"36px" }} className="position-center ">
                                        No Tasks Available
                        </div>
                                      // <Fragment><tr><h6 style={{color:"red"}}>Please Select an Employee and Project</h6></tr></Fragment>
                                    }
                                  </thead>
                                  <tbody className="text-grey">
                                    {this.props.tasks && this.props.tasks.timelog && this.props.tasks.timelog.length > 0 ? <Fragment>
                                      {this.props.tasks?.timelog?.map(obj =>
                                        <tr className={!obj.tracking ? "manual-time" : "auto-tracked"}>
                                          <td>{this.convertToDateFormat(obj.start_time_format)}</td>
                                          <td>{projectList?.filter(i => i.value === obj.project)[0]?.label}</td>
                                          
                                          <td>
                                            {this.props.employees?.allEmployees?.response?.filter(i => i.slug === obj.employee)[0].first_name + " " +
                                              this.props.employees?.allEmployees?.response?.filter(i => i.slug === obj.employee)[0].last_name
                                            }
                                          </td>
                                          <td>
                                            {obj.task_name}
                                          </td>

                                          <td>{(obj.start_time_format) && this.convertToLocalTimeFormat(obj.start_time_format)}</td>
                                          <td>{(obj.end_time_format != null) && this.convertToLocalTimeFormat(obj.end_time_format)}</td>
                                          {/* <td><a href=""><i class="icon-close" style={{ fontSize:"16px", color: "rgb(156, 156, 156)"}}></i></a></td> */}
                                          <td>{obj.worked_hours}</td>
                                          <td>{obj.productive_hours}</td>
                                          <td>{obj.hourly ? 'Yes' : 'No'}</td>
                                          <td>{obj.pm_approved?obj.pm_approved:'00:00:00'} 
                                        
                                          </td>
                                          <td>
                                          {(is_admin ||
                          (permissions &&
                            permissions.includes("change_pm_approved")))&&<Link 
                                            onClick={() => this.addTaskModal("edit_log",obj.slug,obj)}
                                            ><i  className="icon-pencil" style={{ fontSize:"16px", color: "rgb(156, 156, 156)",marginLeft:"4px"}}></i></Link> }
                                          </td>
                                        </tr>
                                      )}</Fragment> : <Fragment><div style={{ color: "red", position: "absolute", left: "33%" ,marginTop:"36px"}} className="position-center ">
                                        No Tasks Available
                        </div></Fragment>}
                                  </tbody>
                                </table></Fragment>
                              }             {!this.state.viewTimelog && !this.state.calendarView &&
                                <table style={{tableLayout:"auto",width:"100%"}} class="table table-cs text-grey table-text-sm table-striped table-borderless">
                                  <thead>{this.props.tasks &&
                                    this.props.tasks.mytasks &&
                                    this.props.tasks.mytasks.length > 0 && (this.props.tasks.mytasks.filter(taskObj => taskObj.task_board === "ongoing" || taskObj.task_board === "todo")).length > 0 ?
                                    <tr>
                                      <th>Project Name</th>
                                      <th>Task Name</th>
                                      <th>Status</th>
                                      <th>Assignee</th>
                                      <th>Due Date</th>
                                    </tr> : null}
                                  </thead>
                                  <tbody>
                                    {this.props.tasks &&
                                      this.props.tasks.mytasks &&
                                      this.props.tasks.mytasks.length > 0 && (this.props.tasks.mytasks.filter(taskObj => taskObj.task_board === "ongoing" || taskObj.task_board === "todo")).length > 0 ? (
                                        this.props.tasks.mytasks.filter(taskObj => taskObj.task_board === "ongoing" || taskObj.task_board === "todo").map(
                                          // this.props.tasks.mytasks.filter(taskObj => taskObj.task_board === ("cancelled"||"done")). map(
                                          (taskObj, index) => (
                                            <tr>
                                              <td>{taskObj.project_name}</td>
                                              <td style={{whiteSpace:"break-spaces"}}><i
                                                style={{ marginRight:"2px" }}
                                                class="fa fa-check-circle-o"
                                                aria-hidden="true"
                                              ></i>{taskObj.task_name}
                                              </td>
                                              <td>{this.formatString(
                                                taskObj && taskObj.task_board
                                              )}</td>
                                              <td><div className="user-img-holder ">
                                                {taskObj &&
                                                  taskObj.assigned &&
                                                  taskObj.assigned.first_name.charAt(0)}
                                                {taskObj &&
                                                  taskObj.assigned &&
                                                  taskObj.assigned.last_name.charAt(0)}
                                              </div>
                                                {taskObj &&
                                                  taskObj.assigned &&
                                                  taskObj.assigned.first_name} {" "}
                                                  {console.log("length",taskObj &&
                                                      taskObj.assigned &&
                                                      taskObj.assigned.last_name.length)}
                                                 {taskObj &&
                                                      taskObj.assigned &&
                                                      taskObj.assigned.last_name&&taskObj.assigned.last_name.length>2?
                                                
                                                      taskObj &&taskObj.assigned && taskObj.assigned.last_name:<Fragment>{taskObj &&taskObj.assigned && taskObj.assigned.last_name&&taskObj.assigned.last_name[0]}{" "}
                                                      {taskObj &&taskObj.assigned && taskObj.assigned.last_name&&taskObj.assigned.last_name[1]}</Fragment>}</td>
                                              <td> {taskObj.due_date}</td>
                                            </tr>

                                          ))
                                      ) : (<Fragment>
                                        {/* {
                        !this.state.myTaskemp?<div className="position-center ">
                        Please select an employee
                      </div>:null
                      } */}
                                        <div style={{ color: "red", position: "absolute", left: "33%" ,marginTop:"36px"}} className="position-center ">
                                          No Tasks Available
                          </div></Fragment>
                                      )}

                                  </tbody>
                                </table>}


                              {this.props.tasks?.isLoading ?
                                <Fragment>
                                  <div className="loader-img">

                                    <img src={LoaderImage} style={loaderStyle} />
                                  </div>
                                </Fragment>
                                : <Fragment>

                                  {this.state.calendarView &&
                                    <Fragment>
                                      <div class="col-sm-12 previous" style={{ textAlign: "end", marginBottom: "15px" }}>
                                        <button type="button" onClick={() => this.previousButton(1, 'days')} class="btn btn-outline-primary add-row" style={{ width: "90px", marginRight: "10px" }}>
                                          <i class="fa fa-chevron-left" aria-hidden="true" style={{ marginRight: "7px" }}></i>Previous
                                      </button>
                                        {Moment(this.state.add_end_date, 'YYYY-MM-DD').isBefore(Moment().format('YYYY-MM-DD')) && <button type="button"
                                          onClick={() => this.nextButton(1, 'days')} class="btn btn-outline-primary add-row" style={{ width: "90px" }}>Next
                                        <i class="fa fa-chevron-right" aria-hidden="true" style={{ marginLeft: "7px" }}></i>
                                        </button>}
                                      </div>

                                      {this.props.tasks && this.props.tasks.timelog && this.props.tasks.timelog.length > 0 ?

                                        <Fragment>



                                          <table class="table table-borderless table-diary table-scroll">
                                            <tbody>


                                              <Fragment>

                                                <table class="table table-borderless table-diary table-scroll">
                                                  <tbody>
                                                    {this.state.calendarView && this.Product().map(i => i)}
                                                  </tbody>
                                                </table>


                                              </Fragment>
                                            </tbody>
                                          </table></Fragment>
                                        :
                                        (<Fragment>


                                          <div style={{ color: "red", position: "absolute", left: "33%",marginTop:"36px" }} className="position-center ">
                                            No Tasks Available
                          </div>
                                          {/* } */}
                                        </Fragment>
                                        )}


                                    </Fragment>
                                  }
                                </Fragment>}
                            </div>
                          </div>

                          <div
                            className="tab-pane userMail-tab fade  text-grey"
                            id="inactivemytask"
                            role="tabpanel"
                            aria-labelledby="inactive-mytask"
                            style={{ width: "100%" }}
                          >
                            <div className="row mt-3" style={{ textAlign: "initial" }}>
                              {!this.state.calendarView && !this.state.viewTimelog &&
                                <table style={{tableLayout:"auto",width:"100%"}} class="table table-cs text-grey table-text-sm table-striped table-borderless">
                                  <thead>{this.props.tasks &&
                                    this.props.tasks.mytasks &&
                                    this.props.tasks.mytasks.length > 0 &&
                                    (this.props.tasks.mytasks.filter(taskObj => taskObj.task_board === "cancelled" || taskObj.task_board === "done")).length > 0 ?
                                    <tr>
                                      <th>Task Name</th>
                                      <th>Status</th>
                                      <th>Assignee</th>
                                      <th>Due Date</th>
                                    </tr> : null}
                                  </thead>
                                  <tbody>
                                    {this.props.tasks &&
                                      this.props.tasks.mytasks &&
                                      this.props.tasks.mytasks.length > 0 &&
                                      (this.props.tasks.mytasks.filter(taskObj => taskObj.task_board === "cancelled" || taskObj.task_board === "done")).length > 0 ? (
                                        this.props.tasks.mytasks.filter(taskObj => taskObj.task_board === "cancelled" || taskObj.task_board === "done").map(
                                          (taskObj, index) => (
                                            <tr>
                                              <td>{projectList?.filter(i => i.value === taskObj.project)[0]?.label}</td>
                                              <td style={{whiteSpace:"break-spaces"}}><i
                                                style={{ marginRight:"2px" }}
                                                class="fa fa-check-circle-o"
                                                aria-hidden="true"
                                              ></i>{taskObj.task_name}
                                              </td>
                                              <td>{this.formatString(
                                                taskObj && taskObj.task_board
                                              )}</td>
                                              <td><div className="user-img-holder  ">
                                                {taskObj &&
                                                  taskObj.assigned &&
                                                  taskObj.assigned.first_name.charAt(0)}
                                                {taskObj &&
                                                  taskObj.assigned &&
                                                  taskObj.assigned.last_name.charAt(0)}
                                              </div>
                                                {taskObj &&
                                                  taskObj.assigned &&
                                                  taskObj.assigned.first_name} {" "}
                                                  {console.log("length",taskObj &&
                                                      taskObj.assigned &&
                                                      taskObj.assigned.last_name.length)}
                                                      {taskObj &&
                                                      taskObj.assigned &&
                                                      taskObj.assigned.last_name&&taskObj.assigned.last_name.length>2?
                                                
                                                      taskObj &&taskObj.assigned && taskObj.assigned.last_name:<Fragment>{taskObj &&taskObj.assigned && taskObj.assigned.last_name&&taskObj.assigned.last_name[0]}{" "}
                                                      {taskObj &&taskObj.assigned && taskObj.assigned.last_name&&taskObj.assigned.last_name[1]}</Fragment>}</td>
                                              <td> {taskObj.due_date ? taskObj.due_date : "Nill"}</td>
                                            </tr>

                                          ))
                                      ) : (<Fragment>
                                        {/* {
                        !this.state.myTaskemp?<div className="position-center ">
                        Please select an employee
                      </div>:null
                      } */}
                                        <div style={{ color: "red", position: "absolute", left: "33%",marginTop:"36px" }} className="position-center ">
                                          No Tasks Available
                          </div></Fragment>
                                      )}

                                  </tbody>
                                </table>}
                            </div>

                          </div>
                        </div>












                      </div>
                      }

                    </div>
                  </div>
                </div>









              </form>
            </div>
          )}
        {this.state.editModal && (
          <AddTaskModal
            {...this.props}
            // id={this.state.index}
            closeModal={this.closeEditModal}
            index={this.state.index}
            editPart={this.state.editPart}
            obj={this.state.obj}
            // projectData=""
            timeLogObj={this.state.obj}
            availbleResources={this.state.availbleResources}
            dashBoardEdit={true}
          />
        )}
      </Fragment>
    );


  }
}
ViewProjectContent = reduxForm({
  form: "addProjectform",
})(ViewProjectContent);
export default ViewProjectContent;
