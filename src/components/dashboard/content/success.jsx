// Success.jsx
import React, { Component,Fragment } from 'react';
// import { Redirect } from 'react-router';
// import { useHistory } from 'react-router-dom';
// import { hashHistory } from 'react-router;'
import LoaderImage from '../../../static/assets/image/loader.gif';
var loaderStyle = {
    width: '3%',
    position: 'fixed',
    top: '50%',
    left: '57%'
 }
class Success extends Component{
    back  = (e) => {
        e.preventDefault();
        this.props.prevStep();
    }
    
    render(){
        const { values } = this.props
        return(
            <Fragment>
            <div className="sub-header" style={{textAlign:"center",marginTop:"20px"}}>
            {this.props.projectData&&this.props.projectData.isLoading?
                           <Fragment>
                              <div className="loader-img"> <img src={LoaderImage} style={loaderStyle} /></div>
                           </Fragment>
                           
                        :
                <Fragment>
                    <div className="row " style={{margin:"auto",width:"520px"}}>
                    <div className=" col-sm-12  form-group" style={{textAlign:"justify"}} >
                    
                     <label for="id_project_description" className="text-light-grey">
                     Comments
                     </label>
                     <textarea
                        name="detail_text"
                        cols="40" rows="10"
                        onChange={this.props.handleChange('detail_text')}
                //  defaultValue={values.project_description}
                        className="input-control input-control"
                        label="comments"
                        placeholder="Comments" />
                  
                  </div>
                  
                 
                         </div>          
                            
                
            
                <div className="row" style={{margin:"auto",width:"520px"}}>
               <div className="col-sm-6 form-group" style={{textAlign:"center"}}>
               
                                    <button type="button" style={{width:"130px"}} className="btn btn-outline-primary add-row" onClick={this.back}>Back </button>

                </div> 
                <div className="col-sm-6 form-group" style={{textAlign:"center"}}>
                <button type="button" onClick={()=>this.props.validateProjectForm('create')} style={{width:"130px"}} className="btn btn-outline-primary add-row"
                               
                                   >Finish</button>
                </div>                   
                </div></Fragment>
    }</div>
            </Fragment>
        )
    }
}

export default Success;