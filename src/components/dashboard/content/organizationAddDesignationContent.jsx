import React, { Component, Fragment } from 'react'
import { Field, reduxForm } from 'redux-form'
import SuccessMessage from '../../common/SuccessMessage'
import ErrorMessage from '../../common/ErrorMessage'
import { Redirect } from 'react-router-dom';


// import normalizePhone from './normalizePhone'

let x=[]
console.log('oppppppppppppppppppppppppppp')
const required = value => {
    return (value || typeof value === 'number' ? undefined : ' is Required')
}

var letters = /^[-a-zA-Z0-9-()]+(\s+[-a-zA-Z0-9-()]+)*$/;
const alphabetOnly =  value => {
    return(value && value.match(letters) ? undefined : ' must be characters only')
}

const renderField = ({
    input,
    label,
    type,
    className,
    existed,
    meta: { touched, error, warning, }
}) => {

    return (
        

        <Fragment>
            <input {...input} placeholder={label} type={type}
                className={(touched && error) || (existed) ?
                    'fullwidth input-control border border-danger' : className} />

            {touched &&
                ((error && <small className="text-danger">{label}{error}</small>) ||
                    (warning && <span>{warning}</span>))}

            {((touched) && existed ===
                    "The fields organization, designation_name must make a unique set."
                ) && <small class="text-danger">
                    This designation name Already exists
        </small>}

        </Fragment>

    )
}


const checkboxField = ({ input, options, className, y, type, checked, meta: { touched, error, warning } }) => (
    
    <Fragment>
        {options.map((option, index) => (
            
            <div className="" key={index}>
                <label htmlFor={option.slug}>
                    <span className="checkbox">
                        <input type="checkbox" id={option.slug} className={className}
                         name={option.group_name} value={option.slug} checked={y.length>0?(y.indexOf(option.slug) !== -1):(input.value.indexOf(option.slug) !== -1) }
                            onChange={(event) => {
                                if(y.length>0){
                                    const newValue = [...y]
                                    if (event.target.checked) {
                                        newValue.push(option.slug);
                                        y.push(option.slug);
                                    } else {
                                        newValue.splice(newValue.indexOf(option.slug), 1);
                                        y.splice(y.indexOf(option.slug), 1);
                                    }
                                    
                                    return input.onChange(newValue);
                                }else{
                                    const newValue = [...input.value];
                                    if (event.target.checked) {
                                        newValue.push(option.slug);
                                    } else {
                                        newValue.splice(newValue.indexOf(option.slug), 1);
                                    }
                                    
                                    return input.onChange(newValue);
                                }
                            }} />
                    </span>
                    {option.group_name}
                </label>
            </div>
        ))}

        <label>{touched &&
            ((error && <small className="text-danger">{error}</small>) ||
                (warning && <span>{warning}</span>))}</label>
    </Fragment>
)



let datas={};

class OrganizationAddDesignationContent extends Component {
    constructor(props) {
        super(props);
        this.state = {show_msg: false,x_l:[],existed_error:''};
      }

      
    getPermissiongroups =(data) =>{
      
          data.map((value,index)=>{
              x.push(value.slug)
              return true
          })
          this.setState({x_l:x})
      
      }

    submit = (values) => {
        if(this.props.update){
            this.props.UpdateDesignation(values,this.props.match.params.designation_slug)
            this.setState({show_msg:true})


        }else{
            let default_roles = []
            default_roles = this.props.roles.roles.filter(role => role.default === true)
            if(!("user_permissions" in values)){
                values.user_permissions =[]
            }
            if(values.user_permissions!== undefined){
                default_roles.map(i => values.user_permissions.push(i.slug))
            }
            
            this.props.CreateDesignations(values)
            this.setState({show_msg:true})
        }

    }

    componentDidMount() {
        this.props.DesignationsListing()
    }

    componentDidUpdate(prevProps){
        if(this.props.update){
                
            if((this.props.desigantions)&&this.props.desigantions.desgn&&this.props.desigantions.desgn.permission_groups&&this.props.desigantions.desgn!==prevProps.desigantions.desgn){   
                           
                datas.designation_name=this.props.desigantions.desgn.designation_name 
                
                this.getPermissiongroups(this.props.desigantions.desgn.permission_groups);
                this.props.update && this.props.initialize(datas);
            }
        }

        if(this.props.desigantions!==prevProps.desigantions&&this.props.desigantions&&this.props.desigantions.designationError&&this.props.desigantions.designationError.data&&
            this.props.desigantions.designationError.data.non_field_errors){
                this.setState({existed_error:this.props.desigantions.designationError.data.non_field_errors[0]})
        }

        
    }

    componentWillUnmount(){
        x=[]
    }

    CloseMsg=()=>{
        this.setState({show_msg:false})
    }

    designationExistedError =() =>{
        this.setState({existed_error:''})
      }

    render() {
      
        console.log('xxx',x)
        const { handleSubmit } = this.props

        if(this.props.desigantions&&this.props.desigantions.updatestatus==='success'){
            return <Redirect
                        to={{
                        pathname: '/departments/designations/'
                        }}
                    />
          }
    
        if(this.props.desigantions&&this.props.desigantions.createstatus === 'success'){
            return <Redirect
                        to={{
                        pathname: '/departments/designations/'
                        }}
                    />
        }
        


        return (

            <Fragment>
                <div className="col-md-10 offset-md-1 card card-signin mt-3 p-5">

                    {/* {this.state.show_msg &&(this.props.desigantions&&this.props.desigantions.status === 'success')&&
                    <SuccessMessage closemsg={this.CloseMsg} msg={`Designation ${this.props.desigantions.designation.designation_name} created succesfully` }/>
                    } */}

                    {/* {this.props.desigantions && this.props.desigantions.status === 'success' && this.props.desgn.designation &&
                        <SuccessMessage message={this.props.desigantions.designation.designation_name} />
                    } */}

                    {this.props.desigantions && (this.props.desigantions.createstatus === 'error'||this.props.desigantions.updatestatus === "error") && this.state.show_msg && <ErrorMessage closeMessage={this.CloseMsg} msg='Invalid details' />}

                    {/* {this.props.desigantions && this.props.desigantions.status === 'error' && <ErrorMessage />} */}

                    <div className="text-left  mb-5">
                        <h2>{this.props.update ? 'Update' : 'Add'} Designation</h2>
                    </div>

                    <form onSubmit={handleSubmit(this.submit)}>

                        <div className="form-group row">
                            <div className="col-md-3">
                                <label className="form-control-label" htmlFor="id_designation_name">
                                    <label htmlFor="id_designation_name">Designation name</label></label>
                                <span className="text-danger">*</span>

                            </div>
                            <div className="col-md-9">
                                {/* <input type="text" name="designation_name" placeholder="Enter Designation Name here" class="input-control input-control" maxlength="128" required="" id="id_designation_name" /> */}
                                <Field type="text"
                                    name="designation_name"
                                    component={renderField}
                                    className="input-control "
                                    label="Designation Name"
                                    existed={this.state.existed_error}
                                    onChange={this.designationExistedError}
                                    validate={[required,alphabetOnly]} />
                            </div>
                        </div>

                        <div className="form-group row ml-15">

                            <div className="col-md-3">
                                <label className="form-control-label" htmlFor="">
                                    <label>Roles</label></label>

                            </div>

                            <ul id="id_selection">
                                {(this.props.roles && this.props.roles.roles) &&
                                    <Field
                                        type="checkbox"
                                        name="user_permissions"
                                        component={checkboxField}
                                        options={this.props.roles.roles.filter(role => role.default === false)}
                                        y ={this.state.x_l}
                                    />
                                }
                            </ul>
                        </div>

                        <div className="m-5">
                            <input type="submit" className="btn width btn-primary float-right" value="SAVE" />
                        </div>
                    </form>
                </div>
            </Fragment>
        )
    }
}
OrganizationAddDesignationContent = reduxForm({
    form: 'addDesignationform'
})(OrganizationAddDesignationContent)
export default OrganizationAddDesignationContent