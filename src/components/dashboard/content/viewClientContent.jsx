import React, { Component, Fragment } from 'react'
import { Field, reduxForm } from 'redux-form'
import { Redirect } from 'react-router-dom';
import { Link } from 'react-router-dom'
import { alphabetOnly } from '../../../constants/validations'
import * as Helper from '../common/helper/helper';
import "../../../../node_modules/jquery/dist/jquery.min.js";
import "../../../../node_modules/bootstrap/dist/js/bootstrap.min.js";
import UpdateClientModal from '../modal/updateClientModal'
import LoaderImage from '../../../static/assets/image/loader.gif';
var loaderStyle={
   width: '3%',
   position: 'fixed',
   top: '50%',
   left: '57%'
   }
// import "../../../../node_modules"
//  clear localStorage on Browser close 
// window.onbeforeunload = function() {
//   localStorage.clear();
// }
const required = value => {
   return (value || typeof value === 'number' ? undefined : ' is Required')
}
const renderField = ({
   input,
   label,
   type,
   existed,
   meta: { touched, error, warning, }
}) => {
   return (
      <Fragment>
         <textarea {...input} placeholder={label} type={type} className={(touched && error) ? 'fullwidth input-control border border-danger' : 'fullwidth input-control'} />
         {touched && ((error && <small className="text-danger">{label}{error}</small>) || (warning && <span>{warning}</span>))}

      </Fragment>
   )
}


const renderHiddenField = ({
   input,
   label,
   type,
   value,

   meta: { touched, error, warning, }
}) => {
   return (
      <Fragment>
         <input {...input} type={type} value={value} />
      </Fragment>
   )
}

class ViewClientContent extends Component {
   constructor(props) {
      super(props);
      this.state = {
         clientSlug: '',
         existed_error: '',
         clientObj: {},
         searchData: '',
         editModal:false,
         editPart:'',
         index:''
      };
   }
   clearExistedError = () => {
      this.setState({ existed_error: '' })
   }
   
   updateClientModal=(editPart,index)=>{
      this.setState({editModal:true,editPart:editPart,index:index})
   }
   closeEditModal = () =>{
      this.setState({editModal:false,editPart:'',index:''})
   }

   submit = (values) => {

      values.client = this.state.clientSlug
      this.props.addClientComment(values)

   }
   componentDidMount() {
      this.setState({
         clientSlug: this.props.match.params.client_slug
      })
      this.props.retrieveClient(this.props.match.params.client_slug)
      this.props.listClients()
      this.props.listClientComments(this.props.match.params.client_slug)

     
   }
   getClient = (slug) => {
      this.setState({ clientSlug: slug })
      this.props.retrieveClient(slug)
      this.props.listClientComments(slug)
   }
   searchClients = (e) => {
      // console.log('eee',e.target.value)
      this.setState({ searchData: e.target.value })
   }
   render() {

   console.log('prp..................................', this.props.clientData&&this.props.clientData.client_obj)
   
   // let combined=[],resources=[],contracts=[],projectList=[]
      
   // projectList=this.props.clientData&&this.props.clientData.client_obj&&this.props.clientData.client_obj.projects

   // if(projectList&&projectList.length>0){

   // }

   // resources=
   //    this.props.clientData&&this.props.clientData.client_obj&&this.props.clientData.client_obj.projects&&
   //    this.props.clientData.client_obj.projects.financial_info&&this.props.clientData.client_obj.projects.financial_info.resources

   //    contracts=
   //    this.props.clientData&&this.props.clientData.client_obj&&this.props.clientData.client_obj.projects&&
   //    this.props.clientData.client_obj.projects.financial_info&&this.props.clientData.client_obj.projects.financial_info.contract_attachments

      
      // if((resources&&resources.length>0)&&(contracts&&contracts.length>0)){
      //    for (let reso in resources){
      //       for (let cont in contracts){
      //          if(resources[reso].resource_assigned.slug===contracts[cont].resource){
      //             combined.push(Object.assign(resources[reso],contracts[cont]))
      //          }
      //       }
      //    }
      // }
      // console.log('ssssssssssssssssssssssssssssss',combined);
      

      const { handleSubmit } = this.props;
      let clientObj, clientList
      if (this.props.clientData && this.props.clientData.client_obj) {
         clientObj = this.props.clientData.client_obj
      }
      if (this.props.clientData && this.props.clientData.clients && this.props.clientData.clients.length > 0) {
         if (this.state.searchData != "") {
            const lowercasedFilter = this.state.searchData.toLowerCase();
            let parameters = ["email", "first_name", "last_name"]
            clientList = this.props.clientData.clients.filter(item => {
               return parameters.some(key =>
                  item.client[key].toLowerCase().includes(lowercasedFilter)
               );
            });
         }
         else {
            clientList = this.props.clientData && this.props.clientData.clients
         }
      }
      return (
      
         <Fragment>
          <Fragment>
            <div className="col-sm-12">
               <div className="row">
                  <div className="search-grid">
                     <div className="searchList-bx border-right">
                        <input type="text" className="search-input" onChange={(e) => this.searchClients(e)} placeholder="Search..." />
                        <span className="search-addon">
                           <button className="btn btn-link-secondary" id="search-button">
                              <i className="icon-search"></i>
                           </button>
                        </span>
                        <div className="search-results">
                           <div className="searchList-bx-result">
                              <span>{clientList ? clientList.length
                                 : 'No '} contacts found</span>
                           </div>
                           <div className="userList-search">
                              <ul className="userList-search-list">
                                 {clientList && clientList.map(client =>
                                    <Fragment>
                                       <li data-id="09bf42ed-3e1d-4c68-9adb-821e88f88bb6" onClick={() => this.getClient(client.client.slug)}
                                          className={(client.client.slug === (clientObj && clientObj.client && clientObj.client.slug)) ? 'active' : 'dd'}
                                       >
                                          <span className="user-img-holder">
                                             {client.client.first_name.charAt(0)}{client.client.last_name.charAt(0)}
                                          </span>
                                          <span className="project-details" >
                                             <span className="project-details-title">
                                                {client.client.first_name} &nbsp;
                              {client.client.last_name}
                                             </span>
                                             <span className="project-details-user text-muted">{client.client.email}</span>
                                          </span>
                                       </li>
                                    </Fragment>
                                 )}
                              </ul>
                           </div>
                        </div>
                     </div>



                     <div className="userMail-grid user-mg-grd">
                        <div className="userMail-grid-nav">
                           <div className="inline-bx mt-1 mb-2">
                              <span className="user-img-holder">
                                 {clientObj && clientObj.client && clientObj.client.first_name.charAt(0)}{clientObj && clientObj.client && clientObj.client.last_name.charAt(0)}
                              </span>
                              <span className="project-details">
                                 <span className="project-details-title">
                                    {clientObj && clientObj.client && clientObj.client.first_name} &nbsp;
                     {clientObj && clientObj.client && clientObj.client.last_name}
                                 </span>
                              </span>
                           </div>
                           <div className="inline-bx mt-1 mb-2">
                              {clientObj && clientObj.client && clientObj.client.mobile ?
                                 <span className="icon-holder"><i class="icon-phone-o b-6"></i></span> : null}
                              <span className="project-details">
                                 <span className="project-details-title">{clientObj && clientObj.client && clientObj.client.mobile}</span>
                              </span>
                           </div>
                           <div className="inline-bx mt-1 mb-2">
                              <span className="icon-holder"><i class="icon-envelope b-6"></i></span>
                              <span className="project-details">
                                 <span className="project-details-title">{clientObj && clientObj.client && clientObj.client.email}</span>
                              </span>
                           </div>

                        </div>
                        <div className="userMail-grid-tab shadow">
                           <ul className="nav nav-tabs" id="myTab" role="tablist">
                              <li className="nav-item">
                                 <a class="nav-link active" data-toggle="tab" href="#overview">Overview</a>
                              </li>
                              <li className="nav-item">
                                 <a class="nav-link" data-toggle="tab" href="#comments">Comments</a>
                              </li>
                              <li className="nav-item">
                                 <a class="nav-link" data-toggle="tab" href="#contracts">Contracts</a>
                              </li>
                              <li className="nav-item">
                                 <a class="nav-link" data-toggle="tab" href="#sales">Sales</a>
                              </li>
                              <li className="nav-item">
                                 <a class="nav-link" data-toggle="tab" href="#timesheet">Timesheet</a>
                              </li>
                           </ul>
                           <div class="tab-content">
                              <div id="overview" class="container tab-pane active">
                                 <br></br>
                                 <div className="userList-search">
                                    <ul>
                                       <li className="project-details-user text-muted">
                                          <p className="user-img-holder clnt-lst12">
                                             {clientObj && clientObj.client && clientObj.client.first_name.charAt(0)}{clientObj && clientObj.client && clientObj.client.last_name.charAt(0)}
                                          </p>

                                          <div className="project-options">
                                             <button onClick= {()=>this.updateClientModal("basic")} className="btn btn-link-secondary client-form " id="client-contact-details" data-url="/clients/client-contact-details-update/09bf42ed-3e1d-4c68-9adb-821e88f88bb6/" data-toggle="modal" data-target="#editDetails">
                                                <i className="icon-pencil"></i>
                                             </button>
                                             {/* <button className="btn btn-link-secondary client-form" id="delete-client" data-url="/clients/delete-client/09bf42ed-3e1d-4c68-9adb-821e88f88bb6/" data-toggle="modal" data-target="#editDetails">
                                                <i className="icon-trash"></i>
                                             </button> */}
                                          </div>

                                          <span className="project-details overview">
                                             <p className="project-details-title">
                                                {clientObj && clientObj.client && clientObj.client.first_name} &nbsp;
                                       {clientObj && clientObj.client && clientObj.client.last_name}
                                             </p>
                                             <p ><i class="icon-envelope b-6"></i><span className="test">
                                                {clientObj && clientObj.client && clientObj.client.email}</span>
                                             </p>
                                             <p style={{ fontSize: "11px" }}>
                                                {clientObj && clientObj.client && clientObj.client.company_name}
                                             </p>
                                             {clientObj && clientObj.client && clientObj.client.mobile ?
                                                <p><i class="icon-phone-o b-6"></i><span className="test">{clientObj.client.mobile}</span></p>
                                                : null}
                                             {clientObj && clientObj.otherdetails && clientObj.otherdetails.skype_name ?
                                                <p > <i className="icon-skype-1 text-primary"></i>
                                                   <span className="test"> {clientObj.otherdetails.skype_name}</span>
                                                </p>
                                                : null}
                                             <p className="test">
                                                <a href="">{clientObj && clientObj.client && clientObj.client.website}</a>
                                             </p>
                                             {/* <span className="project-details-user text-muted">
                                    <a className>Edit &nbsp;</a><a>Send Email &nbsp;</a><a>Delete</a>
                                    </span> */}
                                          </span>
                                       </li>
                                       <hr />
                                       <li className="project-details-user text-muted grid-column">
                                          <div className="project-details overview">
                                             <span className="project-details-title" style={{ fontSize: "10px", color: "black" }}> OTHER DETAILS
                                             
                                             <button onClick= {()=>this.updateClientModal("otherdetails")} className="btn btn-link-secondary client-form " id="client-contact-details" data-url="/clients/client-contact-details-update/09bf42ed-3e1d-4c68-9adb-821e88f88bb6/" data-toggle="modal" data-target="#editDetails">
                                                <i className="icon-pencil"></i>
                                             </button>
                                             
                                             </span>
                                             <p>  CURRENCY :&nbsp;&nbsp;
                                       {clientObj && clientObj.otherdetails && clientObj.otherdetails.currency}
                                             </p>
                                             <p > PORTAL LANGUAGE :&nbsp;&nbsp;
                                       {clientObj && clientObj.otherdetails && clientObj.otherdetails.portal_language}
                                             </p>
                                             <p > PAYMENT TERM :&nbsp;&nbsp;
                                       {clientObj && clientObj.otherdetails && clientObj.otherdetails.payment_terms}
                                             </p>
                                             <p > PORTAL ACCESS :&nbsp;&nbsp;
                                       {clientObj && clientObj.otherdetails && clientObj.otherdetails.enable_portal?'Yes':'No'}
                                             </p>
                                             <p>
                                                FACEBOOK URL:{clientObj && clientObj.otherdetails && clientObj.otherdetails.facebook}
                                             </p>
                                             <p>
                                                SKYPE NAME:{clientObj && clientObj.otherdetails && clientObj.otherdetails.skype_name}
                                             </p>
                                          </div>
                                          {/* 
                              </li>
                              */}
                                          {/* 
                              <hr />
                              */}
                                          {/* 
                              <li className="project-details-user text-muted">
                                 */}
                                          <div className="project-details overview">
                                             <span className="project-details-title" style={{ fontSize: "10px", color: "black" }}> ADDRESS
                                             <button onClick= {()=>this.updateClientModal("address")} className="btn btn-link-secondary client-form " id="client-contact-details" data-url="/clients/client-contact-details-update/09bf42ed-3e1d-4c68-9adb-821e88f88bb6/" data-toggle="modal" data-target="#editDetails">
                                                <i className="icon-pencil"></i>
                                             </button>
                                             </span>
                                             <span className="project-details overview"></span>
                                             <p>
                                                ADDRESS1 : {clientObj && clientObj.billing_address && clientObj.billing_address.address1}
                                             </p>
                                             <p>
                                                ADDRESS2 : {clientObj && clientObj.billing_address && clientObj.billing_address.address2}
                                             </p>
                                             <p >
                                                PINCODE : {clientObj && clientObj.billing_address && clientObj.billing_address.pin_code}
                                             </p>
                                             <p>
                                                CITY :  {clientObj && clientObj.billing_address && clientObj.billing_address.city && clientObj.billing_address.city.name_ascii}
                                             </p>
                                             <p >
                                                STATE :  {clientObj && clientObj.billing_address && clientObj.billing_address.state && clientObj.billing_address.state.name_ascii}
                                             </p>
                                             <p >
                                                COUNTRY :  {clientObj && clientObj.billing_address && clientObj.billing_address.country && clientObj.billing_address.country.name_ascii}
                                             </p>
                                          </div>
                                       </li>
                                       <hr />
                                       <li className="project-details-user text-muted grid-column">
                                          <div className="project-details overview">
                                             <span className="project-details-title" style={{ fontSize: "10px", color: "black" }}> CONTACT PERSONS
                                             
                                             </span>
                                             {clientObj && clientObj.contact_persons && clientObj.contact_persons.length > 0 ?
                                                clientObj.contact_persons.map((contact_person,index) =>
                                                   <Fragment>
                                                      
                                                      <button onClick= {()=>this.updateClientModal("contactperson_single",index)} className="btn btn-link-secondary client-form " id="client-contact-details" data-url="/clients/client-contact-details-update/09bf42ed-3e1d-4c68-9adb-821e88f88bb6/" data-toggle="modal" data-target="#editDetails">
                                                <i className="icon-pencil"></i>
                                             </button>
                                                      <p>SALUTATION:{contact_person.contactperson_salutation}</p>
                                                      <p>FIRST NAME:{contact_person.contactperson_first_name}</p>
                                                      <p>LAST NAME:{contact_person.contactperson_first_name}</p>
                                                      <p>EMAIL:{contact_person.contactperson_email}</p>
                                                      <p>MOBILE NUMBER:{contact_person.contactperson_mobile_number}</p>
                                                      <p>SKYPE NAME:{contact_person.contactperson_skype_name}</p>
                                                      <hr/>
                                                   </Fragment>
                                                ) : 
                                                <Fragment>
                                                   No Contact Persons Found
                                                   <button onClick= {()=>this.updateClientModal("contactperson")} className="btn btn-link-secondary client-form " id="client-contact-details" data-url="/clients/client-contact-details-update/09bf42ed-3e1d-4c68-9adb-821e88f88bb6/" data-toggle="modal" data-target="#editDetails">
                                                <i className="icon-pencil"></i>
                                             </button>
                                                </Fragment>
                                                
                                             }
                                          </div>
                                        
                                          <div className="project-details overview">
                                             <span className="project-details-title" style={{ fontSize: "10px", color: "black" }}> REMARKS
                                             <button onClick= {()=>this.updateClientModal("remarks")} className="btn btn-link-secondary client-form " id="client-contact-details" data-url="/clients/client-contact-details-update/09bf42ed-3e1d-4c68-9adb-821e88f88bb6/" data-toggle="modal" data-target="#editDetails">
                                                <i className="icon-pencil"></i>
                                             </button>
                                             </span>
                                             {(clientObj && clientObj.remark && clientObj.remark.remarks) ?
                                                clientObj.remark.remarks : 'No Remarks'
                                             }
                                          </div>
                                       </li>
                                    </ul>
                                 </div>
                              </div>
                              <div className="container tab-pane fade" id="comments" role="tabpanel" aria-labelledby="remarks-tab">

                                 {/* <textarea className="remarks" cols="40" rows="10" placeholder="Enter Comments" className="input-control" id="id_remarks"></textarea> */}
                                 <form className="row" onSubmit={handleSubmit(this.submit)}>
                                    <Field type="text"
                                       name="comment"
                                       cols="40" rows="10"
                                       component={renderField}
                                       label="Comment"
                                       validate={required}
                                       className="input-control input-control"
                                    />
                                    {/* <span className="text-sm">(For Internal use)</span> */}
                                    <div className="text-sm comment-width">
                                       <button className="btn btn-link-vilot" type='submit'>
                                          ADD COMMENT</button>
                                    </div>
                                 </form>



                                 {this.props.clientData && this.props.clientData.comment_list_status === 200 && this.props.clientData.comment_list &&
                                    this.props.clientData.comment_list.length > 0 ?
                                    this.props.clientData.comment_list.map(commentObj =>
                                       <Fragment>
                                          <ul>
                                             <li className="project-details-user text-muted">
                                                <span className="test-sm" style={{ color: "black" }}>{commentObj.comment}  </span>
                                                <p>{commentObj.commented_by} &nbsp;{commentObj.date_time}</p>

                                             </li>
                                          </ul>
                                       </Fragment>

                                    ) :
                                    'No comments here'

                                 }

                              </div>
                              
                              
                                 
                                 
                              <div id="contracts" className="container tab-pane fade">

                           {this.props.clientData?.client_obj?.projects.map((obj,index)=>
                                 <Fragment>
                                 <div className=" text-muted" style={{ textAlign: "center", marginTop: "10px" }}>Project Name&nbsp;: {obj?.project_name}
                     </div>
                                 <ul className="nav nav-tabs" id="myTab" style={{ fontSize: "11px" }} role="tablist">
                                    
                                    <li className="nav-item">
                                       <a class="nav-link active" data-toggle="tab" href={`#contract_${index}`}>Contract</a>
                                    </li>
                                    <li className="nav-item">
                                       <a class="nav-link " data-toggle="tab" href={`#term_${index}`}>Terms</a>
                                    </li>
                                    {/* <li className="nav-item">
                                       <a class="nav-link" data-toggle="tab" href="#resourcesAssigned">Resources Assigned</a>
                                    </li> */}
                                 </ul>
                                 <div class="tab-content">

                                 <div id={`contract_${index}`} className="container tab-pane active">

                                    <label className="text-light-grey" htmlFor="id_photo">
                                    
                                    
                                    <table className="table border-none">
                                             <thead>
                                                <th>Resource Name</th>
                                                <th>Contract</th>
                                             </thead>
                                       {obj?.resources?.map(res=>
                                       
                                          
                                             <tbody>
                                                <td>{res?.resource_assigned?.first_name} {res?.resource_assigned?.last_name}</td>
                                                <td>{res?.contracts?.map((cont)=>(<table>
                                                   
                                                   <tbody>
                                                      {/* <td>{cont.contract_number}</td>
                                                      <td>{cont.contract}</td> */}
                                                      
                                                      {cont.contract?
                                                      <td className="border-none"> <button type="button" onClick={() => this.props.downloadContract(cont.contract_id, cont.contract)} style={{width:"75px",fontSize:"12px"}}  className="btn btn-outline-primary add-row">Download</button></td>
                                                   : 'Nil'
                                                   }
                                                   </tbody>
                                                </table>))}</td>
                                             </tbody>)}
                                          </table>
                                          {/* <h5>Resource Name:</h5> {res?.resource_assigned?.first_name} {res?.resource_assigned?.last_name}
                                          <h5> Contract: </h5> {res?.contracts?.map((cont)=>(<Fragment>
                                          <div>{cont.contract}</div>
                                          <div>{cont.contract_number}</div>
                                          </Fragment>))} */}
                                      
                                       

                                    </label>
                                 </div>

                                   
                                    <div id={`term_${index}`} className="container tab-pane fade">
                                    <table className="table">
                                         <thead><th>Resource Name</th><th>Terms</th></thead>
                                         <Fragment>
                                         {obj?.resources?.map(res=>
                                    

                                         <Fragment>
                                            <tbody>
                                            <td>{res?.resource_assigned?.first_name} {res?.resource_assigned?.last_name}</td>
                                    {res?.terms?.length>0?<Fragment>
                                    {res?.terms?.map((term)=>
                                         
                                         <Fragment>
                                         {term.termAttachment?
                                         <td > 
                                          <button type="button" style={{width:"75px",fontSize:"12px"}} onClick={() => this.props.downloadTermAttachment(term.term_attachment_id, term.termAttachment)}  className="btn btn-outline-primary add-row">Download</button>
                                          </td>:
                                         'Nil'
                                         }
                                         </Fragment>
                                         )}</Fragment>:<th>No Terms Found</th>}
                                         </tbody> 
                                         </Fragment>
                                         )}
                                         </Fragment>
                                       
                                    </table>
                                    </div>
                                   
                                 </div>
                              
                                 </Fragment>)}
                              </div>
                              
                              
                              <div id="sales" className="container tab-pane fade">
                                 sales
                  </div>
                              <div id="timesheet" className="container tab-pane fade">
                                 Timesheet
                  </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            {this.state.editModal&&<UpdateClientModal {...console.log("UPDATE",this.props,this.state)} 
             {...this.props} closeModal={this.closeEditModal} index={this.state.index} clientSlug={this.state.clientSlug} editpart={this.state.editPart}
             
             />}
         </Fragment>
         
      </Fragment>
      )
   }
}

ViewClientContent = reduxForm({
   form: 'addClientCommentform'
})(ViewClientContent)
export default ViewClientContent

