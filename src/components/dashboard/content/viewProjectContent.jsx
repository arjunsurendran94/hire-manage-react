import React, { Component, Fragment } from "react";
import { Field, FieldArray, reduxForm } from "redux-form";
import { Redirect } from "react-router-dom";
import { alphabetOnly } from "../../../constants/validations";
import * as Helper from "../common/helper/helper";
import LoaderImage from "../../../static/assets/image/loader.gif";
import Select from "react-select";
import DatePicker from "react-datepicker";
import AddTaskModal from "../modal/addTaskModal";
import { Link } from "react-router-dom";
import ErrorMessage from "../../common/ErrorMessage";
import Moment from "moment";
import momentDurationFormatSetup from "moment-duration-format";
import TimeField from "react-simple-timefield";
import DurationInput from "react-duration";

var loaderStyle = {
  width: "3%",
  position: "fixed",
  top: "50%",
  left: "57%",
};
const required = (value) => {
  return value || typeof value === "number" ? undefined : " is Required";
};
const positiveInteger = (value) => {
  var positiveregex = /^[1-9]\d*$/;
  return value && positiveregex.test(value) ? undefined : " is Invalid";
};
const mobileValid = (value) => {
  var mobilenoregex = /^(?!\b(0)\1+\b)(\+?\d{1,3}[. -]?)?\(?\d{3}\)?([. -]?)\d{3}\3\d{4}$/;
  if (value && value.length >= 1) {
    return mobilenoregex.test(value) ? undefined : " is Invalid";
  }
  return undefined;
};
const email = (value) =>
  value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
    ? " is Invalid"
    : undefined;
const websiteValiadtion = (value) => {
  if (value) {
    var pattern = new RegExp(
      "^(https?:\\/\\/)" + // protocol
      "((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|" + // domain name
      "((\\d{1,3}\\.){3}\\d{1,3}))" + // OR ip (v4) address
      "(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*" + // port and path
      "(\\?[;&a-z\\d%_.~+=-]*)?" + // query string
      "(\\#[-a-z\\d_]*)?$",
      "i"
    ); // fragment locator
    return !!pattern.test(value)
      ? undefined
      : "Enter a valid url ( eg:https://websitebuilders.com )";
  }
};

const currency = [
  { value: "abc", label: "asdf" },
  { value: "wer", label: "uji" },
];

const renderTextField = ({
  input,
  label,
  type,

  meta: { touched, error, warning },
}) => {
  return (
    <Fragment>
      <textarea
        {...input}
        placeholder={label}
        type={type}
        className={
          touched && error
            ? "fullwidth input-control border border-danger"
            : "fullwidth input-control"
        }
      />
      {touched &&
        ((error && (
          <small className="text-danger">
            {label}
            {error}
          </small>
        )) ||
          (warning && <span>{warning}</span>))}
    </Fragment>
  );
};

const renderField = ({
  input,
  label,
  type,
  existed,
  work_phone_number_error,
  mobile_number_error,
  website_error,
  facebook_error,
  pin_code_error,
  meta: { touched, error, warning },
}) => {
  return (
    <Fragment>
      <input
        {...input}
        placeholder={label}
        type={type}
        className={
          (touched && error) ||
            (existed && label === "Contact Email") ||
            work_phone_number_error ||
            mobile_number_error ||
            website_error ||
            facebook_error ||
            pin_code_error
            ? "fullwidth input-control border border-danger"
            : "fullwidth input-control"
        }
      />
      {touched &&
        ((error && (
          <small className="text-danger">
            {label}
            {error}
          </small>
        )) ||
          (warning && <span>{warning}</span>))}
      {label === "Contact Email" && existed && (
        <small class="text-danger">{existed}</small>
      )}
      {work_phone_number_error && (
        <small class="text-danger">{work_phone_number_error}</small>
      )}
      {mobile_number_error && (
        <small class="text-danger">{mobile_number_error}</small>
      )}
      {website_error && <small class="text-danger">{website_error}</small>}
      {facebook_error && <small class="text-danger">{facebook_error}</small>}
      {pin_code_error && <small class="text-danger">{pin_code_error}</small>}
    </Fragment>
  );
};
const radioCheckbox = ({
  input,
  label,
  options,
  type,
  checked,
  meta: { touched, error, warning },
}) => (
    <Fragment>
      {options.map((plan, index) => (
        <div className="col-md-3" key={index}>
          <input
            type="radio"
            {...input}
            checked={checked}
            key={index}
            name={plan.name}
            value={plan.value}
            id={plan.id}
          />{" "}
          {plan.text}
        </div>
      ))}
      {touched &&
        ((error && (
          <small className="text-danger">
            {label}
            {error}
          </small>
        )) ||
          (warning && <span>{warning}</span>))}
    </Fragment>
  );
const renderCheckBoxField = ({
  input,
  label,
  options,
  type,
  checked,
  meta: { touched, error, warning },
}) => (
    <Fragment>
      <input type="checkbox" {...input} />
    </Fragment>
  );

const renderDateField = ({
  input,
  label,
  options,
  type,
  startingDate,
  checked,
  meta: { touched, error, warning },
}) => (
    <Fragment>
      <input
        {...input}
        type="date"
        name="start_date"
        className="fullwidth input-control"
        label="Start Date"
        id="myDate"
        value={startingDate}
      />

      {touched &&
        ((error && (
          <small className="text-danger">
            {label}
            {error}
          </small>
        )) ||
          (warning && <span>{warning}</span>))}
    </Fragment>
  );

const renderAttachmentField = ({ fields, meta: { error, submitFailed } }) => (
  <ul>
    <li>
      <tr className="dynamic-form-add">
        <td colspan="2">
          <a className="add-row" href="javascript:void(0)">
            <div className="row">
              <div className="col-sm-12 text-center">
                <button
                  type="button"
                  className="btn btn-outline-primary add-row"
                  onClick={() => fields.push({})}
                >
                  <i className="icon-plus"></i> Add Attachment
                </button>
              </div>
            </div>
          </a>
        </td>
      </tr>

      {/* {submitFailed && error && <span>{error}</span>} */}
    </li>

    <Fragment></Fragment>

    {fields.map((member, index) => (
      <li key={index}>
        {/* <button
              type="button"
              title="Remove Member"
              onClick={() => fields.remove(index)}
            /> */}
        <tr className="contract_formset_row dynamic-form">
          <td className="">
            <input
              type="file"
              name="contract_formset_set-0-contract"
              className="form-control"
              id="id_contract_formset_set-0-contract"
            />
            <div className="form-control-error-list"></div>
          </td>
          <td>
            <input
              type="hidden"
              name="contract_formset_set-0-DELETE"
              id="id_contract_formset_set-0-DELETE"
            />
            <a
              onClick={() => fields.remove(index)}
              className="btn btn-outline-primary delete-row"
              href="javascript:void(0)"
            >
              <i class=" icon-delete"></i> Remove
            </a>
          </td>
        </tr>
      </li>
    ))}
  </ul>
);

const renderResourceField = ({
  fields,
  fixed,
  meta: { error, submitFailed },
}) => (
    <div className="addMore">
      <div className="col-sm-12">
        <button
          type="button"
          className="btn btn-outline-primary add-row"
          onClick={() => fields.push({})}
        >
          <i className="icon-plus"></i> Add More
      </button>
      </div>

      {fields.map((member, index) => (
        <li key={index}>
          <div className="col-sm-4 form-group">
            <label className="text-light-grey" for="id_rate">
              Rate
          </label>
            <Field
              type="text"
              name="rate"
              component={renderField}
              label="Hourly Rate"
            />
          </div>

          {!fixed && (
            <div className="col-sm-4 form-group">
              <label className="text-light-grey" for="id_weekly_limit">
                Weekly limit
            </label>
              <Field
                type="text"
                name="id_weekly_limit"
                component={renderField}
                label="Weekly Limit"
              />
            </div>
          )}
          <div className="col-sm-4 form-group">
            <label
              for="id_client"
              className="text-light-grey"
              style={{ marginBottom: "10px" }}
            >
              Add Resource
          </label>
            <Select />
          </div>

          <a
            onClick={() => fields.remove(index)}
            className="btn btn-outline-primary delete-row"
            href="javascript:void(0)"
          >
            <i class=" icon-delete"></i> Remove
        </a>
        </li>
      ))}
    </div>
  );

const renderContractAttachmentField = ({
  fields,
  meta: { error, submitFailed },
}) => (
    <ul>
      <li>
        <tr className="dynamic-form-add">
          <td colspan="2">
            <a className="add-row" href="javascript:void(0)">
              <div className="row">
                <div className="col-sm-12 text-center">
                  <button
                    type="button"
                    className="btn btn-outline-primary add-row"
                    onClick={() => fields.push({})}
                  >
                    <i className="icon-plus"></i> Add Attachment
                </button>
                </div>
              </div>
            </a>
          </td>
        </tr>

        {/* {submitFailed && error && <span>{error}</span>} */}
      </li>

      {fields.map((member, index) => (
        <li key={index}>
          {/* <button
           type="button"
           title="Remove Member"
           onClick={() => fields.remove(index)}
         /> */}
          <tr className="contract_formset_row dynamic-form">
            <td className="">
              <input
                type="file"
                name="contract_formset_set-0-contract"
                className="form-control"
                id="id_contract_formset_set-0-contract"
              />
              <div className="form-control-error-list"></div>
            </td>
            <td>
              <input
                type="hidden"
                name="contract_formset_set-0-DELETE"
                id="id_contract_formset_set-0-DELETE"
              />
              <a
                onClick={() => fields.remove(index)}
                className="btn btn-outline-primary delete-row"
                href="javascript:void(0)"
              >
                <i class=" icon-delete"></i> Remove
            </a>
            </td>
          </tr>
        </li>
      ))}
    </ul>
  );
let taskFields = [],
  OldTasks = [],
  NewTasks = [];

var dateObj = new Date();
var Currentdate =
  dateObj.getFullYear() +
  "-" +
  (dateObj.getMonth() + 1) +
  "-" +
  dateObj.getDate();
var time =
  dateObj.getHours() + ":" + dateObj.getMinutes() + ":" + dateObj.getSeconds();
var currentDateTime = Currentdate + " " + time;

let projectObj, contracts = [], billableResources = [], combined = [], termAttachments = [];



class ViewProjectContent extends Component {
  constructor(props) {
    super(props);
    var date = new Date();

    // this.ActiveRef = React.createRef();
    // this.InActiveRef = React.createRef();


    var formatedDate = `${date.getFullYear()}-${(
      "0" +
      (date.getMonth() + 1)
    ).slice(-2)}-${("0" + date.getDate()).slice(-2)}`;

    this.state = {
      show_branch: false,
      success_message: false,
      existed_error: "",
      other_details: false,
      checked: false,
      isOpen: false,
      emailValue: "",
      passwordValue: "",
      select_client: false,
      select_internal: false,
      startingDate: formatedDate,
      startingDateError: "",
      editModal: false,
      editPart: "",
      index: "",
      comments: null,
      commentError: null,
      employeeList: "",
      expandNav: false,
      task_name: "",
      assigned: "",
      due_date: "",
      task_board: "",
      task_description: "",
      errors: {
        task_name: "",
        assigned: "",
        due_date: "",
        task_description: "",
        task_board: "",
      },
      taskResponse: true,
      type: "",
      taskObj: "",
      subtaskObj: "",
      subtasks: "",
      errorMessages: [],
      isClicked: false,
      historyLog: false,
      timeLog: false,
      startTime: true,
      individualEdit: "",
      taskIndex: "",

      add_start_date: "",
      add_end_date: "",
      add_start_time: "",
      add_end_time: "",

      duration_error: "",

      add_start_date_error: "",
      add_end_date_error: "",
      add_start_time_error: "",
      add_end_time_error: "",
      started: false,
      seconds: "",
      hours: "",
      minutes: "",

      subTaskLog: [],
      taskComments: [],
      taskComment: "",
      taskCommentError: "",
      listEdit: false,
      taskType: localStorage.getItem("user_type") === "admin" ? 'allTask' : 'myTask',
      timeLogObj: '',
      startTimerTask: '',
      weeklyWorksheetEmp: '',
      projectObj: ''
    };
  }
  clearExistedError = (params, e) => {
    params === "email" && this.setState({ existed_error: "", emailValue: e });
    params === "work_phone" && this.setState({ work_phone_number_error: "" });
    params === "mobile_number" && this.setState({ mobile_number_error: "" });
    params === "website" && this.setState({ website_error: "" });
    params === "facebook" && this.setState({ facebook_error: "" });
    params === "pin_code" && this.setState({ pincodeError: "" });
  };
  select_client = () => {
    this.setState({ select_client: true, select_internal: false });
  };
  select_internal = () => {
    this.setState({ select_internal: true, select_client: false });
  };
  select_hourly = () => {
    this.setState({ select_hourly: true, select_fixed: false });
  };
  select_fixed = () => {
    this.setState({ select_fixed: true, select_hourly: false });
  };
  closeSuccessMessage = () => {
    this.setState({
      success_message: false,
    });
  };

  openNav = (type, taskObj, subtaskObj) => {
    //  debugger
    if (type === "view_task") {
      let errors = { ...this.state.errors };
      Object.keys(errors).forEach((k) => (errors[k] = ""));
      this.setState(
        {
          openNav: true,
          type: type,
          taskObj: taskObj,
          taskResponse: true,
          individualEdit: "",
          errors: errors,
          listEdit: false,
        },
        () =>
          type === "view_task" &&
          (this.props.listingSubTask(taskObj.slug),
            this.props.listingTaskLog(taskObj.slug),

            this.props.listTimeLog({
              ...(localStorage.getItem('user_type') != 'admin' && ({ employee: taskObj?.assigned?.slug })),
              task: taskObj.slug,
              project: this?.props?.match?.params?.project_slug,
            })
          ),
        this.props.listingTaskComments(taskObj.slug)
      );
    } else if (type === "add_task") {
      this.setState({
        openNav: true,
        type: type,
        taskObj: taskObj,
        subtaskObj: subtaskObj,
        expandNav: false,
      });
    } else if (type === "time_log") {

      this.setState({
        openNav: true,
        type: type,
        taskObj: taskObj,
        subtaskObj: subtaskObj,
        expandNav: false,
      }, (this.state.taskType === 'allTask' ?
        () => this.props.listTimeLog({ project: this?.props?.match?.params?.project_slug })
        :
        () => this.props.listTimeLog({
          employee: localStorage.getItem('admin_employee_slug'),
          project: this?.props?.match?.params?.project_slug
        })
      )
      );

    }
    else {
      this.setState({
        openNav: true,
        type: type,
        taskObj: taskObj,
        subtaskObj: subtaskObj,
        expandNav: false,
      });
    }

    // document.getElementById("mySidenav").style.width = "250px";
  };

  editTaskSubTask = (obj, type) => {
    // debugger
    if (type === "edit_task") {
      this.setState({
        task_name: obj.task_name,
        task_description: obj.task_description,
        due_date: obj.due_date,
        openNav: true,
        type: type,
        assigned: obj && obj.assigned && obj.assigned.slug,
        task_board: obj.task_board,
      });
    }
    if (type === "edit_subtask") {
      this.setState({
        task_name: obj.sub_task_name,
        task_description: obj.task_description,
        due_date: obj.due_date,
        openNav: true,
        type: type,
        assigned: obj && obj.assigned && obj.assigned.slug,
        task_board: obj.sub_task_board,
      });
    }
  };

  expandNav = () => {
    let expandNav = this.state.expandNav;
    this.setState({
      expandNav: !expandNav,
      isClicked: false,
    });
  };
  historyLog = () => {
    let expandNav = this.state.expandNav;
    let historyLog = this.state.historyLog;
    this.setState({
      expandNav: true,
      historyLog: true,
      timeLog: false,
      isClicked: false,
      // openNav:false
    });
  };
  timeLog = () => {
    let timeLog = this.state.timeLog;
    this.setState({
      expandNav: true,
      timeLog: true,
      historyLog: false,
    });
  };
  handleIndividualChange = (e) => {

    let taskObj = { ...this.state.taskObj }
    // this.setState((prevState=>({taskObj:{...prevState.taskObj,task_name:e.target.value}})))
    taskObj[e.target.name] = e.target.value

    // debugger
    this.setState({ taskObj: taskObj })
  }
  handleIndividualStatusOrAssignedChange = (e) => {

    let individualEdit = this.state.individualEdit
    const form_data = new FormData();
    form_data.append(individualEdit, e.value);

    this.props.editTask(form_data
      , this.state.taskObj && this.state.taskObj.slug)
  }
  addTimeSubmit = () => {

    let add_end_date_error = '', add_end_time_error = '', add_start_date_error = '', add_start_time_error = '', valid = true, duration_error = ''
    let errors = { ...this.state.errors }

    if (!this.state.add_start_date) {
      add_start_date_error = 'Required'
      valid = false
    }
    // if(!this.state.add_start_time){
    //    add_start_time_error='Required'
    //    valid=false
    // }
    if (!this.state.add_end_date) {
      add_end_date_error = 'Required'
      valid = false
    }
    // if(!this.state.add_end_time){
    //    add_end_time_error='Required'
    //    valid=false
    // }
    if (this.state.taskObj?.assigned === null) {
      valid = false
      errors.assigned = 'Please Select a Assignee '
    }
    if (!this.state.seconds || !this.state.minutes || !this.state.hours) {
      valid = false
      duration_error = 'Invalid Time format'
    }

    if (valid) {
      // debugger
      let data = {}
      data.employee = this.state.taskObj?.assigned?.slug
      data.task = this.state.taskObj?.slug
      data.project = this.state.taskObj?.project
      let start_time = this.state.add_start_date
      let end_time = this.state.add_end_date


      // let startDateTimeMom=Moment(start_time)
      // let endTimeMom=Moment(end_time)
      // let dif=endTimeMom.diff(startDateTimeMom)
      // let dur=Moment.duration(dif)

      // let minutes=dur.minutes()
      // let seconds=dur.seconds()

      // // var s = Math.floor(d.asHours()) + moment.utc(ms).format(":mm:ss");

      // let calculatedDuration = Math.floor(dur.asHours()) + ':'+minutes+':'+seconds



      data.start_time = start_time + " " + "00:00:00"
      data.end_time = end_time + " " + "00:00:00"
      data.worked_hours = `${this.state.hours}:${this.state.minutes}:${this.state.seconds}`
      data.productive_hours = `${this.state.hours}:${this.state.minutes}:${this.state.seconds}`
      data.billable_hours = `${this.state.hours}:${this.state.minutes}:${this.state.seconds}`



      this.setState({ isClicked: !this.state.isClicked }, () => this.props.addTime(data))
    }
    else {
      this.setState({
        add_start_date_error: add_start_date_error, add_start_time_error: add_start_time_error,
        add_end_date_error: add_end_date_error, add_end_time_error: add_end_time_error, errors: errors, duration_error: duration_error
      })
    }

  }
  formatString = (str) => {
    // debugger
    if (str != '') {
      let str1 = str.replace(/_/g, '  ')
      str1 = str1.charAt(0).toUpperCase() + str1.substr(1).toLowerCase()
      return str1
    }
  }


  startTimer = (e, type, task) => {
    e.preventDefault()
    // debugger
    if (type === "start") {
      // let dateObj = new Date();
      // let Currentdate =
      //   dateObj.getFullYear() +
      //   "-" +
      //   (dateObj.getMonth() + 1) +
      //   "-" +
      //   dateObj.getDate();
      // let time =
      //   dateObj.getHours() +
      //   ":" +
      //   dateObj.getMinutes() +
      //   ":" +
      //   dateObj.getSeconds();
      // let currentDateTime = Currentdate + " " + time;

      let currentDateTime = Moment().format('YYYY-MM-DD HH:mm:ss')

      let data = {
        project: this?.props?.match?.params?.project_slug,
        start_time: currentDateTime,
        task: task,
        employee: localStorage.getItem('admin_employee_slug'),
        tracking: true
      };
      const form_data = new FormData();
      for (var key in data) {
        if (data[key] !== " " && data[key] != null) {
          form_data.append(key, data[key]);
        }
      }

      this.props.startTime(data);

    }
    if (type === "end") {

      let timeLogObj = this.props.tasks?.timelog?.find(
        (i) => i.end_time === null
      );
      let startDateTime = timeLogObj.start_time_format;
      let slug = timeLogObj.slug;

      // startDateTime=this.convertToLocalTime(startDateTime)
      let dateTime = new Date(startDateTime);
      // dateTime.setHours(dateTime.getHours() + 5);
      // dateTime.setMinutes(dateTime.getMinutes() + 30);

      // return Moment(dateTime).format('DD-MM-YYYY HH:mm:ss')

      startDateTime = Moment(dateTime).format("YYYY-MM-DD HH:mm:ss");
      let currentDateTimeObj = new Date();
      let currentDateTime = Moment().format('YYYY-MM-DD HH:mm:ss')
      let currentDT = Moment(currentDateTime);
      let dif = currentDT.diff(startDateTime);
      let dur = Moment.duration(dif);

      let minutes = dur.minutes();
      let seconds = dur.seconds();

      // var s = Math.floor(d.asHours()) + moment.utc(ms).format(":mm:ss");

      var calculatedDuration =
        Math.floor(dur.asHours()) + ":" + minutes + ":" + seconds;


        
      this.props.endTime(
        {
          end_time: Moment().format('YYYY-MM-DD HH:mm:ss'),
          worked_hours: calculatedDuration,
          billable_hours: calculatedDuration,
          productive_hours: calculatedDuration,
          tracking: true
        },
        slug
      );
    }

  };
  startTime = (type) => {
    let startTime = this.state.startTime;
    // debugger
    if (type === "start") {
      if (this.state.taskObj?.assigned?.slug != null) {
        let dateObj = new Date();
        let Currentdate =
          dateObj.getFullYear() +
          "-" +
          (dateObj.getMonth() + 1) +
          "-" +
          dateObj.getDate();
        let time =
          dateObj.getHours() +
          ":" +
          dateObj.getMinutes() +
          ":" +
          dateObj.getSeconds();
        let currentDateTime = Currentdate + " " + time;
        let data = {
          employee: this.state.taskObj?.assigned?.slug,
          task: this.state.taskObj?.slug,
          project: this?.props?.match?.params?.project_slug,
          start_time: currentDateTime

        };

        this.props.startTime(data);
      } else {
        this.setState((prevState) => ({
          errors: {
            ...prevState.errors,
            assigned: "Please Select an Assignee before Starting Task",
          },
        }));
      }
    }
    if (type === "end") {
      // debugger
      let timeLogObj = this.props.tasks?.timelog?.find(
        (i) => i.end_time === null
      );
      let startDateTime = timeLogObj.start_time_format;
      let slug = timeLogObj.slug;

      // startDateTime=this.convertToLocalTime(startDateTime)
      let dateTime = new Date(startDateTime);
      dateTime.setHours(dateTime.getHours() + 5);
      dateTime.setMinutes(dateTime.getMinutes() + 30);

      // return Moment(dateTime).format('DD-MM-YYYY HH:mm:ss')

      startDateTime = Moment(dateTime).format("YYYY-MM-DD HH:mm:ss");
      let currentDateTimeObj = new Date();
      let currentDateTime = Moment(currentDateTimeObj).format(
        "YYYY-MM-DD HH:mm:ss"
      );
      let currentDT = Moment(currentDateTimeObj);
      let dif = currentDT.diff(startDateTime);
      let dur = Moment.duration(dif);

      let minutes = dur.minutes();
      let seconds = dur.seconds();

      // var s = Math.floor(d.asHours()) + moment.utc(ms).format(":mm:ss");

      var calculatedDuration =
        Math.floor(dur.asHours()) + ":" + minutes + ":" + seconds;



      this.props.endTime(
        {
          end_time: currentDateTime,
          worked_hours: calculatedDuration,
          billable_hours: calculatedDuration,
          productive_hours: calculatedDuration,
        },
        slug
      );
    }
  };

  closeNav = () => {
    let errors = { ...this.state.errors };
    Object.keys(errors).forEach((k) => (errors[k] = ""));
    this.setState({
      openNav: false,
      errors: errors,
      task_name: "",
      task_description: "",
      assigned: "",
      due_date: "",
      individualEdit: "",
      isClicked: false,
      historyLog: false,
      timeLog: false,
    });
    // document.getElementById("mySidenav").style.width = "0";
  };
  addTaskModal = (editPart, index, obj, emp) => {

    this.setState({ editModal: true, editPart: editPart, index: index, timeLogObj: obj, weeklyWorksheetEmp: emp });
  };
  closeEditModal = () => {
    this.setState({ editModal: false, editPart: "", index: "" });
  };

  simulateClick = (e) => {
    document.getElementById("submit-btn").click();
  };
  handleBranchChange = (e) => {
    this.props.change("Department", "");
    this.props.retrieveDepartments(e.target.value);
  };
  upLoadFile = (e) => {
    let image = e.target.files[0];
    var imageTypes = ["image/png", "image/jpeg", "image/gif"];
    var image_index = imageTypes.indexOf(image.type);
    if (image_index >= 0) {
      this.setState({ image: image, image_validation_error: false }, () => {

      });
    } else {
      this.setState({ image: null, image_validation_error: true });
    }
  };
  handleCountryChange = (e) => {
    // making fields null
    this.props.change("state", "");
    this.props.change("city", "");
    this.props.fetchStatesList(e.target.value);
    // city error resolves after u uncomment it
    this.props.fetchCityList(" ");
  };
  errorOnChange = (param) => {
    if (param === "pincode") {
      this.setState({ pincodeError: "" });
    }
  };

  toggleEnablePortalModal = (e) => {
    e.target.checked
      ? this.setState({ isOpen: !this.state.isOpen, checked: e.target.checked })
      : this.setState({ checked: e.target.checked, passwordValue: "" });
  };

  closePortalModal = () => {
    this.setState({ isOpen: false });
  };
  handleStateChange = (e) => {
    // making fields null
    this.props.change("city", "");
    this.props.fetchCityList(e.target.value);
  };
  submit = (values) => {
    if (!values.hasOwnProperty("organization")) {
      values.organization = localStorage.getItem("organization_slug");
    }
    if (!values.hasOwnProperty("branch")) {
      values.branch = null;
    }
    if (!values.hasOwnProperty("department")) {
      values.department = null;
    }
    if (!values.hasOwnProperty("company_name")) {
      values.company_name = "";
    }
    if (!values.hasOwnProperty("mobile")) {
      values.mobile = "";
    }
    if (!values.hasOwnProperty("work_phone")) {
      values.work_phone = "";
    }
    if (!values.hasOwnProperty("client_display_name")) {
      values.client_display_name = "";
    }
    if (!values.hasOwnProperty("website")) {
      values.website = "";
    }
    values.photo = null;
    // other details
    if (!values.hasOwnProperty("currency")) {
      values.currency = "";
    }
    if (!values.hasOwnProperty("payment_terms")) {
      values.payment_terms = "";
    }
    if (!this.state.checked) {
      values.enable_portal = false;
    } else {
      values.enable_portal = true;
      values.portal_password = this.state.passwordValue;
    }
    if (!values.hasOwnProperty("portal_language")) {
      values.portal_language = "English";
    }
    if (!values.hasOwnProperty("facebook")) {
      values.facebook = "";
    }
    if (!values.hasOwnProperty("skype_name")) {
      values.skype_name = "";
    }
    // address
    if (!values.hasOwnProperty("attention")) {
      values.attention = "";
    }
    if (!values.hasOwnProperty("address1")) {
      values.address1 = "";
    }
    if (!values.hasOwnProperty("address2")) {
      values.address2 = "";
    }
    if (!values.hasOwnProperty("pin_code")) {
      values.pin_code = "";
    }
    if (!values.hasOwnProperty("fax")) {
      values.fax = "";
    }
    if (!values.hasOwnProperty("phone")) {
      values.phone = "";
    }
    // shipping
    if (!values.hasOwnProperty("shipping_attention")) {
      values.shipping_attention = "";
    }
    if (!values.hasOwnProperty("shipping_country")) {
      values.shipping_country = "";
    }
    if (!values.hasOwnProperty("shipping_state")) {
      values.shipping_state = "";
    }
    if (!values.hasOwnProperty("shipping_city")) {
      values.shipping_city = "";
    }
    if (!values.hasOwnProperty("shipping_address1")) {
      values.shipping_address1 = "";
    }
    if (!values.hasOwnProperty("shipping_address2")) {
      values.shipping_address2 = "";
    }
    if (!values.hasOwnProperty("shipping_pin_code")) {
      values.shipping_pin_code = "";
    }
    if (!values.hasOwnProperty("shipping_fax")) {
      values.shipping_fax = "";
    }
    if (!values.hasOwnProperty("shipping_phone")) {
      values.shipping_phone = "";
    }
    // remarks
    if (!values.hasOwnProperty("remarks")) {
      values.remarks = "";
    }
    // contact_person
    if (!values.hasOwnProperty("contact_person")) {
      values.contact_person = [];
    }
    this.setState(
      {
        existed_error: "",
        pincodeError: "",
        work_phone_number_error: "",
        mobile_number_error: "",
        website_error: "",
        facebook_error: "",
      },
      () => {
        this.props.addClient(values);
      }
    );
  };

  dateChange = (e) => {
    this.setState({ startingDate: e.target.value });
  };

  componentDidMount() {


    this.props.listClients();
    let slug =
      this.props.match &&
      this.props.match.params &&
      this.props.match.params.project_slug;

    this.props.retrieveProject(slug);

    this.props.listProjectComment(slug);
    this.props.listEmployees("");
    // debugger
    this.props.listTasks({ project: slug });

    // this.props.listTimeLog()

    if (localStorage.getItem("user_type") === "admin") {
      this.props.listMyTasks({
        project: slug,
        assigned: localStorage.getItem("admin_employee_slug"),
      });
    } else {
      this.props.listMyTasks({
        project: slug,
        assigned: localStorage.getItem("profile_slug"),
      });
    }

    document.addEventListener("mousedown", this.handleClick, false);
  }

  componentWillUnmount() {
    document.removeEventListener("mousedown", this.handleClick, false);
  }



  componentDidUpdate(prevProps) {

    // contracts = [];billableResources = [];combined = [];termAttachments = [];

    // debugger
    if (
      prevProps.tasks != this.props.tasks &&
      this.props.tasks &&
      this.props.tasks.status === "list_subtask_success"
    ) {
      this.setState({ subtasks: this.props.tasks.subtasks });
      // this.openNav('view_task',this.state.taskObj)
    }
    if (
      prevProps.tasks != this.props.tasks &&
      this.props.tasks &&
      this.props.tasks.status === "edit_subtask_success"
    ) {
      this.closeNav();
      this.setState({
        taskResponse: false,
        subtaskObj: this.props.tasks.subtask_obj,
      });
      this.openNav(
        "view_subtask",
        this.state.taskObj,
        this.props.tasks.subtask_obj
      );
    }
    if (
      prevProps.tasks != this.props.tasks &&
      this.props.tasks &&
      this.props.tasks.status === 400
    ) {

      let Errors = [],
        ErrorObj = {};
      let errors =
        this.props.tasks &&
        this.props.tasks.error_obj &&
        this.props.tasks.error_obj.data;
      Object.keys(errors).map(
        (item) => (
          Errors.push(errors[item][0]), (ErrorObj[item] = errors[item][0])
        )
      );
      this.setState({ errorMessages: Errors, errors: ErrorObj });
    }
    if (
      prevProps.tasks != this.props.tasks &&
      this.props.tasks &&
      this.props.tasks.status === 403
    ) {
      let Errors = ["Permission Denied"];
      this.setState({ errorMessages: Errors });
    }
    if (
      prevProps.tasks != this.props.tasks &&
      this.props.tasks &&
      this.props.tasks.status === 500
    ) {
      let Errors = ["Internal Server Error"];
      this.setState({ errorMessages: Errors });
    }

    if (
      prevProps.tasks != this.props.tasks &&
      this.props.tasks?.status === "list_timelog_success"
    ) {
      // debugger
      if (
        this.props.tasks?.timelog?.length > 0 &&
        this.props.tasks?.timelog.some((i) => i.end_time === null)
      ) {
        this.setState({ startTime: false, started: true }, () => this.closeEditModal())
      } else {
        this.setState({ startTime: true, started: false }, () => this.closeEditModal());
      }
    }
    if (
      prevProps.tasks != this.props.tasks &&
      this.props.tasks?.status === "listing_subtasklog_success"
    ) {
      this.setState({ subTaskLog: this.props.tasks?.subTaskLog });
    }
    if (
      prevProps.tasks != this.props.tasks &&
      (this.props.tasks?.status === "add_task_comment_success" ||
        "listing_task_comment_success")
    ) {
      this.setState({
        taskComments: this.props.tasks?.taskComments,
        taskComment: "",
      });
    }

    if (
      this.props.tasks != prevProps.tasks &&
      this.props.tasks &&
      this.props.tasks.status === "edit_task_success"
    ) {


      this.closeNav();
      this.setState({
        taskResponse: false,
        taskObj: this.props.tasks.task_obj,
        individualEdit: "",
        expandNav: false,
      });

      !this.state.listEdit &&
        this.openNav("view_task", this.props.tasks.task_obj);
    }

    if (this.props.tasks != prevProps.tasks && this.props.tasks &&
      this.props.tasks.status === ('create_task_success' || 'edit_time_success')) {
      // this.setState({taskResponse:false})

      this.closeNav()
    }

    if (this.props.tasks != prevProps.tasks && this.props.tasks &&
      this.props.tasks.status === 'edit_time_success') {
      // this.setState({taskResponse:false})

      if (this.state.taskType === 'allTask') {
        this.props.listTimeLog({ project: this?.props?.match?.params?.project_slug })
      }
      else {
        this.props.listTimeLog({
          employee: localStorage.getItem('admin_employee_slug'),
          project: this?.props?.match?.params?.project_slug
        })
      }

      this.closeEditModal()
    }

    


    if (this.props.projectData != prevProps.projectData && this.props.projectData && this.props.projectData.status === "retrive_success") {


      projectObj = this.props.projectData.projectObj;


      billableResources = []
      billableResources =
        projectObj &&
        projectObj.financial_info &&
        projectObj.financial_info.resources &&
        projectObj.financial_info.resources.filter(
          (i) => i.resource_billing_type === "billable"
        );

      contracts = []
      contracts =
        projectObj &&
        projectObj.financial_info &&
        projectObj.financial_info.contract_attachments;

      termAttachments = []
      termAttachments =
        projectObj &&
        projectObj.financial_info &&
        projectObj.financial_info.term_attachments;

      // if(Array.isArray(billableResources)&& billableResources.length>0){
      //    combined=billableResources.map((i,index)=>Object.assign({},i,contracts))
      //    }

      combined = []



      if (contracts && contracts.length > 0) {

        for (let bilres in billableResources) {
          for (let con in contracts) {

            if (
              contracts[con].resource ===
              (billableResources[bilres] &&
                billableResources[bilres].resource_assigned &&
                billableResources[bilres].resource_assigned.slug) &&
              contracts
            ) {
              combined.push(
                { ...billableResources[bilres], ...contracts[con] }
              );
            }
          }
        }
      }

      if (termAttachments && termAttachments.length > 0) {
        for (let bilres in combined) {
          for (let term in termAttachments) {
            if (
              termAttachments[term].resource ===
              (combined[bilres] && combined[bilres].resource)
            ) {
              Object.assign(combined[bilres], termAttachments[term]);
            }
          }
        }
      }

      this.setState({ projectObj: projectObj })
    }

  }

  submit = (values) => {
    console.log(values);
  };

  addComment = (type) => {
    // this.props.addProjectComment(,this.props.match&&this.props.match.params&&this.props.match.params.project_slug)
    if (
      this.state.comments != null &&
      this.state.commentError == null &&
      this.state.comments.trim() != ""
    ) {
      let values = {};
      values.detail_text = this.state.comments;
      const form_data = new FormData();
      for (var key in values) {
        form_data.append(key, values[key]);
      }
      this.setState({ comments: "", commentError: null });
      //   document.getElementById('comment-id').value=''
      this.props.addProjectComment(
        form_data,
        this.props.match &&
        this.props.match.params &&
        this.props.match.params.project_slug
      );
    } else {
      this.setState({ commentError: "Cannot post empty comments" });
    }
  };

  handleComments = (e) => {
    this.setState({ comments: e.target.value, commentError: null });
  };

  convertToLocalTime = (obj) => {
    let dateTime = new Date(obj);
    dateTime.setHours(dateTime.getHours() + 5);
    dateTime.setMinutes(dateTime.getMinutes() + 30);

    return Moment(dateTime).format("DD-MM-YYYY HH:mm:ss");
  };

  convertToLocalTimeFormat = (obj) => {





    let dateTime = new Date(obj);
    // dateTime.setHours(dateTime.getHours() + 5);
    // dateTime.setMinutes(dateTime.getMinutes() + 30);




    return Moment(dateTime).format("h:mm:ss A");
  };

  convertToDateFormat = (obj) => {
    let dateTime = new Date(obj);
    // dateTime.setHours(dateTime.getHours() + 5);
    // dateTime.setMinutes(dateTime.getMinutes() + 30);

    return Moment(dateTime).format("DD-MM-YYYY");
  };

  parseLog = (obj, type, employeeList) => {
    let data = JSON.parse(obj.replace(/\'/gi, '"'));
    if (type === "field") {
      taskFields =
        data &&
        data.map((i) =>
          i.field === "task_board" ? "status" : i.field.replace(/\_/g, " ")
        );
    }

    if (type === "old") {
      OldTasks =
        data &&
        data.map(function (i) {
          if (i.field === "assigned" && i.old != "") {
            return employeeList.filter((emp) => emp.id === i.old)[0].label;
          }
          if (i.field === "due_date" && i.old != "") {
            return Moment(i.old).format("DD-MM-YYYY");
          } else {
            return i.old.replace(/\_/g, " ");
          }
        });
    }
    if (type === "new") {
      NewTasks =
        data &&
        data.map(function (i) {
          if (i.field === "assigned" && i.new != "") {
            return employeeList.filter((emp) => emp.id === i.new)[0].label;
          }
          if (i.field === "due_date" && i.new != "") {
            return Moment(i.new).format("DD-MM-YYYY");
          } else {
            return i.new.replace(/\_/g, " ");
          }
        });
    }
  };

  submitTask = () => {
    this.validateAddTask();
  };

  validateAddTask = () => {
    let valid = true;

    let task_name_error, task_description_error, assigned_error, due_date_error;
    if (!this.state.task_name) {
      task_name_error = "Name is required";
      valid = false;
    }
    if (!this.state.task_description) {
      task_description_error = "Description is required";
      valid = false;
    }
    // if(!this.state.due_date){
    //    due_date_error='Due date is required'
    //    valid=false
    // }
    // if(!this.state.assigned){
    //    assigned_error='Assignee is required'
    //    valid=false
    // }
    if (valid) {
      let data = {};

      if (this.state.type === "add_task") {
        data.assigned = this.state.assigned;
        data.task_name = this.state.task_name;
        data.due_date = this.state.due_date;
        data.task_description = this.state.task_description;
        data.project =
          this.props.match &&
          this.props.match.params &&
          this.props.match.params.project_slug;

        const form_data = new FormData();
        for (var key in data) {
          form_data.append(key, data[key]);
        }

        this.props.addTask(form_data);
      }
      if (this.state.type === "add_subtask") {
        data.assigned = this.state.assigned;
        data.sub_task_name = this.state.task_name;
        data.due_date = this.state.due_date;
        data.task_description = this.state.task_description;
        data.task = this.state.taskObj && this.state.taskObj.slug;

        const form_data = new FormData();
        for (var key in data) {
          form_data.append(key, data[key]);
        }

        this.props.addSubtask(form_data);
      }
      if (this.state.type === "edit_task") {
        data.assigned = this.state.assigned ? this.state.assigned : "";
        data.task_name = this.state.task_name;

        data.due_date = this.state.due_date ? this.state.due_date : "";
        data.task_description = this.state.task_description;
        data.task_board = this.state.task_board;

        const form_data = new FormData();
        for (var key in data) {
          form_data.append(key, data[key]);
        }

        this.props.editTask(
          form_data,
          this.state.taskObj && this.state.taskObj.slug
        );
      }
      if (this.state.type === "edit_subtask") {
        data.assigned = this.state.assigned ? this.state.assigned : "";
        data.sub_task_name = this.state.task_name;

        data.due_date = this.state.due_date ? this.state.due_date : "";
        data.task_description = this.state.task_description;
        data.sub_task_board = this.state.task_board;

        const form_data = new FormData();
        for (var key in data) {
          form_data.append(key, data[key]);
        }

        this.props.editSubTask(
          form_data,
          this.state.subtaskObj && this.state.subtaskObj.slug
        );
      }
    } else {
      let errors = { ...this.state.errors };
      errors.task_name = task_name_error;
      errors.due_date = due_date_error;
      errors.assigned = assigned_error;
      errors.task_description = task_description_error;
      this.setState({ errors: errors });
    }
  };

  handleAssigneeChange = (e) => {
    let errors = { ...this.state.errors };
    errors.assigned = "";
    this.setState({ assigned: e.value, errors: errors });
  };

  handleStatusChange = (e) => {
    let errors = { ...this.state.errors };
    errors.task_board = "";
    this.setState({ task_board: e.value, errors: errors });
  };

  handleChange = (e) => {
    let errors = { ...this.state.errors };
    errors[e.target.name] = "";
    this.setState({ [e.target.name]: e.target.value, errors: errors });
  };

  validateIndividaual = () => {
    // debugger
    let valid = true,
      task_name_error,
      task_description_error;

    if (!this.state.taskObj.task_name) {
      task_name_error = "Name is required";
      valid = false;
    }
    if (!this.state.taskObj.task_description) {
      task_description_error = "Description is required";
      valid = false;
    }
    this.setState((prevState) => ({
      errors: {
        ...prevState.errors,
        task_name: task_name_error,
        task_description: task_description_error,
      },
    }));
    return valid;
  };

  handleClick = (e) => {
    
    // this.node=this.ActiveRef
    
    

    if (!(this.node && this.node.contains(e.target)) && this.state.individualEdit != '' &&
      this.state.individualEdit != 'task_board' && this.state.individualEdit != 'assigned'
      && (this.state.taskObj[this.state.individualEdit] != null)
    ) {
      let validated = this.validateIndividaual();

      if (validated) {
        let individualEdit = this.state.individualEdit;
        const form_data = new FormData();
        form_data.append(individualEdit, this.state.taskObj[individualEdit]);

        this.props.editTask(
          form_data,
          this.state.taskObj && this.state.taskObj.slug
        );
      }
    }

    console.log('this.node',this.ActiveRef)
    console.log('this.node e.target',e.target);
    console.log('this.node.contains(e.target)',this?.node?.contains(e.target));
    
    
    
    if (this.state.taskObj?.due_date === null) {

      if (this.node){
        if(this.node.contains(e.target)==false){
          console.log('tellll');
          
          this.setState({ individualEdit: '', taskIndex: '' })
        }
      }
      
    }

    if (
      this.state.taskObj?.assigned === null) {
      if (!(this.node && this.node.contains(e.target))) {
        console.log('tellll22222');
        this.setState({ individualEdit: "", taskIndex: "" });
      }
    };

  };
  handleIndividualChange = (e) => {
    let taskObj = { ...this.state.taskObj };
    // this.setState((prevState=>({taskObj:{...prevState.taskObj,task_name:e.target.value}})))
    taskObj[e.target.name] = e.target.value;
    // debugger
    this.setState({ taskObj: taskObj });
  };
  handleIndividualStatusOrAssignedChange = (e) => {
    let individualEdit = this.state.individualEdit;
    const form_data = new FormData();
    form_data.append(individualEdit, e.value);

    this.props.editTask(
      form_data,
      this.state.taskObj && this.state.taskObj.slug
    );
  };
  formatString = (str) => {
    // debugger
    if (str != "") {
      let str1 = str.replace(/_/g, "  ");
      str1 = str1.charAt(0).toUpperCase() + str1.substr(1).toLowerCase();
      return str1;
    }
  };
  addTime = () => {
    var isClicked = !this.state.isClicked;
    this.setState({
      isClicked: isClicked,
      add_end_date_error: "",
      add_end_time_error: "",
      add_start_date_error: "",
      add_start_time_error: "",
      add_end_date: "",
      add_end_time: "",
      add_start_date: "",
      add_start_time: "",
      duration_error: "",
    });
  };

  addTimeSubmit = (e) => {
    let add_end_date_error = "",
      add_end_time_error = "",
      add_start_date_error = "",
      add_start_time_error = "",
      valid = true,
      duration_error = "";
    let errors = { ...this.state.errors };

    if (!this.state.add_start_date) {
      add_start_date_error = "Required";
      valid = false;
    }
    // if(!this.state.add_start_time){
    //    add_start_time_error='Required'
    //    valid=false
    // }
    if (!this.state.add_end_date) {
      add_end_date_error = "Required";
      valid = false;
    }
    // if(!this.state.add_end_time){
    //    add_end_time_error='Required'
    //    valid=false
    // }
    if (this.state.taskObj?.assigned === null) {
      valid = false;
      errors.assigned = "Please Select a Assignee ";
    }
    if (!this.state.seconds || !this.state.minutes || !this.state.hours) {
      valid = false;
      duration_error = "Invalid Time format";
    }

    this.setState({ [e.target.name]: e.target.value });
  }
  handleTaskComments = (e) => {
    this.setState({ taskComment: e.target.value, taskCommentError: "" });
  };

  addTaskComment = (e) => {
    if (
      this.state.taskComment != "" &&
      this.state.taskCommentError == "" &&
      this.state.taskComment.trim() != ""
    ) {
      this.props.addTaskComment({
        task: this.state.taskObj?.slug,
        comment: this.state.taskComment,
      });
    } else {
      this.setState({ taskCommentError: "Cannot post empty comments" });
    }
  };

  setStartTimerTask = (e) => {
    this.setState({ started: e }, this.stat)
  }

  render() {

    console.log('projectObj', projectObj)




    let permissions = [];
    permissions = localStorage.getItem("permissions");
    if (permissions && permissions.length > 0) {
      permissions = permissions.split(",");
    }
    let is_admin = localStorage.getItem("user_type") === "admin";



    if (
      this.state.taskResponse &&
      this.props.tasks &&
      this.props.tasks.status === "create_subtask_success"
    ) {
      // debugger
      this.closeNav();
      this.setState({
        taskResponse: false,
        subtaskObj: this.props.tasks.subtask_obj,
      });
      this.openNav(
        "view_subtask",
        this.state.taskObj,
        this.props.tasks.subtask_obj
      );
    }

    if (
      this.props.projectData &&
      this.props.projectData.status === "delete_success"
    ) {
      window.location.reload();
    }

    let comments =
      this.props.projectData && this.props.projectData.projectComments;

    const typeList = [
      { label: "Hourly Type", value: "hourly" },
      { label: "Fixed Type", value: "fixed" },
    ];

    const statusList = [
      { label: "ToDo", value: "todo" },
      { label: "Ongoing", value: "ongoing" },
      { label: "Done", value: "done" },
      { label: "On Hold", value: "on_hold" },
      { label: "Moved To Testing", value: "moved_to_testing" },
      { label: "Cancelled", value: "cancelled" },
    ];

    const resources = [
      { label: "Business Team", value: 1 },
      { label: "Admin", value: 2 },
      { label: "Project Manager", value: 3 },
    ];
    var clientList = [];

    this.props.clientData &&
      this.props.clientData.clients &&
      this.props.clientData.clients.map((obj) =>
        clientList.push({
          name: "Client",
          value: obj.client.slug,
          label: obj.client.first_name + " " + obj.client.last_name,
        })
      );

    const userList = [
      { label: "user 1", value: 1, name: "Abc" },
      { label: "user 2", value: 2, name: "Mathu" },
      { label: "user 3", value: 3, name: "John" },
    ];
    const clientType = [
      {
        text: "Internal Project",
        value: "internal",
        name: "client_type",
        id: "id_client_type_0",
      },
      {
        text: "Client Project",
        value: "client",
        name: "client_type",
        id: "id_client_type_1",
      },
    ];
    const { handleSubmit } = this.props;
    const Currency = Helper.Currency;



    let employeeList = [];

    if (
      this.state.projectObj &&
      this.state.projectObj.financial_info &&
      this.state.projectObj.financial_info.resources &&
      this.state.projectObj.financial_info.resources &&
      employeeList &&
      employeeList.length == 0
    ) {
      this.state.projectObj &&
        this.state.projectObj.financial_info &&
        this.state.projectObj.financial_info.resources &&
        this.state.projectObj.financial_info.resources.map((res) =>
          employeeList.push({
            name: "user",
            value: res.resource_assigned.slug,
            label:
              res.resource_assigned.first_name +
              " " +
              res.resource_assigned.last_name,
            id: res.resource_assigned.id,
          })
        );

      // this.setState({employeeList:employeeList})
    }
    return (
      <Fragment>
        {(this.props.projectData?.isLoading) ? (
          <Fragment>
            <div className="loader-img">
              {" "}
              <img src={LoaderImage} style={loaderStyle} />
            </div>
          </Fragment>
        ) : (
            <div className="col-sm-12">
              <form
                className="row"
                id="create-project-form"
                onSubmit={handleSubmit(this.submit)}
                enctype="multipart/form-data"
              >
                <ul className="nav nav-tabs" id="myTab" role="tablist">
                  <li className="nav-item" style={{ padding: "0px" }}>
                    <a
                      className="nav-link "
                      style={{ padding: "16px" }}
                      id="task-list"
                      data-toggle="tab"
                      href="#tasklist"
                      role="tab"
                      aria-controls="tasklist"
                      aria-selected="true"
                    >
                      Task List
                  </a>
                  </li>
                  <li className="nav-item" style={{ padding: "0px" }}>
                    <a
                      className="nav-link active"
                      style={{ padding: "16px" }}
                      id="generalinfo-tab"
                      data-toggle="tab"
                      href="#generalinfo"
                      role="tab"
                      aria-controls="generalinfo"
                      aria-selected="true"
                    >
                      General Info
                  </a>
                  </li>



                  {(is_admin ||
                    (permissions &&
                      permissions.includes("view_financialinfo"))) &&
                    this.state.projectObj?.project?.project_type === "client_project" &&
                    (<Fragment>

                      <li className="nav-item" style={{ padding: "0px" }}>
                        <a
                          className="nav-link"
                          style={{ padding: "16px" }}
                          id="financial-info"
                          data-toggle="tab"
                          href="#financialinfo"
                          role="tab"
                          aria-controls="finantialinfo"
                          aria-selected="true"
                        >
                          Financial Info
                        </a>
                      </li>

                    </Fragment>
                    )}

                  <li className="nav-item" style={{ padding: "0px" }}>
                    <a
                      className="nav-link"
                      style={{ padding: "16px" }}
                      id="client-communication-tab"
                      data-toggle="tab"
                      href="#clientcommunication"
                      role="tab"
                      aria-controls="clientcommunication"
                      aria-selected="true"
                    >
                      Client Board
                  </a>
                  </li>
                </ul>
                <input
                  type="hidden"
                  name="csrfmiddlewaretoken"
                  value="g1PZ3qSfcIO0rwTa5gGkuhj9860Kmbk8cf9vCDX14Z9PDDgX0nttJfe25aIObXJ8"
                />

                <div className="tab-content col-sm-12" id="myTabContent">
                  <div
                    className="tab-pane userMail-tab fade show active"
                    id="generalinfo"
                    role="tabpanel"
                    aria-labelledby="generalinfo-tab"
                  >
                    <div className="row team-content">
                      <div
                        className="col-sm-12 form-group "
                        style={{ marginTop: "15px" }}
                      >
                        <li>
                          {/* <button onClick= {()=>this.addTaskModal("project")} className="btn btn-link-secondary " id="edit-project" >
                                                <i className="icon-pencil" style={{fontSize:"16px"}}></i>
                                             </button> */}

                          {(is_admin ||
                            (permissions &&
                              permissions.includes("change_project"))) && (
                              <Link onClick={() => this.addTaskModal("project")}>
                                <i
                                  className="btn btn-link-secondary"
                                  id="edit-project"
                                  className="icon-pencil"
                                  style={{ fontSize: "16px" }}
                                ></i>
                              </Link>
                            )}

                          <table className="table" style={{ color: "#9C9C9C" }}>
                            <thead>
                              <tr>
                                <th>
                                  <h6 style={{ color: "#9C9C9C" }}>
                                    Project Name
                                </h6>
                                </th>
                                <th>
                                  {this.state.projectObj &&
                                    this.state.projectObj.project &&
                                    this.state.projectObj.project.project_name}
                                </th>
                                {/* <th><Link onClick= {()=>this.addTaskModal("project")} ><i className="icon-pencil" style={{fontSize:"16px"}}></i></Link></th> */}
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td>
                                  <h6 style={{ color: "#9C9C9C" }}>
                                    Project Description
                                </h6>
                                </td>
                                <td>
                                  {" "}
                                  {this.state.projectObj &&
                                    this.state.projectObj.project &&
                                    this.state.projectObj.project.project_description}
                                </td>
                              </tr>
                              <tr>
                                <td>
                                  <h6 style={{ color: "#9C9C9C" }}>Start Date</h6>
                                </td>
                                <td>
                                  {" "}
                                  {Moment(this.state.projectObj &&
                                    this.state.projectObj.project &&
                                    this.state.projectObj.project.start_date).format("DD-MM-YYYY")}
                                </td>
                              </tr>
                              <tr>
                                <td>
                                  <h6 style={{ color: "#9C9C9C" }}>End Date</h6>
                                </td>
                                <td>
                                  {" "}
                                  {this.state.projectObj &&
                                    this.state.projectObj.project &&
                                    this.state.projectObj.project.end_date}
                                </td>
                              </tr>
                              {this.state.projectObj &&
                                this.state.projectObj.project &&
                                this.state.projectObj.project.project_type ===
                                "client_project" && (
                                  <Fragment>
                                    {this.state.projectObj &&
                                      this.state.projectObj.project &&
                                      this.state.projectObj.project.billing_type ===
                                      "fixed" ? (
                                        <tr>
                                          <td>
                                            <h6 style={{ color: "#9C9C9C" }}>
                                              Project Cost
                                        </h6>
                                          </td>
                                          <td>
                                            {" "}
                                            {this.state.projectObj &&
                                              this.state.projectObj.project &&
                                              this.state.projectObj.project.project_cost}
                                          </td>
                                        </tr>
                                      ) : (
                                        <tr>
                                          <td>
                                            <h6 style={{ color: "#9C9C9C" }}>
                                              Estimated Hours
                                        </h6>
                                          </td>
                                          <td>
                                            {" "}
                                            {this.state.projectObj &&
                                              this.state.projectObj.project &&
                                              this.state.projectObj.project.estimated_hours
                                              ? this.state.projectObj.project.estimated_hours
                                              : "None"}
                                          </td>
                                        </tr>
                                      )}
                                  </Fragment>
                                )}

                              <tr>
                                <td>
                                  <h6 style={{ color: "#9C9C9C" }}>
                                    Project Type
                                </h6>
                                </td>
                                <td>
                                  {" "}
                                  {this.state.projectObj &&
                                    this.state.projectObj.project &&
                                    this.state.projectObj.project.project_type ===
                                    "internal_project"
                                    ? "Internal Project"
                                    : "Client Project"}
                                </td>
                              </tr>
                              {this.state.projectObj &&
                                this.state.projectObj.project &&
                                this.state.projectObj.project.project_type !=
                                "internal_project" && (
                                  <Fragment>
                                    <tr>
                                      <td>
                                        <h6 style={{ color: "#9C9C9C" }}>
                                          Client
                                      </h6>
                                      </td>
                                      <td>
                                        {" "}
                                        {this.state.projectObj &&
                                          this.state.projectObj.project &&
                                          this.state.projectObj.project.client}
                                      </td>
                                    </tr>

                                    <tr>
                                      <td>
                                        <h6 style={{ color: "#9C9C9C" }}>
                                          Billing Type
                                      </h6>
                                      </td>
                                      <td>
                                        {" "}
                                        {this.state.projectObj &&
                                          this.state.projectObj.project &&
                                          this.state.projectObj.project.billing_type}
                                      </td>
                                    </tr>
                                  </Fragment>
                                )}
                              <tr>
                                <td>
                                  <h6 style={{ color: "#9C9C9C" }}>Owner</h6>
                                </td>
                                <td>
                                  {" "}
                                  {this.state.projectObj &&
                                    this.state.projectObj.owner &&
                                    this.state.projectObj.owner.name}
                                </td>

                              </tr>

                              <tr>
                                <td>
                                  <h6 style={{ color: "#9C9C9C" }}>Status</h6>
                                </td>
                                <td>
                                  {" "}
                                  {this.state.projectObj &&
                                    this.state.projectObj.project &&
                                    this.state.projectObj.project.project_status &&
                                    this.state.projectObj.project.project_status === "created"
                                    ? "active"
                                    : this.state.projectObj &&
                                    this.state.projectObj.project &&
                                    this.state.projectObj.project.project_status}
                                </td>
                              </tr>

                              <tr>
                                <td>
                                  <h6 style={{ color: "#9C9C9C" }}>
                                    Attachments
                                </h6>
                                </td>
                                <td>
                                  {this.state.projectObj &&
                                    this.state.projectObj.project_attachements &&
                                    this.state.projectObj.project_attachements.map((obj) => (
                                      <div className="row">
                                        <div className="col-sm-10 ">
                                          <a href="#">{obj.attachment}</a>
                                        </div>
                                        {is_admin ||
                                          (permissions &&
                                            permissions.includes(
                                              "change_project"
                                            ) && (
                                              <div className="col-sm-2 ">
                                                {" "}
                                                <i
                                                  className="btn btn-link-secondary"
                                                  onClick={() =>
                                                    this.props.deleteProjectAttachment(
                                                      obj.id
                                                    )
                                                  }
                                                  className="icon-trash"
                                                  style={{
                                                    fontSize: "16px",
                                                    color: "#9C9C9C",
                                                  }}
                                                ></i>
                                              </div>
                                            ))}
                                        {/* <Link onClick= {()=>this.addTaskModal("attachment-edit",obj.attachment)} ><i className="btn btn-link-secondary"  className="icon-pencil" style={{fontSize:"16px",color:"#9C9C9C"}}></i></Link> */}
                                        {/* <button className="btn btn-outline-primary delete-row" style={{ fontSize:"13px"}} 
                                    // onClick={() => this.props.deleteResource(obj.resource_id)}
                                     href="javascript:void(0)" ><i class=" icon-delete"></i> Remove</button> */}
                                      </div>
                                    ))}
                                </td>
                                {/* <td> 
                        {this.state.projectObj&&this.state.projectObj.project_attachements&&this.state.projectObj.project_attachements.map(obj=>
                       <li> <Link onClick= {()=>this.addTaskModal("attachment-edit",obj.attachment)} ><i className="btn btn-link-secondary"  className="icon-pencil" style={{fontSize:"16px",color:"#9C9C9C"}}></i></Link></li>
                        )}
                        </td>
                        <td>
                        {this.state.projectObj&&this.state.projectObj.project_attachements&&this.state.projectObj.project_attachements.map(obj=>
                                <li> <button className="btn btn-outline-primary delete-row" style={{ fontSize:"13px"}} 
                                    // onClick={() => this.props.deleteResource(obj.resource_id)}
                                     href="javascript:void(0)" ><i class=" icon-delete"></i> Remove</button></li>
                                     )}
                                     </td> */}
                              </tr>

                              {(is_admin ||
                                (permissions &&
                                  permissions.includes("change_project"))) && (
                                  <div className="col-sm-12 ">
                                    <button
                                      style={{ width: "131px" }}
                                      onClick={() =>
                                        this.addTaskModal("attachment-add")
                                      }
                                      type="button"
                                      className="btn btn-outline-primary add-row"
                                    >
                                      <i className="icon-plus"></i> Add Attachment
                                </button>
                                  </div>
                                )}


                            </tbody>
                          </table>
                          <table className="table" style={{ color: "#9C9C9C" }}>
                            <thead>
                              <tr>
                                <th>
                                  <h6 style={{ color: "#9C9C9C" }}>Team</h6>
                                </th>
                                <th></th>
                              </tr>
                            </thead>
                            {this.state.projectObj &&
                              this.state.projectObj.financial_info &&
                              this.state.projectObj.financial_info.resources.length > 0 &&
                              <tbody>
                                <tr>
                                  <td>
                                    <h6 style={{ color: "#9C9C9C" }}>Name</h6>
                                  </td>
                                  <td>
                                    <h6 style={{ color: "#9C9C9C" }}>Role</h6>
                                  </td>
                                  <td>
                                    <h6 style={{ color: "#9C9C9C" }}>Billable</h6>
                                  </td>
                                  <td>
                                    <h6 style={{ color: "#9C9C9C" }}>Tracked</h6>
                                  </td>
                                </tr>

                                {this.state.projectObj &&
                                  this.state.projectObj.financial_info &&
                                  this.state.projectObj.financial_info.resources &&
                                  this.state.projectObj.financial_info.resources.map((obj) => (
                                    <tr>
                                      <td>
                                        {obj &&
                                          obj.resource_assigned &&
                                          obj.resource_assigned.first_name}{" "}
                                        {obj &&
                                          obj.resource_assigned &&
                                          obj.resource_assigned.last_name}
                                      </td>
                                      <td>
                                        {obj &&
                                          obj.resource_assigned &&
                                          obj.resource_assigned.designation &&
                                          obj.resource_assigned.designation
                                            .designation_name}
                                      </td>
                                      <td>
                                        {obj.resource_billing_type === "billable"
                                          ? "Yes"
                                          : "No"}
                                      </td>
                                      <td>{obj.tracker ? "Yes" : "No"}</td>

                                      {(is_admin ||
                                        (permissions &&
                                          permissions.includes(
                                            "change_project"
                                          ))) && (
                                          <td>
                                            <Link
                                              onClick={() =>
                                                this.addTaskModal(
                                                  "team",
                                                  obj.resource_id
                                                )
                                              }
                                            >
                                              <i
                                                className="btn btn-link-secondary"
                                                className="icon-pencil"
                                                style={{
                                                  fontSize: "16px",
                                                  color: "#9C9C9C",
                                                }}
                                              ></i>
                                            </Link>
                                          </td>
                                        )}

                                      {(is_admin ||
                                        (permissions &&
                                          permissions.includes(
                                            "change_project"
                                          ))) && (
                                          <Fragment>
                                            {obj.resource_billing_type ===
                                              "non_billable" ? (
                                                <td>
                                                  <button
                                                    className="btn btn-outline-primary delete-row"
                                                    style={{ fontSize: "13px" }}
                                                    onClick={() =>
                                                      this.props.deleteResource(
                                                        obj.resource_id
                                                      )
                                                    }
                                                    href="javascript:void(0)"
                                                  >
                                                    <i class=" icon-delete"></i> Remove
                                          </button>
                                                </td>
                                              ) : null}
                                          </Fragment>
                                        )}
                                    </tr>
                                  ))}
                              </tbody>}
                            {(is_admin ||
                              (permissions &&
                                permissions.includes("change_project"))) && (
                                <div className="col-sm-12 " style={{ marginTop: "20px" }}>
                                  <button
                                    style={{ width: "130px" }}
                                    onClick={() => this.addTaskModal("team-add")}
                                    type="button"
                                    className="btn btn-outline-primary add-row"
                                  >
                                    <i className="icon-plus"></i> Add Resource
                                </button>
                                </div>
                              )}
                          </table>
                        </li>
                      </div>
                    </div>
                  </div>

                  <div
                    className="tab-pane row fade "
                    id="tasklist"
                    role="tabpanel"
                    aria-labelledby="task-list"
                  >
                    {this.state.errorMessages &&
                      this.state.errorMessages.length > 0 &&
                      this.state.errorMessages.map((i) => (
                        <ErrorMessage msg={i} />
                      ))}

                    <div
                      id="mySidenav"
                      class="sidenav"
                      style={
                        this.state.openNav
                        ? { width: this.state.expandNav ? "100%" : "50%",overflowX: this.state.expandNav ? "hidden" : "auto" }
                          : { width: "0%" }
                      }
                    >
                      {this.props.tasks && this.props.tasks.isLoading ? (
                        <Fragment>
                          <div className="loader-img">
                            {" "}
                            <img src={LoaderImage} style={loaderStyle} />
                          </div>
                        </Fragment>
                      ) : (
                          <Fragment>
                            <div style={{ marginBottom: "50px" }}>
                              <button
                                style={{
                                  position: "absolute",
                                  fontSize: "14px",
                                  width: "110px",
                                }}
                                onClick={this.expandNav}
                                type="button"
                                className="btn btn-outline-primary add-row"
                              >
                                <i
                                  className={
                                    !this.state.expandNav
                                      ? "fa fa-expand"
                                      : "fa fa-compress"
                                  }
                                  style={{ marginRight: "5px" }}
                                  aria-hidden="true"
                                ></i>
                                {!this.state.expandNav ? "Expand" : "Close"}
                              </button>

                              {/* <button className="btn btn-link-secondary show-form-modal" onClick={this.expandNav} style={{position:"absolute",top:"6px",fontSize:"14px"}} >
  <i className={!this.state.expandNav?"fa fa-expand":"fa fa-compress"} aria-hidden="true"></i>{!this.state.expandNav?"Open":"Close"}
                                       </button> */}
                              <button
                                className="btn btn-link-secondary show-form-modal"
                                style={{
                                  position: "absolute",
                                  fontSize: "14px",
                                  right: "0px",
                                }}
                                onClick={this.closeNav}
                              >
                                <i className="fa fa-times" aria-hidden="true"></i>
                              </button>
                            </div>
                            {/* <a  href="#" class="closebtn" onClick={this.closeNav}>&times;</a></div> */}
                            {/* <span className="text-grey" style={{fontSize:"17px"}}>Write a Task Name</span> */}

                            {(this.state.type === "time_log") && (
                              <Fragment>
                                {!this.state.expandNav && this.state.taskType === 'myTask' &&
                                  <div className="row" style={{ marginTop: "10px" }}>
                                    <div class="col-sm-7">
                                      <div class="input-color">
                                        <div class="color-box" style={{ backgroundColor: "green" }}></div><span style={{ marginLeft: "20px", marginRight: "20px" }}> Auto-tracked</span>
                                        <div class="color-box" style={{ backgroundColor: "#cece0e" }}></div><span style={{ marginLeft: "20px", marginRight: "20px" }}>Manual Time</span>
                                      </div></div>
                                  </div>}
                                <div className="row" style={{ marginTop: "10px" }}>
                                  {this.state.taskType === 'allTask' && this.state.openNav &&
                                    <div class="col-sm-7">
                                      <div class="input-color">
                                        <div class="color-box" style={{ backgroundColor: "green" }}></div><span style={{ marginLeft: "20px", marginRight: "20px" }}> Auto-tracked</span>
                                        <div class="color-box" style={{ backgroundColor: "#cece0e" }}></div><span style={{ marginLeft: "20px", marginRight: "20px" }}>Manual Time</span>
                                      </div></div>}
                                  {this.state.taskType === 'myTask' && this.state.expandNav &&
                                    <div class="col-sm-7">
                                      <div class="input-color">
                                        <div class="color-box" style={{ backgroundColor: "green" }}></div><span style={{ marginLeft: "20px", marginRight: "20px" }}> Auto-tracked</span>
                                        <div class="color-box" style={{ backgroundColor: "#cece0e" }}></div><span style={{ marginLeft: "20px", marginRight: "20px" }}>Manual Time</span>
                                      </div></div>}

                                  <div
                                    className={!this.state.expandNav && this.state.taskType === 'myTask' ? "col-sm-12 form-group " : "col-sm-5 form-group "}
                                    style={{ textAlign: "end" }}
                                  >
                                    {this.state.taskType === 'myTask' && <button
                                      style={{
                                        width: "110px",
                                        marginRight: "10px",
                                      }}
                                      onClick={() => this.addTaskModal("StartEndTimer", this.state.started ? 'end' : 'start')}
                                      type="button"
                                      className={
                                        this.state.started
                                          ? "btn btn-outline-danger add-row"
                                          : "btn btn-outline-success add-row"
                                      }
                                    >
                                      <i
                                        style={{ marginRight: "5px" }}
                                        className="fa fa-clock-o"
                                        aria-hidden="true"
                                      ></i>
                                      {this.state.started
                                        ? "Stop Timer"
                                        : "Start Timer"}
                                    </button>}

                                    <button
                                      style={{ width: "140px" }}
                                      onClick={() => this.addTaskModal("log_time")}
                                      type="button"
                                      className="btn btn-outline-primary add-row"
                                    >
                                      <i className="icon-plus"></i> Log Manual Time
                                </button>
                                  </div>
                                </div>
                                {/* Task Listing */}
                                {this.props.tasks?.timelog?.length > 0 ?
                                  <Fragment>

                                    <table style={{ tableLayout: "auto" }} id="dtHorizontalExample" class={this.state.expandNav ? " table-scroll table table-cs text-grey table-text-sm table-striped table-borderless" : "table table-scroll table-striped table-borderless table-sm"} cellspacing="0"
                                      width="100%">
                                      <thead>
                                        <tr>
                                          <th>Task</th>
                                          <th>Assignee</th>
                                          <th>Date</th>
                                          <th>Start</th>
                                          <th>End</th>
                                          <th>Worked hours</th>
                                          <th>Productive hours</th>
                                          <th>Billable</th>
                                          <th>PM Approved</th>
                                        </tr>
                                      </thead>
                                      <tbody className="text-grey">
                                        {this.props.tasks?.timelog?.map(obj =>
                                          <tr className={!obj.tracking ? "manual-time" : "auto-tracked"}>
                                            <td style={{ whiteSpace: "break-spaces" }}>
                                              {this.props.tasks?.tasks.length > 0 && this.props.tasks?.tasks?.filter(i => i.slug === obj.task)[0]?.task_name}

                                            </td>
                                            <td style={{ whiteSpace: "break-spaces" }}>
                                              {this.props.employees?.allEmployees?.response?.filter(i => i.slug === obj.employee)[0].first_name + " " +
                                                this.props.employees?.allEmployees?.response?.filter(i => i.slug === obj.employee)[0].last_name
                                              }
                                            </td>
                                            <td >{this.convertToDateFormat(obj.start_time_format)}</td>
                                            <td >{(obj.start_time_format) && this.convertToLocalTimeFormat(obj.start_time_format)}</td>
                                            <td >{(obj.end_time_format != null) && this.convertToLocalTimeFormat(obj.end_time_format)}</td>
                                            {/* <td><a href=""><i class="icon-close" style={{ fontSize:"16px", color: "rgb(156, 156, 156)"}}></i></a></td> */}
                                            <td style={{ whiteSpace: "break-spaces" }}>{obj.worked_hours}</td>
                                            <td style={{ whiteSpace: "break-spaces" }}>{obj.productive_hours}</td>
                                            <td style={{ whiteSpace: "break-spaces" }}>{obj.hourly ? 'Yes' : 'No'}</td>
                                            <td style={{ whiteSpace: "break-spaces" }}>{obj.pm_approved?obj.pm_approved:'00:00:00'}</td>
                                            <td style={{ whiteSpace: "break-spaces" }} className="td-edit"> {obj.end_time_format != null && <Link onClick={() => this.addTaskModal("edit_log", obj.slug, obj)}><i className="icon-pencil" style={{ fontSize: "16px", color: "rgb(156, 156, 156)" }}></i></Link>}</td>

                                          </tr>
                                        )}
                                      </tbody>
                                    </table>

                                  </Fragment> :

                                  <Fragment>
                                    <div
                                      className="row"
                                      style={{
                                        justifyContent: "center",
                                        marginTop: "30px",
                                      }}
                                    >
                                      <img
                                        style={{ width: "50%" }}
                                        src="//cdn-pjs.teamwork.com/tko/public/assets/blankslates/time.gif"
                                      ></img>
                                    </div>
                                    <div
                                      className="row"
                                      style={{ justifyContent: "center" }}
                                    >
                                      <div className="col-sm-12" style={{ textAlign: "center" }}>
                                        <h3 className="text-grey">No Time Entries Yet</h3>
                                      </div>
                                      <div className="col-sm-12" style={{ textAlign: "center", padding: "0px" }}>
                                        <p className="text-light-grey">
                                          There is no time logged. Add the first time
                                          entry by clicking the button below.
                              </p>
                                      </div>
                                    </div>
                                    <div
                                      className="row"
                                      style={{
                                        justifyContent: "center",
                                        marginTop: "15px",
                                      }}
                                    >
                                      <button
                                        type="button"
                                        className="btn btn-outline-primary add-row"
                                        onClick={() => this.addTaskModal("log_time")}
                                      >

                                        <i className="icon-plus"></i>Add the First Time Log

                              </button>
                                    </div>
                                  </Fragment>}
                                {/* No TASK end */}
                              </Fragment>
                            )}

                            {(this.state.type === "add_task" ||
                              this.state.type === "add_subtask" ||
                              this.state.type === "edit_task" ||
                              this.state.type === "edit_subtask") && (
                                <Fragment>
                                  <div
                                    className="text-grey"
                                    style={{
                                      fontSize: "13px",
                                      paddingBottom: "15px",
                                      paddingTop: "20px",
                                    }}
                                  ></div>

                                  <form
                                    className="row"
                                    id="create-project-form"
                                    onSubmit={handleSubmit(this.submitTask)}
                                    enctype="multipart/form-data"
                                  >
                                    {(this.state.type === "edit_task" ||
                                      this.state.type === "edit_subtask") && (
                                        <div
                                          className="col-sm-12 form-group"
                                          style={{
                                            display: "flex",
                                            flexDirection: "row",
                                            alignItems: "center",
                                          }}
                                        >
                                          <label
                                            className="text-light-grey"
                                            style={{ width: "150px" }}
                                            for="id_end_date"
                                          >
                                            <h5>Edit</h5>
                                          </label>
                                        </div>
                                      )}

                                    <div
                                      className="col-sm-12 form-group"
                                      style={{
                                        display: "flex",
                                        flexDirection: "row",
                                        alignItems: "center",
                                      }}
                                    >
                                      <label
                                        className="text-light-grey"
                                        style={{ width: "150px" }}
                                        for="id_end_date"
                                      >
                                        {this.state.type === "add_subtask" &&
                                          "Sub Task "}
                                        {this.state.type === "add_task" && "Task "}
                                  Name<span className="text-danger">*</span>
                                      </label>
                                      <input
                                        type="text"
                                        name="task_name"
                                        className="half-width input-control"
                                        label="Name"
                                        placeholder="Name"
                                        onChange={(e) => this.handleChange(e)}
                                        value={this.state.task_name}
                                      />
                                    </div>
                                    <div
                                      className="text-danger error-msg"
                                      style={{
                                        width: !this.state.expandNav ? "100%" : "50%",
                                      }}
                                    >
                                      {this.state.errors.task_name}
                                    </div>
                                    <div
                                      className="col-sm-12 form-group"
                                      style={{
                                        display: "flex",
                                        flexDirection: "row",
                                        alignItems: "center",
                                      }}
                                    >
                                      <label
                                        for="id_client"
                                        style={{ width: "150px" }}
                                        className="text-light-grey"
                                      >
                                        Assignee
                                </label>
                                      <Select
                                        options={this.state.taskType === 'myTask' ? employeeList?.filter(i => i.value === localStorage.getItem('profile_slug')) : employeeList}
                                        onChange={(e) => this.handleAssigneeChange(e)}
                                        value={employeeList.filter(
                                          (emp) => emp.value === this.state.assigned
                                        )}
                                      />
                                    </div>
                                    <div className="text-danger error-msg">
                                      {this.state.errors.assigned}
                                    </div>

                                    {(this.state.type === "edit_task" ||
                                      this.state.type === "edit_subtask") && (
                                        <div
                                          className="col-sm-12 form-group"
                                          style={{
                                            display: "flex",
                                            flexDirection: "row",
                                            alignItems: "center",
                                          }}
                                        >
                                          <label
                                            for="id_client"
                                            style={{ width: "150px" }}
                                            className="text-light-grey"
                                          >
                                            Status
                                  </label>
                                          <Select
                                            options={statusList}
                                            onChange={(e) => this.handleStatusChange(e)}
                                            value={statusList.filter(
                                              (emp) =>
                                                emp.value === this.state.task_board
                                            )}
                                          />
                                        </div>
                                      )}

                                    <div
                                      className="col-sm-12 form-group"
                                      style={{
                                        display: "flex",
                                        flexDirection: "row",
                                        alignItems: "center",
                                      }}
                                    >
                                      <label
                                        for="id_client"
                                        style={{ width: "150px" }}
                                        className="text-light-grey"
                                      >
                                        Due date
                                </label>
                                      <input
                                        type="date"
                                        name="due_date"
                                        className="half-width input-control"
                                        label="End Date"
                                        onChange={(e) => this.handleChange(e)}
                                        defaultValue={this.state.due_date}
                                        min={this.state.startingDate}
                                      />
                                    </div>
                                    <div className="text-danger error-msg">
                                      {this.state.errors.due_date}
                                    </div>

                                    {this.state.type === "add_subtask" && (
                                      <Fragment>
                                        <div
                                          className="col-sm-12 form-group"
                                          style={{
                                            fontSize: "13px",
                                            display: "flex",
                                            flexDirection: "row",
                                            alignItems: "center",
                                          }}
                                        >
                                          <label
                                            for="id_client"
                                            style={{ width: "100%" }}
                                            className="text-light-grey"
                                          >
                                            Project:
                                      <span style={{ marginLeft: "7px" }}>
                                              {this.state.projectObj &&
                                                this.state.projectObj.project &&
                                                this.state.projectObj.project.project_name}
                                              <i
                                                class="fa fa-angle-double-right"
                                                style={{
                                                  fontSize: "13px",
                                                  margin: "0 8px",
                                                }}
                                                aria-hidden="true"
                                              ></i>
                                              {this.state.taskObj &&
                                                this.state.taskObj.task_name}
                                            </span>
                                          </label>
                                        </div>
                                      </Fragment>
                                    )}

                                    {this.state.type === "add_task" && (
                                      <div
                                        className="col-sm-12 form-group"
                                        style={{
                                          fontSize: "13px",
                                          display: "flex",
                                          flexDirection: "row",
                                          alignItems: "center",
                                        }}
                                      >
                                        <label
                                          style={{ width: "150px" }}
                                          className="text-light-grey"
                                          for="id_end_date"
                                        >
                                          Project
                                  </label>
                                        <input
                                          type="text"
                                          name="project"
                                          className="half-width input-control"
                                          label="Project"
                                          placeholder="Project"
                                          // onChange={(e) => this.weeklyHourlyonChange(e)}
                                          value={
                                            this.state.projectObj &&
                                            this.state.projectObj.project &&
                                            this.state.projectObj.project.project_name
                                          }
                                        />
                                      </div>
                                    )}

                                    <div className="text-danger error-msg"></div>
                                    <div
                                      className="col-sm-12 form-group"
                                      style={{
                                        display: "flex",
                                        flexDirection: "row",
                                      }}
                                    >
                                      <label
                                        style={{ width: "150px" }}
                                        for="id_project_description"
                                        className="text-light-grey"
                                      >
                                        Description
                                  <span className="text-danger">*</span>
                                      </label>
                                      {/* <ReactQuill onChange={(e) => this.taskdescription(e)}>
                          <div className="my-editing-area" />
                        </ReactQuill> */}

                                      <textarea
                                        name="task_description"
                                        cols="40"
                                        rows="10"
                                        onChange={(e) => this.handleChange(e)}
                                        value={this.state.task_description}
                                        className="input-control half-width"
                                        label="Description"
                                        placeholder="Description"
                                      />
                                    </div>
                                    <div
                                      className="text-danger error-msg"
                                      style={{
                                        width: !this.state.expandNav ? "100%" : "50%",
                                      }}
                                    >
                                      {this.state.errors.task_description}
                                    </div>

                                    <div className="col-sm-12 form-group">
                                      <input
                                        type="submit"
                                        className="btn btn-success"
                                        value="Save"
                                      />
                                    </div>
                                  </form>
                                </Fragment>
                              )}
                            {this.state.type === "view_task" && (
                              <Fragment>
                                {/* <div style={{height:"25px"}} onClick={()=>this.editTaskSubTask(this.state.taskObj,'edit_task')}>
                                       <button className="btn btn-link-secondary show-form-modal" onClick={()=>this.editTaskSubTask(this.state.taskObj,'edit_task')}>
                                          <i className="icon-pencil icon-right" ></i>
                                       </button>
                                       </div> */}


                                <div>

                                  <div
                                    className="col-sm-12 form-group"
                                    style={{
                                      display: "flex",
                                      flexDirection: "row",
                                      alignItems: "center",
                                    }}
                                  >
                                    <label
                                      style={{ width: "100%" }}
                                      for="id_end_date"
                                    >
                                      Task Name&nbsp;:&nbsp;
                                  {this.state.individualEdit === "task_name" ? (
                                        <Fragment>
                                          <input
                                            ref={(node) => (this.node = node)}
                                            name="task_name"
                                            onChange={(e) =>
                                              this.handleIndividualChange(e)
                                            }
                                            className="half-width input-control"
                                            value={
                                              this.state.taskObj &&
                                              this.state.taskObj.task_name
                                            }
                                          />
                                          <div
                                            className="text-danger error-msg"
                                            style={{
                                              width: !this.state.expandNav
                                                ? "100%"
                                                : "50%",
                                            }}
                                          >
                                            {this.state.errors.task_name}
                                          </div>
                                        </Fragment>
                                      ) : (
                                          <label
                                            onClick={() =>
                                              this.setState({
                                                individualEdit: "task_name",
                                              })
                                            }
                                            className="text-light-grey"
                                          >
                                            {this.state.taskObj &&
                                              this.state.taskObj.task_name}
                                          </label>
                                        )}
                                      <button
                                        className="btn btn-link-secondary show-form-modal"
                                        onClick={() =>
                                          this.editTaskSubTask(
                                            this.state.taskObj,
                                            "edit_task"
                                          )
                                        }
                                      >
                                        <i
                                          className="icon-pencil icon-right"
                                          style={{
                                            position: "absolute",
                                            top: "0px",
                                          }}
                                        ></i>
                                      </button>{" "}
                                    </label>
                                  </div>
                                  <div
                                    className="col-sm-12 form-group"
                                    style={{
                                      fontSize: "13px",
                                      display: "flex",
                                      flexDirection: "row",
                                      alignItems: "center",
                                    }}
                                  >
                                    <label
                                      style={{ width: "100%" }}
                                      for="id_end_date"
                                    >
                                      {" "}
                                  Project &nbsp;:&nbsp;
                                  <span class="text-light-grey">
                                        {this.state.projectObj &&
                                          this.state.projectObj.project &&
                                          this.state.projectObj.project.project_name}
                                        <i
                                          class="fa fa-angle-double-right"
                                          style={{
                                            fontSize: "13px",
                                            margin: "0 8px",
                                          }}
                                          aria-hidden="true"
                                        ></i>
                                        {this.state.taskObj &&
                                          this.state.taskObj.task_name}
                                      </span>
                                      {this.state.subtasks &&
                                        this.state.subtasks.length > 0 &&
                                        this.state.subtasks.map((subtaskObj) => (
                                          <li>
                                            <Link
                                              style={{
                                                fontSize: "13px",
                                                color: "rgb(0, 86, 179)",
                                                marginTop: "5px",
                                              }}
                                              onClick={() =>
                                                this.openNav(
                                                  "view_subtask",
                                                  this.state.taskObj,
                                                  subtaskObj,
                                                  this.props.subTaskLog(
                                                    subtaskObj.slug
                                                  )
                                                )
                                              }
                                            >
                                              {subtaskObj.sub_task_name}
                                            </Link>
                                          </li>
                                        ))}
                                    </label>
                                  </div>
                                  <div
                                    className="col-sm-12 form-group"
                                    style={{
                                      display: "flex",
                                      flexDirection: "row",
                                      alignItems: "center",
                                    }}
                                  >
                                    <label
                                      style={{ width: "100%" }}
                                      for="id_end_date"
                                    >
                                      {" "}
                                  Created by&nbsp;:&nbsp;
                                  <label className="text-light-grey">
                                        {this.state.taskObj &&
                                          this.state.taskObj.created_by}{" "}
                                      </label>
                                    </label>
                                  </div>
                                  <div
                                    className="col-sm-12 form-group"
                                    style={{
                                      display: "flex",
                                      flexDirection: "row",
                                      alignItems: "center",
                                    }}
                                  >
                                    <label
                                      style={{ width: "100%" }}
                                      for="id_end_date"
                                    >
                                      Assigned date &nbsp;:&nbsp;
                                  <label className="text-light-grey">
                                        {this.state.taskObj &&
                                          this.state.taskObj.assigned_time}{" "}
                                      </label>
                                    </label>
                                  </div>
                                  <div
                                    className="col-sm-12 form-group"
                                    style={{
                                      display: "flex",
                                      flexDirection: "row",
                                      alignItems: "center",
                                    }}
                                  >
                                    <label
                                      style={{ width: "100%" }}
                                      for="id_end_date"
                                    >
                                      Due date&nbsp;:&nbsp;
                                  {this.state.individualEdit === "due_date" ? (
                                        <Fragment>
                                          <input
                                            ref={(node) => (this.node = node)}
                                            type="date"
                                            name="due_date"
                                            className="half-width input-control"
                                            label="End Date"
                                            onChange={(e) =>
                                              this.handleIndividualChange(e)
                                            }
                                            defaultValue={
                                              this.state.taskObj.due_date
                                            }
                                            min={this.state.startingDate}
                                          />
                                          <div
                                            className="text-danger error-msg"
                                            style={{
                                              width: !this.state.expandNav
                                                ? "100%"
                                                : "50%",
                                            }}
                                          >
                                            {this.state.errors.due_date}
                                          </div>
                                        </Fragment>
                                      ) : (
                                          <label
                                            onClick={() =>
                                              this.setState({
                                                individualEdit: "due_date",
                                              })
                                            }
                                            className="text-light-grey"
                                          >
                                            {this.state.taskObj &&
                                              this.state.taskObj.due_date
                                              ? Moment(
                                                this.state.taskObj &&
                                                this.state.taskObj.due_date
                                              ).format("DD-MM-YYYY")
                                              : "Nil"}{" "}
                                          </label>
                                        )}
                                    </label>
                                  </div>
                                  <div
                                    className="col-sm-12 form-group"
                                    style={{
                                      display: "flex",
                                      flexDirection: "row",
                                      alignItems: "center",
                                    }}
                                  >
                                    <div
                                      className="row"
                                      style={{
                                        width: "100%",
                                        paddingLeft: "15px",
                                        display: "flex",
                                        flexDirection: "row",
                                        alignItems: "center",
                                      }}
                                    >
                                      <label for="id_end_date">
                                        Status &nbsp;:&nbsp;{" "}
                                      </label>
                                      {this.state.individualEdit ===
                                        "task_board" ? (
                                          
                                          
                                          <div ref={(node) => (this.node = node) }>
                                            {console.log(
                                            'nodee',this.node)
                                            }
                                            <Select
                                              options={statusList}
                                              onChange={(e) =>
                                                this.handleIndividualStatusOrAssignedChange(
                                                  e
                                                )
                                              }
                                              value={statusList.filter(
                                                (emp) =>
                                                  emp.value ===
                                                  this.state.taskObj.task_board
                                              )}
                                            /></div>
                                        ) : (
                                          <label
                                            onClick={() =>
                                              this.setState({
                                                individualEdit: "task_board",
                                              })
                                            }
                                            className="text-light-grey"
                                          >
                                            {/* {this.state.taskObj&&this.state.taskObj.task_board.replace('_',"  ").charAt(0).toUpperCase() + this.state.taskObj.task_board.substr(1).toLowerCase()}  */}
                                            {this.formatString(
                                              this.state.taskObj &&
                                              this.state.taskObj.task_board
                                            )}
                                          </label>
                                        )}
                                    </div>
                                  </div>
                                  <div
                                    className="col-sm-12 form-group"
                                    style={{
                                      display: "flex",
                                      flexDirection: "row",
                                      alignItems: "center",
                                    }}
                                  >
                                    <div
                                      className="row"
                                      style={{
                                        width: "100%",
                                        paddingLeft: "15px",
                                        display: "flex",
                                        flexDirection: "row",
                                        alignItems: "center",
                                      }}
                                    >
                                      <label for="id_end_date">
                                        Assignee &nbsp;:&nbsp;
                                  </label>

                                      {this.state.individualEdit === "assigned" ? (
                                        <Fragment>
                                          <div ref={(node) =>
                                            (this.node = node)
                                          }>
                                            <Select
                                              options={employeeList}
                                              onChange={(e) =>
                                                this.handleIndividualStatusOrAssignedChange(
                                                  e
                                                )
                                              }
                                              value={employeeList.filter(
                                                (emp) =>
                                                  emp.value ===
                                                  (this.state.taskObj &&
                                                    this.state.taskObj.assigned &&
                                                    this.state.taskObj.assigned.slug)
                                              )}
                                            />
                                            <div className="text-danger error-msg">
                                              {this.state.errors.assigned}
                                            </div>
                                          </div>
                                        </Fragment>
                                      ) : (
                                          <label
                                            onClick={() =>
                                              this.setState({
                                                individualEdit: "assigned",
                                              })
                                            }
                                            className="text-light-grey"
                                          >
                                            {this.state.taskObj &&
                                              this.state.taskObj.assigned &&
                                              this.state.taskObj.assigned.first_name
                                              ? this.state.taskObj.assigned.first_name
                                              : "Nil"}{" "}
                                            {this.state.taskObj &&
                                              this.state.taskObj.assigned &&
                                              this.state.taskObj.assigned.last_name}
                                          </label>
                                        )}
                                    </div>
                                  </div>

                                  <div
                                    className="col-sm-12 form-group"
                                    style={{
                                      display: "flex",
                                      flexDirection: "row",
                                      alignItems: "center",
                                    }}
                                  >
                                    <div
                                      className="row"
                                      style={{ paddingLeft: "15px" }}
                                    >
                                      <div for="id_end_date">
                                        Description &nbsp;:&nbsp;
                                  </div>
                                      {this.state.individualEdit ===
                                        "task_description" ? (
                                          <Fragment>
                                            <textarea
                                              ref={(node) => (this.node = node)}
                                              name="task_description"
                                              cols="40"
                                              rows="10"
                                              onChange={(e) =>
                                                this.handleIndividualChange(e)
                                              }
                                              value={
                                                this.state.taskObj.task_description
                                              }
                                              className="input-control half-width"
                                              label="Description"
                                              placeholder="Description"
                                            />
                                            <div
                                              className="text-danger error-msg"
                                              style={{
                                                width: !this.state.expandNav
                                                  ? "100%"
                                                  : "50%",
                                              }}
                                            >
                                              {this.state.errors.task_description}
                                            </div>
                                            {/* <span className="text-danger">{this.state.errors.task_description}</span> */}
                                          </Fragment>
                                        ) : (
                                          <div
                                            onClick={() =>
                                              this.setState({
                                                individualEdit: "task_description",
                                              })
                                            }
                                            className="text-light-grey"
                                          >
                                            {this.state.taskObj &&
                                              this.state.taskObj.task_description
                                              ? this.state.taskObj.task_description
                                              : "Nil"}{" "}
                                          </div>
                                        )}
                                    </div>
                                  </div>
                                </div>
                                <div
                                  className="col-sm-12 form-group text-allign"
                                  style={{
                                    paddingLeft: "15px",
                                    paddingTop: "0px",
                                    paddingBottom: "15px",
                                    marginBottom: "0px",
                                  }}
                                >
                                  <button
                                    style={{ width: "110px" }}
                                    onClick={() =>
                                      this.openNav(
                                        "add_subtask",
                                        this.state.taskObj
                                      )
                                    }
                                    type="button"
                                    className="btn btn-outline-primary add-row"
                                  >
                                    <i className="icon-plus"></i> Add subtask
                              </button>
                                  {/* <button style={{width:"110px",position:"absolute",right:"0"}} type="button" className="btn btn-outline-primary add-row">
                                      
                                      <i className="icon-plus"></i> Add Time
                                      </button> */}
                                </div>
                                {!this.state.expandNav && (
                                  <div
                                    className="col-sm-12 form-group text-allign"
                                    style={{
                                      paddingLeft: "15px",
                                      marginBottom: "30px",
                                    }}
                                  >
                                    <button
                                      style={{ width: "110px" }}
                                      onClick={this.historyLog}
                                      type="button"
                                      className="btn btn-outline-primary add-row"
                                    >
                                      History Log
                                </button>

                                    <button
                                      style={{
                                        width: "110px",
                                        position: "absolute",
                                        right: "0",
                                      }}
                                      onClick={this.timeLog}
                                      type="button"
                                      className="btn btn-outline-primary add-row"
                                    >
                                      Time Log
                                </button>
                                  </div>
                                )}
                                {this.state.expandNav && this.state.timeLog && (
                                  <h5 style={{ paddingLeft: "15px" }}>Time Log</h5>
                                )}


                                {/*{this.state.expandNav &&
                              this.state.timeLog &&
                              this.state.taskObj?.task_board === "ongoing" && (
                                <div className="row">
                                  
                                  {!this.state.isClicked && (
                                    <div
                                      className="col-sm-2 form-group"
                                      style={{ textAlign: "center" }}
                                    >
                                      <button
                                        style={{ width: "100px" }}
                                        onClick={() =>
                                          this.startTime(
                                            this.state.startTime
                                              ? "start"
                                              : "end"
                                          )
                                        }
                                        type="button"
                                        className={
                                          this.state.startTime
                                            ? "btn btn-outline-success add-row"
                                            : "btn btn-outline-danger add-row"
                                        }
                                      >
                                        {this.state.startTime ? "Start" : "End"}
                                      </button>
                                      <div className="text-danger error-msg">
                                        {this.state.errors.assigned}
                                      </div>
                                    </div>
                                  )}
                                  
                                  <div
                                    className="col-sm-2 form-group"
                                    style={{ textAlign: "center" }}
                                  >
                                    <button
                                      style={{
                                        width: "110px",
                                        textAlign: "center",
                                      }}
                                      type="button"
                                      onClick={this.addTime}
                                      className="btn btn-outline-primary add-row"
                                    >
                                      {this.state.isClicked
                                        ? "Close"
                                        : "Add Time"}
                                    </button>
                                  </div>
                                </div>
                              )} */}


                                {this.state.isClicked && this.state.expandNav && (
                                  <Fragment>
                                    <span className="text-danger">
                                      {this.state.errors.assigned}
                                    </span>

                                    <div className="row">
                                      <div className="col-sm-3 form-group">
                                        <label
                                          className="text-light-grey"
                                          for="id_weekly_limit"
                                        >
                                          Start Date
                                    </label>
                                        <input
                                          type="date"
                                          name="add_start_date"
                                          component={renderField}
                                          onChange={(e) =>
                                            this.setState({
                                              [e.target.name]: e.target.value,
                                              add_start_date_error: "",
                                            })
                                          }
                                          className="input-control"
                                          label="Date and Time"
                                        />
                                        <span className="text-danger">
                                          {this.state.add_start_date_error}
                                        </span>
                                      </div>

                                      <div className="col-sm-3 form-group">
                                        <label
                                          className="text-light-grey"
                                          for="id_weekly_limit"
                                        >
                                          End Date
                                    </label>
                                        <input
                                          type="date"
                                          name="add_end_date"
                                          component={renderField}
                                          onChange={(e) =>
                                            this.setState({
                                              [e.target.name]: e.target.value,
                                              add_end_date_error: "",
                                            })
                                          }
                                          className="input-control"
                                          label="Date and Time"
                                          min={this.state.add_start_date}
                                        />
                                        <span className="text-danger">
                                          {this.state.add_end_date_error}
                                        </span>
                                      </div>

                                      <div
                                        className="col-sm-4 form-group"
                                        style={{
                                          marginTop: "29px",
                                          justifyContent: "center",
                                          alignItems: "center",
                                        }}
                                      >
                                        <label
                                          className="text-light-grey"
                                          for="id_weekly_limit"
                                        >
                                          Duration (HH:MM:SS)
                                    </label>
                                        <input
                                          style={{ width: "30px" }}
                                          value={this.state.hours}
                                          type="number"
                                          name="hours"
                                          onChange={(e) => this.checkDuration(e)}
                                        />
                                    :
                                    <input
                                          style={{ width: "30px" }}
                                          value={this.state.minutes}
                                          type="number"
                                          name="minutes"
                                          onChange={(e) => this.checkDuration(e)}
                                        />
                                    :
                                    <input
                                          style={{ width: "30px" }}
                                          value={this.state.seconds}
                                          name="seconds"
                                          onChange={(e) => this.checkDuration(e)}
                                          type="number"
                                        />
                                        <div
                                          className="text-danger"
                                          style={{ marginTop: "6px" }}
                                        >
                                          {this.state.duration_error}
                                        </div>
                                      </div>
                                      {/* </div>

                                     <div className="row" > */}

                                      <div
                                        className="col-sm-2 form-group"
                                        style={{
                                          display: "flex",
                                          paddingTop: "24px",
                                          justifyContent: "center",
                                          alignItems: "center",
                                        }}
                                      >
                                        <button
                                          onClick={this.addTimeSubmit}
                                          style={{ width: "100px" }}
                                          className="btn btn-outline-success add-row"
                                        >
                                          Save
                                    </button>
                                      </div>
                                    </div>
                                  </Fragment>
                                )}

                                {/* <h5>Time Log</h5> */}

                                {this.state.expandNav && this.state.timeLog && (
                                  <Fragment>
                                    {this.props.tasks?.timelog?.length > 0 ?
                                      <Fragment>

                                        <table style={{ textAlign: "center" }} id="dtHorizontalExample" class="table table-striped table-borderless table-sm" cellspacing="0"
                                          width="100%">
                                          <thead>
                                            <tr>
                                              <th>Task</th>
                                              <th>Assignee</th>
                                              <th>Date</th>
                                              <th>Start</th>
                                              <th>End</th>
                                              <th>Worked hours</th>
                                              <th>Productive hours</th>
                                              <th>Billable</th>
                                              <th>PM Approved</th>
                                            </tr>
                                          </thead>
                                          <tbody className="text-grey">
                                            {this.props.tasks?.timelog?.map(obj =>
                                              <tr>
                                                <td>
                                                  {this.props.tasks?.tasks.length > 0 && this.props.tasks?.tasks?.filter(i => i.slug === obj.task)[0]?.task_name}

                                                </td>
                                                <td>
                                                  {this.props.employees?.allEmployees?.response?.filter(i => i.slug === obj.employee)[0].first_name + " " +
                                                    this.props.employees?.allEmployees?.response?.filter(i => i.slug === obj.employee)[0].last_name
                                                  }
                                                </td>
                                                <td>{this.convertToDateFormat(obj.start_time_format)}</td>
                                                <td>{(obj.start_time_format) && this.convertToLocalTimeFormat(obj.start_time_format)}</td>
                                                <td>{(obj.end_time_format != null) && this.convertToLocalTimeFormat(obj.end_time_format)}</td>
                                                {/* <td><a href=""><i class="icon-close" style={{ fontSize:"16px", color: "rgb(156, 156, 156)"}}></i></a></td> */}
                                                <td>{obj.worked_hours}</td>
                                                <td>{obj.productive_hours}</td>
                                                <td>{obj.tracking ? 'Yes' : 'No'}</td>
                                                <td>{obj.pm_approved?obj.pm_approved:'00:00:00'}</td>
                                                <td className="td-edit">  {obj.end_time_format != null && <Link onClick={() => this.addTaskModal("edit_log", obj.slug, obj)}><i className="icon-pencil" style={{ fontSize: "16px", color: "rgb(156, 156, 156)" }}></i></Link>}</td>

                                              </tr>
                                            )}
                                          </tbody>
                                        </table>

                                      </Fragment> : <div>No Time Log Found <hr /></div>}
                                  </Fragment>
                                )}




                                {this.state.expandNav && this.state.historyLog && (
                                  <div className="col-sm-12 form-group">
                                    <h5>History Log</h5>
                                    <table
                                      className="d table"
                                      style={{ borderCollapse: "collapse" }}
                                    >
                                      <thead>
                                        <tr>
                                          <th style={{ width: "79px" }}>Action</th>
                                          <th>Time</th>
                                          <th>Action Performed By</th>
                                          <th>Field</th>
                                          <th>Old</th>
                                          <th>New</th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        {this?.props?.tasks?.tasklog &&
                                          this.props.tasks.tasklog
                                            .filter(
                                              (logObj) =>
                                                (logObj.history_type === "~" ||
                                                  logObj.history_type === "+") &&
                                                logObj.history_change_reason != "[]"
                                            )
                                            .map((logObj) => (
                                              <tr>
                                                <td
                                                  className="td-table"
                                                  style={{ paddingLeft: "5px" }}
                                                >
                                                  {logObj.history_type === "+"
                                                    ? "Created"
                                                    : "Updated"}
                                                </td>
                                                <td
                                                  className="td-table"
                                                  style={{ paddingLeft: "5px" }}
                                                >
                                                  {this.convertToLocalTime(
                                                    logObj.created
                                                  )}
                                                </td>
                                                <td
                                                  className="td-table"
                                                  style={{ paddingLeft: "5px" }}
                                                >
                                                  {logObj.history_user}
                                                </td>
                                                <td
                                                  style={{
                                                    paddingLeft: "5px",
                                                    overflowX: "hidden",
                                                  }}
                                                >
                                                  {logObj.history_type === "+"
                                                    ? null
                                                    : (this.parseLog(
                                                      logObj.history_change_reason,
                                                      "field"
                                                    ),
                                                      taskFields &&
                                                      taskFields.length > 0 &&
                                                      taskFields.map((i) => (
                                                        <p>{i}</p>
                                                      )))}
                                                </td>

                                                <td
                                                  style={{
                                                    paddingLeft: "5px",
                                                    overflowX: "hidden",
                                                  }}
                                                >
                                                  {logObj.history_type === "+"
                                                    ? null
                                                    : (this.parseLog(
                                                      logObj.history_change_reason,
                                                      "old",
                                                      employeeList
                                                    ),
                                                      OldTasks &&
                                                      OldTasks.length > 0 &&
                                                      OldTasks.map((i) => (
                                                        <p>{i}</p>
                                                      )))}
                                                </td>

                                                <td
                                                  style={{
                                                    paddingLeft: "5px",
                                                    overflowX: "hidden",
                                                  }}
                                                >
                                                  {logObj.history_type === "+"
                                                    ? null
                                                    : (this.parseLog(
                                                      logObj.history_change_reason,
                                                      "new",
                                                      employeeList
                                                    ),
                                                      NewTasks &&
                                                      NewTasks.length > 0 &&
                                                      NewTasks.map((i) => (
                                                        <p>{i}</p>
                                                      )))}
                                                </td>
                                              </tr>
                                            ))}
                                      </tbody>
                                    </table>
                                  </div>
                                )}
                                <Fragment>
                                  <label
                                    className="text-light-grey"
                                    for="id_remarks"
                                  >
                                    Comments
                              </label>
                                  {this.state.taskComments?.length > 0 ? (
                                    <Fragment>
                                      {this.state.taskComments.map((com) => (
                                        <Fragment>
                                          <ul>
                                            <li className="project-details-user text-muted">
                                              <span
                                                className="test-sm"
                                                style={{ color: "black" }}
                                              >
                                                {com.comment}{" "}
                                              </span>
                                              <p>
                                                {com.commented_by}&nbsp;
                                            {com.date_time}
                                              </p>
                                            </li>
                                          </ul>
                                        </Fragment>
                                      ))}
                                    </Fragment>
                                  ) : (
                                      <p>No comments</p>
                                    )}
                                </Fragment>

                                <div className="position-main text-gray">
                                  <div
                                    className=" text-grey col-sm-12 form-group"
                                    style={{
                                      lineHeight: "35px",
                                      position: "absolute",
                                      fontSize: "smaller",
                                    }}
                                  >
                                    <div
                                      className="col-sm-12 form-group "
                                      style={{ margin: "none" }}
                                    >
                                      <Fragment>
                                        <textarea
                                          id="comment-id"
                                          name="details"
                                          onChange={(e) =>
                                            this.handleTaskComments(e)
                                          }
                                          className="input-control input-control"
                                          value={this.state.taskComment}
                                          placeholder="Comments"
                                        />

                                        <span className="text-danger">
                                          {this.state.taskCommentError}
                                        </span>

                                        <div className="col-sm-12 text-center">
                                          <button
                                            type="button"
                                            className="btn btn-outline-primary add-row"
                                            onClick={this.addTaskComment}
                                          >
                                            <i className="icon-plus"></i> Add
                                        Comment
                                      </button>
                                        </div>
                                      </Fragment>
                                    </div>
                                  </div>
                                </div>
                              </Fragment>
                            )}

                            {this.state.type === "view_subtask" && (
                              <Fragment>
                                {/* <div style={{height:"25px"}}onClick={()=>this.editTaskSubTask(this.state.subtaskObj,'edit_subtask')}>
                     <button className="btn btn-link-secondary show-form-modal" onClick={()=>this.editTaskSubTask(this.state.subtaskObj,'edit_subtask')}>
                                          <i className="icon-pencil icon-right" ></i>
                                       </button>
                                       </div>                         */}
                                <div
                                  className="col-sm-12 form-group"
                                  style={{
                                    fontSize: "13px",
                                    display: "flex",
                                    flexDirection: "row",
                                    alignItems: "center",
                                  }}
                                >
                                  <label
                                    style={{ fontSize: "13px", width: "100%" }}
                                    for="id_end_date"
                                  >
                                    Sub Task Name &nbsp;:&nbsp;{" "}
                                    <label className="text-light-grey">
                                      {" "}
                                      {this.state.subtaskObj &&
                                        this.state.subtaskObj.sub_task_name}
                                    </label>
                                    <button
                                      className="btn btn-link-secondary show-form-modal"
                                      onClick={() =>
                                        this.editTaskSubTask(
                                          this.state.subtaskObj,
                                          "edit_subtask"
                                        )
                                      }
                                    >
                                      <i
                                        className="icon-pencil icon-right"
                                        style={{ position: "absolute", top: "0px" }}
                                      ></i>
                                    </button>
                                  </label>
                                </div>
                                <div
                                  style={{
                                    fontSize: "13px",
                                    display: "inline",
                                    width: "100%",
                                  }}
                                  className="col-sm-12 form-group"
                                  style={{
                                    display: "flex",
                                    flexDirection: "row",
                                    alignItems: "center",
                                  }}
                                >
                                  <label
                                    for="id_end_date"
                                    style={{ width: "100%" }}
                                  >
                                    {" "}
                                Project &nbsp;:&nbsp;
                                <span className="text-light-grey">
                                      {this.state.projectObj &&
                                        this.state.projectObj.project &&
                                        this.state.projectObj.project.project_name}
                                      <i
                                        class="fa fa-angle-double-right"
                                        style={{
                                          fontSize: "13px",
                                          margin: "0 8px",
                                        }}
                                        aria-hidden="true"
                                      ></i>
                                      <Link
                                        style={{
                                          fontSize: "13px",
                                          display: "inline",
                                          color: "rgb(0, 86, 179)",
                                        }}
                                        onClick={() =>
                                          this.openNav(
                                            "view_task",
                                            this.state.taskObj
                                          )
                                        }
                                      >
                                        {this.state.taskObj &&
                                          this.state.taskObj.task_name}
                                      </Link>
                                      <i
                                        class="fa fa-angle-double-right"
                                        style={{ margin: "0 8px" }}
                                        aria-hidden="true"
                                      ></i>
                                      {this.state.subtaskObj &&
                                        this.state.subtaskObj.sub_task_name}
                                    </span>
                                  </label>
                                </div>
                                <div
                                  className="col-sm-12 form-group"
                                  style={{
                                    display: "flex",
                                    flexDirection: "row",
                                    alignItems: "center",
                                  }}
                                >
                                  <label
                                    style={{ width: "100%" }}
                                    for="id_end_date"
                                  >
                                    {" "}
                                Created by&nbsp;:&nbsp;{" "}
                                    <label className="text-light-grey">
                                      {" "}
                                      {this.state.subtaskObj &&
                                        this.state.subtaskObj.created_by}{" "}
                                    </label>
                                  </label>
                                </div>
                                <div
                                  className="col-sm-12 form-group"
                                  style={{
                                    display: "flex",
                                    flexDirection: "row",
                                    alignItems: "center",
                                  }}
                                >
                                  <label
                                    style={{ width: "100%" }}
                                    for="id_end_date"
                                  >
                                    Assigned date &nbsp;:&nbsp;{" "}
                                    <label className="text-light-grey">
                                      {" "}
                                      {this.state.subtaskObj &&
                                        this.state.subtaskObj.assigned_time}{" "}
                                    </label>
                                  </label>
                                </div>
                                <div
                                  className="col-sm-12 form-group"
                                  style={{
                                    display: "flex",
                                    flexDirection: "row",
                                    alignItems: "center",
                                  }}
                                >
                                  <label
                                    style={{ width: "100%" }}
                                    for="id_end_date"
                                  >
                                    Due date &nbsp;:&nbsp;{" "}
                                    <label className="text-light-grey">
                                      {" "}
                                      {this.state.subtaskObj &&
                                        this.state.subtaskObj.due_date}{" "}
                                    </label>
                                  </label>
                                </div>
                                <div
                                  className="col-sm-12 form-group"
                                  style={{
                                    display: "flex",
                                    flexDirection: "row",
                                    alignItems: "center",
                                  }}
                                >
                                  <label
                                    style={{ width: "100%" }}
                                    for="id_end_date"
                                  >
                                    Status &nbsp;:&nbsp;{" "}
                                    <label className="text-light-grey">
                                      {" "}
                                      {this.state.subtaskObj?.sub_task_board}{" "}
                                    </label>
                                  </label>
                                </div>
                                <div
                                  className="col-sm-12 form-group"
                                  style={{
                                    display: "flex",
                                    flexDirection: "row",
                                    alignItems: "center",
                                  }}
                                >
                                  <label
                                    style={{ width: "100%" }}
                                    for="id_end_date"
                                  >
                                    Assignee &nbsp;:&nbsp;{" "}
                                    <label className="text-light-grey">
                                      {" "}
                                      {this.state.subtaskObj &&
                                        this.state.subtaskObj.assigned &&
                                        this.state.subtaskObj.assigned
                                          .first_name}{" "}
                                      {this.state.subtaskObj &&
                                        this.state.subtaskObj.assigned &&
                                        this.state.subtaskObj.assigned
                                          .last_name}{" "}
                                    </label>
                                  </label>
                                </div>
                                <div
                                  className="col-sm-12 form-group"
                                  style={{
                                    display: "flex",
                                    flexDirection: "row",
                                    alignItems: "center",
                                  }}
                                >
                                  <label
                                    style={{ width: "100%" }}
                                    for="id_end_date"
                                  >
                                    Description &nbsp;:&nbsp;{" "}
                                    <label className="text-light-grey">
                                      {" "}
                                      {this.state.subtaskObj &&
                                        this.state.subtaskObj.task_description}{" "}
                                    </label>
                                  </label>
                                </div>
                                {/* <div className="col-sm-12 form-group text-allign" style={{paddingLeft: "0px"}}> */}

                                {/* <button style={{width:"110px"}} type="button" className="btn btn-outline-primary add-row">
                                      
                                      <i className="icon-plus"></i> Add Time
                                      </button> */}

                                {!this.state.expandNav && (
                                  <div
                                    className="col-sm-12 form-group text-allign"
                                    style={{
                                      paddingLeft: "15px",
                                      marginBottom: "30px",
                                    }}
                                  >
                                    <button
                                      style={{ width: "110px" }}
                                      onClick={this.historyLog}
                                      type="button"
                                      className="btn btn-outline-primary add-row"
                                    >
                                      History Log
                                </button>

                                    {/* <button style={{width:"110px",position:"absolute",right:"0"}} onClick={this.timeLog}type="button" className="btn btn-outline-primary add-row" >
                                      
                                     Time Log
                                      </button> */}
                                  </div>
                                )}

                                {this.state.expandNav && this.state.timeLog && (
                                  <h5 style={{ paddingLeft: "15px" }}>Time Log</h5>
                                )}
                                {/* <div class="col-sm-12 form-group table-heading" >History Log</div> */}
                                {this.state.expandNav && this.state.timeLog && (
                                  <div className="row">
                                    <div
                                      className="col-sm-2 form-group"
                                      style={{ textAlign: "center" }}
                                    >
                                      <button
                                        style={{ width: "100px" }}
                                        onClick={this.startTime}
                                        type="button"
                                        className={
                                          this.state.startTime
                                            ? "btn btn-outline-success add-row"
                                            : "btn btn-outline-danger add-row"
                                        }
                                      >
                                        {this.state.startTime ? "Start" : "End"}
                                      </button>
                                    </div>
                                    <div
                                      className="col-sm-2 form-group"
                                      style={{ textAlign: "center" }}
                                    >
                                      <button
                                        style={{
                                          width: "110px",
                                          textAlign: "center",
                                        }}
                                        type="button"
                                        onClick={this.addTime}
                                        className="btn btn-outline-primary add-row"
                                      >
                                        {this.state.isClicked
                                          ? "Close"
                                          : "Add Time"}
                                      </button>
                                    </div>
                                  </div>
                                )}

                                {this.state.isClicked && this.state.expandNav && (
                                  <div className="row">
                                    <div className="col-sm-4 form-group">
                                      <label
                                        className="text-light-grey"
                                        for="id_weekly_limit"
                                      >
                                        Date and Time
                                  </label>
                                      <Field
                                        type="datetime-local"
                                        name="id_weekly_limit"
                                        component={renderField}
                                        label="Date and Time"
                                      />
                                    </div>

                                    <div
                                      className="col-sm-2 form-group"
                                      style={{
                                        display: "flex",
                                        paddingTop: "24px",
                                        justifyContent: "center",
                                        alignItems: "center",
                                      }}
                                    >
                                      <input
                                        type="submit"
                                        style={{ width: "100px" }}
                                        className="btn btn-outline-success add-row"
                                        value="Save"
                                      />
                                    </div>
                                  </div>
                                )}
                                {this.state.expandNav && this.state.historyLog && (
                                  <div className="col-sm-12 form-group">
                                    <h5>History Log</h5>

                                    <table class="table">
                                      <thead>
                                        <tr>
                                          <th>Action</th>
                                          <th>Time</th>
                                          <th>Action Performed by</th>
                                          <th>Field</th>
                                          <th>Old</th>
                                          <th>New</th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        {this.state.subTaskLog?.length > 0 ? (
                                          <Fragment>
                                            {this.state.subTaskLog?.map(
                                              (logObj) => (
                                                <tr>
                                                  <td
                                                    className="td-table"
                                                    style={{ paddingLeft: "5px" }}
                                                  >
                                                    {logObj.history_type === "+"
                                                      ? "Created"
                                                      : "Updated"}
                                                  </td>
                                                  <td
                                                    className="td-table"
                                                    style={{ paddingLeft: "5px" }}
                                                  >
                                                    {this.convertToLocalTime(
                                                      logObj.created
                                                    )}
                                                  </td>
                                                  <td
                                                    className="td-table"
                                                    style={{ paddingLeft: "5px" }}
                                                  >
                                                    {logObj.history_user}
                                                  </td>
                                                  <td
                                                    style={{
                                                      paddingLeft: "5px",
                                                      overflowX: "hidden",
                                                    }}
                                                  >
                                                    {logObj.history_type === "+"
                                                      ? null
                                                      : (this.parseLog(
                                                        logObj.history_change_reason,
                                                        "field"
                                                      ),
                                                        taskFields &&
                                                        taskFields.length > 0 &&
                                                        taskFields.map((i) => (
                                                          <p>{i}</p>
                                                        )))}
                                                  </td>

                                                  <td
                                                    style={{
                                                      paddingLeft: "5px",
                                                      overflowX: "hidden",
                                                    }}
                                                  >
                                                    {logObj.history_type === "+"
                                                      ? null
                                                      : (this.parseLog(
                                                        logObj.history_change_reason,
                                                        "old",
                                                        employeeList
                                                      ),
                                                        OldTasks &&
                                                        OldTasks.length > 0 &&
                                                        OldTasks.map((i) => (
                                                          <p>{i}</p>
                                                        )))}
                                                  </td>

                                                  <td
                                                    style={{
                                                      paddingLeft: "5px",
                                                      overflowX: "hidden",
                                                    }}
                                                  >
                                                    {logObj.history_type === "+"
                                                      ? null
                                                      : (this.parseLog(
                                                        logObj.history_change_reason,
                                                        "new",
                                                        employeeList
                                                      ),
                                                        NewTasks &&
                                                        NewTasks.length > 0 &&
                                                        NewTasks.map((i) => (
                                                          <p>{i}</p>
                                                        )))}
                                                  </td>
                                                </tr>
                                              )
                                            )}
                                          </Fragment>
                                        ) : (
                                            "Not Found"
                                          )}
                                      </tbody>
                                    </table>
                                  </div>
                                )}
                              </Fragment>
                            )}
                          </Fragment>
                        )}
                    </div>

                    <div
                      className="row team-content"
                      style={{
                        width: this.state.openNav ? "50%" : "100%",
                        margin: "0px",
                        padding: "0px",
                        maxHeight: "100vh",
                        minHeight: "100vh",
                        overflowY: "scroll",
                      }}
                    >
                      <ul
                        className="nav nav-tabs"
                        id="myTab"
                        role="tablist"
                        style={{ minHeight: "51px", maxHeight: "0px" }}
                      >
                        {!is_admin && <li className="nav-item">
                          <a
                            className="nav-link active"
                            style={{ fontSize: "13px", padding: "13px" }}
                            id="mytask-list"
                            data-toggle="tab"
                            href="#mytasklist"
                            role="tab"
                            aria-controls="mytasklist"
                            aria-selected="true"
                            onClick={() => this.setState({ taskType: 'myTask' }, () => this.closeNav())}
                          >
                            My Task
                        </a>
                        </li>}
                        <li className="nav-item">
                          {(is_admin ||
                            (permissions &&
                              permissions.includes("view_all_tasks"))) && (
                              <a
                                className={(is_admin) ? "nav-link active " : "nav-link"}
                                id="alltask-list"
                                data-toggle="tab"
                                style={{ fontSize: "13px", padding: "13px" }}
                                href="#alltasklist"
                                role="tab"
                                aria-controls="alltasklist"
                                aria-selected="true"
                                onClick={() => this.setState({ taskType: 'allTask' }, () => this.closeNav())}
                              >
                                {" "}
                            All Task
                              </a>
                            )}
                        </li>
                      </ul>
                      <div
                        className="tab-content"
                        id="myTabContent"
                        style={{ width: "100%", minHeight: "100vh" }}
                      >


                        <div
                          className={(is_admin) ? "tab-pane userMail-tab fade show active text-grey" : "tab-pane userMail-tab fade  text-grey"}
                          id="alltasklist"
                          // style={{overflow: "auto",minHeight: "50vh",maxHeight:"50vh"}}
                          // style={{ padding: "0px 15px" }}
                          style={{ paddingTop: "2px" }}
                          role="tabpanel"
                          aria-labelledby="alltask-list"
                        >
                          {!this.state.openNav ? (
                            <div className="row" style={{ alignItems: "center" }}>
                              <div className="row team col-sm-4">
                                <ul className="nav nav-tabs" id="myTask" role="tablist">
                                  <li className="nav-item">
                                    <a
                                      className="nav-link active "
                                      id="active-tab"
                                      data-toggle="tab"
                                      href="#active"
                                      role="tab"
                                      aria-controls="active"
                                      aria-selected="true"
                                    >
                                      Active
                            </a>
                                  </li>
                                  <li className="nav-item" style={{ padding: "0px" }}>
                                    <a
                                      className="nav-link "
                                      id="inactive-tab"
                                      data-toggle="tab"
                                      href="#inactive"
                                      role="tab"
                                      aria-controls="inactive"
                                      aria-selected="true"
                                    >
                                      Inactive
                            </a>
                                  </li>
                                </ul>
                              </div>
                              <div className="col-sm-4  "
                                style={{ textAlign: "end" }}>
                                {(is_admin ||
                                  (permissions &&
                                    permissions.includes("add_task"))) && (
                                    <button
                                      style={{ width: "110px" }}
                                      onClick={() => this.openNav("add_task")}
                                      type="button"
                                      className="btn btn-outline-primary add-row"
                                    >
                                      <i className="icon-plus"></i> Add Task
                                    </button>
                                  )}
                              </div>
                              <div
                                className="col-sm-4  "
                                style={{ textAlign: "end" }}
                              >
                                {(
                                  <button
                                    style={{ width: "110px" }}
                                    onClick={() => this.openNav("time_log")}
                                    type="button"
                                    className="btn btn-outline-primary add-row"
                                  >
                                    <i className="icon-plus"></i> Time Log
                                  </button>
                                )}
                              </div>
                            </div>
                          ) : null}
                          <div className="tab-content col-sm-12" id="myTaskContent">
                            <div
                              className="tab-pane userMail-tab fade show active text-grey"
                              id="active"
                              role="tabpanel"
                              aria-labelledby="active-tab"
                            >
                              <div className="col-sm-12 mt-3">
                                {this.props.tasks &&
                                  this.props.tasks.tasks &&
                                  this.props.tasks.tasks.length > 0 ? (
                                    <table style={{ tableLayout: "auto", width: "100%" }} class="table table-cs text-grey table-text-sm table-striped table-borderless">
                                      <thead>
                                        <tr>
                                          <th className="thead-padding">Task Name</th>
                                          {!this.state.openNav && (
                                            <th className="thead-padding">Status</th>
                                          )}
                                          {!this.state.openNav && (
                                            <th className="thead-padding">Assignee</th>
                                          )}
                                          {!this.state.openNav && (
                                            <th className="thead-padding">Due Date</th>
                                          )}
                                        </tr>
                                      </thead>
                                      <tbody>


                                        {this.props.tasks.tasks.filter(taskObj => taskObj.task_board === "ongoing" || taskObj.task_board === "todo").map(
                                          (taskObj, index) => (
                                            <tr>
                                              <td style={{ whiteSpace: "break-spaces" }}>
                                                <i
                                                  style={{ marginRight: "2px" }}
                                                  class="fa fa-check-circle-o"
                                                  aria-hidden="true"
                                                ></i>
                                                <Link
                                                  onClick={() =>
                                                    this.openNav("view_task", taskObj)
                                                  }
                                                >
                                                  {taskObj.task_name}


                                                </Link>
                                              </td>
                                              {!this.state.openNav && (
                                                <Fragment>
                                                  {(this.state.individualEdit === 'task_board' && index === this.state.taskIndex) ?

                                                    <div ref={(node) => 
                                                      (this.node = node)
                                                    }>
                                                      <Select options={statusList}
                                                        onChange={(e) => this.handleIndividualStatusOrAssignedChange(e)}
                                                        value={statusList.filter((emp) => emp.value === this.state.taskObj.task_board)}
                                                      /></div>
                                                    :

                                                    <td onClick={() => this.setState({ individualEdit: 'task_board', taskIndex: index, taskObj: taskObj, listEdit: true })} >
                                                      {this.formatString(taskObj && taskObj.task_board)}
                                                    </td>

                                                  }</Fragment>
                                              )}
                                              {!this.state.openNav && (
                                                <td>
                                                  {this.state.individualEdit ===
                                                    "assigned" &&
                                                    index === this.state.taskIndex ? (
                                                      <Fragment>

                                                        <div
                                                          ref={(node) => 
                                                            (this.node = node)
                                                          }
                                                        >
                                                          <Select
                                                            options={employeeList}
                                                            onChange={(e) =>
                                                              this.handleIndividualStatusOrAssignedChange(
                                                                e
                                                              )
                                                            }
                                                            value={employeeList.filter(
                                                              (emp) =>
                                                                emp.value ===
                                                                (this.state.taskObj &&
                                                                  this.state.taskObj
                                                                    .assigned &&
                                                                  this.state.taskObj
                                                                    .assigned.slug)
                                                            )}
                                                          />
                                                        </div>
                                                        <div className="text-danger error-msg">
                                                          {this.state.errors.assigned}
                                                        </div>
                                                      </Fragment>
                                                    ) : (
                                                      <Fragment>
                                                        {taskObj &&
                                                          taskObj.assigned &&
                                                          taskObj.assigned.first_name ? (
                                                            <Fragment>
                                                              <span className="user-img-holder  ">
                                                                {taskObj &&
                                                                  taskObj.assigned &&
                                                                  taskObj.assigned.first_name.charAt(
                                                                    0
                                                                  )}
                                                                {taskObj &&
                                                                  taskObj.assigned &&
                                                                  taskObj.assigned.last_name.charAt(
                                                                    0
                                                                  )}
                                                              </span>

                                                              <span
                                                                onClick={() =>
                                                                  this.setState({
                                                                    individualEdit:
                                                                      "assigned",
                                                                    taskIndex: index,
                                                                    taskObj: taskObj,
                                                                    listEdit: true,
                                                                  })
                                                                }
                                                              >
                                                                {taskObj &&
                                                                  taskObj.assigned &&
                                                                  taskObj.assigned
                                                                    .first_name}
                                                                {taskObj &&
                                                                  taskObj.assigned &&
                                                                  taskObj.assigned
                                                                    .last_name}
                                                              </span>
                                                            </Fragment>
                                                          ) : (
                                                            <div
                                                              onClick={() =>
                                                                this.setState({
                                                                  individualEdit:
                                                                    "assigned",
                                                                  taskIndex: index,
                                                                  taskObj: taskObj,
                                                                  listEdit: true,
                                                                })
                                                              }
                                                            >
                                                              Nill
                                                            </div>
                                                          )}
                                                      </Fragment>
                                                    )}
                                                </td>
                                              )}
                                              {!this.state.openNav && (
                                                <td>
                                                  <div>
                                                    {this.state.individualEdit ===
                                                      "due_date" &&
                                                      index === this.state.taskIndex ? (
                                                        <Fragment>
                                                          <input
                                                            ref={(node) =>
                                                              (this.node = node)
                                                            }
                                                            type="date"
                                                            name="due_date"
                                                            className=" input-control"
                                                            label="End Date"
                                                            onChange={(e) =>
                                                              this.handleIndividualChange(
                                                                e
                                                              )
                                                            }
                                                            defaultValue={
                                                              this.state.taskObj.due_date
                                                            }
                                                            min={this.state.startingDate}
                                                          />
                                                          <div
                                                            className="text-danger error-msg"
                                                            style={{
                                                              width: !this.state.expandNav
                                                                ? "100%"
                                                                : "50%",
                                                            }}
                                                          >
                                                            {this.state.errors.due_date}
                                                          </div>
                                                        </Fragment>
                                                      ) : (
                                                        <label
                                                          onClick={() =>
                                                            this.setState({
                                                              individualEdit: "due_date",
                                                              taskIndex: index,
                                                              taskObj: taskObj,
                                                              listEdit: true,
                                                            })
                                                          }
                                                          className="text-light-grey"
                                                        >
                                                          {taskObj?.due_date
                                                            ? taskObj?.due_date
                                                            : "Nill"}{" "}
                                                        </label>
                                                      )}
                                                  </div>
                                                </td>
                                              )}
                                            </tr>
                                          )
                                        )}
                                      </tbody>
                                    </table>
                                  ) : (
                                    "No Tasks Available"
                                  )}
                              </div>
                            </div>
                            <div
                              className="tab-pane userMail-tab fade  text-grey"
                              id="inactive"
                              role="tabpanel"
                              aria-labelledby="inactive-tab"
                            >
                              <div className="col-sm-12 mt-3">
                                {this.props.tasks &&
                                  this.props.tasks.tasks &&
                                  this.props.tasks.tasks.length > 0 ? (
                                    <table style={{ tableLayout: "auto", width: "100%" }} class="table table-cs text-grey table-text-sm table-striped table-borderless">
                                      <thead>
                                        <tr>
                                          <th className="thead-padding">Task Name</th>
                                          {!this.state.openNav && (
                                            <th className="thead-padding">Status</th>
                                          )}
                                          {!this.state.openNav && (
                                            <th className="thead-padding">Assignee</th>
                                          )}
                                          {!this.state.openNav && (
                                            <th className="thead-padding">Due Date</th>
                                          )}
                                        </tr>
                                      </thead>
                                      <tbody>


                                        {this.props.tasks.tasks.filter(taskObj => taskObj.task_board === "cancelled" || taskObj.task_board === "done").map(
                                          (taskObj, index) => (
                                            <tr>
                                              <td style={{ whiteSpace: "break-spaces" }}>
                                                <i
                                                  style={{ marginRight: "2px" }}
                                                  class="fa fa-check-circle-o"
                                                  aria-hidden="true"
                                                ></i>
                                                <Link
                                                  onClick={() =>
                                                    this.openNav("view_task", taskObj)
                                                  }
                                                >
                                                  {taskObj.task_name}


                                                </Link>
                                              </td>
                                              {!this.state.openNav && (
                                                <Fragment>
                                                  {(this.state.individualEdit === 'task_board' && index === this.state.taskIndex) ?

                                                    <div ref={(node) => 
                                                      (this.node = node)
                                                    }>
                                                      <Select options={statusList}
                                                        onChange={(e) => this.handleIndividualStatusOrAssignedChange(e)}
                                                        value={statusList.filter((emp) => emp.value === this.state.taskObj.task_board)}
                                                      /></div>
                                                    :

                                                    <td onClick={() => this.setState({ individualEdit: 'task_board', taskIndex: index, taskObj: taskObj, listEdit: true })} >
                                                      {this.formatString(taskObj && taskObj.task_board)}
                                                    </td>

                                                  }</Fragment>
                                              )}
                                              {!this.state.openNav && (
                                                <td>
                                                  {this.state.individualEdit ===
                                                    "assigned" &&
                                                    index === this.state.taskIndex ? (
                                                      <Fragment>

                                                        <div
                                                          ref={(node) =>
                                                            (this.node = node)
                                                          }
                                                        >
                                                          <Select
                                                            options={employeeList}
                                                            onChange={(e) =>
                                                              this.handleIndividualStatusOrAssignedChange(
                                                                e
                                                              )
                                                            }
                                                            value={employeeList.filter(
                                                              (emp) =>
                                                                emp.value ===
                                                                (this.state.taskObj &&
                                                                  this.state.taskObj
                                                                    .assigned &&
                                                                  this.state.taskObj
                                                                    .assigned.slug)
                                                            )}
                                                          />
                                                        </div>
                                                        <div className="text-danger error-msg">
                                                          {this.state.errors.assigned}
                                                        </div>
                                                      </Fragment>
                                                    ) : (
                                                      <Fragment>
                                                        {taskObj &&
                                                          taskObj.assigned &&
                                                          taskObj.assigned.first_name ? (
                                                            <Fragment>
                                                              <span className="user-img-holder  ">
                                                                {taskObj &&
                                                                  taskObj.assigned &&
                                                                  taskObj.assigned.first_name.charAt(
                                                                    0
                                                                  )}
                                                                {taskObj &&
                                                                  taskObj.assigned &&
                                                                  taskObj.assigned.last_name.charAt(
                                                                    0
                                                                  )}
                                                              </span>

                                                              <span
                                                                onClick={() =>
                                                                  this.setState({
                                                                    individualEdit:
                                                                      "assigned",
                                                                    taskIndex: index,
                                                                    taskObj: taskObj,
                                                                    listEdit: true,
                                                                  })
                                                                }
                                                              >
                                                                {taskObj &&
                                                                  taskObj.assigned &&
                                                                  taskObj.assigned
                                                                    .first_name}
                                                                {taskObj &&
                                                                  taskObj.assigned &&
                                                                  taskObj.assigned
                                                                    .last_name}
                                                              </span>
                                                            </Fragment>
                                                          ) : (
                                                            <div
                                                              onClick={() =>
                                                                this.setState({
                                                                  individualEdit:
                                                                    "assigned",
                                                                  taskIndex: index,
                                                                  taskObj: taskObj,
                                                                  listEdit: true,
                                                                })
                                                              }
                                                            >
                                                              Nill
                                                            </div>
                                                          )}
                                                      </Fragment>
                                                    )}
                                                </td>
                                              )}
                                              {!this.state.openNav && (
                                                <td>
                                                  <div>
                                                    {this.state.individualEdit ===
                                                      "due_date" &&
                                                      index === this.state.taskIndex ? (
                                                        <Fragment>
                                                          <input
                                                            ref={(node) =>
                                                              (this.node = node)
                                                            }
                                                            type="date"
                                                            name="due_date"
                                                            className=" input-control"
                                                            label="End Date"
                                                            onChange={(e) =>
                                                              this.handleIndividualChange(
                                                                e
                                                              )
                                                            }
                                                            defaultValue={
                                                              this.state.taskObj.due_date
                                                            }
                                                            min={this.state.startingDate}
                                                          />
                                                          <div
                                                            className="text-danger error-msg"
                                                            style={{
                                                              width: !this.state.expandNav
                                                                ? "100%"
                                                                : "50%",
                                                            }}
                                                          >
                                                            {this.state.errors.due_date}
                                                          </div>
                                                        </Fragment>
                                                      ) : (
                                                        <label
                                                          onClick={() =>
                                                            this.setState({
                                                              individualEdit: "due_date",
                                                              taskIndex: index,
                                                              taskObj: taskObj,
                                                              listEdit: true,
                                                            })
                                                          }
                                                          className="text-light-grey"
                                                        >
                                                          {taskObj?.due_date
                                                            ? taskObj?.due_date
                                                            : "Nill"}{" "}
                                                        </label>
                                                      )}
                                                  </div>
                                                </td>
                                              )}
                                            </tr>
                                          )
                                        )}
                                      </tbody>
                                    </table>
                                  ) : (
                                    "No Tasks Available"
                                  )}
                              </div></div>


                          </div>

                        </div>


                        {!is_admin &&
                          <div
                            className="tab-pane userMail-tab fade show active text-grey"
                            id="mytasklist"
                            // style={{overflow: "auto",height: "260px"}}
                            // style={{ paddingLeft: "0px 15px" }}
                            style={{ paddingTop: "2px" }}
                            role="tabpanel"
                            aria-labelledby="mytask-list"
                          >
                            {!this.state.openNav ? (
                              <div className="row" style={{ alignItems: "center" }}>
                                <div className="row team col-sm-4">
                                  <ul className="nav nav-tabs" id="myTask" role="tablist">
                                    <li className="nav-item">
                                      <a
                                        className="nav-link active "
                                        id="active-mytask"
                                        data-toggle="tab"
                                        href="#activemytask"
                                        role="tab"
                                        aria-controls="activemytask"
                                        aria-selected="true"
                                      >
                                        Active
                            </a>
                                    </li>
                                    <li className="nav-item" style={{ padding: "0px" }}>
                                      <a
                                        className="nav-link "
                                        id="inactive-mytask"
                                        data-toggle="tab"
                                        href="#inactivemytask"
                                        role="tab"
                                        aria-controls="inactive-mytask"
                                        aria-selected="true"
                                      >
                                        Inactive
                            </a>
                                    </li>
                                  </ul>
                                </div>
                                <div className="col-sm-4  "
                                  style={{ textAlign: "end" }}>
                                  {(is_admin ||
                                    (permissions &&
                                      permissions.includes("add_task"))) && (
                                      <button
                                        style={{ width: "110px" }}
                                        onClick={() => this.openNav("add_task")}
                                        type="button"
                                        className="btn btn-outline-primary add-row"
                                      >
                                        <i className="icon-plus"></i> Add Task
                                      </button>
                                    )}
                                </div>
                                <div
                                  className="col-sm-4  "
                                  style={{ textAlign: "end" }}
                                >
                                  {(
                                    <button
                                      style={{ width: "110px" }}
                                      onClick={() => this.openNav("time_log")}
                                      type="button"
                                      className="btn btn-outline-primary add-row"
                                    >
                                      <i className="icon-plus"></i> Time Log
                                    </button>
                                  )}
                                </div>
                              </div>
                            ) : null}
                            <div className="tab-content col-sm-12" id="myTaskContent">
                              <div
                                className="tab-pane userMail-tab fade show active text-grey"
                                id="activemytask"
                                role="tabpanel"
                                aria-labelledby="active-mytask"
                              >
                                <table style={{ tableLayout: "auto", width: "100%" }} class="table table-cs text-grey table-text-sm table-striped table-borderless">
                                  <thead>
                                    <tr>
                                      <th>Task Name</th>
                                      {!this.state.openNav && (<th>Status</th>)}
                                      {!this.state.openNav && (<th>Assignee</th>)}
                                      {!this.state.openNav && (<th>Due Date</th>)}

                                    </tr>
                                  </thead>
                                  <tbody>
                                    {this.props.tasks &&
                                      this.props.tasks.mytasks &&
                                      this.props.tasks.mytasks.length > 0 ? (
                                        this.props.tasks.mytasks.filter(taskObj => taskObj.task_board === "ongoing" || taskObj.task_board === "todo").map(
                                          (taskObj, index) => (
                                            <tr>
                                              <td style={{ whiteSpace: "break-spaces" }}><i
                                                style={{ marginRight: "2px" }}
                                                class="fa fa-check-circle-o"
                                                aria-hidden="true"
                                              ></i>
                                                <Link
                                                  onClick={() =>
                                                    this.openNav("view_task", taskObj)
                                                  }
                                                >
                                                  {taskObj.task_name}
                                                </Link></td>
                                              {!this.state.openNav && (<td>{this.formatString(
                                                taskObj && taskObj.task_board
                                              )}</td>)}
                                              {!this.state.openNav && (<td><div className="user-img-holder">
                                                {taskObj &&
                                                  taskObj.assigned &&
                                                  taskObj.assigned.first_name.charAt(0)}
                                                {taskObj &&
                                                  taskObj.assigned &&
                                                  taskObj.assigned.last_name.charAt(0)}
                                              </div>
                                                {taskObj &&
                                                  taskObj.assigned &&
                                                  taskObj.assigned.first_name}
                                                {taskObj &&
                                                  taskObj.assigned &&
                                                  taskObj.assigned.last_name}</td>)}
                                              {!this.state.openNav && (<td> {taskObj.due_date}</td>)}
                                            </tr>

                                          ))
                                      ) : (
                                        <div className="position-main ">
                                          No Tasks Available
                                        </div>
                                      )}
                                  </tbody>
                                </table>
                              </div>

                              <div
                                className="tab-pane userMail-tab fade  text-grey"
                                id="inactivemytask"
                                role="tabpanel"
                                aria-labelledby="inactive-mytask"
                              >

                                {/* MY TASKS */}

                                <table style={{ tableLayout: "auto", width: "100%" }} class="table table-cs text-grey table-text-sm table-striped table-borderless">
                                  <thead>
                                    <tr>
                                      <th>Task Name</th>
                                      {!this.state.openNav && (<th>Status</th>)}
                                      {!this.state.openNav && (<th>Assignee</th>)}
                                      {!this.state.openNav && (<th>Due Date</th>)}

                                    </tr>
                                  </thead>
                                  <tbody>
                                    {this.props.tasks &&
                                      this.props.tasks.mytasks &&
                                      this.props.tasks.mytasks.length > 0 ? (
                                        this.props.tasks.mytasks.filter(taskObj => taskObj.task_board === "cancelled" || taskObj.task_board === "done").map(
                                          (taskObj, index) => (
                                            <tr>
                                              <td style={{ whiteSpace: "break-spaces" }}><i
                                                style={{ marginRight: "2px" }}
                                                class="fa fa-check-circle-o"
                                                aria-hidden="true"
                                              ></i>
                                                <Link
                                                  onClick={() =>
                                                    this.openNav("view_task", taskObj)
                                                  }
                                                >
                                                  {taskObj.task_name}
                                                </Link></td>
                                              {!this.state.openNav && (<td>{this.formatString(
                                                taskObj && taskObj.task_board
                                              )}</td>)}
                                              {!this.state.openNav && (<td><div className="user-img-holder">
                                                {taskObj &&
                                                  taskObj.assigned &&
                                                  taskObj.assigned.first_name.charAt(0)}
                                                {taskObj &&
                                                  taskObj.assigned &&
                                                  taskObj.assigned.last_name.charAt(0)}
                                              </div>
                                                {taskObj &&
                                                  taskObj.assigned &&
                                                  taskObj.assigned.first_name}
                                                {taskObj &&
                                                  taskObj.assigned &&
                                                  taskObj.assigned.last_name}</td>)}
                                              {!this.state.openNav && (<td> {taskObj.due_date}</td>)}
                                            </tr>

                                          ))
                                      ) : (
                                        <div className="position-main ">
                                          No Tasks Available
                                        </div>
                                      )}
                                  </tbody>
                                </table>



                              </div></div>








                          </div>
                        }

                      </div>
                    </div>
                  </div>

                  <div
                    className="tab-pane userMail-tab fade"
                    id="financialinfo"
                    role="tabpanel"
                    aria-labelledby="financial-info"
                  >
                    {console.log("fin", this.state.projectObj &&
                      this.state.projectObj.project && combined.length === 0)}
                    <div className="row team-content">
                      <table
                        className="table"
                        style={{ color: "#9C9C9C", textAlign: "center" }}
                      >
                        {/* {this.state.projectObj &&
                            this.state.projectObj.project &&
                            this.state.projectObj.project.billing_type != "fixed" &&combined.length===0&&<Fragment>
                              <div>no resourses found</div></Fragment>} */}
                        {this.state.projectObj &&
                          this.state.projectObj.project &&
                          this.state.projectObj.project.billing_type != "fixed" ?
                          <thead>
                            {combined.length > 0 ?
                              <tr>
                                <th style={{ color: "#9C9C9C" }}>Contract Id</th>
                                <th style={{ color: "#9C9C9C" }}>Name</th>
                                <th style={{ color: "#9C9C9C" }}>Role</th>
                                <th style={{ color: "#9C9C9C" }}> Hourly Rate</th>
                                <th style={{ color: "#9C9C9C" }}> Weekly Limit</th>
                                <th style={{ color: "#9C9C9C" }}>Status</th>
                                <th style={{ color: "#9C9C9C" }}>Details</th>
                              </tr> : <div className="text-danger">No resources found </div>}
                          </thead> :
                          <thead>
                            <tr>
                              <th style={{ color: "#9C9C9C" }}>Contract Id</th>
                              <th style={{ color: "#9C9C9C" }}>Status</th>
                              <th style={{ color: "#9C9C9C" }}>Details</th>
                            </tr>
                          </thead>}
                        <tbody>
                          {this.state.projectObj &&
                            this.state.projectObj.project &&
                            this.state.projectObj.project.billing_type === "fixed" ? (
                              <Fragment>
                                {this.state.projectObj &&
                                  this.state.projectObj.financial_info &&
                                  this.state.projectObj.financial_info.contract_attachments &&
                                  this.state.projectObj.financial_info.contract_attachments.map(
                                    (obj, index) => (
                                      <tr>
                                        <td>{obj && obj.contract_number}</td>

                                        {/* {obj.contract?
                        <td> <button type="button" onClick={() => this.props.downloadContract(obj.id,obj.contract)} style={{width:"75px",fontSize:"12px"}}  className="btn btn-outline-primary add-row">Download{console.log("obj",obj)} </button></td>
                        :<td><button type="button" onClick={()=>this.addTaskModal("upload",obj.id)} style={{width:"75px",fontSize:"12px"}}  className="btn btn-outline-primary add-row">Upload{console.log("obj",obj)} </button></td>} */}

                                        <td>
                                          {obj &&
                                            obj.contract_status &&
                                            obj.contract_status
                                              .charAt(0)
                                              .toUpperCase() +
                                            obj.contract_status.slice(1)}
                                          <Link
                                            onClick={() =>
                                              this.addTaskModal(
                                                "status",
                                                obj.contract_id
                                              )
                                            }
                                          >
                                            <i
                                              className="btn btn-link-secondary"
                                              className="icon-pencil"
                                              style={{
                                                fontSize: "16px",
                                                marginLeft: "3px",
                                                color: "rgb(156, 156, 156)",
                                              }}
                                            ></i>
                                          </Link>
                                        </td>
                                        <td>
                                          <Link
                                            onClick={() =>
                                              this.addTaskModal("view project")
                                            }
                                          >
                                            View
                                      </Link>
                                        </td>
                                      </tr>
                                    )
                                  )}
                              </Fragment>
                            ) : (
                              <Fragment>

                                {combined.length > 0 &&
                                  combined.map((obj, index) => (
                                    <tr>
                                      <td>{obj && obj.contract_number}</td>
                                      <td>
                                        {obj &&
                                          obj.resource_assigned &&
                                          obj.resource_assigned.first_name}{" "}
                                        {obj &&
                                          obj.resource_assigned &&
                                          obj.resource_assigned.last_name}
                                      </td>
                                      <td>
                                        {obj &&
                                          obj.resource_assigned &&
                                          obj.resource_assigned.designation &&
                                          obj.resource_assigned.designation
                                            .designation_name}
                                      </td>

                                      {this.state.projectObj &&
                                        this.state.projectObj.project &&
                                        this.state.projectObj.project.billing_type !=
                                        "fixed" && (
                                          <Fragment>
                                            <td>{obj && obj.hourly_rate}</td>
                                            <td>{obj && obj.weekly_limit}</td>
                                          </Fragment>
                                        )}
                                      {/* {obj.contract?
                        <td> <button type="button" style={{width:"75px",fontSize:"12px"}} onClick={() => this.props.downloadContract(obj.id,obj.contract)} className="btn btn-outline-primary add-row">Download </button></td>:
                        <td><button type="button" onClick={()=>this.addTaskModal("upload",obj.id)} style={{width:"75px",fontSize:"12px"}}  className="btn btn-outline-primary add-row">Upload{console.log("obj",obj)} </button></td>} */}
                                      <td>
                                        {obj &&
                                          obj.contract_status &&
                                          obj.contract_status
                                            .charAt(0)
                                            .toUpperCase() +
                                          obj.contract_status.slice(1)}

                                        {(is_admin ||
                                          (permissions &&
                                            permissions.includes(
                                              "change_project_status"
                                            ))) && (
                                            <Link
                                              onClick={() =>
                                                this.addTaskModal(
                                                  "status",
                                                  obj.contract_id
                                                )
                                              }
                                            >
                                              <i
                                                className="btn btn-link-secondary"
                                                className="icon-pencil"
                                                style={{
                                                  fontSize: "16px",
                                                  marginLeft: "3px",
                                                  color: "rgb(156, 156, 156)",
                                                }}
                                              ></i>
                                            </Link>
                                          )}
                                      </td>
                                      <td>
                                        <Link
                                          onClick={() =>
                                            this.addTaskModal(
                                              "view project",
                                              obj.resource_id, null, obj.resource
                                            )
                                          }
                                        >
                                          View
                                    </Link>
                                      </td>
                                      <td>
                                        {(is_admin ||
                                          (permissions &&
                                            permissions.includes(
                                              "change_financialinfo"
                                            ))) && (
                                            <Link
                                              onClick={() =>
                                                this.addTaskModal(
                                                  "financial",
                                                  obj.resource_id
                                                )
                                              }
                                            >
                                              <i
                                                className="btn btn-link-secondary"
                                                className="icon-pencil"
                                                style={{
                                                  fontSize: "16px",
                                                  color: "#9C9C9C",
                                                }}
                                              ></i>
                                            </Link>
                                          )}
                                      </td>
                                    </tr>
                                  ))}
                              </Fragment>
                            )}
                        </tbody>
                      </table>
                    </div>
                  </div>
                  <div
                    className="tab-pane userMail-tab fade"
                    id="clientcommunication"
                    role="tabpanel"
                    aria-labelledby="client-communication-tab"
                  >
                    <div className="col-sm-12 form-group">
                      <Fragment>
                        <label className="text-light-grey" for="id_remarks">
                          Comments
                      </label>
                        {comments != null && comments.length > 0 ? (
                          <Fragment>
                            {comments.map((com) => (
                              <Fragment>
                                <ul>
                                  <li className="project-details-user text-muted">
                                    <span
                                      className="test-sm"
                                      style={{ color: "black" }}
                                    >
                                      {com.detail_text}{" "}
                                    </span>
                                    <p>
                                      {com.commented_by} &nbsp;{com.date_time}
                                    </p>
                                  </li>
                                </ul>
                              </Fragment>
                            ))}
                          </Fragment>
                        ) : (
                            <p>No comments</p>
                          )}
                      </Fragment>

                      <Fragment>
                        <textarea
                          id="comment-id"
                          name="details"
                          onChange={(e) => this.handleComments(e)}
                          className="input-control input-control"
                          value={this.state.comments}
                          placeholder="Comments"
                        />

                        <span className="text-danger">
                          {this.state.commentError}
                        </span>

                        <div className="col-sm-12 text-center">
                          <button
                            type="button"
                            className="btn btn-outline-primary add-row"
                            onClick={() => this.addComment()}
                          >
                            <i className="icon-plus"></i> Add Comment
                        </button>
                        </div>
                      </Fragment>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          )}
        {this.state.editModal && (
          <AddTaskModal
            {...this.props}
            typeList={typeList}
            id={this.state.index}
            clientList={clientList}
            projectObj={this.state.projectObj}
            combined={combined}
            closeModal={this.closeEditModal}
            index={this.state.index}
            editPart={this.state.editPart}
            availbleResources={employeeList}
            handleIndividualStatusOrAssignedChange={this.handleIndividualStatusOrAssignedChange}
            taskType={this.state.taskType}
            timeLogObj={this.state.timeLogObj}
            startTimer={this.startTimer}
            formatString={this.formatString}
            weeklyWorksheetEmp={this.state.weeklyWorksheetEmp}
            dashBoardEdit={false}
          />
        )}
      </Fragment>
    );
  }
}
ViewProjectContent = reduxForm({
  form: "addProjectform",
})(ViewProjectContent);
export default ViewProjectContent;
