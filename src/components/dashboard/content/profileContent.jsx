import React, {Component, Fragment} from 'react'
import { Field, reduxForm } from 'redux-form'
import {countryReorder} from '../../../countryReorder'
import LoaderImage from '../../../static/assets/image/loader.gif';

var loaderStyle={
    width: '3%',
    position: 'fixed',
    top: '50%',
    left: '57%'
  }

const MODAL_OPEN_CLASS = "modal-open";
const required = value => {
    return (value || typeof value === 'number' ? undefined : ' is Required')
}
// const mobileNumderRequired =value => {
//     return (value ||typeof value === 'number' ? undefined : 'Mobile Number is Required')
// }

const maxLength = max => value =>
    value && value.length > max ? `Must be ${max} characters or less` : undefined
const maxLength15 = maxLength(15)
// const maxLength200 = maxLength(200)

const mobileValid = value =>{
    var mobilenoregex = /^(?!\b(0)\1+\b)(\+?\d{1,3}[. -]?)?\(?\d{3}\)?([. -]?)\d{3}\3\d{4}$/
    if(value && value.length >=1){
        return (mobilenoregex.test(value) ? undefined : 'Enter a Valid Mobile Number with country code')
    }
    return undefined
}
var letters = /^[-a-zA-Z0-9-()]+(\s+[-a-zA-Z0-9-()]+)*$/;
const alphabetOnly =  value => {
    return(value && value.match(letters) ? undefined : ' must be characters only')
}


const renderField = ({
    input,
    label,
    type,
    existed,
    id,
    className,
    accept,
    initialValue,
    name,
    meta: { touched, error, warning, }
}) => {
            return (
        
                
                <Fragment>
                    <input {...input} type={type} className={className} id={id} accept={accept} defaultValue={initialValue} />
                    {touched && 
                        ((error && <small className="text-danger">{label !=="Enter Contact Number" &&label}{error}{label ==="Enter Contact Number" }</small>) ||
                            (warning && <span>{warning}</span>))}
                    

                </Fragment>
            )}

const renderFieldSelect = ({
input,
label,
country,
countryState,
city,
timezone,
className,
initialValue,
meta: { touched, error, warning, }
}) => {
return (


    <Fragment>
        <select {...input} className={className} defaultValue ={initialValue}>
            <option value="">{label}</option>
            {(country && label === "Country") &&
                <Fragment>
                    {country.countryList.map((plan, index) => (
                        <option key={index} value={plan.id}>{plan.name_ascii}</option>
                    ))}</Fragment>
            }
            {(countryState && label === "State") &&
                <Fragment>
                    {countryState.stateList.map((plan, index) => (
                        <option key={index} value={plan.id}>{plan.name_ascii}</option>
                    ))}</Fragment>
            }
            {(city && label === "City") &&
                <Fragment>
                    {city.cityList.map((plan, index) => (
                        <option key={index} value={plan.id}>{plan.name_ascii}</option>
                    ))}</Fragment>
            }
            {(timezone && label === "Time Zone") &&
                <Fragment>
                    {timezone.timeZones.map((plan, index) => (
                        <option key={index} value={plan}>{plan}</option>
                    ))}</Fragment>
            }
            
        </select>
        <div>
            {touched &&
                ((error && <small className="text-danger">{label}{error}</small>) ||
                    (warning && <span>{warning}</span>))}
        </div>
        </Fragment>
)}
class ProfileContent extends Component{
    // constructor(props){
    //     super(props)
    //     }
    state={
        profileFlag:false,
        image_validation_error:false,
        existed_error:''
    }
    editProfile =() =>{
        this.props.initialize({ 
            first_name: this.props.profileDetails.profileDetail.first_name,
            last_name: this.props.profileDetails.profileDetail.last_name,
            timezone: this.props.profileDetails.profileDetail.time_zone&&this.props.profileDetails.profileDetail.time_zone,
            // designation: this.props.profileDetails.profileDetail.designation&&this.props.profileDetails.profileDetail.designation,
            country: this.props.profileDetails.profileDetail.country&&this.props.profileDetails.profileDetail.country.id,
            city: this.props.profileDetails.profileDetail.city &&this.props.profileDetails.profileDetail.city.id,
            state: this.props.profileDetails.profileDetail.state&&this.props.profileDetails.profileDetail.state.id,
            contact_number: this.props.profileDetails.profileDetail.contact_number&&this.props.profileDetails.profileDetail.contact_number,
            
        });
        this.setState({
            profileFlag:!this.state.profileFlag,existed_error:''
        })
        if(this.props.profileDetails ){
            if(this.props.profileDetails.profileDetail)
            {   if(this.props.profileDetails.profileDetail.country){
                this.props.fetchStatesList(this.props.profileDetails.profileDetail.country.id)
                }
                if(this.props.profileDetails.profileDetail.state){
                    this.props.fetchCityList(this.props.profileDetails.profileDetail.state.id)
                }
            }
        }
        if(this.props.editProfileDetails&&this.props.editProfileDetails.error&&
            this.props.editProfileDetails.error.response.data.contact_number[0]){
                this.props.clearerrors()
        }

    }
    componentDidUpdate(prevProps){
        if(prevProps.editProfileDetails!=this.props.editProfileDetails&&this.props.editProfileDetails.error&&this.props.editProfileDetails.error.response
            &&this.props.editProfileDetails.error.response.data&&this.props.editProfileDetails.error.response.data.contact_number){
            this.setState({existed_error:this.props.editProfileDetails.error.response.data.contact_number[0]})
        }
    }

    componentDidMount() {
        document.body.classList.add(MODAL_OPEN_CLASS);
        this.props.getProfile()
        this.props.fetchCountryList();
        this.props.fetchTimeZoneList()
        if(this.props.editProfileDetails){
            if(this.props.editProfileDetails.profileEditFlag === true){
                this.setState({
                    profileFlag:!this.state.profileFlag
                })
            }
        }
    }
    componentWillUnmount() {
        document.body.classList.remove(MODAL_OPEN_CLASS);
    }
    handleCountryChange = (e) => {
        // making fields null
        this.props.change('state','')  
        this.props.change('city','')

        this.props.fetchStatesList(e.target.value);
        this.props.fetchCityList(" ");
    }
    handleStateChange = (e) => {
        // making fields null
        this.props.change('city','')
        
        this.props.fetchCityList(e.target.value);
    }
    imageChange = (e) => {
        
        let image = e.target.files[0]
        var imageTypes = ['image/png','image/jpg','image/gif','image/jpeg']
        var image_index = imageTypes.indexOf(image.type)
        if (image_index >= 0){
            this.setState({image_validation_error:false},() =>{console.log(this.state.image,'uploaded image')})
            const form_data = new FormData();
            form_data.append("photo",image );
            form_data.append("first_name",this.props.profileDetails.profileDetail.first_name)
            form_data.append("last_name",this.props.profileDetails.profileDetail.last_name)
            if(this.props.profileDetails.profileDetail.contact_number!== null ){
                form_data.append("contact_number",this.props.profileDetails.profileDetail.contact_number)
            }
            
            this.props.editProfile(form_data)
        }
        else{
            this.setState({image_validation_error:true})
        }
    }
    submit = (values) => {
        const form_data = new FormData();
        for ( var key in values ) {
            if(values[key] !==" " && values[key] !=null){
            form_data.append(key, values[key]);
            }
        }
        this.props.editProfile(form_data)
        
        // values.logo = this.state.image
        // values.owner = localStorage.getItem("profile_slug")
        // const form_data = new FormData();
        // for ( var key in values ) {
        //     form_data.append(key, values[key]);
        // }
        // this.props.createOrganization(form_data)
    }
    contactnumberExisted = () => {
        this.setState({existed_error:''})
    }
    render(){
        console.log('profiles',this.props)
        console.log('staeeee',this.state)
        if(this.props.editProfileDetails){
            if(this.props.editProfileDetails.profileEditFlag === true){
                window.location.reload();
            }
        }
        const { handleSubmit } = this.props
        return (
            <Fragment>
                {this.props.editProfileDetails?.isLoading?
                <Fragment>
                <div className="loader-img"> <img src={LoaderImage} style={loaderStyle} /></div>
                </Fragment>:

                <Fragment>
                {this.props.profileDetails && this.props.profileDetails.profileDetail&&
                <Fragment>
                <div className="clnt-prof bg-white p-4 fullwidth m-auto" onChange={this.imageChange}>

                    <div id="message" className="col-lg-12 float-left fullwidth">

                    </div>

                    <div className="emp-prm-info fullwidth float-left position-relative mb-4 mt-4">

                    {/* <form className="emp-img-inf emp-img position-relative float-left image-update-form" novalidate=""> */}
                    <div className="emp-img-inf emp-img position-relative float-left image-update-form">
                    <input className="d-none" type="file" name="photo"   />

                    <div className="">
                        {(this.props.profileDetails && this.props.profileDetails.profileDetail.photo)?
                        <img alt="logo" className="img-fluid" src={this.props.profileDetails.profileDetail.photo}/>:
                        <img alt="dummy pic" className="img-fluid" src={require("../../../static/assets/image/user.jpg")}/>}
                    
                    <div className="p-relative">
                        <label htmlFor="id_photo" className="id-pic" id="overlay"> <i className="icon-edit"></i><br/> Update</label>
                        <div className="d-none"><input  type="file" name="photo" accept="image/*" id="id_photo"/></div>
                    </div>
                    {this.state.image_validation_error && <div class="text-danger error-message">Upload a valid image</div>}
                    </div>
                    </div>
                    {/* </form> */}
                    
                    <ul className="float-left">
                        <li className="mb-4"><h5 className="m-0">{this.props.profileDetails.profileDetail.first_name +' ' +this.props.profileDetails.profileDetail.last_name} </h5></li>
                        <li className="mb-4"><p className="m-0 pro-color">{this.props.profileDetails.profileDetail.email}</p></li>
                        <li className="mb-4"><p className="m-0 pro-color">{this.props.profileDetails.profileDetail.designation}</p></li>
                        <li className="mb-4"><p className="m-0 pro-color">{this.props.profileDetails.profileDetail.contact_number}</p></li>

                        

                        
                    </ul>

                    <button type="button" className="btn btn-link-secondary btn-edit-abs p-0 show-form-modal" onClick={this.editProfile}>
                        <i className="icon-edit h6 text-blue-grey"></i>
                    </button>

                    </div>
                    <div className="fullwidth float-left position-relative">
                    <h5 className="text-uppercase pro-hd-col pro-hd-col2 float-left fullwidth"><span></span></h5>
                    <div className="detail-pro position-relative float-left fullwidth">
                        <ul className="p-0 float-left mt-4">

                        <li className="float-left fullwidth mb-3">
                            <div className="col-lg-6 col-sm-6 float-left">
                            <div className="row">
                                <h6 className="text-capitalize">Country :</h6>
                            </div>
                            </div>
                            <div className="col-lg-6 col-sm-6 float-left">
                            <div className="row">
                                <h6 className="text-capitalize pro-color">{this.props.profileDetails.profileDetail.country.name_ascii}</h6>
                            </div>
                            </div>
                        </li>
                        <li className="float-left fullwidth mb-3">
                            <div className="col-lg-6 col-sm-6 float-left">
                            <div className="row">
                                <h6 className="text-capitalize">State :</h6>
                            </div>
                            </div>
                            <div className="col-lg-6 col-sm-6 float-left">
                            <div className="row">
                                <h6 className="text-capitalize pro-color">{this.props.profileDetails.profileDetail.state&&this.props.profileDetails.profileDetail.state.name_ascii} </h6>
                            </div>
                            </div>
                        </li>

                        <li className="float-left fullwidth mb-3">
                            <div className="col-lg-6 col-sm-6 float-left">
                            <div className="row">
                                <h6 className="text-capitalize">City :</h6>
                            </div>
                            </div>
                            <div className="col-lg-6 col-sm-6 float-left">
                            <div className="row">
                                <h6 className="text-capitalize pro-color">{this.props.profileDetails.profileDetail.city &&this.props.profileDetails.profileDetail.city.name_ascii} </h6>
                            </div>
                            </div>
                        </li>

                        <li className="float-left fullwidth mb-3">
                            <div className="col-lg-6 col-sm-6 float-left">
                            <div className="row">
                                <h6 className="text-capitalize">Time Zone :</h6>
                            </div>
                            </div>
                            <div className="col-lg-6 col-sm-6 float-left">
                            <div className="row">
                                <h6 className="text-capitalize pro-color">{this.props.profileDetails.profileDetail.time_zone} </h6>
                            </div>
                            </div>
                        </li>

                        </ul>
                    </div>
                    </div>
                    </div>
                
                    
                    {/* <div className="modal form-modal show" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"> */}
                    {this.state.profileFlag&& 
                    <div id="modal-block">

                        <div className="modal form-modal show" id="editDetails" tabIndex="-1" role="dialog" aria-labelledby="TermsLabel" style={{display: "block", paddingLeft: "15px"}}>
                        <div className="modal-dialog modal-dialog-centered" role="document">
                            <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title" id="TermsLabel">Edit Profile</h5>
                                <button type="button" className="btn btn-black" data-dismiss="modal" aria-label="Close" onClick={this.editProfile}>
                                <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div className="modal-body">
                                {/* <form method="post" action="/accounts/admin-profile-edit/3b5c4315-b443-4d08-86ae-7ab33fb5c77b/" class="popup-form" id="edit-details" novalidate=""> */}
                                <form onSubmit={handleSubmit(this.submit)} noValidate>
                                <input type="hidden" name="csrfmiddlewaretoken" value="GuTFe9rqmwN24MhL6oH22397ZtVDTioecqAn6wsbmL1GF7G3cjAPv2S8rZDSllWW"/>
                                <div className="row">

                                    <div className="col-sm-6 form-group">
                                    <label htmlFor="id_first_name">First name</label>
                                        {/* <input type="text" name="first_name" value="ANjitha" placeholder="First Name" class="fullwidth input-control" maxlength="128" required="" id="id_first_name"/> */}
                                        <Field type="text" name="first_name" component={renderField}
                                                        label="First Name" validate={[required,alphabetOnly]} className="fullwidth input-control" />
                                        {/* <div class="text-danger error-message"></div> */}
                                    </div>

                                    <div className="col-sm-6 form-group">
                                    <label htmlFor="id_last_name">Last name</label>
                                        {/* <input type="text" name="last_name" value="Deva" placeholder="Last Name" class="fullwidth input-control" maxlength="128" required="" id="id_last_name"/> */}
                                        <Field type="text" name="last_name" component={renderField}
                                                        label="Last Name" validate={[required,alphabetOnly]} className="fullwidth input-control" initialValue={this.props.profileDetails.profileDetail.last_name}/>
                                        {/* <div class="text-danger error-message"></div> */}
                                    </div>

                                    

                                    <div className="col-sm-6 form-group">
                                    <label htmlFor="id_contact_number">Contact number</label>
                                        {/* <input type="tel" name="contact_number" placeholder="Enter Contact Number" class="input-control input-control" id="id_contact_number"/> */}
                                        <Field type="text" name="contact_number" component={renderField}
                                                        label="Enter Contact Number" onChange={this.contactnumberExisted} validate={[mobileValid]} className="input-control input-control" />
                                    <div id="contact-no-error" className="text-danger">
                                    <small className="text-danger">{this.state.existed_error}</small>
                                    </div>

                                    </div>

                                    <div className="col-sm-6 form-group">
                                    <label htmlFor="id_country">Country 
                                        
                                    </label>
                                    <Field
                                        name="country"
                                        type="select"
                                        country={countryReorder(this.props.countryDetails)}
                                        component={renderFieldSelect}
                                        className="input-control select-country"
                                        label="Country"
                                        validate={required}
                                        onChange={this.handleCountryChange} />
                                        <div className="text-danger error-message"></div>
                                    </div>

                                    <div className="col-sm-6 form-group">
                                    <label htmlFor="id_state">State 
                                        
                                    </label>
                                    <Field
                                        name="state"
                                        type="select"
                                        countryState={this.props.stateDetails}
                                        component={renderFieldSelect}
                                        className="input-control select-state"
                                        label="State"
                                        validate={required}
                                        onChange={this.handleStateChange}
                                    />
                                        <div className="text-danger error-message"></div>
                                    </div>

                                    <div className="col-sm-6 form-group">
                                    <label htmlFor="id_city">City 
                                        
                                    </label>
                                    <Field
                                        name="city"
                                        type="select"
                                        city={this.props.cityDetails}
                                        component={renderFieldSelect}
                                        validate={required}
                                        className="input-control select-city"
                                        label="City" />
                                        {/* <div class="text-danger error-message"></div> */}
                                    </div>

                                    <div className="col-sm-6 form-group">
                                    <label htmlFor="id_time_zone">Time zone</label>
                                    <Field
                                        name="timezone"
                                        type="select"
                                        timezone={this.props.timeZoneDetails}
                                        component={renderFieldSelect}
                                        className="input-control select-city"
                                        label="Time Zone" />
                                        {/* <div class="text-danger error-message"></div> */}
                                    </div>
                                </div>
                                <div className="modal-footer">
                                <button type="submit" className="btn btn-success">Save</button>
                                </div>
                                </form>
                            </div>
                            
                            </div>
                        </div>
                        </div></div>
                    }
                    </Fragment>
                }
                </Fragment>}
            </Fragment>
        )
    }
}

ProfileContent = reduxForm({
    form: 'EditProfile',
})(ProfileContent)

export default ProfileContent