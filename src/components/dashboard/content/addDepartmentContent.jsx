import React, {Component, Fragment} from 'react'
import { Field, reduxForm } from 'redux-form'
import { Redirect } from 'react-router-dom';
import {alphabetOnly} from '../../../constants/validations'



const required =  value => {
  return(value || typeof value === 'number' ? undefined : ' is Required')
}

const renderField = ({
  input,
  label,
  type,
  existed,
  meta: { touched, error, warning,  }
}) => {
    
  return(
  

    <Fragment>
      <input {...input} placeholder={label} type={type} className={(touched && error)||(existed) ? 'fullwidth input-control border border-danger': 'fullwidth input-control'}/>
      {touched &&
        ((error && <small className="text-danger">{label}{error}</small>) ||
          (warning && <span>{warning}</span>))}
      
      {(existed) && <small class="text-danger">
          {existed}
      </small>}
      
    </Fragment>
  
)}

const renderRadioField = ({ input, label, type, checked }) => (
  <Fragment>
    
    <label>{label}</label>
    <input {...input} type={type} checked={checked} /> 
    
  </Fragment>
);

const renderFieldSelect = ({
  input,
  label,
  type,
  existed,
  options,
  meta: { touched, error, warning,  }
}) => {
    
  return(
    <Fragment>
      {(options)? 
          <select {...input} className={(touched && error)? 'fullwidth input-control border border-danger': 'fullwidth input-control'}>
              <option id='no-branch' value="">-----</option>
              {options.data.map((branch)=>
                            <option value={branch.slug} >{branch.branch_name}  </option>
                              )}
          </select>:<p className="text-danger">Please create a branch first.</p>
      }
      <div>
      {touched &&
        ((error && <small className="text-danger">{label}{error}</small>) ||
          (warning && <span>{warning}</span>))}
      </div>
      
      
    </Fragment>
  
)}


class AddDepartmentContent extends Component{
  constructor(props) {
    super(props);
    this.state = {
      show_branch: false,
      success_message: false,
      existed_error:''
    };
  }
  componentDidMount(){
    this.props.retrieveOrganization();
    this.props.retrieveBranches(localStorage.getItem('organization_slug'),localStorage.getItem('token'))
    // this.props.retrieveDepartments('');
  }

  componentWillUnmount(){
    this.props.clearDepartmentsStatusSuccess();
  }

  clearExistedError =() =>{
      this.setState({existed_error:''})
    }

  componentDidUpdate(prevProps){
    if(this.props.edit&&(this.props.departmentlist!=prevProps.departmentlist)){
      this.props.departmentlist&&this.props.departmentlist.error&&this.props.departmentlist.error.data&&this.props.departmentlist.error.data.non_field_errors&&
      this.props.departmentlist.error.data.non_field_errors[0]==='The fields department_name, organization, branch must make a unique set.'&&
      this.setState({existed_error:'Department Name already exists'})
    }
    else{
      if((this.props.department!=prevProps.department)){
      this.props.department&&this.props.department.departmentCreateError&&this.props.department.departmentCreateError.data&&this.props.department.departmentCreateError.data.non_field_errors&&
      this.props.department.departmentCreateError.data.non_field_errors[0]==='The fields department_name, organization, branch must make a unique set.'&&
      this.setState({existed_error:'Department Name already exists'})
    }
    }
  }

  show_branch=()=>{ 
    this.setState({show_branch:true})
  }

  hide_branch=()=>{ 
    this.setState({show_branch:false})   
  }

  closeSuccessMessage = ()=>{
    this.setState({
        success_message:false
    })
  }

  submit = (values) => {
    if("branch_id" in values){
      if(!("owner_type" in values)){
        values.owner_type = "branch"
      }
    }
    this.setState({
      success_message:true
    })
    this.props.createDepartments(values)
  }
    render(){
      const { handleSubmit} = this.props

        return (

            <Fragment>
              {(this.props.department&&this.props.department.status==='success')?
              window.location.replace("/departments/departments/"):
            
              <div className="col-md-10 offset-md-1 card card-signin mt-3 p-5">
                {(this.state.success_message&&this.props.department&&this.props.department.department&&this)&&
                <div className="alert alert-dismissible fade show alert-warning-green mt-3">
                  <i className="icon-checked b-6 alert-icon mr-2"></i> Department {this.props.department.department.department_name} was created sucessfully
                  <button onClick={this.closeSuccessMessage} type="button" className="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                </div>
                }

                <div className="text-left  mt-3 mb-5">
                  <h2>Add Department</h2>
                </div>

                <form onSubmit={handleSubmit(this.submit)}>
                  <input type="hidden" name="csrfmiddlewaretoken" value="DkFtmdTiKMIddfiLNH6AE6GUFR8HkAcQlKovQL0pt4FB2ZHhrONkEOMJmjp9zIJk"/>
                  {(this.props.branches&&this.props.branches.data&&this.props.branches.data.length>0)?
                  <div className="form-group row">
                    <div className="col-md-3"></div>
                    <div className="col-md-9">
                      <label>
                        <Field name="owner_type" checked={(!this.state.show_branch)?true:false} component={renderRadioField} type="radio" value="null" onChange={this.hide_branch} />{' '}
                          Add for {(this.props.organization&&this.props.organization.organization_data)&&this.props.organization.organization_data.organization_name}
                      </label><br/>
                      <label>
                        <Field name="owner_type" checked={this.state.show_branch}  component={renderRadioField} type="radio" value="branch" onChange={this.show_branch}/>{' '}
                          Add for Branch
                      </label>
                    </div>
                  </div> :
                  <div className="form-group row" id="buttondisable">
                    <div className="col-md-3"></div>
                    <div className="col-md-9 text-info branch">
                      Note: This organization has no branches. add new branch to create departments under branch
                    </div>
                  </div>
                  }
                  {this.state.show_branch&&
                  <div className="form-group row branch" >
                    <div className="col-md-3">
                      <label className="form-control-label" htmlFor="id_branch">
                        <label htmlFor="id_branch">Branch</label>
                        <span className="text-danger">*</span>
                      </label>
                    </div>
                    <div className="col-md-9">
                    <Field 
                      name="branch_id" 
                      type ="select"
                      options = {this.props.branches} 
                      component={renderFieldSelect} 
                      className="fullwidth" 
                      label="Branch"
                      validate={required}/>
                      <div className="error-message text-danger">                          
                      </div>
                    </div>
                  </div>
                  }

                  <div className="form-group row">
                    <div className="col-md-3">
                      <label className="form-control-label" htmlFor="id_department_name">
                          <label htmlFor="id_department_name">Department Name</label>
                      </label>
                      <span className="text-danger">*</span>
                    </div>
                    <div className="col-md-9">
                      <Field type="text"
                        name="department_name"
                        component={renderField}
                        label="Department Name"
                        onChange={this.clearExistedError}
                        existed={this.state.existed_error}
                        validate={[required,alphabetOnly]}/>
                    </div>
                  </div>

                  <div className="form-group row">
                    <div className="col-md-3">
                      <label className="form-control-label" htmlFor="id_description">
                        <label htmlFor="id_description">Description</label>
                      </label>
                    </div>
                    <div className="col-md-9">
                      <Field type="text"
                          name="description"
                          cols="40" rows="10"
                          component="textarea" 
                          className="input-control input-control"
                          placeholder="Description" />
                    </div>
                  </div>

                  <div className="m-5">
                    <input type="submit" className="btn width btn-primary float-right" value="SAVE"/>
                  </div>
                </form>
              </div>
              }
            </Fragment>
        )
    }
}

AddDepartmentContent = reduxForm({
    form:'addDepartmentform'
  })(AddDepartmentContent)
export default AddDepartmentContent