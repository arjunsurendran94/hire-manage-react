// PersonalDetails.jsx
import React, { Component, Fragment } from 'react';
import { Form, Button } from 'semantic-ui-react';
import { throws } from 'assert';
import Select from 'react-select';
import UpdateClientModal from '../modal/addClientProjectModal'


var clientList = [];

class PersonalDetails extends Component {
   constructor(props) {
      super(props);
      this.state={
         
      }
   }
   saveAndContinue = (e) => {
      e.preventDefault();
      
      let validate=this.props.validateProjectForm('save')

      validate&&this.props.nextStep();
   }

   back = (e) => {
      e.preventDefault();
      this.props.prevStep();
   }

   

   componentDidMount(){
      this.props.clientListing()
      
   }

   // componentDidUpdate(prevprops){
   //    if(prevprops.clientList!=this.props.clientList){
   //       // let clients=this.props.clientList
   //       console.log('sssssssssssssssssssssssss');
         
   //       this.setState({clients:this.props.clientList})
   //    }
   // }

   componentDidUpdate(prevProps,prevState){
   }

   


   render() {
 
      const { values } = this.props
      console.log(this.props);
      
         
      return (
         <Fragment>
            <div className="sub-header" style={{ marginTop: "20px" }}>


               {values.select_client || values.select_internal ?
                  <div className="row">

                     <div className="col-sm-6 form-group">
                        <label for="id_project_name" className="text-light-grey">
                           Project name<span className="text-danger">*</span>
                        </label>

                        <input type="text"
                           name="project_name"
                           label="Project Name"
                           className="fullwidth input-control"
                           // validate={required}
                           placeholder="Project Name"
                           onChange={this.props.handleChange('project_name')}
                           defaultValue={values.project_name}
                        // onChange={this.clearExistedError}
                        // existed={this.state.existed_error}
                        />
                        <span className='text-danger'>{this.props.errors&&this.props.errors.project_name}</span>

                     </div>
                     {values.select_client ?
                        <div className="col-sm-4 form-group">
                           <label for="id_client" className="text-light-grey">
                              Select Client<span className="text-danger">*</span>
                           </label>
                           {/* {console.log('zzzzzzzzzzzz',this.state.clientList[0]&&this.state.clientList[0].label)
                           } */}
                           <Select 
                              options={this.props.clientList}
                              value={this.props.selectedOption}
                              onChange={(e)=>this.props.handleclientChange(e)}

                           />
                           <span className='text-danger'>{this.props.errors&&this.props.errors.client}</span>
                        </div> : null}
                        
                     {values.select_client ?
                        <div className="col-sm-2 form-group text-allign">
                           <button style={{ width: "110px" }} onClick={this.props.updateClientModal} type="button" className="btn btn-outline-primary add-row">

                              <i className="icon-plus"></i> Add
                                       </button>
                        </div> : null}
                       

                     {values.select_client ?
                       <div className="col-sm-6 form-group">
                       <label for="id_client" className="text-light-grey">
                          Select Project Type<span className="text-danger">*</span>
                       </label>
                            {/* <input type="radio" className="individual-button" checked={values.select_hourly} name="projectinfo" onClick={this.props.select_hourly} value="Internal" />
                           <label for="internal" className="individual">Hourly Project</label>


                           <input type="radio" name="projectinfo" checked={values.select_fixed} className="individual-button" onClick={this.props.select_fixed} value="Client" />
                           <label for="client" className="individual" >Fixed Project</label> */}
                           <Select 
                              options={this.props.typeList}
                              value={this.props.selectedType}
                              onChange={(e)=>this.props.handleTypeChange(e)}

                           />
                           <span className="text-danger">{this.props.errors&&this.props.errors.selectedType}</span>
                           </div> : null} 


                     <div className="col-sm-6 form-group">
                        <label className="text-light-grey" for="id_estimated_time">Start Date</label><span className="text-danger">*</span>
                        
                        <input
                           type='date'
                           name="start_date"
                           label="Start Date"
                           className="fullwidth input-control"
                           // startingDate={this.state.startingDate}
                           //   validate={required}
                           value={values.start_date}
                           onChange={this.props.dateChange}
                           min={values.todays_date}
                        />
                         <span className='text-danger'>{this.props.errors&&this.props.errors.start_date}</span>
                        {/* <span className="text-danger">{this.state.startingDateError}</span> */}

                     </div>
                     {values.typeValue==="hourly" ?
                        <div className="col-sm-6 form-group">
                           <label className="text-light-grey" for="id_estimated_time">Estimated hours </label>
                           <input type="text"
                              name="estimated_hours"
                              className="fullwidth input-control"
                              label="Estimated Hours"
                              value={values.estimated_hours}
                              placeholder="Estimated Hours"
                              onChange={this.props.handleChange('estimated_hours')}
                           //  defaultValue={values.city}
                           />
                            <span className='text-danger'>{this.props.errors&&this.props.errors.estimated_hours}</span>
                        </div> : null}
                     {values.typeValue==="fixed" ?
                        <div className="col-sm-6 form-group">
                           <label className="text-light-grey" for="id_estimated_time">Project Cost<span className="text-danger">*</span></label>
                           <input type="text"
                              name="project_cost"
                              className="fullwidth input-control"
                              label="Project Cost"
                              value={values.project_cost}
                              placeholder="Project Cost"
                              onChange={this.props.handleChange('project_cost')}
                           
                           />
                            <span className='text-danger'>{this.props.errors&&this.props.errors.project_cost}</span>
                        </div> : null}
                     <div className="col-sm-6 form-group">
                        <label className="text-light-grey" for="id_end_date">End date{values.typeValue==="fixed" ? <span className="text-danger">*</span> : null}</label>
                        <input type="date"
                           name="end_date"
                           className="fullwidth input-control"
                           label="End Date"
                           onChange={this.props.handleChange('end_date')}
                           value={values.end_date}
                           min={values.start_date}
                        //    defaultValue="5/13/2020"
                        />
                        <span className='text-danger'>{this.props.errors&&this.props.errors.end_date}</span>
                     </div>
                     {values.typeValue==="fixed" ?
                        <div className="col-sm-3 form-group">
                           <label className="text-light-grey" for="id_estimated_time">Hourly Rate<span className="text-danger">*</span></label>
                           <input type="text"
                              name="hourly_rate"
                              className="fullwidth input-control"
                              label="Hourly Rate"
                              value={values.hourly_rate}
                              placeholder="Hourly Rate"
                              onChange={this.props.handleChange('hourly_rate')}
                           
                           />
                            <span className='text-danger'>{this.props.errors&&this.props.errors.hourly_rate}</span>
                        </div> : null}
                        {values.typeValue==="fixed" ?
                        <div className="col-sm-3 form-group">
                           <label className="text-light-grey" for="id_estimated_time">Total hours</label>
                           <input type="text"
                              name="total_hours"
                              className="fullwidth input-control"
                              label="Total Hours"
                              value={values.total_hours}
                              placeholder="Total Hours"
                              
                           
                           />
                            
                        </div> : null}
                        {values.typeValue==="fixed" ?
                        <div className="col-sm-6 form-group">
                           <label className="text-light-grey" for="id_estimated_time">Attach Contract<span className="text-danger"></span></label>
                        
                        <input type="file" className="fullwidth input-control" name="contract" onChange={(event) => this.props.upLoadAttachment(0, event,'contracts')} className="form-control" id="id_contract_formset_set-0-contract"/>

                      
                       {/* <span >{values.contractAttachments[0]&&values.contractAttachments[0].contract_name}</span> */}
                       
                          <div className="form-control-error-list">
                          

                          <span className="text-danger">{values.fixed_contract_error}</span>
                          </div>
                     </div>: null} 
                     
                     <div className="col-sm-6 form-group">
                        <label for="id_project_description" className="text-light-grey">
                           Project description<span className="text-danger">*</span>
                        </label>
                        
                        <textarea
                           name="project_description"
                           cols="40" rows="10"
                           onChange={this.props.handleChange('project_description')}
                           defaultValue={values.project_description}
                           className="input-control input-control"
                           label="Project Description"
                           placeholder="Project Description" />
                           <span className='text-danger'>{this.props.errors&&this.props.errors.project_description}</span>
                     </div>
                     <table className="table">
                        <thead>
                           <tr>
                              <th>Attachment</th>
                              <th></th>
                           </tr>
                        </thead>
                        <tbody>


                           <tr className="dynamic-form-add">
                              <td colspan="2">
                                 <a className="add-row" href="javascript:void(0)">
                                    <div className="row">
                                       <div className="col-sm-12 text-center">
                                          <button type="button"
                                             onClick={()=>this.props.addAttachment('attachments')}
                                             className="btn btn-outline-primary add-row" >
                                             <i className="icon-plus"></i> Add Attachment</button>
                                       </div>
                                    </div>
                                 </a>
                              </td>
                           </tr>


                           {values.attachments.map((obj, index) => {
                              return (
                                 <Fragment>
                                 <tr className="contract_formset_row dynamic-form">
                                    <td className="">
                                       <input type="file" name="attachments" onChange={(event) => this.props.upLoadAttachment(index, event,'attachments')} className="form-control" id="id_contract_formset_set-0-contract" />
                                       <div className="form-control-error-list">
                                       </div>
                                    </td>
                                    <td>
                                       <a className="btn btn-outline-primary delete-row" onClick={() => this.props.removeAttachment(index,'attachments')} href="javascript:void(0)"><i class=" icon-delete"></i> Remove</a>
                                    </td>
                                    
                                 </tr>
                              <span className="text-danger">{obj.error}</span>
                                 </Fragment>
                              )
                           })}


                        </tbody>
                     </table>
                  </div> : null}
               <div className="row">
               <div className="col-sm-4 form-group" style={{ textAlign: "center" }}>
                     <button type="button" style={{ width: "130px" }} onClick={this.back} className="btn btn-outline-primary add-row" >Back </button>

                  </div>
                  <div className="col-sm-4 form-group" style={{ textAlign: "center" }}>
                     <button type="button" style={{ width: "130px" }} onClick={()=>this.props.validateProjectForm('create')} className="btn btn-outline-primary add-row" >Create and close </button>

                  </div>
                  <div className="col-sm-4 form-group" style={{ textAlign: "center" }}>
                     <button type="button" style={{ width: "130px" }} className="btn btn-outline-primary add-row" onClick={this.saveAndContinue}>Save And Next </button>

                  </div>
               </div>
            </div>

            
            {values.editModal && <UpdateClientModal {...this.props}
               closeModal={this.props.closeEditModal}

            />}
         </Fragment>

      )
   }
}

export default PersonalDetails;