import React, { Component, Fragment } from 'react'
import ErrorMessage from '../../common/ErrorMessage'
import LoaderImage from '../../../static/assets/image/loader.gif';

var loaderStyle = {
    width: '3%',
    position: 'fixed',
    top: '50%',
    left: '57%'
 }
export default class DesignationDetailComponent extends Component {

    componentDidMount(){   
        this.props.RetrieveDesignation(this.props.match.params.designation_slug)
    }

    render() {
        return (
            <Fragment>
                {this.props.desigantions&&this.props.desigantions.isLoading?
                           <Fragment>
                              <div className="loader-img"> <img src={LoaderImage} style={loaderStyle} /></div>
                           </Fragment>
                           
                        :
            <Fragment>

            <div className="desg-detail fullwidth float-left bg-white p-4 sub-header mt-3">
                {this.props.show_message&&this.props.desigantions&&this.props.desigantions.deletestatus === 'failure'&&
                this.props.desigantions.designationDelete&&this.props.desigantions.designationDelete.data&&
                <ErrorMessage closeMessage={this.props.setShowMessage} msg='Cannot Delete this designation as it is Associated with an Employee'/>
                }
                <h5 className="mb-4">{this.props.desigantions && this.props.desigantions.status==='success' &&
                     this.props.desigantions.desgn&&this.props.desigantions.desgn.designation_name
            }</h5>

                <ul className="p-0 fullwidth float-left m-0">
                    <li className="col-lg-12 float-left mb-3">
                        <div>
                            <span className="b-6">ROLES</span>
                        </div>

                        
                        {this.props.desigantions && this.props.desigantions.status==='success' &&
                        this.props.desigantions.desgn&&
                        this.props.desigantions.desgn.permission_groups.length ? 
                    this.props.desigantions.desgn.permission_groups.map(
                        (i,index)=><div className="text-muted text-sm" key={index}>{i.group_name}</div>
                        ):
                        <div className="text-muted text-sm">No Roles Found</div>
                        
                    }
                

                    </li>
                </ul>
            </div>
                </Fragment>}</Fragment>
        )
    }
}