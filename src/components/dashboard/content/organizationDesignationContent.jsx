import React, { Component, Fragment } from 'react'
import RLDD from 'react-list-drag-and-drop/lib/RLDD';

import { Link } from 'react-router-dom'
import SuccessMessage from '../../common/SuccessMessage'
import LoaderImage from '../../../static/assets/image/loader.gif';
import Loader from '../../common/Loader'
var loaderStyle = {
    width: '3%',
    position: 'fixed',
    top: '50%',
    left: '57%'
 }
var searchData=[]
var msg
class OrganizationDesignationContent extends Component {
    constructor(props) {
        super(props);

        this.state = {
            item: [],
            renderOnce: true,
            updatesuccess: false,
            sortButtonClick:false,
            show_message:false
        };
    }

    componentDidMount() {
        console.log('Mounttttttttttttttttttttttttttttt')
        if(!this.props.designations){
            console.log('callleee')
            this.props.DesignationsListing()
        }
        if(this.props.location && this.props.location.state &&this.props.location.state.delete&&this.props.location.state.delete === true){
            this.setState({
                show_message:true
            })
        }
        this.setState(this.props.location.state)
        
    }
    
    componentDidUpdate(prevProps) {
        
        if (this.props.desigantions && this.props.desigantions.designation && this.props.desigantions !== prevProps.designations && this.state.renderOnce) {
            this.props.desigantions.designation.map((obj, index) => {
                obj.id = index;
                return true
            })
            let items = this.props.desigantions.designation
            console.log('did update stateeeeeeeeeee',items)
            // 

            this.setState({ item: items, renderOnce: false })
            

            if(this.props.desigantions&&this.props.desigantions.createstatus==='success'&&this.props.desigantions.designation_obj){
                msg = `Designation ${this.props.desigantions.designation_obj.designation_name} was created sucessfully`
                this.setState({updatesuccess:true,item: items, renderOnce: true})
                this.props.clearSuccessMessages()
            }
    
            if (this.props.desigantions && this.props.desigantions.updatestatus === 'success') {
                msg = `Designation ${this.props.desigantions.desgn.designation_name} was updated sucessfully`
                this.setState({updatesuccess:true,item: items, renderOnce: true })
                this.props.clearSuccessMessages()
            }

        }

        
    }

    componentWillUnmount(){
        this.props.clearDesignationSearch()
    }

    handleRLDDChange = (newItems) => {
        this.setState({ item: newItems });
    }

    CloseMsg = () => {
        this.setState({ updatesuccess: false })
    }
    closeMessage = () => {
        this.setState({ show_message: false })
    }
    designationData = (designation_list) => {
        this.setState({
            item: designation_list
        })
        return this.state.item
    }

    sortButton=()=>{

        this.setState({sortButtonClick:!this.state.sortButtonClick})

        if(this.state.sortButtonClick){  
            let sort=[]
            this.state.item.filter((i)=> sort.push(i.slug))
            this.props.sortDesignations(sort)
        }
    }


    render() {
        this.props.desigantions&&console.log('propsss',this.props.desigantions.designation)
        console.log('stateeeeeee',this.state.item)
        if(this.props.desigantions){

            if(this.props.desigantions.hasOwnProperty('search')){

                let fulldata=this.props.desigantions.designation
                searchData=fulldata.filter(
                    (designation)=>designation.designation_name.toLowerCase().indexOf(this.props.desigantions.search.toLowerCase()) !==-1)
        
            }
            else{
                
                searchData=this.props.desigantions.designation
            }
        }

        // if(this.props.desigantions&&this.props.desigantions.updatestatus==='success'){
        //     this.setState({ item: items, renderOnce: true })
        //     this.props.clearSuccessMessages()
        // }

        
        var items = []
        if (this.props.desigantions &&
            this.props.desigantions.designation) {
            this.props.desigantions.designation.map((obj, index) => {
                obj.id = index;
                return null
            })
             items = this.props.desigantions.designation
        }

        // if (this.props.location && this.props.location.state &&
        //     this.props.location.state.updatesuccess) {
        // }

        // if(this.props.desigantions&&this.props.desigantions.createstatus==='success'&&this.props.desigantions.designation_obj){
        //     var msg = `Designation ${this.props.desigantions.designation_obj.designation_name} was updated sucessfully`

        //     this.setState({updatesuccess:true})
        // }

        

        return (
            <Fragment>
                {this.props.desigantions&&this.props.desigantions.isLoading?
                           <Fragment>
                              <div className="loader-img"> <img src={LoaderImage} style={loaderStyle} /></div>
                           </Fragment>
                           
                        :
            <Fragment>

                <div className="mt-3 col-sm-12 sub-header">
                    {this.props.designation && this.props.desigantions.isLoading && <Loader />}
                    <div className="row">
                        <div className="col-sm-12">
                            <div className="bx-content shadow">
                                <div className="form-group p-4 clearfix">
                                    <span>{(this.props.desigantions && this.props.desigantions.designation) ? searchData.length : 'No'} Designations Found</span>

                                    {
                                        this.state.updatesuccess && (
                                            <SuccessMessage closemsg={this.CloseMsg} msg={msg} />)
                                    }

                                    {(this.state.show_message)&&this.props.location && this.props.location.state &&
                                        this.props.location.state.delete &&

                                        <SuccessMessage closemsg={this.closeMessage} msg='Designation Deleted Successfully' />
                                    }

                                    {this.props.desigantions && this.props.desigantions.designation && this.props.desigantions.designation.length > 1 &&
                                    <span className="float-right">
                                        <button id="enable-sorting" onClick={this.sortButton} className="btn btn-primary " href="javascript:void(0);" >
                                            <span style={{ color: 'white' }}>{!this.state.sortButtonClick?'Sort':'Click to Save the Order'}</span>
                                        </button>
                                    </span>
                                    }

                                </div>
                                {/* DESIGNATIONSSSS */}
                                {this.props.desigantions && this.props.desigantions.designation && this.props.desigantions.designation.length > 0 &&
                                <div className="scroll-wrapper table-responsive-cs" style={{ position: 'relative' }}>
                                   {this.props.desigantions&&this.props.desigantions.isLoading ? <Loader/>:
                                    <div className="table-responsive-cs scroll-content" style={{ height: 'auto', marginBottom: '-10px', marginRight: '-10px', maxHeight: '373.817px' }}>
                                        <table className="table table-cs text-grey table-text-sm table-striped table-borderless ">
                                            <thead>
                                                <tr>
                                                    <th> DESIGNATION NAME </th>
                                                </tr>
                                            </thead>
                                    
                                            <tbody id="sortable" className="ui-sortable ui-sortable-disabled "  >

                                                {(this.props.desigantions && this.props.desigantions.designation && this.props.desigantions.designation.length > 0) &&
                                                      this.state.sortButtonClick ?  
                                                    
                                                    <RLDD
                                                        items={this.state.item}
                                                        
                                                        itemRenderer={(item, index) => {
                                                            return (

                                                                    <tr id="a8df28f5-4ba4-44b5-b315-ae9223f37e2d" className="ui-sortable-handle sorting-table" style={{}}>
                                                                        <td>
                                                                            <span className="user-img-holder">{item.designation_name.charAt(0)}
                                                                                {item.designation_name.charAt(0)}</span>
                                                                            <span className="project-details item">
                                                                                <Link  to={`/departments/designation/${item.slug}`} >
                                                                                    <span className="project-details-title text-black">
                                                                                        {item.designation_name}
                                                                                    </span>
                                                                                </Link>
                                                                            </span>
                                                                        </td>
                                                                    </tr>
                                                                
                                                            );
                                                        }}
                                                        onChange={this.handleRLDDChange}
                                                    />:
                                                    
                                                    (this.props.desigantions && this.props.desigantions.designation && this.props.desigantions.designation.length > 0) &&
                                                    searchData.map((item,index)=>
                                                    
                                                        <tr key={index} id="a8df28f5-4ba4-44b5-b315-ae9223f37e2d" className="ui-sortable-handle sorting-table" style={{}}>
                                                            <td>
                                                                <span className="user-img-holder">{item.designation_name.charAt(0)}
                                                                    {item.designation_name.charAt(0)}</span>
                                                                <span className="project-details item">
                                                                    <Link  to={`/departments/designation/${item.slug}`} >
                                                                        <span className="project-details-title text-black">
                                                                            {item.designation_name} 
                                                                        </span>
                                                                    </Link>
                                                                </span>
                                                            </td>
                                                        </tr>
                                                    
                                                    )

                                                }

                                            </tbody>

                                            <tfoot>
                                                <tr>
                                                    <th colSpan={6} className="text-center p-4">
                                                    </th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                   }
                                </div>
                                   }

                            </div>
                        </div>
                    </div>
                </div>
            </Fragment>}</Fragment>
        )
    }
}

export default OrganizationDesignationContent