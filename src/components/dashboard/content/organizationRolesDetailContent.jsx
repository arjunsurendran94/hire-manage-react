import React, { Component, Fragment } from 'react'
import * as permission from '../../../constants/componentConstants';
import { isEmpty } from "lodash"
import {filteringPermissions} from '../../../constants/componentConstants'
import LoaderImage from '../../../static/assets/image/loader.gif';
var loaderStyle = {
  width: '3%',
  position: 'fixed',
  top: '50%',
  left: '57%'
}
var x = {}
class OrganizationRolesDetailContent extends Component {
  constructor(props) {
    super(props);
    this.state = {
        show_message: false,
        reRender:false,
        permission:{},
        x:{}
    };
  }

  componentDidMount() {
    
    if (this.props.roleEdited && this.props.roleEdited.editRole) {
        this.setState({ show_message: true ,reRender:true})
    }
    if (this.props.match.params) {
      this.props.retreivePermissionGroups(this.props.match.params.role_slug)         
    }
  }

  componentDidUpdate(prevProps){
    if(prevProps.roles!=this.props.roles&&this.props.roles?.allPermissions?.length>0&&
        this.props.roles?.allPermissionsStatus===200 
      ){
        
        
          let res=filteringPermissions(this.props.roles?.allPermissions)
          console.log('res',res);
          
          this.setState({permission:res})
      }
      
      // console.log('this.props.roleObject',this.props.roleObject);
      // console.log('prevProps.roleObject',prevProps.roleObject);
      // console.log('this.props.roleObject?.roleData?.permissions',this.props.roleObject?.roleData?.permissions);
      // console.log('empty this.state.permission',!isEmpty(this.state.permission));
      // console.log('isEmpty(this.state.x)',isEmpty(this.state.x));
      
      
      
    if(this.props.roleObject?.roleData?.permissions?.length>0&&
      !isEmpty(this.state.permission)&&isEmpty(this.state.x))
    {
      // console.log('mmmmmmmmmmmmmmmm',this.props.roleObject?.roleData?.permissions);
      let arr={}
      this.props.roleObject.roleData.permissions.map((permissionObj, index) => {
        
        for (var key in this.state.permission?.permissionDict) {
          
          if (this.state.permission?.permissionDict[key]?.includes(permissionObj.id)) {
            if (key in arr) {
              
              arr[key].push(permissionObj.name)
            } else {
              arr[key] = [permissionObj.name]
            }
          }
          console.log('arr',arr);
          
        }
      })
      this.setState({x:arr})
    }
  }

  clearDict = () => {
    this.setState({x:{}})
    // x = {}
  }
  rolePermissionData = (permissionObj, index) => {
    
    console.log('state x',this.state.x)
    let arr=this.state.x
    for (var key in this.state.permission?.permissionDict) {

      if (this.state.permission?.permissionDict[key]?.includes(permissionObj.id)) {
        if (key in arr) {
          
          arr[key].push(permissionObj.name)
        } else {
          arr[key] = [permissionObj.name]
        }
      }
    }
    //return true
  }

  renderPermission = (roles_dict) => {
    // console.log('roles_dict',roles_dict)
    
    let a = []
    for (var key in roles_dict) {
      a.push(
        <li className="col-xl-2 col-lg-3 float-left mb-3" key={key}>
          <span className="b-6"> {key}</span>
          {roles_dict[key].map((value, index) => {
            return (
              <div className="text-muted text-sm" key={index}>
                {value}
              </div>)
          })}
        </li>)

    }
    console.log('roles_dict',roles_dict);
    console.log('aaaaaaaaaaaaaaaaa',a);
    
    return a
  }

  closeDeleteMessage = () => {
    this.setState({
        show_message: false
    })
    window.location.reload()
  }

  render() {
    console.log('permissions',this.props.roleObject?.roleData?.permissions);
    console.log('this.state.x',this.state.x)
    console.log('state permissions',this.state.permission);
    
    
    return (
      <Fragment>
      {this.props.roleObject&&this.props.roleObject.isLoading?
        <Fragment>
           <div className="loader-img"> <img src={LoaderImage} style={loaderStyle} /></div>
        </Fragment>
        
     :
      <Fragment>
        <div className="desg-detail fullwidth float-left bg-white p-4 sub-header mt-3">
          {(this.state.show_message&&this.props.location.state&&this.props.location.state.name)&&
            <div class="alert alert-dismissible fade show alert-warning-green mt-3">
              <i class="icon-checked b-6 alert-icon mr-2"></i> Role "{this.props.location.state.name}" updated successfully
              <button onClick={this.closeDeleteMessage} type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
            </div>
          }
          <h5 className="mb-4">{(this.props.roleObject && this.props.roleObject.roleData) &&
            this.props.roleObject.roleData.group_name.toUpperCase()
          }</h5>
          <ul className="p-0 fullwidth float-left m-0">
            <li className="col-lg-2 float-left mb-3">
              <div>
                <span className="b-6">ROLE NAME</span>
              </div>
              <div className="text-muted text-sm">
                {(this.props.roleObject && this.props.roleObject.roleData) &&
                  this.props.roleObject.roleData.group_name
                }
              </div>
            </li>
            <li className="col-lg-2 float-left mb-3">
              <div>
                <span className="b-6">DEFAULT</span>
              </div>
              <div className="text-muted text-sm">
                {(this.props.roleObject && this.props.roleObject.roleData) &&
                  this.props.roleObject.roleData.default.toString().charAt(0).toUpperCase() + this.props.roleObject.roleData.default.toString().slice(1)
                }
              </div>
            </li>
          </ul>
          <h6 className="float-left fullwidth mb-3 pt-4">PERMISSION</h6>
          <ul className="p-0 fullwidth float-left m-0 mb-3 pt-4">
            {
              this.props.roleObject &&
              (this.props.roleObject.roleData &&
                (this.props.roleObject.roleData.permissions &&
                  (this.props.roleObject.roleData.permissions.length > 0 ?
                    (
                      // (isEmpty(x)|| this.state.reRender) && 
                     
                      //   console.log('ccc',x),                        
                      this.state.x && this.renderPermission(this.state.x)
                      )
                    :
                    <li className="col-xl-2 col-lg-3 float-left mb-3">
                      <div className="text-muted text-sm">
                        No permission selected
                              </div>
                    </li>
                  )
                )
              )
            }


          </ul>
        </div>
      </Fragment>}</Fragment>
    )
  }
}

export default OrganizationRolesDetailContent