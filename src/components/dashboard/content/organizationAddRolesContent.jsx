import React, { Component, Fragment } from 'react'
import { Field, reduxForm } from 'redux-form'
import * as permission from '../../../constants/componentConstants';
import { Redirect } from 'react-router-dom';
import {alphabetOnly} from '../../../constants/validations'
import {filteringPermissions} from '../../../constants/componentConstants'
import { isEmpty } from "lodash"



const required = value => {
    return (value || typeof value === 'number' ? undefined : ' is required')
  }
const maxLength = max => value =>
  value && value.length > max ? `Must be ${max} characters or less` : undefined
const maxLength128 = maxLength(128)
  
  const renderField = ({input,label,type,existed,meta: { touched, error, warning, }}) => {
    return (
      <Fragment>
        <input {...input} placeholder={label} type={type} className={(touched && error) || (existed) ? 'input-control input-control border border-danger' : 'input-control input-control'} />
        {touched &&
          ((error && <small className="text-danger">{label}{error}</small>) ||
            (warning && <span>{warning}</span>))}
        
        {(existed) && <small className="text-danger">
                    This Role name Already exists</small>
        }
      </Fragment>
    )
  }
const Checkbox = ({ input,className, meta: { touched, error, warning } }) => (
    <Fragment>
            <input type="checkbox" className={className} {...input} />
        {/* {touched &&
            ((error && <small className="text-danger" style={{fontSize:"11px"}}>{error}</small>) ||
                (warning && <span>{warning}</span>))} */}
    </Fragment>
)

const checkboxField = ({ input,options,className,type, checked,meta: { touched, error, warning } }) => (
    <Fragment>    
        {options?.map((option, index) => (
            <div className="" key={index}>
                <label htmlFor={option.id}>
                    <span className="checkbox">
                        {/* <input id="delete_history" name="permissions" type="checkbox" value="46"/> */}
                        {/* <input id={option.id} type="checkbox" className={className} value={option.value} {...input} />     */}
                        <input type="checkbox" id={option.id} className={className} name={`${input.name}[${index}]`} value={option.value} checked={input.value.indexOf(option.value) !== -1}
                            onChange={(event) => {
                                const newValue = [...input.value];
                                if (event.target.checked) {
                                    newValue.push(option.value);
                                } else {
                                    newValue.splice(newValue.indexOf(option.value), 1);
                                }
                                return input.onChange(newValue);
                            }}/>
                    </span>
                    {option.name}
                </label>
            </div>
            ))}
        
        <label>{touched &&
            ((error && <small className="text-danger">{error}</small>) ||
                (warning && <span>{warning}</span>))}</label>
    </Fragment>
)


const editCheckboxField = ({ input,options,className,makeCheck,edit,type, checked,meta: { touched, error, warning } }) => {
    
    
    return(
    <Fragment>
        
        {options?.map((option, index) => (
            <div className="" key={index}>
                <label htmlFor={option.id}>
                    <span className="checkbox">
                        <input type="checkbox" id={option.id} className={className} name={`${input.name}[${index}]`} value={option.value} checked={makeCheck.length>0?(makeCheck.indexOf(option.value) !== -1):(input.value.indexOf(option.value) !== -1)}
                            onChange={(event) => {
                                
                                if(makeCheck.length>0){
                                    const newValue = [...makeCheck]
                                    if (event.target.checked) {
                                        newValue.push(option.value);
                                        makeCheck.push(option.value);
                                    } else {
                                        newValue.splice(newValue.indexOf(option.value), 1);
                                        makeCheck.splice(makeCheck.indexOf(option.value), 1);
                                    }
                                    return input.onChange(newValue);
                                }else{
                                    const newValue = [...input.value];
                                    if (event.target.checked) {
                                        newValue.push(option.value);
                                        // makeCheck.push(option.value);
                                    } 
                                    else {
                                        newValue.splice(newValue.indexOf(option.value), 1);
                                    }
                                    return input.onChange(newValue);

                                }
                                // const newValue = makeCheck.length>0 ? ([...makeCheck]) :([...input.value])
                                // if (event.target.checked) {
                                //     newValue.push(option.value);
                                //     makeCheck.push(option.value);
                                // } else {
                                //     newValue.splice(newValue.indexOf(option.value), 1);
                                //     makeCheck.splice(makeCheck.indexOf(option.value), 1);
                                // }
                                // return input.onChange(newValue);
                            }}/>
                    </span>
                    {option.name}
                </label>
            </div>
            ))}
        
        <label>{touched &&
            ((error && <small className="text-danger">{error}</small>) ||
                (warning && <span>{warning}</span>))}</label>
    </Fragment>
)}

console.log('ssssssssssssssssssssssssssssssss');

// var rolePermissionGroups = {}

class OrganizationAddRolesContent extends Component {

    constructor(props) {
        super(props);
        this.state = { 
            toggleAlert: false ,
            redirectToDetail:false,
            existed_error:'',
            renderOnce:true,
            permission:{},
            rolePermissionGroups:{}
        };
      }


    componentDidMount(){
        
    }

    ischecked=(type,value)=>{
    
    
    if(!isEmpty(this.state.rolePermissionGroups)){
        
    return this.state.rolePermissionGroups[type]?.includes(value)
    }}

    handleChange=(e,type)=>{
        
        let curState={...this.state.rolePermissionGroups}
        let res=[]
        if(e.target.checked){
            if(!(type in curState)){
                curState[type]=[]
            }
            curState[type].push(parseInt(e.target.value))
            // res=curState
            this.setState({rolePermissionGroups:curState})
        }else{
            res=curState[type].filter(i=>i!=e.target.value)
            curState[type]=res
            this.setState({rolePermissionGroups:curState})
        }
    }

    submit = (values) => {
        var permissions = []
        var existed_permission = []
        var new_permissions = []
        if(this.props.edit){
            
            
            
            let final_values = {}
            let ab1=this.state.permission?.permissionList.map((permission, index) => {
                if(permission in values){
                    new_permissions = new_permissions.concat(values[permission])
                }
                return true
            })
            
            for(var key in this.state.rolePermissionGroups){
                this.state.rolePermissionGroups[key].map((value,index)=>{
                    existed_permission.push(value)
                    return true
                })
            }
            permissions = existed_permission.concat(new_permissions.filter((item) => existed_permission.indexOf(item) < 0))
            final_values.organization = localStorage.getItem("organization_slug")|| null
            final_values.group_name = values.group_name
            final_values.default = values.default
            final_values.permissions = permissions
            this.props.editRole(final_values,this.props.match.params.role_slug)
            this.setState({redirectToDetail:true})
        }else{
            
            this.setState({toggleAlert:true})
            values.organization = localStorage.getItem("organization_slug")||null
            var check = "default" in values
            
            var role_value = {}
            if(!check){
                values.default = false
            }

            for(var key in this.state.rolePermissionGroups){
                this.state.rolePermissionGroups[key].map((value,index)=>{
                    existed_permission.push(value)
                    return true
                })
            }

            role_value.organization=localStorage.getItem("organization_slug")||null
            role_value.group_name = values.group_name
            role_value.permissions = existed_permission
            role_value.default = values.default
            this.props.createPermissionGroups(role_value)
            window.scrollTo(0, 0)
        }
    }

    closeAlertMessage = () => {
        this.setState({toggleAlert:false})
    }

    roleExistedError = () =>{
        this.setState({existed_error:''})
    }

    componentDidUpdate(prevProps){
        // existed case for create
        if(this.props.rolesCreated!=prevProps.rolesCreated){
            
            this.props.rolesCreated&&this.props.rolesCreated.createdRolesError&&this.props.rolesCreated.createdRolesError.data&&this.props.rolesCreated.createdRolesError.data.non_field_errors&&
            this.props.rolesCreated.createdRolesError.data.non_field_errors[0]==="The fields organization, group_name must make a unique set."&&
            this.setState({existed_error:'Role Name Already exists'})
        }
        // existed case for edit
        if(this.props.edit&&this.props.roleEdited!=prevProps.roleEdited){
            this.props.roleEdited&&this.props.roleEdited.editRoleError&&this.props.roleEdited.editRoleError.data&&this.props.roleEdited.editRoleError.data.non_field_errors&&
            this.props.roleEdited.editRoleError.data.non_field_errors[0]==='The fields organization, group_name must make a unique set.'&&
            this.setState({existed_error:'Role Name Already exists'})
        }

        
        
        
        if(isEmpty(this.state.permission)&&prevProps.roles!=this.props.roles&&this.props.roles?.allPermissions?.length>0&&
            this.props.roles?.allPermissionsStatus===200 
          ){

              let res=filteringPermissions(this.props.roles?.allPermissions)
              console.log('res',res)
              this.setState({permission:res})
          }

        // Intializing the form
        if(this.props.edit&&
            // this.props.roleObject!=prevProps.roleObject&&
            this.props.roleObject.roleData
            // &&this.state.renderOnce
            ){

            
            

            let data={};
            data.group_name=this.props.roleObject&&this.props.roleObject.roleData&&this.props.roleObject.roleData.group_name;
            data.default=this.props.roleObject&&this.props.roleObject.roleData&&this.props.roleObject.roleData.default;
            
            
            console.log('this.props.roleObject?.roleData?.permissions',this.props.roleObject?.roleData?.permissions)
            // console.log('isEmpty(rolePermissionGroups)',isEmpty(rolePermissionGroups))
            console.log('!isEmpty(this.state.permission)',!isEmpty(this.state.permission));
            
            
            

            if(this.props.roleObject?.roleData?.permissions?.length > 0 
                && isEmpty(this.state.rolePermissionGroups)&&!isEmpty(this.state.permission)
            ){
                
                let e=isEmpty(this.state.permission)

                if(!e){
                    
                let s=this.props.roleObject.roleData.permissions.map(
                    (permissionObj, index) => 
                                            this.rolePermissionData(permissionObj, index)
                                        )
                //   x && renderPermission(x))

                }
                // console.log('rolePermissionGroups',rolePermissionGroups)
                
                // this.setState({rolePermissionGroups:rolePermissionGroups})
            }
            
            
            this.props.edit && this.props.initialize(data);
            // this.setState({renderOnce:false})
        } 

        
        if(this.props.rolesCreated?.createdRolesSucessStatus===201){
            console.log('tureeeeeeeeee');
            window.location.replace('/permission-groups/')
        }

        
    }

    rolePermissionData = (permissionObj, index) => {
        
        console.log('permissionDict',this?.state?.permission);

        let rolePermissionGroups=this.state.rolePermissionGroups

        for (var key in this.state?.permission?.permissionDict) {
            
            console.log('permissionObj.id',permissionObj.id);
            
            if (this?.state?.permission?.permissionDict[key].includes(permissionObj.id)) {
            console.log('permissionObj',permissionObj);
            
            if (key in rolePermissionGroups) {
                rolePermissionGroups[key].push(permissionObj.id)
            }

            else {
                rolePermissionGroups[key] = [permissionObj.id]
            }
            
          }
        //   else{
        //         rolePermissionGroups[key] = []
        //     }
        }
        // console.log('22222222',rolePermissionGroups);
        this.setState({rolePermissionGroups:rolePermissionGroups})
      }

    render() {
        
        // console.log('this.state.rolePermissionGroups',this.state.rolePermissionGroups)
        

        const { handleSubmit} = this.props
        if(this.props.edit&&this.state.redirectToDetail&&this.props.roleEdited&&this.props.roleEdited.editRolesSucessStatus&&this.props.roleEdited.editRolesSucessStatus === 200){
            // return <Redirect
            //       to={{
            //         pathname: `/permission-detail/${this.props.match.params.role_slug}`,
            //         state: { name: this.props.roleEdited.editRole.group_name }
            //       }}
            // />
            window.location.replace(`/permission-detail/${this.props?.match?.params?.role_slug}`);
        }
        return (
            <Fragment>
                <div className="col-md-10 offset-md-1 card card-signin mt-3 p-5">
                    {((this.state.toggleAlert)&&this.props.rolesCreated && this.props.rolesCreated.createdRolesSucessStatus)&&
                        <div className="alert alert-dismissible fade show alert-warning-green mt-3">
                            <i className="icon-checked b-6 alert-icon mr-2"></i> Role "{this.props.rolesCreated.createdRoles.group_name}" created successfully
                            <button onClick={this.closeAlertMessage} type="button" className="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                            </button>
                        </div>
                    }

                    <div className="text-left  mb-5">
                        <h2>{this.props.edit ? 'Update ' : 'Add New'}Role</h2>
                    </div>
                    <form onSubmit={handleSubmit(this.submit)}>
                        <input type="hidden" name="csrfmiddlewaretoken" value="uMPteQcBBovDfp6TQHP2x2bFUiVHNTtZDYo1VYU07RPbgkplMt52Dyq75xRBkyUN"/>

                            <div className="form-group row">
                                <div className="col-md-3">
                                    <label className="form-control-label" htmlFor="id_group_name">
                                        <label htmlFor="id_group_name">Role Name:</label> <span className="text-danger ">*</span> </label>
                                </div>
                                <div className="col-md-9">
                                    {/* <input type="text" name="group_name" placeholder="Role Name" class="form-control input-control" maxlength="128" required="" id="id_group_name"/> */}
                                    <Field type="text" name="group_name" component={renderField} validate={[required, maxLength128,alphabetOnly]} onChange={this.roleExistedError} existed={this.state.existed_error} label="Role Name" id="id_group_name" />
                            
                                </div>
                                </div>

                                <div className="form-group row">
                                    <div className="col-md-3">
                                        <label className="form-control-label" htmlFor="id_default">
                                            <label htmlFor="id_default">Default:</label></label>
                                    </div>
                                    <div className="col-md-9">
                                        <Field name="default" id="default" component={Checkbox} type="checkbox"/>                
                                    </div>
                                    </div>
                                    <div className="row mt-5">
                                        <h3 className="col-md-12 mb-0">Permissions </h3>
                                        <div className="col-lg-4 col-md-6 mt-3">
                                            <h5>User</h5>
                                            
                                            {/* <Field
                                                type="checkbox"
                                                name="user_permissions"
                                                component={this.props.edit?editCheckboxField:checkboxField}
                                                options = {this.state.permission?.USER}
                                                makeCheck = {"USER" in this.state.rolePermissionGroups ? this.state.rolePermissionGroups.USER : []}
                                                /> */}

                                            {this.state?.permission['USER']?.filter(perm=>perm.model==='user')
                                            .map((perm)=>(<Fragment><input value={perm.value} onChange={(e)=>this.handleChange(e,'USER')}
                                            checked={this.ischecked('USER',perm.value)}

                                            type="checkbox" id={perm.id}/>
                                                {perm.name}</Fragment>))}


                                        </div>
                                        {/* <div className="col-lg-4 col-md-6 mt-3">
                                            <h5>History</h5>
                                            <Field
                                                type="checkbox"
                                                name="history_permissions"
                                                component={this.props.edit?editCheckboxField:checkboxField}
                                                options = {permission.HISTORY}
                                                makeCheck = {"HISTORY" in rolePermissionGroups ? rolePermissionGroups.HISTORY : []}
                                                />
                                        </div> */}

                                    <div className="col-lg-4 col-md-6 mt-3">
                                        <h5>Branch</h5>


                                        {/* <Field
                                            type="checkbox"
                                            name="branch_permissions"
                                            component={this.props.edit?editCheckboxField:checkboxField}
                                            options = {this.state.permission?.BRANCH}
                                            makeCheck = {"BRANCH" in this.state.rolePermissionGroups ? this.state.rolePermissionGroups.BRANCH : []}
                                            /> */}

                                        {this.state?.permission['BRANCH']?.filter(perm=>perm.model==='branch')
                                            .map((perm)=>(
                                            <div ><input value={perm.value} checked={this.ischecked('BRANCH',perm.value)} onChange={(e)=>this.handleChange(e,'BRANCH')} type="checkbox" id={perm.id}/>
                                                {perm.name}</div>
                                                ))}

                                    </div>

                                    <div className="col-lg-4 col-md-6 mt-3">
                                        <h5>Department</h5>
                                        {/* <Field
                                            type="checkbox"
                                            name="department_permissions"
                                            component={this.props.edit?editCheckboxField:checkboxField}
                                            options = {this.state.permission?.DEPARTMENT}
                                            makeCheck = {"DEPARTMENT" in this.state.rolePermissionGroups ? this.state.rolePermissionGroups.DEPARTMENT : []}
                                            /> */}
                                        {this.state?.permission['DEPARTMENT']?.filter(perm=>perm.model==='department')
                                            .map((perm)=>(
                                            <div ><input value={perm.value} onChange={(e)=>this.handleChange(e,'DEPARTMENT')} checked={this.ischecked('DEPARTMENT',perm.value)} type="checkbox" id={perm.id}/>
                                                {perm.name}</div>
                                                ))}
                                    </div>

                                    <div className="col-lg-4 col-md-6 mt-3">
                                        <h5>Designation</h5>
                                        {/* <Field
                                            type="checkbox"
                                            name="designation_permissions"
                                            component={this.props.edit?editCheckboxField:checkboxField}
                                            options = {this.state.permission?.DESIGNATION}
                                            makeCheck = {"DESIGNATION" in this.state.rolePermissionGroups ? this.state.rolePermissionGroups.DESIGNATION : []}
                                            /> */}
                                        {this.state?.permission['DESIGNATION']?.filter(perm=>perm.model==='designation')
                                            .map((perm)=>(
                                            <div ><input value={perm.value} onChange={(e)=>this.handleChange(e,'DESIGNATION')} checked={this.ischecked('DESIGNATION',perm.value)} type="checkbox" id={perm.id}/>
                                                {perm.name}</div>
                                                ))}
                                    </div>

                                    <div className="col-lg-4 col-md-6 mt-3">
                                        <h5>Employee</h5>
                                        {/* <Field
                                            type="checkbox"
                                            name="employee_permissions"
                                            component={this.props.edit?editCheckboxField:checkboxField}
                                            options = {this.state.permission?.EMPLOYEE}
                                            makeCheck = {"EMPLOYEE" in this.state.rolePermissionGroups ? this.state.rolePermissionGroups.EMPLOYEE : []}
                                            /> */}
                                        {this.state?.permission['EMPLOYEE']?.filter(perm=>perm.model==='employee')
                                            .map((perm)=>(
                                            <div ><input value={perm.value} onChange={(e)=>this.handleChange(e,'EMPLOYEE')} checked={this.ischecked('EMPLOYEE',perm.value)} type="checkbox" id={perm.id}/>
                                                {perm.name}</div>
                                                ))}
                                    </div>

                                    <div className="col-lg-4 col-md-6 mt-3">
                                        <h5>Client</h5>
                                        {/* <Field
                                            type="checkbox"
                                            name="client_permissions"
                                            component={this.props.edit?editCheckboxField:checkboxField}
                                            options = {this.state.permission?.CLIENT}
                                            makeCheck = {"CLIENT" in this.state.rolePermissionGroups ? this.state.rolePermissionGroups.CLIENT : []}
                                            /> */}
                                            {this.state?.permission['CLIENT']?.filter(perm=>perm.model==='client')
                                            .map((perm)=>(
                                            <div ><input value={perm.value} onChange={(e)=>this.handleChange(e,'CLIENT')} checked={this.ischecked('CLIENT',perm.value)} type="checkbox" id={perm.id}/>
                                                {perm.name}</div>
                                                ))}
                                    </div>

                                    {/* <div className="col-lg-4 col-md-6 mt-3">
                                        <h5>Comment</h5>
                                        <Field
                                            type="checkbox"
                                            name="comment_permissions"
                                            component={this.props.edit?editCheckboxField:checkboxField}
                                            options = {permission.COMMENT}
                                            makeCheck = {"COMMENT" in rolePermissionGroups ? rolePermissionGroups.COMMENT : []}
                                            />
                                    </div> */}

                                    {/* <div className="col-lg-4 col-md-6 mt-3">
                                        <h5>Milestone</h5>
                                        <Field
                                            type="checkbox"
                                            name="milestone_permissions"
                                            component={this.props.edit?editCheckboxField:checkboxField}
                                            options = {permission.MILESTONE}
                                            makeCheck = {"MILESTONE" in rolePermissionGroups ? rolePermissionGroups.MILESTONE : []}
                                            />
                                    </div> */}

                                    <div className="col-lg-4 col-md-6 mt-3">
                                        <h5>Project</h5>
                                        {/* <Field
                                            type="checkbox"
                                            name="project_permissions"
                                            component={this.props.edit?editCheckboxField:checkboxField}
                                            options = {this.state.permission?.PROJECT}
                                            makeCheck = {"PROJECT" in this.state.rolePermissionGroups ? this.state.rolePermissionGroups.PROJECT : []}
                                            /> */}
                                        {this.state?.permission['PROJECT']?.filter(perm=>perm.model==='project')
                                            .map((perm)=>(
                                            <div ><input value={perm.value} onChange={(e)=>this.handleChange(e,'PROJECT')} checked={this.ischecked('PROJECT',perm.value)} type="checkbox" id={perm.id}/>
                                                {perm.name}</div>
                                                ))}
                                    </div>

                                    <div className="col-lg-4 col-md-6 mt-3">
                                        <h5>Task</h5>
                                        {/* <Field
                                            type="checkbox"
                                            name="task_permissions"
                                            component={this.props.edit?editCheckboxField:checkboxField}
                                            options = {this.state.permission?.TASK}
                                            makeCheck = {"TASK" in this.state.rolePermissionGroups ? this.state.rolePermissionGroups.TASK : []}
                                            /> */}
                                        {this.state?.permission['TASK']?.filter(perm=>perm.model==='task')
                                            .map((perm)=>(
                                            <div ><input value={perm.value} onChange={(e)=>this.handleChange(e,'TASK')} checked={this.ischecked('TASK',perm.value)} type="checkbox" id={perm.id}/>
                                                {perm.name}</div>
                                                ))}
                                    </div>

                                    <div className="col-lg-4 col-md-6 mt-3">
                                        <h5>Worksheet</h5>
                                        {/* <Field
                                            type="checkbox"
                                            name="worksheet_permissions"
                                            component={this.props.edit?editCheckboxField:checkboxField}
                                            options = {this.state.permission?.WORKSHEET}
                                            makeCheck = {"WORKSHEET" in this.state.rolePermissionGroups ? this.state.rolePermissionGroups.WORKSHEET : []}
                                            /> */}
                                        {this.state?.permission['WORKSHEET']?.filter(perm=>perm.model==='worksheet')
                                            .map((perm)=>(
                                            <div ><input value={perm.value} onChange={(e)=>this.handleChange(e,'WORKSHEET')} checked={this.ischecked('WORKSHEET',perm.value)} type="checkbox" id={perm.id}/>
                                                {perm.name}</div>
                                                ))}
                                    </div>
                                </div>
                                <div className="m-5">
                                    <input type="submit" className="btn width btn-primary float-right" value="SAVE"/>
                                </div>
                            </form>
                </div>
            </Fragment>
                )
            }
        }
 
        OrganizationAddRolesContent = reduxForm({
    form:'addrolesform'
  })(OrganizationAddRolesContent)
export default OrganizationAddRolesContent