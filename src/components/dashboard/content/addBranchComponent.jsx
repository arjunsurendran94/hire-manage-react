import React, {Component, Fragment} from 'react'
import { Field, reduxForm } from 'redux-form'
import {countryReorder} from '../../../countryReorder'
import {alphabetOnly} from '../../../constants/validations'


const required = value => {
    return (value || typeof value === 'number' ? undefined : ' is Required')
  }
  
  const renderField = ({
    input,
    label,
    type,
    existed,
    meta: { touched, error, warning,pristine }
  }) => {

    return (
      
  
      <Fragment>
        <input {...input} placeholder={label} type={type} className={(touched && error) || (existed === 'The fields organization, branch_name must make a unique set.') ? 'input-control input-control border border-danger' : 'input-control input-control'} />
        
        {touched &&
          ((error && <small className="text-danger">{label}{error}</small>) ||
            (warning && <span>{warning}</span>))}
            
        {/* frommmmmmm herreeeeeeeeee */}
        { (touched) && (existed === 'The fields organization, branch_name must make a unique set.'
          ) && <small class="text-danger">
          Branch Name already exists
        </small>

      }
  
      </Fragment>
  
    )
  }

  const renderFieldSelect = ({
    input,
    label,
    country,
    countryState,
    city,
    className,
    meta: { touched, error, warning, }
  }) => {
    return (
  
  
      <Fragment>
        <select {...input} className={(touched && error)? className+' border border-danger': className}>
          <option value="">{label}</option>
          {(country && label === "Country") &&
            <Fragment>
              {country.countryList.map((plan, index) => (
                <option key={index} value={plan.id}>{plan.name_ascii}</option>
              ))}</Fragment>
          }
          {(countryState && label === "State") &&
            <Fragment>
              {countryState.stateList.map((plan, index) => (
                <option key={index} value={plan.id}>{plan.name_ascii}</option>
              ))}</Fragment>
          }
          {(city && label === "City") &&
            <Fragment>
              {city.cityList.map((plan, index) => (
                <option key={index} value={plan.id}>{plan.name_ascii}</option>
              ))}</Fragment>
          }
        </select>
        <div>
          {touched &&
            ((error && <small className="text-danger">{label}{error}</small>) ||
              (warning && <span>{warning}</span>))}
        </div>
        {/* {((existed)&&(existed.error.status == 500)) && <small class="text-danger">
            {existed.errors.error}
        </small>} */}
  
      </Fragment>
  
    )
  }


class AddBranchContent extends Component{
    state = {
        success_message: false,
        existed_error:''
      };

    componentDidMount() {
        this.props.fetchCountryList();
      }



    branchExistedError =() =>{
      this.setState({existed_error:''})
    }

    componentDidUpdate(prevProps){


      
      if((prevProps.branchCreatedData!=this.props.branchCreatedData)){


        this.props.branchCreatedData.status==='failure'&&
        this.props.branchCreatedData&&this.props.branchCreatedData.branchCreateError&&
        this.props.branchCreatedData.branchCreateError.data&&this.props.branchCreatedData.branchCreateError.data.non_field_errors&&
        this.props.branchCreatedData.branchCreateError.data.non_field_errors[0]==='The fields organization, branch_name must make a unique set.'&&
        
        this.setState({
          existed_error:this.props.branchCreatedData.branchCreateError.data.non_field_errors[0]
        })
      }
      
      
    }




    handleCountryChange = (e) => {

        // making fields null
        this.props.change('state','')  
        this.props.change('city','')

        this.props.fetchStatesList(e.target.value);
        this.props.fetchCityList("");
  
    }
    handleStateChange = (e) => {

        // making fields null
        this.props.change('city','')
        
        this.props.fetchCityList(e.target.value);
    }
    closeSuccessMessage = ()=>{
        this.setState({
            success_message:false
        })
    }
    submit = (values) => {
        this.setState({
            success_message:true
            
        })
        values.organization = localStorage.getItem("organization_slug")
        this.props.createBranch(values)
    
    }
    render(){


        const { handleSubmit} = this.props;

        return (
            <Fragment>
                <div className="col-md-10 offset-md-1 card card-signin mt-3 p-5">
                    {(this.state.success_message&&this.props.branchCreatedData&&this.props.branchCreatedData.branchName)&&
                    <div className="alert alert-dismissible fade show alert-warning-green mt-3">
                        <i className="icon-checked b-6 alert-icon mr-2"></i> Branch {this.props.branchCreatedData.branchName} was created sucessfully
                        <button onClick={this.closeSuccessMessage} type="button" className="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    }
                    <div className="text-left  mb-5">
                        <h2>Add Branch</h2>
                    </div>

                    <form onSubmit={handleSubmit(this.submit)} >
                        <input type="hidden" name="csrfmiddlewaretoken" value="pBcXpeB1T435IowHktd7UsjGCsjG2lk071VZTMI8Cm0tx8VdYAURUapvjUA8htRu"/>

                        <div className="form-group row">
                            <div className="col-md-3">
                                <label className="form-control-label" htmlFor="id_branch_name">
                                    <label htmlFor="id_branch_name">Branch Name</label>
                                </label>
                                <span className="text-danger">*</span>
                            </div>
                            <div className="col-md-9">
                               <Field type="text" 
                               name="branch_name" 
                               component={renderField} 
                               existed={this.state.existed_error}
                               validate={[required,alphabetOnly]} 
                               onChange={this.branchExistedError}
                               label="Branch Name" 
                               id="id_branch_name" />
                            </div>
                        </div>

                        <div className="form-group row">
                            <div className="col-md-3">
                                <label className="form-control-label" htmlFor="id_country">
                                    <label htmlFor="id_country">Country</label>
                                </label>
                                <span className="text-danger">*</span>
                            </div>
                            <div className="col-md-9">
                                <Field
                                  name="country"
                                  type="select"
                                  country={countryReorder(this.props.countryDetails)}
                                  component={renderFieldSelect}
                                  className="input-control select-country"
                                  label="Country"
                                  validate={required}
                                  onChange={this.handleCountryChange} />
                            </div>
                        </div>

                        <div className="form-group row">
                            <div className="col-md-3">
                                <label className="form-control-label" htmlFor="id_state">
                                    <label htmlFor="id_state">State</label>
                                </label>
                                <span className="text-danger">*</span>
                            </div>
                            <div className="col-md-9">
                                <Field
                                  name="state"
                                  type="select"
                                  countryState={this.props.stateDetails}
                                  component={renderFieldSelect}
                                  className="input-control select-state"
                                  label="State"
                                  validate={required}
                                  onChange={this.handleStateChange}
                                />
                            </div>
                        </div>

                        <div className="form-group row">
                            <div className="col-md-3">
                                <label className="form-control-label" htmlFor="id_city">
                                    <label htmlFor="id_city">City</label>
                                </label>
                                <span className="text-danger">*</span>
                            </div>
                            <div className="col-md-9">
                                <Field
                                  name="city"
                                  type="select"
                                  city={this.props.cityDetails}
                                  component={renderFieldSelect}
                                  className="input-control select-city"
                                  label="City"
                                  validate={required} />
                            </div>
                        </div>

                        <div className="form-group row">
                            <div className="col-md-3">
                                <label className="form-control-label" htmlFor="id_address">
                                    <label htmlFor="id_address">Address</label>
                                </label>
                            </div>
                            <div className="col-md-9">
                                <Field name="address" type="text"  placeholder="Address" className="input-control input-control" component="textarea" cols="40" rows="10" label=" Address" id="id_address" />
                            </div>
                        </div>

                        <div className="m-5">
                            <input type="submit" className="btn width btn-primary float-right" value="SAVE"/>
                        </div>
                    </form>
                </div>
            </Fragment>
        )
    }
}

AddBranchContent = reduxForm({
    form:'addBranchContentform'
  })(AddBranchContent)
export default AddBranchContent