import React, {Component, Fragment} from 'react'
import BranchDetailComponent from '../common/branchDetailComponent'
import { Link } from 'react-router-dom'

// import SideBar from '../common/sideBar'
// import DashBoardHeader from '../common/dashboardHeader'
// import DashBoardSubheader from '../common/dashboardSubHeader'

import Loader from '../../common/Loader'



class OrganziationContent extends Component{
    state = {
        branch_name:"",
        branch_slug:"",
        index:"org",
        departments_list:[]
    }

    componentDidMount(){
        this.props.retrieveOrganization();
        this.props.retrieveBranches(localStorage.getItem('organization_slug'),localStorage.getItem('token'))
        if(this.props.departmentlist&&this.props.departmentlist.organization_department.length<=0){
            this.props.retrieveDepartments('');
        }  
        if(this.props.departmentlist.organization_department.length >0 ){
            this.getOrganizationDepts(null,'','org')
    }
    }
    componentDidUpdate(prevProps){
        if(this.props.departmentlist&&this.props.departmentlist.organization_department !==prevProps.departmentlist.organization_department &&this.props.departmentlist.organization_department.length >0 ){
                this.getOrganizationDepts(null,'','org')
        }
    }

    getOrganizationDepts(branch_slug,branch_name,index){
        this.setState({
            branch_name:branch_name,
            branch_slug:branch_slug,
            index:index,
            departments_list:this.props.departmentlist.organization_department

        })
    }
    getBranchDepts=(branch_slug,branch_name,index)=>{
        this.setState({
            branch_name:branch_name,
            branch_slug:branch_slug,
            index:index,
            departments_list:(this.props.departmentlist&&this.props.departmentlist.connections&&branch_slug in this.props.departmentlist.connections)&&this.props.departmentlist.branch_department
        })
    }

    render(){
        if((this.props.department && this.props.department.status==='success')||
        (this.props.branchCreatedData&&this.props.branchCreatedData.status==='success')){
            window.location.reload()
            
        }
        return (
            <Fragment>
            {(this.props.organization && this.props.organization.isLoading) ?
                <Loader/>:

           
        <div className="col-sm-12 mt-3 sub-header">
            <div className="row mt-3">
            <div className="col-md-4 p-0">
                <div className="org-bx">
                <h4>Organization</h4>
                <div onClick={() => this.getOrganizationDepts(null,'','org')} className="nav d-block" id="nav-tab" role="tablist">
                    <Link className={this.state.index==='org' ? "braches branch-slug d-flex active":"braches branch-slug d-flex" }>
                    <div className="d-inline-block ">
                        <div className="branches-ic">
                        {(this.props.organization&&this.props.organization.organization_data&&this.props.organization.organization_data.logo)?
                        <img alt="" src={this.props.organization.organization_data.logo} style={{width:"65px", height:"65px", borderRadius: "50%"}}/>
                        :
                        <img alt="" src={require("../../../static/assets/image/loc-ic.png") } /*style={{width:"65px", height:"65px", borderRadius: "50%"}}*//>
                        }
                        </div>
                    </div>
                    <div className="d-inline-block braches-txt">
                        <h4>
                        {(this.props.organization && this.props.organization.organization_data) &&
                         this.props.organization.organization_data.organization_name
                        }
                        </h4>
                        {(this.props.departmentlist&&(this.props.departmentlist.organization_department.length>0))
                        ?<p>{this.props.departmentlist.organization_department.length} Departments</p>:<p>No Departments</p> }

                        
                        <h5><img src={require("../../../static/assets/image/b-loc-ic.png") } alt="Department logo" />
                        {(this.props.organization && this.props.organization.organization_data) &&
                         this.props.organization.organization_data.city.name_ascii
                        }
                        
                        </h5>
                        

                    </div>
                    </Link>
                </div>
                
                <h4>Branches</h4>

                {(this.props.branches && this.props.branches.data&&this.props.branches.data.length>0) ? 
                    this.props.branches.data.map((branch,index)=>(
                    <BranchDetailComponent key={index} index={index}branch={branch} getBranchDepts = {this.getBranchDepts}indexValue={this.state.index} {...this.props}/>)
                    ):
                
                    <div className="no-branch">
                        <div className="i-icon">i</div>
                        <p>There is no Branch for this Organization</p>
                    </div>
                }
                </div>

            </div>

            <div className="col-md-4 p-0">

                <div className="org-bx tab-content " id="myTabContent">
                <h4>Departments</h4>

                {(this.state.departments_list&&this.state.departments_list.length>0)?
                this.state.departments_list.map((department,index) =>
                ((department.branch === this.state.branch_slug)&&
                <div key={index} id="brach1" className="tab-pane fade show active" role="tabpanel" aria-labelledby="brach1">
                    <div className="nav d-block" id="nav-tab">
                    <div className="positions  department-after">
                        <div className="d-inline-block member-ic">
                        {department.department_name.charAt(0)}
                        </div>
                        <div className="d-inline-block positions-name">
                        {department.department_name}
                        </div>
                    </div>
                    </div>
                </div>
                )):
                <div className="no-branch">
                    <div className="i-icon">i</div>
                    <p>There is no Department for {this.state.branch_name ? this.state.branch_name : 'this organization' }</p>
                </div>
                }

                </div>
            </div>

            </div>
            </div>
            }
                        
        </Fragment>
        )
    }
}

export default OrganziationContent