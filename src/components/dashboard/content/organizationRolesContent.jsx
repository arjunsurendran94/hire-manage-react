import React, { Component, Fragment } from 'react'
import { Link } from 'react-router-dom'

var searchData=[]
class OrganizationRolesContent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            show_message: false,
        };
    }
    componentDidMount() {
        if (this.props.roleDeleted &&this.props.roleDeleted.role_delete) {
            this.setState({ show_message: true })
        }
    }
    closeDeleteMessage = () => {
        this.setState({
            show_message: false
        })
        // window.location.reload()
    }
    render() {
        
        if(this.props.roles&&this.props.roles.roles){
            
            if(this.props.roles.hasOwnProperty('search')){
                let fulldata=this.props.roles.roles
                searchData=fulldata.filter(
                    (role)=>role.group_name.toLowerCase().indexOf(this.props.roles.search.toLowerCase()) !==-1)
            
            }
            else{
                searchData=this.props.roles.roles
            }
        }

        return (
            
            <Fragment>
               
                <div className="mt-3 col-sm-12 sub-header">
                    <div className="row">
                        <div className="col-sm-12">
                            <div className="bx-content shadow">
                                <div className="form-group p-4">
                                    <span><b>{(this.props.roles&&this.props.roles.roles)&& searchData.length}</b> Roles Found</span>
                                    <span className="float-right">
                                    </span>
                                    {(this.state.show_message&&this.props.location.state&&this.props.location.state.name)&&
                                        <div className="alert alert-dismissible fade show alert-warning-green mt-3">
                                            <i className="icon-checked b-6 alert-icon mr-2"></i> Role "{this.props.location.state.name}" deleted sucessfully
                                            <button type="button" className="close" data-dismiss="alert" aria-label="Close" onClick={this.closeDeleteMessage}>
                                                <span aria-hidden="true">×</span>
                                            </button>
                                        </div>
                                    }
                                </div>
                                <div className="scroll-wrapper table-responsive-cs" style={{position: "relative"}}>
                                    <div className="table-responsive-cs scroll-content" style={{height: "auto", marginBottom: "-10px",marginRight: "-10px", maxHeight: "373.817px"}}>
                                        <table className="table table-cs text-grey table-text-sm table-striped table-borderless ">
                                            <thead>
                                                <tr>
                                                    <th>ROLE NAME </th>
                                                    
                                                </tr>
                                            </thead>
                                            <tbody>
                                            {(this.props.roles&&this.props.roles.roles)&&
                                                searchData.map((role,index) => (
                                                    <tr key={index}>
                                                        <td>
                                                            <span className="user-img-holder">{role.group_name.charAt(0)}{role.group_name.charAt(0)}</span>
                                                            <span className="project-details">
                                                            <span className="project-details-title"><Link to={`/permission-detail/${role.slug}`} className="text-black">
                                                            {role.group_name}</Link></span>
                                                            </span>
                                                        </td>
                                                        <td></td>
                                                    </tr>
                                                ))
                                            }
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th colSpan="6" className="text-center p-4">

                                                    </th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </Fragment>
        )
    }
}

export default OrganizationRolesContent