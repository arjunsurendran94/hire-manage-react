import React, { Component, Fragment } from 'react'
import { Field, reduxForm } from 'redux-form'

const required = value => {
    return (value || typeof value === 'number' ? undefined : ' is Required')
}

const maxLength = max => value =>
    value && value.length > max ? `Must be ${max} characters or less` : undefined

const maxLength128 = maxLength(128)

const email = value =>
    value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
        ? ' is Invalid'
        : undefined

var letters = /^[a-zA-Z.]+\s*$/;
const alphabetOnly = value => {
    return (value && value.match(letters) ? undefined : ' must be only alphabets without spaces in between')
}

const renderRadioField = ({ input, label, type, checked }) => (
    <Fragment>

        <label>{label}</label>
        <input {...input} type={type} checked={checked} />

    </Fragment>
);

const renderFieldSelect = ({
    input,
    label,
    disable,
    type,
    existed,
    options,
    meta: { touched, error, warning, }
}) => {
    // debugger
    return (
        <Fragment>
            {(options && label === "Designation") &&
                <select {...input} className="fullwidth input-control">
                    <option id='no_designation' value="">Select Designation</option>
                    {options && options.designation && options.designation.map((designation, index) =>

                        <option key={index}

                            value={designation.slug} >{designation.designation_name} </option>
                    )}
                </select>
                // :<p className="text-danger">Please create a branch first.</p>
            }
            {(options && label === "Branch") &&
                <select {...input} className="fullwidth input-control" disabled={disable}>
                    <option id='no_designation' value="">Select Branch</option>
                    {options.map((branch, index) =>
                        <option key={index} value={branch.slug} >{branch.branch_name}  </option>
                    )}
                </select>
                // :<p className="text-danger">Please create a branch first.</p>
            }
            {(options && label === "Department") &&
                <select {...input} className="fullwidth input-control">
                    {console.log('options', options)
                    }
                    <option id='no_designation' value="">Select Department</option>
                    {options.map((department, index) =>
                        <option key={index} value={department.slug} >{department.department_name}  </option>
                    )}
                </select>
                // :<p className="text-danger">Please create a branch first.</p>
            }
            <div>
                {touched &&
                    ((error && <small className="text-danger">{label}{error}</small>) ||
                        (warning && <span>{warning}</span>))}
            </div>
            {/* {((existed)&&(existed.error.status == 500)) && <small class="text-danger">
            {existed.errors.error}
        </small>} */}

        </Fragment>

    )
}

const renderFieldLocationSelect = ({
    input,
    label,
    country,
    countryState,
    city,
    className,
    meta: { touched, error, warning, }
}) => {

    return (


        <Fragment>
            <select {...input} className={(touched && error) ? 'input-control border border-danger' : 'input-control'}>
                <option value="">{label}</option>
                {(country && label === "Country") &&
                    <Fragment>
                        {country.countryList.map((plan, index) => (
                            <option key={index} value={plan.id}>{plan.name_ascii}</option>
                        ))}</Fragment>
                }
                {(countryState && label === "State") &&
                    <Fragment>
                        {countryState.stateList.map((plan, index) => (
                            <option key={index} value={plan.id}>{plan.name_ascii}</option>
                        ))}</Fragment>
                }
                {(city && label === "City") &&
                    <Fragment>
                        {city.cityList.map((plan, index) => (
                            <option key={index} value={plan.id}>{plan.name_ascii}</option>
                        ))}</Fragment>
                }
            </select>
            <div>
                {touched &&
                    ((error && <small className="text-danger">{label}{error}</small>) ||
                        (warning && <span>{warning}</span>))}
            </div>
            {/* {((existed)&&(existed.error.status == 500)) && <small class="text-danger">
                {existed.errors.error}
            </small>} */}

        </Fragment>

    )
}

const mobileValid = value => {
    var mobilenoregex = /^(?!\b(0)\1+\b)(\+?\d{1,3}[. -]?)?\(?\d{3}\)?([. -]?)\d{3}\3\d{4}$/
    if (value && value.length >= 1) {
        return (mobilenoregex.test(value) ? undefined : 'Enter a Valid Mobile Number')
    }

    return undefined
}



const rendermobileField = ({ input, label, type, existed, invalid_error, id, className, accept, initialValue, name, meta: { touched, error, warning, } }) => {
    return (
        <Fragment>
            <input {...input} placeholder={label} type={type} className={(touched && error) || (invalid_error && invalid_error.phone) ? 'input-control border border-danger' : 'input-control'} id={id} accept={accept} defaultValue={initialValue} />
            {touched &&
                ((error && <small className="text-danger">{label !== "Phone number" && label}{error} with country code</small>) ||
                    (warning && <span>{warning}</span>))}

            {invalid_error && invalid_error.phone &&
                <small className="text-danger">{invalid_error.phone[0]} with country code</small>
            }
        </Fragment>
    )
}
const renderField = ({
    input,
    label,
    type,
    existed,
    pincodeError,
    meta: { touched, error, warning, }
}) => {

    return (


        <Fragment>
            {console.log('existed', existed)}
            <input {...input} placeholder={label} type={type} className={(touched && error) || ((existed && existed.email) || (pincodeError && pincodeError.non_field_errors)) ? 'input-control border border-danger' : 'input-control'} />
            {touched &&
                ((error && <small className="text-danger">{label}{error}</small>) ||
                    (warning && <span>{warning}</span>))}
            {((existed) && (existed.email)) && <small className="text-danger">
                {existed.email[0]}
            </small>}
            {((pincodeError) && (pincodeError.non_field_errors)) && <small className="text-danger">
                {pincodeError.non_field_errors[0]}
            </small>}
            {/* {((existed)&&(existed.error.status == 500)) && <small class="text-danger">
            {existed.errors.error}
        </small>} */}

        </Fragment>

    )
}
let Errors=[]
class Addemployeeprimaryinfo extends Component {
    constructor(props) {
        super(props)

        this.state = {
            
        }
    }

    validateMethod = () => {
        console.log('this.props.pstate.show_branch', this.props.pstate.show_branch);
        if (this.props.pstate.show_branch) {
            return required
        } else {
            return null
        }
    }

    parentSelect_org=()=>{

        this.props.change('branch','')
        this.props.change('department','')

        this.props.select_org()
    }

    parentSelect_branch=()=>{

        this.props.select_branch()
    }

    componentDidUpdate(prevProps){
    
    }
  
    render() {

        if(this.props.employeeCreated?.createdEmployeeStatus===200){
            debugger
            let img=document.getElementById("photo-preview")
            if(img){ 
                img.src=require("../../../static/assets/image/user_avatar.png")
                }
            
        }
        

        const { handleSubmit } = this.props
        var buttonClass1 = `nav-link ${this.props.pstate.active ? " active show" : ""}`;
        var buttonClass2 = `nav-link ${this.props.pstate.notactive ? " active show" : ""}`;
        var primaryClass = `tab-pane userMail-tab fade float-left fullwidth ${this.props.pstate.active ? " active show" : ""}`;
        var otherClass = `tab-pane userMail-tab fade position-relative float-left fullwidth${this.props.pstate.notactive ? " active show" : ""}`;

        return (
            <Fragment>
                <form onSubmit={handleSubmit}>
                    <div className={primaryClass} id="billing" role="tabpanel" aria-labelledby="billing-tab" style={{ position: "relative" }}>
                        {(this.props.pstate.showEmpCreatedMessage) &&
                            <div className="alert alert-dismissible fade show alert-warning-green mt-3">
                                <i className="icon-checked b-6 alert-icon mr-2"></i> We have sent an invitation email to {this.props.pstate.empEmail}
                                <button onClick={this.props.showEmpCreatedMessage} type="button" className="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                        }

                        {this.props.pstate.allErrors &&this.props.pstate.allErrors?.length>0 &&
                            this.props.pstate.allErrors.map(msg=>
                            <div className="alert alert-dismissible fade show alert-danger mt-3">
                                <i className="icon-warning-triangle alert-icon mr-2"></i> {msg}
                                                    <button onClick={this.props.closeErrorMessage} type="button" className="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>)
                        }

                        
                            {/* {this.props.pstate.emailError&&<div className="alert alert-dismissible fade show alert-danger mt-3">
                                <i className="icon-warning-triangle alert-icon mr-2"></i> {this.props.pstate.emailError}
                                <button onClick={this.props.closeErrorMessage} type="button" className="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>}

                            {this.props.pstate.contactNoError&&<div className="alert alert-dismissible fade show alert-danger mt-3">
                                <i className="icon-warning-triangle alert-icon mr-2"></i> {this.props.pstate.contactNoError}
                                <button onClick={this.props.closeErrorMessage} type="button" className="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>}
                        
                            {this.props.pstate.pincodeError&&<div className="alert alert-dismissible fade show alert-danger mt-3">
                                <i className="icon-warning-triangle alert-icon mr-2"></i> {this.props.pstate.pincodeError}
                                <button onClick={this.props.closeErrorMessage} type="button" className="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>} */}



                        <div className="col-sm-12">
                            <div className="row">

                                <div className="pro-pic mb-4 mt-3">
                                    <img id="photo-preview" src={this.props.pstate.image ? this.props.pstate.image : require("../../../static/assets/image/user_avatar.png")} className="img-fluid" />
                                </div>
                                <div className="col-sm-12 mb-4 text-center">
                                    <div className="d-none">
                                        <input className="inputfile" type="file" onChange={this.props._onChange} id="id_photo" accept="image/*" />
                                        {/* <img id="target" src={require("../../../static/assets/image/user_avatar.png")}/> */}
                                        {/* <input type="file" name="photo" accept="image/*" onchange="readURL(this);" class="inputfile" id="id_photo"/> */}
                                    </div>
                                    <label className="btn btn-fb" htmlFor="id_photo">
                                        <i className="icon-paper-clip"></i> Upload Profile Photo
                                                        </label>
                                    <div className="text-danger">
                                        {this.props.pstate.image_validation_error}
                                    </div>

                                </div>
                                <div className="col-md-6">
                                    <div className="row">
                                        <div className="col-sm-12 form-group">
                                            <label className="text-light-grey" htmlFor="id_first_name">
                                                <label htmlFor="id_first_name">First name</label></label>
                                            <span className="text-danger">*</span>
                                            {/* <input type="text" name="first_name" placeholder="First name" maxlength="128" className="input-control" required="" id="id_first_name"/>          */}
                                            <Field
                                                type="text"
                                                name="first_name"
                                                component={renderField}
                                                label="First Name"
                                                validate={[required, alphabetOnly, maxLength128]} />
                                        </div>
                                        <div className="col-sm-12 form-group">
                                            <label className="text-light-grey" htmlFor="id_last_name">
                                                <label htmlFor="id_last_name">Last name</label></label>
                                            <span className="text-danger">*</span>
                                            {/* <input type="text" name="last_name" placeholder="Last name" maxlength="128" className="input-control" required="" id="id_last_name"/>         */}
                                            <Field
                                                type="text"
                                                name="last_name"
                                                component={renderField}
                                                label="Last Name"
                                                validate={[required, alphabetOnly, maxLength128]} />
                                        </div>
                                        <div className="col-sm-12 form-group">
                                            <label className="text-light-grey" htmlFor="id_email">
                                                <label htmlFor="id_email">Email Address:</label></label>
                                            <span className="text-danger">*</span>
                                            {console.log('{this.props.pstate.emailError}',this.props.pstate.emailError)                                            }
                                            <Field
                                                type="email"
                                                name="email"
                                                component={renderField}
                                                label="Email Address"
                                                existed={this.props.pstate.emailError}
                                                onChange={() => this.props.errorOnChange('email')}
                                                validate={[required, email]} />
                                        </div>

                                        <div className="col-sm-12 form-group">

                                            <label className="text-light-grey" htmlFor="id_designation">
                                                <label htmlFor="id_designation">Designation</label></label>
                                            <span className="text-danger">*</span>
                                            {/* <select name="designation" className="input-control" required="" id="id_designation">
                                                                    <option value="" selected="">Select Designation</option>
                                                                    <option value="43">lok</option>
                                                                </select>   */}
                                            <div className="position-relative pr-5">
                                                <button type="button" onClick={this.props.toggleAddDesignationModal} className="btn-plus-icn">
                                                    <i className="icon-plus-circle-o"></i>
                                                </button>

                                                <Field
                                                    name="designation"
                                                    type="select"
                                                    options={(this.props.designationList)}
                                                    component={renderFieldSelect}

                                                    className="fullwidth"
                                                    label="Designation"
                                                    validate={required}

                                                />
                                            </div>


                                            <span className="text-danger designation-error"></span>

                                            {(this.props.designationList && this.props.designationList.designation && this.props.designationList.designation.length <= 0) &&
                                                <div className="text-danger">
                                                    Note: Please create Designation first.
                                                                    </div>
                                            }

                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-6">
                                    <div className="row">
                                        {(this.props.branches && this.props.branches.data && this.props.branches.data.length > 0) ? (
                                            <Fragment>
                                                <div className="col-sm-12 form-group">
                                                    <label htmlFor="id_selection_0">
                                                        <Field name="owner_type" checked={(!this.props.pstate.show_branch) ? true : false} component={renderRadioField} type="radio" value="organization" onChange={this.parentSelect_org} />{' '}
                                                                            Invite for Organization
                                                                    </label>
                                                    <label htmlFor="id_selection_1">
                                                        <Field name="owner_type" checked={this.props.pstate.show_branch} component={renderRadioField} type="radio" value="branch" onChange={this.parentSelect_branch} />{' '}
                                                                            Invite for Branch
                                                                    </label>
                                                </div>

                                                {this.props.pstate.show_branch&&<div className="col-sm-12 form-group branch">
                                                    <label className="text-light-grey" htmlFor="id_branch">
                                                        <label htmlFor="id_branch">Branch</label>
                                                    </label>


                                                    <Field
                                                        name="branch"
                                                        type="select"
                                                        options={(this.props.branches && this.props.branches.data && this.props.branches.data && this.props.branches.data.length > 0) && this.props.branches.data}
                                                        component={renderFieldSelect}
                                                        className="fullwidth"
                                                        label="Branch"
                                                        disable={!this.props.pstate.show_branch}
                                                        onChange={this.props.handleBranchChange}
                                                        validate={this.validateMethod()} />
                                                </div>}
                                            </Fragment>) :
                                            <div className="col-sm-12 form-group " id="buttondisable">
                                                <div className="text-info branch">
                                                    Note: This organization has no branches. add new branch to create departments under branch
                                                                </div>
                                            </div>
                                        }

                                        <div className="col-sm-12 form-group dept">
                                            <label className="text-light-grey" htmlFor="id_department">
                                                <label htmlFor="id_department">Department</label>
                                            </label>

                                            {console.log('this.props?.departmentList', this.props?.departmentList)
                                            }
                                            <Field
                                                name="department"
                                                type="select"
                                                options={this.props?.departmentList}
                                                component={renderFieldSelect}
                                                className="fullwidth"
                                                label="Department"
                                            // validate={required}
                                            />
                                            <span className="text-danger department-error"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button type="submit" className="btn btn-prmary-blue float-right">Next</button>
                    </div>

                    
                
                </form>
            </Fragment>
        )
    }
}

Addemployeeprimaryinfo = reduxForm({
    form: 'addEmployeeForm',
    destroyOnUnmount: false, // <------ preserve form data
    forceUnregisterOnUnmount: true, // <------ unregister fields on unmount

})(Addemployeeprimaryinfo)
export default Addemployeeprimaryinfo