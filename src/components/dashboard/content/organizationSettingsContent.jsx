import React, {Component,Fragment} from 'react'
import OrganizationDetailComponent from '../common/organizationDetailComponent'
import BranchDetailComponent from '../common/branchDetailComponent'

class OrganizationSettingsContent extends Component{
    componentDidMount(){
        this.props.retrieveOrganization();
        this.props.retrieveBranches(localStorage.getItem('organization_slug'),localStorage.getItem('token'))
        // this.props.retrieveDepartments('');
    }
    render(){

        if((this.props.branches && this.props.branches.status==='success')||
        (this.props.departmentlist&&this.props.departmentlist.status==='success')){
            window.location.reload()
            
        }
        return( 
            <Fragment>
                <div className="col-sm-12 sub-header mt-3">
                    <div className="row">
                        {/* <!-- userMail-grid --> */}
                        <div className="userMail-grid fullwidth">
                        <div className="col-sm-12 p-4 float-left">
                            <OrganizationDetailComponent {...this.props}/>
                            {(this.props.branches&&this.props.branches.data&&this.props.branches.data&&this.props.branches.data.length>0)&&
                            this.props.branches.data.map((branch,index) => (<BranchDetailComponent key={index} branch={branch} {...this.props}/>))
                            
                            }
                        </div>
                        </div>
                        </div>
                    </div>
            </Fragment>
        )
    }
}

export default OrganizationSettingsContent