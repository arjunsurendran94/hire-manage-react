import React, { Component, Fragment } from 'react'

import AddTaskModal from '../modal/addTaskModal'
import { Link } from 'react-router-dom';

   
class ViewTaskContent extends Component {
constructor(props) {
super(props);
var date = new Date();

this.state = {

checked:false,
isOpen:false,

editModal:false,
         editPart:'',
         index:''
};
}

addTaskModal=(editPart,index)=>{
   this.setState({editModal:true,editPart:editPart,index:index})
}
closeEditModal = () =>{
   this.setState({editModal:false,editPart:'',index:''})
}
render() {
   
   
return (
<Fragment>
   
      
            <div className="row team-content">
            <div className="col-sm-12 form-group ">
            
              
               <li>
                  <table className="table" >
                    <thead> <tr>
                        <th><h6 style={{color:"#9C9C9C"}}>Task Name</h6></th>
                        <th>Task 1</th>
                        

                     </tr>
                     </thead>
                     <tbody>
                     <tr>
                        <td><h6 style={{color:"#9C9C9C"}}>Task Description</h6></td>
                        <td> Description</td>
                        
                     </tr>
                     <tr>
                        <td><h6 style={{color:"#9C9C9C"}}>Task Duration</h6></td>
                        <td> 200</td>
                       
                     </tr>
                     <tr>
                        <td><h6 style={{color:"#9C9C9C"}}>Status</h6></td>
                        <td> Created</td>
                       
                     </tr>
                     <tr>
                        <td><h6 style={{color:"#9C9C9C"}}>Created By</h6></td>
                        <td> Owner Name</td>
                       
                     </tr>
                     <tr>
                        <td><h6 style={{color:"#9C9C9C"}}>Assigned To</h6></td>
                        <td> Developer Name</td>
                       
                     </tr>
                     <tr>
                        <td><h6 style={{color:"#9C9C9C"}}>Estimated Hours</h6></td>
                        <td> 200</td>
                       
                     </tr>
                     
                     
                     </tbody>
                  </table>
               </li>
               </div>
              </div>
            
            
{this.state.editModal&&<AddTaskModal {...console.log("UPDATE",this.props,this.state)} 
             {...this.props} closeModal={this.closeEditModal} index={this.state.index}  editpart={this.state.editPart}
             
             />}
</Fragment>
)
}
}

export default ViewTaskContent