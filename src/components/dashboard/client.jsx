import React, { Component, Fragment } from 'react'
import ClientContent from './content/clientContent'
import ClientList from './content/clientList'
import SideBar from './common/sideBar'
import DashBoardHeaderContainer from '../../containers/common/dashBoardHeaderContainer'
import DashBoardSubheader from './common/dashboardSubHeader'
import Loader from '../common/Loader'
import LoaderImage from '../../static/assets/image/loader.gif';
import { Link } from 'react-router-dom'
var loaderStyle={
width: '3%',
position: 'fixed',
top: '50%',
left: '57%'
}
class Client extends Component {
componentDidMount() {
this.props.clientListing()
}
componentWillUnmount(){
   this.props.clearSearch()
}
render() {
console.log(this.props.clientData);
// Dummy client data
let clients 
if (this.props.clientData&&this.props.clientData.clients&&this.props.clientData.clients.length>0){
if(this.props.clientData&&this.props.clientData.search&&this.props.clientData.search!=''){
   const lowercasedFilter = this.props.clientData.search.toLowerCase();
            let parameters = ["email", "first_name", "last_name"]
            clients = this.props.clientData.clients.filter(item => {
               return parameters.some(key =>
                  item.client[key].toLowerCase().includes(lowercasedFilter)
               );
            });
}
else{
clients=this.props.clientData.clients
}
} 
return (
<Fragment>
   <div className="container-fluid">
      <div className="row">
         <SideBar />
         <div className="col-sm-12 container-body ">
            <DashBoardHeaderContainer />
            <div className="row">
               <DashBoardSubheader {...this.props} subheader="Clients" />
               {this.props.clientData&&this.props.clientData.isLoading?
               <Fragment>
                  <div className="loader-img"> <img src={LoaderImage} style={loaderStyle} /></div>
               </Fragment>
               :
               <Fragment>
                  {clients&&clients.length>0
                  ?
                  <div className='col-sm-12 mt-3 sub-header'>
                     <table class="table table-cs text-grey table-text-sm table-striped table-borderless">
                        <thead>
                           <tr>
                              <th className="thead-padding">CONTACT NAME</th>
                              <th className="thead-padding">COMPANY NAME</th>
                              <th className="thead-padding">CONTRACTS</th>
                              <th className="thead-padding">COUNTRY</th>
                              <th className="thead-padding">CONTACT NUMBER</th>
                              {/* 
                              <th>SKYPE ID</th>
                              <th>WEBSITE</th>
                              */}
                           </tr>
                        </thead>
                        <tbody>
                           {clients.map(clientObj =>
                           <tr>
                              <td data-title="CONTACT NAME">
                                 <div className="position-relative">
                                    <span className="user-img-holder clnt-lst12">
                                    {clientObj&&clientObj.client&&clientObj.client.first_name.charAt(0)}{clientObj&&clientObj.client&&clientObj.client.last_name.charAt(0)}
                                    </span>
                                    <span className="project-details clnt-mr-lft">
                                    <span className="project-details-title">
                                    <Link to={{pathname:`/clients/client/${clientObj.client.slug}`, client:{clientObj},clientListing:{}}} className="text-black">
                                    {clientObj&&clientObj.client&&clientObj.client.first_name} &nbsp;
                                    {clientObj&&clientObj.client&&clientObj.client.last_name}
                                    </Link>
                                    <div className="project-details-user" style={{display:"block"}}>{clientObj&&clientObj.client&&clientObj.client.email}</div>
                                    </span>
                                    </span>
                                 </div>
                              </td>
                              <td data-title="COMPANY NAME">{clientObj&&clientObj.client&&clientObj.client.company_name}</td>
                              <td data-title="CONTRACTS">
                                 <div class="dropdown dropright">
                                    <button type="button"  style={{fontSize:"11px"}}class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                    Multiple Contracts
                                    </button>
                                    <div class="dropdown-menu" style={{fontSize:"10px"}}>


                                       {clientObj&&clientObj.projects&&clientObj.projects.map((projct)=>
                                          <Link class="dropdown-item" to={{ pathname:`/projects/project/${projct.slug}`}}> 
                                          {projct&&projct.project_name}</Link>
                                       )}
                                       
                                       
                                    </div>
                                 </div>
                              </td>
                              <td data-title="COUNTRY">
                                 <span className="badge badge-pill badge-outline-primary">
                                 {clientObj && clientObj.billing_address && clientObj.billing_address.country && clientObj.billing_address.country.name_ascii}</span>
                              </td>
                              <td data-title="CONTACT NUMBER">
                                 <div className="position-relative">
                                       <span className="text-muted clnt-lst12">{clientObj?.client?.mobile&&<i class="icon-phone"></i>}</span>
                                    <span className="span-column clnt-mr-lft2">
                                    <span className="span-row">{clientObj && clientObj.client && clientObj.client.mobile}</span>
                                    <span className="span-row"></span>
                                    </span>
                                 </div>
                              </td>
                              {/* 
                              <td data-title="SKYPE ID">
                                 {clientObj && clientObj.otherdetails && clientObj.otherdetails.skype_name}
                              </td>
                              <td data-title="WEBSITE">
                                 <a href="">{clientObj && clientObj.client && clientObj.client.website}</a>
                              </td>
                              */}
                           </tr>
                           )}
                        </tbody>
                     </table>
                  </div>
                  :
                  <ClientContent {...this.props} />
                  }
               </Fragment>
               }
            </div>
         </div>
      </div>
   </div>
</Fragment>
)
}
}
export default Client