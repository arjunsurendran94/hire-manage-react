import Moment from "moment";

const timeframes=()=>{
    let timings=[];
    var timeValue = ["12:00 am", "01:00 am",
    "02:00 am", "03:00 am", "04:00 am", "05:00 am", "06:00 am","07:00 am", "08:00 am", "09:00 am", "10:00 am", "11:00 am", "12:00 pm",
  "01:00 pm", "02:00 pm", "03:00 pm", "04:00 pm", "05:00 pm", "06:00 pm", "07:00 pm"
  , "08:00 pm", "09:00 pm", "10:00 pm", "11:00 pm"]

    for(let obj of timeValue){
      
        for(let i=0;i<60;i+=10){
        
        let timeframe=Moment(obj,"h:mm a").add(i,'minutes').format("h:mm a");
        let startHourlyTf=Moment(timeframe,"h:mm a").format("HH:mm:ss");
        let endHourlyTf=Moment(timeframe,"h:mm a").add(9,'minutes').add(59,'seconds').format("HH:mm:ss");

        timings.push({ 
                        timeframe:timeframe ,

                        startHourlyTf:startHourlyTf,
                        endHourlyTf:endHourlyTf,                      
                        background: "screenshot-container", 
                        task: "", 
                        start_time: "", 
                        end_time: "" ,
                        worked_hours:"",

                        startPercent:0,
                        endPercent:100,
                        color:""
                        
        })

        }
  
    }
  return timings
}

export default timeframes