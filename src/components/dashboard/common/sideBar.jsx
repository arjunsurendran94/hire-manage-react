import React, { Component, Fragment } from 'react'
import { Link } from 'react-router-dom'

class SideBar extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isOpen: false,
            toggle: true,
            addClass: false
        }
    }
    toggleOpen = () => {
        this.setState({
            isOpen: !this.state.isOpen,
            toggle: false
        });
    }
    toggleUserTab = () => {
        this.setState({ addClass: !this.state.addClass, toggle: false })
    }
    render() {
        console.log('props',this.props);
        
        var navClass = `col-sm-12 sidemenu-bx scroll-content${this.state.isOpen ? " scroll-scrolly_visible" : ""}`;
        var newClass = `submenu c-blue${this.state.isOpen ? " active open" : ""}`;
        var userClass = `menu-group-item inner-menu${this.state.addClass ? " open" : ""}`;
        var iconClass = this.state.addClass ? "icon-right-pos icon-chevron-arrow-down" : "icon-right-pos icon-chevron-arrow-up"
        var userSubMenuClass = `summenu-items inner-menu-item${this.state.addClass ? "" : " d-none"}`;

        if (this.props.a && this.state.toggle && (
            (!this.props?.location?.pathname?.includes("/clients/") && (!this.props?.location?.pathname?.includes("/projects/"))))) {
            navClass = "col-sm-12 sidemenu-bx scroll-content scroll-scrolly_visible";
            newClass = "submenu c-blue active open";
            userClass = "menu-group-item inner-menu open";
            userSubMenuClass = "summenu-items inner-menu-item"
            iconClass = "icon-right-pos icon-chevron-arrow-down"
        }

        let permissions = []
        permissions = localStorage.getItem('permissions')
        if (permissions && permissions.length > 0) {
            permissions = permissions.split(',')
        }
        // console.log('permissions',permissions.includes('view_project'));
        
        let is_admin=localStorage.getItem('user_type') === 'admin'

        return (
            <Fragment>
                <div className="scroll-wrapper col-sm-12 sidemenu-bx" style={{ position: "relative" }}>
                    <nav className={navClass} style={{ height: "auto", marginBottom: "0px", marginRight: "0px", maxHeight: "350px" }}>
                        <div className="menu-group">

                            <Link to='/dashboard' className="menu-group-item c-green"><span className="icon-bx"><i className="icon-graph-level"></i></span>
                            Dashboard
                        </Link>
                            {(is_admin) &&
                                <div className={newClass}>

                                    <div className="menu-group-item c-blue " data-toggle="dropmenu" onClick={this.toggleOpen}>

                                        <Link to='/organization/organization/'><span className="icon-bx"><i className="icon-group"></i></span>Organization</Link>

                                        <i className="icon-chevron-arrow-down icon-right-pos" ></i>
                                    </div>

                                    <div className="summenu-items">

                                        <Link to={{ pathname: '/organization/settings/1/' }} className={(this.props.location && (this.props.location.pathname === "/organization/settings/1/")) ? "menu-group-item c-vilot  active open" : "menu-group-item c-vilot"}>Settings</Link>
                                        <Link to={{ pathname: '/departments/departments/' }} className={(this.props.location && ((this.props.location.pathname === "/departments/departments/") || (this.props.location.pathname === "/departments/add-new-department/"))) ? "menu-group-item c-vilot  active open" : "menu-group-item c-vilot"}>Departments</Link>
                                        <Link to={{ pathname: '/organization/branches/' }} className={(this.props.location && ((this.props.location.pathname === "/organization/branches/") || (this.props.location.pathname === "/organization/add-new-branch/"))) ? "menu-group-item c-vilot  active open" : "menu-group-item c-vilot"}>Branches</Link>

                                        <div className={userClass} onClick={this.toggleUserTab}>
                                            Users
                                    <i className={iconClass}></i>
                                        </div>
                                        <div className={userSubMenuClass}>
                                            <Link to={{ pathname: '/employees/employees/' }} className={(this.props.location && ((this.props.location.pathname === "/employees/employees/") || (this.props.location.pathname === "/employees/create-employee/"))) ? "menu-group-item c-vilot  active open" : "menu-group-item c-vilot"}>Employees</Link>
                                            <Link to={{ pathname: '/permission-groups/' }} className={(this.props.location && ((this.props.location.pathname === "/permission-groups/") || (this.props.location.pathname === "/add-permission-group/") || (this.props.location.pathname === `/permission-detail/${this.props.match.params.role_slug}`))) ? "menu-group-item c-vilot  active open" : "menu-group-item c-vilot"}>Roles</Link>
                                            <Link to={{ pathname: '/departments/designations/' }} className={(this.props.location && ((this.props.location.pathname === "/departments/designations/") || (this.props.location.pathname === "/departments/add-designation/"))) ? "menu-group-item c-vilot  active open" : "menu-group-item c-vilot"}>Designations</Link>
                                            <Link to={{ pathname: '/groups/' }} className="menu-group-item c-vilot">Groups</Link>
                                        </div>

                                    </div>

                                </div>}

                            {(is_admin||(permissions&&permissions.length>0&&permissions.includes('view_client'))) 
                            && <Link to="/clients/clients/"
                                className={this.props && this.props.location && this.props.location.pathname
                                    && (this.props.location.pathname.includes("/clients/")) ?
                                    "menu-group-item c-orange active" : "menu-group-item c-orange"}><span className="icon-bx"><i className="icon-user-level"></i></span>
                        Clients</Link>}



                            {(is_admin||(permissions&&permissions.length>0&&permissions.includes('view_project')))&&
                            <Link to="/projects/projects/"
                                className={this.props && this.props.location && this.props.location.pathname
                                    && (this.props.location.pathname.includes("/projects/")) ?
                                    "menu-group-item c-yellow active" : "menu-group-item c-yellow"}>
                                        <span className="icon-bx"><i className="icon-user-level"></i></span>
                            Projects</Link>}

                            <Link to={{ pathname: '/reports/' }} className="menu-group-item c-green"><span className="icon-bx"><i className="icon-calendar"></i></span>
                        Reports</Link>

                        </div>
                    </nav>
                    <div className="scroll-element scroll-x"><div className="scroll-element_outer"><div className="scroll-element_size"></div><div className="scroll-element_track"></div><div className="scroll-bar" style={{ width: "0px" }}></div></div></div>
                    <div className="scroll-element scroll-y"><div className="scroll-element_outer"><div className="scroll-element_size"></div><div className="scroll-element_track"></div><div className="scroll-bar" style={{ height: "0px" }}></div></div></div>
                </div>
            </Fragment>
        )
    }
}

export default SideBar