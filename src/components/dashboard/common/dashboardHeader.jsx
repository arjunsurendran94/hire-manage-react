import React, { Component, Fragment } from 'react'
import { Redirect } from 'react-router';
import { Link } from 'react-router-dom'
import { withRouter } from "react-router-dom";
const MODAL_OPEN_CLASS = "open";






class DashBoardHeader extends Component {
  state = {
    profileDrop: false,
    toggle: false,
    organizationDrop: false,
  }
  changeOrganization = (organizationSlug) => {
    localStorage.setItem("organization_slug", organizationSlug)
    window.location.reload(true);
    // return <Redirect to={{pathname:window.location.pathname}} />
  }
  sidemenuToggle = () => {
    this.state.toggle ? document.body.classList.remove(MODAL_OPEN_CLASS) : document.body.classList.add(MODAL_OPEN_CLASS);
    this.setState({ toggle: !this.state.toggle })
  }
  openProfileDrop = () => {
    this.setState({ profileDrop: !this.state.profileDrop, organizationDrop: false })
  }
  organizationFlagSet = () => {
    this.setState({ organizationDrop: !this.state.organizationDrop, profileDrop: false })
  }
  logOut = (e) => {
    e.preventDefault()
    this.props.logOut()
  }
  myHandler = () => {
    this.setState({ profileDrop: false, organizationDrop: false })
  }
  componentDidMount() {

    this.props.fetchUserOrganization()
    this.props.getProfile()
    // document.body.addEventListener('click', this.myHandler);
  }
  getFirstTwoCharacter(str){
    // debugger
    let strlist=str.split(' ')
    
  }
  render() {
    if (!localStorage.getItem("token")) {
    // return <Redirect to="/login" />
    this.props.history.push("/login");
    }
    
    if (localStorage.getItem("organization_slug") === null) {
      if (this.props.organzationList) {
        localStorage.setItem("organization_slug", this.props.organzationList.data[0].slug)
      }
    }
    

    return (

      <Fragment>
        <header className="navbar navbar-expand-lg navbar-light">
          <div className="mr-auto">
            <button type="button" className="btn btn-nav" data-toggle="sidemenu" data-target="#sidemenubar" onClick={this.sidemenuToggle}>
              <span className="btn-nav-item"></span>
              <span className="btn-nav-item"></span>
              <span className="btn-nav-item"></span>
            </button>

            <Link to="#">
              <img src={require("../../../static/assets/image/logo.png")} className="brand-img" alt="Hire &amp; Manage" />
            </Link>
          </div>
          <div className="menu-rightbar">
            {this.props.organzationList && this.props.organzationList.data && (
              <Fragment>
                {this.props.organzationList.data.length <= 1 ?
                  <div className="dropdown nav-dropdown">
                    <div className="btn" style={{ cursor: "auto" }}>
                      <div className="pro-rnd">

                      {(this.props.organzationList&&this.props.organzationList.data[0] && this.props.organzationList.data[0] &&this.props.organzationList.data[0].logo)?
                      <img alt="logo" src={(this.props.organzationList.data[0].logo)} className="img-round img-user-icon org-prof-img" />:
                      
                      <div>
                      <span className="user-img-holder dept-updt yellow">  {this.props.organzationList.data[0]&&this.props.organzationList.data[0].organization_name.charAt(0)}</span>
                      
                      </div>

                    }
                    
                      </div>
                    </div>
                  </div> :
                  <div className="dropdown nav-dropdown show">
                    <button type="button" className="btn btn-link-secondary dropdown-toggle pro-arw" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" onClick={this.organizationFlagSet}>
                      <div className="pro-rnd">
                        {this.props.organzationList.data.map((organization, index) => (
                          (organization.slug === localStorage.getItem("organization_slug")) &&
                          (organization.logo ?
                          <img key={index}  src={organization.logo} className="img-round img-user-icon org-prof-img" />
                          :
                          <span className="user-img-holder dept-updt yellow">{organization.organization_name.charAt(0)}</span>
                          )
                        ))}
                      </div>
                    </button>
                    {this.state.organizationDrop &&
                      <div className="dropdown-menu dropdown-menu-right dr-dp-dw show" aria-labelledby="dropdownMenu2">
                        {this.props.organzationList.data.map((organization, index) => (
                          <Fragment>
                            <button key={index} class={(organization.slug === localStorage.getItem("organization_slug")) ? "dropdown-item active" : "dropdown-item"} href="" onClick={()=>this.changeOrganization(organization.slug)}>{organization.organization_name}</button>

                          </Fragment>
                        ))}
                      </div>
                    }
                  </div>
                }
              </Fragment>
            )}



            <div className="dropdown nav-dropdown position-relative">


              <button className="btn btn-notify" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i className="icon-bell"></i>
              </button>

              <div className="dropdown-menu dropdown-menu-right p-0 side-notif fullwidth" aria-labelledby="dropdownMenu2">
                <div className="not-head fullwidth float-left border-bottom p-2">

                  <h6 className="float-left mb-0">Notifications</h6>





                </div>




                <Link to="#" className="hist notif-bg fullwidth float-left p-3 border-bottom position-relative dropdown-item">
                  <div className="hist-cont float-left">
                    <span className="not-para wh-space">No notification found!</span>
                  </div>
                </Link>


              </div>
            </div>


            <div className="dropdown nav-dropdown">
              <button className="btn btn-link-secondary dropdown-toggle pro-arw" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" onClick={this.openProfileDrop}>
                <div className="pro-rnd">

                  <img alt="logo" src={(this.props.profileDetails && this.props.profileDetails.profileDetail && this.props.profileDetails.profileDetail.photo) ?
                    this.props.profileDetails.profileDetail.photo :
                    require("../../../static/assets/image/man.png")} className="img-round img-user-icon prof-img" />

                </div>
              </button>
              <div className="dropdown-menu dropdown-menu-right dr-dp-dw" aria-labelledby="dropdownMenu2" style={this.state.profileDrop ? { display: "block" } : { display: "none" }}>
                <a href={"/accounts/profile/"} className="dropdown-item">Profile</a>
                <a href="/history/" className="dropdown-item">History</a>
                <a href="/login" onClick={this.logOut} className="dropdown-item">Logout</a>
              </div>
            </div>

          </div>
        </header>
      </Fragment>
    )
  }
}

export default withRouter(DashBoardHeader)