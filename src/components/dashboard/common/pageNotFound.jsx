// Success.jsx
import React, { Component,Fragment } from 'react';
import { Link } from 'react-router-dom/cjs/react-router-dom.min';
import Imgg from '../../../static/assets/image/pagenotfound.jpg'
  
class pageNotFound extends Component{
    render(){
        return(
            <Fragment>
                <div id="bg">
                    <img src={Imgg} alt=""/>
                    <div class="caption">
                        <span style={{color:"red"}}>OOPS! PAGE NOT FOUND</span><br/>
                        <Link to="/dashboard"><button type="button" class="btn btn-info" style={{width:"140px"}}>Back to Dashboard</button></Link>     
                    </div>
                </div>
            </Fragment>
        )
    }
}

export default pageNotFound;