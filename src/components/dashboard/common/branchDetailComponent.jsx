import React, { Component, Fragment } from "react";
import BranchEditModal from '../modal/editBranchModal'
import DepartmentDetailComponent from '../../../components/dashboard/common/departmentDetailComponent'
import DeleteModal from '../modal/deleteModal'
import { Link } from 'react-router-dom'

class BranchDetailComponent extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isOpen: false,
      isDelete: false,
    }
  }
  toggleModal = () => {
    this.setState({ isOpen: !this.state.isOpen });
  }
  toggleDeleteBranch = (branch_slug) => {
    window.scrollTo(0, 0)
    this.setState({ isDelete: !this.state.isDelete });
  }
  componentDidMount() {
    if (this.props.departmentlist && this.props.departmentlist.branch_department.length <= 0) {
      this.props.retrieveDepartments(this.props.branch.slug);
    }

  }
  render() {
    var navClass = `mt-4 fullwidth float-left position-relative update-ul${((this.props.departmentlist) && (this.props.departmentlist.branch_department) && (this.props.departmentlist.branch_department.length > 0) && (this.props.departmentlist.connections) && (this.props.branch.slug in this.props.departmentlist.connections) && ((this.props.subheader !== "Branches") && (this.props.subheader !== "Departments"))) ? " brd-btm-col " : ""}`;
    return (<Fragment>
      {(this.props.subheader === "organization") ?
        <div onClick={() => this.props.getBranchDepts(this.props.branch.slug, this.props.branch.branch_name, this.props.index)} className="nav d-block" id="nav-tab" role="tablist">
          <Link className={this.props.indexValue === this.props.index ? "braches branch-slug d-flex active" : "braches branch-slug d-flex"} >
            <div className="d-inline-block ">
              <div className="branches-ic">
                <img src={require("../../../static/assets/image/loc-ic.png")} alt="logo" />
              </div>
            </div>
            <div className="d-inline-block braches-txt">
              <h4>{this.props.branch.branch_name}</h4>
              {(this.props.departmentlist && this.props.departmentlist.connections && (this.props.branch.slug in this.props.departmentlist.connections)) ?
                <p>{this.props.departmentlist.connections[this.props.branch.slug]} Departments</p> :
                <p>No Departments</p>
              }


              <h5><img src={require("../../../static/assets/image/b-loc-ic.png")} alt="logo" />{this.props.branch.city.name_ascii}</h5>
            </div>
          </Link>
        </div>
        :
        <div className="bx-content shadow mt-4">
          <div className="grid-contract">
            <div className="grid-contract-body bg-body-theme p-3 bg-white">
              <h5>Branch</h5>
              {this.props.editable &&
                <div className="project-options up-org-opt">
                  <button className="btn btn-link-secondary branch-edit show-form-modal" data-url="/organization/ajax-update-branch/7f3576d8-0bae-4c80-a670-2d4b1dfa9512/" onClick={this.toggleModal}>
                    <i className="icon-pencil"></i>
                  </button>
                  <button className="btn btn-link-secondary" data-id="7f3576d8-0bae-4c80-a670-2d4b1dfa9512" onClick={() => this.toggleDeleteBranch(this.props.branch.slug)}>
                    <i className="icon-trash"></i>
                  </button>
                </div>
              }
              {this.state.isOpen && <BranchEditModal modalClose={this.toggleModal} {...this.props} />}
              {this.state.isDelete && <DeleteModal deleteType="branch" onCloseModal={this.toggleDeleteBranch} {...this.props} />}
              <ul className={navClass}>
                <div className="d-inline-block u-br-icon">
                  <div className="branches-ic up-branch-icon">
                    <img src={require("../../../static/assets/image/loc-ic.png")} alt="logo" />

                  </div>
                </div>
                <li className="col-lg-3 float-left mb-3">
                  <div>
                    <span className="b-6">BRANCH NAME</span>
                  </div>
                  <div className="text-muted text-sm">
                    {this.props.branch.branch_name}
                  </div>
                </li>
                <li className="col-lg-3 float-left mb-3">
                  <div>
                    <span className="b-6">COUNTRY</span>
                  </div>
                  <div className="text-muted text-sm">
                    {this.props.branch.country.name_ascii}
                  </div>
                </li>
                <li className="col-lg-3 float-left mb-3">
                  <div>
                    <span className="b-6">STATE</span>
                  </div>
                  <div className="text-muted text-sm">
                    {this.props.branch.state.name_ascii}
                  </div>
                </li>
                <li className="col-lg-3 float-left mb-3">
                  <div>
                    <span className="b-6">CITY</span>
                  </div>
                  <div className="text-muted text-sm">
                    {this.props.branch.city.name_ascii}
                  </div>
                </li>
                <li className="col-lg-3 float-left mb-3">
                  <div>
                    <span className="b-6">ADDRESS</span>
                  </div>
                  <div className="text-muted text-sm">
                    {(this.props.branch && this.props.branch.address) ? this.props.branch.address : "Nill"}
                  </div>
                </li>
              </ul><div className="clearfix"></div>
              {(!this.props.searchstatus)&&(this.props.departmentFlag) && ((this.props.departmentlist) && (this.props.departmentlist.branch_department) && (this.props.departmentlist.branch_department.length > 0)) &&
                <DepartmentDetailComponent testCondition={false} branch_slug={this.props.branch.slug} branch_dict={this.props.departmentlist.connections} allDepartments={this.props.departmentlist.branch_department} {...this.props} />
              }

              {(this.props.searchstatus)&&(this.props.departmentFlag) && ((this.props.departmentlist) && (this.props.departmentlist.search_branch_department) && (this.props.departmentlist.search_branch_department.length > 0)) &&
                <DepartmentDetailComponent testCondition={false} branch_slug={this.props.branch.slug} branch_dict={this.props.departmentlist.connections} allDepartments={this.props.departmentlist.search_branch_department} {...this.props} />
              }

            </div>
          </div>
        </div>
      }
    </Fragment>
    )
  }
}
export default BranchDetailComponent