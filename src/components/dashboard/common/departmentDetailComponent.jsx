import React, {Component ,Fragment} from "react";
import AddDepartment from '../modal/addDepartmentModal'
import DeleteModal from '../modal/deleteModal'


export default class DepartmentDetailComponent extends Component{

  constructor(props){
    super(props)
    this.state = {
        isOpen: false,
        isDelete:false,
        keyValue:''
    }
  }

  toggleDepartmentModal = (dep_slug) => {   
    this.setState({ isOpen: !this.state.isOpen ,
      keyValue : dep_slug
    });
  }
  toggleDeleteDepartment = (dep_slug) => {
    window.scrollTo(0, 0)
    this.setState({ isDelete: !this.state.isDelete ,
      keyValue : dep_slug          
    });
    
  }

    render(){
      return(
          <Fragment>          
            {((this.props.allDepartments)&&(this.props.allDepartments.length>0))&&((this.props.testCondition)||((this.props.branch_dict)&&(this.props.branch_slug in this.props.branch_dict)))?
              <Fragment>
              <h6>DEPARTMENT</h6>
              <div className="col-md-12">
                <div className="row">
              {this.props.allDepartments.map((department,index)=>            
                  <Fragment key={index}>
                  {(department.branch === this.props.branch_slug)&&
                    <div className="col-md-4 float-left">
                      <div className="bx-content position-relative bx-bg-col">
                        {this.props.editable&&
                          <div className="project-options up-org-opt">
                            <button onClick={()=>this.toggleDepartmentModal(department.slug)} className="btn btn-link-secondary dept-edit show-form-modal" data-url="/departments/ajax-update-department/bfe331cc-24ff-4ad0-aad4-c5a155df5683/">
                              <i className="icon-pencil"></i>
                            </button>
                            <button className="btn btn-link-secondary"  onClick={()=>this.toggleDeleteDepartment(department.slug)}>
                              <i className="icon-trash"></i>
                            </button>
                          </div>
                        }
                        {(this.state.isOpen && (this.state.keyValue=== department.slug)) && <AddDepartment {...this.props} department_obj={department} edit={true} onCloseModal={this.toggleDepartmentModal}/> }
                        {(this.state.isDelete && (this.state.keyValue=== department.slug)) && <DeleteModal deleteType="department" {...this.props} slug={department.slug} name={department.department_name} onCloseModal={this.toggleDeleteDepartment}/> }
                          
                          <div className="bx-content-header pl-3 pt-3 pr-3">
                            <div className="bx-content-item">
                              <span className="user-img-holder user-img-holder1 dept-updt yellow">{department.department_name.charAt(0)}  </span>
                              <span className="project-details project-details1">
                                <span className="project-details-title"><h6 className="m-0">{department.department_name} </h6></span>
                              </span>
                            </div>
                          </div>         
                          <div className="bx-content-body pl-3 pb-3 pr-3 text-muted text-sm">
                          </div>
                      </div>
                    </div>
                  }
                  </Fragment>
              
                )}
                </div>
              </div>
              </Fragment>
              :''
            }
              
          </Fragment>
        )
    }
}






