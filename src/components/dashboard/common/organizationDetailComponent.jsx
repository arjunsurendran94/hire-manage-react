import React, { Component } from "react";
import DepartmentDetailComponent from '../../../components/dashboard/common/departmentDetailComponent'
import OrganizationEditModal from '../modal/editOrganizationModal'
import DeleteModal from '../modal/deleteModal'
import { Redirect } from 'react-router-dom';

class OrganizationDetailComponent extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isOpen: false,
      show_succes: true,
      isDelete: false,
      org_name: '',
      org_del_sucess: false,
      show_failure: false,
      del_org: true,
      del_branch: true,
    }
  }
  componentDidMount() {
    if (this.props.departmentlist && this.props.departmentlist.organization_department.length <= 0) {
      this.props.retrieveDepartments('');
    }
  }

  toggleOrganizationModal = () => {
    if (this.props.organization && this.props.organization.status === 'success') {
      this.props.clearstatus()

    }
    this.setState({ isOpen: !this.state.isOpen });
  }
  closeSucess = () => {
    this.setState({ show_succes: !this.state.show_succes });
    window.location.reload()
  }

  closeCantDelete = () => {
    this.setState({ del_org: false })
    window.location.reload()
  }
  closeCantDeleteBranch = () => {
    this.setState({ del_branch: false })
    window.location.reload()
  }

  toggleDeleteOrganization = () => {
    window.scrollTo(0, 0)
    this.setState({
      isDelete: !this.state.isDelete,
      org_del_sucess: true,

    });
    if (this.props.organization && this.props.organization.organization_data) {
      this.setState({ org_name: this.props.organization.organization_data.organization_name })
    }
  }


  render() {


    if (this.props.organizationDelete && this.props.organizationDelete.organization_delete && this.state.org_del_sucess) {
      return <Redirect
        to={{
          pathname: '/dashboard',
          state: { name: this.state.org_name }
        }}
      />
    }
    return (
      <div className="bx-content shadow">
        <div className="grid-contract">
          <div className="grid-contract-body bg-body-theme p-3 bg-white">
            <h5 className="col-md-3 ">Organization</h5>

            {(this.state.show_succes && this.props.departmentDelete && this.props.departmentDelete.department_delete) &&
              <div className="col-sm-12 ">
                <div className="alert alert-dismissible fade show alert-warning-green mt-3">
                  <i className="icon-checked b-6 alert-icon mr-2"></i> Department {this.props.departmentDelete.department_name} deleted sucessfully
                          <button onClick={this.closeSucess} type="button" className="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                </div>
              </div>
            }

            {(this.state.show_succes && this.props.departmentDelete && this.props.departmentDelete.error && this.props.departmentDelete.error.data) &&
              <div className="col-sm-12 ">
                <div className="alert alert-dismissible fade show alert-danger mt-3">
                  <i className="icon-checked b-6 alert-icon mr-2"></i> This Department is associated with an employee so cannot be deleted 
                          <button onClick={this.closeSucess} type="button" className="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                </div>
              </div>
            }

            {(this.state.show_succes && this.props.branchDelete && this.props.branchDelete.branch_delete) &&
              <div className="col-sm-12 ">
                <div className="alert alert-dismissible fade show alert-warning-green mt-3">
                  <i className="icon-checked b-6 alert-icon mr-2"></i> Branch {this.props.branchDelete.branch_name} deleted sucessfully
                          <button onClick={this.closeSucess} type="button" className="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                </div>
              </div>
            }

            {/* {(this.state.show_succes&&this.props.organizationDelete&&this.props.organizationDelete.error)&&
                    <div class="col-sm-12 ">
                    <div class="alert alert-dismissible fade show alert-danger mt-3">
                      <i class="icon-warning-triangle alert-icon mr-2"></i> Cannot delete this Organization as this is protected 
                      <button onClick={this.closeSucess} type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                    </div>
                    </div>
                    } */}

            {this.props.editable &&
              <div className="project-options up-org-opt">
                {(this.props.organzationList && this.props.organzationList.data && (this.props.organzationList.data.length > 1)) &&
                  <button onClick={this.toggleDeleteOrganization} className="btn btn-link-secondary" data-toggle="modal" data-id="62437483-98dc-48fc-ae87-863b47a1e058">
                    <i className="icon-trash"></i>
                  </button>
                }
                <button className="btn btn-link-secondary show-form-modal" data-url="/organization/update-organization/f33d0eee-a1eb-4784-b00e-b3d9645c081f/" onClick={this.toggleOrganizationModal}>
                  <i className="icon-pencil"></i>
                </button>
              </div>
            }
            {(this.state.del_org && this.props.organizationDelete && this.props.organizationDelete.org_delete_error) &&
              <div className="col-sm-12 ">
                <div className="alert alert-dismissible fade show alert-danger mt-3">
                  <i className="icon-warning-triangle alert-icon mr-2"></i> Cannot delete this Organization as this is protected
                        <button type="button" className="close" data-dismiss="alert" aria-label="Close" onClick={this.closeCantDelete}>
                    <span aria-hidden="true">×</span>
                  </button>
                </div>
              </div>
            }
            {(this.state.del_branch && this.props.branchDelete && this.props.branchDelete.branch_delete_error) &&
              <div className="col-sm-12 ">
                <div className="alert alert-dismissible fade show alert-danger mt-3">
                  <i className="icon-warning-triangle alert-icon mr-2"></i> Cannot delete this branch as this is protected
                        <button type="button" className="close" data-dismiss="alert" aria-label="Close" onClick={this.closeCantDeleteBranch}>
                    <span aria-hidden="true">×</span>
                  </button>
                </div>
              </div>
            }


            {this.state.isDelete && <DeleteModal deleteType="organization" onCloseModal={this.toggleDeleteOrganization} {...this.props} />}
            {this.state.isOpen && <OrganizationEditModal modalClose={this.toggleOrganizationModal} {...this.props} />}
            <ul className="mt-4 fullwidth float-left position-relative update-ul  brd-btm-col  ">
              <div className="d-inline-block u-br-icon">
                {(this.props.organization && this.props.organization.organization_data && this.props.organization.organization_data.logo) ?
                  <div className="branches-ic up-branch-icon">

                    <img src={this.props.organization.organization_data.logo} alt="Logo" style={{ width: "65px", height: "65px", borderRadius: "50%" }} />
                  </div> :
                  <div className="branches-ic up-branch-icon">

                    <img src={require("../../../static/assets/image/loc-ic.png")} alt="Logo" />
                  </div>
                }
              </div>
              <li className="col-xl-3 col-lg-4 col-md-6 col-sm-6 float-left mb-3">
                <div>
                  <span className="b-6">ORGANIZATION NAME</span>
                </div>
                <div className="text-muted text-sm">
                  {(this.props.organization && this.props.organization.organization_data) ?
                    this.props.organization.organization_data.organization_name :
                    "Nill"}
                </div>
              </li>
              <li className="col-xl-3 col-lg-4 col-md-6 col-sm-6 float-left mb-3">
                <div>
                  <span className="b-6">COUNTRY</span>
                </div>
                <div className="text-muted text-sm">
                  {(this.props.organization && this.props.organization.organization_data && this.props.organization.organization_data.country) ?
                    this.props.organization.organization_data.country.name_ascii :
                    "Nill"}
                </div>
              </li>
              <li className="col-xl-3 col-lg-4 col-md-6 col-sm-6 float-left mb-3">
                <div>
                  <span className="b-6">STATE</span>
                </div>
                <div className="text-muted text-sm">
                  {(this.props.organization && this.props.organization.organization_data && this.props.organization.organization_data.state) ?
                    this.props.organization.organization_data.state.name_ascii :
                    "Nill"}
                </div>
              </li>
              <li className="col-xl-3 col-lg-4 col-md-6 col-sm-6 float-left mb-3">
                <div>
                  <span className="b-6">CITY</span>
                </div>
                <div className="text-muted text-sm">
                  {(this.props.organization && this.props.organization.organization_data && this.props.organization.organization_data.city) ?
                    this.props.organization.organization_data.city.name_ascii :
                    "Nill"}
                </div>
              </li>
              <li className="col-xl-3 col-lg-4 col-md-6 col-sm-6 float-left mb-3">
                <div>
                  <span className="b-6">ADDRESS</span>
                </div>
                <div className="text-muted text-sm">
                  {(this.props.organization && this.props.organization.organization_data && this.props.organization.organization_data.address) ?
                    this.props.organization.organization_data.address :
                    "Nill"}
                </div>
              </li>
              <li className="col-xl-3 col-lg-4 col-md-6 col-sm-6 float-left mb-3">
                <div>
                  <span className="b-6">WEBSITE</span>
                </div>
                <div className="text-muted text-sm">
                  {(this.props.organization && this.props.organization.organization_data && this.props.organization.organization_data.website) ?
                    this.props.organization.organization_data.website :
                    "Nill"}
                </div>
              </li>
            </ul><div className="clearfix"></div>

            {(!this.props.searchstatus) && (this.props.departmentFlag) && ((this.props.departmentlist) && (this.props.departmentlist.organization_department) && (this.props.departmentlist.organization_department.length > 0)) &&
              <DepartmentDetailComponent testCondition={true} branch_slug={null} allDepartments={this.props.departmentlist.organization_department} branch_dict={false} {...this.props} />
            }

            {(this.props.searchstatus) && (this.props.departmentFlag) && ((this.props.departmentlist) && (this.props.departmentlist.search_organization_department) && (this.props.departmentlist.search_organization_department.length > 0)) &&
              <DepartmentDetailComponent testCondition={true} branch_slug={null} allDepartments={this.props.departmentlist.search_organization_department} branch_dict={false} {...this.props} />
            }


          </div>
        </div>
      </div>
    )
  }
}

export default OrganizationDetailComponent