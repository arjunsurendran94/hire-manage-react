import React, { Component, Fragment } from 'react'
import AddDepartment from '../modal/addDepartmentModal'
import OrganizationEditModal from '../modal/editOrganizationModal'
import { Link } from 'react-router-dom'
import AddBranch from '../modal/addBranchModal'
import DeleteModal from '../modal/deleteModal'
import EmployeeStatusModal from '../modal/employeeStatusModal'
import { Redirect } from 'react-router-dom';
import SuccessMessage from '../../common/SuccessMessage'

import { submit } from 'redux-form'



class DashBoardSubheader extends Component {

    constructor(props) {
        super(props);
        this.state = {
            add_department_modal: false,
            add_branch_modal: false,
            add_organization: false,
            show_message: false,
            show_branch_dropdown: false,
            search_box_value: '',
            dropdownValue: "all_dept",
            branchSlug: '',
            isDelete: false,
            role_del_sucess: false,
            role_name: '',
            designation_delete_modal: false,
            employee_delete_modal: false,
            employee_status_modal: false,
            employeeDelete: false,
            designation_delete_sucess: false,
            toActive: '',
            employeeSelect: '',
            clientcreatesuccess: false,

            obj_list: {
                q: '',
                branch: '',
                listing: '',
                status: '',
                department: '',
                page: ''
            }

            // value:''
        };
    }

    CloseMsg = () => {
        this.setState({ clientcreatesuccess: false })
    }

    componentDidUpdate(prevProps) {

        if (prevProps.clientData != this.props.clientData && (this.props.clientData && this.props.clientData.status === 'success')) {
            this.setState({ clientcreatesuccess: true })
        }
    }

    componentDidMount() {
        if (this.props.organizationDelete && this.props.organizationDelete.organization_delete) {
            this.setState({ show_message: true })
        }
        if (this.props.subheader === 'Employees') {
            this.props.retrieveBranches(localStorage.getItem('organization_slug'), localStorage.getItem('token'))
            this.props.allDepartmentsListing('')
        }
    }
    toggleDepartmentModal = () => {
        this.setState({
            add_department_modal: !this.state.add_department_modal
        })
    }
    toggleBranchModal = () => {
        this.setState({
            add_branch_modal: !this.state.add_branch_modal
        })
    }
    closeDeleteMessage = () => {
        this.setState({
            show_message: false
        })
    }
    toggleOrganizationModal = () => {
        this.setState({
            add_organization: !this.state.add_organization
        })
    }

    search = (e) => {

        if (this.props.subheader === 'Branches') {
            this.props.searchBranch(e.target[0].value)
        }
        if (this.props.subheader === 'Departments') {

            this.props.searchOrganizationDepartments(this.state.search_box_value,
                this.state.dropdownValue, this.state.branchSlug)
        }

        if (this.props.subheader === 'Employees') {
            let name = e.target[0].name
            let value = e.target[0].value
            this.setState({
                ...this.state, obj_list: {
                    ...this.state.obj_list, [name]: value
                }
            }, () => {
                this.props.searchFilterEmployees(this.state.obj_list)
            })

        }
        e.preventDefault();

    }

    handleChange = (e) => {

        this.setState(state => ({
            show_branch_dropdown: !state.show_branch_dropdown, dropdownValue: e.currentTarget.value
        }));
        this.props.searchOrganizationDepartments(this.state.search_box_value,
            e.currentTarget.value, this.state.branchSlug)

        this.props.changeSearchStatus()
    }

    handleInputChange = (e) => {
        if (this.props.subheader === 'Departments') {

            this.setState({ search_box_value: e.target.value })
            this.props.changeSearchStatus()
            this.props.searchOrganizationDepartments(e.target.value,
                this.state.dropdownValue, this.state.branchSlug)

        }

        if (this.props.subheader === 'Branches') {
            this.props.searchBranch(e.target.value)
        }

        if (this.props.subheader === 'Designations') {
            this.props.searchDesignations(e.target.value)
        }

        if (this.props.subheader === 'Roles') {
            this.props.roleSearch(e.target.value)
        }

        if (this.props.subheader === 'Clients') {
            this.props.searchClients(e.target.value)
        }

        if (this.props.subheader === 'Projects') {
            this.props.searchProjects(e.target.value)
        }

    }

    handleBranchChange = (e) => {
        this.setState({ branchSlug: e.currentTarget.value })
        this.props.searchOrganizationDepartments(this.state.search_box_value,
            this.state.dropdownValue, e.currentTarget.value)
        this.props.changeSearchStatus()
    }


    toggleDeleteRole = () => {
        window.scrollTo(0, 0)
        this.setState({
            isDelete: !this.state.isDelete,
            role_del_sucess: true,

        });
        if (this.props.roleObject && this.props.roleObject.roleData) {
            this.setState({ role_name: this.props.roleObject.roleData.group_name })
        }
    }

    toggleDeleteDesignation = () => {
        this.setState({
            isDelete: !this.state.isDelete,
            designation_delete_modal: !this.state.designation_delete_modal,
            designation_delete_sucess: true
        })
    }
    toggleDeleteEmployee = () => {
        this.setState({ employee_delete_modal: !this.state.employee_delete_modal })
    }

    inActivateEmployee = () => {
        this.setState({ employee_status_modal: !this.state.employee_status_modal, toActive: "active_to_inactive" })
    }
    activateEmployee = () => {
        this.setState({ employee_status_modal: !this.state.employee_status_modal, toActive: "inactive_to_active" })
    }

    employeeName = (employeeData) => {
        const employee_name = employeeData.first_name + ' ' + employeeData.last_name
        return employee_name
    }

    cannotDelete = () => {
        this.setState({
            employeeDelete: !this.state.employeeDelete
        })
    }

    statusChange = (event) => {
        this.props.employeeStatusValue(event.target.value)
    }



    handleEmployeeChange = (e) => {
        let name = e.target.name
        let value = e.target.value
        this.setState({
            ...this.state, obj_list: {
                ...this.state.obj_list, [name]: value
            }
        }, () => {
            if (name === 'listing' && value === 'all') {
                window.location.reload()
            }
            this.props.searchFilterEmployees(this.state.obj_list)
        })
    }

    clearMessages = () => {
        this.props.clearSuccessMessages()
    }

    createProjectRedirect = () => {

        window.location.href = "/projects/add-new-project/"
    }

    render() {
        
        

        let permissions = []
        permissions = localStorage.getItem('permissions')
        if (permissions && permissions.length > 0) {
            permissions = permissions.split(',')
        }

        const { dispatch } = this.props;
        if (this.props.desigantions && this.props.desigantions.deletestatus === 'success' && this.state.designation_delete_sucess) {
            return <Redirect
                to={{
                    pathname: '/departments/designations/',
                    state: { delete: true }
                }}
            />
        }
        if (this.props.organizationCreatedData && this.props.organizationCreatedData.status === 'success') {
            window.location.reload()
        }
        if (this.props.organizationDelete && this.props.organizationDelete.organization_delete && this.props.organzationList && this.props.organzationList.data) {
            localStorage.setItem("organization_slug", this.props.organzationList.data[0].slug)
        }
        if (this.props.roleDeleted && this.props.roleDeleted.role_delete && this.state.role_del_sucess) {
            return <Redirect
                to={{
                    pathname: '/permission-groups/',
                    state: { name: this.state.role_name }
                }}
            />
        }
        if (this.props.employees && this.props.employees.employeeStatusChange) {
            window.location.reload()
            return <Redirect to='/employees/employees/' />
        }

        return (
            <Fragment>

                <div className="sub-header" 
                style={{lineHeight:this.props.subheader==="View Project"?"40px":null,
                height:this.props.subheader==="View Project"?"40px":null,
                padding:this.props.subheader==="View Project"?"0px":null,}}
                >
                    {
                        (this.props.subheader === "Dashboard") &&
                        <div className="col-sm-12 mt-3">
                    <div className="col-sm">
                        {/* <div className="mt-5 justify-content-center"> */}
                            <div className="align-self-center text-center">
                            <h1>Welcome to Dashboard</h1>
                            
                            {/* </div> */}
                        </div>
                    </div>
                </div>
                    }
                    <div className="bx-inline-block">
                        {
                            ((this.props.subheader === "Dashboard") || (this.props.subheader === "Branches") ||
                                (this.props.subheader === "Departments") || (this.props.subheader === "Settings") ||
                                (this.props.subheader === "My Profile") || (this.props.subheader === "Employees") ||
                                (this.props.subheader === "Roles") || (this.props.subheader === "Designations")) ||
                            (this.props.subheader === "View Client") &&
                            <h6 className="text-grey">
                                <nav aria-label="breadcrumb">
                                    <ol className="breadcrumb">
                                        <li className="breadcrumb-item active" aria-current="page">{this.props.subheader}</li>
                                    </ol>
                                </nav>
                            </h6>
                        }
                        {
                            ((this.props.subheader === "organization") && (this.props.organization) && (this.props.organization.organization_data)) &&
                            <h6 className="text-grey">
                                <nav aria-label="breadcrumb">
                                    <ol className="breadcrumb">
                                        <li className="breadcrumb-item active" aria-current="page">{this.props.organization.organization_data.organization_name}</li>
                                    </ol>
                                </nav>
                            </h6>
                        }
                         
                        {((this.props.subheader === "Departments") || (this.props.subheader === "Branches") ||
                            (this.props.subheader === "Employees") || (this.props.subheader === "Roles") ||
                            (this.props.subheader === 'Clients') || (this.props.subheader === 'Projects') || (this.props.subheader === "Designations")) &&
                            <div className="search-group inline-bx">
                                <form id="search-box" onSubmit={(e) => this.search(e)}>
                                    <input name="q" onChange={(e) => this.handleInputChange(e)} type="text" className="input-control" placeholder="Search..." />
                                    <span className="addon-btn">
                                        <button type="submit" href="" className="btn btn-black">
                                            <i className="icon-search"></i>
                                        </button>
                                    </span>
                                </form>
                            </div>
                        }
                        {((this.props.subheader === "Departments")) &&
                            <div className="search-group inline-bx search-box">
                                <select onChange={this.handleChange} name="listing" className="input-control">
                                    <option value="all_departments">All Departments</option>
                                    <option value="branches_only">Branches only</option>
                                </select>
                            </div>
                        }

                        {
                            (this.props.subheader === "Departments" && this.state.show_branch_dropdown) &&
                            <div className="search-group inline-bx search-box">
                                <select name="branch" onChange={this.handleBranchChange} form="search-box" className="input-control">
                                    <option value="branches_only">All Branches</option>
                                    {this.props.branches && this.props.branches.data && this.props.branches.data.map((branch) =>

                                        <option value={branch.slug}>{branch.branch_name}</option>

                                    )}
                                </select>
                            </div>
                        }

                        {((this.props.subheader === "Add Department")) &&
                            <h6 className="text-grey">
                                <nav aria-label="breadcrumb">
                                    <ol className="breadcrumb">
                                        <li className="breadcrumb-item">
                                            <a href="/departments/departments/">Departments</a>
                                        </li>
                                        <li className="breadcrumb-item active" aria-current="page">
                                            Add Department
                                        </li>
                                    </ol>
                                </nav>
                            </h6>
                        }
                        {((this.props.subheader === "Add Branches")) &&
                            <h6 className="text-grey">
                                <nav aria-label="breadcrumb">
                                    <ol className="breadcrumb">
                                        <li className="breadcrumb-item">
                                            <a href="/organization/branches/">Branches</a>
                                        </li>
                                        <li className="breadcrumb-item active" aria-current="page">
                                            Add Branch
                                        </li>
                                    </ol>
                                </nav>
                            </h6>
                        }
                        {((this.props.subheader === "Add Client")) &&
                            <h6 className="text-grey">

                                <nav aria-label="breadcrumb">
                                    <ol className="breadcrumb">
                                        <li className="breadcrumb-item">
                                            <a href="/clients/clients/">Clients</a>
                                        </li>
                                        <li className="breadcrumb-item active" aria-current="page">
                                            Create Client
                                        </li>
                                    </ol>
                                </nav>
                            </h6>


                        }
                        {((this.props.subheader === "View Task")) &&
                            <h6 className="text-grey">

                                <nav aria-label="breadcrumb">
                                    <ol className="breadcrumb">
                                        <li className="breadcrumb-item">
                                            <Link to={{ pathname: `/projects/project/` }}>Tasks</Link>
                                        </li>
                                        <li className="breadcrumb-item active" aria-current="page">
                                            View task
                                        </li>
                                    </ol>
                                </nav>
                            </h6>


                        }


                        {this.props.subheader === "Add Project" &&
                            <h6 className="text-grey">

                                <nav aria-label="breadcrumb">
                                    <ol className="breadcrumb">
                                        <li className="breadcrumb-item">
                                            <a href="/projects/projects/">Projects</a>
                                        </li>
                                        <li className="breadcrumb-item active" aria-current="page">
                                            Create Project
                                        </li>
                                    </ol>
                                </nav>
                            </h6>
                        }





                        {((this.props.subheader === "View Client")) &&
                            <h6 className="text-grey">

                                <nav aria-label="breadcrumb">
                                    <ol className="breadcrumb">
                                        <li className="breadcrumb-item">
                                            <Link to="/clients/clients/">Clients</Link>
                                        </li>
                                        <li className="breadcrumb-item active" aria-current="page">
                                            View Client
                                        </li>
                                    </ol>
                                </nav>
                            </h6>


                        }
                        {((this.props.subheader === "View Project")) &&
                            <h6 className="text-grey">

                                <nav aria-label="breadcrumb" style={{marginLeft:"19px"}}>
                                    <ol className="breadcrumb">
                                        <li className="breadcrumb-item">
                                            <Link to="/projects/projects/">Projects</Link>
                                        </li>
                                        <li className="breadcrumb-item active" aria-current="page">
                                        {this.props.projectData&&this.props.projectData.projectObj&&
                                        this.props.projectData.projectObj.project&&
                                        this.props.projectData.projectObj.project.project_name
                                        }
                                        </li>
                                    </ol>
                                </nav>
                            </h6>


                        }
                        {
                            (this.props.subheader === "Employees") &&
                            <div className="search-group inline-bx search-box">
                                <select value={this.state.obj_list.listing} onChange={this.handleEmployeeChange} name="listing" form="search-box" className="input-control">
                                    <option value="all">All Employees</option>
                                    <option value="branches">Branches only</option>
                                    <option value="general">Organization Departments</option>
                                </select>
                            </div>
                        }
                        {
                            (this.props.subheader === "createEmployee") &&
                            <h6 className="text-grey">
                                <nav aria-label="breadcrumb">
                                    <ol className="breadcrumb">
                                        <li className="breadcrumb-item">
                                            <a href="/employees/employees/">Employees</a>
                                        </li>
                                        <li className="breadcrumb-item active" aria-current="page">
                                            Invite Employee
                                        </li>
                                    </ol>
                                </nav>
                            </h6>
                        }


                        {
                            (this.props.subheader === "Employees") &&
                            <div className="search-group search-box inline-bx">
                                <select name="status" form="search-box" className="input-control" onChange={this.handleEmployeeChange}>
                                    <option value="active">Active</option>
                                    <option value="inactive">In-Active</option>
                                    <option value="invited">Invited</option>
                                </select>
                            </div>
                        }

                        {
                            (this.state.obj_list.listing === 'branches') &&
                            <div className="search-group inline-bx search-box">
                                <select name="branch" form="search-box" class="input-control" onChange={this.handleEmployeeChange}>
                                    <option value="">All Branches</option>
                                    {this.props.branches && this.props.branches.data && this.props.branches.data.map((branch) =>
                                        <option value={branch.slug}>{branch.branch_name}</option>

                                    )}


                                </select>
                            </div>
                        }

                        {(this.state.obj_list.listing === 'general') && <div className="search-group search-box d-inline-block">
                            <select name="department" form="search-box" class="input-control" onChange={this.handleEmployeeChange}>
                                <option value="">All Departments</option>

                                {this.state.obj_list.listing === 'general' && this.props.departmentlist && this.props.departmentlist.alldepartments &&
                                    this.props.departmentlist.alldepartments.filter(dept => dept.branch === null).map((dept) =>
                                        <option value={dept.slug}>{dept.department_name}</option>
                                    )}

                            </select>
                        </div>}

                        {/* {(this.state.obj_list.listing==='general') && <div className="search-group search-box d-inline-block">
                            <select name="department" form="search-box" class="input-control" onChange={this.handleEmployeeChange}>
                                <option value="">All Departments</option>

                                {this.state.obj_list.listing==='general'&& this.props.departmentlist&&this.props.departmentlist.alldepartments&&
                                this.props.departmentlist.alldepartments.filter(dept=>dept.branch===null).map((dept)=>
                                    <option value={dept.slug}>{dept.department_name}</option>
                                )}

                            </select>
                        </div>} */}


                        {
                            (this.props.subheader === "Add Designation") &&
                            <h6 className="text-grey">
                                <nav aria-label="breadcrumb">
                                    <ol className="breadcrumb">
                                        <li className="breadcrumb-item">
                                            <a href="/departments/designations/">Designation</a>
                                        </li>
                                        <li className="breadcrumb-item active" aria-current="page">
                                            {this.props.update ? 'Update' : 'Add'} Designation
                                        </li>
                                    </ol>
                                </nav>
                            </h6>
                        }
                        {
                            (this.props.subheader === "Add Roles" || this.props.subheader === "Update Roles") &&
                            <h6 className="text-grey">
                                <nav aria-label="breadcrumb">
                                    <ol className="breadcrumb">
                                        <li className="breadcrumb-item">
                                            <a href="/permission-groups/">Roles</a>
                                        </li>
                                        <li className="breadcrumb-item active" aria-current="page">
                                            {this.props.subheader === "Update Roles" ? 'Update ' : 'Add New '}Role
                                            </li>
                                    </ol>
                                </nav>
                            </h6>
                        }
                        {
                            (this.props.subheader === "roleDetails") &&
                            <h6 className="text-grey">
                                <nav aria-label="breadcrumb">
                                    <ol className="breadcrumb">
                                        <li className="breadcrumb-item">
                                            <a href="/permission-groups/">Roles</a>
                                        </li>
                                        <li className="breadcrumb-item active" aria-current="page">
                                            {(this.props.roleObject && this.props.roleObject.roleData) &&
                                                this.props.roleObject.roleData.group_name
                                            }
                                        </li>
                                    </ol>
                                </nav>
                            </h6>
                        }
                        {
                            (this.props.subheader === "employeeDetail") &&
                            <h6 className="text-grey">
                                <nav aria-label="breadcrumb">
                                    <ol className="breadcrumb">
                                        <li className="breadcrumb-item">
                                            <a href="/employees/employees/">Employees</a>
                                        </li>
                                        <li className="breadcrumb-item active" aria-current="page">
                                            {(this.props.employeeObject && this.props.employeeObject.employeeData) && (
                                                // (this.props.employeeObject.employeeData.first_name ,this.props.employeeObject.employeeData.last_name)
                                                this.employeeName(this.props.employeeObject.employeeData)
                                            )

                                            }
                                        </li>
                                    </ol>
                                </nav>
                            </h6>
                        }
                    </div>
                    {((this.props.subheader === "Dashboard") && this.state.show_message && this.props.location.state && this.props.location.state.name) &&
                        <div className="alert alert-dismissible fade show alert-warning-green mt-3">
                            <i className="icon-checked b-6 alert-icon mr-2"></i> Organization {this.props.location.state.name} deleted sucessfully
                            <button type="button" className="close" data-dismiss="alert" aria-label="Close" onClick={this.closeDeleteMessage}>
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                    }

                    {
                        (this.props.subheader === "organization") &&
                        <span className="float-right">
                            <a className="btn btn-link-vilot" href="/organization/settings/b0551b07-da11-48d2-95a6-14fefe7129a6/">
                                <i className="icon-gear b-6 h2"></i>
                                <span className="btn-title">Settings</span>
                            </a>
                            <button className="btn btn-link-vilot show-form-modal" onClick={() => this.toggleDepartmentModal('dept')} >
                                <i className="icon-add-folder b-6 h2"></i>
                                <span className="btn-title">Add Department</span>
                            </button>
                            <button className="btn btn-link-vilot show-form-modal" onClick={() => this.toggleBranchModal('branch')}>
                                <i className="icon-add-folder b-6 h2"></i>
                                <span className="btn-title">Add Branch</span>
                            </button>
                        </span>
                    }
                    {
                        (this.props.subheader === "Branches") &&
                        <span className="float-right">
                            <Link to="/organization/add-new-branch/" className="btn btn-link-vilot">
                                <i className="icon-add-user b-6 h2"></i>
                                <span className="btn-title">Add New Branch</span>
                            </Link>
                        </span>

                    }
                    {
                        (this.props.subheader === "Clients" || this.props.subheader === "View Client") &&
                        <span className="float-right">
                            <Link to="/clients/add-new-client/" className="btn btn-link-vilot">
                                <i className="icon-add-user b-6 h2"></i>
                                <span className="btn-title">Add New Client</span>
                            </Link>
                        </span>
                    }


                    {(localStorage.getItem('user_type') === 'admin' ||
                        (permissions && permissions.includes('add_project'))) && <Fragment>
                            {
                                (this.props.subheader === "Projects" ) &&
                                <span className="float-right">

                                    <button onClick={() => this.createProjectRedirect()} className="btn btn-link-vilot">
                                        <i className="icon-add-user b-6 h2"></i>
                                        <span className="btn-title">Add New Project</span>
                                    </button>
                                </span>
                            }
                        </Fragment>}

                    {(this.props.subheader === "Add Client") &&
                        <span className="float-right">
                            <button className="btn btn-link-vilot" form="create-client-form" onClick={() => this.props.submitclientform()}
                            >
                                <i className="icon-save b-6 h2"></i>
                                <span className="btn-title">Save</span>
                            </button>
                        </span>
                    }

                    {/* {(this.props.subheader === "Add Project") &&
                        <span className="float-right">
                            <button className="btn btn-link-vilot" form="create-client-form" onClick={() => this.props.submitprojectform()}
                            >
                            <i className="icon-save b-6 h2"></i>
                            <span className="btn-title">Save</span>
                            </button>
                        </span>
                    } */}

                    {
                        this.state.clientcreatesuccess && (
                            <SuccessMessage closemsg={this.CloseMsg}
                                msg={`Client ${this.props.clientData && this.props.clientData.client_obj && this.props.clientData.client_obj.client
                                    && this.props.clientData.client_obj.client.first_name}  ${this.props.clientData && this.props.clientData.client_obj && this.props.clientData.client_obj.client && this.props.clientData.client_obj.client.last_name}   
                         Created Successfully`} />)
                    }


                    {
                        (this.props.subheader === "My Profile") &&
                        <span className="float-right">
                            <button onClick={this.toggleOrganizationModal} className="btn btn-link-vilot show-form-modal" data-url="/organization/add-organization/">
                                <i className="icon-add-user b-6 h2"></i>
                                <span className="btn-title">Add New Organization</span>
                            </button>
                        </span>
                    }
                    
                    {
                        (this.props.subheader === "Employees") &&
                        <span className="float-right">
                            <Link to="/employees/create-employee/" className="btn btn-link-vilot">
                                <i className="icon-add-user b-6 h2"></i>
                                <span className="btn-title">Invite New Employee</span>
                            </Link>
                        </span>
                    }
                    {
                        (this.props.subheader === "Roles") &&
                        <span className="float-right">
                            <a href="/add-permission-group/" className="btn btn-link-vilot">
                                <i className="icon-add-user b-6 h2"></i>
                                <span className="btn-title">Add New Role</span>
                            </a>
                        </span>
                    }
                    {
                        (this.props.subheader === "Designations") &&

                        <span className="float-right">
                            <Link to="/departments/add-designation/" onClick={this.clearMessages} className="btn btn-link-vilot">
                                <i className="icon-add-user b-6 h2"></i>
                                <span className="btn-title">Add New Designation</span>
                            </Link>
                        </span>
                    }
                    {
                        (this.props.subheader === "DesignationDetail") &&
                        <span className="float-right">
                            <Link to={`/departments/update-designation/${this.props.match.params.designation_slug}`} >
                                <button className="btn btn-link-danger">
                                    <i className="icon-edit b-6 h1"></i>
                                    <span className="btn-title mt-1">Update</span>
                                </button>
                            </Link>
                            <button onClick={this.toggleDeleteDesignation} className="btn btn-link-danger" data-toggle="modal" data-target="#deleteconfirmModal">
                                <i className="icon-delete b-6 h1"></i>
                                <span className="btn-title mt-1">Delete</span>
                            </button>
                        </span>
                    }

                    {(this.props.subheader === "DesignationDetail") &&
                        <div className="bx-inline-block">
                            <h6 className="text-grey">
                                <nav aria-label="breadcrumb">
                                    <ol className="breadcrumb">
                                        <li className="breadcrumb-item">
                                            <Link to="/departments/designations/">Designations</Link>
                                        </li>
                                        <li className="breadcrumb-item active" aria-current="page">
                                            Update Designation
                                        </li>
                                    </ol>
                                </nav>
                            </h6>
                        </div>}
                    {

                        (this.props.subheader === "roleDetails") &&
                        <span className="float-right">
                            <Link className="btn btn-link-danger" to={`/permission-update/${this.props.match.params.role_slug}`} >
                                <i className="icon-edit b-6 h1"></i>
                                <span className="btn-title mt-1">Update</span>
                            </Link>
                            <button onClick={this.toggleDeleteRole} className="btn btn-link-danger" data-toggle="modal" data-target="#deleteconfirmModal">
                                <i className="icon-delete b-6 h1"></i>
                                <span className="btn-title mt-1">Delete</span>
                            </button>
                        </span>
                    }

                    {
                        (this.props.subheader === "employeeDetail") && (this.props.employeeObject && this.props.employeeObject.employeeData && !this.props.employeeObject.employeeData.is_owner) &&
                        <span className="float-right">
                            {(this.props.employeeObject && this.props.employeeObject.employeeData && (this.props.employeeObject.employeeData.status === "Active" || this.props.employeeObject.employeeData.status === "Invited")) &&
                                <button onClick={this.inActivateEmployee} className="btn btn-link-danger " data-toggle="modal" data-target="#confirmModal">
                                    <i className="icon-checked-circle-o b-6 h1"></i>
                                    <span className="btn-title mt-1"> In-Activate </span>
                                </button>
                            }
                            {(this.props.employeeObject && this.props.employeeObject.employeeData && this.props.employeeObject.employeeData.status === "Inactive") &&
                                <button onClick={this.activateEmployee} className="btn btn-link-danger text-success" data-toggle="modal" data-target="#confirmModal">
                                    <i className="icon-checked-circle-o b-6 h1"></i>
                                    <span className="btn-title mt-1"> Activate </span>
                                </button>
                            }


                            <button onClick={this.toggleDeleteEmployee} className="btn btn-link-danger" data-toggle="modal" data-target="#employeedeleteconfirmModal">
                                <i className="icon-delete b-6 h1"></i>
                                <span className="btn-title mt-1">Delete</span>
                            </button>
                        </span>
                    }

                    {
                        (this.state.employeeDelete && this.props.subheader === "employeeDetail") &&
                        <div id="message" className="col-lg-12 float-left fullwidth">
                            <div className="alert alert-dismissible fade show alert-danger mt-3">
                                <i className="icon-warning-triangle alert-icon mr-2"></i> Cannot delete this employee as this is protected
                                <button onClick={this.cannotDelete} type="button" className="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                        </div>
                    }
                </div>
                {this.state.add_department_modal && <AddDepartment {...this.props} onCloseModal={this.toggleDepartmentModal} />}
                {this.state.add_branch_modal && <AddBranch {...this.props} onCloseModal={this.toggleBranchModal} />}
                {this.state.add_organization &&
                    <OrganizationEditModal profile_org={true} modalClose={this.toggleOrganizationModal}  {...this.props} />
                }
                {this.state.isDelete && <DeleteModal deleteType="role" onCloseModal={this.toggleDeleteRole} {...this.props} />}

                {this.state.designation_delete_modal && <DeleteModal deleteType="designation" onCloseModal={this.toggleDeleteDesignation} {...this.props} />}
                {this.state.employee_delete_modal && <DeleteModal deleteType="employee" onCloseModal={this.toggleDeleteEmployee} cannotDelete={this.cannotDelete} name={(this.props.employeeObject && this.props.employeeObject.employeeData) && (this.employeeName(this.props.employeeObject.employeeData))} {...this.props} />}
                {this.state.employee_status_modal && <EmployeeStatusModal onCloseModal={this.inActivateEmployee} name={(this.props.employeeObject && this.props.employeeObject.employeeData) && (this.employeeName(this.props.employeeObject.employeeData))} toActive={this.state.toActive} {...this.props} />}

            </Fragment>
        )
    }
}

export default DashBoardSubheader