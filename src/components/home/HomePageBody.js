import React ,{ Component } from 'react';


export default class Homebody extends Component{
    render(){
        return(
        <section className="slider-wrapper fullwidth pull-left position-relative d-table">
            <div className="overlay"></div>
            
            <div className="slider-content d-table-cell align-middle position-relative text-white">
                <div className="container">
                    <h1 className="log-fnt-40">Lorem Ipsum</h1>
                    <p className="log-fnt-16">Lorem ipsum dolor sit amet, consectetur adipiscing elit,<br /> sed do eiusmod tempor incididunt  </p>
                    <button type="button" className="bg text-white fullwidth pt-2 pb-2 log-fnt-16">Get Started</button>
                </div>
            </div>
        </section>
        )
    }
}