import React ,{ Component } from 'react';
import { Link } from 'react-router-dom';


export default class Footer extends Component{
    render(){
        return(
            <footer className="pull-left fullwidth" >
            <div className="footer-wrapper fullwidth pull-left">
                <div className="container">
                    <div className="un-line fullwidth pull-left pt-3 pb-3">
                        <ul className="pull-left text-white">
                            <li className="text-uppercase ftr-blk ft-link">about us</li>
                            <Link className="text-uppercase ftr-blk ft-link" to="/login">log in</Link>
                            <Link className="text-uppercase ftr-blk ft-link" to="/signup">sign up</Link>
                            <li className="text-uppercase ftr-blk ft-link">contact us</li>
                        </ul>
                        <ul className="pull-right m-0">
                            <div class="soc-mid">
                                <li class="d-inline social-list mr-3 pull-left"><Link class="pull-left"><i class="fa fa-facebook-f"></i></Link></li>
                                <li class="d-inline social-list mr-3 pull-left"><Link class="pull-left"><i class="fa fa-linkedin"></i></Link></li>
                                <li class="d-inline social-list pull-left"><Link class="pull-left"><i class="fa fa-twitter"></i></Link></li>
                            </div>
                        </ul>
                    </div>
                </div>
            </div>
            <div className="copyright fullwidth text-center pt-3 pb-3 pull-left text-white"> &copy;2019 HIRE & MANAGE.</div>
        </footer>
        )
    }
}