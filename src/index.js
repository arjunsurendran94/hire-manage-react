import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

import App from './App';

import './static/assets/css/bootstrap.min.css'
import './static/assets/css/main.css'
import './static/assets/css/custom.css'
import './static/assets/fonts/hire/style.css'
import './static/assets/plugins/scrollbar/jquery.scrollbar.css'

import './static/web/css/main.css'
import './static/web/css/custom.css'
import './static/web/css/responsive.css'

import './static/web/fonts/hire/style.css'



import rootReducer from './redux/reducers/index';

import * as serviceWorker from './serviceWorker';

import { createStore,applyMiddleware,compose } from 'redux';
import {Provider} from 'react-redux';

import thunk from "redux-thunk";

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(rootReducer, composeEnhancers(applyMiddleware(thunk)));


    ReactDOM.render(<Provider store={store}><App /></Provider>
        , document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
