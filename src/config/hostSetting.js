// ------------------------SERVER------------------------------------------------------------


// server frontend 
//  export const API_URL = 'http://202.88.246.92:8015'

// server backend
    // export const BACKEND_URL = 'http://202.88.246.92:8016'


// ---------- LOCAL--------------------------------------------------------------------------

// local frontend
 export const API_URL = 'http://localhost:3000'

// local backend
  export const BACKEND_URL = 'http://localhost:8000'


// -----------HEROKU---------------------------------------------------------------------

// export const API_URL = 'https://floating-taiga-37078.herokuapp.com'
// export const BACKEND_URL='https://thawing-eyrie-82605.herokuapp.com/'
