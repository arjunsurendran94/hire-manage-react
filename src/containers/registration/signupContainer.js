import { connect } from "react-redux";
import Signup from '../../components/registration/Signup'
import userSignUp from '../../redux/actions/signupAction';


const mapStateToProps = state=>{
    return{
        userSignupDetails:state.userSignup,
    }
}

const mapDispatchToProps = dispatch => ({

    userSignUp: (signUpData) => dispatch(userSignUp(signUpData)),
})

export default connect(mapStateToProps,mapDispatchToProps)(Signup)