import CompleteResetPassword from '../../components/registration/changePassword';
import { connect } from "react-redux";
import changePassword from '../../redux/actions/changePasswordAction';
import resetEmailVerification from '../../redux/actions/resetPasswordEmailVerification';
import employeeEmailVerification from '../../redux/actions/employeeEmailVerificationAction';


const mapStateToProps = state=>{
    return{
        changePasswordStatus:state.changePassword,
        email_response:state.resetEmailVerification,
        employeeVerification:state.employeeVerification
    }
}

const mapDispatchToProps = dispatch => ({
    employeeEmailVerification: (verificationData) => dispatch(employeeEmailVerification(verificationData)),
    changePassword: (urlData,password) => dispatch(changePassword(urlData,password)),
    resetEmailVerification: (verificationData) => dispatch(resetEmailVerification(verificationData)),
})

export default connect(mapStateToProps,mapDispatchToProps)(CompleteResetPassword)