import Login from '../../components/registration/login';
import { connect } from "react-redux";
import userLogin from '../../redux/actions/loginAction';


const mapStateToProps = state=>{
    return{
        loginDetails:state.userLogin,
        organzationList:state.organzationList,
        changePasswordStatus:state.changePassword
    }
}

const mapDispatchToProps = dispatch => ({
    userLogin: (loginData) => dispatch(userLogin(loginData)),
})

export default connect(mapStateToProps,mapDispatchToProps)(Login)