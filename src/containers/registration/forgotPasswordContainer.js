
import ForgotPassword from '../../components/registration/forgotPassword';
import { connect } from "react-redux";
import resetPassword from '../../redux/actions/resetPasswordAction.js';

const mapStateToProps = state=>{
    return{
        loginDetails:state.userLogin,
        organzationList:state.organzationList,
        resetPasswordStatus:state.resetPassword
    }
}

const mapDispatchToProps = dispatch => ({

    resetPassword: (email) => dispatch(resetPassword(email)),
})


export default connect(mapStateToProps,mapDispatchToProps)(ForgotPassword)