import ConfirmSignup from '../../components/registration/confirmSignup';
import emailVerification from '../../redux/actions/emailVerificationAction';
import registrationComplete from '../../redux/actions/registrationCompletionAction';
import fetchCountry from '../../redux/actions/countryAction';
import { connect } from "react-redux";


const mapStateToProps = state=>{
    return{
        isEmailVerified:state.emailVerificationData,
        countryDetails:state.countryData,
        registrationCompletion:state.registrationCompletion
    }
}

const mapDispatchToProps = dispatch => ({

    emailVerification: (verificationData) => dispatch(emailVerification(verificationData)),
    fetchCountryList: () => dispatch(fetchCountry()),
    registrationComplete: (registrationData) => dispatch(registrationComplete(registrationData))
})

export default connect(mapStateToProps,mapDispatchToProps)(ConfirmSignup)