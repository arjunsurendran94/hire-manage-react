import CreateBranch from "../../components/organization/createBranch";
import { connect } from "react-redux";

import fetchCountry from '../../redux/actions/countryAction';
import fetchStates from '../../redux/actions/stateAction';
import fetchcity from '../../redux/actions/citiesAction';
import CreateBranchAction from '../../redux/actions/createBranchAction';
import fetchUserBranch from '../../redux/actions/listBranch';
import retrieveOrganization from '../../redux/actions/retrieveOrganizationAction';

const mapStateToProps = state=>{
    
    return{

        organizationCreatedData:state.retrieveOrganization,
        branchCreatedData:state.createBranch,
        countryDetails:state.countryData,
        stateDetails:state.stateData,
        cityDetails:state.cityData,
        branchList:state.branchList

    }
}

const mapDispatchToProps = dispatch => ({
    fetchCountryList: () => dispatch(fetchCountry()),
    fetchStatesList: (country) => dispatch(fetchStates(country)),
    fetchCityList: (countryState) => dispatch(fetchcity(countryState)),
    createBranch: (values) => dispatch(CreateBranchAction(values)),
    fetchBranchList: (org_slug,token) => dispatch(fetchUserBranch(org_slug,token)),
    retrieveOrganization: () => dispatch(retrieveOrganization())
})

export default connect(mapStateToProps,mapDispatchToProps)(CreateBranch)