import CreateOrganization from '../../components/organization/createOrganization'
import { connect } from "react-redux";
import fetchCountry from '../../redux/actions/countryAction';
import fetchStates from '../../redux/actions/stateAction';
import fetchcity from '../../redux/actions/citiesAction';
import createOrganization from '../../redux/actions/createOrganizationAction'


const mapStateToProps = state=>{
    
    return{
        countryDetails:state.countryData,
        stateDetails:state.stateData,
        cityDetails:state.cityData,
        organizationCreatedData:state.createOrganization,

    }
}

const mapDispatchToProps = dispatch => ({
    fetchCountryList: () => dispatch(fetchCountry()),
    fetchStatesList: (country) => dispatch(fetchStates(country)),
    fetchCityList: (countryState) => dispatch(fetchcity(countryState)),
    createOrganization: (values) => dispatch(createOrganization(values)),
    
    
})

export default connect(mapStateToProps,mapDispatchToProps)(CreateOrganization)