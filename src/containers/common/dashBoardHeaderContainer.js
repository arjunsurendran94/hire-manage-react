import  DashBoardHeader from '../../components/dashboard/common/dashboardHeader';
import { connect } from "react-redux";
import getProfile from '../../redux/actions/getProfileAction';
import fetchUserOrganization from '../../redux/actions/listOrganization';
import logOut from '../../redux/actions/logOutAction';




const mapStateToProps = state=>{
    return{
        profileDetails:state.profileDetail,
        organzationList:state.organzationList,
        logOutFlag:state.logOut
    }
}

const mapDispatchToProps = dispatch => ({
    getProfile: () => dispatch(getProfile()),
    fetchUserOrganization: () => dispatch(fetchUserOrganization()),
    logOut:() => dispatch(logOut())
})

export default connect(mapStateToProps,mapDispatchToProps)(DashBoardHeader)