import  Profile from '../../components/dashboard/profile';
import { connect } from "react-redux";
import getProfile from '../../redux/actions/getProfileAction';
import fetchCountry from '../../redux/actions/countryAction';
import fetchStates from '../../redux/actions/stateAction';
import fetchcity from '../../redux/actions/citiesAction';
import fetchTimeZone from '../../redux/actions/timeZoneAction';
import editProfile from '../../redux/actions/editProfileAction';
import createOrganization from '../../redux/actions/createOrganizationAction'

import {CLEAR_PROFILE_ERRORS} from '../../constants/actionConstants'


const clearerrors = () =>{
    return{type:CLEAR_PROFILE_ERRORS}
}


const mapStateToProps = state=>{
    return{
        profileDetails:state.profileDetail,
        countryDetails:state.countryData,
        stateDetails:state.stateData,
        cityDetails:state.cityData,
        timeZoneDetails:state.timeZoneDetails,
        editProfileDetails:state.editProfileDetails,
        organizationCreatedData:state.createOrganization,
    }
}

const mapDispatchToProps = dispatch => ({
    getProfile: () => dispatch(getProfile()),
    fetchCountryList: () => dispatch(fetchCountry()),
    fetchStatesList: (country) => dispatch(fetchStates(country)),
    fetchCityList: (countryState) => dispatch(fetchcity(countryState)),
    fetchTimeZoneList:()=> dispatch(fetchTimeZone()),
    editProfile:(profileDetails)=>dispatch(editProfile(profileDetails)),
    createOrganization: (values) => dispatch(createOrganization(values)),
    clearerrors: () => dispatch(clearerrors())
})

export default connect(mapStateToProps,mapDispatchToProps)(Profile)