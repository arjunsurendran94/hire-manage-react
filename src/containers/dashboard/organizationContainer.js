import Organization from "../../components/dashboard/organization";
import { connect } from "react-redux";
import retrieveOrganization from '../../redux/actions/retrieveOrganizationAction';
import fetchUserBranch from '../../redux/actions/listBranch'; 
import listDepartments from '../../redux/actions/listDepartmentsAction';
import CreateDepartment from '../../redux/actions/createDepartmentAction'

import fetchCountry from '../../redux/actions/countryAction'
import fetchStates from '../../redux/actions/stateAction'
import fetchcity from '../../redux/actions/citiesAction'
import CreateBranchAction from '../../redux/actions/createBranchAction'

const mapStateToProps = state=>{
    return{
        organization:state.retrieveOrganization,
        branches:state.branchList,
        departmentlist:state.departmentList,
        department:state.createDepartment,

        countryDetails:state.countryData,
        stateDetails:state.stateData,
        cityDetails:state.cityData,
        branchCreatedData:state.createBranch,
    }
}

const mapDispatchToProps = (dispatch) => ({

    retrieveOrganization: () => dispatch(retrieveOrganization()),
    retrieveBranches: (org_slug,token) => dispatch(fetchUserBranch(org_slug,token)),
    retrieveDepartments:(branch_slug) => dispatch(listDepartments(branch_slug)),
    createDepartments:(values) => dispatch(CreateDepartment(values)),

    fetchCountryList: () => dispatch(fetchCountry()),
    fetchStatesList: (country) => dispatch(fetchStates(country)),
    fetchCityList: (countryState) => dispatch(fetchcity(countryState)),
    createBranch: (values) => dispatch(CreateBranchAction(values)),


})

export default connect(mapStateToProps,mapDispatchToProps)(Organization)