import viewTask from "../../components/dashboard/viewTask";
import { connect } from "react-redux";

import retrieveClient from '../../redux/actions/retriveClientAction'
import listClients from '../../redux/actions/listClientsAction'
import listClientComments from '../../redux/actions/listClientCommentsAction'
import addClientComment from '../../redux/actions/addClientCommentAction'

import retrieveOrganization from '../../redux/actions/retrieveOrganizationAction';
import fetchUserBranch from '../../redux/actions/listBranch';
import listDepartments from '../../redux/actions/listDepartmentsAction';

import editClientBasicDetails from '../../redux/actions/editClientBasicDetailsAction'
import editOtherDetails from '../../redux/actions/editOtherDetailsAction'
import editAddressDetails from '../../redux/actions/editAddressAction'
import editRemarkDetails from '../../redux/actions/editRemarkAction'
import editContactPersonDetails from '../../redux/actions/editContactPersonAction'

import fetchCountry from '../../redux/actions/countryAction';
import fetchStates from '../../redux/actions/stateAction';
import fetchcity from '../../redux/actions/citiesAction';

import { submit } from 'redux-form'

const mapStateToProps = state=>{
    
    return{
        
        clientData:state.clientReducer,
        countryDetails:state.countryData,
        stateDetails:state.stateData,
        cityDetails:state.cityData,

    }
}

const mapDispatchToProps = dispatch => ({
    
    retrieveClient:(clientSlug) => dispatch(retrieveClient(clientSlug)),
    listClients:() => dispatch(listClients()),
    listClientComments:(clientSlug)=> dispatch(listClientComments(clientSlug)),
    addClientComment:(commentData)=> dispatch(addClientComment(commentData)),

    retrieveBranches: (org_slug,token) => dispatch(fetchUserBranch(org_slug,token)),
    retrieveDepartments:(branch_slug) => dispatch(listDepartments(branch_slug)),

    fetchCountryList: () => dispatch(fetchCountry()),
    fetchStatesList: (country) => dispatch(fetchStates(country)),
    fetchCityList: (countryState) => dispatch(fetchcity(countryState)),

    editClientBasicDetails: (data,slug) => dispatch(editClientBasicDetails(data,slug)),
    editOtherDetails:(data,slug) => dispatch(editOtherDetails(data,slug)),
    editAddressDetails:(data,slug) => dispatch(editAddressDetails(data,slug)),
    editRemarkDetails:(data,slug) => dispatch(editRemarkDetails(data,slug)),
    editContactPersonDetails:(data,slug) => dispatch(editContactPersonDetails(data,slug))


})

export default connect(mapStateToProps,mapDispatchToProps)(viewTask)