import OrganizationDesignations from "../../components/dashboard/organizationDesignations";
import { connect } from "react-redux";
import DesignationsListing from '../../redux/actions/designationsListingAction';
import SortDesignations from '../../redux/actions/sortDesignationAction'; 

import {DESIGNATION_SEARCH, DESIGNATION_CLEAR_SEARCH,DESIGNATION_CLEAR_MESSAGES} from '../../constants/actionConstants'



const searchDesignations = (search) =>{
    return{type:DESIGNATION_SEARCH,payload:search}
}

const clearDesignationSearch = () =>{
    return{type:DESIGNATION_CLEAR_SEARCH}
}

const clearSuccessMessages = () =>{
    return{type:DESIGNATION_CLEAR_MESSAGES}
}


const mapStateToProps = state=>{
    return{
        desigantions:state.DesignationData
    }
}

const mapDispatchToProps = dispatch => ({
    DesignationsListing: () => dispatch(DesignationsListing()),
    sortDesignations: (itemList) => dispatch(SortDesignations(itemList)),
    searchDesignations: (search) => dispatch(searchDesignations(search)),
    clearDesignationSearch: () => dispatch(clearDesignationSearch()),
    clearSuccessMessages: () => dispatch(clearSuccessMessages())
    
})

export default connect(mapStateToProps,mapDispatchToProps)(OrganizationDesignations)