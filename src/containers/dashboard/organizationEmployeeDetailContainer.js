import OrganizationEmployeeDetail from "../../components/dashboard/organizationEmployeeDetail";
import { connect } from "react-redux";
import listEmployees from '../../redux/actions/listEmployeeAction';
import retreiveEmployee from '../../redux/actions/retreiveEmployeeAction';
import DeleteEmployee from '../../redux/actions/deleteEmployeeAction';
import DesignationsListing from '../../redux/actions/designationsListingAction';
import fetchCountry from '../../redux/actions/countryAction';
import fetchStates from '../../redux/actions/stateAction';
import fetchcity from '../../redux/actions/citiesAction';
import fetchPermissionGroups from '../../redux/actions/listRolesAction';
import retrieveOrganization from '../../redux/actions/retrieveOrganizationAction';
import fetchUserBranch from '../../redux/actions/listBranch';
import listDepartments from '../../redux/actions/listDepartmentsAction';
import partialUpdateEmployee from '../../redux/actions/partialUpdateEmployeeAction';
import employeeStatusChange from '../../redux/actions/employeeStatusChangeAction';
import updateEmployeePermissions from '../../redux/actions/updateEmployeePermissionAction';
import listEmployeePermissions from '../../redux/actions/listEmployeePermissionAction';

const mapStateToProps = state=>{
    return{
        employees :state.employees,
        employeeObject:state.employeeData,
        designationList:state.DesignationData,
        countryDetails:state.countryData,
        stateDetails:state.stateData,
        cityDetails:state.cityData,
        roles:state.rolesData,
        organization:state.retrieveOrganization,
        branches:state.branchList,
        departmentlist:state.departmentList,
        employeePermissions:state.employeeRoles
    }
}

const mapDispatchToProps = dispatch => ({
    retrieveOrganization: () => dispatch(retrieveOrganization()),
    retrieveBranches: (org_slug,token) => dispatch(fetchUserBranch(org_slug,token)),
    listEmployees:(branch_slug) => dispatch(listEmployees(branch_slug)),
    retreiveEmployee: (employee_slug) => dispatch(retreiveEmployee(employee_slug)),
    DeleteEmployee: (employee_slug,name) => dispatch(DeleteEmployee(employee_slug,name)),
    designationsListing:() => dispatch(DesignationsListing()),
    fetchCountryList: () => dispatch(fetchCountry()),
    fetchStatesList: (country) => dispatch(fetchStates(country)),
    fetchCityList: (countryState) => dispatch(fetchcity(countryState)),
    fetchPermissionGroups: () => dispatch(fetchPermissionGroups()),
    retrieveDepartments:(branch_slug) => dispatch(listDepartments(branch_slug)),
    partialUpdateEmployee:(employee_slug,values) => dispatch(partialUpdateEmployee(employee_slug,values)),
    employeeStatusChange:(employee_slug) => dispatch(employeeStatusChange(employee_slug)),
    updateEmployeePermissions:(employee_slug,values)=> dispatch(updateEmployeePermissions(employee_slug,values)),
    listEmployeePermissions:(employee_slug) => dispatch(listEmployeePermissions(employee_slug)),
})

export default connect(mapStateToProps,mapDispatchToProps)(OrganizationEmployeeDetail)