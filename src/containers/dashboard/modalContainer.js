import ModalComponent from "../../components/dashboard/modal/modal";
import { connect } from "react-redux";
import fetchCountry from '../../redux/actions/countryAction';
import fetchStates from '../../redux/actions/stateAction';
import fetchcity from '../../redux/actions/citiesAction';

const mapStateToProps = state=>{
    
    return{
        countryDetails:state.countryData,
        stateDetails:state.stateData,
        cityDetails:state.cityData,

    }
}

const mapDispatchToProps = dispatch => ({
    fetchCountryList: () => dispatch(fetchCountry()),
    fetchStatesList: (country) => dispatch(fetchStates(country)),
    fetchCityList: (countryState) => dispatch(fetchcity(countryState)),
})

export default connect(mapStateToProps,mapDispatchToProps)(ModalComponent)