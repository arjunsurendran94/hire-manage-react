import OrganizationAddNewDepartments from "../../components/dashboard/organizationADDNewDepartments";
import { connect } from "react-redux";
import retrieveOrganization from '../../redux/actions/retrieveOrganizationAction';
import fetchUserBranch from '../../redux/actions/listBranch';
import CreateDepartment from '../../redux/actions/createDepartmentAction'
import {DEPARTMENTS_CLEAR_SUCCESS_STATUS} from '../../constants/actionConstants';



const clearDepartmentsStatusSuccess = () =>{
    return{type:DEPARTMENTS_CLEAR_SUCCESS_STATUS}
}

const mapStateToProps = state=>{
    
    return{
        organization:state.retrieveOrganization,
        branches:state.branchList,
        department:state.createDepartment,

    }
}

const mapDispatchToProps = dispatch => ({
    retrieveOrganization: () => dispatch(retrieveOrganization()),
    retrieveBranches: (org_slug,token) => dispatch(fetchUserBranch(org_slug,token)),
    createDepartments:(values) => dispatch(CreateDepartment(values)),
    clearDepartmentsStatusSuccess:() => dispatch(clearDepartmentsStatusSuccess())


})

export default connect(mapStateToProps,mapDispatchToProps)(OrganizationAddNewDepartments)