import OrganizationAddNewBranches from "../../components/dashboard/organizationAddNewBranches";
import { connect } from "react-redux";
import fetchCountry from '../../redux/actions/countryAction'
import fetchStates from '../../redux/actions/stateAction'
import fetchcity from '../../redux/actions/citiesAction'
import CreateBranchAction from '../../redux/actions/createBranchAction'


const mapStateToProps = state=>{
    
    return{
        countryDetails:state.countryData,
        stateDetails:state.stateData,
        cityDetails:state.cityData,
        branchCreatedData:state.createBranch,
    }
}

const mapDispatchToProps = dispatch => ({
    fetchCountryList: () => dispatch(fetchCountry()),
    fetchStatesList: (country) => dispatch(fetchStates(country)),
    fetchCityList: (countryState) => dispatch(fetchcity(countryState)),
    createBranch: (values) => dispatch(CreateBranchAction(values))

})

export default connect(mapStateToProps,mapDispatchToProps)(OrganizationAddNewBranches)