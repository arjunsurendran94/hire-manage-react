import OrganizationRoleUpdate from "../../components/dashboard/organizationRoleUpdate";
import { connect } from "react-redux";
import fetchPermissionGroups from '../../redux/actions/listRolesAction'
import createPermissionGroups from '../../redux/actions/createRolesAction'
import retreivePermissionGroups from '../../redux/actions/retreiveRoleAction'
import deleteRole from '../../redux/actions/deleteRoleAction'
import editRole from '../../redux/actions/editRoleAction'
import listingAllPermisions from '../../redux/actions/listingAllPermissionsAction'


const mapStateToProps = state=>{
    
    
    return{
        roles:state.rolesData,
        rolesCreated:state.rolesCreated,
        roleObject:state.role,
        roleDeleted:state.roleDelete,
        roleEdited:state.roleEdit

    }
}

const mapDispatchToProps = dispatch => ({
    fetchPermissionGroups: () => dispatch(fetchPermissionGroups()),
    createPermissionGroups: (values) => dispatch(createPermissionGroups(values)),
    retreivePermissionGroups: (role_slug) => dispatch(retreivePermissionGroups(role_slug)),
    deleteRole: (role_slug) => dispatch(deleteRole(role_slug)),
    editRole: (values,role_slug) => dispatch(editRole(values,role_slug)),
    listingAllPermisions:()=>dispatch(listingAllPermisions())
})

export default connect(mapStateToProps,mapDispatchToProps)(OrganizationRoleUpdate)