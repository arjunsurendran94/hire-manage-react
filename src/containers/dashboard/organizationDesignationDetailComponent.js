import DesignationDetail from "../../components/dashboard/designationDetail";
import { connect } from "react-redux";
import RetrieveDesignation from '../../redux/actions/designationDetailAction'
import DeleteDesignation from '../../redux/actions/deleteDesignationAction'


const mapStateToProps = state=>{
    return{
        desigantions:state.DesignationData
    }
}

const mapDispatchToProps = dispatch => ({
    RetrieveDesignation: (slug) => dispatch(RetrieveDesignation(slug)),
    DeleteDesignation: (slug) => dispatch(DeleteDesignation(slug))

})

export default connect(mapStateToProps,mapDispatchToProps)(DesignationDetail)