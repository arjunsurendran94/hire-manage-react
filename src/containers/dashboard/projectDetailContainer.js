import viewProject from "../../components/dashboard/viewProject";
import { connect } from "react-redux";

import retrieveClient from '../../redux/actions/retriveClientAction'
import listClients from '../../redux/actions/listClientsAction'
import listClientComments from '../../redux/actions/listClientCommentsAction'
import addClientComment from '../../redux/actions/addClientCommentAction'

import retrieveOrganization from '../../redux/actions/retrieveOrganizationAction';
import fetchUserBranch from '../../redux/actions/listBranch';
import listDepartments from '../../redux/actions/listDepartmentsAction';

import editClientBasicDetails from '../../redux/actions/editClientBasicDetailsAction'
import editOtherDetails from '../../redux/actions/editOtherDetailsAction'
import editAddressDetails from '../../redux/actions/editAddressAction'
import editRemarkDetails from '../../redux/actions/editRemarkAction'
import editContactPersonDetails from '../../redux/actions/editContactPersonAction'

import fetchCountry from '../../redux/actions/countryAction';
import fetchStates from '../../redux/actions/stateAction';
import fetchcity from '../../redux/actions/citiesAction';

import listEmployees from '../../redux/actions/listEmployeeAction'
import addTask from '../../redux/actions/addTaskAction'
import retrieveProject from '../../redux/actions/projectDetailAction'
import addProjectComment from '../../redux/actions/addProjectCommentAction'
import listProjectComment from '../../redux/actions/listingProjectCommentAction'
import downloadContract from '../../redux/actions/downloadContractAction'
import editProjectGeneralInfo from '../../redux/actions/editProjectGeneralAction'
import editResource from '../../redux/actions/editResourceAction'
import addResource from '../../redux/actions/addResourceAction'
import deleteResource from '../../redux/actions/deleteResourceAction'
import createContract from '../../redux/actions/createContractAction'
import addBillableResource from '../../redux/actions/addBillableResourceAction'
import addProjectAttachment from '../../redux/actions/addProjectAttachmentAction'
import deleteProjectAttachment from '../../redux/actions/deleteProjectAttachmentAction'
import getCurrentEmployeePermission from '../../redux/actions/getCurrentEmployeePermissionAction'
import addContractAttachment from '../../redux/actions/addNewContractAttachmentAction'
import editContractStatus from '../../redux/actions/editContractStatusAction'
import addTermAttachment from '../../redux/actions/addTermAttachmentAction'
import downloadTermAttachment from '../../redux/actions/termAttachmentDownloadAction'
import listTasks from '../../redux/actions/listTaskAction'
import listMyTasks from '../../redux/actions/listMyTaskAction'
import addSubtask from '../../redux/actions/addSubTaskAction'
import listingSubTask from '../../redux/actions/listingSubtaskAction'
import editTask from '../../redux/actions/editTaskAction'
import editSubTask from '../../redux/actions/editSubTaskAction'
import listingTaskLog from '../../redux/actions/listingTaskLogAction'
import startTime from '../../redux/actions/startTimeAction'
import listTimeLog from '../../redux/actions/listTimeLogAction'
import endTime from '../../redux/actions/endTimeAction'
import addTime from '../../redux/actions/addTimeAction'
import subTaskLog from '../../redux/actions/subTaskLogAction'
import addTaskComment from '../../redux/actions/addTaskCommentAction' 
import listingTaskComments from '../../redux/actions/listingTaskCommentsActions'
import editTime from '../../redux/actions/editTimeAction'
import listEmployeeWeeklyWorksheet from '../../redux/actions/listEmployeeWeeklyWorksheetAction'
import editEmployeeWeeklyWorksheet from '../../redux/actions/editEmployeeWeeklyWorksheetAction'
import listFixedWeeklyWorksheet from '../../redux/actions/listingFixedWeeklyWorksheetAction'

import { submit } from 'redux-form'

const mapStateToProps = state=>{
    
    return{
        
        clientData:state.clientReducer,
        countryDetails:state.countryData,
        stateDetails:state.stateData,
        cityDetails:state.cityData,
        employees:state.employees,
        tasks:state.taskReducer,
        projectData:state.projectReducer,
        employeePermissions:state.employees.employeePermissions
    }
}

const mapDispatchToProps = dispatch => ({
    
    retrieveClient:(clientSlug) => dispatch(retrieveClient(clientSlug)),
    listClients:() => dispatch(listClients()),
    listClientComments:(clientSlug)=> dispatch(listClientComments(clientSlug)),
    addClientComment:(commentData)=> dispatch(addClientComment(commentData)),

    retrieveBranches: (org_slug,token) => dispatch(fetchUserBranch(org_slug,token)),
    retrieveDepartments:(branch_slug) => dispatch(listDepartments(branch_slug)),

    fetchCountryList: () => dispatch(fetchCountry()),
    fetchStatesList: (country) => dispatch(fetchStates(country)),
    fetchCityList: (countryState) => dispatch(fetchcity(countryState)),

    editClientBasicDetails: (data,slug) => dispatch(editClientBasicDetails(data,slug)),
    editOtherDetails:(data,slug) => dispatch(editOtherDetails(data,slug)),
    editAddressDetails:(data,slug) => dispatch(editAddressDetails(data,slug)),
    editRemarkDetails:(data,slug) => dispatch(editRemarkDetails(data,slug)),
    editContactPersonDetails:(data,slug) => dispatch(editContactPersonDetails(data,slug)),

    listEmployees:(branch_slug) => dispatch(listEmployees(branch_slug)),
    addTask:(data)=> dispatch(addTask(data)),
    retrieveProject:(slug)=>dispatch(retrieveProject(slug)),

    addProjectComment:(data,slug)=>dispatch(addProjectComment(data,slug)),
    listProjectComment:(slug)=>dispatch(listProjectComment(slug)),
    downloadContract:(id,name)=>dispatch(downloadContract(id,name)),

    editProjectGeneralInfo:(data,slug)=>dispatch(editProjectGeneralInfo(data,slug)),
    editResource:(data,id)=>dispatch(editResource(data,id)),
    addResource:(data)=>dispatch(addResource(data)),
    deleteResource:(id)=>dispatch(deleteResource(id)),
    createContract:(data)=>dispatch(createContract(data)),
    addBillableResource:(data)=>dispatch(addBillableResource(data)),
    addProjectAttachment:(data)=>dispatch(addProjectAttachment(data)),
    deleteProjectAttachment:(id)=>dispatch(deleteProjectAttachment(id)),
    addContractAttachment:(data,id)=>dispatch(addContractAttachment(data,id)),
    
    editContractStatus:(data,id)=>dispatch(editContractStatus(data,id)),
    addTermAttachment:(data)=>dispatch(addTermAttachment(data)),
    downloadTermAttachment:(id,name)=>(dispatch(downloadTermAttachment(id,name))),
    listTasks:(data)=>dispatch(listTasks(data)),
    listMyTasks:(data)=>dispatch(listMyTasks(data)),
    addSubtask:(data)=>dispatch(addSubtask(data)),
    listingSubTask:(data)=>dispatch(listingSubTask(data)),
    editTask:(data,slug)=>dispatch(editTask(data,slug)),
    editSubTask:(data,slug)=>dispatch(editSubTask(data,slug)),
    listingTaskLog:(data)=>dispatch(listingTaskLog(data)),
    startTime:(data)=>dispatch(startTime(data)),
    listTimeLog:(data)=>dispatch(listTimeLog(data)),
    endTime:(data,slug)=>dispatch(endTime(data,slug)),
    addTime:(data,type)=>dispatch(addTime(data,type)),
    subTaskLog:(data)=>dispatch(subTaskLog(data)),
    addTaskComment:(data)=>dispatch(addTaskComment(data)),
    listingTaskComments:(data)=>dispatch(listingTaskComments(data)),
    editTime:(data,slug,type,dashboardEdit)=>dispatch(editTime(data,slug,type,dashboardEdit)),
    listEmployeeWeeklyWorksheet:(data,project_slug)=>dispatch(listEmployeeWeeklyWorksheet(data,project_slug)),
    editEmployeeWeeklyWorksheet:(id,data,slug,project_slug,type)=>dispatch(editEmployeeWeeklyWorksheet(id,data,slug,project_slug,type)),
    listFixedWeeklyWorksheet:(project_slug)=>dispatch(listFixedWeeklyWorksheet(project_slug))

})

export default connect(mapStateToProps,mapDispatchToProps)(viewProject)