import AddProject from "../../components/dashboard/addProject";
import { connect } from "react-redux";
import retrieveOrganization from '../../redux/actions/retrieveOrganizationAction';
import fetchUserBranch from '../../redux/actions/listBranch';
import DesignationsListing from '../../redux/actions/designationsListingAction';
import fetchCountry from '../../redux/actions/countryAction';
import fetchStates from '../../redux/actions/stateAction';
import fetchcity from '../../redux/actions/citiesAction';
import listDepartments from '../../redux/actions/listDepartmentsAction';
import addEmployee from '../../redux/actions/addEmployeeAction';
import fetchPermissionGroups from '../../redux/actions/listRolesAction'
import CreateDesignation from '../../redux/actions/createDesignationAction';

import addNewDesignationFromModal from '../../redux/actions/addNewDesignationAction';
import addClient from '../../redux/actions/addClientAction'

import listClients from '../../redux/actions/listClientsAction'

import listEmployees from '../../redux/actions/listEmployeeAction';

import addProject from '../../redux/actions/addProjectAction';

import {SET_CLIENT_PORTAL_PASSWORD,CLEAR_PORTAL_PASSWORD_STATUS} from '../../constants/actionConstants'



import { submit } from 'redux-form'

const setClientPortalPassword = (value) =>{
    return{type:SET_CLIENT_PORTAL_PASSWORD,payload:value}
}
const clearClientPortalPasswordStatus=()=>{
    return{type:CLEAR_PORTAL_PASSWORD_STATUS}
}



const mapStateToProps = state=>{
    
    return{
        // organization:state.retrieveOrganization,
        branches:state.branchList,
        department:state.createDepartment,
        designationList:state.DesignationData,
        countryDetails:state.countryData,
        stateDetails:state.stateData,
        cityDetails:state.cityData,
        departmentlist:state.departmentList,
        
        
        employees :state.employees,
        
        formStateValues:state.form.addEmployeeForm,

        roles:state.rolesData,
        
        clientData:state.clientReducer,
        projectData:state.projectReducer
        
    }
}

const mapDispatchToProps = dispatch => ({
    retrieveOrganization: () => dispatch(retrieveOrganization()),
    retrieveBranches: (org_slug,token) => dispatch(fetchUserBranch(org_slug,token)),
    
    fetchCountryList: () => dispatch(fetchCountry()),
    fetchStatesList: (country) => dispatch(fetchStates(country)),
    fetchCityList: (countryState) => dispatch(fetchcity(countryState)),
    retrieveDepartments:(branch_slug) => dispatch(listDepartments(branch_slug)),
    addClient:(clientData) => dispatch(addClient(clientData)),
    setClientPortalPassword:(value) => dispatch(setClientPortalPassword(value)),
    clearClientPortalPasswordStatus:() => dispatch(clearClientPortalPasswordStatus()),

    listEmployees:(branch_slug) => dispatch(listEmployees(branch_slug)),

    clientListing:() => dispatch(listClients()),
    addProject:(data) => dispatch(addProject(data)),
    DesignationsListing: () => dispatch(DesignationsListing()),

})

export default connect(mapStateToProps,mapDispatchToProps)(AddProject)