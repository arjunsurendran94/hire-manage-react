import OrganizationAddDesignations from "../../components/dashboard/organizationAddDesignations";
import { connect } from "react-redux";
import fetchPermissionGroups from '../../redux/actions/listRolesAction';
import CreateDesignation from '../../redux/actions/createDesignationAction';
import RetrieveDesignation from '../../redux/actions/designationDetailAction';
import UpdateDesignation from '../../redux/actions/designationUpdateAction';
import DesignationsListing from '../../redux/actions/designationsListingAction'

const mapStateToProps = state=>{
    
    
    return{
        roles:state.rolesData,
        desigantions:state.DesignationData

    }
}

const mapDispatchToProps = dispatch => ({
    fetchPermissionGroups: () => dispatch(fetchPermissionGroups()),
    CreateDesignations: (values) => dispatch(CreateDesignation(values)),
    RetrieveDesignation: (slug) => dispatch(RetrieveDesignation(slug)),
    UpdateDesignation: (values,slug) => dispatch(UpdateDesignation(values,slug)),
    DesignationsListing: () => dispatch(DesignationsListing()),



})

export default connect(mapStateToProps,mapDispatchToProps)(OrganizationAddDesignations)