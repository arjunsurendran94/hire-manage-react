import AddEmployee from "../../components/dashboard/addEmployee";
import { connect } from "react-redux";
import retrieveOrganization from '../../redux/actions/retrieveOrganizationAction';
import fetchUserBranch from '../../redux/actions/listBranch';
import DesignationsListing from '../../redux/actions/designationsListingAction';
import fetchCountry from '../../redux/actions/countryAction';
import fetchStates from '../../redux/actions/stateAction';
import fetchcity from '../../redux/actions/citiesAction';
import listDepartments from '../../redux/actions/listDepartmentsAction';
import addEmployee from '../../redux/actions/addEmployeeAction';
import fetchPermissionGroups from '../../redux/actions/listRolesAction'
import CreateDesignation from '../../redux/actions/createDesignationAction';

import addNewDesignationFromModal from '../../redux/actions/addNewDesignationAction';
import {CLEAR_MODAL_SUCCESS} from '../../constants/actionConstants'

const clearModalStatus = () =>{
    return{type:CLEAR_MODAL_SUCCESS}
}

const mapStateToProps = state=>{
    
    return{
        organization:state.retrieveOrganization,
        branches:state.branchList,
        department:state.createDepartment,
        designationList:state.DesignationData,
        countryDetails:state.countryData,
        stateDetails:state.stateData,
        cityDetails:state.cityData,
        departmentlist:state.departmentList,
        employeeCreated:state.employees,
        formStateValues:state.form.addEmployeeForm,

        roles:state.rolesData,
        desigantions:state.DesignationData


    }
}

const mapDispatchToProps = dispatch => ({
    retrieveOrganization: () => dispatch(retrieveOrganization()),
    retrieveBranches: (org_slug,token) => dispatch(fetchUserBranch(org_slug,token)),
    designationsListing:() => dispatch(DesignationsListing()),
    fetchCountryList: () => dispatch(fetchCountry()),
    fetchStatesList: (country) => dispatch(fetchStates(country)),
    fetchCityList: (countryState) => dispatch(fetchcity(countryState)),
    retrieveDepartments:(branch_slug) => dispatch(listDepartments(branch_slug)),
    addEmployee:(employeeData) => dispatch(addEmployee(employeeData)),
    fetchPermissionGroups: () => dispatch(fetchPermissionGroups()),
    CreateDesignations: (values) => dispatch(CreateDesignation(values)),
    addNewDesignationFromModal: (values) => dispatch(addNewDesignationFromModal(values)),
    clearModalStatus: () => dispatch(clearModalStatus())

})

export default connect(mapStateToProps,mapDispatchToProps)(AddEmployee)