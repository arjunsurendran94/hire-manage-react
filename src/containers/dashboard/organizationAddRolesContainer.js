import OrganizationAddRoles from "../../components/dashboard/organizationAddRoles";
import { connect } from "react-redux";
import fetchPermissionGroups from '../../redux/actions/listRolesAction'
import createPermissionGroups from '../../redux/actions/createRolesAction'
import listingAllPermisions from '../../redux/actions/listingAllPermissionsAction'

const mapStateToProps = state=>{
    
    
    return{
        roles:state.rolesData,
        rolesCreated:state.rolesCreated

    }
}

const mapDispatchToProps = dispatch => ({
    fetchPermissionGroups: () => dispatch(fetchPermissionGroups()),
    createPermissionGroups: (values) => dispatch(createPermissionGroups(values)),
    listingAllPermisions:()=>dispatch(listingAllPermisions())
})

export default connect(mapStateToProps,mapDispatchToProps)(OrganizationAddRoles)