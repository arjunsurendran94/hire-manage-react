import Project from '../../components/dashboard/project'

import { connect } from "react-redux";

import listClients from '../../redux/actions/listClientsAction'
import listProjects from '../../redux/actions/listProjectsAction'
import { submit } from 'redux-form'

import {PROJECT_SEARCH,CLEAR_PROJECT_SEARCH} from '../../constants/actionConstants'



const searchProjects = (search) =>{
    return{type:PROJECT_SEARCH,payload:search}
}
const clearProjectsSearch = () =>{
    return{type:CLEAR_PROJECT_SEARCH}
}

const mapStateToProps = state=>{
    
    return{
        // organization:state.retrieveOrganization,
        projectData:state.projectReducer
    }
}

const mapDispatchToProps = dispatch => ({
    
    clientListing:() => dispatch(listClients()),
    listProjects:() => dispatch(listProjects()),
    searchProjects:(data)=> dispatch(searchProjects(data)),
    clearProjectsSearch:()=>dispatch(clearProjectsSearch())

})

export default connect(mapStateToProps,mapDispatchToProps)(Project)