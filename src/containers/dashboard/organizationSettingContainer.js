import OrganizationSettings from "../../components/dashboard/organizationSettings";
import { connect } from "react-redux";
import fetchCountry from '../../redux/actions/countryAction';
import fetchStates from '../../redux/actions/stateAction';
import fetchcity from '../../redux/actions/citiesAction';
import retrieveOrganization from '../../redux/actions/retrieveOrganizationAction';
import fetchUserBranch from '../../redux/actions/listBranch'; 
import listDepartments from '../../redux/actions/listDepartmentsAction';
import deleteDepartment from '../../redux/actions/deleteDepartmentAction';
import editOrganization from '../../redux/actions/editOrganizationAction'
import deleteBranch from '../../redux/actions/deleteBranchAction';
import fetchUserOrganization from '../../redux/actions/listOrganization';
import deleteOrganization from '../../redux/actions/deleteOrganizationAction'
import editBranch from '../../redux/actions/editBranchAction'
import editDepartment from '../../redux/actions/editDepartmentAction' 
import {EDIT_ORGANIZATION_CLEAR_SUCCESS} from '../../constants/actionConstants'

const mapStateToProps = state=>{

    return{
        organization:state.retrieveOrganization,
        branches:state.branchList,
        countryDetails:state.countryData,
        stateDetails:state.stateData,
        cityDetails:state.cityData,
        departmentlist:state.departmentList,
        departmentDelete:state.deleteDepartment,
        branchDelete:state.deleteBranch,
        organzationList:state.organzationList,
        organizationDelete:state.deleteOrganization,

    }
}
const clearstatus=()=> {
    return { type: EDIT_ORGANIZATION_CLEAR_SUCCESS }
  }

const mapDispatchToProps = dispatch => ({
    retrieveOrganization: () => dispatch(retrieveOrganization()),
    retrieveBranches: (org_slug,token) => dispatch(fetchUserBranch(org_slug,token)),
    retrieveDepartments:(branch_slug) => dispatch(listDepartments(branch_slug)),
    fetchCountryList: () => dispatch(fetchCountry()),
    fetchStatesList: (country) => dispatch(fetchStates(country)),
    fetchCityList: (countryState) => dispatch(fetchcity(countryState)),
    deleteDepartment: (department_slug,department_name) => dispatch(deleteDepartment(department_slug,department_name)),
    editOrganization: (values) => dispatch(editOrganization(values)),
    clearstatus: () =>dispatch(clearstatus()),
    deleteBranch: (branch_slug,branch_name) => dispatch(deleteBranch(branch_slug,branch_name)),
    fetchUserOrganization: () => dispatch(fetchUserOrganization()),
    deleteOrganization: (org_slug) => dispatch(deleteOrganization(org_slug)),
    editBranch: (values,branch_slug) => dispatch(editBranch(values,branch_slug)),
    editDepartment: (values,department_slug) => dispatch(editDepartment(values,department_slug))

})

export default connect(mapStateToProps,mapDispatchToProps)(OrganizationSettings)