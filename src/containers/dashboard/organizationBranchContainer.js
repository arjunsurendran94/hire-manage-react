import OrganizationBranches from "../../components/dashboard/organizationBranches";
import { connect } from "react-redux";
import retrieveOrganization from '../../redux/actions/retrieveOrganizationAction';
import fetchUserBranch from '../../redux/actions/listBranch'; 
import listDepartments from '../../redux/actions/listDepartmentsAction';
import searchBranch from '../../redux/actions/searchBranchAction';


const mapStateToProps = state=>{
    
    return{
        organization:state.retrieveOrganization,
        branches:state.branchList,
        departmentlist:state.departmentList

    }
}

const mapDispatchToProps = dispatch => ({
    retrieveOrganization: () => dispatch(retrieveOrganization()),
    retrieveBranches: (org_slug,token) => dispatch(fetchUserBranch(org_slug,token)),
    retrieveDepartments:(branch_slug) => dispatch(listDepartments(branch_slug)),
    searchBranch:(item) => dispatch(searchBranch(item))

})

export default connect(mapStateToProps,mapDispatchToProps)(OrganizationBranches)