import OrganizationEmployees from "../../components/dashboard/organizationEmployees";
import { connect } from "react-redux";
import listEmployees from '../../redux/actions/listEmployeeAction';
import searchFilterEmployees from '../../redux/actions/searchFilterEmployeeAction';
import fetchUserBranch from '../../redux/actions/listBranch';
import listDepartments from '../../redux/actions/listDepartmentsAction';
import allDepartmentsListing from '../../redux/actions/allDepartmentsListingAction'

const mapStateToProps = state=>{
    return{
        employees :state.employees,
        branches:state.branchList,
        departmentlist:state.departmentList
    }
}

const mapDispatchToProps = dispatch => ({
    listEmployees:(branch_slug) => dispatch(listEmployees(branch_slug)),
    retrieveBranches: (org_slug,token) => dispatch(fetchUserBranch(org_slug,token)),
    searchFilterEmployees: (obj_list) => dispatch(searchFilterEmployees(obj_list)),
    retrieveDepartments:(branch_slug) => dispatch(listDepartments(branch_slug)),
    allDepartmentsListing:(branch_slug) => dispatch(allDepartmentsListing(branch_slug))

})

export default connect(mapStateToProps,mapDispatchToProps)(OrganizationEmployees)