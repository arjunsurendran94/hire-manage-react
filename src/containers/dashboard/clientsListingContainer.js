import Client from '../../components/dashboard/client'

import { connect } from "react-redux";

import listClients from '../../redux/actions/listClientsAction'

import { submit } from 'redux-form'

import {CLIENT_SEARCH,CLEAR_CLIENT_SEARCH} from '../../constants/actionConstants'


const searchClients = (search) =>{
    return{type:CLIENT_SEARCH,payload:search}
}
const clearSearch = () =>{
    return{type:CLEAR_CLIENT_SEARCH}
}


const mapStateToProps = state=>{
    
    return{
        
        clientData:state.clientReducer

    }
}

const mapDispatchToProps = dispatch => ({
    
    clientListing:() => dispatch(listClients()),
    searchClients:(search) => dispatch(searchClients(search)),
    clearSearch:()=>dispatch(clearSearch())

})

export default connect(mapStateToProps,mapDispatchToProps)(Client)