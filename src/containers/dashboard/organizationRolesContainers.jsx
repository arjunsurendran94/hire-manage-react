import OrganizationRoles from "../../components/dashboard/organizationRoles";
import { connect } from "react-redux";
import fetchPermissionGroups from '../../redux/actions/listRolesAction'

import {ROLES_SEARCH,CLEAR_SEARCH} from '../../constants/actionConstants'

const roleSearch = (itemList) =>{
    return{type:ROLES_SEARCH,payload:itemList}
}

const clearSearch = () =>{
    return{type:CLEAR_SEARCH}
}


const mapStateToProps = state=>{
    
    
    return{
        roles:state.rolesData,
        roleDeleted:state.roleDelete

    }
}

const mapDispatchToProps = dispatch => ({
    fetchPermissionGroups: () => dispatch(fetchPermissionGroups()),
    roleSearch: (itemList) => dispatch(roleSearch(itemList)),
    clearSearch: () => dispatch(clearSearch())
})

export default connect(mapStateToProps,mapDispatchToProps)(OrganizationRoles)