import OrganizationDepartments from "../../components/dashboard/organizationDepartments";
import { connect } from "react-redux";
import retrieveOrganization from '../../redux/actions/retrieveOrganizationAction';
import fetchUserBranch from '../../redux/actions/listBranch'; 
import listDepartments from '../../redux/actions/listDepartmentsAction';
import searchOrganizationDepartments from '../../redux/actions/searchOrganizationDepartmentsAction';



const mapStateToProps = state=>{
    
    return{
        organization:state.retrieveOrganization,
        branches:state.branchList,
        departmentlist:state.departmentList

    }
}

const mapDispatchToProps = dispatch => ({
    retrieveOrganization: () => dispatch(retrieveOrganization()),
    retrieveBranches: (org_slug,token) => dispatch(fetchUserBranch(org_slug,token)),
    retrieveDepartments:(branch_slug) => dispatch(listDepartments(branch_slug)),
    searchOrganizationDepartments:(item,filtering,branch_slug) => dispatch(searchOrganizationDepartments(item,filtering,branch_slug)),
})

export default connect(mapStateToProps,mapDispatchToProps)(OrganizationDepartments)