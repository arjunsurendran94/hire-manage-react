export const USER = [
    {id:"change_profile",value:41,name:"Can change profile"}
];
export const HISTORY = [
    {id:"delete_history",value:46,name:"Can delete history"},
    {id:"view_history",value:47,name:"Can view history"}
];
export const BRANCH = [
    {id:"add_branch",value:68,name:"Can add branch"},
    {id:"change_branch",value:69,name:"Can change branch"},
    {id:"delete_branch",value:70,name:"Can delete branch"},
    {id:"view_branch",value:71,name:"Can view branch"}
];
export const DEPARTMENT = [
    {id:"add_department",value:76,name:"Can add department"},
    {id:"change_department",value:77,name:"Can change department"},
    {id:"delete_department",value:78,name:"Can delete department"},
    {id:"view_department",value:79,name:"Can view department"}
];
export const DESIGNATION =[
    {id:"add_designation",value:80,name:"Can add designation"},
    {id:"change_designation",value:81,name:"Can change designation"},
    {id:"delete_designation",value:82,name:"Can delete designation"},
    {id:"view_designation",value:83,name:"Can view designation"}
];
export const EMPLOYEE = [
    {id:"add_employee",value:88,name:"Can add employee"},
    {id:"change_employee",value:89,name:"Can change employee"},
    {id:"change_permissions",value:93,name:"Can change permissions"},
    {id:"change_tracker_info",value:95,name:"Can change tracker info"},
    {id:"update_status",value:91,name:"Can update status"},
    {id:"view_employee",value:90,name:"Can view employee"},
    {id:"view_permissions",value:92,name:"Can view permissions"},
    {id:"view_tracker_info",value:94,name:"Can view tracker info"}
];

export const CLIENT = [
    {id:"add_client",value:108,name:"Can add client"},
    {id:"change_client",value:109,name:"Can change client"},
    {id:"change_client_status",value:112,name:"Can change client status"},
    {id:"delete_client",value:110,name:"Can delete client"},
    {id:"view_client",value:111,name:"Can view client"}
];
export const COMMENT = [
    {id:"add_comment",value:125,name:"Can add comment"},
    {id:"change_comment",value:126,name:"Can change comment"},
    {id:"change_others_comment",value:129,name:"Can change others comment"},
    {id:"delete_comment",value:127,name:"Can delete comment"},
    {id:"view_comment",value:128,name:"Can view comment"}
];
export const MILESTONE = [
    {id:"add_milestone",value:146,name:"Can add milestone"},
    {id:"change_milestone",value:147,name:"Can change milestone"},
    {id:"delete_milestone",value:148,name:"Can delete milestone"},
    {id:"view_milestone",value:149,name:"Can view milestone"}
];

export const PROJECT = [
    {id:"add_project",value:150,name:"Can add project"},
    {id:"change_project",value:151,name:"Can change project"},
    {id:"change_project_status",value:156,name:"Can change project status"},
    {id:"delete_project",value:152,name:"Can delete project"},
    {id:"view_hours_per_rate",value:154,name:"Can view hours per rate"},
    {id:"view_project",value:153,name:"Can view project"},
    {id:"view_weekly_limit",value:155,name:"Can view weekly limit"}
];
export const TASK = [
    {id:"add_task",value:161,name:"Can add task"},
    {id:"assign_to_a_task",value:165,name:"Can assign to a task"},
    {id:"change_task",value:162,name:"Can change task"},
    {id:"change_task_board",value:166,name:"Can change task board"},
    {id:"delete_task",value:163,name:"Can delete task"},
    {id:"view_task",value:164,name:"Can view task"}
];
export const WORKSHEET = [
    {id:"add_all_employee_worksheet",value:174,name:"Can add all employee worksheet"},
    {id:"add_worksheet",value:167,name:"Can add worksheet"},
    {id:"change_all_employee_worksheet",value:175,name:"Can change all employee worksheet"},
    {id:"change_worksheet",value:168,name:"Can change worksheet"},
    {id:"delete_all_employee_worksheet",value:176,name:"Can delete all employee worksheet"},
    {id:"delete_worksheet",value:169,name:"Can delete worksheet"},
    {id:"view_all_employee_worksheet",value:172,name:"Can view all employee worksheet"},
    {id:"view_all_project_employee_worksheet",value:171,name:"Can view all project employee worksheet"},
    {id:"view_employee_worksheet",value:173,name:"Can view employee worksheet"},
    {id:"view_worksheet",value:170,name:"Can view worksheet"} 
];

export const permissionDict = {
    "USER":[41],
    "HISTORY":[46,47],
    "BRANCH":[68,69,70,71],
    "DEPARTMENT":[76,77,78,79],
    "DESIGNATION":[80,81,82,83],
    "EMPLOYEE":[88,89,93,95,91,90,92,94],
    "CLIENT":[108,109,110,111],
    "COMMENT":[125,126,127,128],
    "MILESTONE":[146,147,148,149],
    "PROJECT":[150,151,152,153,154,155,156],
    "TASK":[161,165,162,166,163,164],
    "WORKSHEET":[174,167,175,168,176,169,172,171,173,170]
}

export const permissionList = [
    "user_permissions",
    "history_permissions",
    "branch_permissions",
    "department_permissions",
    "designation_permissions",
    "employee_permissions",
    "client_permissions",
    "comment_permissions",
    "milestone_permissions",
    "project_permissions",
    "task_permissions",
    "worksheet_permissions"
]


export const filteringPermissions = (data) => {
    
    let fullPermissions={}

    let permissionList = [
        "user_permissions",
        // "history_permissions",
        "branch_permissions",
        "department_permissions",
        "designation_permissions",
        "employee_permissions",
        "client_permissions",
        // "comment_permissions",
        // "milestone_permissions",
        "project_permissions",
        "task_permissions",
        "worksheet_permissions"
    ]

    let user=data.filter(i=>i.model==='user')
    let branch=data.filter(i=>i.model==='branch')
    let designation=data.filter(i=>i.model==='designation')
    let department=data.filter(i=>i.model==='department')
    let client=data.filter(i=>i.model==='client')
    let employee=data.filter(i=>i.model==='employee')
    let project=data.filter(i=>i.model==='project') 
    let task=data.filter(i=>i.model==='task') 
    let worksheet=data.filter(i=>i.model==='worksheet') 
    
    let permissionDict = {
        "USER":data.filter(i=>i.model==='user').map(i=>i.value),
        // "HISTORY":[46,47],
        "BRANCH":data.filter(i=>i.model==='branch').map(i=>i.value),
        "DEPARTMENT":data.filter(i=>i.model==='department').map(i=>i.value),
        "DESIGNATION":data.filter(i=>i.model==='designation').map(i=>i.value),
        "EMPLOYEE":data.filter(i=>i.model==='employee').map(i=>i.value),
        "CLIENT":data.filter(i=>i.model==='client').map(i=>i.value),
        // "COMMENT":[125,126,127,128],
        // "MILESTONE":[146,147,148,149],
        "PROJECT":data.filter(i=>i.model==='project').map(i=>i.value),
        "TASK":data.filter(i=>i.model==='task').map(i=>i.value),
        "WORKSHEET":data.filter(i=>i.model==='worksheet').map(i=>i.value)
    }

    fullPermissions={
        'permissionList':permissionList,
        'permissionDict':permissionDict,

        'USER':user,
        'BRANCH':branch,
        'DEPARTMENT':department,
        'DESIGNATION':designation,
        'CLIENT':client,
        'EMPLOYEE':employee,
        'PROJECT':project,
        'TASK':task,
        'WORKSHEET':worksheet
    }
    return fullPermissions
}