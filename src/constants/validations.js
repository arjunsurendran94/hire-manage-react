var letters = /^[-a-zA-Z0-9-()]+(\s+[-a-zA-Z0-9-()]+)*$/;
export const alphabetOnly =  value => {
    return(value && value.match(letters) ? undefined : ' must be characters only')
}