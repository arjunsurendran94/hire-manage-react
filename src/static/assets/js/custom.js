$(document).ready(function() {
	$(document).on("click", '[data-toggle="sidemenu"]', function() {
	  $("body").toggleClass("open");
	});

	$(document).on('click', '[data-toggle="dropmenu"]', function(){
	  $(this).parent().toggleClass('open');
	});
});

$('#clearall').click(function () {
  $('#notification-clear').submit();
});


jQuery(document).ready(function() {
	jQuery(".sidemenu-bx").scrollbar();
});

/* change organization function to trigger organization change */
function changeOrganization(slug){
	$("#change-organization-form [name='organization']").val(slug);
	$("#change-organization-form").submit();
}

/* submiting this form when changing an organization */
$('form#change-organization select').change(function(){
	$('form#change-organization').submit();
})

/* preventing multiple click on form submission */
$(document).on('submit', 'form', function(){
	$(this).find(':submit').attr( 'disabled','disabled' );
});

/* preventing multiple click on modal form submission */
$(document).on('submit', '.form-modal form', function(){
	$('.form-modal').find(':submit').attr( 'disabled','disabled' );
});

$('.menu-group-item.inner-menu').click(function(){

	$('.menu-group-item.inner-menu').toggleClass("open");
	$('.menu-group-item.inner-menu .icon-right-pos').toggleClass("icon-chevron-arrow-down");
	$('.menu-group-item.inner-menu .icon-right-pos').toggleClass("icon-chevron-arrow-up");
	$('.menu-group-item.inner-menu').next('.inner-menu-item').toggleClass("d-none");

});