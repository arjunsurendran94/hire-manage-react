$(document).ready(function(){
    /*$(".memeber-name").mouseenter(function(){
        $(this).siblings(".member-popup").addClass("d-block");
    });
    $(".memeber-name").mouseleave(function(){
        $(this).siblings(".member-popup").hide();
    });
    /*$(".memeber-name").hover(function(){
        $(this).siblings(".member-popup").addClass("d-block");
    });
    $(".memeber-name").mouseleave(function(){
        $(this).siblings(".member-popup").removeClass("d-block");
    });*/
    /*$(function(){
        $(window).width(function(){
            console.log($(this).width())
            if($(this).width() <992){
            $('.braches').addClass('braches2')
            }
            else{
            $('.braches').removeClass('braches2')
            }
        });
    });
    $('#brw-btn').inputFileText({
        text: 'Upload Profile Photo'
    });*/
    $('.tool_tip').tooltip({trigger:'manual'}).tooltip('show');  
    
    $('.pjt-lst').owlCarousel({
        loop:false,
        margin:10,
        nav:true,
        navText : ['<i class="icon-chevron-arrow-left" aria-hidden="true"></i>','<i class="icon-chevron-arrow-right" aria-hidden="true"></i>'],
        navContainer: '.projt-list .custom-nav',
        responsive:{
            0:{
                items:1
            },
            550:{
                items:2
            },
            1120:{
                items:3
            },
            1380:{
                items:4
            },
            1650:{
                items:5
            }
        }
    });
    var width = $(window).width();
    $(window).width(function () {
        if (width < 992) {
            $(".braches").addClass("braches2");
            $('.positions').addClass('branch-ins');
        }
    });
    $(".braches2").click(function(){
        $(".branch-in").show();
        $(".branch-i").hide();
        $(".branch-ins2").hide();
    });
    $(".branch-ins").click(function(){
        $(".branch-in").hide();
        $(".branch-i").hide();
        $(".branch-ins2").show();
    });
    $(".back").click(function(){
        $(".branch-i").show();
        $(".branch-in").hide();
        $(".branch-ins2").hide();
    });
    $(".back2").click(function(){
        $(".branch-in").show();
        $(".branch-i").hide();
        $(".branch-ins2").hide();
    });
    $(".add-dpt").click(function(){
        $('#myModal').modal('show');
    });
    /*var height=$(window).height();
    $('.bg-wit').css('height', height);*/

});
$(window).on('load',function(){
    $('#myModal').modal('show');
});